﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Threading;
using System.Runtime.InteropServices;

using Ivk.IOSys;
using Tdm;
using AfxEx;

namespace Tdm.Unification
{
    [StructLayout(LayoutKind.Explicit)]
    public struct UniFloatUint32Union
    {
        [FieldOffset(0)]
        public float _Float;

        [FieldOffset(0)]
        public UInt32 _Uint;
    }
    /// <summary>
    /// Делегат обработки события - изменилось состояние подсистемы ввода
    /// </summary>
    /// <param name="uos">Подсистема ввода которая изменилась</param>
    /// <param name="newState">Новое состояние подсистемы</param>
    /// <param name="oldState">Старое состояние подсистемы</param>
    public delegate void OnConnectionStateChanged(UniObjSystem uos, UniObjSystem.LinkState newState, UniObjSystem.LinkState oldState);
    
    /// <summary>
    /// Класс для работы фильтра по ДСам. 
    /// </summary>
    public class DiagStateFilter : DlxObject
    {
        /// <summary>
        /// ID ДСа
        /// </summary>
        public uint DiagStateID;
        /// <summary>
        /// Наименование ДС
        /// </summary>
        public string DiagStateName;

        /// <summary>
        /// Тип объекта (ДДЦ)
        /// </summary>
        public uint Type;

        /// <summary>
        /// Подтип объекта (ДДЦ)
        /// </summary>
        public uint SubType;
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            DiagStateID = Loader.GetAttributeUInt("DiagStateID", 0);
            DiagStateName = Loader.GetAttributeString("DiagStateName", string.Empty);
            Type = Loader.GetAttributeUInt("Type", 0);
            SubType = Loader.GetAttributeUInt("SubType", 0);
        }
    }
    
    /// <summary>
	/// Класс подсистемы унифицированных объектов
	/// </summary>
	public class UniObjSystem : ControlObjSystem
	{
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

            Site site = Loader.GetOwnerOfType(typeof(Site)) as Site;
			if (site != null && site.ID > 0)
                _siteID = (uint)site.ID;
            else
			    _siteID = Loader.GetAttributeUInt("SiteID", 0);

			_concentrator = Loader.GetObjectFromAttribute("Concentrator")/*, null, null , false)*/ as UnificationConcentrator;

			var xmldsFilters = Loader.CurXmlElement.Elements.Where(x => (x.GetName() == "FILTERS")).FirstOrDefault();
			if(xmldsFilters != null)
			{
				_dsFilters = Loader.GetObject("FILTERS") as DlxCollection;

				foreach(var filter in xmldsFilters.Elements)
				{
					if (filter.Attributes.ContainsKey("Enable") && (filter.Attributes["Enable"] == "false"))
					{
						var obj =_dsFilters.GetObject(filter.Attributes["NAME"]);
						_dsFilters.RemoveObject(obj as DlxObject);
					}
				}
			}
        }

		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>удачная ли инициализация</returns>
		public override void Create()
		{
            base.Create();

			if (_siteID == 0)
			{
				if (Owner is Site)
				{
					_siteID = (uint) (Owner as Site).ID;
				}
				else
				{
					throw new ArgumentException("UniObjSystem: wrong site id!");
				}
			}

			_idobjTOrefMAPuni = GetIDtoRefMap();
			if (_idobjTOrefMAPuni == null)
			{
				throw new ArgumentException("UniObjSystem: can't create IDtoRefMap!");
			}

			BaseIOSystem iosys = FindObject("../IOSys") as BaseIOSystem;
            if (_concentrator != null)
            {
                if (iosys != null)
                {
                    _concentrator.AddUpdateDependence(iosys, this);
                }
                // подписываемся на себя
                _concentrator.AddUpdateDependence(this, this);
            }
		}
		/// <summary>
		/// Обработка данных в виде объекта
		/// </summary>
		/// <param name="dataObj"></param>
		public virtual void ProcessDataObj(object dataObj)
		{
			// проверяем - если прошло 30 секунд, то вызываем обновление индикаторов количество ситуаций
			if (((TimeSpan)(_lastDiagNotify - DataTime)).TotalSeconds >= 30)
			{
				_lastDiagNotify = DataTime;
				Call_DiagStatesCounterChange();
			}

			if (dataObj == null)
				return;

			// если тип данных - состояние
			if (typeof(UnificationConcentrator.UniObjState).IsInstanceOfType(dataObj))
			{
				UnificationConcentrator.UniObjState state = dataObj as UnificationConcentrator.UniObjState;
				if (state._siteID != SiteID)
					return;


				IOBaseElement obj = null;
				if (_idobjTOrefMAPuni.TryGetValue((int)state._objectID, out obj) && (obj is UniObject))
				{
					(obj as UniObject).SetObjState(state._uniState);
				}
				else
				{
					Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + state._objectID.ToString());
				}
			}
			// если тип данных - диагностическое состояние
			else if (typeof(UnificationConcentrator.UniDiagState).IsInstanceOfType(dataObj))
			{
				UnificationConcentrator.UniDiagState diagstate = dataObj as UnificationConcentrator.UniDiagState;
				if (diagstate._siteID != SiteID)
					return;

				IOBaseElement obj = null;
				if (_idobjTOrefMAPuni.TryGetValue((int)diagstate._objectID, out obj) && (obj is UniObject))
				{
					if (diagstate._endTime == DateTime.MinValue)
					{
						if (diagstate._detectTime == DateTime.MinValue)
							(obj as UniObject).ActivateObjDiagState(diagstate._uniDiagState);
						else
							(obj as UniObject).ActivateObjDiagState(diagstate._uniDiagState, diagstate._detectTime);
					}
					else
					{
						(obj as UniObject).DeactivateObjDiagState(diagstate._uniDiagState, diagstate._detectTime, diagstate._endTime);
					}
				}
				else
				{
					Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + diagstate._objectID.ToString());
				}
			}
			// если тип данных - числовой параметр
			else if (typeof(UnificationConcentrator.UniNumericParamState).IsInstanceOfType(dataObj))
			{
				UnificationConcentrator.UniNumericParamState numparam = dataObj as UnificationConcentrator.UniNumericParamState;
				if (numparam._siteID != SiteID)
					return;

				UniObjData data = null;
				if (_idparamTOrefMAPuni.TryGetValue(numparam._paramID, out data))
				{
					if (data.SetParamState(numparam._paramValue))
						SetObjParamState(data, numparam._paramTime);
				}
				else
				{
					Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown param id " + numparam._paramID.ToString());
				}
			}
			// если тип данных - нечисловой параметр
			else if (typeof(UnificationConcentrator.UniDataParamState).IsInstanceOfType(dataObj))
			{
				UnificationConcentrator.UniDataParamState dataparam = dataObj as UnificationConcentrator.UniDataParamState;
				if (dataparam._siteID != SiteID)
					return;

				UniObjData data = null;
				if (_idparamTOrefMAPuni.TryGetValue(dataparam._paramID, out data))
				{
					if (data.SetParamState(dataparam._paramValue))
						SetObjParamState(data, dataparam._dataTime);
				}
				else
				{
					Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown param id " + dataparam._paramID.ToString());
				}
			}
			else if (typeof(UnificationConcentrator.UniObjWork).IsInstanceOfType(dataObj))
			{
				UnificationConcentrator.UniObjWork work = dataObj as UnificationConcentrator.UniObjWork;
				if (work._siteID != SiteID)
					return;

				IOBaseElement obj = null;
				if (_idobjTOrefMAPuni.TryGetValue((int)work._objectID, out obj) && (obj is UniObject))
				{
                    (obj as UniObject).ActivateObjWork(work._flag, work._workID, work._punktID, work._beginTime, work._endTime, work._diags);
				}
				else
				{
					Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + work._objectID.ToString());
				}
			}
			else 
				throw new ArgumentException("UniObjSystem " + Owner.Name + "/" + Name + "can't process data obj - unknown type!");
			// для того, что бы вызвалось обновление при вызове SetValidData
			IsValidData = false;
		}
		/// <summary>
		/// Установить неизвестное состояние для всех дочерних объектов объекта Obj
		/// </summary>
		/// <param name="Obj">объект</param>
		/// <param name="invalidState">значения состояния - неизвестное сост объекта</param>
		static protected void SetInvalidSubObjectsState(IOBaseElement Obj, int invalidState)
		{
			if (Obj.SubElements != null)
			{
				foreach (DlxObject subdlx in Obj.SubElements.Objects)
				{
					UniObject sub = subdlx as UniObject;
					if (sub != null)
					{
						sub.SetInvalidObjState(invalidState);
						SetInvalidSubObjectsState(sub, invalidState);
					}
				}
			}
		}

        /// <summary>
        /// Поиск ДС в фильтрах по ид сбоя и типу объекта 
        /// </summary>
        /// <param name="idDs">Унифицированный ид ДС</param>
        /// <param name="type">Тип объекта</param>
        /// <returns></returns>
        public bool FindDiagStateInFilter(int idDs, ObjType type)
        {
            // если нет фильтров то все нормально, пропускаем ДС
            if ((DSFilters != null) && (DSFilters.Objects.Any()))
            {
                if (idDs == -1) return false;
				foreach(DlxCollection filter in DSFilters.Objects)
				{
					if(filter.Objects.Count(x => ((x as DiagStateFilter).DiagStateID == idDs) && // ищем совпадения по ид
												 (((x as DiagStateFilter).Type == type.SubGroupID) || (x as DiagStateFilter).Type == 0) && // по типу объекта 
												 (((x as DiagStateFilter).SubType == type.SubObjectID) || (x as DiagStateFilter).SubType == 0 || type.SubObjectID == 0)) > 0)    // по подтипу объекта  
					{
						return true;
					}
				}
            }
            else
                return true; // если нет никого в списках, то все нормально отрабатываем как обычно
            return false;
        }

        /// <summary>
        /// Поиск ДС в фильтрах с ид объекта (для поиска типа)
        /// </summary>
        /// <param name="idDs">ИД ДС</param>
        /// <param name="ObjID">ИД Объекта</param>
        /// <returns></returns>
        public bool FindDiagStateInFilter(int idDs, int ObjID)
        {
            if ((DSFilters != null) && (DSFilters.Objects.Any()))
            {
                IOBaseElement uniObj;
                if (this.UniObjsDictionary.TryGetValue(ObjID, out uniObj))     // получаем унифицированный объект
                {
                    if (uniObj != null)
                        return FindDiagStateInFilter(idDs, (uniObj as UniObject).Type);     // отправляем тип
                }
            }
            else
                return true; // если нет никого в списках, то все нормально отрабатываем как обычно
            return false;
        }

        /// <summary>
        /// Установить неизвестное значение всех объектов подсистемы объектов
        /// </summary>
        public virtual void SetInvalidObjectsState()
		{
			SetInvalidSubObjectsState(this, (int) _unknownStateID);
			SetInvalidData();
		}
		/// <summary>
		/// Обновление состояния объектов
		/// </summary>
		public void UpdateObjectStates()
		{
			NotifyForEndPeriods();
			// обновляем состояния
			foreach (UniObject obj in SubElements.Objects)
			{
				if (obj != null)
				{
					obj.UpdateObjectState();
					obj.UpdateSubObjectStates();
				}
			}
			// обновляем диагностику
			foreach (UniObject obj in SubElements.Objects)
			{
				if (obj != null)
				{
					obj.UpdateObjectDiagState();
					obj.UpdateSubObjectDiagStates();
				}
			}
		}
		/// <summary>
		/// получить значение параметра диагностирования по пути
		/// </summary>
		/// <param name="paramPath">полное имя параметра диагностирования</param>
		/// <param name="objID">идентификатор объекта</param>
		/// <param name="paramID">идентификаор параметра</param>
		/// <returns>значение парметра (если параметр не задан, то возвращается float.MaxValue)</returns>
		public virtual float GetDiagParamValue(int objID, uint paramID, string paramPath)
		{
			return float.MaxValue;
		}
		/// <summary>
		/// Изменилось состояние параметра
		/// </summary>
		/// <param name="data">параметр</param>
		/// <param name="parTime">время измерения</param>
		public virtual void SetObjParamState(UniObjData data, DateTime parTime)
		{
			if (data.IsSigAnalog)
			{
				try
				{
                    // Если сигнал принят с ошибкой, то обрабатываем это
                    if (data.SigAnalog.IOSignals[0].IsErrorState == true) throw new ArgumentOutOfRangeException();

                    data._lastSetParamState = (float)data.SigAnalog.Double;
					data._lastSetParamTime = parTime;

					OnObjectParamState(0, data.ParamID, parTime, data._lastSetParamState);
				}
				catch (ArgumentOutOfRangeException)
				{
					data._lastSetParamState = float.NaN;
					data._lastSetParamTime = parTime;
					OnObjectParamState(0, data.ParamID, parTime, data._lastSetParamState);
				}
			}
            else if (data.IsComputed)
            {
                data._lastSetParamState = (data.SigDiscreteLogic as ControlSubTask).State;
                data._lastSetParamTime = parTime;

                OnObjectParamState(0, data.ParamID, parTime, data._lastSetParamState);
            }
            //else if (data.IsDouble)
            else if (data.IsNumeric)
			{
				data._lastSetParamState = (float) data.Double;
				data._lastSetParamTime = parTime;

				OnObjectParamState(0, data.ParamID, parTime, data._lastSetParamState);
			}
			else // нечисловой параметр
			{
				if (data.IsIOSignal)
				{
					char[] chbuf = data.IOSignal.StringState.ToCharArray();
					byte[] buf = new byte[chbuf.Length];
					for (int i = 0; i < chbuf.Length; i++)
						buf[i] = (byte)chbuf[i];
					OnObjectDataParamState(0, data.ParamID, parTime, buf);
					data._lastSetParamTime = parTime;
				}
				else if (data.IsString)
				{
					char[] chbuf = data.String.ToCharArray();
					byte[] buf = new byte[chbuf.Length];
					for (int i = 0; i < chbuf.Length; i++)
						buf[i] = (byte)chbuf[i];
					OnObjectDataParamState(0, data.ParamID, parTime, buf);
					data._lastSetParamTime = parTime;
				}
			}
		}
		/// <summary>
		/// Добавить унифицированный параметр в map
		/// </summary>
		/// <param name="data"></param>
		public void AddObjParamToMap(UniObjData data)
		{
			_idparamTOrefMAPuni.Add(data.ParamID, data);
		}
		/// <summary>
		/// Статический метод записи сосотяния объектов в унифицированноом формате с проходом по иерархии
		/// </summary>
		/// <param name="element">элемент, для которого нужно записать дочерние элементы</param>
		/// <param name="writer">поток данных</param>
		/// <param name="Count">счетчик записанных объектов</param>
		/// <returns></returns>
		static public void WriteSubObjectsState(IOBaseElement element, BinaryWriter writer, ref int Count)
		{
			if (element.SubElements != null)
			{
				foreach (BaseControlObj sub in element.SubElements.Objects)
				{
					if (sub != null)
					{
						writer.Write((UInt32) sub.Number);
						writer.Write((byte)sub.ObjState.Current);
						Count++;

						WriteSubObjectsState(sub, writer, ref Count);
					}
				}
			}
		}
		/// <summary>
		/// Запись сосотяния всех объктов подсистемы в старом унифицированном формате
		/// </summary>
		/// <param name="writer">проток данных</param>
		public virtual void WriteOverallState_old(BinaryWriter writer)
		{
			writer.Write((uint)_siteID);
			writer.Flush();
			long pos_count = writer.BaseStream.Position;
			int Count = 0;
			writer.Write((UInt16)Count);

			WriteSubObjectsState(this, writer, ref Count);

			writer.Flush();
			long pos_end = writer.BaseStream.Position;
			writer.BaseStream.Seek(pos_count, SeekOrigin.Begin);
			writer.Write((UInt16)Count);
			writer.BaseStream.Seek(pos_end, SeekOrigin.Begin);
		}
		/// <summary>
		/// Запись сосотяния всех объктов подсистемы в новом унифицированном формате
		/// </summary>
		/// <param name="writer">поток данных</param>
		public virtual void WriteOverallState(BinaryWriter writer)
		{
			writer.Write((uint)_siteID);
			writer.Flush();
			long pos_count = writer.BaseStream.Position;
			int Count = 0;
			writer.Write((UInt32)Count);

			WriteSubObjectsState(this, writer, ref Count);

			writer.Flush();
			long pos_end = writer.BaseStream.Position;
			writer.BaseStream.Seek(pos_count, SeekOrigin.Begin);
			writer.Write((UInt32)Count);
			writer.BaseStream.Seek(pos_end, SeekOrigin.Begin);
		}

		#region Методы извещения об изменении состояний объекта и его параметров

		/// <summary>
		/// Изменение состояния объекта
		/// </summary>
		/// <param name="objectID">Идентификатор объекта</param>
		/// <param name="uniState">Унифицированное состояние</param>
		/// <param name="uniPrevState">предыдущее унифицированное состояние</param>
		public virtual void OnObjectState(UInt32 objectID, byte uniState, byte uniPrevState)
		{
			if (_concentrator == null)
				return;

			UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
			state._dataTime = DataTime;
			state._siteID = _siteID;
			state._objectID = objectID;
			state._uniState = uniState;
			state._uniPrevState = uniPrevState;
			_concentrator.AddDataToConsumers(state, this);
		}
		/// <summary>
		/// Изменение диагностического состояния объекта
		/// </summary>
		/// <param name="objectID">Идентификатор объекта</param>
		/// <param name="uniDiagState">Унифицированное диагностическое состояние</param>
		/// <param name="detectTime">Время выявления</param>
		/// <param name="endTime">Время окончания</param>
		public virtual void OnObjectDiagState(UInt32 objectID, int uniDiagState, DateTime detectTime, DateTime endTime)
		{
			if (_concentrator == null)
				return;
			UnificationConcentrator.UniDiagState state = new UnificationConcentrator.UniDiagState();
			state._dataTime = DataTime;
			state._siteID = _siteID;
			state._objectID = objectID;
			state._uniDiagState = uniDiagState;
			state._detectTime = detectTime;
			state._endTime = endTime;
			_concentrator.AddDataToConsumers(state, this);
		}
		/// <summary>
		/// Изменение параметра рбъекта
		/// </summary>
		/// <param name="objectID">Идентификатор объекта</param>
		/// <param name="paramID">Идентификатор параметра</param>
		/// <param name="paramTime">Время измерения</param>
		/// <param name="paramValue">Значение параметра</param>
		public virtual void OnObjectParamState(UInt32 objectID, UInt32 paramID, DateTime paramTime, float paramValue)
		{
			if (_concentrator == null)
				return;

			UnificationConcentrator.UniNumericParamState state = new UnificationConcentrator.UniNumericParamState();
			state._dataTime = DataTime;
			state._siteID = _siteID;
			state._objectID = objectID;
			state._paramID = paramID;
			state._paramTime = paramTime;

            if (this._idparamTOrefMAPuni.ContainsKey(paramID) && this._idparamTOrefMAPuni[paramID]._isRizolation)
                state._paramValue = !_idparamTOrefMAPuni[paramID]._isIMSI && paramValue == 0 
                    ? UniObjData.IzolationMaxAdkMeasuredValue 
                    : paramValue * 1000;
            else
                state._paramValue = paramValue;
            
			_concentrator.AddDataToConsumers(state, this);
		}
		/// <summary>
		/// Изменение нечислового параметра
		/// </summary>
		/// <param name="objectID">Идентификатор объекта</param>
		/// <param name="paramID">Идентификатор параметра</param>
		/// <param name="paramTime">Время измерения</param>
		/// <param name="paramValue">Значение параметра</param>
		public virtual void OnObjectDataParamState(UInt32 objectID, UInt32 paramID, DateTime paramTime, byte[] paramValue)
		{
			if (_concentrator == null)
				return;

			UnificationConcentrator.UniDataParamState state = new UnificationConcentrator.UniDataParamState();
			state._dataTime = DataTime;
			state._siteID = _siteID;
			state._objectID = objectID;
			state._paramID = paramID;
			state._paramValue = new byte[paramValue.Length];
			System.Array.Copy(paramValue, state._paramValue, paramValue.Length);
			_concentrator.AddDataToConsumers(state, this);
		}
		
		/// <summary>
		/// Выявлено ТО
		/// </summary>
		public virtual void OnObjectWork(UInt32 objectID, UnificationConcentrator.UniObjWork.Kind kind, UInt32 workID, UInt32 punktID, DateTime begTime, DateTime endTime, Dictionary<UInt16, DateTime> diags)
		{
			if (_concentrator == null)
				return;

			UnificationConcentrator.UniObjWork state = new UnificationConcentrator.UniObjWork();
			state._siteID = _siteID;
			state._objectID = objectID;
            state._flag = kind;
			state._workID = workID;
			state._punktID = punktID;
			state._beginTime = begTime;
			state._endTime = endTime;
            state._dataTime = endTime;

            //TODO: ! copy of diags made
            Dictionary<ushort, DateTime> localDiags = new Dictionary<ushort, DateTime>();
            foreach (ushort key in diags.Keys)
                localDiags.Add(key, diags[key]);
            
            state._diags = localDiags;
			_concentrator.AddDataToConsumers(state, this);
		}

		#endregion

		/// <summary>
		/// Идентификатор места
		/// </summary>
		public UInt32 SiteID
		{
			get
			{
				return _siteID;
			}
		}
		/// <summary>
		/// идентификатор места
		/// </summary>
		protected UInt32 _siteID;
		/// <summary>
		/// Ссылка на концентратор
		/// </summary>
		protected UnificationConcentrator _concentrator;
		public UnificationConcentrator Concentrator
		{
			get { return _concentrator; }
		}
        /// <summary>
        /// Список ДС для фильтрации
        /// </summary>
        protected DlxCollection _dsFilters;
        public DlxCollection DSFilters
        {
            get { return _dsFilters; }
        }
        /// <summary>
        /// идентификатор неизвестного состояния
        /// </summary>
        protected byte _unknownStateID = 0;
		/// <summary>
		/// Map объектов
		/// </summary>
		public Dictionary<int, IOBaseElement> UniObjsDictionary
		{
			get { return _idobjTOrefMAPuni; }
		}
		/// <summary>
		/// Map для объектов контроля
		/// </summary>
		protected Dictionary<int, IOBaseElement> _idobjTOrefMAPuni = null;
		/// <summary>
		/// Map параметров
		/// </summary>
		public Dictionary<uint, UniObjData> UniParamsDictionary
		{
			get { return _idparamTOrefMAPuni; }
		}
		/// <summary>
		/// Map для унифицированных параметров
		/// </summary>
		protected Dictionary<UInt32, UniObjData> _idparamTOrefMAPuni = new Dictionary<UInt32, UniObjData>();

		#region Оповещение об истечении промежутоков времени
		/// <summary>
		/// Оповещение об истечении промежутоков времени
		/// </summary>
		protected void NotifyForEndPeriods()
		{
			if (_notificationList.Count == 0)
			{
				_lastNotifyTime = DataTime;
				return;
			}

			long period = (long) ((TimeSpan)(DataTime - _lastNotifyTime)).TotalMilliseconds;
			_lastNotifyTime = DataTime;

			while (period > 0)
			{
				if (period >= _notificationList[0]._millisecondsOffset)
				{
					_notificationList[0]._notification(this, DataTime, true);
					period -= _notificationList[0]._millisecondsOffset;
					_notificationList.RemoveAt(0);

					if (_notificationList.Count == 0)
						return;
				}
				else
				{
					_notificationList[0]._millisecondsOffset -= period;
				}
			}
		}
		/// <summary>
		/// Оповещение об истечении промежутоков времени
		/// </summary>
		/// <param name="elem">элемент</param>
		/// <param name="time">время</param>
		/// <param name="force">принудительно</param>
		public delegate void Notification(IOBaseElement elem, DateTime time, bool force);
		/// <summary>
		/// Добавить делегат
		/// </summary>
		/// <param name="milliseconds">время, через которое вызвать</param>
		/// <param name="notification">делегат</param>
		public void AddNotificationEvent(int milliseconds, Notification notification)
		{
			long offset = 0;
			for (int i = 0; i < _notificationList.Count; i++)
			{
				if (offset + _notificationList[i]._millisecondsOffset > milliseconds)
				{
					_notificationList[i]._millisecondsOffset = offset + _notificationList[i]._millisecondsOffset - milliseconds;
					_notificationList.Insert(i, new NotificationElem(milliseconds - offset, notification));
					return;
				}

				offset += _notificationList[i]._millisecondsOffset;
			}
			_notificationList.Add(new NotificationElem(milliseconds - offset, notification));
		}
		/// <summary>
		/// елемент списка извещений
		/// </summary>
		public class NotificationElem
		{
			/// <summary>
			/// конструктор
			/// </summary>
			/// <param name="milliseconds">мимллисекунды вызова</param>
			/// <param name="notification">делегат</param>
			public NotificationElem(long milliseconds, Notification notification)
			{
				_millisecondsOffset = milliseconds;
				_notification = notification;
			}
			/// <summary>
			/// миллисекунды вызова
			/// </summary>
			public long _millisecondsOffset;
			/// <summary>
			/// делегат
			/// </summary>
			public Notification _notification;
		}
		/// <summary>
		/// список извещений
		/// </summary>
		protected List<NotificationElem> _notificationList = new List<NotificationElem>();
		/// <summary>
		/// время последнего извещения
		/// </summary>
		protected DateTime _lastNotifyTime = DateTime.MinValue;

		#endregion

		protected DateTime _lastDiagNotify = DateTime.MinValue;

		
		/// <summary>
		/// Общие состояния связи
		/// </summary>
		public enum LinkState
		{
			/// <summary>
			/// Есть связю
			/// </summary>
			OK = 0,
			/// <summary>
			/// Нет связи у этого процесса
			/// </summary>
			LocalConnectionError,
			/// <summary>
			/// Нет связи у удаленного процесса
			/// </summary>
			RemoteConnectionError
		} ;
        private const string LocalConnectionErrorString = "Нет связи с сервером";
        private const string RemoteConnectionErrorString = "Нет связи сo станцией";
        private const string OKString = "Связь в норме";
        
        /// <summary>
        /// Получить текстовое описание состояния связи
        /// </summary>
        /// <param name="state">Состояние связи</param>
        /// <returns>Текстовое описание состояния связи</returns>
        public static string GetLinkStateDescription(LinkState state)
        {
            switch(state)
            {
                case LinkState.OK:
                    return OKString;
                case LinkState.LocalConnectionError:
                    return LocalConnectionErrorString;
                case LinkState.RemoteConnectionError:
                    return RemoteConnectionErrorString;
                default:
                    {
                        Debug.Assert(false);
                        return string.Empty;
                    }
            }
        }


		protected LinkState _linkState = LinkState.LocalConnectionError;
        /// <summary>
        /// Состояние связи подсистемы ввода
        /// </summary>
		public LinkState ConnectionState
		{
			get { return _linkState; }
			set 
            {
			    LinkState oldState = _linkState;
                _linkState = value;
                if (value != oldState && ConnectionStateChanged != null)
                    ConnectionStateChanged(this, value, oldState);
            }
		}

        /// <summary>
        /// Событие. Состояние связи подсистемы ввода изменилось
        /// </summary>
        public event OnConnectionStateChanged ConnectionStateChanged;

	}
	
	/// <summary>
	/// Класс подсистемы унифицированных объектов с автоматическим созданием объектов при приходе данных
	/// </summary>
	public class UniObjSystemAutoObjects : UniObjSystem
	{
        /// <summary>
        /// Cоздание объекта (если не было раньше) по пришедшим данным + обработка данных методом базового класса
		/// </summary>
		/// <param name="dataObj">Элмент данных</param>
        public override void ProcessDataObj(object dataObj)
		{
			if (dataObj != null)
			{	
				// если тип данных - состояние
				if (typeof(UnificationConcentrator.UniObjState).IsInstanceOfType(dataObj))
				{
					UnificationConcentrator.UniObjState state = dataObj as UnificationConcentrator.UniObjState;
					if (state._siteID != SiteID)
						return;

					IOBaseElement obj = null;
					if (!_idobjTOrefMAPuni.TryGetValue((int)state._objectID, out obj))
					{
						//TODO: create new object - Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + state._objectID.ToString());
                        AddNewUniObject((int)state._objectID);
					}
				}
				// если тип данных - диагностическое состояние
				else if (typeof(UnificationConcentrator.UniDiagState).IsInstanceOfType(dataObj))
				{
					UnificationConcentrator.UniDiagState diagstate = dataObj as UnificationConcentrator.UniDiagState;
					if (diagstate._siteID != SiteID)
						return;

					IOBaseElement obj = null;
					if (!_idobjTOrefMAPuni.TryGetValue((int)diagstate._objectID, out obj))
					{
						//TODO: create new object - Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + diagstate._objectID.ToString());
                        AddNewUniObject((int)diagstate._objectID);
					}
				}
				// если тип данных - числовой параметр
				else if (typeof(UnificationConcentrator.UniNumericParamState).IsInstanceOfType(dataObj))
				{
					UnificationConcentrator.UniNumericParamState numparam = dataObj as UnificationConcentrator.UniNumericParamState;
					if (numparam._siteID != SiteID)
						return;

					UniObjData data = null;
					if (!_idparamTOrefMAPuni.TryGetValue(numparam._paramID, out data))
					{
						//TODO: create new param of type double - Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown param id " + numparam._paramID.ToString());
                        data = new UniObjData(numparam._objectID, numparam._paramID, true);
                        _idparamTOrefMAPuni.Add(numparam._paramID, data);
					}
				}
				// если тип данных - нечисловой параметр
				else if (typeof(UnificationConcentrator.UniDataParamState).IsInstanceOfType(dataObj))
				{
					UnificationConcentrator.UniDataParamState dataparam = dataObj as UnificationConcentrator.UniDataParamState;
					if (dataparam._siteID != SiteID)
						return;

					UniObjData data = null;
					if (!_idparamTOrefMAPuni.TryGetValue(dataparam._paramID, out data))
					{
						//TODO: create new param of type string - Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown param id " + dataparam._paramID.ToString());
                        data = new UniObjData(dataparam._objectID, dataparam._paramID, false);
                        _idparamTOrefMAPuni.Add(dataparam._paramID, data);
					}
				}
				else if (typeof(UnificationConcentrator.UniObjWork).IsInstanceOfType(dataObj))
				{
					UnificationConcentrator.UniObjWork work = dataObj as UnificationConcentrator.UniObjWork;
					if (work._siteID != SiteID)
						return;

					IOBaseElement obj = null;
					if (!_idobjTOrefMAPuni.TryGetValue((int)work._objectID, out obj))
					{
						//TODO: create new object - Debug.WriteLine("UniObjSystem " + Owner.Name + "/" + Name + ": Unknown obj id " + diagstate._objectID.ToString());
						obj = new UniObject((int)work._objectID, string.Empty, 0, 0);
						SubElements.AddObject(obj);
						_idobjTOrefMAPuni.Add((int)work._objectID, obj);
					}
				}
			}

			base.ProcessDataObj(dataObj);
		}

        protected void AddNewUniObject(int id)
        {
            UniObject obj = new UniObject(id, string.Empty, 0, 0);
            (obj as UniObject)._objSystem = this;
            SubElements.AddObject(obj);
            _idobjTOrefMAPuni.Add(id, obj);
        }
	}
	
	/// <summary>
	/// Унифицированный объект
	/// </summary>
	public class UniObject : BaseControlObj
	{
		public UniObject()
		{ }
		public UniObject(int ID, string name, int grouptype, int type)
			: base(null, name, ID, new ObjType(grouptype, type))
		{
		}
		public UniObject(int ID, string name, int grouptype, int type, IEnumerable<UniObjData> objdata)
			: base(null, name, ID, new ObjType(grouptype, type))
		{
		}


		/// <summary>
		/// Диагностические знаки
		/// </summary>
		public enum UniDiagSign
		{
			/// <summary>
			/// Выявлено диагностич. сосотяние со степенью "Отказ"
			/// </summary>
			Refusal = 0,

			/// <summary>
			/// Выявлено диагностич. сосотяние со степенью "Предотказ-1"
			/// </summary>
			Unstable1 = 1,

			/// <summary>
			///  Выявлено диагностич. сосотяние со степенью "Предотказ-2"
			/// </summary>
			Unstable2 = 2,

			/// <summary>
			/// Выявлено диагностич. сосотяние со степенью "Предотказ-3"
			/// </summary>
			Unstable3 = 3,

			/// <summary>
			/// Выявлено диагностич. сосотяние со степенью "Штатно"
			/// </summary>
			Regular = 4

		};

		public override bool IsObjectHasNoDiagState
		{
			get
			{
				return false;
			}
		}

		public override bool IsObjectHasNorma
		{
			get
			{
				return false;
			}
		}

		public override bool IsObjectHasRefusal
		{
			get
			{
				foreach (var state in DiagStates)
				{
					if (state.Sign == (byte) UniDiagSign.Refusal)
						return true;
				}
				return false;
			}
		}

		public override bool IsObjectHasUnstable
		{
			get
			{
				foreach (var state in DiagStates)
				{
					if (state.Sign == (byte)UniDiagSign.Unstable1
						|| state.Sign == (byte)UniDiagSign.Unstable2
						|| state.Sign == (byte)UniDiagSign.Unstable3)
						return true;
				}
				return false;
			}
		}

		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>удачно ли прошла инициализация</returns>
		public override void Create()
		{
			// пробуем получить стандартным способом
			_objSystem = ObjSys as UniObjSystem;
			// если не получилось, то ищем самостоятельно
			if (_objSystem == null)
			{
				// ищем подсистему объектов типа UniObjSystem
				IOBaseElement elem = this;
				while (elem.Owner as IOBaseElement != null)
				{
					elem = elem.Owner as IOBaseElement;
					if (elem == null)
					{
						throw new ArgumentException("UniObject " + Name + ": Не найдена подсистема объектов типа UniObjSystem");
					}

					if (elem as UniObjSystem != null)
					{
						_objSystem = elem as UniObjSystem;
						break;
					}
				}
			}
			if (_objSystem == null)
			{
				throw new ArgumentException("UniObject " + Name + ": Не найдена подсистема объектов типа UniObjSystem");
			}

            base.Create();

			// добавляем все параметры в map подсистемы
			foreach (ObjData data in _objData.Objects)
			{
				if ((data as UniObjData) != null)
					_objSystem.AddObjParamToMap(data as UniObjData);
			}
		}

		/// <summary>
        /// Идентификатор комплекса. !=0, если отлицается от идентификатора в ObjSystem
        /// Нужен для объектов, принадлежащих перегону по АСУШ
        /// </summary>
        public UInt32 SiteID
        {
            get { return _SiteID; }
        }
        /// <summary>
        /// Идентификатор комплекса, отличный от базового
        /// </summary>
        protected UInt32 _SiteID;
        /// <summary>
        /// Загрузка
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _SiteID = Loader.GetAttributeUInt("SiteID", 0);
        }

		/// <summary>
		/// Обновить состояние объекта
		/// </summary>
		public virtual void UpdateObjectState()
		{
		}
		/// <summary>
		/// Обновить все дочерние объекты
		/// </summary>
		public void UpdateSubObjectStates()
		{
			if (SubElements == null)
				return;
			foreach (UniObject obj in SubElements.Objects)
			{
				if (obj != null)
				{
					obj.UpdateObjectState();
					obj.UpdateSubObjectStates();
				}
			}
		}
		/// <summary>
		/// Обновить диагностическое состояние объекта
		/// </summary>
		public virtual void UpdateObjectDiagState()
		{
		}
		/// <summary>
		/// Обновить диагностическое состояние всех дочерних объектов
		/// </summary>
		public void UpdateSubObjectDiagStates()
		{
			if (SubElements == null)
				return;
			foreach (UniObject obj in SubElements.Objects)
			{
				if (obj != null)
				{
					obj.UpdateObjectDiagState();
					obj.UpdateSubObjectDiagStates();
				}
			}
		}
		
		public virtual int GetInvalidObjState(int defaultInvalidState)
		{
			return defaultInvalidState;
		}
		/// <summary>
		/// Установить неизвестное состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		public virtual void SetInvalidObjState(int state)
		{
			SetObjState(GetInvalidObjState(state));
			GeneralState.Current = IOGeneralState.Unknown;
		}
		/// <summary>
		/// Установить состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		public virtual void SetObjState(int state)
		{
			if (state != _objState.Current)
			{
				Trace.WriteLine(Name + ":\t" + _objState.Current + " -> " + state);
				byte prevstate = (byte) _objState.Current;
				_objState.Current = state;
				if (state != 0)
                    GeneralState.Current = IOGeneralState.Normal;
				else
                    GeneralState.Current = IOGeneralState.Unknown;

			    IsDataChanged = true;
				_objSystem.OnObjectState((UInt32)Number, (byte)state, prevstate);
			}
		}
		/// <summary>
		/// Установить состояние по-умолчанию
		/// </summary>
		public override void SetToInitialState(bool setForSubs)
		{
			List<DiagState> copyDiagStates = DiagStates;
			DiagStates = null;
            base.SetToInitialState(setForSubs);
			DiagStates = copyDiagStates;
		}
		/// <summary>
		/// Установить диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		public virtual void ActivateObjDiagState(int state)
		{
			ActivateObjDiagState(state, _objSystem.DataTime);
		}
		/// <summary>
		/// Установить диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		/// <param name="detectTime">время выявления</param>
		public virtual void ActivateObjDiagState(int state, DateTime detectTime)
		{
			// если такое состояние уже есть, то ничего не делаем
            for (int i = 0; i < DiagStates.Count; i++)
			{
				if (DiagStates[i].Id == state)
					return;
			}

			BaseControlObj.DiagState dState = new DiagState();
			dState.Id = state;

			if (Site != null)
				dState.Sign = Site.TypeInfo.DiagStateSign(this, state);
			else
				dState.Sign = 1;

			// добавляем активное диагностичекое состояние
			AddDiagState(dState, detectTime);
			// передаем подсистеме
			_objSystem.OnObjectDiagState((UInt32)Number, state, detectTime, DateTime.MinValue);
			// запоминаем время появления
			DateTime time = DateTime.MinValue;
			if (!_diagStateTOtimeMAPuni.TryGetValue(state, out time) )
				_diagStateTOtimeMAPuni.Add(state, detectTime);
		}
		/// <summary>
		/// Снять диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		public virtual void DeactivateObjDiagState(int state)
		{
			// определяем время обнаружения
			DateTime detect;
			if (!_diagStateTOtimeMAPuni.TryGetValue(state, out detect))
				detect = DateTime.MinValue;

			DeactivateObjDiagState(state, detect, _objSystem.DataTime);
		}
		/// <summary>
		/// Снять диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		/// <param name="detectTime">время выявления</param>
		public virtual void DeactivateObjDiagState(int state, DateTime detectTime)
		{
			DeactivateObjDiagState(state, detectTime, _objSystem.DataTime);
		}
		/// <summary>
		/// Снять диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		/// <param name="detectTime">время выявления</param>
		/// <param name="endTime">время окончания ситуации</param>
		public virtual void DeactivateObjDiagState(int state, DateTime detectTime, DateTime endTime)
		{
            if (DiagStates.Count(ds => ds.Id == state) > 0)
            {
                // находим и удаляем активное диагностическое сосотяние
                RemoveDiagState(state);
                // передаем подсистеме
                _objSystem.OnObjectDiagState((UInt32)Number, state, detectTime, endTime);
                // удаляем время появления
                _diagStateTOtimeMAPuni.Remove(state);
            }
		}
		/// <summary>
		/// Выявлено выполнение ТО
		/// </summary>
        public virtual void ActivateObjWork(UnificationConcentrator.UniObjWork.Kind kind, UInt32 workID, UInt32 punktID, DateTime begTime, DateTime endTime, Dictionary<UInt16, DateTime> diags)
		{
			// передаем подсистеме
			_objSystem.OnObjectWork((UInt32)Number, kind, workID, punktID, begTime, endTime, diags);
		}
		/// <summary>
		/// Коллекция член данных
		/// </summary>
		public DlxCollection ObjDataCollection
		{
			get { return _objData; }
		}
		/// <summary>
		/// Подсистема объектов
		/// </summary>
		public UniObjSystem _objSystem = null;

		public override DateTime GetDiagDetectTime(int DiagStateID)
		{
			DateTime tm = DateTime.MinValue;
			_diagStateTOtimeMAPuni.TryGetValue(DiagStateID, out tm);
			return tm;
		}
		/// <summary>
		/// Map для появления диагностики
		/// </summary>
		public Dictionary<int, DateTime> _diagStateTOtimeMAPuni = new Dictionary<int, DateTime>();
		/// <summary>
		/// Получить перечень контролируемых работ по ТО
		/// </summary>
		public List<UInt32> Punkts
		{
			get { return _punktsTOnsi; }
		}
		/// <summary>
		/// перечень контролируемых работ по ТО
		/// </summary>
		protected List<UInt32> _punktsTOnsi = new List<uint>();

		/// <summary>
		/// Получение текста подсказки. 
		/// <remarks>Эта функция вызвается при наведении курсора мыши на граф. элемент
		/// принадлежащий объекту контроля. В случае если текст подсказки для граф. элемента 
		/// не задан</remarks>
		/// </summary>   
		public override string GetToolText()
		{
			return "%1%ss";
		}
	}
	/// <summary>
	/// Унифицированный объект
	/// </summary>
	public class UniObjectUpSt : UniObject
	{
		/// <summary>
		/// Задача формирования сосотяния на основе исходных данных
		/// </summary>
		abstract public class ObjectState : IDlx
		{
			/// <summary>
			///  Инициализация
			/// </summary>
			/// <param name="obj">родительский объект контроля</param>
			/// <returns>истина, если удачно</returns>
			public virtual bool Create(UniObject obj)
			{
				if (obj == null)
					return false;
				_controlObj = obj;
				return true;
			}
			/// <summary>
			/// Деинициализация
			/// </summary>
			public virtual void Destroy()
			{ 
			}
			/// <summary>
			/// Загрузка
			/// </summary>
			/// <param name="Loader">загрузчик</param>
			public virtual void Load(DlxLoader Loader)
			{ 
				// в базовом классе ничего не делаем
			}

			/// <summary>
			/// обработка исходных данных - вызывается, если IsModify() == TRUE
			/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
			/// </summary>
			public virtual void UpdateObjectState()
			{
                SetModify();//ResetModify();
			}

			/// <summary>
			/// Вызывается если нужно установить неизвестное состояние
			/// </summary>
			public virtual void SetInvalidObjState(int state)
			{
				SetState(state);
			}
			
			/// <summary>
			/// проверка изменения исходных данных для состояния
			/// </summary>
			/// <returns>изменились ли исходные данные</returns>
			public virtual bool IsModify()
			{ 
				return _bModify;
			}
			/// <summary>
			/// родительский объект контроля
			/// </summary>
			protected UniObject _controlObj;// родительский объект мониторинга
	
			/// <summary>
			/// вызвать при изменении состояния
			/// </summary>
			/// <param name="state">новое состояние</param>
			protected void SetState(int state)
			{
				_controlObj.SetObjState(state);
			}
			/// <summary>
			/// вызвать при изменении состояния другого объекта
			/// </summary>
			/// <param name="state">новое состояние</param>
			/// <param name="obj">объект контроля</param>
			protected void SetState(int state, UniObject obj)
			{ 
				if(obj != null)
					obj.SetObjState(state);
			}
			/// <summary>
			/// получить текущее состояние родительского объекта
			/// </summary>
			/// <returns>текущее состояние родительского объекта</returns>
			protected int GetState()
			{ 
				return _controlObj.ObjState.Current;
			}
			/// <summary>
			/// Признак изменения исходны данных
			/// </summary>
			protected bool _bModify = true;
			/// <summary>
			/// Установить признак изменения исходных данных
			/// </summary>
			protected void SetModify()
			{ 
				_bModify = true;
			}
			/// <summary>
			/// Снять признак изменения исходных данных
			/// </summary>
			protected void ResetModify()
			{ 
				_bModify = false;
			}
			/// <summary>
			/// Добавление обработчика изменения данных
			/// </summary>
			/// <param name="signal"></param>
			public void AddWatchToSignal(IOSignal signal)
			{
				IOBaseElement sigOwner = signal.Owner as IOBaseElement;
				if (sigOwner != null)
					sigOwner.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
			}

            public void AddWatchToModule(IOBaseElement sigOwner)
            {
                if (sigOwner != null)
                {
                    sigOwner.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
                    sigOwner.GeneralStateChanged += new IOBaseElement.OnGeneralStateChangedHandler(OnGeneralStateChanged);
                }
            }

			/// <summary>
			/// Обработчик изменения данных
			/// </summary>
			/// <param name="module">Модуль, данные которго изменились</param>
			/// <param name="time">Время изменения</param>
			/// <param name="force">Обработка внештатной ситуации (обыв связи)</param>
			protected virtual void OnChanged(IOBaseElement module, DateTime time, bool force)
			{
				SetModify();
			}
            protected virtual void OnGeneralStateChanged(IOBaseElement module)
            {
                SetModify();
            }

			public virtual int GetInvalidObjState(int defaultInvalidState)
			{
				return defaultInvalidState;
			}
		}

		public virtual int GetInvalidObjState(int defaultInvalidState)
		{
			if (_objStateObject != null)
				return GetInvalidObjState(defaultInvalidState);
			else
				return base.GetInvalidObjState(defaultInvalidState);
		}
		/// <summary>
		/// Загрузка (загружается задача формирования состояния)
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			IDlx obj = Loader.GetObject("STATE", null, null, false);
            if (obj != null)
                _objStateObject = obj as ObjectState;
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>удачно ли прошла инициализация</returns>
		public override void Create()
		{
            base.Create();

			if (_objStateObject != null)
			{
				if (!_objStateObject.Create(this))
                    throw new Exception("Ошибка создания _objStateObject");
			}
		}
		/*/// <summary>
		/// Деинициализация
		/// </summary>
		public override void Destroy()
		{
			base.Destroy();
			
			if (_objStateObject != null)
				_objStateObject.Destroy();
		}*/
		/// <summary>
		/// Установить неизвестное состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		public override void SetInvalidObjState(int state)
		{
			if (_objStateObject != null)
				_objStateObject.SetInvalidObjState(state);
			else
				base.SetInvalidObjState(state);
		}
		/// <summary>
		/// Обновить состояние объекта
		/// </summary>
		public override void UpdateObjectState()
		{
			base.UpdateObjectState();
			
			if (_objStateObject != null && _objStateObject.IsModify())
				_objStateObject.UpdateObjectState();

			UniObjData data;
			foreach (DlxObject objdata in _objData.Objects)
			{
				data = objdata as UniObjData;
				if (data != null)
				{
                    /* Использовалось для устранения ошибки http://nir.ugpa.ru/issues/23660#change-80114
                    if (
                        data.FullDescription == "//Концентратор/Services/SUIV9R/SiteList/Полтавская/Objects/Фидер1//F1Sig"
                        && data._lastSetParamState == 4294967.5
                        )
                    {
                        Trace.WriteLine("Некорректно обработано измерение");
                    }
                    */

                    // Когда нет признака что это ошибка
                    if (data.IsSigAnalog && !data.SigAnalog.IOSignals[0].IsErrorState)
                    {
                        UpdateEssentialParamState(data);
                    }
                    else if (data.IsComputed)
                    {
                        UpdateEssentialParamState(data, data.SigDiscreteLogic as ControlSubTask);
                    }
                    else
                    {
                        if (!float.IsNaN(data._lastSetParamState))
                            _objSystem.SetObjParamState(data, _objSystem.DataTime);
                    }
				}
			}
		}
        /// <summary>
        /// Обновление значения параметра при его существенном изменении
        /// </summary>
        /// <param name="data">Унифицированный параметр</param>
        protected void UpdateEssentialParamState(UniObjData data)
        {
            try
            {
                if (Math.Abs(data.SigAnalog.Double - data._lastSetParamState) >= UniObjData.ChangeCoef * Math.Abs(data._lastSetParamState) * (1 - ((TimeSpan)(_objSystem.DataTime - data._lastSetParamTime)).TotalSeconds / UniObjData.ChangeTimeout))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
                else if (float.IsNaN(data._lastSetParamState))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
            }
            catch (ArgumentOutOfRangeException)
            {
                if (!float.IsNaN(data._lastSetParamState))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
            }
        }
        /// <summary>
        /// Обновление значения вычисляемого параметра при его существенном изменении
        /// </summary>
        /// <param name="data">Унифицированный параметр</param>
        /// <param name="task">Вычисляемая подзадача</param>
        protected void UpdateEssentialParamState(UniObjData data, ControlSubTask task)
        {
            try
            {
                if (Math.Abs(task.State - data._lastSetParamState) >= UniObjData.ChangeCoef * Math.Abs(data._lastSetParamState) * (1 - ((TimeSpan)(_objSystem.DataTime - data._lastSetParamTime)).TotalSeconds / UniObjData.ChangeTimeout))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
                else if (float.IsNaN(data._lastSetParamState))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
            }
            catch (ArgumentOutOfRangeException)
            {
                if (!float.IsNaN(data._lastSetParamState))
                    _objSystem.SetObjParamState(data, _objSystem.DataTime);
            }
        }
        /// <summary>
		/// Установить диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		/// <param name="detectTime">время выявления</param>
		public override void ActivateObjDiagState(int state, DateTime detectTime)
		{
			base.ActivateObjDiagState(state, detectTime);
			
			// передаем значения всех изменившихся параметров
			UniObjData data;
			foreach (DlxObject objdata in _objData.Objects)
			{
				data = objdata as UniObjData;
				if (data != null && data.IsSigAnalog)
				{
					try
					{
						if (data.SigAnalog.Double != data._lastSetParamState)
							_objSystem.SetObjParamState(data, _objSystem.DataTime);
					}
					catch (ArgumentOutOfRangeException)
					{ 
						if( !float.IsNaN(data._lastSetParamState))
							_objSystem.SetObjParamState(data, _objSystem.DataTime);
					}
				}
			}
		}
		/// <summary>
		/// Снять диагностическое состояние объекта
		/// </summary>
		/// <param name="state">состояние</param>
		/// <param name="detectTime">время выявления</param>
		/// <param name="endTime">время окончания ситуации</param>
		public override void DeactivateObjDiagState(int state, DateTime detectTime, DateTime endTime)
		{
			base.DeactivateObjDiagState(state, detectTime, endTime);
			// передаем значения всех изменившихся параметров
			UniObjData data;
			foreach (DlxObject objdata in _objData.Objects)
			{
				data = objdata as UniObjData;
				if (data != null && data.IsSigAnalog)
				{
					try
					{
						if (data.SigAnalog.Double != data._lastSetParamState)
							_objSystem.SetObjParamState(data, _objSystem.DataTime);
					}
					catch (ArgumentOutOfRangeException)
					{
						if (!float.IsNaN(data._lastSetParamState ))
							_objSystem.SetObjParamState(data, _objSystem.DataTime);
					}
				}
			}
		}
		/// <summary>
		/// Задача формироования состояния
		/// </summary>
		protected ObjectState _objStateObject = null;
	}
	/// <summary>
	/// Унифицированный объект с диагностикой
	/// </summary>
	public class UniObjectUpDiag : UniObjectUpSt
	{
		/// <summary>
		/// Базовый класс задачи диагностирования
		/// </summary>
		public class ObjectTask : Tdm.ExTechTask
		{
			/// <summary>
			///  Инициализация
			/// </summary>
			/// <returns>истина, если удачно</returns>
			public override void Create()
			{
				if ((Owner != null) && (Owner is UniObject))
					_controlObj = Owner as UniObject;
				else
                    throw new Exception("Owner is not UniObject");

				base.Create();
			}

			/// <summary>
			/// обработка исходных данных - вызывается, если IsModify() == TRUE
			/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
			/// </summary>
			public virtual void UpdateObjectDiagState()
			{
				ResetModify();
			}

			/// <summary>
			/// проверка изменения исходных данных для состояния
			/// </summary>
			/// <returns>изменились ли исходные данные</returns>
			public bool IsModify()
			{
				return _bModify;
			}
			/// <summary>
			/// родительский объект контроля
			/// </summary>
			protected UniObject _controlObj;// родительский объект мониторинга

			/// <summary>
			/// Активировать ситуацию
			/// </summary>
			/// <param name="state">новое состояние</param>
			protected void ActivateObjDiagState(int state)
			{
				_controlObj.ActivateObjDiagState(state);
			}
			/// <summary>
			/// Деактивировать ситуацию
			/// </summary>
			/// <param name="state">новое состояние</param>
			protected void DeactivateObjDiagState(int state)
			{
				_controlObj.DeactivateObjDiagState(state);
			}
			/// <summary>
			/// Признак изменения исходны данных
			/// </summary>
			protected bool _bModify = true;
			/// <summary>
			/// Установить признак изменения исходных данных
			/// </summary>
			protected void SetModify()
			{
				_bModify = true;
			}
			/// <summary>
			/// Снять признак изменения исходных данных
			/// </summary>
			protected void ResetModify()
			{
				_bModify = false;
			}
			/// <summary>
			/// Добавление обработчика изменения данных
			/// </summary>
			/// <param name="signal"></param>
			public void AddWatchToSignal(IOSignal signal)
			{
				IOBaseElement sigOwner = signal.Owner as IOBaseElement;
				if (sigOwner != null)
					sigOwner.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
			}
			/// <summary>
			/// Добавление обработчика изменения данных
			/// </summary>
			/// <param name="obj">объект наблюдения</param>
			public void AddWatchToObject(BaseControlObj obj)
			{
				obj.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
			}
			/// <summary>
			/// Обработчик изменения данных
			/// </summary>
			/// <param name="elem">Модуль, данные которго изменились</param>
			/// <param name="time">Время изменения</param>
			/// <param name="force">Обработка внештатной ситуации (обыв связи)</param>
			protected virtual void OnChanged(IOBaseElement elem, DateTime time, bool force)
			{
				SetModify();
				//Debug.WriteLine("ObjectState " + _controlObj.Name + ": Обработчик изменения данных base.");
			}

			/// <summary>
			/// Получаем член данное у объекта контроля и подписываемся на рассылку его изменения
			/// </summary>
			/// <param name="objdataName">Имя член данного</param>
			/// <returns>true, если все успешно. false, если у объекта контроля нет такого член данного или оно не является сигналом или объектом</returns>
			protected bool AssignToData(string objdataName)
			{
				// Получаем и подписываемся на рассылку изменения для сигнала sigName
				ObjData objData = (_controlObj.GetObject(objdataName) as ObjData);
				if (objData == null)
				{
					Debug.WriteLine("ObjectTask: не найдено член данное [" + objdataName + "] у обънекта " + _controlObj.Name);
					return false;
				}
				if (objData.Object is ISignalUnion)
				{
					foreach (IOSignal sig in (objData.Object as ISignalUnion).IOSignals)
						AddWatchToSignal(sig);
				}
				else if (objData.IsControlObj)
					AddWatchToObject(objData.ControlObj);
				else
					return false;

				return true;
			}
			
			
			/// <summary>
            /// Получаем объект по имени член данного у объекта контроля и подписываемся на рассылку его изменения
			/// </summary>
			/// <param name="sigName">Имя член данного</param>
            /// <returns>Искомый объект по имени член данного, если все успешно. null, если у объекта контроля нет такого член данного или оно не является сигналом или объектом</returns>
			protected BaseControlObj AssignToRefObj(string objdataName)
			{
				// Вызываем получение объекта по член данному объекта контроля
				return AssignToRefObj(_controlObj, objdataName);
			}

            /// <summary>
            /// Получаем объект по имени член данного у объекта obj и подписываемся на рассылку его изменения
            /// </summary>
            /// <param name="obj">Объект, у которого получаем член данное</param>
            /// <param name="objdataName">Имя член данного</param>
            /// <returns>Искомый объект по имени член данного, если все успешно. null, если у объекта obj нет такого член данного или оно не является объектом</returns>
            protected BaseControlObj AssignToRefObj(UniObject obj, string objdataName)
            {
                // Получаем и подписываемся на рассылку изменения для сигнала sigName
                ObjData objData = (obj.GetObject(objdataName) as ObjData);
                if (objData == null)
                {
                    Debug.WriteLine("ObjectTask: не найдено член данное [" + objdataName + "] у обънекта " + obj.Name);
                    return null;
                }
                if (objData.IsControlObj)
                {
                    AddWatchToObject(objData.ControlObj);
                    return objData.ControlObj;
                }
                else
                    return null;
            }

			/// <summary>
			/// Получаем член данное у объекта контроля и подписываемся на рассылку его изменения
			/// </summary>
			/// <param name="objdataName">Имя член данного</param>
			/// <returns>true, если все успешно. false, если у объекта контроля нет такого член данного или оно не является сигналом или объектом</returns>
			protected ISigDiscreteLogic AssignToDiscretSignal(string objdataName)
			{
				// Получаем и подписываемся на рассылку изменения для сигнала sigName
				ObjData objData = (_controlObj.GetObject(objdataName) as ObjData);
				if (objData == null)
				{
					Debug.WriteLine("ObjectTask: не найдено член данное [" + objdataName + "] у обънекта " + _controlObj.Name);
					return null;
				}
				if (objData.Object is ISigDiscreteLogic)
				{
					foreach (IOSignal sig in (objData.Object as ISigDiscreteLogic).IOSignals)
						AddWatchToSignal(sig);

					return objData.Object as ISigDiscreteLogic;
				}
				
				return null;
			}

			/// <summary>
			/// Получаем член данное у объекта контроля и подписываемся на рассылку его изменения
			/// </summary>
			/// <param name="objdataName">Имя член данного</param>
			/// <returns>true, если все успешно. false, если у объекта контроля нет такого член данного или оно не является сигналом или объектом</returns>
			protected ISigAnalog AssignToAnalogSignal(string objdataName)
			{
				// Получаем и подписываемся на рассылку изменения для сигнала sigName
				ObjData objData = (_controlObj.GetObject(objdataName) as ObjData);
				if (objData == null)
				{
					Debug.WriteLine("ObjectTask: не найдено член данное [" + objdataName + "] у обънекта " + _controlObj.Name);
					return null;
				}
				if (objData.Object is ISigAnalog)
				{
					foreach (IOSignal sig in (objData.Object as ISigAnalog).IOSignals)
						AddWatchToSignal(sig);

					return objData.Object as ISigAnalog;
				}
				
				return null;
			}

			/// <summary>
			/// Получаем член данное у объекта контроля 
			/// </summary>
			/// <param name="objdataName">Имя член данного</param>
			/// <returns>true, если все успешно. false, если у объекта контроля нет такого член данного или оно не является сигналом или объектом</returns>
			protected BaseControlObj GetRefObj(string objdataName)
			{
				// Получаем и подписываемся на рассылку изменения для сигнала sigName
				ObjData objData = (_controlObj.GetObject(objdataName) as ObjData);
				if (objData == null)
				{
					Debug.WriteLine("ObjectTask: не найдено член данное [" + objdataName + "] у обънекта " + _controlObj.Name);
					return null;
				}
				if (objData.IsControlObj)
				{
					return objData.ControlObj;
				}
				else
					return null;
			}
			/// <summary>
			/// Добавить вызов обновления через milleseconds миллисекунд
			/// </summary>
			/// <param name="milleseconds">миллисекунды</param>
			protected void AddNotificationEvent(int milleseconds)
			{
				_controlObj._objSystem.AddNotificationEvent(milleseconds, OnChanged);
			}

		}

		/// <summary>
		/// Обновление состояния объекта
		/// </summary>
		public override void UpdateObjectDiagState()
		{
			base.UpdateObjectDiagState();

            foreach (DlxObject dlx in _techTasks.Objects)
            {
                ObjectTask task = dlx as ObjectTask;
                if (task != null && task.IsModify())
                    task.UpdateObjectDiagState();
            }
		}
	}

    /// <summary>
    /// Унифицированный объект с задачами выявления ТО
    /// </summary>
    public class UniObjectUpTO : UniObjectUpDiag
    {
        /// <summary>
        /// Базовая задача выявления фактов ТО
        /// </summary>
        public class TOTask : ObjectTask
        {
            /// <summary>
            /// Контролируемый бъект
            /// </summary>
            public UniObject ControlObj { get { return _controlObj; } }
            /// <summary>
            /// Перечень независимо выполняющихся подзадач
            /// </summary>
            public List<TOSubTask> subTasks = new List<TOSubTask>();

            /// <summary>
            /// Уведомление подзадач выявления об изменении входных данных
            /// </summary>
            public void UpdateSubtasks()
            {
                foreach (TOSubTask st in subTasks)
                {
                    st.Update();
                }
                ResetModify();
            }

            /// <summary>
            /// Объект-владелец задачи
            /// </summary>
            protected UniObjectUpTO ObjTO;

            public override void Create()
            {
                base.Create();
                
                ObjTO = _controlObj as UniObjectUpTO;

                bool res = ObjTO != null;
                foreach (TOSubTask st in subTasks)
                    res &= st.Create(ObjTO, this);

                LogUsedTask();
                if (res == false)
                    throw new Exception("Error to create sub tasks for " + Name);
            }

            protected void LogUsedTask()
            {
                string task = System.Type.GetTypeArray(new object[] { this }).First().Name;
                if (!usedTasks.Contains(task))
                {
                    usedTasks.Add(task);

                    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_usedTOTasks.txt", true);
                    sw.WriteLine(string.Format("{0}", task));
                    sw.Flush();
                    sw.Close();
                }
            }
            protected static List<string> usedTasks = new List<string>();


            /// <summary>
            /// Обработка изменения сигналов объекта
            /// </summary>
            public virtual void UpdateObjectSignals()
            {
                ResetModify();
            }

            #region Классы подзадач выявления ТО
            /// <summary>
            /// Базовая задача выявления фактов ТО
            /// </summary>
            public class TOSubTask
            {
                #region Data
                /// <summary>
                /// Родительская задача выявления ТО
                /// </summary>
                public TOTask ObjTask = null;

                /// <summary>
                /// ID пункта инструкции ТО в АСУ-Ш
                /// </summary>
                protected uint punkt_id = 0;
                /// <summary>
                /// Признак выявления ТО независимо от наличия работы в плане
                /// </summary>
                protected bool toIgnorePlan = false;
                /// <summary>
                /// Время начала выполнения ТО
                /// </summary>
                protected DateTime TO_BeginTime = DateTime.MinValue;
                /// <summary>
                /// Время завершения выполнения ТО
                /// </summary>
                protected DateTime TO_EndTime = DateTime.MinValue;
                /// <summary>
                /// Перечень диагностических ситуаций, связанных с работой по ТО
                /// </summary>
                protected Dictionary<UInt16, DateTime> TO_DiagList = new Dictionary<UInt16, DateTime>();
                /// <summary>
                /// Объект-владелец задачи
                /// </summary>
                protected UniObjectUpTO ObjTO;
                /// <summary>
                /// Набор условий задачи
                /// </summary>
                protected List<Condition> conds = new List<Condition>();
                /// <summary>
                /// Номер очередного условия
                /// </summary>
                protected int curCondID = 0;
                /// <summary>
                /// Время выполнения предыдущего условия
                /// </summary>
                public DateTime PrevConditionTime = DateTime.MinValue;
                /// <summary>
                /// Перечень ДС, которые могут быть результатом отслеживаемых задачей изменений сигналов
                /// </summary>
                public readonly List<int> RelatedDiagStates = new List<int>();
                /// <summary>
                /// Объект синхронизации для потокобезопасного выполнения задачи
                /// </summary>
                protected object locker = new object();

                public uint Punkt_ID
                {
                    get
                    {
                        return punkt_id;
                    }
                }
                #endregion

                public bool Create(UniObjectUpTO objTO, TOTask t)
                {
                    ObjTO = objTO;
                    ObjTask = t;
                    return ObjTO != null && ObjTask != null;
                }

                public void AddCondition(Condition c)
                {
                    if (c != null)
                        conds.Add(c);
                }

                #region Проверка выполнения условий
                /// <summary>
                /// Проверка выполнения текущего условия задачи
                /// </summary>
                /// <returns></returns>
                protected virtual bool ConditionsCompleted()
                {
                    return false;
                }
                /// <summary>
                /// Обработка успешного условия
                /// </summary>
                /// <param name="c">Условие</param>
                /// <param name="rawTime">Время подсистемы сигналов/объектов</param>
                /// <returns>Признак выполнения задачи</returns>
                protected bool ProcessSuccessfulCondition(Condition c, DateTime rawTime)
                {
                    bool res = false;
                    if (!c.HasSelfTimeout
                        || c.IsWaiting && c.EndWaiting(rawTime))
                    {
                        CheckRelatedDiagStates();
                        
                        if (c.IsFinal)
                        {   // выполнено заключительное условие, фиксируем ТО (возвращая curres==true), и переводим задачу в исходное состояние
                            FinishTask(rawTime);
                            res = true;
                        }
                        else
                        {   // активируем следующее условие
                            ContinueTask(c, rawTime);
                            // подменяя результат, т.к. условие - не последнее
                            res = false;
                        }
                        c.StopWaiting();
                    }
                    else if (!c.IsWaiting)
                    {
                        StartCheckTimeout(c, rawTime);
                        res = false;
                    }
                    return res;
                }
                /// <summary>
                /// Проверка наличия у объекта заданных ДС
                /// </summary>
                protected void CheckRelatedDiagStates()
                {
                    foreach (DiagState d in ObjTask.ControlObj.DiagStates.Where(ds => RelatedDiagStates.Contains(ds.Id)))
                    {
                        ushort id = (ushort)d.Id;
                        if (!TO_DiagList.ContainsKey(id))
                        {   // добавляются только ДС, отсутствующие в списке
                            TO_DiagList.Add(id, ObjTask.ControlObj._diagStateTOtimeMAPuni[id]);
                        }
                    }
                }
                /// <summary>
                /// Перевод задачи в исходное состояние
                /// </summary>
                protected void ResetTask(bool finished)
                {
                    curCondID = 0;
                    TO_BeginTime = DateTime.MinValue;
                    PrevConditionTime = DateTime.MinValue;
                    TO_DiagList.Clear();

                    LogExecution(finished ? "Completed" : "Interrupted");
                }
                /// <summary>
                /// Переход к следующему условию задачи
                /// </summary>
                /// <param name="c">Текущее условие</param>
                /// <param name="t">Время выполнения текущего условия</param>
                protected void ContinueTask(Condition c, DateTime t)
                {
                    // если условие - первое, фиксируем время начала возможного выполнения ТО
                    if (TO_BeginTime == DateTime.MinValue)
                        TO_BeginTime = t;

                    curCondID = c.NextOrderID;
                    PrevConditionTime = t;
                }
                /// <summary>
                /// Завершение выполнения задачи
                /// </summary>
                /// <param name="t"></param>
                protected void FinishTask(DateTime t)
                {
                    TO_EndTime = t;
                }
                /// <summary>
                /// Запуск проверки длительности выполнения условия
                /// </summary>
                /// <param name="c">Условие</param>
                /// <param name="t">Время первого выполнения</param>
                protected void StartCheckTimeout(Condition c, DateTime t)
                {
                    c.StartWaiting(t);
                    ThreadPool.QueueUserWorkItem(
                        delegate(object o)
                        {
                            //int delay = (int)(DateTime.Now - (DateTime)o).TotalMilliseconds;
                            //int rest = (int)(c.TimeoutSelf - delay);
                            //Thread.Sleep(rest > 0 ? rest : 0);
                            Thread.Sleep((int)c.TimeoutSelf);
                            Update();
                        }
                        , DateTime.Now);
                }
                #endregion

                #region Обработка изменений
                /// <summary>
                /// Метод имитации обновления данных объекта (для вызова с пула потоков по истечении таймаута)
                /// </summary>
                public virtual void Update()
                { }
                #endregion

                protected void LogExecution(string result)
                {
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_execTasks.txt", true);
                    sw.WriteLine(string.Format("{0}\t{1}\t{2}", DateTime.Now, this.ObjTask._controlObj.Name, result));
                    sw.Flush();
                    sw.Close();
                }
            }
            /// <summary>
            /// Подзадача выявления ТО по ДС
            /// </summary>
            public class TOSubTaskDiag : TOSubTask
            {
                public TOSubTaskDiag(uint punkt, bool ignorePlan)
                {
                    punkt_id = punkt;
                    toIgnorePlan = ignorePlan;
                }
                public TOSubTaskDiag(uint punkt)
                    : this(punkt, true) // выявление ТО по ДС возможно и при отсутствии работы в плане
                { }
                /// <summary>
                /// Проверка выполнения текущего условия задачи (к сбросу задачи в исходное состояние приводит только завершение суток)
                /// </summary>
                /// <returns></returns>
                protected override bool ConditionsCompleted()
                {
                    bool res = true;
                    DateTime rawTime = DateTime.MinValue;
                    List<Condition> cc = conds.Where(c => c.OrderID == curCondID && c.cType == ConditionType.Diag).ToList();

                    try
                    {
                        if (TO_BeginTime == DateTime.MinValue || TO_BeginTime.Date == DateTime.Today)
                        {   // если задача в исходном состоянии,
                            // либо (задача проверяется) и начало возможного выполнения ТО приходится на эти сутки
                            foreach (Condition cond in cc)
                            {
                                bool? b = cond.IsExecuted(this, ref rawTime);
                                res &= b.HasValue ? b.Value : false;
                            }

                            if (res)
                            {   // текущее условие выполнено - обрабатываем его результат
                                res = ProcessSuccessfulCondition(cc[0], rawTime);
                                Console.WriteLine("ТО!!! Условие выполнилось");
                            }
                        }
                        else
                        {   // иначе - переводим задачу в исходное состояние
                            ResetTask(false);
                            Console.WriteLine("ТО!!! Условие не выполнилось");
                        }
                    }
                    catch { res = false; }
                    return res;
                }

                public override void Update()
                {
                    UpdateObjectDiagState();
                }
                /// <summary>
                /// Обработка обновления перечня ДС
                /// </summary>
                protected void UpdateObjectDiagState()
                {
                    lock (locker)
                    {
                        // => изменения подсистемы ввода/объектов, произошедшие во время проверки текущего условия, либо контроля длительности,
                        // будут обработаны в порядке поступления
                        try
                        {
                            if (ConditionsCompleted())
                            {   // все условия задачи выполнены
                                Console.WriteLine("ТО!!! все условия задачи выполнены");
                                uint workID = ObjTO.GetWorkID(punkt_id);
                                Console.WriteLine("ТО!!! КОод работы workID, punkt_id - {0} -- {1}", workID, punkt_id);
                                if (workID > 0 || IsInWorkTime(TO_BeginTime, TO_EndTime))
                                {   // если в плане ТО есть работа по пункту инструкции, соотв. задаче, => фиксируем ТО
                                    TO_DiagList = new Dictionary<ushort, DateTime>();
                                    // в список добавляется только ДС, соответствующая последнему условию задачи
                                    TO_DiagList.Add((ushort)((DSCondition)conds.Last()).DiagStateID, TO_BeginTime);
                                    Console.WriteLine("ТО!!! Активация");
                                    ObjTask.ControlObj.ActivateObjWork(UnificationConcentrator.UniObjWork.Kind.Complete, workID, punkt_id, TO_BeginTime, TO_EndTime, TO_DiagList);
                                    Console.WriteLine("ТО!!! Активировано");
                                }
                                ResetTask(true);
                            }
                        }
                        catch
                        {
                            //Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
                        }
                    }

                    #region Old version
                    //List<BaseControlObj.DiagState> dstates = _controlObj.DiagStates.Where(d => d.Id == diagstate_id).ToList();
                    //if (dstates.Count > 0)
                    //{   // появилась ДС, ожидаемая задачей, => фиксируем время
                    //    _controlObj._diagStateTOtimeMAPuni.TryGetValue((int)diagstate_id, out TO_BeginTime);
                    //}
                    //else
                    //{   // ДС отсутствует
                    //    if (TO_BeginTime != DateTime.MinValue)
                    //    {   // до этого ДС была
                    //        if (TO_BeginTime.Date == DateTime.Today)
                    //        {   // и появилась ДС сегодня, => фиксируем работу по ТО
                    //            uint workID = ObjTO.GetWorkID(punkt_id);
                    //            if (workID > 0)
                    //            {   // если в плане ТО есть работа по пункту инструкции, соотв. задаче
                    //                TO_DiagList = new Dictionary<ushort, DateTime>();
                    //                TO_DiagList.Add((ushort)diagstate_id, TO_BeginTime);
                    //                _controlObj.ActivateObjWork(UnificationConcentrator.UniObjWork.Kind.Complete, workID, punkt_id, TO_BeginTime, _controlObj.ObjSys._DataTime, TO_DiagList);
                    //            }
                    //        }
                    //        // сбрасываем время начала работы
                    //        TO_BeginTime = DateTime.MinValue;
                    //    }
                    //}
                    #endregion
                }
                /// <summary>
                /// Признак выполнения ТО в рабочее время (при условии, что отсутствие работы в план-графике можно игнорировать)
                /// </summary>
                /// <param name="beg">Начало выполнения работ по ТО</param>
                /// <param name="end">Завершение выполнения работ по ТО</param>
                /// <returns>Признак выполнения работ по ТО в рабочее время</returns>
                protected bool IsInWorkTime(DateTime beg, DateTime end)
                {
                    return toIgnorePlan
                        && beg.Hour > WorkShift_Begin && beg.Hour < WorkShift_End
                        && end.Hour > WorkShift_Begin && end.Hour < WorkShift_End;
                }
                
                /// <summary>
                /// Начало рабочего времени
                /// </summary>
                public const int WorkShift_Begin = 8;
                /// <summary>
                /// Окончание рабочего времени
                /// </summary>
                public const int WorkShift_End = 20;

            }
            /// <summary>
            /// Подзадача выявления ТО по подзадачам контроля/измерения
            /// </summary>
            public class TOSubTaskControl : TOSubTask
            {
                public TOSubTaskControl(uint punkt, bool ignorePlan, List<int> relDiags)
                {
                    punkt_id = punkt;
                    toIgnorePlan = ignorePlan;
                    if (relDiags != null)
                        RelatedDiagStates.AddRange(relDiags);
                }
                public TOSubTaskControl(uint punkt, List<int> relDiags)
                    : this(punkt, false, relDiags)
                { }
                public TOSubTaskControl(uint punkt)
                    : this(punkt, false, null)
                { }

                /// <summary>
                /// Проверка выполнения текущего условия задачи
                /// </summary>
                /// <returns></returns>
                protected override bool ConditionsCompleted()
                {
                    bool res = true;
                    DateTime rawTime = DateTime.MinValue;
                    List<Condition> cc = conds.Where(c => c.OrderID == curCondID && (c.cType == ConditionType.Control || c.cType == ConditionType.Mixed)).ToList();

                    try
                    {
                        if (TO_BeginTime == DateTime.MinValue || TO_BeginTime.Date == DateTime.Today)
                        {   // если задача в исходном состоянии,
                            // либо (задача проверяется) и начало возможного выполнения ТО приходится на эти сутки
                            foreach (Condition cond in cc)
                            {
                                Console.WriteLine("ТО - проверка условия {0}", cond);
                                bool? b = cond.IsExecuted(this, ref rawTime);
                                res &= b.HasValue ? b.Value : false;
                            }
                        }
                        else
                        {
                            res = false;
                        }

                        if (res)
                        {   // текущее условие выполнено - обрабатываем его результат
                            res = ProcessSuccessfulCondition(cc[0], rawTime);
                            Console.WriteLine("ТО - текущее условие выполнено - обрабатываем его результат");
                        }
                        else
                        {   // иначе - переводим задачу в исходное состояние
                            ResetTask(false);
                            Console.WriteLine("ТО - условие не выполнилось!!!");
                        }
                    }
                    catch { res = false; }
                    return res;
                }
                /// <summary>
                /// Метод имитации обновления данных объекта (для вызова с пула потоков по истечении таймаута)
                /// </summary>
                public override void Update()
                {
                    UpdateObjectSignals();
                }
                /// <summary>
                /// Обработка изменения сигналов объекта - вызывается, если IsModify() == TRUE
                /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
                /// </summary>
                protected void UpdateObjectSignals()
                {
                    lock (locker)
                    {
                        try
                        {
                            if (ConditionsCompleted())
                            {   // все условия задачи выполнены
                                Console.WriteLine("ТО !!!! Выполнилось");
                                uint workID = ObjTO.GetWorkID(punkt_id);
                                Console.WriteLine("ТО !!!! Выполнилось work id {0}, punkt {1} ", workID, punkt_id);
                                if (workID > 0)
                                {   // если в плане ТО есть работа по пункту инструкции, соотв. задаче, => фиксируем ТО
                                    Console.WriteLine("ТО - Фиксируем");
                                    ObjTask.ControlObj.ActivateObjWork(UnificationConcentrator.UniObjWork.Kind.Complete, workID, punkt_id, TO_BeginTime, TO_EndTime, TO_DiagList);
                                }
                                ResetTask(true);
                            }
                        }
                        catch
                        {
                            //Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
                        }
                    }
                }
            }

            /// <summary>
            /// Подзадача выявления ТО по состоянию
            /// </summary>
            public class TOSubTaskState : TOSubTask
            {
                public TOSubTaskState(uint punkt, bool ignorePlan)
                {
                    punkt_id = punkt;
                    toIgnorePlan = ignorePlan;
                }
                public TOSubTaskState(uint punkt)
                    : this(punkt, true) // выявление ТО по ДС возможно и при отсутствии работы в плане
                { }
                /// <summary>
                /// Проверка выполнения текущего условия задачи (к сбросу задачи в исходное состояние приводит только завершение суток)
                /// </summary>
                /// <returns></returns>
                protected override bool ConditionsCompleted()
                {
                    bool res = true;
                    DateTime rawTime = DateTime.MinValue;
                    List<Condition> cc = conds.Where(c => c.OrderID == curCondID && c.cType == ConditionType.State).ToList();

                    try
                    {
                        if (TO_BeginTime == DateTime.MinValue || TO_BeginTime.Date == DateTime.Today)
                        {   // если задача в исходном состоянии,
                            // либо (задача проверяется) и начало возможного выполнения ТО приходится на эти сутки
                            foreach (Condition cond in cc)
                            {
                                bool? b = cond.IsExecuted(this, ref rawTime);
                                res &= b.HasValue ? b.Value : false;
                            }

                            if (res)
                            {   // текущее условие выполнено - обрабатываем его результат
                                res = ProcessSuccessfulCondition(cc[0], rawTime);
                                Console.WriteLine("ТО - Условие выполнилось");
                            }
                        }
                        else
                        {   // иначе - переводим задачу в исходное состояние
                            ResetTask(false);
                            Console.WriteLine("ТО - Условие не выполнилось");
                        }
                    }
                    catch { res = false; }
                    return res;
                }

                public override void Update()
                {
                    UpdateObjectState();
                }
                /// <summary>
                /// Обработка обновления перечня ДС
                /// </summary>
                protected void UpdateObjectState()
                {
                    lock (locker)
                    {
                        // => изменения подсистемы ввода/объектов, произошедшие во время проверки текущего условия, либо контроля длительности,
                        // будут обработаны в порядке поступления
                        try
                        {
                            if (ConditionsCompleted())
                            {   // все условия задачи выполнены
                                Console.WriteLine("ТО Выполнилось");
                                uint workID = ObjTO.GetWorkID(punkt_id);
                                Console.WriteLine("ТО!!! КОод работы workID, punkt_id - {0} -- {1}", workID, punkt_id);
                                if (workID > 0 || IsInWorkTime(TO_BeginTime, TO_EndTime))
                                {
                                    Console.WriteLine("ТО Выполнилось - Фиксируем");
                                    ObjTask.ControlObj.ActivateObjWork(UnificationConcentrator.UniObjWork.Kind.Complete, workID, punkt_id, TO_BeginTime, TO_EndTime, TO_DiagList);
                                }
                                ResetTask(true);
                            }
                        }
                        catch
                        {
                            //Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
                        }
                    }
                }
                /// <summary>
                /// Признак выполнения ТО в рабочее время (при условии, что отсутствие работы в план-графике можно игнорировать)
                /// </summary>
                /// <param name="beg">Начало выполнения работ по ТО</param>
                /// <param name="end">Завершение выполнения работ по ТО</param>
                /// <returns>Признак выполнения работ по ТО в рабочее время</returns>
                protected bool IsInWorkTime(DateTime beg, DateTime end)
                {
                    return toIgnorePlan
                        && beg.Hour > WorkShift_Begin && beg.Hour < WorkShift_End
                        && end.Hour > WorkShift_Begin && end.Hour < WorkShift_End;
                }

                /// <summary>
                /// Начало рабочего времени
                /// </summary>
                public const int WorkShift_Begin = 8;
                /// <summary>
                /// Окончание рабочего времени
                /// </summary>
                public const int WorkShift_End = 20;

            }
            #endregion

            #region Классы условий
            /// <summary>
            /// Тип условия
            /// </summary>
            public enum ConditionType
            {
                /// <summary>
                /// Условие на значение подзадачи контроля/измерения
                /// </summary>
                Control,
                /// <summary>
                /// Условие на наличие ДС
                /// </summary>
                Diag,
                /// <summary>
                /// Смешанное условие
                /// </summary>
                Mixed,
                /// <summary>
                /// Условие на состояние объекта
                /// </summary>
                State
            }
            
            /// <summary>
            /// Условие выполнения задачи по выявлению ТО
            /// </summary>
            public class Condition
            {
                /// <summary>
                /// Условие выполнения задачи по выявлению ТО
                /// </summary>
                /// <param name="orderID">Порядковый номер условия</param>
                /// <param name="nextOrderID">Порядковый номер следующего условия</param>
                /// <param name="timeoutMin">Минимально необходимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutMax">Максимально допустимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutSelf">Необходимая длительность выполнения данного условия, с</param>
                public Condition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf)
                {
                    OrderID = orderID;
                    NextOrderID = nextOrderID;
                    TimeoutMin = Math.Abs(timeoutMin);
                    TimeoutMax = Math.Abs(timeoutMax);
                    TimeoutSelf = Math.Abs(timeoutSelf);
                    cType = ConditionType.Mixed;
                }

                /// <summary>
                /// Выполняется ли условие для данной задачи?
                /// </summary>
                /// <param name="task">Задача выявления ТО</param>
                /// <param name="exTime">Время выполнения условия</param>
                /// <returns>Признак выполнения условия</returns>
                public virtual bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {
                    return false;
                }
                /// <summary>
                /// Признак соблюдения таймаута
                /// </summary>
                /// <param name="span">Период времени, с</param>
                /// <returns>Соблюден ли таймаут?</returns>
                protected virtual bool IsTimeoutKeeped(double span)
                {
                    return (TimeoutMin == 0 || span > TimeoutMin)
                        && (TimeoutMax == 0 || span < TimeoutMax);
                }
                /// <summary>
                /// Является ли условие последним в цепочке?
                /// </summary>
                /// <returns>Признак последнего условия</returns>
                public bool IsFinal
                {
                    get { return NextOrderID == 0; }
                }

                #region Data
                /// <summary>
                /// Порядковый номер условия
                /// </summary>
                public readonly int OrderID = 0;
                /// <summary>
                /// Минимально необходимый таймаут от момента выполнения предыдущего условия, мс
                /// </summary>
                public readonly float TimeoutMin = 0;
                /// <summary>
                /// Максимально допустимый таймаут от момента выполнения предыдущего условия, мс
                /// </summary>
                public readonly float TimeoutMax = 0;
                /// <summary>
                /// Необходимая длительность выполнения данного условия, мс
                /// </summary>
                public readonly float TimeoutSelf = 0;
                /// <summary>
                /// Порядковый номер следующего условия
                /// </summary>
                public readonly int NextOrderID = 0;
                /// <summary>
                /// Проверка длительности начата
                /// </summary>
                public bool IsWaiting
                {
                    get { return _isWaiting; }
                }
                protected bool _isWaiting = false;
                /// <summary>
                /// Время начала проверки длительности
                /// </summary>
                protected DateTime StartWaitTime = DateTime.MinValue;
                /// <summary>
                /// Время начала проверки длительности (локальное)
                /// </summary>
                protected DateTime StartWaitLocalTime = DateTime.MinValue;
                /// <summary>
                /// Тип условия
                /// </summary>
                public ConditionType cType { get; protected set; }
                #endregion

                /// <summary>
                /// Условие должно выполняться в течение некоторого периода времени
                /// </summary>
                public bool HasSelfTimeout
                {
                    get { return TimeoutSelf != 0; }
                }
                /// <summary>
                /// Начало проверки длительности
                /// </summary>
                /// <param name="t"></param>
                public void StartWaiting(DateTime t)
                {
                    _isWaiting = true;
                    StartWaitLocalTime = DateTime.Now;
                    StartWaitTime = t;
                }
                /// <summary>
                /// Завершение проверки длительности
                /// </summary>
                /// <param name="t"></param>
                public void StopWaiting()
                {
                    _isWaiting = false;
                    StartWaitTime = DateTime.MinValue;
                }
                /// <summary>
                /// Проверка длительности выполнена
                /// </summary>
                /// <param name="t">Время подсистемы</param>
                /// <returns>Признак завершения проверки</returns>
                public bool EndWaiting(DateTime t)
                {
                    // проверка выполнена, если:
                    // - произошло изменение подсистемы сигналов/объектов, и его время отличается от стартового на заданный интервал
                    // - истек заданный интервал времени
                    return (t - StartWaitTime).TotalMilliseconds >= TimeoutSelf
                        || (DateTime.Now - StartWaitLocalTime).TotalMilliseconds >= TimeoutSelf;
                }

                protected void LogExecution(TOTask t, Condition c)
                {
                    string task = System.Type.GetTypeArray(new object[] { this }).First().Name;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_execTasks.txt", true);
                    sw.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}", DateTime.Now, t._controlObj.Name, task, c.OrderID));
                    sw.Flush();
                    sw.Close();
                }


            }
            /// <summary>
            /// Условие - появление определенной ДС
            /// </summary>
            public class DSCondition : Condition
            {
                /// <summary>
                /// Условие на ДС
                /// </summary>
                /// <param name="orderID">Порядковый номер условия</param>
                /// <param name="nextOrderID">Порядковый номер следующего условия</param>
                /// <param name="timeoutMin">Минимально необходимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutMax">Максимально допустимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutSelf">Необходимая длительность выполнения данного условия, с</param>
                /// <param name="dsID">ID ДС</param>
                /// <param name="isPresent">Признак наличия/отсутствия ДС</param>
                public DSCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, int dsID, bool isPresent)
                    : base(orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf)
                {
                    DiagStateID = dsID;
                    IsPresent = isPresent;
                    cType = ConditionType.Diag;
                }
                public DSCondition(int orderID, int nextOrderID, int dsID, bool isPresent)
                    : this(orderID, nextOrderID, 0, 0, 0, dsID, isPresent)
                { }


                /// <summary>
                /// Выполняется ли условие для данной задачи?
                /// </summary>
                /// <param name="task">Задача выявления ТО</param>
                /// <param name="exTime">Время выполнения условия</param>
                /// <returns>Признак выполнения условия</returns>
                public override bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {

                    bool res = false;
                    DiagStateDetectTask dst = task.ObjTask as DiagStateDetectTask;
                    if (dst != null)
                    {
                        if (IsTimeoutKeeped((dst.ControlObj.ObjSys.DataTime - task.PrevConditionTime).TotalMilliseconds))
                        {   // если соблюдены таймауты, => проверим выполнение условия на ДС
                            if (IsPresent)
                                dst.ControlObj._diagStateTOtimeMAPuni.TryGetValue(DiagStateID, out exTime);
                            else
                                exTime = dst.ControlObj.ObjSys.DataTime;

                            List<BaseControlObj.DiagState> dstates = dst.ControlObj.DiagStates.Where(d => d.Id == DiagStateID).ToList();
                            res = IsPresent && dstates.Count > 0 || !IsPresent && dstates.Count == 0;

                            if (res)
                                LogExecution(dst, this);
                        }
                    }
                    return res;
                }
                /// <summary>
                /// ID ДС
                /// </summary>
                public readonly int DiagStateID = 0;
                /// <summary>
                /// Признак необходимости наличия/отсутствия ДС
                /// </summary>
                public readonly bool IsPresent = true;
            }
            /// <summary>
            /// Условие - состояние задачи контроля
            /// </summary>
            public class CTCondition : Condition
            {
                /// <summary>
                /// Условие на задачу контроля
                /// </summary>
                /// <param name="orderID">Порядковый номер условия</param>
                /// <param name="nextOrderID">Порядковый номер следующего условия</param>
                /// <param name="timeoutMin">Минимально необходимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutMax">Максимально допустимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutSelf">Необходимая длительность выполнения данного условия, с</param>
                /// <param name="taskName">Название задачи</param>
                /// <param name="taskState">Состояние задачи</param>
                public CTCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, string taskName, bool taskState, bool taskReq)
                    : base(orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf)
                {
                    TaskName = taskName;
                    TaskState = taskState;
                    TaskRequired = taskReq;
                    cType = ConditionType.Control;
                }
                public CTCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, string taskName, bool taskState)
                    : this (orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf, taskName, taskState, true)
                { }
                public CTCondition(int orderID, int nextOrderID, string taskName, bool taskState, bool taskReq)
                    : this(orderID, nextOrderID, 0, 0, 0, taskName, taskState, taskReq)
                { }
                public CTCondition(int orderID, int nextOrderID, string taskName, bool taskState)
                    : this(orderID, nextOrderID, 0, 0, 0, taskName, taskState)
                { }
                public CTCondition(string taskName, bool taskState)
                    : this(0, 0, 0, 0, 0, taskName, taskState)
                { }
                /// <summary>
                /// Выполняется ли условие для данной задачи?
                /// </summary>
                /// <param name="task">Задача выявления ТО</param>
                /// <param name="exTime">Время выполнения условия</param>
                /// <returns>Признак выполнения условия</returns>
                public override bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {
                    bool res = false;
                    ControlSigDetectTask cst = task.ObjTask as ControlSigDetectTask;
                    if (cst != null)
                    {
                        if (IsTimeoutKeeped((cst.ControlObj.IOSystem.DataTime - task.PrevConditionTime).TotalMilliseconds))
                        {   // если соблюдены таймауты, => проверяем условие на сигнал
                            ISigDiscreteLogic s = cst.GetControlTask(TaskName);
                            if (s != null)
                            {
                                exTime = cst.ControlObj.IOSystem.DataTime;
                                res = CheckSignalState(s) && s.On == TaskState;

                                if (res)
                                    LogExecution(cst, this);
                            }
                            else
                                return null;
                        }
                    }
                    return res;
                }
                protected void LogExecution(TOTask t, CTCondition c)
                {
                    string task = System.Type.GetTypeArray(new object[] { this }).First().Name;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_execTasks.txt", true);
                    sw.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}", DateTime.Now, t._controlObj.Name, task, c.OrderID, c.TaskName));
                    sw.Flush();
                    sw.Close();
                }

                protected bool CheckSignalState(ISigDiscreteLogic d)
                {
                    UnaryDiscreteLogic udl = d as UnaryDiscreteLogic;
                    if (udl != null && udl.IOSignals.Length > 0 && udl.IOSignals[0].GetCurrent >= 0 && udl.IOSignals[0].GetCurrent <= 2)
                        return true;

                    BinaryDiscreteLogic bdl = d as BinaryDiscreteLogic;
                    if (bdl != null && bdl.IOSignals.Length > 0 && bdl.IOSignals[0].GetCurrent >= 0 && bdl.IOSignals[0].GetCurrent <= 2)
                        return true;
                                        
                    return false;
                }
                /// <summary>
                /// Название задачи
                /// </summary>
                public readonly string TaskName = string.Empty;
                /// <summary>
                /// Состояние задачи
                /// </summary>
                public readonly bool TaskState = true;
                /// <summary>
                /// Признак обязательности задачи
                /// </summary>
                public readonly bool TaskRequired = true;
            }
            /// <summary>
            /// Условие - состояние задачи измерения
            /// </summary>
            public class MTCondition : Condition
            {
                /// <summary>
                /// Условие на задачу измерения
                /// </summary>
                /// <param name="orderID">Порядковый номер условия</param>
                /// <param name="nextOrderID">Порядковый номер следующего условия</param>
                /// <param name="timeoutMin">Минимально необходимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutMax">Максимально допустимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutSelf">Необходимая длительность выполнения данного условия, с</param>
                /// <param name="taskName">Название задачи</param>
                /// <param name="taskValue">Значение сигнала задачи</param>
                /// <param name="taskOp">Операция над значением сигнала задачи</param>
                public MTCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, string taskName, float taskValue, Operation taskOp)
                    : base(orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf)
                {
                    TaskName = taskName;
                    TaskValue = taskValue;
                    TaskOp = taskOp;
                    cType = ConditionType.Control;
                }

                /// <summary>
                /// Выполняется ли условие для данной задачи?
                /// </summary>
                /// <param name="task">Задача выявления ТО</param>
                /// <param name="exTime">Время выполнения условия</param>
                /// <returns>Признак выполнения условия</returns>
                public override bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {
                    bool res = false;
                    ControlSigDetectTask cst = task.ObjTask as ControlSigDetectTask;
                    if (cst != null)
                    {
                        if (IsTimeoutKeeped((cst.ControlObj.IOSystem.DataTime - task.PrevConditionTime).TotalMilliseconds))
                        {   // если соблюдены таймауты, => проверяем условие на сигнал
                            ISigAnalog s = cst.GetMeasureTask(TaskName);
                            if (s != null)
                            {
                                exTime = cst.ControlObj.IOSystem.DataTime;
                                res = IsComplete(s.IOSignals[0].GetCurrent);

                                if (res)
                                    LogExecution(cst, this);
                            }
                            else
                                return null;
                        }
                    }
                    return res;
                }
                /// <summary>
                /// Выполнено ли условие
                /// </summary>
                /// <param name="val">Состояние задачи измерения</param>
                public bool IsComplete(float val)
                {
                    switch (TaskOp)
                    {
                        case Operation.Equal:
                            return val == TaskValue;
                        case Operation.Less:
                            return val < TaskValue;
                        case Operation.More:
                            return val > TaskValue;
                        default:
                            return false;
                    }
                }

                #region Data
                /// <summary>
                /// Название задачи
                /// </summary>
                public readonly string TaskName = string.Empty;
                /// <summary>
                /// Значение сигнала задачи
                /// </summary>
                public readonly float TaskValue = 0;
                /// <summary>
                /// Операция над значением сигнала задачи
                /// </summary>
                public readonly Operation TaskOp = Operation.Equal;
                #endregion

                /// <summary>
                /// Набор операций над значением сигнала задачи
                /// </summary>
                public enum Operation
                {
                    Equal,
                    More,
                    Less
                }

            }
            /// <summary>
            /// Составное условие
            /// </summary>
            public class MultiCondition : Condition
            {
                /// <summary>
                /// Составное условие
                /// </summary>
                /// <param name="orderID">Порядковый номер условия</param>
                /// <param name="nextOrderID">Порядковый номер следующего условия</param>
                /// <param name="timeoutMin">Минимально необходимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutMax">Максимально допустимый таймаут от момента выполнения предыдущего условия, с</param>
                /// <param name="timeoutSelf">Необходимая длительность выполнения данного условия, с</param>
                /// <param name="dsID">ID ДС</param>
                /// <param name="isPresent">Признак наличия/отсутствия ДС</param>
                public MultiCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, List<Condition> conds, Logic l)
                    : base(orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf)
                {
                    if (conds != null)
                        conditions.AddRange(conds);
                    logic = l;
                    cType = ConditionType.Mixed;
                }
                public MultiCondition(int orderID, int nextOrderID, List<Condition> conds, Logic l)
                    : this(orderID, nextOrderID, 0, 0, 0, conds, l)
                { }
                /// <summary>
                /// Выполняется ли составное условие для данной задачи?
                /// </summary>
                /// <param name="task">Задача выявления ТО</param>
                /// <param name="exTime">Время выполнения условия</param>
                /// <returns>Признак выполнения условия</returns>
                public override bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {
                    foreach (Condition c in conditions)
                    {
                        bool? b = c.IsExecuted(task, ref exTime);
                        switch (logic)
                        {
                            case Logic.And:
                                if (!b.HasValue)
                                    return true;
                                else if (!b.Value)
                                    return false;
                                break;
                            case Logic.Or:
                                if (!b.HasValue)
                                    return false;
                                else if (b.Value)
                                    return true;
                                break;
                            default:
                                break;
                        }
                    }
                    return logic == Logic.And ? true : false;
                }

                #region Data
                /// <summary>
                /// Перечень вложенных условий
                /// </summary>
                protected List<Condition> conditions = new List<Condition>();
                /// <summary>
                /// Логика обработки вложенных условий
                /// </summary>
                protected Logic logic = Logic.And;
                #endregion

                /// <summary>
                /// Логическая операция над условиями
                /// </summary>
                public enum Logic
                {
                    And,
                    Or
                }

            }

            /// <summary>
            /// Условие по событию
            /// </summary>
            public class SCondition : Condition
            {
                public SCondition(int orderID, int nextOrderID, float timeoutMin, float timeoutMax, float timeoutSelf, int stateID, bool isPresent)
                    : base(orderID, nextOrderID, timeoutMin, timeoutMax, timeoutSelf)
                {
                    StateID = stateID;
                    IsPresent = isPresent;
                    cType = ConditionType.State;
                }
                public SCondition(int orderID, int nextOrderID, int stateID, bool isPresent)
                    : this(orderID, nextOrderID, 0, 0, 0, stateID, isPresent)
                { }
                public override bool? IsExecuted(TOSubTask task, ref DateTime exTime)
                {
                    bool res = false;
                    StateDetectTask cst = task.ObjTask as StateDetectTask;
                    if (cst != null)
                    {
                        if (IsTimeoutKeeped((cst.ControlObj.IOSystem.DataTime - task.PrevConditionTime).TotalMilliseconds))
                        {   // если соблюдены таймауты, => проверяем условие на сигнал
                            if (cst.ControlObj.ObjState.Current == StateID)
                            {
                                exTime = cst.ControlObj.IOSystem.DataTime;
                                return true;
                            }
                        }
                    }
                    return res;
                }
                protected void LogExecution(TOTask t, CTCondition c)
                {
                    string task = System.Type.GetTypeArray(new object[] { this }).First().Name;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_execTasks.txt", true);
                    sw.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}", DateTime.Now, t._controlObj.Name, task, c.OrderID, c.TaskName));
                    sw.Flush();
                    sw.Close();
                }

                protected bool CheckSignalState(ISigDiscreteLogic d)
                {
                    UnaryDiscreteLogic udl = d as UnaryDiscreteLogic;
                    if (udl != null && udl.IOSignals.Length > 0 && udl.IOSignals[0].GetCurrent >= 0 && udl.IOSignals[0].GetCurrent <= 2)
                        return true;

                    BinaryDiscreteLogic bdl = d as BinaryDiscreteLogic;
                    if (bdl != null && bdl.IOSignals.Length > 0 && bdl.IOSignals[0].GetCurrent >= 0 && bdl.IOSignals[0].GetCurrent <= 2)
                        return true;

                    return false;
                }
                /// <summary>
                /// ID ДС
                /// </summary>
                public readonly int StateID = 0;
                /// <summary>
                /// Признак необходимости наличия/отсутствия состояния
                /// </summary>
                public readonly bool IsPresent = true;
            }
            #endregion


        }

        /// <summary>
        /// Хранилище плана работ по ТО
        /// </summary>
        protected TOPlanProvider planProvider;

        /// <summary>
        /// Путь по умолчанию 
        /// </summary>
        protected readonly string defPlanProviderPath = "/App/Common/TOPlanProvider";

        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            string path = string.Empty;
            if (Loader.GetAttribute("TOPlanProvider", out path))
                planProvider = Loader.GetObject(path) as TOPlanProvider;
            else
                planProvider = Loader.GetObject(defPlanProviderPath) as TOPlanProvider;
        }
        
        public override void Create()
        {
            base.Create();
            if(planProvider == null)
                throw new ArgumentException(Name + ": planProvider is empty");
        }

        public override void UpdateObjectState()
        {
            base.UpdateObjectState();

            foreach (DlxObject dlx in _techTasks.Objects)
            {
                TOTask task = dlx as TOTask;
                if (task != null /*&& task.IsModify()*/) // /*
                    if (task is ControlSigDetectTask)
                        task.UpdateObjectSignals();
                    else
                        task.UpdateObjectDiagState();
            }
        }

        /// <summary>
        /// Получить идентификатор работы из плана ТО
        /// </summary>
        /// <returns></returns>
        public uint GetWorkID(uint punkt_id)
        {
            if (planProvider == null)
                return 0;
            
            PlanWorkTO plan = planProvider.GetPlanWorks(Site.ID, Number).Where(w => w.punkt_id == punkt_id).FirstOrDefault();
            return plan == null ? 0 : (uint)plan.wk_id;
        }

    }

	/// <summary>
	/// Класс параметра унифицированного объекта
	/// </summary>
	public class UniObjData : ObjData
	{
        /// <summary>
        /// Конструктор для содания параметра в авто-подсистеме объектов
        /// </summary>
        /// <param name="objID">ID Объекта</param>
        /// <param name="paramID">ID параметра</param>
        /// <param name="isNumeric">Признак числового параметра</param>
        public UniObjData(uint objID, uint paramID, bool isNumeric)
        {
            _objID = objID;
            _paramID = paramID;
            _isDouble = isNumeric;
        }
        public UniObjData(uint paramID, bool isNumeric)
            : this(0, paramID, isNumeric)
        { }
        /// <summary>
        /// Конструктор для содания параметра в обычной подсистеме объектов при объектной загрузке
        /// </summary>
        public UniObjData()
            : this(0, 0, true)
        { }

		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
        override public void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            _paramID = Loader.GetAttributeUInt("ParamID", 0xffffffff);
            _diagParamsPath = Loader.GetAttributeString("NormalPath", string.Empty);
            _diagParamsPathExt = Loader.GetAttributeString("NormalPathExt", string.Empty);
            _paramType = Loader.GetAttributeUInt("ParamType", 0);
            _codeSign = Loader.GetAttributeString("CODE", string.Empty);
            string s = string.Empty;
            if (Loader.GetAttributeValue("Operation", ref s))
            {
                _isComputed = true;
            }

            _isRizolation = _diagParamsPath.Contains(RizolDiagParamsPath);
            _isIMSI = _diagParamsPath.Contains("IMSI") || _diagParamsPath.Contains("ISI");
        }
		/// <summary>
		/// Инициализация - получаем нормали
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
            base.Create();

			if (Owner as UniObject != null && IsIOSignal)
			{
				UniObject obj = Owner as UniObject;
				if (_diagParamsPath.Length != 0)
				{
					_normalMax = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, _diagParamsPath + "/" + "Max");
					_normalMin = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, _diagParamsPath + "/" + "Min");
					_normalExt = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, _diagParamsPath + "/" + "Ext");
				}
				else
				{
					_normalMax = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, obj.RelativeName + "/" + Name + "/" + "Max");
					_normalMin = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, obj.RelativeName + "/" + Name + "/" + "Min");
					_normalExt = obj._objSystem.GetDiagParamValue(obj.Number, _paramID, obj.RelativeName + "/" + Name + "/" + "Ext");
				}
			}
		}
        /// <summary>
		/// Установить значение параметра (числовое)
		/// </summary>
		/// <param name="state">значение</param>
		/// <returns>удалось ли установить это значение</returns>
        public virtual bool SetParamState(double state)
        {
            return SetParamState(Convert.ToSingle(state));
        }
		/// <summary>
		/// Установить значение параметра (числовое)
		/// </summary>
		/// <param name="state">значение</param>
		/// <returns>удалось ли установить это значение</returns>
		public virtual bool SetParamState(float state)
		{
			if (IsIOSignal)
			{
				if (IsSigAnalog)
				{
					IOSignal.StateBitCount = 32;
					((IOModule)IOSignal.Owner).GeneralState.Current = IOGeneralState.Normal;
				}
                
                if (state == float.MaxValue || float.IsNaN(state) || float.IsInfinity(state) || state > UInt32.MaxValue)
                    IOSignal.State = 0xFFFFFFFF;
                else
                {
                   /* if (IOSignal is IOAnalogSignal)
                    {
                        if (state < UInt32.MaxValue / 1000)
                        {
                            (IOSignal as IOAnalogSignal).IsDirectDouble = false;
                            IOSignal.State = (uint)(state * 1000 + 0.5);
                        }
                        else
                        {
                            (IOSignal as IOAnalogSignal).IsDirectDouble = true;
                            IOSignal.State = (uint)state;
                        }
                    }*/
                    if (IOSignal is IOAnalogSignal)
                        IOSignal.State = (uint)(state * 1000 + 0.5);
                    else if (IOSignal is IOAnalogFloatSignal)
                        IOSignal.State = (new UniFloatUint32Union { _Float = state })._Uint;
                }

                ((IOModule)IOSignal.Owner).RaiseDataChangedEvent(true);
				return true;
			}
			//else if (IsDouble)
            else if (IsNumeric)
			{
                Double par = state;
                _Object = par;
				return true;
			}

			return false;
		}
		/// <summary>
		/// Установить значение параметра (нечисловое)
		/// </summary>
		/// <param name="state">значение</param>
		/// <returns>удалось ли установить это значение</returns>
		public virtual bool SetParamState(byte[] state)
		{
			if (IsString)
			{
				String par = _Object as String;
				par.Remove(0, par.Length);
				par.Insert(0, System.Text.Encoding.UTF8.GetString(state));
				return true;
			}

			return false;
		}

        /// <summary>
        /// Идентификатор параметра в рамках объекта
        /// </summary>
        public UInt32 ObjectID
        {
            get { return _objID; }
        }
        /// <summary>
		/// Идентификатор параметра в рамках объекта
		/// </summary>
		public UInt32 ParamID
		{
            get { return _paramID; }
		}
		/// <summary>
		/// Идентификатор типа
		/// </summary>
		public UInt32 ParamType
		{
			get { return _paramType; }
		}
		/// <summary>
		/// Проверка, что это числовой параметр
		/// </summary>
		public virtual bool IsNumeric
		{
            get { return _isDouble; }
		}
        /*/// <summary>
        /// Возвращает true если объект содержит данные типа Double
        /// </summary>
        public override bool IsDouble
        {
            get { return _isDouble; }
        }*/
        /// <summary>
        /// Возвращает true если объект содержит данные типа Double
        /// </summary>
        public bool IsComputed
        {
            get { return _isComputed; }
        }
        /// <summary>
		/// Краткое описание параметра
		/// </summary>
		public virtual string ShortDescription
		{
			get
			{
                if (IsIOSignal && IOSignal.Comment != null)
					return IOSignal.Comment.Code;
				else
					return Name;
			}
		}
		/// <summary>
		/// Описание параметра
		/// </summary>
		public virtual string Description
		{
			get
			{
                if (IsIOSignal && IOSignal.Comment != null)
					return IOSignal.Comment.Text;
				else
					return Name;
			}
		}

        /// <summary>
        /// идентификатор объекта в рамках станции
        /// </summary>
        protected UInt32 _objID;
        /// <summary>
		/// идентификатор параметра в рамках станции
		/// </summary>
		protected UInt32 _paramID;
		/// <summary>
		/// Идентификатор типа
		/// </summary>
		protected UInt32 _paramType;
		/// <summary>
		/// Максимальная нормаль на значение
		/// </summary>
		public float _normalMax = float.MaxValue;
		/// <summary>
		/// Минимальная нормаль на значение
		/// </summary>
		public float _normalMin = float.MaxValue;
		/// <summary>
		/// Дополнительная нормаль
		/// </summary>
		public float _normalExt = float.MaxValue;
		/// <summary>
		/// Путь к параметрам диагностирования
		/// </summary>
		public string _diagParamsPath = string.Empty;
		/// <summary>
		/// Путь к дополнительным параметрам диагностирования
		/// </summary>
		public string _diagParamsPathExt = string.Empty;
        /// <summary>
        /// Признак измерения сопротивления изоляции (данные из АДК передаются в кОм)
        /// </summary>
        public bool _isRizolation = false;
        /// <summary>
        /// Признак измерения сопротивления изоляции модулями ИМСИ/ИСИ (данные из АДК передаются в кОм)
        /// </summary>
        public bool _isIMSI = false;
        /// <summary>
        /// Шаблон пути к нормалям для измерений сопротивления изоляции
        /// </summary>
        public const string RizolDiagParamsPath = "Soprotivl";
        /// <summary>
        /// Максимально возможное измеренное значение сопротивления (изерение на шунте, неприменимо к ИМСИ, ИСИ)
        /// </summary>
        public const UInt32 IzolationMaxAdkMeasuredValue = UInt32.MaxValue;
		/// <summary>
		/// Последнее переданное в подсистему значение
		/// </summary>
		public float _lastSetParamState = float.NaN;
		/// <summary>
		/// Время последнего переданного в подсистему значение
		/// </summary>
		public DateTime _lastSetParamTime = DateTime.MinValue;
        /// <summary>
        /// Признак числового параметра с плавающей запятой
        /// </summary>
        protected bool _isDouble = true;
        /// <summary>
        /// Признак вычисляемого параметра
        /// </summary>
        protected bool _isComputed = false;
        /// <summary>
        /// Коэффициент изменения аналогового сигнала, при превышении которого выполнится передача нового значения потребителям:
        /// запись y1 если пред. записаное значение y0 такое что: ( |y1-y0| > _changeCoef*|y0|*(1-dt/T) )
		/// старая версия (0.7): запись y1 если пред. записаное значение y0 такое что: ( |y1-y0| > _changeCoef*|y0|/dt )
		/// </summary>
        public const double ChangeCoef = 0.05;
        /// <summary>
        /// Таймаут повторной передачи неизменившегося значения (T)
        /// </summary>
        public const double ChangeTimeout = 60;
        /// <summary>
        /// Шифр сигнала
        /// </summary>
        public string _codeSign = string.Empty;
	}

	/// <summary>
	/// Класс параметра унифицированного объекта
	/// </summary>
	public class ConserveUniObjData : UniObjData
	{
        public ConserveUniObjData(uint objID, uint paramID, bool isNumeric)
            : base(objID, paramID, isNumeric)
        { }
        public ConserveUniObjData(uint paramID, bool isNumeric)
            : base(paramID, isNumeric)
        { }
        public ConserveUniObjData()
            : base()
        { }
        /// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		override public void Load(DlxLoader Loader)
		{
			if (!Loader.GetAttribute(XmlElement.s_TextName, out m_Name) || (m_Name.Length == 0))
				throw new ArgumentException(string.Format("Имя объекта должно быть определено: {0}", Loader.CurName));

			double value = float.NaN;
			_Object = value;

			_paramID = Loader.GetAttributeUInt("ParamID", 0xffffffff);
			if(_paramID != 0xFFFFFFFF)// параметр измерения - добавляем сигнал для возможности использования в отображении
            {
                IOSignalBase sig = new IOAnalogSignal();
                sig.Owner = new IOSigModule();
                _Object = sig;
            }
			
			_diagParamsPath = Loader.GetAttributeString("NormalPath", string.Empty);
			_diagParamsPathExt = Loader.GetAttributeString("NormalPathExt", string.Empty);
			_paramType = Loader.GetAttributeUInt("ParamType", 0);
		}
	}

    /// <summary>
    /// Класс, преобразующий диагностику напрямую из ТДМ
    /// </summary>
    public class UniObjectTdmDiag : UniObjectUpSt
    {
        /// <summary>
        /// Преобразователь диагностики
        /// </summary>
        Tdm.Unification.v2.TdmToUniConverter _DiagConverter;
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _DiagConverter = (Tdm.Unification.v2.TdmToUniConverter)Loader.GetObject("DIAGSTATE");
        }

        public override void Create()
        {
            base.Create();

            if (_DiagConverter.Create(this) == false)
                throw new Exception(Name + ": Error to create _DiagConverter");
        }
    }

    /// <summary>
    /// Унифицированный объект без дочерних элементов (для АРМ)
    /// </summary>
    public class UniObjectEmpty : UniObject
    {
        public override void Load(DlxLoader Loader)
        {
            while (Loader.CurXmlElement.ElementsCount > 0)
                Loader.CurXmlElement.RemoveElement(Loader.CurXmlElement.Elements[0]);

            base.Load(Loader);
        }
    }
}
