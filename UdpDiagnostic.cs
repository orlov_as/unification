﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AfxEx;
using Ivk.IOSys;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.ComponentModel;

namespace Tdm.Unification
{
    class UdpDiagnostic : DlxObject
    {
        /// <summary>
        /// Очередь сообщений для передачи
        /// </summary>
        protected Queue<byte[]> _MessagesQueue = new Queue<byte[]>();
        protected UdpClient udpLogSender = null;
        protected byte _cntr = 0;
        protected byte _cntrState = 0;
        protected byte _Request;
        protected object _Locker = new object();
        protected int _UdpPort = 0;
        protected UInt32 _StateSendPeriod = 0;
        protected Timer _Timer;
        protected Dictionary<string, string> _ObjStates = new Dictionary<string,string>();
        protected BackgroundWorker _BW = new BackgroundWorker();

        public UdpDiagnostic()
        {
            
        }

        /// <summary>
        /// Добавление в очередь на отправку сообщения-лог
        /// </summary>
        /// <param name="msg">сообщение</param>
        public void WriteLog(string msg)
        {
            try
            {
                lock (_Locker)
                {
                    MemoryStream ms = new MemoryStream();
                    BinaryWriter bw = new BinaryWriter(ms);
                    bw.Write((byte)0);
                    bw.Write((byte)_cntr++);
                    bw.Write(msg + "\n");
                    byte[] data = new byte[ms.Position];
                    Array.Copy(ms.GetBuffer(), data, ms.Position);
                    _MessagesQueue.Enqueue(data);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Добавление в очередь на отправку сообщения-лог
        /// </summary>
        /// <param name="msg">сообщение</param>
        protected void WriteState()
        {
            try
            {
                lock (_Locker)
                {
                    MemoryStream ms = new MemoryStream();
                    BinaryWriter bw = new BinaryWriter(ms);
                    bw.Write((byte)1);
                    bw.Write((byte)_cntrState++);
                    foreach( string obj in _ObjStates.Keys )
                    {
                        string state = _ObjStates[obj];
                        bw.Write(obj);
                        bw.Write(state);
                    }
                    byte[] data = new byte[ms.Position];
                    Array.Copy(ms.GetBuffer(), data, ms.Position);
                    _MessagesQueue.Enqueue(data);
                    ms.Close();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }
        
        public void FixState(DlxObject obj, string msgState)
        {
            lock (_Locker)
            {
                _ObjStates[obj.FullName] = msgState;
            }
        } 

        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _StateSendPeriod = Loader.GetAttributeUInt("StateSendPeriod", 5000);
            _UdpPort = Loader.GetAttributeInt("UdpPort", 40000);
        }

        /// <summary>
        /// Создание объекта. Синхронизация объектов, измерений и отношений между объектами
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            
            // Запускаем таймер передачи состояния
            _Timer = new Timer(new TimerCallback(OnTimer), null, 0, _StateSendPeriod);
            // Запускаем поток передачи
            _BW.WorkerSupportsCancellation = true;
            _BW.WorkerReportsProgress = true;
            _BW.DoWork += new DoWorkEventHandler(_BW_DoWork);
            _BW.RunWorkerAsync(_UdpPort);
        }

        IPEndPoint _Requester = new IPEndPoint(0, 0);
        void _BW_DoWork(object sender, DoWorkEventArgs e)
        {
            while (_BW.CancellationPending == false)
            {
                byte request = 0;
                try
                {
                    if (udpLogSender == null)
                        udpLogSender = new UdpClient((int)e.Argument);
                    else
                    {
                        // получили запрос - запоминимаем отправителя
                        if (udpLogSender.Available > 0)
                        {
                            byte[] rcvdata = udpLogSender.Receive(ref _Requester);
                            request = rcvdata[0];
                            //if (_Reseter > 0)
                            //_Reseter = 0;
                           // WriteLog("## Connected to " + _Requester + " ##");
                            _BW.ReportProgress(request, _Requester);
                        }

                        lock(_Locker)
                        {
                            while (_MessagesQueue.Count > 0)
                            {
                                byte[] msg = _MessagesQueue.Dequeue();
                                if (_Requester.Port == 0)// || (msg[0] == 0x01 && (request & 0x10) == 0))
                                    continue;
                                udpLogSender.Send(msg, msg.Length, _Requester);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
        }

        //protected int _Reseter = 0;
        /// <summary>
        /// таймер передачи в очередь сообщения текущего состояния
        /// </summary>
        /// <param name="sender"></param>
        void OnTimer(object sender)
        {
            /*if (++_Reseter > 5)
            {
                udpLogSender.Close();
                udpLogSender = null;
            }*/
            WriteState();
        }
    }
}
