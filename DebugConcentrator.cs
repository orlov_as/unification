﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using AfxEx;
using Tdm.Unification;
using Tdm;
using Ivk;
using Ivk.IOSys;
using System.Data.SqlClient;
using System.Data;

namespace UnificationDebug
{
	public class DebugConcentrator : UnificationConcentrator, IDebugConcentrator
	{
		public override void Create()
		{
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnUpdateTimer);
			_updateTimer = new Timer(timerDelegate, null, 1000, 1000);

			base.Create();
		}
		protected override void OnData(DataCollector.Data data)
		{
			// если не никаких данных => ошибка обмена
			if (data._dataObj == null && data._data == null && data._targetElement != null)
			{
				if (!_guardedElements.Contains(data._targetElement))
					base.OnData(data);
				else if (data._targetElement is BaseIOSystem)
				{
					(data._targetElement as BaseIOSystem).SetValidData();
				}
			}
			else
				base.OnData(data);
		}

		public void OnUpdateTimer(Object stateInfo)
		{
			foreach (IOBaseElement elem in _guardedElements)
			{
				if (elem is BaseIOSystem)
				{
					(elem as BaseIOSystem).DataTime = DateTime.Now;
					(elem as BaseIOSystem).RaiseDataChangedEvent(DateTime.Now);
				}

				if (elem is UniObjSystem)
				{
					(elem as UniObjSystem).DataTime = DateTime.Now;
					(elem as UniObjSystem).UpdateObjectStates();
					(elem as UniObjSystem).RaiseDataChangedEvent(DateTime.Now);

					(elem as UniObjSystem).UpdateObjectStates();
					(elem as UniObjSystem).RaiseDataChangedEvent(DateTime.Now);
				}
			}
		}

		public void GuardElement(IOBaseElement elem)
		{
			_guardedElements.Add(elem);
		}

		public void StopAllClients()
		{
			/*foreach (DlxObject obj in Objects)
			{
				obj.Destroy();
				obj.PostDestroy();
			}*/
		}

		protected Timer _updateTimer = null;
		protected List<IOBaseElement> _guardedElements = new List<IOBaseElement>();
	}

	public class DebugSelConcentrator : SelectionConcentrator, IDebugConcentrator
	{
		public override void Create()
		{
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnUpdateTimer);
			_updateTimer = new Timer(timerDelegate, null, 1000, 1000);

			base.Create();
		}
		protected override void OnData(DataCollector.Data data)
		{
			// если не никаких данных => ошибка обмена
			if (data._dataObj == null && data._data == null && data._targetElement != null)
			{
				if (!_guardedElements.Contains(data._targetElement))
					base.OnData(data);
				else if (data._targetElement is BaseIOSystem)
				{
					(data._targetElement as BaseIOSystem).SetValidData();
				}
			}
			else
				base.OnData(data);
		}

		public void OnUpdateTimer(Object stateInfo)
		{
			foreach (IOBaseElement elem in _guardedElements)
			{
				if (elem is BaseIOSystem)
				{
					(elem as BaseIOSystem).DataTime = DateTime.Now;
					(elem as BaseIOSystem).RaiseDataChangedEvent(DateTime.Now);

					UpdateDataBy(elem as BaseIOSystem);
				}
			}
		}

		public void GuardElement(IOBaseElement elem)
		{
			_guardedElements.Add(elem);
		}

		public void StopAllClients()
		{
			/*foreach (DlxObject obj in Objects)
			{
				obj.Destroy();
				obj.PostDestroy();
			}*/
		}

		protected Timer _updateTimer = null;
		protected List<IOBaseElement> _guardedElements = new List<IOBaseElement>();
	}

	public class BDUnificationSimulation : Collector.ClientBaseOnThread
	{
		DBConnector _connector = new DBConnector();
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			base.Load(Loader);
			_findRoot = Loader.GetObjectFromAttribute("FindRoot") as DlxObject;

			_connector.Load(Loader);

			string time = Loader.GetAttributeString("StartTime", "");
			if (!DateTime.TryParse(time, out _startArhTime))
				_startArhTime = DateTime.Today;
		}

		public override bool OnInitThread()
		{
			_connector.Connect();
			return true;
		}

		protected override void Run()
		{
			DateTime RealTime = DateTime.Now.AddMinutes(-30);
			// Всегда
			while (true)
			{
				try
				{
					TimeSpan span = DateTime.Now - RealTime;
					if (span.TotalSeconds < 1)
					{
						Thread.Sleep(1000);
						continue;
					}
					// если есть сессия
					if (_connector.IsConnected)
					{
						List<UnificationConcentrator.UniDataWithObjSys> dataList = new List<UnificationConcentrator.UniDataWithObjSys>();
						/////////////////////////////////////////////
						// читаем из базы

						SqlCommand sql = new SqlCommand();
						sql.Connection = _connector.SqlConn;
						sql.CommandText = "SELECT Site_ID, Object_ID, State_ID, Time FROM ARH_StatesDYN WHERE Time>=@StartTime AND Time<@EndTime";

						sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
						sql.Parameters["@StartTime"].Value = _startArhTime;
						sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
						sql.Parameters["@EndTime"].Value = _startArhTime + span;

						SqlDataReader reader = sql.ExecuteReader();

						try
						{

							int SiteID, ObjID, StateID;
							bool IsDiag;
							DateTime BegTime, EndTime;
							while (reader.Read())
							{
								SiteID = reader.GetInt32(0);

								UniObjSystem objsSys = GetObjectsSys((uint)SiteID);

								if (objsSys == null)
									continue;

								ObjID = reader.GetInt32(1);
								StateID = reader.GetInt32(2);
								BegTime = reader.GetDateTime(3);

								UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
								state._dataTime = BegTime;
								state._siteID = (uint)SiteID;
								state._objectID = (uint)ObjID;
								state._uniState = (byte)StateID;

								dataList.Add(new UnificationConcentrator.UniDataWithObjSys(state, objsSys));
							}

						}
						finally
						{
							reader.Close();
						}

						SetData(dataList, RealTime, null);

						_startArhTime += span;
						RealTime += span;

					}
				}
				catch (ThreadInterruptedException)
				{
					return;
				}
				catch (ThreadAbortException)
				{
					return;
				}
				catch (Exception e)
				{
					// закрываем сессию
					_connector.Disconnect();
				}
				// в любом случае проверяем сесию
				if (!_connector.IsConnected)
				{
					// если нет полученных сообщений и не требуется перезапуск, то выдерживаем время
					Thread.Sleep(10000);

					_connector.Connect();
				}
			}
		}

		/// <summary>
		/// Переопределено для поиска подсистемы объектов
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if (_findRoot != null)
			{
				if (typeof(DlxCollection).IsInstanceOfType(_findRoot))
				{
					DlxCollection coll = _findRoot as DlxCollection;
					// поиск в первом уровне вложенности
					foreach (DlxObject obj in coll.Objects)
					{
						UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
						if (objSys != null)
							_idsysTOrefMAP.Add(objSys.SiteID, objSys);
					}
					// если ничего не нашли
					if (_idsysTOrefMAP.Count == 0)
					{
						// поиск во втором уровне вложенности
						foreach (DlxCollection subcoll in coll.Objects)
						{
							if (subcoll != null)
							{
								foreach (DlxObject obj in subcoll.Objects)
								{
									UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
									if (objSys != null)
										_idsysTOrefMAP.Add(objSys.SiteID, objSys);
								}
							}
						}
					}
				}
			}

			if (_idsysTOrefMAP.Count == 0)
			{
                throw new ArgumentException("Карта идентификаторов пустая");
			}

			base.Create();
		}

		/// <summary>
		/// Получить подсистему объектов по идентификатору
		/// </summary>
		/// <param name="SiteID">идентификатор</param>
		/// <returns>подсистема объектов</returns>
		public UniObjSystem GetObjectsSys(uint SiteID)
		{
			UniObjSystem iosys = null;
			if (_idsysTOrefMAP.TryGetValue(SiteID, out iosys))
				return iosys;
			else
				return null;
		}

		/// <summary>
		/// Карта идентификаторов к указателям на подсистемы ввода
		/// </summary>
		protected Dictionary<uint, UniObjSystem> _idsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
		/// <summary>
		/// начало требуемого интервала
		/// </summary>
		protected DateTime _startArhTime;
		/// <summary>
		/// конец требуемого интервала
		/// </summary>
		protected DateTime _endArhTime;
		/// <summary>
		/// Объект - корень для поиска фабрики соединений, подсистемы ввода и подсистемы объектов
		/// </summary>
		protected DlxObject _findRoot = null;
	}
}
