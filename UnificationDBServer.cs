﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Threading;
using System.Data;
using Dps;
using Dps.UniRequest;

namespace Tdm.Unification
{
	/// <summary>
	/// Серверное соединения по унифицированному протоколу второй редакции для доступа к БД (факт ТО, архив, план ТО)
	/// </summary>
	public class UnificDBSrvConnection : UnificServerConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public UnificDBSrvConnection()
		{
		}
		/// <summary>
		/// Конструктор
		/// </summary>
		public UnificDBSrvConnection(DBConnector dbconnector)
		{
			_dbconnector = new DBConnector("DBSrv", dbconnector.ConnectionString);
		}
		/// <summary>
		/// Деструктор
		/// </summary>
		~UnificDBSrvConnection()
		{
			Disconnect();
			_workTimer.Dispose();
		}

		/// <summary>
		/// Обработать запрос
		/// </summary>
		/// <param name="Msg">Обрабатываемое сообщение</param>
		protected override void ExcecuteRequest(Message Msg)
		{
			try
			{
				if (!Connect())
					throw new RequestException(ErrorCode.Unknown, "Can't to connect to Data Base for required information!");

				BinaryReader reader = new BinaryReader(new MemoryStream(Msg.Data));
				_Answer = null;

				if (_bWaitFirstRequest && !CheckIdentification(reader))
					throw new RequestException(ErrorCode.WrongMode);

				if (Msg.Code == (RequestCode)0)// запрос идентификации
				{
					_Answer = new Message();
					_Answer.Code = (RequestCode)0;
				}
				else if (Msg.Code == (RequestCode)1)// запрос текущего состояния
				{
					// считываем требования
					ReadRequirements(reader);

                    if (IsNeedFactWorks && !IsNeedPlanWorks)
                        PrepareAnswerFactWorks();
                    else if (!IsNeedFactWorks && IsNeedPlanWorks)
                        PrepareAnswerPlanWorks();
                    else
                    {   // к НСИ-службе прислан запрос текущего состояния, готовим пустой ответ вместо генерации исключения
                        PrepareAnswerNSI_CurStub();
                        Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)502, "Отправлен пустой ответ на запрос за текущим состоянием");
                    }
                    //else
                    //    throw new RequestException(ErrorCode.WrongMode, "This server is used real time mode only for fact works and plan works individually!");

				}
				else if (Msg.Code == (RequestCode)2)// запрос версии НСИ
				{
					// подготавливаем ответ
					PrepareAnswerNSIVersion();
				}
				else if (Msg.Code == (RequestCode)3)// запрос состава НСИ
				{
					// считываем требования
					ReadRequirements(reader);
					// подготавливаем ответ
					PrepareAnswerNSI();
				}
				else if (Msg.Code == (RequestCode)0x11)// запрос архива
				{
					///////////////////////////////
					int StartTime = reader.ReadInt32();
					int EndTime = reader.ReadInt32();

					_startArhTime = Afx.CTime.ToDateTime(StartTime);
					_endArhTime = Afx.CTime.ToDateTime(EndTime);

					// считываем требования
					ReadRequirements(reader);
					// подготавливаем ответ
					PrepareAnswerFirstArchState();
					/////////////////////////////
				}
				else
					throw new RequestException(ErrorCode.WrongCode);

			}
			catch (Exception ex)
			{
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)502, "Ошибка обработки запроса к серверу НСИ и Архив: " + ex.Message);
				throw ex;
			}

		}
		/// <summary>
		/// получить ответ
		/// </summary>
		/// <returns>сообщение ответа</returns>
		protected override Message GetAnswer()
		{
			return base.GetAnswer();
		}

		#region НСИ

		/// <summary>
		/// Подготовка ответа версии НСИ
		/// </summary>
		protected override void PrepareAnswerNSIVersion()
		{
			_bSporadic = false;
			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)2;

			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();
		}
		/// <summary>
		/// Подготовка ответа состава НСИ
		/// </summary>
		protected override void PrepareAnswerNSI()
		{
			_bSporadic = false;
			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			/////////////////////////////////////////////
			UInt32 SiteCount;
			UInt32 Size = 0;
			long pos1, pos2, pos3, pos4;

			////////////////////////////////////////////
			#region запись НСИ объектов
			Size = 0;
			if (IsNeedNSIObjects)
			{
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;

				///////////////////////////////////////
				// читаем из базы
                SqlCommand sqlSites = new SqlCommand("SELECT TOP (100) PERCENT ISNULL(Site_ID_peregon, Site_ID) AS Site_ID FROM PRJ_Objects GROUP BY ISNULL(Site_ID_peregon, Site_ID) ORDER BY ISNULL(Site_ID_peregon, Site_ID)", _sqlConn);
				SqlDataAdapter da = new SqlDataAdapter(sqlSites);
				DataSet ds = new DataSet();
				da.Fill(ds);
				DataTable tbsites = ds.Tables[0];
				foreach (DataRow row in tbsites.Rows)
				{
					uint SiteID = Convert.ToUInt32(row[0]);
					// если требуются НСИ по станции
                    if (_bAllSites || _reqSites.Contains(SiteID))
					{
						// пишем идентификатор
						writer.Write(SiteID);
						// увеличиваем счетчик
						SiteCount++;

						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ObjCount = 0;
						// пишем количество записей
						writer.Write(ObjCount);
                        SqlCommand sqlObjs = new SqlCommand(string.Format("SELECT Object_ID, Type_ID, Object_Name FROM PRJ_Objects WHERE ISNULL(Site_ID_peregon, Site_ID)={0} ORDER BY Object_ID", SiteID), _sqlConn);
						SqlDataReader readerObjs = sqlObjs.ExecuteReader();
						while (readerObjs.Read())
						{
                            if (readerObjs.IsDBNull(0) || readerObjs.IsDBNull(1) || readerObjs.IsDBNull(2))
                                continue;
							ObjCount++;
							// пишем идентификатор объекта
							writer.Write(readerObjs.GetInt32(0));
							// пишем идентификатор типа
							writer.Write(readerObjs.GetInt32(1));
							// пишем имя объекта
							byte[] name = System.Text.Encoding.Default.GetBytes(readerObjs.GetSqlString(2).Value);
							if (name.Length > Byte.MaxValue)
							{
								byte[] newname = new byte[Byte.MaxValue];
								Array.Copy(name, newname, Byte.MaxValue);
								name = newname;
							}
							writer.Write((Byte)name.Length);
							writer.Write(name);
						}
						readerObjs.Close();
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ObjCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ диагностики
			Size = 0;
			if (IsNeedNSIDiag)
			{
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;

				///////////////////////////////////////
				// читаем из БД
				SqlCommand sqlSites = new SqlCommand("SELECT     TOP (100) PERCENT ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_DiagStates.Site_ID) AS Site_ID, PRJ_DiagStates.Site_ID AS Site_ID_out " +
										"FROM PRJ_DiagStates LEFT OUTER JOIN PRJ_Objects ON PRJ_DiagStates.Site_ID = PRJ_Objects.Site_ID AND PRJ_DiagStates.Object_ID = PRJ_Objects.Object_ID " +
										"GROUP BY PRJ_DiagStates.Site_ID, ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_DiagStates.Site_ID) ORDER BY Site_ID", _sqlConn);
				SqlDataAdapter da = new SqlDataAdapter(sqlSites);
				DataSet ds = new DataSet();
				da.Fill(ds);
				DataTable tbsites = ds.Tables[0];
				foreach (DataRow row in tbsites.Rows)
				{
					uint SiteIDget = Convert.ToUInt32(row[0]);
					uint SiteID = Convert.ToUInt32(row[1]);
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(SiteIDget) )
					{
						// пишем идентификатор
						writer.Write(SiteID);
						// увеличиваем счетчик
						SiteCount++;

						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 DiagCount = 0;
						// пишем количество записей
						writer.Write(DiagCount);
						SqlCommand sqlDiags = new SqlCommand();
						sqlDiags.Connection = _sqlConn;

						if (SiteID == SiteIDget)
							sqlDiags.CommandText = "SELECT     TOP (100) PERCENT dbo.PRJ_DiagStates.Object_ID, dbo.PRJ_DiagStates.DiagState_ID, dbo.PRJ_DiagStates.Site_ID " +
								"FROM dbo.PRJ_DiagStates LEFT OUTER JOIN dbo.PRJ_Objects ON dbo.PRJ_DiagStates.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_DiagStates.Object_ID = dbo.PRJ_Objects.Object_ID " +
								"WHERE (dbo.PRJ_DiagStates.Site_ID = @Site) AND (dbo.PRJ_Objects.Site_ID_peregon IS NULL OR dbo.PRJ_Objects.Site_ID_peregon = @Site) " +
								"ORDER BY dbo.PRJ_DiagStates.Object_ID";
						else
							sqlDiags.CommandText = "SELECT     TOP (100) PERCENT dbo.PRJ_DiagStates.Object_ID, dbo.PRJ_DiagStates.DiagState_ID, dbo.PRJ_DiagStates.Site_ID " +
								"FROM dbo.PRJ_DiagStates LEFT OUTER JOIN dbo.PRJ_Objects ON dbo.PRJ_DiagStates.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_DiagStates.Object_ID = dbo.PRJ_Objects.Object_ID " +
								"WHERE (dbo.PRJ_DiagStates.Site_ID = @Site) AND dbo.PRJ_Objects.Site_ID_peregon = @SiteGet " +
								"ORDER BY dbo.PRJ_DiagStates.Object_ID";

						sqlDiags.Parameters.Add("@Site", SqlDbType.Int);
						sqlDiags.Parameters["@Site"].Value = SiteID;
						sqlDiags.Parameters.Add("@SiteGet", SqlDbType.Int);
						sqlDiags.Parameters["@SiteGet"].Value = SiteIDget;

						SqlDataReader readerDiags = sqlDiags.ExecuteReader();
						while (readerDiags.Read())
						{
                            if (readerDiags.IsDBNull(0) || readerDiags.IsDBNull(1))
                                continue;
							DiagCount++;
							// пишем идентификатор объекта
							writer.Write(readerDiags.GetInt32(0));
							// пишем идентификатор ситуации
							writer.Write(readerDiags.GetInt16(1));
						}
						readerDiags.Close();
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(DiagCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}
				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ параметров
			Size = 0;
			if (IsNeedNSIParams)
			{
				///////////////////////////////////////
				#region записываем НСИ числовых парметров
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;

				///////////////////////////////////////
				// читаем из БД
                SqlCommand sqlSites = new SqlCommand(@"SELECT     TOP (100) PERCENT ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID) AS Site_ID
                                                    FROM PRJ_Params 
                                                    INNER JOIN PRJ_Objects ON PRJ_Params.Site_ID = PRJ_Objects.Site_ID AND PRJ_Params.Object_ID = PRJ_Objects.Object_ID
                                                    WHERE (PRJ_Params.IsNumeric = 1 OR PRJ_Params.IsNumeric IS NULL)
                                                    GROUP BY ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID)
                                                    ORDER BY ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID)", _sqlConn);
				SqlDataAdapter da = new SqlDataAdapter(sqlSites);
				DataSet ds = new DataSet();
				da.Fill(ds);
				DataTable tbsites = ds.Tables[0];
				foreach (DataRow row in tbsites.Rows)
				{
					uint SiteID = Convert.ToUInt32(row[0]);
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(SiteID))
					{
						// пишем идентификатор
						writer.Write(SiteID);
						// увеличиваем счетчик
						SiteCount++;

						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ParamsCount = 0;
						// пишем количество записей
						writer.Write(ParamsCount);

						SqlCommand sqlParams = new SqlCommand(
							string.Format(@"SELECT     TOP (100) PERCENT dbo.PRJ_Params.Object_ID, dbo.PRJ_Params.Param_ID, dbo.PRJ_Params.Param_Type_ID, dbo.PRJ_Params.Param_Name, 
                                    dbo.PRJ_Params.Param_Description, dbo.PRJ_Params.Normal_Max, dbo.PRJ_Params.Normal_Min, dbo.PRJ_Params.Normal_Ext 
                                    FROM dbo.PRJ_Params INNER JOIN dbo.PRJ_Objects ON dbo.PRJ_Params.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_Params.Object_ID = dbo.PRJ_Objects.Object_ID 
                                    WHERE ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID) = {0} AND (PRJ_Params.IsNumeric = 1 OR PRJ_Params.IsNumeric IS NULL)
                                    ORDER BY dbo.PRJ_Params.Object_ID", SiteID), _sqlConn);
						SqlDataReader readerParams = sqlParams.ExecuteReader();
						while (readerParams.Read())
						{
                            if (readerParams.IsDBNull(0) || readerParams.IsDBNull(1) || readerParams.IsDBNull(2) || readerParams.IsDBNull(3))
                                continue;

							ParamsCount++;
							// пишем идентификатор объекта
							writer.Write(readerParams.GetInt32(0));
							// пишем идентификатор параметра
							writer.Write(readerParams.GetInt32(1));
							// пишем идентификатор типа
							writer.Write(readerParams.GetInt32(2));
							// пишем имя параметра
							string strname = readerParams.GetSqlString(3).Value.Trim();
							byte[] name = System.Text.Encoding.Default.GetBytes(strname);
							if (name.Length > Byte.MaxValue)
							{
								byte[] newname = new byte[Byte.MaxValue];
								Array.Copy(name, newname, Byte.MaxValue);
								name = newname;
							}
							writer.Write((Byte)name.Length);
							writer.Write(name);

							// пишем описание параметра
                            if (!readerParams.IsDBNull(4))
                            {
                                name = System.Text.Encoding.Default.GetBytes(readerParams.GetSqlString(4).Value);
                                writer.Write((UInt16)name.Length);
                                writer.Write(name);
                            }
                            else
                                writer.Write((UInt16)0);

							// пишем норамли
							if (!readerParams.IsDBNull(5))
								writer.Write(readerParams.GetFloat(5));
							else
								writer.Write((UInt32)0xFFFFFFFF);
							if (!readerParams.IsDBNull(6))
								writer.Write(readerParams.GetFloat(6));
							else
								writer.Write((UInt32)0xFFFFFFFF);
                            if (!readerParams.IsDBNull(7) && readerParams.GetFloat(7) != float.MaxValue && !float.IsNaN(readerParams.GetFloat(7)) && !float.IsInfinity(readerParams.GetFloat(7)))
								writer.Write(readerParams.GetFloat(7));
							else
								writer.Write((UInt32)0xFFFFFFFF);
						}
						readerParams.Close();
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ParamsCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}
				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);

				#endregion
				///////////////////////////////////////

				///////////////////////////////////////
				#region записываем НСИ нечисловых парметров
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;

				///////////////////////////////////////
				// читаем из БД
				sqlSites = new SqlCommand(@"SELECT TOP (100) PERCENT ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID) AS Site_ID
                                        FROM PRJ_Params 
                                        INNER JOIN PRJ_Objects ON PRJ_Params.Site_ID = PRJ_Objects.Site_ID AND PRJ_Params.Object_ID = PRJ_Objects.Object_ID
                                        WHERE ((PRJ_Params.IsNumeric = 0) OR (PRJ_Params.IsNumeric IS NULL)) AND (PRJ_Params.IsNumeric = 0)
                                        GROUP BY ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID)
                                        ORDER BY ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID)", _sqlConn);
				da = new SqlDataAdapter(sqlSites);
				ds = new DataSet();
				da.Fill(ds);
				tbsites = ds.Tables[0];
				foreach (DataRow row in tbsites.Rows)
				{
					uint SiteID = Convert.ToUInt32(row[0]);
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(SiteID) )
					{
						// пишем идентификатор
						writer.Write(SiteID);
						// увеличиваем счетчик
						SiteCount++;

						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ParamsCount = 0;
						// пишем количество записей
						writer.Write(ParamsCount);

                        SqlCommand sqlParams = new SqlCommand(
                            string.Format(@"SELECT     TOP (100) PERCENT dbo.PRJ_Params.Object_ID, dbo.PRJ_Params.Param_ID, dbo.PRJ_Params.Param_Type_ID, dbo.PRJ_Params.Param_Name, 
                                    dbo.PRJ_Params.Param_Description, dbo.PRJ_Params.Normal_Max, dbo.PRJ_Params.Normal_Min, dbo.PRJ_Params.Normal_Ext 
                                    FROM dbo.PRJ_Params INNER JOIN dbo.PRJ_Objects ON dbo.PRJ_Params.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_Params.Object_ID = dbo.PRJ_Objects.Object_ID 
                                    WHERE ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Objects.Site_ID) = {0} AND (PRJ_Params.IsNumeric = 0)
                                    ORDER BY dbo.PRJ_Params.Object_ID", SiteID), _sqlConn);
						SqlDataReader readerParams = sqlParams.ExecuteReader();

						while (readerParams.Read())
						{
                            if (readerParams.IsDBNull(0) || readerParams.IsDBNull(1) || readerParams.IsDBNull(2) || readerParams.IsDBNull(3))
                                continue;

							ParamsCount++;
							// пишем идентификатор объекта
							writer.Write(readerParams.GetInt32(0));
							// пишем идентификатор параметра
							writer.Write(readerParams.GetInt32(1));
							// пишем идентификатор типа
							writer.Write(readerParams.GetInt32(2));
							// пишем имя параметра
							byte[] name = System.Text.Encoding.Default.GetBytes(readerParams.GetSqlString(3).Value);
							if (name.Length > Byte.MaxValue)
							{
								byte[] newname = new byte[Byte.MaxValue];
								Array.Copy(name, newname, Byte.MaxValue);
								name = newname;
							}
							writer.Write((Byte)name.Length);
							writer.Write(name);
                            if (!readerParams.IsDBNull(4))
                            {
                                // пишем описание параметра
                                name = System.Text.Encoding.Default.GetBytes(readerParams.GetSqlString(4).Value);
                                writer.Write((UInt16)name.Length);
                                writer.Write(name);
                            }
                            else
                                writer.Write((UInt16)0);
						}
						readerParams.Close();
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ParamsCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}
				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);

				#endregion
				///////////////////////////////////////
			}
			else
			{
				writer.Write(Size);
				writer.Write(Size);
			}
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ работ
			////////////////////////////////////////////
			Size = 0;
			if (IsNeedNSIWorks)
			{
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;

				///////////////////////////////////////
				// читаем из БД
				SqlCommand sqlSites = new SqlCommand("SELECT     TOP (100) PERCENT ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Works.Site_ID) AS Site_ID, PRJ_Works.Site_ID AS Site_ID_out " +
										"FROM PRJ_Objects RIGHT OUTER JOIN PRJ_Works ON PRJ_Objects.Site_ID = PRJ_Works.Site_ID AND PRJ_Objects.Object_ID = PRJ_Works.Object_ID " +
										"GROUP BY ISNULL(PRJ_Objects.Site_ID_peregon, PRJ_Works.Site_ID), PRJ_Works.Site_ID " +
										"ORDER BY Site_ID, Site_ID_out", _sqlConn);
				SqlDataAdapter da = new SqlDataAdapter(sqlSites);
				DataSet ds = new DataSet();
				da.Fill(ds);
				DataTable tbsites = ds.Tables[0];
				foreach (DataRow row in tbsites.Rows)
				{
					uint SiteIDget = Convert.ToUInt32(row[0]);
					uint SiteID = Convert.ToUInt32(row[1]);
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(SiteIDget))
					{
						// пишем идентификатор
						writer.Write(SiteID);
						// увеличиваем счетчик
						SiteCount++;

						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 TOCount = 0;
						// пишем количество записей
						writer.Write(TOCount);

						SqlCommand sqlTO = new SqlCommand();
						sqlTO.Connection = _sqlConn;
						if (SiteID == SiteIDget)
							sqlTO.CommandText = "SELECT     TOP (100) PERCENT dbo.PRJ_Works.Object_ID, dbo.PRJ_Works.Punkt_ID " +
									"FROM dbo.PRJ_Works LEFT OUTER JOIN dbo.PRJ_Objects ON dbo.PRJ_Works.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_Works.Object_ID = dbo.PRJ_Objects.Object_ID " +
									"WHERE (dbo.PRJ_Works.Site_ID = @Site) AND (dbo.PRJ_Objects.Site_ID_peregon IS NULL OR dbo.PRJ_Objects.Site_ID_peregon = @Site) " +
									"ORDER BY dbo.PRJ_Works.Object_ID";
						else
							sqlTO.CommandText = "SELECT     TOP (100) PERCENT dbo.PRJ_Works.Object_ID, dbo.PRJ_Works.Punkt_ID " +
									"FROM dbo.PRJ_Works LEFT OUTER JOIN dbo.PRJ_Objects ON dbo.PRJ_Works.Site_ID = dbo.PRJ_Objects.Site_ID AND dbo.PRJ_Works.Object_ID = dbo.PRJ_Objects.Object_ID " +
									"WHERE (dbo.PRJ_Works.Site_ID = @Site) AND (dbo.PRJ_Objects.Site_ID_peregon = @SiteGet) " +
									"ORDER BY dbo.PRJ_Works.Object_ID";

						sqlTO.Parameters.Add("@Site", SqlDbType.Int);
						sqlTO.Parameters["@Site"].Value = SiteID;
						sqlTO.Parameters.Add("@SiteGet", SqlDbType.Int);
						sqlTO.Parameters["@SiteGet"].Value = SiteIDget;

						SqlDataReader readerTO = sqlTO.ExecuteReader();
						while (readerTO.Read())
						{
                            if (readerTO.IsDBNull(0) || readerTO.IsDBNull(1) )
                                continue;

							TOCount++;
							// пишем идентификатор объекта
							writer.Write(readerTO.GetInt32(0));
							// пишем идентификатор ситуации
							writer.Write(readerTO.GetInt32(1));
						}
						readerTO.Close();
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(TOCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}
				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			/////////////////////////////////////////
			#endregion
			////////////////////////////////////////////

			/////////////////////////////////////////////
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)3;
			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();
		}
        /// <summary>
        /// Подготовка пустого ответа (имитация передачи текущего состояния)
        /// </summary>
        protected void PrepareAnswerNSI_CurStub()
        {
            _bSporadic = false;
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);

            // записываем версию
            writer.Write((uint)_versionNSI);

            // пишем время
            int stateTime = Afx.CTime.ToTimeT(DateTime.Now);
            writer.Write(stateTime);
            // пишем все пустые данные
            int size = 0;
            writer.Write(size);
            writer.Write(size);
            writer.Write(size);
            writer.Write(size);
            writer.Write(size);
            // копируем данные из потока в ответ
            writer.Flush();
            writer.Close();

            _Answer = new Message();
            _Answer.Code = (RequestCode)1;
            // получаем массив данных для ответа
            _Answer.Data = ms.ToArray();
            // закрываем поток
            ms.Close();
        }

		#endregion

		#region Архив

		/// <summary>
		/// Класс сравнения данных унифицированного обмена
		/// </summary>
		protected class UniDataTimeComparer : IComparer<UnificationConcentrator.UniDataItem>
		{
			/// <summary>
			/// Сравнение двух событий по времени появления
			/// </summary>
			/// <param name="x">первый параметр</param>
			/// <param name="y">второй параметр</param>
			/// <returns>стандартный результат сравнения</returns>
			public int Compare(UnificationConcentrator.UniDataItem x, UnificationConcentrator.UniDataItem y)
			{
				return x._dataTime.CompareTo(y._dataTime);
			}

		}

		/// <summary>
		/// Подготовка первого ответа на запрос за архивом 
		/// </summary>
		protected void PrepareAnswerFirstArchState()
		{
			_sporadicAnswerCode = 0x11;
			_bArchiveMode = true;
			_bSporadic = true;

			List<UnificationConcentrator.UniDataItem> dataList = new List<UnificationConcentrator.UniDataItem>();
			DateTime correctStartArhTime = _startArhTime - TimeSpan.FromMinutes(10);
			/////////////////////////////////////////////
			// читаем из базы

			#region Состояния и ситуации
			if (IsNeedObjectStates || IsNeedObjectDiags)
			{
				SqlCommand sql = new SqlCommand();
				sql.Connection = _sqlConn;
				sql.CommandText = "SELECT * FROM ARH_StatesAll_view WHERE Time>@StartTime AND Time<@EndTime";

				sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
				sql.Parameters["@StartTime"].Value = correctStartArhTime;
				sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
				sql.Parameters["@EndTime"].Value = _endArhTime;

				if (_reqSites.Count > 0)
				{
					sql.CommandText += " AND ( Site_ID=" + _reqSites[0].ToString();
					for (int i = 1; i < _reqSites.Count; i++)
						sql.CommandText += " OR Site_ID=" + _reqSites[0].ToString();

					sql.CommandText += ")";
				}

				if (!IsNeedObjectStates)
					sql.CommandText += " AND IsDiag=1";

				SqlDataReader reader = sql.ExecuteReader();

				int SiteID, ObjID, StateID;
				bool IsDiag;
				DateTime BegTime, EndTime;
				while (reader.Read())
				{
					SiteID = reader.GetInt32(1);
					AddSiteToDictionaries((uint)SiteID);
					ObjID = reader.GetInt32(2);
					StateID = reader.GetInt32(3);
					IsDiag = reader.GetBoolean(6);

					BegTime = reader.GetDateTime(4);
					EndTime = reader.GetDateTime(5);

					if (IsDiag)
					{
						UnificationConcentrator.UniDiagState diag = new UnificationConcentrator.UniDiagState();
						diag._dataTime = BegTime;
						diag._siteID = (uint) SiteID;
						diag._objectID = (uint) ObjID;
						diag._uniDiagState = StateID;
						diag._detectTime = BegTime;
						diag._endTime = EndTime;
						dataList.Add(diag);
					}
					else
					{
						UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
						state._dataTime = BegTime;
						state._siteID = (uint) SiteID;
						state._objectID = (uint) ObjID;
						state._uniState = (byte) StateID;
						dataList.Add(state);
					}
				}

				reader.Close();
			}
			#endregion

			#region Числовые параметры
			if (IsNeedObjectsNumericParams)
			{
				SqlCommand sql = new SqlCommand();
				sql.Connection = _sqlConn;
				sql.CommandText = "SELECT * FROM ARH_ParamsNumericUni WHERE Time>@StartTime AND Time<@EndTime";

				sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
				sql.Parameters["@StartTime"].Value = correctStartArhTime;
				sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
				sql.Parameters["@EndTime"].Value = _endArhTime;

				if (_reqSites.Count > 0)
				{
					sql.CommandText += " AND ( Site_ID=" + _reqSites[0].ToString();
					for (int i = 1; i < _reqSites.Count; i++)
						sql.CommandText += " OR Site_ID=" + _reqSites[0].ToString();

					sql.CommandText += ")";
				}

				SqlDataReader reader = sql.ExecuteReader();

				int SiteID, ParamID;
				float Value;
				DateTime Time;
				while (reader.Read())
				{
					SiteID = reader.GetInt32(1);
					AddSiteToDictionaries((uint)SiteID);
					ParamID = reader.GetInt32(2);
					Value = reader.GetFloat(4);

					Time = reader.GetDateTime(3);

					UnificationConcentrator.UniNumericParamState par = new UnificationConcentrator.UniNumericParamState();
					par._dataTime = Time;
					par._siteID = (uint) SiteID;
					par._paramID = (uint) ParamID;
					par._paramValue = Value;
					par._paramTime = Time;
					dataList.Add(par);
				}

				reader.Close();
			}
			#endregion

			#region Нечисловые параметры
			if (IsNeedObjectsDataParams)
			{
				SqlCommand sql = new SqlCommand();
				sql.Connection = _sqlConn;
				sql.CommandText = "SELECT * FROM ARH_ParamsDataUni WHERE Time>@StartTime AND Time<@EndTime";

				sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
				sql.Parameters["@StartTime"].Value = correctStartArhTime;
				sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
				sql.Parameters["@EndTime"].Value = _endArhTime;

				if (_reqSites.Count > 0)
				{
					sql.CommandText += " AND ( Site_ID=" + _reqSites[0].ToString();
					for (int i = 1; i < _reqSites.Count; i++)
						sql.CommandText += " OR Site_ID=" + _reqSites[0].ToString();

					sql.CommandText += ")";
				}

				SqlDataReader reader = sql.ExecuteReader();

				int SiteID, ParamID;
				byte[] Value;
				DateTime Time;
				while (reader.Read())
				{
					SiteID = reader.GetInt32(1);
					AddSiteToDictionaries((uint)SiteID);
					ParamID = reader.GetInt32(2);
					Value = reader.GetSqlBytes(4).Buffer;

					Time = reader.GetDateTime(3);

					UnificationConcentrator.UniDataParamState par = new UnificationConcentrator.UniDataParamState();
					par._dataTime = Time;
					par._siteID = (uint) SiteID;
					par._paramID = (uint) ParamID;
					par._paramValue = Value;
					dataList.Add(par);
				}

				reader.Close();
			}
			#endregion

			/////////////////////////////////////////////
			// сортируем очередь данных
			UniDataTimeComparer comparer = new UniDataTimeComparer();
			dataList.Sort(comparer);
			
			/////////////////////////////////////////////
			// добавляем данные в очередь
			_dataQueue.Clear();
			for (int i = 0; i < dataList.Count; i++)
				_dataQueue.Enqueue(dataList[i]);
			/////////////////////////////////////////////
		}

		#endregion

		#region Факты работ

		/// <summary>
		/// Подготовка первого ответа на запрос за фактом ТО
		/// </summary>
		protected void PrepareAnswerFactWorks()
		{
			_sporadicAnswerCode = 0x1;
			_bArchiveMode = false;
			_bSporadic = true;

			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			// пишем время
			DateTime time = DateTime.Now;
			writer.Write((UInt32)Afx.CTime.ToTimeT(time));


			SqlCommand sql = new SqlCommand();
			sql.Connection = _sqlConn;
			sql.CommandText = "SELECT * FROM ARH_CompletedWorks WHERE fact_end_time>@StartTime AND fact_beg_time<@EndTime";

			sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
			sql.Parameters["@StartTime"].Value = DateTime.Today;
			sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
			sql.Parameters["@EndTime"].Value = DateTime.Today.AddDays(1);

			if (_reqSites.Count > 0)
			{
				sql.CommandText += " AND ( Site_ID=" + _reqSites[0].ToString();
				for (int i = 1; i < _reqSites.Count; i++)
					sql.CommandText += " OR Site_ID=" + _reqSites[0].ToString();

				sql.CommandText += ")";
			}

			SqlDataReader reader = sql.ExecuteReader();

			int SiteID, ObjID, PunktID, WorkID;
			Int64 ID;
			DateTime BegTime, EndTime;
			while (reader.Read())
			{
				ID = reader.GetInt64(0);
				if (_maxFactWorkID < ID)
					_maxFactWorkID = ID;

				SiteID = reader.GetInt32(2);
				AddSiteToDictionaries((uint)SiteID);
				ObjID = reader.GetInt32(4);
				PunktID = reader.GetInt32(5);
				WorkID = reader.GetInt32(6);

				BegTime = reader.GetDateTime(7);
				EndTime = reader.GetDateTime(8);

				UnificationConcentrator.UniObjWork work = new UnificationConcentrator.UniObjWork();
				work._flag = UnificationConcentrator.UniObjWork.Kind.Complete;// факт работы
				work._siteID = (uint)SiteID;
				work._objectID = (uint)ObjID;
				work._punktID = (uint) PunktID;
				work._workID = (uint)WorkID;
				work._beginTime = BegTime;
				work._endTime = EndTime;
                work._dataTime = EndTime;
                work._diags = new Dictionary<ushort, DateTime>();

				// получаем все ситуации соответствующие ТО
				GetRelativeDiags(ID, SiteID, ObjID, work._diags);

				_curAnswerWorks[(uint)SiteID].Enqueue(work);
			}

			reader.Close();

			// записываем данные в поток
			WriteCurAnswerData(writer);

			/////////////////////////////////////////////
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)_sporadicAnswerCode;
			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();

			/////////////////////////////////////////////
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnFactWorksTimer);
			_workTimer = new Timer(timerDelegate, null, 60000, 60000);
		}
		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// </summary>
		public void OnFactWorksTimer(Object stateInfo)
		{
			SqlCommand sql = new SqlCommand();
			sql.Connection = _sqlConn;
			sql.CommandText = "SELECT * FROM ARH_CompletedWorks WHERE ID>" + _maxFactWorkID.ToString();

			if (_reqSites.Count > 0)
			{
				sql.CommandText += " AND ( Site_ID=" + _reqSites[0].ToString();
				for (int i = 1; i < _reqSites.Count; i++)
					sql.CommandText += " OR Site_ID=" + _reqSites[0].ToString();

				sql.CommandText += ")";
			}

			SqlDataReader reader = sql.ExecuteReader();

			int SiteID, ObjID, PunktID, WorkID;
			Int64 ID;
			DateTime BegTime, EndTime;
			while (reader.Read())
			{
				ID = reader.GetInt64(0);
				if (_maxFactWorkID < ID)
					_maxFactWorkID = ID;

				SiteID = reader.GetInt32(2);
				AddSiteToDictionaries((uint)SiteID);
				ObjID = reader.GetInt32(4);
				PunktID = reader.GetInt32(5);
				WorkID = reader.GetInt32(6);

				BegTime = reader.GetDateTime(7);
				EndTime = reader.GetDateTime(8);

				UnificationConcentrator.UniObjWork work = new UnificationConcentrator.UniObjWork();
                work._flag = UnificationConcentrator.UniObjWork.Kind.Complete;// факт работы
				work._siteID = (uint)SiteID;
				work._objectID = (uint)ObjID;
				work._punktID = (uint)PunktID;
				work._workID = (uint)WorkID;
				work._beginTime = BegTime;
				work._endTime = EndTime;
                work._dataTime = EndTime;
				work._diags = new Dictionary<ushort, DateTime>();

				//GetRelativeDiags(ID, SiteID, ObjID, work._diags);

				_dataQueue.Enqueue(work);
			}
			reader.Close();
		}
		/// <summary>
		/// Получить по ID работы все соответствующие сбои
		/// </summary>
		/// <param name="ID">идентификатор завершенной работы</param>
		/// <param name="SiteID">идентификатор станции</param>
		/// <param name="ObjID">идентификатор объекта</param>
		/// <param name="diags">МАР диагностических ситуаций</param>
		protected void GetRelativeDiags(Int64 ID, int SiteID, int ObjID, Dictionary<ushort, DateTime> diags)
		{
			SqlCommand diagscmd = new SqlCommand();
			diagscmd.Connection = _sqlConn;
			diagscmd.CommandText = "SELECT ARH_States.State_ID, ARH_States.Time"
				+ " FROM ARH_States RIGHT JOIN ARH_WorkStateRelations ON ARH_States.ID = ARH_WorkStateRelations.StateARH_ID"
				+ " WHERE ARH_WorkStateRelations.TOARH_ID=" + ID.ToString() + " AND ARH_States.Site_ID=" + SiteID.ToString() + " AND ARH_States.Object_ID=" + ObjID.ToString()
				+ " AND ARH_States.IsDiag=1";

			SqlDataReader diagsreader = diagscmd.ExecuteReader();
			int StateID;
			DateTime DiagTime;
			while (diagsreader.Read())
			{
				StateID = diagsreader.GetInt32(0);
				DiagTime = diagsreader.GetDateTime(1);
				diags.Add((ushort)StateID, DiagTime);
			}
			diagsreader.Close();
		}

		#endregion

		#region План работ

		/// <summary>
		/// Подготовка первого ответа на запрос за планом ТО
		/// </summary>
		protected void PrepareAnswerPlanWorks()
		{
			_sporadicAnswerCode = 0x1;
			_bArchiveMode = false;
			_bSporadic = true;
			_curWorks.Clear();

			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			// пишем время
			DateTime time = DateTime.Now;
			writer.Write((UInt32)Afx.CTime.ToTimeT(time));

			// вибираем работы на сегодня и позже (чтобы определить перенос)
			SqlCommand sql = new SqlCommand();
			sql.Connection = _sqlConn;
			sql.CommandText = "SELECT ASH_PlanWorks.obj_osn_id, ASH_PlanWorks.obj_kod, ASH_PlanWorks.punkt_id, ASH_PlanWorks.wk_id, ASH_PlanWorks.plan_beg_time, ASH_PlanWorks.plan_end_time"
                + " FROM ASH_PlanWorks --RIGHT JOIN ASH_KTOObjs ON ASH_KTOObjs.obj_osn_id=ASH_PlanWorks.obj_osn_id AND ASH_KTOObjs.obj_id=ASH_PlanWorks.obj_id"
				+ " WHERE ASH_PlanWorks.plan_end_time>@StartTime";

			sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
			sql.Parameters["@StartTime"].Value = DateTime.Today;

			if (_reqSites.Count > 0)
			{
				sql.CommandText += " AND ( ASH_PlanWorks.obj_osn_id=" + _reqSites[0].ToString();
				for (int i = 1; i < _reqSites.Count; i++)
					sql.CommandText += " OR ASH_PlanWorks.obj_osn_id=" + _reqSites[0].ToString();

				sql.CommandText += ")";
			}

			SqlDataReader reader = sql.ExecuteReader();

			int SiteID, ObjID, PunktID, WorkID;
			DateTime BegTime, EndTime;
			while (reader.Read())
			{
				SiteID = reader.GetInt32(0);
				AddSiteToDictionaries((uint)SiteID);
				ObjID = reader.GetInt32(1);
				PunktID = reader.GetInt32(2);
				WorkID = reader.GetInt32(3);

				BegTime = reader.GetDateTime(4);
				EndTime = reader.GetDateTime(5);

				UnificationConcentrator.UniObjWork work = new UnificationConcentrator.UniObjWork();
                work._flag = UnificationConcentrator.UniObjWork.Kind.Add;// добавление работы
				work._siteID = (uint)SiteID;
				work._objectID = (uint)ObjID;
				work._punktID = (uint)PunktID;
				work._workID = (uint)WorkID;
				work._beginTime = BegTime;
				work._endTime = EndTime;
                work._dataTime = EndTime;

				_curAnswerWorks[(uint)SiteID].Enqueue(work);
				_curWorks.Add(work);
			}
			reader.Close();

			// записываем данные в поток
			WriteCurAnswerData(writer);

			/////////////////////////////////////////////
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)_sporadicAnswerCode;
			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();

			/////////////////////////////////////////////
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnPlanWorksTimer);
			_workTimer = new Timer(timerDelegate, null, 60000, 60000);
		}
		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// </summary>
		public void OnPlanWorksTimer(Object stateInfo)
		{
			List<UnificationConcentrator.UniObjWork> newWorks = new List<UnificationConcentrator.UniObjWork>();

			// вибираем работы на сегодня и позже (чтобы определить перенос)
			SqlCommand sql = new SqlCommand();
			sql.Connection = _sqlConn;
			sql.CommandText = "SELECT ASH_PlanWorks.obj_osn_id, ASH_PlanWorks.obj_kod, ASH_PlanWorks.punkt_id, ASH_PlanWorks.wk_id, ASH_PlanWorks.plan_beg_time, ASH_PlanWorks.plan_end_time"
                + " FROM ASH_PlanWorks -- RIGHT JOIN ASH_KTOObjs ON ASH_KTOObjs.obj_osn_id=ASH_PlanWorks.obj_osn_id AND ASH_KTOObjs.obj_id=ASH_PlanWorks.obj_id"
				+ " WHERE ASH_PlanWorks.plan_end_time>@StartTime";

			sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
			sql.Parameters["@StartTime"].Value = DateTime.Today;

			if (_reqSites.Count > 0)
			{
				sql.CommandText += " AND ( ASH_PlanWorks.obj_osn_id=" + _reqSites[0].ToString();
				for (int i = 1; i < _reqSites.Count; i++)
					sql.CommandText += " OR ASH_PlanWorks.obj_osn_id=" + _reqSites[0].ToString();

				sql.CommandText += ")";
			}

			SqlDataReader reader = sql.ExecuteReader();

			int SiteID, ObjID, PunktID, WorkID;
			DateTime BegTime, EndTime;
			while (reader.Read())
			{
				SiteID = reader.GetInt32(0);
				AddSiteToDictionaries((uint)SiteID);
				ObjID = reader.GetInt32(1);
				PunktID = reader.GetInt32(2);
				WorkID = reader.GetInt32(3);

				BegTime = reader.GetDateTime(4);
				EndTime = reader.GetDateTime(5);

				UnificationConcentrator.UniObjWork work = new UnificationConcentrator.UniObjWork();
                work._flag = UnificationConcentrator.UniObjWork.Kind.Add;// добавление работы
				work._siteID = (uint)SiteID;
				work._objectID = (uint)ObjID;
				work._punktID = (uint)PunktID;
				work._workID = (uint)WorkID;
				work._beginTime = BegTime;
				work._endTime = EndTime;
                work._dataTime = EndTime;

				newWorks.Add(work);
			}

			reader.Close();
			////////////////////////////////////////////////
			// сравниваем новый перечень работ со старым
			foreach (UnificationConcentrator.UniObjWork newWork in newWorks)
			{
				for(int i = 0; i < _curWorks.Count; i++)
				{
					if (newWork._workID == _curWorks[i]._workID )
					{
						// если изменилось время работы, то добавляем в очередь данных
						if (newWork._beginTime != _curWorks[i]._beginTime || newWork._endTime != _curWorks[i]._endTime)
							_dataQueue.Enqueue(newWork);
						
						// удаляем из старых работ (обработали)
						_curWorks.RemoveAt(i);
						
						break;
					}
				}
			}
			// передаем как удаленные все оставшиеся в _curWorks работы 
			foreach (UnificationConcentrator.UniObjWork oldWork in _curWorks)
			{
                oldWork._flag = UnificationConcentrator.UniObjWork.Kind.Remove;// удаление
				_dataQueue.Enqueue(oldWork);
			}

			// запоминаем последний перечень
			_curWorks = newWorks;
		}

		#endregion

		#region Подключение к БД
		/// <summary>
		/// соединение с БД
		/// </summary>
		protected bool Connect()
		{
			return _dbconnector.Connect();
		}
		/// <summary>
		/// Закрыть соединение
		/// </summary>
		protected void Disconnect()
		{
			// закываем соединение
			_dbconnector.Disconnect();
		}
		/// <summary>
		/// Открыто ли соединение
		/// </summary>
		protected bool IsConnected
		{
			get
			{
				return _dbconnector.IsConnected;
			}
		}

		#region Данные
		// параметры соединения с БД
		DBConnector _dbconnector = null;

		/// <summary>
		/// Соединение с БД
		/// </summary>
		private SqlConnection _sqlConn
		{
			get { return _dbconnector.SqlConn; }
		}

		#endregion

		#endregion

		#region Данные

		/// <summary>
		/// начало требуемого интервала
		/// </summary>
		protected DateTime _startArhTime;
		/// <summary>
		/// конец требуемого интервала
		/// </summary>
		protected DateTime _endArhTime;
		/// <summary>
		/// Таймер получения фактов работ
		/// </summary>
		protected Timer _workTimer = null;
		/// <summary>
		/// последний максмимальный идентификатор работы
		/// </summary>
		protected Int64 _maxFactWorkID = 0;
		/// <summary>
		/// перечень текущих работ
		/// </summary>
		protected List<UnificationConcentrator.UniObjWork> _curWorks = new List<UnificationConcentrator.UniObjWork>();
		#endregion
	}

	/// <summary>
	/// Фабрика соединений по унифицированному протоколу второй редакции
	/// </summary>
	public class UnificDBSrvConnFactory : ConnectionFactory
	{
		/// <summary>
		/// констркутор
		/// </summary>
		public UnificDBSrvConnFactory()
		{
		}
		/// <summary>
		/// Создание серверного соединения
		/// </summary>
		/// <returns></returns>
		public override AConnection CreateConnection()
		{
			return new UnificDBSrvConnection(_dbconnector);
		}
		/// <summary>
		/// Загрузка параметров
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(AfxEx.DlxLoader Loader)
		{
			base.Load(Loader);

			// загрузка данных о БД
			_dbconnector.Load(Loader);
		}

		// параметры соединения с БД
		DBConnector _dbconnector = new DBConnector();
	}
}
