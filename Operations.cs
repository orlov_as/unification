﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AfxEx;
using Ivk.IOSys;
using LibEx;
using Tdm;
using Dlx.Extensions;

namespace Tdm.Unification
{
    #region Операции
    /// <summary>
    /// Подзадача контроля
    /// </summary>
    public class ControlSubTask : IOSignalBase, ISigDiscreteLogic
    {
        /// <summary>
        /// Значение сигнала в строковом виде в стиле пс ввода (IoFrm)
        /// </summary>
        public override string SignalStateIoFrmStyle
        {
            get
            {
                try
                {
                    return State2String(State, true);
                }
                catch (Exception ex)
                {
                    return "-exception-";
                }
            }
        }
        /// <summary>
        /// Операции над сигналами
        /// </summary>
        public enum Operations
        {
            /// <summary>
            /// 1 сигнал без преобразований
            /// </summary>
            op_1_1 = 1,
            /// <summary>
            /// 1 соотв. миганию исходного сигнала ("*зри"/ иск разд по миганию)
            /// </summary>
            op_1_M = 2,
            /// <summary>
            /// исп. 2 сигнала с правилом "или" (замыкание = "*чи" или "*ни")
            /// </summary>
            op_2_1_OR_1 = 3,
            /// <summary>
            /// исп. 2 сигнала с правилом "и"
            /// </summary>
            op_2_1_AND = 4,
            /// <summary>
            /// исп. 2 сигнала (по миганию) с правилом "или" (Искусств. Разд. = "*К2" или "*Б2")
            /// </summary>
            op_2_M_OR = 5,
            /// <summary>
            /// исп. 2 сигнала (по миганию) с правилом "и"
            /// </summary>
            op_2_M_AND = 6,
            /// <summary>
            /// исп. 2 сигнала с правилом "или" (замыкание = "*чи" или "*ни")
            /// </summary>
            op_2_1_OR_2 = 7,
            /// <summary>
            /// исп. 2 сигнала с правилом "или" (замыкание = "*чи" или "*ни")
            /// </summary>
            op_2_OR_AC = 8,
            /// <summary>
            /// исп. 2 сигнала с правилом "или" (замыкание = "*чи" или "*ни")
            /// </summary>
            op_2_1_OR_3 = 9,
            /// <summary>
            /// 1 сигнал с инверсией
            /// </summary>
            op_1_1_I = 10,
            /// <summary>
            /// 1 соотв. НЕмиганию исходного сигнала ("*зри"/ иск разд по миганию)
            /// </summary>
            op_1_M_I = 11,
            /// <summary>
            /// операции "Ответвление"
            /// </summary>
            op_branchA1 = 12,
            op_branchB2 = 13,
            op_branchC3 = 14,
            op_branchD4 = 15,
            /// <summary>
            /// КПТШ
            /// </summary>
            op_1_KPTSH = 16,
            /// <summary>
            /// Аналоговое значение из 8 дискретных сигналов
            /// </summary>
            op_8_Value = 18,
            /// <summary>
            /// Дискретный по аналоговому
            /// </summary>
            op_1_DCByAC = 19,
            #region Комбинации
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_00 = 20,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_01 = 21,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_10 = 23, // отдел Т не смог посчитать от 00 до 11 без ошибки ;-)
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_11 = 22, // отдел Т не смог посчитать от 00 до 11 без ошибки ;-)
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_000 = 24,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_001 = 25,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_010 = 26,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_011 = 27,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_100 = 28,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_101 = 29,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_110 = 30,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_111 = 31,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0000 = 32,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0001 = 33,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0010 = 34,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0011 = 35,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0100 = 36,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0101 = 37,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0110 = 38,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_0111 = 39,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1000 = 40,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1001 = 41,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1010 = 42,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1011 = 43,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1100 = 44,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1101 = 45,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1110 = 46,
            /// <summary>
            /// Проверка соответствия значения сигнала определенной комбинации битов
            /// </summary>
            op_1_Combi_1111 = 47,
            #endregion
            /// <summary>
            /// 1 соотв. миганию исходного сигнала (режимы оптической сигнализации в тоннеле)
            /// </summary>
            op_1_M_Tunnel = 48,
            /// <summary>
            /// 
            /// </summary>
            op_1_1_original = 49,
            /// <summary>
            /// Аналоговое значение из 10 дискретных сигналов
            /// </summary>
            op_10_Value = 80
        }
        /// <summary>
        /// Имя атрибута со ссылкой на сигнал
        /// </summary>
        protected const string _refAttrName = "Ref";
        /// <summary>
        /// Имя атрибута с кодом операции
        /// </summary>
        protected const string _opCodeAttrName = "Operation";
        /// <summary>
        /// Имя атрибута с состоянием сигнала
        /// </summary>
        protected const string _stateAttrName = "State";

        #region Members, Properties
        /// <summary>
        /// Операция над сигналами подзадачи контроля
        /// </summary>
        protected BaseOp _op;
        /// <summary>
        /// Сигналы подзадачи контроля
        /// </summary>
        protected List<IOSignal> _signals = new List<IOSignal>();
        /// <summary>
        /// Сосотояния сигналов подзадачи контроля
        /// </summary>
        protected List<bool> _states = new List<bool>();
        #endregion

        /// <summary>
        /// Создание объекта операции
        /// </summary>
        /// <param name="opCode">Код операции из типовой части ИО</param>
        /// <returns>Объект операции</returns>
        public static BaseOp GetOperation(Operations opCode)
        {
            BaseOp op = null;
            if (opCode == Operations.op_1_1)
                op = new Op_1_1();
            else if (opCode == Operations.op_1_M)
                op = new Op_1_M();
            else if (opCode == Operations.op_2_1_OR_1
                  || opCode == Operations.op_2_1_OR_2
                  || opCode == Operations.op_2_1_OR_3)
                op = new Op_2_1_OR();
            else if (opCode == Operations.op_2_1_AND)
                op = new Op_2_1_AND();
            else if (opCode == Operations.op_2_M_OR)
                op = new Op_2_M_OR();
            else if (opCode == Operations.op_2_M_AND)
                op = new Op_2_M_AND();
            else if (opCode == Operations.op_2_OR_AC)
                op = new Op_2_OR_AC();
            else if (opCode == Operations.op_1_1_I)
                op = new Op_1_1_I();
            else if (opCode == Operations.op_1_M_I)
                op = new Op_1_M_I();
            else if (opCode == Operations.op_8_Value)
                op = new Op_1_Value();
            else if (opCode == Operations.op_1_DCByAC)
                op = new Op_1_DCByAC();
            else if (opCode >= Operations.op_1_Combi_00 && opCode <= Operations.op_1_Combi_1111)
                op = Op_1_Combi.Parse(opCode.ToString());
            else if (opCode == Operations.op_1_M_Tunnel)
                op = new Op_1_M_Tunnel();
            else if (opCode == Operations.op_1_1_original)
                op = new Op_1_1_original();
            else if (opCode == Operations.op_10_Value)
                op = new Op_1_Value10();
            return op;
        }

        #region Dlx Members
        /// <summary>
        /// Загрузка из xml-конфигурации
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            #region Загрузка сигналов подзадачи контроля
            // Загрузка сигнала из атрибута
            int opCode = AddSignalToList(Loader, Loader);
            
            // Загрузка сигналов из вложенных элементов
            DlxLoader subLoader = new DlxLoader();
            subLoader.CurXmlElement = Loader.CurXmlElement;
            List<XmlElement> elems;
            subLoader.CurXmlElement.EnumInhElements(out elems);
            foreach (XmlElement elem in elems)
            {
                subLoader.CurXmlElement = elem;
                int opc = AddSignalToList(subLoader, Loader);
                if (opCode != 0 && opCode != opc)
                {
                    string msg = string.Format("Different operation codes inside subtask {0}!", this.Name);
                    Console.WriteLine(msg);
                    throw new ArgumentException(msg);
                }
                opCode = opc;
            }
            #endregion

            #region Инициализация операции
            _op = GetOperation((Operations)opCode);
            if (_op != null)
                _op.SetSignals(_signals.ToArray(), _states.ToArray());
            #endregion
        }

        /// <summary>
        /// Загрузка сигнала по ссылке из атрибута Ref, и его состояния из атрибута State
        /// </summary>
        /// <param name="subLdr">XML-загрузчик</param>
        protected int AddSignalToList(DlxLoader subLdr, DlxLoader ldr)
        {
            string sRef = string.Empty;
            int opCode = 0;
            if (subLdr.GetAttributeValue(_refAttrName, ref sRef))
            {
                IOSignal sig = ldr.GetObject(sRef, typeof(IOSignal)) as IOSignal;
                if (sig != null)
                {
                    _signals.Add(sig);
                    _states.Add(subLdr.GetAttributeInt(_stateAttrName, 1) == 1);

                    opCode = subLdr.GetAttributeInt(_opCodeAttrName, 0);
                }
            }
            return opCode;
        }
        #endregion

        #region ISigDiscreteLogic Members
        /// <summary>
        /// Признак "мигание"
        /// </summary>
        public bool Blink
        {
            get { return _op.Blink; }
        }
        /// <summary>
        /// Признак "состояние сброшено"
        /// </summary>
        public bool Off
        {
            get { return _op.Off; }
        }
        /// <summary>
        /// Признак "состояние установлено"
        /// </summary>
        public bool On
        {
            get { return _op.On; }
        }
        /// <summary>
        /// Перечень сигналов подзадачи контроля
        /// </summary>
        public IOSignal[] IOSignals
        {
            get { return _op.IOSignals; }
        }
        #endregion

        #region IOSinalBase implemented members
        public override IOModule GetOwnerModule()
        {
            return _op.GetOwnerModule();
        }
        public override string SignalState
        {
            get { return _op.SignalState; }
        }
        public override bool IsDataAvailable
        {
            get { return _op.IsDataAvailable; }
        }
        public override uint State
        {
            get { return _op.State; }
            set { _op.State = value; }
        }
        public override bool IsErrorState
        {
            get { return _op.IsErrorState; }
        }
        public override bool IsChangedState
        {
            get { return _op.IsChangedState; }
        }
        public override uint StateBitCount
        {
            get { return _op.StateBitCount; }
            set { _op.StateBitCount = value; }
        }
        #endregion
    }
    
    /// <summary>
    /// Сигнал-операнд
    /// </summary>
    public class Operand : IOSignalBase
    {
        /// <summary>
        /// Значение сигнала в строковом виде в стиле пс ввода (IoFrm)
        /// </summary>
        public override string SignalStateIoFrmStyle
        {
            get
            {
                try
                {
                    return State2String(State, true);
                }
                catch (Exception ex)
                {
                    return "-exception-";
                }
            }
        }
        /// <summary>
        /// Сигнал
        /// </summary>
        protected IOSignal _sig;
        /// <summary>
        /// Текст исключения при попытке задать значение сигнала через мутатор св-ва операции
        /// </summary>
        public const string Exc_SetSignalOverOperation = "Установка значения сигнала через св-во State операции недопустима!";

        public Operand(IOSignal Signal) { _sig = Signal; }

        #region IOSinalBase implemented members
        public override IOModule GetOwnerModule()
        {
            return _sig == null ? null : _sig.GetOwnerModule();
        }
        public override string SignalState
        {
            get { return _sig == null ? string.Empty : _sig.SignalState; }
        }
        public override bool IsDataAvailable
        {
            get { return _sig == null ? false : _sig.IsDataAvailable; }
        }
        public override uint State
        {
            get { return _sig == null ? 0 : _sig.State; }
            set { throw DlxExceptionCreator.ArgumentException(this, null, Exc_SetSignalOverOperation); }
        }
        public override bool IsErrorState
        {
            get { return _sig == null ? true : _sig.IsErrorState; }
        }
        public override bool IsChangedState
        {
            get { return _sig == null ? false : _sig.IsChangedState; }
        }
        public override uint StateBitCount
        {
            get { return _sig == null ? 0 : _sig.StateBitCount; }
            set { throw DlxExceptionCreator.ArgumentException(this, null, Exc_SetSignalOverOperation); }
        }
        #endregion
    }
    /// <summary>
    /// Дискретный сигнал-операнд
    /// </summary>
    public class DiscreteOperand : Operand, ISigDiscreteLogic
    {
        /// <summary>
        /// Сигнал
        /// </summary>
        protected IODiscreteSignal _dsig;
        /// <summary>
        /// Признак инверсии
        /// </summary>
        protected bool _inv;

        public DiscreteOperand(IOSignal Signal, bool State) : base (Signal)
        {
            _dsig = Signal as IODiscreteSignal;
            _inv = !State;
        }

        #region ISigDiscreteLogic Members
        public virtual bool On
        {
            get { return _dsig.On ^ _inv; }
        }
        public virtual bool Off
        {
            get { return !On; }
        }
        public virtual bool Blink
        {
            get { return false; }
        }
        public virtual IOSignal[] IOSignals
        {
            get { return new IOSignal[] { _sig }; }
        }
        #endregion
    }
    /// <summary>
    /// Аналоговый сигнал-операнд
    /// </summary>
    public class AnalogOperand : Operand, ISigAnalog
    {
        protected IOAnalogSignal _asig;

        public AnalogOperand(IOSignal Signal) : base(Signal)
        {
            _asig = _sig as IOAnalogSignal;
        }

        public double Double
        {
            get { return _asig.Double; ; }
        }
        public uint UInt32
        {
            get { return _asig.UInt32; }
        }
        public IOSignal[] IOSignals
        {
            get { return new IOSignal[] { _sig }; }
        }
    }
    
    /// <summary>
    /// Базовый класс операции
    /// </summary>
    public class BaseOp : IOSignalBase, ISigDiscreteLogic
    {
        /// <summary>
        /// Значение сигнала в строковом виде в стиле пс ввода (IoFrm)
        /// </summary>
        public override string SignalStateIoFrmStyle
        {
            get
            {
                try
                {
                    return State2String(State, true);
                }
                catch (Exception ex)
                {
                    return "-exception-";
                }
            }
        }
        /// <summary>
        /// Текст исключения при попытке задать значение сигнала через мутатор св-ва операции
        /// </summary>
        public const string Exc_SetSignalOverOperation = "Установка значения сигнала через св-во State операции недопустима!";
        /// <summary>
        /// Кол-во операндов
        /// </summary>
        protected virtual int _sigsCnt { get { return 0; } }
        /// <summary>
        /// Сигналы подзадачи с учетом заданного в конфигурации состояния State (монтажной инверсии)
        /// </summary>
        protected Operand[] _operands;

        public Operand FirstOp { get { return _operands != null && _operands.Length >= 1 ? _operands[0] : new Operand(null); } }
        public Operand SecondOp { get { return _operands != null && _operands.Length >= 2 ? _operands[1] : new Operand(null); } }

        /// <summary>
        /// Задать сигналы подзадачи
        /// </summary>
        /// <param name="sigs"></param>
        public virtual void SetSignals(IOSignal[] sigs, bool[] states)
        {
            if (sigs == null || sigs.Length != _sigsCnt)
                throw new ArgumentNullException(string.Format("Wrong number of signals ({0}) for {1}-nary operation!", sigs.Length, _sigsCnt));
            IOSignals = new IOSignal[sigs.Length];
            sigs.CopyTo(IOSignals, 0);
        }

        #region ISigDiscreteLogic Members
        public virtual bool On
        {
            get { return false; }
        }
        public virtual bool Off
        {
            get { return !On; }
        }
        public virtual bool Blink
        {
            get { return false; }
        }
        public virtual IOSignal[] IOSignals
        {
            get;
            protected set;
        }
        #endregion

        #region IOSinalBase implemented members
        public override IOModule GetOwnerModule()
        {
            return FirstOp.GetOwnerModule();
        }

        public override string SignalState
        {
            get { return FirstOp.SignalState; }
        }

        public override bool IsDataAvailable
        {
            get { return FirstOp.IsDataAvailable; }
        }

        public override uint State
        {
            get { return (uint)(On ? 1 : 0); }
            set { throw DlxExceptionCreator.ArgumentException(this, null, Exc_SetSignalOverOperation); }
        }

        public override bool IsErrorState
        {
            get { return FirstOp.IsErrorState; }
        }

        public override bool IsChangedState
        {
            get { return FirstOp.IsChangedState; }
        }

        public override uint StateBitCount
        {
            get { return FirstOp.StateBitCount; }
            set { throw DlxExceptionCreator.ArgumentException(this, null, Exc_SetSignalOverOperation); }
        }
        #endregion
    }
    /// <summary>
    /// Операция с дискретным(и) сигнал(ом/ами)
    /// </summary>
    public abstract class DiscreteOp : BaseOp
    {
        /// <summary>
        /// Задать сигналы подзадачи
        /// </summary>
        /// <param name="sigs"></param>
        public override void SetSignals(IOSignal[] sigs, bool[] states)
        {
            base.SetSignals(sigs, states);
            
            _operands = new Operand[sigs.Length];
            for (int i = 0; i < sigs.Length; i++)
            {
                if (sigs[i] is IODiscreteSignal)
                    _operands[i] = new DiscreteOperand(sigs[i], states[i]);
                else
                    throw new ArgumentNullException(string.Format("Wrong type of signal {0} for DiscreteOperand!", sigs[i].FullName));
            }
        }
    }
    /// <summary>
    /// Операция с аналоговым(и) сигнал(ом/ами)
    /// </summary>
    public abstract class AnalogOp : BaseOp
    {
        /// <summary>
        /// Задать сигналы подзадачи
        /// </summary>
        /// <param name="sigs"></param>
        public override void SetSignals(IOSignal[] sigs, bool[] states)
        {
            base.SetSignals(sigs, states);

            _operands = new Operand[sigs.Length];
            for (int i = 0; i < sigs.Length; i++)
            {
                if (sigs[i] is IOAnalogSignal)
                    _operands[i] = new AnalogOperand(sigs[i]);
                else
                    throw new ArgumentNullException(string.Format("Wrong type of signal {0} for AnalogOperand!", sigs[i].FullName));
            }
        }
    }
    /// <summary>
    /// Унарная операция над дискретным сигналом
    /// </summary>
    public abstract class UnaryDiscreteOp : DiscreteOp
    {
        /// <summary>
        /// Кол-во операндов
        /// </summary>
        protected override int _sigsCnt { get { return 1; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public DiscreteOperand Sig { get { return (DiscreteOperand)FirstOp; } }
    }
    /// <summary>
    /// Унарная операция над аналоговым сигналом
    /// </summary>
    public abstract class UnaryAnalogOp : AnalogOp
    {
        /// <summary>
        /// Кол-во операндов
        /// </summary>
        protected override int _sigsCnt { get { return 1; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public AnalogOperand Sig { get { return (AnalogOperand)FirstOp; } }
    }
    /// <summary>
    /// Бинарная операция над дискретными сигналами
    /// </summary>
    public abstract class BinaryDiscreteOp : DiscreteOp
    {
        /// <summary>
        /// Кол-во операндов
        /// </summary>
        protected override int _sigsCnt { get { return 2; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public DiscreteOperand Sig1 { get { return (DiscreteOperand)FirstOp; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public DiscreteOperand Sig2 { get { return (DiscreteOperand)SecondOp; } }
    }
    /// <summary>
    /// Бинарная операция над аналоговыми сигналами
    /// </summary>
    public abstract class BinaryAnalogOp : AnalogOp
    {
        /// <summary>
        /// Кол-во операндов
        /// </summary>
        protected override int _sigsCnt { get { return 2; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public AnalogOperand Sig1 { get { return (AnalogOperand)FirstOp; } }
        /// <summary>
        /// Сигнал подзадачи контроля
        /// </summary>
        public AnalogOperand Sig2 { get { return (AnalogOperand)SecondOp; } }
    }
    /// <summary>
    /// Операция "один к одному"
    /// </summary>
    public class Op_1_1 : UnaryDiscreteOp
    {
        public override bool On     { get { return Sig.On; } }
    }
    /// <summary>
    /// Операция "один по миганию"
    /// </summary>
    public class Op_1_M : UnaryDiscreteOp
    {
        public override bool On     { get { return Sig.Blink; } }
        public override bool Blink  { get { return On; } }
    }
    /// <summary>
    /// Операция ИЛИ для 2-х дискретных сигналов
    /// </summary>
    public class Op_2_1_OR : BinaryDiscreteOp
    {
        public override bool On     { get { return Sig1.On || Sig2.On; } }
    }
    /// <summary>
    /// Операция И для 2-х дискретных сигналов
    /// </summary>
    public class Op_2_1_AND : BinaryDiscreteOp
    {
        public override bool On     { get { return Sig1.On && Sig2.On; } }
    }
    /// <summary>
    /// Операция ИЛИ для определения мигания одного из 2-х дискретных сигналов
    /// </summary>
    public class Op_2_M_OR : BinaryDiscreteOp
    {
        public override bool On     { get { return Sig1.Blink || Sig2.Blink; } }
        public override bool Blink  { get { return On; } }
    }
    /// <summary>
    /// Операция И для определения мигания 2-х дискретных сигналов
    /// </summary>
    public class Op_2_M_AND : BinaryDiscreteOp
    {
        public override bool On     { get { return Sig1.Blink && Sig2.Blink; } }
        public override bool Blink  { get { return On; } }
    }
    /// <summary>
    /// Операция ИЛИ для определения значения одного из 2-х аналоговых сигналов
    /// </summary>
    public class Op_2_OR_AC : BinaryAnalogOp
    {
        
    }
    /// <summary>
    /// 1 сигнал с инверсией
    /// </summary>
    public class Op_1_1_I : UnaryDiscreteOp
    {
        public override bool On     { get { return Sig.Off; } }
    }
    /// <summary>
    /// 1 соотв. НЕмиганию исходного сигнала ("*зри"/ иск разд по миганию)
    /// </summary>
    public class Op_1_M_I : UnaryDiscreteOp
    {
        public override bool On     { get { return Sig.Blink; } }
        public override bool Blink  { get { return Sig.Blink; } }
    }
    /// <summary>
    /// Значение
    /// </summary>
    public class Op_1_Value : DiscreteOp
    {
        protected override int _sigsCnt { get { return 8; } }

        public override uint State
        {
            get
            {
                return _operands.Length == 8 ?
                    (uint)(_operands[0].State       | _operands[1].State << 1 | _operands[2].State << 2 | _operands[3].State << 3
                         | _operands[4].State << 4  | _operands[5].State << 5 | _operands[6].State << 6 | _operands[7].State << 7) : 0;
            }
        }
    }
    /// <summary>
    /// Значение
    /// </summary>
    public class Op_1_Value10 : DiscreteOp
    {
        protected override int _sigsCnt { get { return 10; } }

        public override uint State
        {
            get
            {
                return _operands.Length == 10 ?
                    (uint)(_operands[0].State | _operands[1].State << 1 | _operands[2].State << 2 | _operands[3].State << 3
                         | _operands[4].State << 4 | _operands[5].State << 5 | _operands[6].State << 6 | _operands[7].State << 7
                         | _operands[8].State << 8 | _operands[9].State << 9) : 0;
            }
        }
    }
    /// <summary>
    /// Дискретный по аналоговому
    /// </summary>
    public class Op_1_DCByAC : UnaryAnalogOp
    {
        public override bool On     { get { return Sig.State > 0; } }
    }
    /// <summary>
    /// Сравнение значения сигнала с заданным значением
    /// </summary>
    public class Op_1_Combi : UnaryDiscreteOp
    {
        /// <summary>
        /// Проверочное значение для сравнения со значением сигнала
        /// </summary>
        protected uint _checkCode = 0;

        public Op_1_Combi(uint CheckCode) { _checkCode = CheckCode; }
        
        /// <summary>
        /// Создание операции с инициализацией её значением, извлеченным из названия комбинации
        /// </summary>
        /// <param name="s">Название комбинации</param>
        /// <returns>Операция сравнения сигнала с заданным значением</returns>
        public static Op_1_Combi Parse(string s)
        {
            string[] parts = s.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            return new Op_1_Combi(Convert.ToUInt32(parts[parts.Length - 1], 2));
        }

        public override bool On     { get { return Sig.State == _checkCode; } }
    }
    /// <summary>
    /// 1 соотв. миганию исходного сигнала (режимы оптической сигнализации в тоннеле (М*))
    /// </summary>
    public class Op_1_M_Tunnel : UnaryDiscreteOp
    {
        
    }
    /// <summary>
    /// 
    /// </summary>
    public class Op_1_1_original : UnaryDiscreteOp
    {
        
    }

    #endregion

    #region Многозначная логика

    /// <summary>
    /// Многозначная дискретная логика
    /// </summary>
    public class MultiDiscreteLogic : IOSignalBase, IDlx, ISigDiscreteLogic
    {
        /// <summary>
        /// Значение сигнала в строковом виде в стиле пс ввода (IoFrm)
        /// </summary>
        public override string SignalStateIoFrmStyle
        {
            get
            {
                try
                {
                    return State2String(State, true);
                }
                catch (Exception ex)
                {
                    return "-exception-";
                }
            }
        }
        /// <summary>
        /// Операнды
        /// </summary>
        protected List<UnaryDiscreteLogic> _logics = new List<UnaryDiscreteLogic>();

        public override IOModule GetOwnerModule()
        {
            return _logics[0].GetOwnerModule();
        }

        /// <summary>
        /// Загрузка
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        public override void Load(DlxLoader Loader)
        {
            string strReference = string.Empty;

            if (Loader.GetAttributeValue("Logic", ref strReference))
            {
                switch (strReference.ToUpper())
                {
                    case "AND":
                        _operation = Operation.And;
                        break;
                    case "OR":
                        _operation = Operation.Or;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("Logic");
                }
            }
            else
                throw new ArgumentNullException("Logic");

            for (int i = 1; i <= 100; i++)
            {
                UnaryDiscreteLogic udl = new UnaryDiscreteLogic();
                try
                {
                    udl.Load(Loader, i.ToString());
                }
                catch { break; }
                _logics.Add(udl);
            }
        }

        /// <summary>
        /// Допустимые операции в бинарной логике
        /// </summary>
        protected enum Operation
        {
            And,
            Or
        }

        /// <summary>
        /// Операция логики
        /// </summary>
        protected Operation _operation;

        protected IOSignal[] _sigs = null;
        #region ISignalUnion Members
        /// <summary>
        /// Сигналы участвующие в объединении
        /// </summary>
        public IOSignal[] IOSignals
        {
            get
            {
                if (_sigs == null)
                {
                    _sigs = new IOSignal[_logics.Count];
                    for (int i = 0; i < _logics.Count; i++)
                        _sigs[i] = _logics[i].IOSignals[0];
                }
                return _sigs;
            }
        }

        #endregion

        #region ISigDiscreteLogic Members

        /// <summary>
        /// Признак состояния "ON" - Вкл.
        /// </summary>
        public bool On
        {
            get
            {
                switch (_operation)
                {
                    case Operation.And:
                        {
                            bool res = true;
                            foreach (UnaryDiscreteLogic u in _logics)
                                res &= u.On;
                            return res;
                        }
                    case Operation.Or:
                        {
                            bool res = false;
                            foreach (UnaryDiscreteLogic u in _logics)
                                res |= u.On;
                            return res;
                        }
                }
                return false;
            }
        }

        /// <summary>
        /// Признак состояния "OFF" - Выкл.
        /// </summary>
        public bool Off
        {
            get
            {
                return !On;
            }
        }

        /// <summary>
        /// Признак состояния "BLINK" - Мигание
        /// </summary>
        public bool Blink
        {
            get
            {
                switch (_operation)
                {
                    case Operation.And:
                        {
                            bool res = true;
                            foreach (UnaryDiscreteLogic u in _logics)
                                res &= u.Blink;
                            return res;
                        }
                    case Operation.Or:
                        {
                            bool res = false;
                            foreach (UnaryDiscreteLogic u in _logics)
                                res |= u.Blink;
                            return res;
                        }
                }
                return false;
            }
        }

        #endregion

        public override string SignalState
        {
            get
            {

                //if (IsErrorState)
                //    return new string(DiscreteSig.ToChar(3), 1);

                //if (On)
                //    return new string(DiscreteSig.ToChar(1), 1);

                //return new string(DiscreteSig.ToChar(0), 1);

                return string.Empty;

            }
        }

        public override bool IsDataAvailable
        {
            get
            {
                foreach (UnaryDiscreteLogic logic in _logics)
                    if (!logic.IsDataAvailable)
                        return false;

                return true;
            }
        }

        public override uint State
        {
            get
            {
                return _logics[0].State;
            }
            set
            {

            }
        }

        public override bool IsErrorState
        {
            get
            {

                foreach (UnaryDiscreteLogic logic in _logics)
                    if (!logic.IsErrorState)
                        return false;

                return true;
            }
        }

        public override bool IsChangedState
        {
            get
            {
                foreach (UnaryDiscreteLogic logic in _logics)
                    if (!logic.IsChangedState)
                        return false;

                return true;
            }
        }

        public override uint StateBitCount
        {
            get
            {
                return _logics[0].StateBitCount;
            }
            set
            {
                foreach (UnaryDiscreteLogic logic in _logics)
                    logic.StateBitCount = value;
            }
        }
    }

    #endregion


}
