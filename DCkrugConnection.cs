﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using AfxEx;
using Ivk.IOSys;
using Tdm;
using Dps.UniRequest;
using Dps.UniTcpIp;

namespace Tdm.Unification
{

	/// <summary>
	/// Класс соединения по TcpIp с использованием нового заголовка
	/// </summary>
	class DCkrugConnection : TcpConnection
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		/// <param name="sock">tcp клиент</param>
		public DCkrugConnection(Socket sock)
			: base(sock, true)
		{
		}

		/// <summary>
		/// Переопределен метод для отправки старого заголовка
		/// </summary>
		/// <param name="Hd">заголовк в формате ДЦ-Юг КРУГ</param>
		protected override void OnSendHeader(Header Hd)
		{
			if (Hd == null)
				throw new ArgumentNullException("Header");
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);

			//Установка глобального taimout
			//_Stream.SetTotalTimeout(_Timeout);
			_Stream.WriteTimeout = _Timeout;

			Writer.Write((UInt16)0xAA55);
			Writer.Write((UInt16)Hd.Size + 4);
		}
		/// <summary>
		/// Переопределен метод для получения заголовка формате ДЦ-Юг КРУГ
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();

			//Установка глобального taimout
			//if (IsPartMessage || (Reader == null))
			//	_Stream.SetTotalTimeout(_Timeout);
			_Stream.ReadTimeout = _Timeout;

			UInt16 Signature = Reader.ReadUInt16();
			Hd.Size = Reader.ReadUInt16();
			Hd.Size -= 4;
			Hd.Code = RequestCode.CurrentState;
			
			return Hd;
		}
	}
	/// <summary>
	/// Заголовок по формату обмена ДЦ с Гид Урал
	/// </summary>
	public class GidUralHeader
	{
		/// <summary>
		/// Загрузка заголовка
		/// </summary>
		/// <param name="reader">поток данных</param>
		public void Load(BinaryReader reader)
		{
			_cDcName = System.Text.Encoding.Default.GetString(reader.ReadBytes(12));
			_tModified = Afx.CTime.ToDateTime(reader.ReadInt32());
			_nCountKP = reader.ReadUInt16();
			_nRecLen = reader.ReadUInt16();
			reader.ReadBytes(12);
		}
		/// <summary>
		/// сигнатура ДЦ-источника 
		/// </summary>
		public string _cDcName;
		/// <summary>
		/// дата/время передачи
		/// </summary>
		public DateTime _tModified;
		/// <summary>
		/// число записей 
		/// </summary>
		public UInt16 _nCountKP;
		/// <summary>
		/// длина одной записи GidUralRecord 
		/// </summary>
		public UInt16 _nRecLen;
	}

	/// <summary>
	/// Серверное соединения по протоколу с ДЦ-Юг КРУГ
	/// </summary>
	public class DCkrugServerConnection : ServerConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public DCkrugServerConnection()
		{
			_concentrator = null;
			PrepareAnswer();
		}
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="concentrator">концентратор</param>
		/// <param name="idsysTOrefMAP">словарь станций</param>
		public DCkrugServerConnection(Concentrator concentrator, Dictionary<int, BaseIOSystem> idsysTOrefMAP)
		{
			_concentrator = concentrator;
			_idsysTOrefMAP = idsysTOrefMAP;
			PrepareAnswer();
		}
		/// <summary>
		/// Закрытие соединения
		/// </summary>
		public override void Close()
		{
			foreach (int kp in _kpRecievedList)
				_concentrator.SetData(null, null, DateTime.Now, _idsysTOrefMAP[kp]);

			base.Close();
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			foreach (int kp in _kpRecievedList)
				_concentrator.SetData(null, null, DateTime.Now, _idsysTOrefMAP[kp]);

			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)601, "Закрытие соединения с ДЦ-Юг КП КРУГ: неизвестно состояние " + _kpRecievedList.Count + " станций.");
			base.Dispose(disposing);
		}

		/// <summary>
		/// Обработать запрос
		/// </summary>
		/// <param name="Msg">Обрабатываемое сообщение</param>
		protected override void ExcecuteRequest(Message Msg)
		{
			try
			{
				if (Msg.Code != RequestCode.CurrentState)
					throw new RequestException(ErrorCode.WrongCode);

                BinaryReader reader = new BinaryReader(new MemoryStream(Msg.Data));
				GidUralHeader dataHeader = new GidUralHeader();
				// читаем заголовок
				dataHeader.Load(reader);
				Debug.WriteLine("DCkrugServerConnection: получено сообщение " + dataHeader._cDcName + " по " + dataHeader._nCountKP + " станциям!");
				// проверяем размеры
				if (Msg.Data.Length < 32 + dataHeader._nRecLen * dataHeader._nCountKP)
					throw new RequestException(ErrorCode.WrongFormat);

				int kpID;
				DateTime kpTime;
				byte[] data;
				BaseIOSystem iosys;
				// читаем данные по каждой станции
				for (int i = 0; i < dataHeader._nCountKP; i++)
				{
					// идентификатор станции
					kpID = reader.ReadInt32();
					// время данных
					kpTime = Afx.CTime.ToDateTime(reader.ReadInt32());
					// резерв
					reader.ReadBytes(8);
					// данные ТС
					data = reader.ReadBytes(dataHeader._nRecLen - 16);
					// если есть такая подсистема, то передаем коллектору
					if (_idsysTOrefMAP.TryGetValue(kpID, out iosys))
					{
						Debug.WriteLine("\t станция=" + iosys.Owner.Name + " time=" + kpTime.ToString("yyyy:MM:dd HH:mm:ss"));
						_concentrator.SetData(data, null, kpTime, iosys);
                        // добавляем в перечень станций
						if (_addKpIDtoList && !_kpRecievedList.Contains(kpID) )
							_kpRecievedList.Add(kpID);
					}
				}
				_addKpIDtoList = false;
			}
			catch (Exception ex)
			{
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)602, "Ошибка обаботки данных от ДЦ-Юг КП КРУГ: " + ex.Message);
				throw new Exception(string.Empty);
			}
		}
		/// <summary>
		/// получить ответ
		/// </summary>
		/// <returns>сообщение ответа</returns>
		protected override Message GetAnswer()
		{
			// возвращаем подготовленный ответ
			return _Answer;
		}
		/// <summary>
		/// Подготовка ответа
		/// </summary>
		protected virtual void PrepareAnswer()
		{
			_Answer = new Message();
			_Answer.Code = RequestCode.CurrentState;
		}

		#region Данные
		/// <summary>
		/// ссылка на концентратор, который обрабатывает данные
		/// </summary>
		protected Concentrator _concentrator = null;
		/// <summary>
		/// Заранее подготовленный ответ
		/// </summary>
		protected Message _Answer = null;
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<int, BaseIOSystem> _idsysTOrefMAP = null;
		/// <summary>
		/// Список станций, по которым приходили данные
		/// </summary>
		protected List<int> _kpRecievedList = new List<int>();
		/// <summary>
		/// Признак необходимости добавить в список КП, по которым есть данные
		/// </summary>
		protected bool _addKpIDtoList = true;

		#endregion
	}

	/// <summary>
	/// Фабрика серверных соединений по протоколу ДЦ-Юг КРУГ
	/// </summary>
	public class DCkrugServerConnectionFactory : UnificServerConnectionFactory
	{
		/// <summary>
		/// констркутор
		/// </summary>
		public DCkrugServerConnectionFactory()
		{
		}
		/// <summary>
		/// Загрузка соответствия идентификаторов ДЦ-Юг КП КРУГ
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			List<XmlElement> kpAccordance;
			Loader.CurXmlElement.EnumInhElements(out kpAccordance, "kp", null, null);
			int kpid = 0;
			string sitepath;
			foreach (XmlElement elem in kpAccordance)
			{
				sitepath = string.Empty;
				if (elem.GetAttributeValue("kpid", ref kpid) && elem.GetAttribute("sitepath", out sitepath))
					_idsysTOpathMAP.Add(kpid, sitepath);
			}
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>true при удачной инициализации</returns>
		public override void Create()
		{
			DlxObject obj;
			foreach (int kpid in _idsysTOpathMAP.Keys)
			{
				obj = _siteList.FindObject(_idsysTOpathMAP[kpid] + "/IOSys" );
				if (obj != null && obj is BaseIOSystem)
					_idsysTOrefMAP.Add(kpid, obj as BaseIOSystem);
				else
				{
					throw new ArgumentException("DCkrugServerConnectionFactory " + Name + ": Can't find site by name " + _idsysTOpathMAP[kpid]);
				}
			}
			base.Create();
		}
		/// <summary>
		/// Создание серверного соединения
		/// </summary>
		/// <returns>интерфейс соединения</returns>
		public override AConnection CreateConnection()
		{
			return new DCkrugServerConnection(_concentrator, _idsysTOrefMAP);
		}
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<int, BaseIOSystem> _idsysTOrefMAP = new Dictionary<int, BaseIOSystem>();
		/// <summary>
		/// MAP путей к станциям
		/// </summary>
		private Dictionary<int, string> _idsysTOpathMAP = new Dictionary<int, string>();
	}

	/// <summary>
	/// Сервер обмена с ДЦ-Юг КП Круг
	/// </summary>
	public class DCkrugServer : Server
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		public DCkrugServer()
		{
		}
		/// <summary>
		/// Создание соединения
		/// </summary>
		/// <param name="client">tcp клиент</param>
		/// <returns>соединение</returns>
		public override AConnection CreateConnection(Socket client)
		{
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)600, "Подключение ДЦ-Юг КП КРУГ с удаленного адреса " + IPAddress.Parse(((IPEndPoint)client.RemoteEndPoint).Address.ToString()));
			// создаем соединение унификации
			return new DCkrugConnection(client);
		}
	}

	/// <summary>
	/// Подсистема ввода ДЦ (нет линий - сразу состоит из модулей дискретных сигналов)
	/// </summary>
	public class DCIOSys : IOSystem
	{
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>true - в случае успеха</returns>
		public override void Create()
		{
            base.Create();

			GeneralState.Current = IOGeneralState.Unknown;
			SetSubsToUnknownState();
			IsValidData = false;
		}

		/// <summary>
		/// Обработка данных подсистемы, формата текущего ДЦ
		/// </summary>
		/// <param name="reader">читатель</param>
		/// <exception cref="System.Exception">Выбрасывается при прочтении некорректных данных</exception>
		public override void ProcessData(BinaryReader reader)
		{
			//Каждая подмодуль обрабатывает свои данные
			foreach (IOBaseElement elem in SubElements.Objects)
				elem.ProcessData(reader);

			IsValidData = false;
		}
	}
	/// <summary>
	/// Класс модуля ДЦ
	/// </summary>
	public class DCModule : IOSigModule
	{
        /// <summary>
		/// Переопределена обработка данных
		/// </summary>
		/// <param name="reader"></param>
		public override void ProcessData(BinaryReader reader)
		{
			SetData(_size, reader);
        }
		/// <summary>
		/// Переопределяем распаковку данных
		/// </summary>
		public override void Update()
		{
			int size = 2; // разрядность значения сигнала
			int count = Signals.Objects.Count;         // число сигналов
			int needSize = (size * count) / 8 + ((size * count % 8) > 0 ? 1 : 0);

			if (Data.Length != needSize)
				throw new ArgumentException("DCModule::Update(): неправильный размер данных " + Data.Length + "/" + needSize);

			IOSignal sig;
			for (int i = 0; i < count; i++)
			{
				sig = Signals.Objects[i] as IOSignal;
				if( sig != null )
				{
					sig.StateBitCount = (uint)size;
					sig.State = (uint) ((Data[i / 4] >> 2*(i % 4)) & 0x3);
				}
			}
		}
	}
}
