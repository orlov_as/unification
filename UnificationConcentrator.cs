﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Text;
using AfxEx;
using Ivk.IOSys;
using Dps.AppDiag;
using Dps.UniRequest;

namespace Tdm.Unification
{
	/// <summary>
	/// Унифицированный концентратор
	/// </summary>
	public class UnificationConcentrator : ProcessConcentrator, ISuivComponent
	{
		#region Классы, задающие данные в очереди потребителей

		/// <summary>
		/// Базовый класс единичных данных
		/// </summary>
		public class UniDataItem
		{
			/// <summary>
			/// время соответствующее данным
			/// </summary>
			public DateTime _dataTime;
			/// <summary>
			/// Идентификатор  места
			/// </summary>
			public UInt32 _siteID;
			/// <summary>
			/// Идентификатор объекта
			/// </summary>
			public UInt32 _objectID;
		}
		/// <summary>
		/// унифицированное состояние объекта
		/// </summary>
		public class UniObjState : UniDataItem
		{
			/// <summary>
			/// Унифицированное состояние
			/// </summary>
			public byte _uniState;
			/// <summary>
			/// Предыдущее унифицированное состояние
			/// </summary>
			public byte _uniPrevState;
		}
		/// <summary>
		/// Диагностическое состояние
		/// </summary>
		public class UniDiagState : UniDataItem
		{
			/// <summary>
			/// Унифицированное диагностическое сосотяние
			/// </summary>
			public int _uniDiagState;
			/// <summary>
			/// Время выявления
			/// </summary>
			public DateTime _detectTime;
			/// <summary>
			/// Время окончания
			/// </summary>
			public DateTime _endTime;

			/// <summary>
			/// Сформировать диагностическое состояние на основе данных из таблицы
			/// </summary>
			/// <param name="row">Строка таблицы, на основе которой будет сформированно диагностическое состояние</param>
			/// <param name="EndTime">Время окончания диагностической ситуации</param>
			/// <returns>Сформированная диагностическая ситуация</returns>
			public static UniDiagState CreateUniDiagState(DataRow row, DateTime EndTime)
			{
				UniDiagState diag = new UniDiagState();
				diag._dataTime = (DateTime)row["Time"];
				diag._siteID = (uint)(int)(row["Site_ID"]);
				diag._objectID = (uint)(int)row["Object_ID"];
				diag._uniDiagState = (int)row["State_ID"];
				diag._detectTime = (DateTime)row["Time"];
				diag._endTime = EndTime;
				return diag;
			}


		}
		/// <summary>
		/// Числовой параметр объекта
		/// </summary>
		public class UniParamState : UniDataItem
		{
			/// <summary>
			/// Идентификатор параметра в рамках объекта
			/// </summary>
			public UInt32 _paramID;
		}
		/// <summary>
		/// Числовой параметр объекта
		/// </summary>
		public class UniNumericParamState : UniParamState
		{
			/// <summary>
			/// Время измерения параметра
			/// </summary>
			public DateTime _paramTime;
			/// <summary>
			/// Значение параметра
			/// </summary>
			public float _paramValue;
		}
		/// <summary>
		/// Нечисловой параметр объекта
		/// </summary>
		public class UniDataParamState : UniParamState
		{
			/// <summary>
			/// Значение параметра
			/// </summary>
			public byte[] _paramValue;
		}

		/// <summary>
		/// Работа по объекту
		/// </summary>
		public class UniObjWork : UniDataItem
		{
			/// <summary>
			/// Тип действия над работой
			/// </summary>
			public enum Kind : byte
			{
				/// <summary>
				/// Добавление или перенос работы в план ТО по объекту
				/// </summary>
				Add = 0,
				/// <summary>
				/// Удаление работы из плана ТО
				/// </summary>
				Remove = 1,
				/// <summary>
				/// Факт проведения работы
				/// </summary>
				Complete = 2
			}

			/// <summary>
			/// Действие над работой
			/// </summary>
			public Kind _flag;
			/// <summary>
			/// идентификатор работы
			/// </summary>
			public UInt32 _workID;
			/// <summary>
			/// идентификатор пункта инструкции
			/// </summary>
			public UInt32 _punktID;
			/// <summary>
			/// flag=0: Запланированное время начала работы;
			/// flag=1: Поле игнорируется;
			/// flag=2: Фактическое время начала проведения работы; 
			/// </summary>
			public DateTime _beginTime;
			/// <summary>
			/// flag=0: Запланированное время окончания работы;
			/// flag=1: Поле игнорируется;
			/// flag=2: Фактическое время окончания проведения работы;  
			/// </summary>
			public DateTime _endTime;
			/// <summary>
			/// перечень ситуаций, на осонове которых была выявлена работа
			/// </summary>
			public Dictionary<UInt16, DateTime> _diags;

			/// <summary>
			/// Типовой комментарий к ДС, расцененной как выполнение ТО
			/// </summary>
			public const string Comment = "Выявлен факт проведения технического обслуживания по тех.карте №{0}";

			public string GetComment(int punkt_id)
			{
				string comment = string.Format("Выявлен факт проведения технического обслуживания (пункт инструкции АСУ-Ш-2 №{0})", punkt_id);
				switch(punkt_id)
				{
					case 26781488: comment += "Проверка видимости пригласительного огня (пункт 1.2 ЦШ-720-09)"; break;
					case 26781490: comment += "Смена ламп красных, огней входных, выходных и маршрутных светофоров на главных путях и путях безостановочного пропуска поездов, а также светофоров прикрытия для ламп без контроля переключения на резервную нить (пункт 1.4.1.1 ЦШ-720-09)"; break;
					case 26781491: comment += "Смена ламп красных, огней входных, выходных и маршрутных светофоров на главных путях и путях безостановочного пропуска поездов, а также светофоров прикрытия для ламп с контролем переключения на резервную нить (пункт 1.4.1.2 ЦШ-720-09)"; break;
					case 26781492: comment += "Смена ламп огней проходных светофоров автоблокировки без переключения на резервную нить (пункт 1.4.2.1 ЦШ-720-09)"; break;
					case 26781495: comment += "Смена ламп выходных светофоров с боковых путей, кроме путей, перечисленных в пункте 1.4.1.1; маневровых светофоров, выходных и маршрутных светофоров, огней выходных и маршрутных светофоров без переключения на резервную нить (пункт 1.4.3.1 ЦШ-720-09)"; break;
					case 26781493: comment += "Смена ламп огней проходных светофоров автоблокировки с переключением на резервную нить (пункт 1.4.2.2 ЦШ-720-09)"; break;
					case 26781496: comment += "Смена ламп выходных светофоров с боковых путей, кроме путей, перечисленных в пункте 1.4.1.1; маневровых светофоров, выходных и маршрутных светофоров, огней выходных и маршрутных светофоров с переключением на резервную нить (пункт 1.4.3.2 ЦШ-720-09)"; break;
					case 26781524: comment += "Проверка замыкания остряков стрелки или подвижного сердечника при закладке щупа толщиной 2мм и 4мм (пункт 2.1.2.1 ЦШ-720-09)"; break;
					case 26781709: comment += "Проверка зазора между опорной поверхностью колесосбрасывающего башмака и головкой рельса (пункт 2.1.2.2 ЦШ-720-09)"; break;
					case 26781536: comment += "Измерение напряжения на выводах электродвигателя при работе на фрикцию (пункт 2.1.11 ЦШ-720-09)"; break;
					case 26781529: comment += "Измерение переводных усилий эл.привода при работе эл.двигателя переменного тока на фрикцию (пункт 2.1.6 ЦШ-720-09)"; break;
					case 26781528: comment += "Измерение силы тока электродвигателя постоянного тока при нормальном переводе и работе на фрикцию (пункт 2.1.5 ЦШ-720-09)"; break;
					case 26781668: comment += "Станция стыкования. Проверка переключателей контактной сети (пункт 5.12 ЦШ-720-09)"; break;
					case 26781691: comment += "Осмотр тормозного упора с установкой и снятием колодок, проверка зазора между опорной поверхностью полоза и головкой рельса, вертикальности установки колодок, соосности полоза с продольными осями головок рельсов. Смазка шарнирных соединений рычажного механизма, осей кронштейна с упорами (пункт 15.2 ЦШ-720-09)"; break;
					case 26781544: comment += "Проверка шунтовой чувствительности рельсовых цепей (пункт 3.3.1 ЦШ-720-09)"; break;
					case 26781545: comment += "Проверка станционных однониточных рельсовых цепей на шунтовую чувствительность (пункт 3.3.2 ЦШ-720-09)"; break;
					case 26781516: comment += "Смена ламп накаливания и измерение напряжения на лампах заградительных светофоров: однонитевых и двухнитевых без переключения на резервную нить и не имеющих контроля перегорания у дежурного работника (ДСП, диспетчер ШЧ) (пункт 9.3.1 ЦШ-720-09)"; break;
					case 26781607: comment += "Смена ламп накаливания и измерение напряжения на лампах заградительных светофоров: однонитевых и двухнитевых с переключением на резервную нить и имеющих контроль перегорания у дежурного работника (ДСП, диспетчер ШЧ) (пункт 9.3.2 ЦШ-720-09)"; break;
					case 26781608: comment += "Проверка работоспособности схем контроля сопротивления изоляции цепей питания относительно земли (пункт 10.1.8 ЦШ-720-09)"; break;
					case 26781688: comment += "Проверка работы схемы контроля датчиков УКСПС (пункт 14.4 ЦШ-720-09)"; break;
					case 26781508: comment += "Проверка действия схемы двойного снижения напряжения (пункт 1.12 ЦШ-720-09)"; break;
					case 26781600: comment += "Проверка работы схемы смены направления автоблокировки основным и вспомогательным режимом (пункт 5.2.1 ЦШ-720-09)"; break;
					case 26781639: comment += "Внешний осмотр и чистка ДГА; проверка наличия топлива, уровня масла и воды; пуск ДГА без нагрузки; проверка вырабатываемой частоты и напряжения, действия схемы сигнализации и контроля (пункт 11.4.1 ЦШ-720-09)"; break;
					case 26781642: comment += "ДГА - проверка состояния и пробный запуск ДГА с подключением нагрузки (пункт 11.4.4 ЦШ-720-09)"; break;
					// Пункты работ, выявляемые по триггерам БД ЦДМ
					case 26780085: comment += "Проверка РЦ на шунтовую чувствительность (пункт 4.3 ЦШ-720)"; break;
					case 26780061: comment += "Проверка централизованных стрелок на невозможность их замыкания с применением щупа 4 мм (пункт 3.1.2 ЦШ-720)"; break;
					case 26780023: comment += "Смена ламп линзовых светофоров (пункт 2.3 ЦШ-720)"; break;
					case 26780041: comment += "Смена ламп линзовых светофоров (пункт 2.3 ЦШ-720)"; break;
					case 26780039: comment += "Смена ламп линзовых светофоров (пункт 2.3 ЦШ-720)"; break;
					default:; break;
				}
				return string.Empty;
			}

			/// <summary>
			/// Параметры ДС, отнесенной к ТО
			/// </summary>
			public class RootDiag
			{
				public RootDiag(IDataReader rdr)
				{
					try
					{
						ID = Convert.ToInt64(rdr["ID"]);
						Object_ID = Convert.ToInt32(rdr["Object_ID"]);
						Time = Convert.ToDateTime(rdr["Time"]);
						Comment_DS_ID = rdr.IsDBNull(rdr.GetOrdinal("Comment_DS_ID")) ? -1 : Convert.ToInt64(rdr["Comment_DS_ID"]);
						IsTO = Convert.ToBoolean(rdr["IsTO"]);
					}
					catch { }
				}
				/// <summary>
				/// Получение списка параметров ДС из БД
				/// </summary>
				/// <param name="query">Запрос параметров</param>
				/// <param name="conn">Соединение с БД</param>
				/// <returns>Список параметров ДС</returns>
				public static List<RootDiag> GetDiags(string query, SqlConnection conn)
				{
					List<RootDiag> res = new List<RootDiag>();
					SqlCommand cmd = new SqlCommand(query, conn);
					if(conn.State != ConnectionState.Open)
						conn.Open();
					SqlDataReader rdr = null;
					try
					{
						rdr = cmd.ExecuteReader();
						if(rdr.HasRows)
							while(rdr.Read())
								res.Add(new RootDiag(rdr));
					}
					catch { }
					finally { if(rdr != null) rdr.Close(); }
					return res;
				}
				/// <summary>
				/// Определение ID ДС
				/// </summary>
				/// <param name="diags"></param>
				/// <param name="work"></param>
				/// <param name="stateID"></param>
				/// <returns></returns>
				public static Int64 GetDiagID(List<RootDiag> diags, UniObjWork work, UInt16 stateID)
				{
					List<RootDiag> dd = diags.Where(d => d.Time == work._diags[stateID]).ToList();
					if(dd.Count > 0)
						return dd.Select(d => d.ID).FirstOrDefault();
					else
						return -1;
				}

				/// <summary>
				/// Проверка, все ли ДС в списке относятся к одному объекту
				/// </summary>
				/// <param name="diags"></param>
				/// <param name="Object_ID"></param>
				/// <returns></returns>
				public static bool IsOnlyObject(List<RootDiag> diags, int Object_ID)
				{
					foreach(RootDiag d in diags)
						if(d.Object_ID != Object_ID)
							return false;
					return true;
				}
				/// <summary>
				/// ID ДС
				/// </summary>
				public readonly long ID = 0;
				/// <summary>
				/// ID объекта
				/// </summary>
				public readonly int Object_ID = 0;
				/// <summary>
				/// Время ДС
				/// </summary>
				public readonly DateTime Time = new DateTime();
				/// <summary>
				/// ID ДС в таблице комментариев
				/// </summary>
				protected readonly long Comment_DS_ID = -1;
				/// <summary>
				/// Признак наличия комментария
				/// </summary>
				public bool IsCommented { get { return Comment_DS_ID > -1; } }
				/// <summary>
				/// Признак ТО
				/// </summary>
				public readonly bool IsTO = false;
			}
		}
		/// <summary>
		/// Класс - данные с подсистемой ввода
		/// </summary>
		public class UniDataWithObjSys
		{
			/// <summary>
			/// Конструктор
			/// </summary>
			/// <param name="data">данные</param>
			/// <param name="ObjsOwner">подсистема</param>
			public UniDataWithObjSys(UniDataItem data, UniObjSystem objSys)
			{
				_data = data;
				_objSys = objSys;
			}
			/// <summary>
			/// Данные
			/// </summary>
			public UniDataItem _data;
			/// <summary>
			/// подсистема
			/// </summary>
			public UniObjSystem _objSys;
		}

		#endregion
		/// <summary>
		/// Загрузка параметров
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			if(Loader.CurName != "/App/Concentrator" && Loader.GetAttributeBool("Reg", false))
				Loader.MapObject("/App/Concentrator", this);
			base.Load(Loader);
        }

        public override void Create()
        {
            base.Create();

            base.SendState += SendState;
            base.SendAllSitesState += SendAllSitesState;
        }

        /// <summary>
        /// Обработка данных
        /// </summary>
        /// <param name="data">данные</param>
        protected override void OnData(Data data)
		{
			if(data._targetElement == null && data._dataObj != null && !(data._dataObj is string))
			{
				List<UniDataWithObjSys> dataList = data._dataObj as List<UniDataWithObjSys>;
				if(dataList != null)
				{
					foreach(UniDataWithObjSys dataWithSys in dataList)
					{
						// блокируем элемент
						Monitor.Enter(dataWithSys._objSys);
						try
						{
							// обновляем время
							UpdateTime(dataWithSys._objSys, data._time);
							// разбор данных
							dataWithSys._objSys.ProcessDataObj(dataWithSys._data);
							SendSiteState(data);
						}
						catch(Exception e)
						{
							Trace.WriteLine("UnificationConcentrator " + Name + ": Ошибка обработки данных для элемента " + dataWithSys._objSys.Owner.Name + "/" + dataWithSys._objSys.Name + ": " + e.ToString());
							Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)202, "Ошибка обработки унифицированных данных для " + dataWithSys._objSys.Owner.Name + "/" + dataWithSys._objSys.Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
							// неизвестное состояние
							dataWithSys._objSys.SetInvalidObjectsState();
							SendSiteState(data, e.Message);
						}
						finally
						{
							Monitor.Exit(dataWithSys._objSys);
						}
					}

					foreach(UniDataWithObjSys dataWithSys in dataList)
						dataWithSys._objSys.SetValidData();

					return;
				}
			}

			// пришла ошибка для подсистемы объектов
			if(data._dataObj != null && data._dataObj is string && data._targetElement != null && typeof(UniObjSystem).IsInstanceOfType(data._targetElement))
			{
				UniObjSystem uniObjs = data._targetElement as UniObjSystem;
				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(uniObjs, data._time);
					// неизвестное состояние
					uniObjs.SetInvalidObjectsState();
					SendSiteState(data, data._dataObj.ToString());
				}
				catch(Exception e)
				{
					Trace.WriteLine("UnificationConcentrator " + Name + ": Ошибка установки неизвестного состояния для элемента " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.ToString());
					// все элементы в неизвестное сосотяние
					uniObjs.SetInvalidObjectsState();
					SendSiteState(data, e.Message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			// есть данные в виде объекта
			else if(data._dataObj != null && data._targetElement != null && typeof(UniObjSystem).IsInstanceOfType(data._targetElement))
			{
				UniObjSystem uniObjs = data._targetElement as UniObjSystem;

				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(uniObjs, data._time);
					// разбор данных
					uniObjs.ProcessDataObj(data._dataObj);
					// известное состояние
					uniObjs.SetValidData();
					SendSiteState(data);
				}
				catch(Exception e)
				{
					Trace.WriteLine("UnificationConcentrator " + Name + ": Ошибка обработки данных для элемента " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.ToString());
					Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)202, "Ошибка обработки унифицированных данных для " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					// неизвестное состояние
					uniObjs.SetInvalidObjectsState();
					SendSiteState(data, e.Message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			// если нет никаких данных => ошибка обмена
			else if(data._dataObj == null && data._data == null && data._targetElement != null && typeof(UniObjSystem).IsInstanceOfType(data._targetElement))
			{
				UniObjSystem uniObjs = data._targetElement as UniObjSystem;
				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(uniObjs, data._time);
					// неизвестное состояние
					uniObjs.SetInvalidObjectsState();
					SendSiteState(data, "Нет данных - ошибка обмена");
				}
				catch(Exception e)
				{
					Trace.WriteLine("UnificationConcentrator " + Name + ": Ошибка установки неизвестного состояния для элемента " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.ToString());
					// все элементы в неизвестное сосотяние
					uniObjs.SetInvalidObjectsState();
					SendSiteState(data, e.Message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			// если данные в виде массива и для DpsOldSubControlObjSystem вложенной станции
			// то обрабатываем
			else if(data._targetElement is DpsOldSubControlObjSystem)
			{
				DpsOldSubControlObjSystem uniObjs = data._targetElement as DpsOldSubControlObjSystem;

				// если данных нет - то ошибка связи, но делать ничего не будем,
				// т.к. сбой будет по подсистеме ввода
				if(data._data == null)
				{
					Trace.WriteLine("ProcessConcentrator " + Name + ": Нет данных о сбоя АДК " + uniObjs.ObjSys.Owner.Name);
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)202, "ProcessConcentrator " + Name + ": Нет данных о сбоя АДК " + uniObjs.ObjSys.Owner.Name);
					return;
				}

				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(uniObjs.ObjSys, data._time);
					// разбор данных
					uniObjs.ProcessData(new BinaryReader(new MemoryStream(data._data)));
					// известное состояние
					uniObjs.ObjSys.SetValidData();
					SendSiteState(data);
				}
				catch(Exception e)
				{
					Trace.WriteLine("UnificationConcentrator " + Name + ": Ошибка обработки данных для элемента " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.ToString());
					Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)202, "Ошибка обработки унифицированных данных для " + uniObjs.Owner.Name + "/" + uniObjs.Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					SendSiteState(data, e.Message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			// есть данные в виде массива - вызываем базовый метод
			else
			{
				base.OnData(data);
			}
		}

		private void SendSiteState(Data data, string message = "")
		{
			string subname = "";
			int siteId;

			if(data._targetElement is DpsOld.IOSysOld)
				subname = "Подсистема ввода";
			else if(data._targetElement is DpsOldControlObjSystem)
				subname = "Подсистема объектов";

			if((data._targetElement as UniObjSystem)?.SiteID == null)
				siteId = (data._targetElement.Owner as Tdm.Site).ID;
			else
				siteId = (int)(data._targetElement as UniObjSystem).SiteID;

			SendState?.Invoke(this, subname, siteId, data._time, message);
		}
		/// <summary>
		/// Обновление зависимых данных
		/// </summary>
		/// <param name="iosys">подсистема, для которого пришли данные</param>
		protected override void UpdateDataBy(BaseIOSystem iosys)
		{
			if(iosys == null)
				return;

			UniObjSystem uniObjs = null;
			if(_updateDependence.TryGetValue(iosys, out uniObjs))
			{
				if(uniObjs == null)
					return;
				Monitor.Enter(uniObjs);
				try
				{
					// обновляем время
					UpdateTime(uniObjs, iosys.DataTime);

					uniObjs.UpdateObjectStates();
					uniObjs.SetValidData();

					// убрали один цикл обработки, т.к. добавили пописку подсистемы объектов на себя
					//uniObjs.UpdateObjectStates();
					//uniObjs.SetValidData();
				}
				finally
				{
					Monitor.Exit(uniObjs);
				}
			}
		}
		/// <summary>
		/// Добавить зависимость обновления
		/// </summary>
		/// <param name="iosys">подсистема ввода</param>
		/// <param name="uniObjs">подсистема объектов</param>
		public void AddUpdateDependence(BaseIOSystem iosys, UniObjSystem uniObjs)
		{
			_updateDependence.Add(iosys, uniObjs);
		}
		/// <summary>
		/// Карта зависимости подсистем для обновления
		/// </summary>
		protected Dictionary<BaseIOSystem, UniObjSystem> _updateDependence = new Dictionary<BaseIOSystem, UniObjSystem>();

		public event Action<DlxObject, string, int, DateTime?, string> SendState;
		public event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;
	}

	/// <summary>
	/// Клиент получения данных по унифицированному протоколу (первая версия протокола)
	/// </summary>
	public class UniOldRequestClient : RequestClientBase
	{
		#region Методы работы на потоке

		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected override void ProcessAnswer()
		{
			Trace.WriteLine("UniOldRequestClient " + Name + ": Получен ответ: размер данных = " + _answerMsg.Data.Length);

			// передаем данные коллектору
			if(_answerMsg.Code == (RequestCode)0x1)// полное состояние
			{
				BinaryReader reader = new BinaryReader(new MemoryStream(_answerMsg.Data));

				Int32 time;
				UInt32 SiteID, ObjID;
				UInt16 ObjCount;
				byte State;

				// считываем время
				time = reader.ReadInt32();
				DateTime dateTime = Afx.CTime.ToDateTime(time);
				// читаем по станциям
				while(reader.BaseStream.Position < reader.BaseStream.Length)
				{
					// считываем идентификатор станции
					SiteID = reader.ReadUInt32();
					IOBaseElement objsSys = GetObjectsSys(SiteID);
					if(objsSys == null)
						continue;
					// считываем количество объектов
					ObjCount = reader.ReadUInt16();
					for(int i = 0; i < ObjCount; i++)
					{
						// считываем идентификатор объекта
						ObjID = reader.ReadUInt32();
						// считываем состояние объекта
						State = reader.ReadByte();

						UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
						state._dataTime = dateTime;
						state._objectID = ObjID;
						state._siteID = SiteID;
						state._uniState = State;

						SetData(state, dateTime, objsSys);
					}
				}

			}
			else if(_answerMsg.Code == (RequestCode)0x2)// изменение
			{
				BinaryReader reader = new BinaryReader(new MemoryStream(_answerMsg.Data));

				Int32 time;
				UInt32 SiteID, ObjID;
				byte State;

				// считываем время
				time = reader.ReadInt32();
				DateTime dateTime = Afx.CTime.ToDateTime(time);
				// читаем по станциям
				while(reader.BaseStream.Position < reader.BaseStream.Length)
				{
					// считываем идентификатор станции
					SiteID = reader.ReadUInt32();
					IOBaseElement objsSys = GetObjectsSys(SiteID);
					if(objsSys == null)
						continue;

					// считываем идентификатор объекта
					ObjID = reader.ReadUInt32();
					// считываем состояние объекта
					State = reader.ReadByte();

					UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
					state._dataTime = dateTime;
					state._objectID = ObjID;
					state._siteID = SiteID;
					state._uniState = State;

					SetData(state, dateTime, objsSys);
				}
			}
		}
		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected override void OnExchengeError()
		{
			// произошла ошибка - пустые данные коллектору
			foreach(UniObjSystem sys in _idsysTOrefMAP.Values)
			{
				if(sys != null)
					SetData(null, DateTime.MinValue, sys);
			}
		}

		#endregion

		/// <summary>
		/// Переопределена загрузка для получения объектов
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			if(_findRoot == null)
			{
				// загружаем ссылку на объекты
				UniObjSystem targetObjects = Loader.GetObjectFromAttribute("Objects") as UniObjSystem;
				if(!_idsysTOrefMAP.ContainsKey(targetObjects.SiteID))
					_idsysTOrefMAP.Add(targetObjects.SiteID, targetObjects);
			}
		}
		/// <summary>
		/// Переопределено для поиска подсистемы объектов
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if(_findRoot != null)
			{
				if(typeof(DlxCollection).IsInstanceOfType(_findRoot))
				{
					DlxCollection coll = _findRoot as DlxCollection;
					// поиск в первом уровне вложенности
					foreach(DlxObject obj in coll.Objects)
					{
						UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
						if(objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
							_idsysTOrefMAP.Add(objSys.SiteID, objSys);
					}
					// если ничего не нашли
					if(_idsysTOrefMAP.Count == 0)
					{
						// поиск во втором уровне вложенности
						foreach(DlxCollection subcoll in coll.Objects)
						{
							if(subcoll != null)
							{
								foreach(DlxObject obj in subcoll.Objects)
								{
									UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
									if(objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
										_idsysTOrefMAP.Add(objSys.SiteID, objSys);
								}
							}
						}
					}
				}
			}

			if(_idsysTOrefMAP.Count == 0)
			{
				throw new ArgumentException("UniOldRequestClient " + Name + ": Не найдена ни одна подсистема объектов в корне " + _findRoot.FullName);

			}

			base.Create();
		}
		/// <summary>
		/// Получить подсистему объектов по идентификатору
		/// </summary>
		/// <param name="SiteID">идентификатор</param>
		/// <returns>подсистема объектов</returns>
		public BaseIOSystem GetObjectsSys(uint SiteID)
		{
			UniObjSystem iosys = null;
			if(_idsysTOrefMAP.TryGetValue(SiteID, out iosys))
				return iosys;
			else
				return null;
		}

		#region Данные

		/// <summary>
		/// Путь от корня для поиска подсистемы объектов
		/// </summary>
		protected string _objectsPath = string.Empty;
		/// <summary>
		/// Карта идентификаторов к указателям на подсистемы ввода
		/// </summary>
		protected Dictionary<uint, UniObjSystem> _idsysTOrefMAP = new Dictionary<uint, UniObjSystem>();

		#endregion
	}

	/// <summary>
	/// Клиент получения данных по унифицированному протоколу (новая версия протокола)
	/// </summary>
	public class UniRequestClient : RequestClientBase, IRequesterIOSys
	{
		#region Методы работы на потоке

		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected override void ProcessAnswer()
		{
			if(_answerMsg.Data == null || _answerMsg.Data.Length == 0)
			{
				Trace.WriteLine("UniRequestClient " + Name + ": Получен пустой ответ: нет изменений " + DateTime.Now.ToString("HH:mm:ss"));
				return;
			}

			BinaryReader reader = new BinaryReader(new MemoryStream(_answerMsg.Data));

			Int32 time;
			UInt32 SiteID, VersionNSI, Size, SiteCount, ItemCount;
			long pos;

			// считываем время VersionNSI
			VersionNSI = reader.ReadUInt32();
			if(VersionNSI != _versionNSI)
				throw new RequestException("Versions of NSI is't equal (recv =" + VersionNSI + ", local=" + _versionNSI + ")!!!");

			// считываем время
			time = reader.ReadInt32();
			DateTime dateTime = Afx.CTime.ToDateTime(time);
			bool bToLocal = false;
			if((((TimeSpan)(DateTime.Now - dateTime)).Hours > 0))
			{
				dateTime = dateTime.ToLocalTime();
				bToLocal = true;
			}

			Trace.WriteLine("UniRequestClient " + Name + ": Получен ответ: размер данных = " + _answerMsg.Data.Length + " time = " + dateTime.ToString("yyyy.MM.dd HH:mm:ss") + (bToLocal ? "(to local)" : ""));

			List<UnificationConcentrator.UniDataWithObjSys> dataList = new List<UnificationConcentrator.UniDataWithObjSys>();
			List<uint> uncontrolSites = new List<uint>();
			///////////////////////////////////////////////////

			#region читаем состояния
			// читаем размер
			Size = reader.ReadUInt32();
			// если есть данные
			if(Size > 0)
			{
				// запоминаем позицию
				pos = reader.BaseStream.Position;
				// читаем количество станций
				SiteCount = reader.ReadUInt32();
				// цикл по станциям
				while(SiteCount-- > 0)
				{
					// читаем идентификатор станции
					SiteID = reader.ReadUInt32();
					UniObjSystem objsSys = GetObjectsSys(SiteID);
					int UnknownStateCount = 0, NotUnknownStateCount = 0;
					// читаем количество состояний
					ItemCount = reader.ReadUInt32();
					// цикл по состояниям
					while(ItemCount-- > 0)
					{
						UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
						state._dataTime = dateTime;
						// считываем идентификатор объекта
						state._objectID = reader.ReadUInt32();
						state._siteID = SiteID;
						// считываем состояние объекта
						state._uniState = reader.ReadByte();
						Debug.WriteLine("\tполучено: obj " + state._siteID + ":" + state._objectID + " in " + state._uniState);

						// подсчитываем количество неизвестных состояний
						if(state._uniState == (byte)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS
							|| state._uniState == (byte)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS
							|| state._uniState == (byte)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
							UnknownStateCount++;
						else
							NotUnknownStateCount++;

						// передаем в коллектор
						//SetData(state, dateTime, objsSys);
						if(objsSys != null)
							dataList.Add(new UnificationConcentrator.UniDataWithObjSys(state, objsSys));
					}

					// если количество неизвестных состояний равно количеству объектов 
					// (отнимаем единицу, т.к. в UniObjsDictionary есть еще и сама подсистема объектов)
					if(UnknownStateCount >= objsSys.UniObjsDictionary.Count - 2 && NotUnknownStateCount == 0)
						objsSys.ConnectionState = UniObjSystem.LinkState.RemoteConnectionError;
					else
						objsSys.ConnectionState = UniObjSystem.LinkState.OK;

					if(_bFilterUnknownStates && !IsFirstAnswer && UnknownStateCount >= NotUnknownStateCount)
						uncontrolSites.Add(SiteID);
				}
				// проверяем позицию данных
				if(pos + Size != reader.BaseStream.Position)
					throw new InvalidDataException("Len of State data is not valid!!!");
			}
			#endregion
			///////////////////////////////////////////////////

			///////////////////////////////////////////////////
			#region читаем ситуации
			// читаем размер
			Size = reader.ReadUInt32();
			// если есть данные
			if(Size > 0)
			{
				Int32 diagTime;
				// запоминаем позицию
				pos = reader.BaseStream.Position;
				// читаем количество станций
				SiteCount = reader.ReadUInt32();
				// список станций, по которым пришли данные
				List<uint> recsites = new List<uint>();
				// цикл по станциям
				while(SiteCount-- > 0)
				{
					// читаем идентификатор станции
					SiteID = reader.ReadUInt32();
					UniObjSystem objsSys = GetObjectsSys(SiteID);
					// запоминаем станцию
					recsites.Add(SiteID);
					// читаем количество ситуаций
					ItemCount = reader.ReadUInt32();

					// список старых ситуаций
					List<UnificationConcentrator.UniDiagState> oldDiags = null;
					// если первй ответ и objsSys является UniObjSystem, то запоминаем все старые сбои
					if(IsFirstAnswer && objsSys is UniObjSystem)
					{
						oldDiags = new List<UnificationConcentrator.UniDiagState>();
						foreach(IOBaseElement elem in (objsSys as UniObjSystem).UniObjsDictionary.Values)
						{
							UniObject obj = elem as UniObject;
							if(obj == null)
								continue;
							foreach(int diagid in obj._diagStateTOtimeMAPuni.Keys)
							{
								UnificationConcentrator.UniDiagState diagObj = new UnificationConcentrator.UniDiagState();
								diagObj._siteID = SiteID;
								diagObj._objectID = (uint)obj.Number;
								diagObj._uniDiagState = diagid;
								obj._diagStateTOtimeMAPuni.TryGetValue(diagid, out diagObj._detectTime);
								diagObj._dataTime = diagObj._detectTime;
								oldDiags.Add(diagObj);
							}
						}
					}
					// собираем новые
					List<UnificationConcentrator.UniDiagState> newDiags = null;
					if(oldDiags != null && oldDiags.Count > 0)
						newDiags = new List<UnificationConcentrator.UniDiagState>();

					while(ItemCount-- > 0)
					{
						UnificationConcentrator.UniDiagState diag = new UnificationConcentrator.UniDiagState();
						diag._dataTime = dateTime;
						// считываем идентификатор объекта
						diag._objectID = reader.ReadUInt32();
						diag._siteID = SiteID;
						// считываем ситуацию объекта
						diag._uniDiagState = reader.ReadUInt16();

						// считываем время обнаружения
						diagTime = reader.ReadInt32();
						if(diagTime == 0)
							diagTime = time;
						diag._detectTime = Afx.CTime.ToDateTime(diagTime);
						if(bToLocal)
							diag._detectTime = diag._detectTime.ToLocalTime();

						// считываем время пропадания
						diagTime = reader.ReadInt32();
						if(diagTime == 0)
							diag._endTime = DateTime.MinValue;
						else
						{
							diag._endTime = Afx.CTime.ToDateTime(diagTime);
							if(bToLocal)
								diag._endTime = diag._endTime.ToLocalTime();
						}

						Debug.WriteLine(string.Format("\tполучено: obj {0}:{1} diag {2} {3} at {4} ({5})"
											, diag._siteID
											, diag._objectID
											, diag._uniDiagState
											, diag._endTime == DateTime.MinValue ? "started" : "ended"
											, diag._endTime == DateTime.MinValue ? diag._detectTime : diag._endTime
											, diagTime));
						//Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)1500
						//, string.Format("\tполучено: obj {0}:{1} diag {2} {3} at {4} ({5})"
						//, diag._siteID
						// , diag._objectID
						// , diag._uniDiagState
						// , diag._endTime == DateTime.MinValue ? "started" : "ended"
						// , diag._endTime == DateTime.MinValue ? diag._detectTime : diag._endTime
						// , diagTime));

						if(Helper == null || Helper.TransferDiag(diag))
						{	
							// если фильтрация ДС отключена, либо ДС есть в списке транслируемых
							if(newDiags != null)
								newDiags.Add(diag);
							else
							{
								//SetData(diag, dateTime, objsSys);
								if(_bNeedDiag && objsSys != null)
									dataList.Add(new UnificationConcentrator.UniDataWithObjSys(diag, objsSys));
							}
						}
					}

					// сливаем ситуации
					if(newDiags != null)
					{
						if(newDiags.Count == 0)
							Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)102, "В первом сообщении пришло ноль ситуаций, хотя было не ноль, по станции " + objsSys.Owner.Name);

						MergeDiags(ref oldDiags, ref newDiags, dateTime);

						// передаем в коллектор
						foreach(UnificationConcentrator.UniDiagState diag in newDiags)
						{
							//SetData(diag, dateTime, objsSys);
							if(_bNeedDiag && objsSys != null)
								dataList.Add(new UnificationConcentrator.UniDataWithObjSys(diag, objsSys));
						}
					}
				}
				// если первый ответ, то проверяем старые ситуации
				if(IsFirstAnswer)
				{
					// для всех подсиcтем
					foreach(UniObjSystem sys in _idsysTOrefMAP.Values)
					{
						if(sys != null)
						{
							// если данные по этой подсистеме не получены
							if(!recsites.Contains(sys.SiteID))// то убираем все имеющиеся ситуации
							{
								//Баглаев EventLog закомментирован, т.к. тормозил запуск службы suivmain ~ 2 мин
								//Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)102, "Сброс диагностики по станции " + sys.Owner.Name);
								DeactivateDiags(sys, ref dataList, dateTime);
							}
						}
					}
				}
				// проверяем позицию данных
				if(pos + Size != reader.BaseStream.Position)
					throw new InvalidDataException("Len of Situations data is not valid!!!");
			}

			else if(IsFirstAnswer)// первый ответ, но нет диагностики - нет сбоев
			{
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)102, "Сброс диагностики по всем станциям");
				// для всех подсиcтем

				foreach(UniObjSystem sys in _idsysTOrefMAP.Values)
				{
					if(sys != null)//убираем все имеющиеся сбои
						DeactivateDiags(sys, ref dataList, dateTime);
				}
			}
			#endregion
			///////////////////////////////////////////////////

			///////////////////////////////////////////////////
			#region читаем измерения
			// читаем размер
			Size = reader.ReadUInt32();
			// если есть данные
			if(Size > 0)
			{
				Int32 parTime;
				// запоминаем позицию
				pos = reader.BaseStream.Position;
				// читаем количество станций
				SiteCount = reader.ReadUInt32();
				// цикл по станциям
				while(SiteCount-- > 0)
				{
					// читаем идентификатор станции
					SiteID = reader.ReadUInt32();
					UniObjSystem objsSys = GetObjectsSys(SiteID);
					// читаем количество измерений
					ItemCount = reader.ReadUInt32();
					// цикл по измерениям
					while(ItemCount-- > 0)
					{
						UnificationConcentrator.UniNumericParamState par = new UnificationConcentrator.UniNumericParamState();
						par._dataTime = dateTime;
						par._objectID = 0;
						par._siteID = SiteID;
						// считываем идентификатор измерения
						par._paramID = reader.ReadUInt32();
						// считываем время измерения
						parTime = reader.ReadInt32();
						if(parTime == 0)
							parTime = time;
						par._paramTime = Afx.CTime.ToDateTime(parTime);
						if(bToLocal)
							par._paramTime = par._paramTime.ToLocalTime();
						// считываем значение: если там было 0xFFFFFFFF, то получится float.NaN - это нам и нужно.
						par._paramValue = reader.ReadSingle();

						Debug.WriteLine("\tполучено: param " + par._siteID + ":" + par._paramID + " in " + par._paramValue);
						// передаем в коллектор
						//SetData(par, dateTime, objsSys);
						if(objsSys != null)
							dataList.Add(new UnificationConcentrator.UniDataWithObjSys(par, objsSys));
					}
				}
				// проверяем позицию данных
				if(pos + Size != reader.BaseStream.Position)
					throw new InvalidDataException("Len of Numeric Params data is not valid!!!");
			}
			#endregion
			///////////////////////////////////////////////////

			///////////////////////////////////////////////////
			#region читаем нечисловые данные
			// читаем размер
			Size = reader.ReadUInt32();
			// если есть данные
			if(Size > 0)
			{
				UInt16 DataSize;
				// запоминаем позицию
				pos = reader.BaseStream.Position;
				// читаем количество станций
				SiteCount = reader.ReadUInt32();
				// цикл по станциям
				while(SiteCount-- > 0)
				{
					// читаем идентификатор станции
					SiteID = reader.ReadUInt32();
					UniObjSystem objsSys = GetObjectsSys(SiteID);
					// читаем количество измерений
					ItemCount = reader.ReadUInt32();
					// цикл по данным
					while(ItemCount-- > 0)
					{
						UnificationConcentrator.UniDataParamState par = new UnificationConcentrator.UniDataParamState();
						par._dataTime = dateTime;
						par._objectID = 0;
						par._siteID = SiteID;
						// считываем идентификатор параметра
						par._paramID = reader.ReadUInt32();
						// считываем размер 
						DataSize = reader.ReadUInt16();
						// считываем данные
						par._paramValue = reader.ReadBytes(DataSize);

						// передаем в коллектор
						//SetData(par, dateTime, objsSys);
						if(objsSys != null)
							dataList.Add(new UnificationConcentrator.UniDataWithObjSys(par, objsSys));

					}
				}
				// проверяем позицию данных
				if(pos + Size != reader.BaseStream.Position)
					throw new InvalidDataException("Len of Data params data is not valid!!!");
			}
			#endregion
			///////////////////////////////////////////////////

			///////////////////////////////////////////////////
			#region читаем данные о ТО
			// читаем размер
			Size = reader.ReadUInt32();
			// если есть данные
			if(Size > 0)
			{
				Int32 workTime;
				byte DiagsCount;
				// запоминаем позицию
				pos = reader.BaseStream.Position;
				// читаем количество станций
				SiteCount = reader.ReadUInt32();
				// цикл по станциям
				while(SiteCount-- > 0)
				{
					// читаем идентификатор станции
					SiteID = reader.ReadUInt32();
					UniObjSystem objsSys = GetObjectsSys(SiteID);
					// читаем количество измерений
					ItemCount = reader.ReadUInt32();
					// цикл по данным
					while(ItemCount-- > 0)
					{
						UnificationConcentrator.UniObjWork work = new UnificationConcentrator.UniObjWork();
						// считываем флаг
						work._flag = (UnificationConcentrator.UniObjWork.Kind)reader.ReadByte();
						work._dataTime = dateTime;
						// считываем идентификатор объекта
						work._objectID = reader.ReadUInt32(); ;
						work._siteID = SiteID;
						// считываем работу
						work._workID = reader.ReadUInt32();
						work._punktID = reader.ReadUInt32();

						// считываем время начала
						workTime = reader.ReadInt32();
						if(workTime == 0)
							workTime = time;
						work._beginTime = Afx.CTime.ToDateTime(workTime);
						if(bToLocal)
							work._beginTime = work._beginTime.ToLocalTime();

						// считываем время выполнения
						workTime = reader.ReadInt32();
						if(workTime == 0)
							work._endTime = DateTime.MinValue;
						else
						{
							work._endTime = Afx.CTime.ToDateTime(workTime);
							if(bToLocal)
								work._endTime = work._endTime.ToLocalTime();
						}
						work._dataTime = work._endTime;

						// считываем соответствующие ситуации
						DiagsCount = reader.ReadByte();
						work._diags = new Dictionary<ushort, DateTime>();
						UInt16 diagID;
						while(DiagsCount-- > 0)
						{
							diagID = reader.ReadUInt16();
							workTime = reader.ReadInt32();
							if(bToLocal)
								work._diags[diagID] = Afx.CTime.ToDateTime(workTime).ToLocalTime();
							else
								work._diags[diagID] = Afx.CTime.ToDateTime(workTime);
						}
						// передаем в коллектор
						//SetData(work, dateTime, objsSys);
						if(objsSys != null)
							dataList.Add(new UnificationConcentrator.UniDataWithObjSys(work, objsSys));
					}
				}
				// проверяем позицию данных
				if(pos + Size != reader.BaseStream.Position)
					throw new InvalidDataException("Len of TO data is not valid!!!");
			}
			#endregion
			///////////////////////////////////////////////////

			if(_bFilterUnknownStates && uncontrolSites.Count > 0)
			{
				foreach(uint siteID in uncontrolSites)
					RemoveSiteData(siteID, dataList);
			}
			SetData(dataList, dateTime, null);
		}

		private void RemoveSiteData(uint siteID, List<UnificationConcentrator.UniDataWithObjSys> dataList)
		{
			for(int i = dataList.Count - 1; i >= 0; i--)
			{
				if(dataList[i]._data._siteID == siteID)
					dataList.RemoveAt(i);
			}
		}
		/// <summary>
		/// Убираем все сбои по станции
		/// </summary>
		/// <param name="sys">подсистема объектов</param>
		/// <param name="dataList">список данных</param>
		/// <param name="dateTime">время данных</param>
		private void DeactivateDiags(UniObjSystem sys, ref List<UnificationConcentrator.UniDataWithObjSys> dataList, DateTime dateTime)
		{
			foreach(IOBaseElement elem in sys.UniObjsDictionary.Values)
			{
				UniObject obj = elem as UniObject;
				if(obj == null)
					continue;
				foreach(int diagid in obj._diagStateTOtimeMAPuni.Keys)
				{
					UnificationConcentrator.UniDiagState diagObj = new UnificationConcentrator.UniDiagState();
					diagObj._siteID = sys.SiteID;
					diagObj._objectID = (uint)obj.Number;
					diagObj._uniDiagState = diagid;
					obj._diagStateTOtimeMAPuni.TryGetValue(diagid, out diagObj._detectTime);
					diagObj._dataTime = diagObj._detectTime;
					diagObj._endTime = dateTime;

					if(_bNeedDiag)
						dataList.Add(new UnificationConcentrator.UniDataWithObjSys(diagObj, sys));
				}
			}
		}

		/// <summary>
		/// Класс сравнения диагностических ситуаций
		/// </summary>
		protected class UniDiagStateComparer : IComparer<UnificationConcentrator.UniDiagState>
		{
			/// <summary>
			/// Сравнение двух ситуаций
			/// </summary>
			/// <param name="x">первый параметр</param>
			/// <param name="y">второй параметр</param>
			/// <returns>стандартный результат сравнения</returns>
			public int Compare(UnificationConcentrator.UniDiagState x, UnificationConcentrator.UniDiagState y)
			{
				if(x._siteID == y._siteID && x._objectID == y._objectID && x._uniDiagState == y._uniDiagState)
					return 0;

				if(x._siteID != y._siteID)
					return (int)x._siteID - (int)y._siteID;

				if(x._objectID != y._objectID)
					return (int)x._objectID - (int)y._objectID;

				if(x._uniDiagState != y._uniDiagState)
					return (int)x._uniDiagState - (int)y._uniDiagState;

				return 0;
			}

		}
		/// <summary>
		/// Слияние диагностики
		/// </summary>
		/// <param name="oldDiags">старые диагнозы</param>
		/// <param name="newDiags">новые диагнозы</param>
		/// <param name="dateTime">время</param>
		protected void MergeDiags(ref List<UnificationConcentrator.UniDiagState> oldDiags, ref List<UnificationConcentrator.UniDiagState> newDiags, DateTime dateTime)
		{
			UniDiagStateComparer comparer = new UniDiagStateComparer();
			foreach(UnificationConcentrator.UniDiagState diag in oldDiags)
			{
				// если в новых ситуациях такого нет, то добавляем с текущим временем окончания ситуации
				if(newDiags.BinarySearch(diag, comparer) < 0)
				{
					diag._endTime = dateTime;
					newDiags.Add(diag);
				}
			}
		}

		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected override void OnExchengeError()
		{
			if(_idsysTOrefMAP.Count == 0)
				SetData((object)null, DateTime.MinValue, null);

			// произошла ошибка - пустые данные коллектору
			foreach(UniObjSystem sys in _idsysTOrefMAP.Values)
			{
				if(sys != null)
				{
					sys.ConnectionState = UniObjSystem.LinkState.LocalConnectionError;
					SetData((object)null, DateTime.MinValue, sys);
				}
			}
		}
		/// <summary>
		/// Подготовка запроса с требованиями ReqType
		/// </summary>
		/// <param name="ReqType">требования</param>
		protected void PrepareRequest(UInt32 ReqType)
		{
			_requestMsg.Code = (RequestCode)1;
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			// пишем имя клиента
			byte[] name = System.Text.Encoding.ASCII.GetBytes(_nameUniClient);
			writer.Write((byte)name.Length);
			writer.Write(name);
			// пишем пароль
			byte[] pswd = System.Text.Encoding.ASCII.GetBytes(_pswdUniClient);
			writer.Write((byte)pswd.Length);
			writer.Write(pswd);

			writer.Write((uint)ReqType);
			UInt32 SiteCount = (UInt32)_idsysTOrefMAP.Count;// все требуемые станции
			writer.Write(SiteCount);
			foreach(UInt32 SiteID in _idsysTOrefMAP.Keys)
			{
				writer.Write(SiteID);
			}
			writer.Flush();
			writer.Close();
			// записываем данные
			_requestMsg.Data = ms.ToArray();
			ms.Close();
		}
		/// <summary>
		/// Подготовка запроса
		/// </summary>
		protected override void PrepareRequest()
		{
			UInt32 ReqType = 0;
			if(_bNeedState)// состояние
				ReqType |= 1;

			if(_bNeedDiag)// ситуации
				ReqType |= 2;

			if(_bNeedParams)// измерения
				ReqType |= 4;

			if(_bNeedDataParams)// данные
				ReqType |= 8;

			if(_bNeedWorks)// работы по ТО
				ReqType |= 0x10;

			PrepareRequest(ReqType);
		}

		#endregion

		/// <summary>
		/// Переопределена загрузка для получения объектов
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_bNeedState = Loader.GetAttributeBool("NeedState", _bNeedState);
			_bNeedDiag = Loader.GetAttributeBool("NeedDiag", _bNeedDiag);
			_bNeedParams = Loader.GetAttributeBool("NeedParams", _bNeedParams);
			_bNeedDataParams = Loader.GetAttributeBool("NeedDataParams", _bNeedDataParams);
			_bNeedWorks = Loader.GetAttributeBool("NeedWorks", _bNeedWorks);

			_bFilterUnknownStates = Loader.GetAttributeBool("FilterUnknownStates", _bFilterUnknownStates);

			if(_findRoot == null)
			{
				// загружаем ссылку на объекты
				UniObjSystem targetObjects = Loader.GetObjectFromAttribute("Objects") as UniObjSystem;
				if(targetObjects != null && !_idsysTOrefMAP.ContainsKey(targetObjects.SiteID))
					_idsysTOrefMAP.Add(targetObjects.SiteID, targetObjects);
			}

			_nameUniClient = Loader.GetAttributeString("UniClientName", string.Empty);
			_pswdUniClient = Loader.GetAttributeString("UniClientPswd", string.Empty);
			_versionNSI = Loader.GetAttributeUInt("VersionNSI", 0);

			List<XmlElement> sites = new List<XmlElement>();
			Loader.CurXmlElement.EnumInhElements(out sites, "site", null, null);
			foreach(XmlElement elem in sites)
				_sites.Add(elem.Text);

			Helper = Loader.GetObjectFromAttribute("Helper") as DiagFilterHelper;
		}
		/// <summary>
		/// Переопределено для поиска подсистемы объектов
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if(_findRoot != null && _sites.Count == 0)
			{
				if(typeof(DlxCollection).IsInstanceOfType(_findRoot))
				{
					DlxCollection coll = _findRoot as DlxCollection;
					// поиск в первом уровне вложенности
					foreach(DlxObject obj in coll.Objects)
					{
						UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
						if(objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
							_idsysTOrefMAP.Add(objSys.SiteID, objSys);
					}
					// если ничего не нашли
					if(_idsysTOrefMAP.Count == 0)
					{
						// поиск во втором уровне вложенности
						foreach(DlxCollection subcoll in coll.Objects)
						{
							if(subcoll != null)
							{
								foreach(DlxObject obj in subcoll.Objects)
								{
									UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
									if(objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
										_idsysTOrefMAP.Add(objSys.SiteID, objSys);
								}
							}
						}
					}
				}
			}
			else if(_findRoot != null && _sites.Count > 0)
			{
				foreach(string site in _sites)
				{
					UniObjSystem objSys = _findRoot.FindObject(site + "/Objects") as UniObjSystem;
					if(objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
						_idsysTOrefMAP.Add(objSys.SiteID, objSys);
				}
				_sites.Clear();
			}

			if(_idsysTOrefMAP.Count == 0)
				Trace.WriteLine("UniRequestClient " + Name + ": Не найдена ни одна подсистема объектов!");

			base.Create();

			if(Helper != null)
				Helper.Create();
		}
		/// <summary>
		/// Получить подсистему объектов по идентификатору
		/// </summary>
		/// <param name="SiteID">идентификатор</param>
		/// <returns>подсистема объектов</returns>
		public UniObjSystem GetObjectsSys(uint SiteID)
		{
			UniObjSystem iosys = null;
			if(_idsysTOrefMAP.TryGetValue(SiteID, out iosys))
				return iosys;
			else
				return null;
		}
		/// <summary>
		/// Проверка необходимости запросов
		/// </summary>
		/// <returns></returns>
		public override bool IsNeedRequest()
		{
			return _idsysTOrefMAP.Count > 0;
		}

		#region IRequesterIOSys Members
		/// <summary>
		/// Начать запрашивать состояние подсистемы sys (вдобавок к предыдущим)
		/// </summary>
		/// <param name="sys">подсистема</param>
		/// <returns>true, когда это возможно</returns>
		public virtual bool StartRequestFor(BaseIOSystem sys)
		{
			if(sys != null && sys is UniObjSystem)
			{
				if(!_idsysTOrefMAP.ContainsKey((sys as UniObjSystem).SiteID))
					_idsysTOrefMAP.Add((sys as UniObjSystem).SiteID, sys as UniObjSystem);
				PrepareRequest();
				_bRestart = true;
				_restartEvent.Set();
				return true;
			}
			return false;
		}
		/// <summary>
		/// Закончить запрашивать состояние подсистемы sys (для остальных запросы продолжаются)
		/// </summary>
		/// <param name="sys">подсистема</param>
		public virtual void StopRequsetFor(BaseIOSystem sys)
		{
			if(sys != null && sys is UniObjSystem)
			{
				_idsysTOrefMAP.Remove((sys as UniObjSystem).SiteID);
				PrepareRequest();
				_bRestart = true;
				_restartEvent.Set();
			}
		}

		#endregion

		#region Данные

		/// <summary>
		/// Путь от корня для поиска подсистемы объектов
		/// </summary>
		protected string _objectsPath = string.Empty;
		/// <summary>
		/// Карта идентификаторов к указателям на подсистемы ввода
		/// </summary>
		protected Dictionary<uint, UniObjSystem> _idsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
		/// <summary>
		/// Имя унифицированного клиента
		/// </summary>
		protected string _nameUniClient;
		/// <summary>
		/// пароль
		/// </summary>
		protected string _pswdUniClient;
		/// <summary>
		/// Требуемая версия НСИ
		/// </summary>
		protected uint _versionNSI;
		/// <summary>
		/// Список имен станций
		/// </summary>
		protected List<string> _sites = new List<string>();

		protected bool _bNeedState = true;
		protected bool _bNeedDiag = true;
		protected bool _bNeedParams = true;
		protected bool _bNeedDataParams = true;
		protected bool _bNeedWorks = true;
		protected bool _bFilterUnknownStates = false;

		/// <summary>
		/// Хранилище соответствий состояний
		/// </summary>
		protected DiagFilterHelper Helper = new DiagFilterHelper();

		#endregion
	}
	/// <summary>
	/// Клиент получения фактов ТО по унифицированному протоколу (новая версия протокола)
	/// </summary>
	public class UniFactTORequestClient : UniRequestClient
	{
		/// <summary>
		/// Подготовка запроса (только факты ТО)
		/// </summary>
		protected override void PrepareRequest()
		{
			PrepareRequest(0x10);// только факты ТО
		}
	}

	/// <summary>
	/// Клиент получения плана ТО по унифицированному протоколу (новая версия протокола)
	/// </summary>
	public class UniPlanTORequestClient : UniRequestClient
	{
		/// <summary>
		/// Подготовка запроса (только факты ТО)
		/// </summary>
		protected override void PrepareRequest()
		{
			PrepareRequest(0x20);// только планы ТО
		}
		/// <summary>
		/// Установить коллектора плана ТО
		/// </summary>
		/// <param name="collector"></param>
		public void SetPlanCollector(ICollector collector)
		{
			_collectorPlan = collector;
		}
		/// <summary>
		/// Переопределяем получение коллектора => будем запрашивать план только при подключенном коллекторе
		/// </summary>
		/// <returns></returns>
		public override ICollector GetOwnerCollector()
		{
			return _collectorPlan;
		}
		/// <summary>
		/// Ссылка на коллектор плана ТО
		/// </summary>
		protected ICollector _collectorPlan = null;
	}
	/// <summary>
	/// Вспомогательный класс для загрузки из БД перечня кодов транслируемых ДС
	/// </summary>
	public class DiagFilterHelper : v2.DBHelper
	{
		protected List<int> _allowedDiags = new List<int>();
		/// <summary>
		/// Запрос правил конвертации состояний из БД
		/// </summary>
		private string diagsQuery = "";

		/// <summary>
		/// Загрузка данных из списка ситуаций
		/// </summary>
		protected override bool LoadData()
		{
			// Избегаем повторной загрузки
			if(_allowedDiags.Count > 0)
				return true;

			bool res = true;
			SqlConnection conn = new SqlConnection(_ConnectionString);
			try
			{
				conn.Open();
				_allowedDiags = GetAllItems(conn, diagsQuery).ToList();
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка соединения с БД!\n" + ex.Message);
				res = false;
			}
			finally
			{
				conn.Close();
			}
			return res;
		}

		/// <summary>
		/// Получить все сочетания преобразований
		/// </summary>
		/// <param name="conn">Открытое SQL-соединение</param>
		/// <returns></returns>
		public static List<int> GetAllItems(SqlConnection conn, string Query)
		{
			List<int> res = new List<int>();
			SqlDataReader reader = null;
			try
			{
				var cmd = new SqlCommand(Query, conn);
				reader = cmd.ExecuteReader();
				if(reader.HasRows)
					while(reader.Read())
						res.Add(Convert.ToInt32(reader[0]));
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка получения списка элементов! " + ex.Message);
				return null;
			}
			finally
			{
				if(reader != null)
					reader.Close();
			}
			res.Sort();
			return res;
		}
		/// <summary>
		/// Транслировать ДС клиентам?
		/// </summary>
		/// <param name="ds">ДС</param>
		/// <returns>Признак необходимости трансляции</returns>
		public bool TransferDiag(UnificationConcentrator.UniDiagState ds)
		{
			return _allowedDiags.BinarySearch(ds._uniDiagState) > -1;
		}
	}


}
