﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using AfxEx;
using Ivk.IOSys;

namespace Tdm.Unification
{
    /// <summary>
    /// Класс, выполняющий синхронизацию конфигурации xml с базой данных
    /// Выполняет удаление всей конфигурации по данному комплексу из БД
    /// и загружает в БД конфигурацию на основе xml одной станции/перегона
    /// Пример конфигурирования
    /// <CFG NAME="CfgSynch" CLASS="Tdm.Unification.UniConfigSynсhronizer" ObjSys="../Objects" IOSys="../IOSys" ConnString="/App/Common/ConnString" />
    /// </summary>
    public class UniConfigSynсhronizer: DlxObject
    {
        protected string _PositionMsg;

        protected DataTable _tableToSync;
        /// <summary>
        /// Подключение к базе. Отправление всех 
        /// </summary>
        protected DBConnectorCmn _DBConnector = new DBConnectorCmn();

        /// <summary>
        /// Загрука атрибутов DlxObject, а также ссылки на подсистему ввода, подсистему объектов, подключение к БД
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _DBConnector.Load(Loader);

            _ObjSystem = Loader.GetObjectFromAttribute("ObjSys") as UniObjSystem;
            if (_ObjSystem == null)
                throw new ArgumentNullException("ObjSys", "Error load ObjSystem for UniConfigSynсhronizer");

            _IOSystem = Loader.GetObjectFromAttribute("IOSys") as IOSystem;
            if (_IOSystem == null)
                throw new ArgumentNullException("IOSys", "Error load IOSystem for UniConfigSynсhronizer");
        }

        /// <summary>
        /// Создание объекта. Синхронизация объектов, измерений и отношений между объектами
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
     
            try
            {
                if (_DBConnector.Connect())
                {
                    // Копируем данные о станциях, шч, регионах обслуживания!
                    SyncStationAndShch();
                    // Записываем объекты
                    SynсhObjects();

                    // Записываем измерения
                    SynсhParams();

                    // Записываем отношения между объектами
                    //SynсhRelations();

                    // Записываем работы по объектам
                    SynchTOWorks();

                    
                    _DBConnector.Disconnect();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(_PositionMsg + ": " + ex.Message);
            }
            
        }

        /// <summary>
        /// Синхронизация данных о станции, шч и региону
        /// </summary>
        protected void SyncStationAndShch()
        {
            Tdm.Site site = null;
            try
            {
                
                if ((site = (this.Owner as Tdm.Site)) != null)
                {
                    SqlCommand cmd = _DBConnector.SqlConn.CreateCommand();
                    cmd.Parameters.Add(new SqlParameter("SiteID", site.ID));
                    cmd.CommandText = @"if not exists(select 1 from loc_sites where site_id=@SiteID)
                        insert into loc_sites
                        select * from loc_sitesfull
                        where site_id=@SiteID";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"if not exists(select 1 from loc_shch join loc_sites on loc_shch.shch_id = loc_sites.shch_id where site_id=@SiteID)
                        insert into loc_shch
                        select * from loc_shchFull where shch_id in (select shch_id from loc_sites where site_id=@SiteID)";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"if not exists(select 1 from cfg_regionSites where site_id=@SiteID)
                        insert into cfg_regionSites
                        select * from cfg_regionSitesFull where site_id=@SiteID";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"if not exists(select 1 from cfg_regions join cfg_regionSites on cfg_regions.id = cfg_regionSites.id where site_id=@SiteID)
                        begin
                        set IDENTITY_INSERT cfg_regions on;
                        insert into cfg_regions (id, RegionName)
                        select * from cfg_regionsFull where id in (select id from cfg_regionSitesFull where site_id=@SiteID)
                        set IDENTITY_INSERT cfg_regions off;
                        end";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                Debug.WriteLine(string.Format("Не удалось скопировать данные по станции {0}, /n {1}", site?.ID, ex.Message));
            }
            catch (Exception e)
            {
                Debug.WriteLine("Не удалось скопировать данные по станции, ШЧ, региону обслуживания");
            }
        }

        /// <summary>
        /// Запись имеющихся работ по объектам станции
        /// </summary>
        protected void SynchTOWorks()
        {
            Debug.WriteLine("Старт синхронизации работ. " + _ObjSystem.SubElements.Objects.Count);
            _PositionMsg = "Синхронизация работ. ";
            SqlCommand cmd = _DBConnector.SqlConn.CreateCommand();
            cmd.CommandText = "DELETE FROM dbo.PRJ_Works Where Site_ID=" + _ObjSystem.SiteID + ";";
            int res = cmd.ExecuteNonQuery();
            CreateLocalTOWorkTable();
            AddTOWorksData();
        }

        /// <summary>
        /// Запись в базу данных обо всех объектах PRJ_Objects
        /// </summary>
        protected void SynсhObjects()
        {
            Debug.WriteLine("Старт синхронизации объектов. " + _ObjSystem.SubElements.Objects.Count);
            _PositionMsg = "Синхронизация объектов. ";
            // удаляем все записанные в БД объекты
            SqlCommand cmd = _DBConnector.SqlConn.CreateCommand();
            cmd.CommandText = "DELETE FROM dbo.PRJ_Objects Where Site_ID=" + _ObjSystem.SiteID + ";";
            int res = cmd.ExecuteNonQuery();


            cmd.CommandText = @"IF NOT EXISTS(SELECT 1 FROM information_schema.columns
                WHERE TABLE_NAME = 'PRJ_objects' AND COLUMN_NAME = 'SubType')
                ALTER TABLE PRJ_Objects ADD SubType INT NULL";

            cmd.ExecuteNonQuery();

            // добавляем все объекты из xml
            CreateLocalTable();
            AddData();
        }
        /// <summary>
        /// Создание локальной таблицы для bulk
        /// </summary>
        public void CreateLocalTable()
        {
            _tableToSync = new DataTable("StationObjects");

            _tableToSync.Columns.Add("Site_ID", typeof(Int32));
            _tableToSync.Columns.Add("Object_ID", typeof(Int32));
            _tableToSync.Columns.Add("Type_ID", typeof(Int32));
            _tableToSync.Columns.Add("Object_Name", typeof(String));
            _tableToSync.Columns.Add("Site_ID_Peregon", typeof(Int32));
            _tableToSync.Columns.Add("SubType", typeof(Int32));
        }

        /// <summary>
        /// Создание локальной таблицы для bulk
        /// </summary>
        public void CreateLocalTOWorkTable()
        {
            _tableToSync = new DataTable("TOWorks");

            _tableToSync.Columns.Add("Site_ID", typeof(Int32));
            _tableToSync.Columns.Add("Object_ID", typeof(Int32));
            _tableToSync.Columns.Add("Punkt_ID", typeof(Int32));
        }


        /// <summary>
        /// Создание локальной таблицы для bulk параметров
        /// </summary>
        public void CreateLocalTableParams()
        {
            _tableToSync = new DataTable("StationParams");

            _tableToSync.Columns.Add("Site_ID", typeof(Int32));
            _tableToSync.Columns.Add("Object_ID", typeof(Int32));
            _tableToSync.Columns.Add("Param_ID", typeof(Int32));
            _tableToSync.Columns.Add("Param_Type_ID", typeof(Int32));
            _tableToSync.Columns.Add("Param_Name", typeof(String));
            _tableToSync.Columns.Add("Param_Description", typeof(String));
            _tableToSync.Columns.Add("Normal_Max", typeof(double));
            _tableToSync.Columns.Add("Normal_Min", typeof(double));
            _tableToSync.Columns.Add("Normal_Ext", typeof(double));
            _tableToSync.Columns.Add("IsNumeric", typeof(String));
        }

        /// <summary>
        /// Добавление данных для записи в БД (Prj_Objects)
        /// </summary>
        public void AddData()
        {
            DataRow row;

            foreach (UniObject obj in _ObjSystem.SubElements.Objects)
            {
                _PositionMsg = "Синхронизация объекта " + obj.Name;
                row = _tableToSync.NewRow();

                row["Site_ID"] = _ObjSystem.SiteID;
                row["Object_ID"] = obj.Number;
                row["Type_ID"] = obj.Type.SubGroupID;
                row["SubType"] = obj.Type.SubObjectID;
                row["Object_Name"] = obj.Name;
                uint? site_id_peregon = ((obj.SiteID > 0) ? obj.SiteID : (uint?)null);
                if (obj.SiteID > 0)
                    row["Site_ID_Peregon"] = obj.SiteID;
                else row["Site_ID_Peregon"] = DBNull.Value;
                _tableToSync.Rows.Add(row);
            }

            using (SqlBulkCopy loader = new SqlBulkCopy(_DBConnector.ConnectionString))
            {
                loader.BulkCopyTimeout = 3600;
                loader.DestinationTableName = "prj_objects";
                loader.WriteToServer(_tableToSync);
            }
        }


        /// <summary>
        /// Добавление данных для записи в БД (Prj_Params)
        /// </summary>
        public void AddParamsData()
        {
            DataRow row;

            foreach (UniObject obj in _ObjSystem.SubElements.Objects)
            {
                foreach (Tdm.ObjData od in obj.ObjDataCollection.Objects)
                {
                    UniObjData objData = od as UniObjData;
                    if (objData != null)
                    {
                        _PositionMsg = "Синхронизация параметра " + obj.Name + "/" + objData.Name;
                        if (objData.IsIOSignal)
                        {
                            IOSignalBase sig = objData.IOSignal;
                            if (sig != null)
                            {
                                row = _tableToSync.NewRow();

                                row["Site_ID"] = _ObjSystem.SiteID;
                                row["Object_ID"] = obj.Number;
                                row["Param_ID"] = objData.ParamID;
                                row["Param_Type_ID"] = objData.ParamType;
                                row["Param_Name"] = objData._codeSign;
                                row["Param_Description"] = objData._diagParamsPath;
                                if (objData.IsNumeric)
                                    row["IsNumeric"] = "1";
                                _tableToSync.Rows.Add(row);
                            }
                        }
                    }
                }
            }

            using (SqlBulkCopy loader = new SqlBulkCopy(_DBConnector.ConnectionString))
            {
                loader.BulkCopyTimeout = 3600;
                loader.DestinationTableName = "prj_params";
                loader.WriteToServer(_tableToSync);
            }
        }


        /// <summary>
        /// Добавление данных для записи в БД (Prj_Objects)
        /// </summary>
        public void AddTOWorksData()
        {
            DataRow row;
            foreach (UniObject obj in _ObjSystem.SubElements.Objects)
            {
                _PositionMsg = "Синхронизация  " + obj.Name;
                foreach (UniObjectUpTO.TOTask task in obj.TechTasks.Objects)
                {
                    foreach (var subTask in task.subTasks)
                    {
                        row = _tableToSync.NewRow();

                        row["Site_ID"] = _ObjSystem.SiteID;
                        row["Object_ID"] = obj.Number;
                        row["Punkt_ID"] = subTask.Punkt_ID;
                        _tableToSync.Rows.Add(row);
                    }
                }
            }

            using (SqlBulkCopy loader = new SqlBulkCopy(_DBConnector.ConnectionString))
            {
                loader.BulkCopyTimeout = 3600;
                loader.DestinationTableName = "prj_works";
                loader.WriteToServer(_tableToSync);
            }
        }

        /// <summary>
        /// Запись в базу данных обо всех измерениях PRJ_Params
        /// </summary>
        protected void SynсhParams()
        {
            Debug.WriteLine("Старт синхронизации параметров. ");
            _PositionMsg = "Синхронизация параметров. ";
            // удаляем все записанные в БД измерения
            SqlCommand cmd = _DBConnector.SqlConn.CreateCommand();
            cmd.CommandText = "DELETE FROM dbo.PRJ_Params Where Site_ID=" + _ObjSystem.SiteID + ";";
            int res = 0;
            cmd.ExecuteNonQuery();
            Console.WriteLine("PRJ_Params Deleted " + res);
            /*
            // добавляем все измерения из xml
            cmd.CommandText = "INSERT INTO dbo.PRJ_Params VALUES ";
            int cnt = 0;
            string shifr = string.Empty;
            string comment = string.Empty;
            foreach (UniObject obj in _ObjSystem.SubElements.Objects)
            {
                foreach (Tdm.ObjData od in obj.ObjDataCollection.Objects)
                {
                    UniObjData objData = od as UniObjData;
                    if (objData != null)
                    {
                        _PositionMsg = "Синхронизация параметра " + obj.Name + "/" + objData.Name;
                        if (objData.IsIOSignal)
                        {
                            IOSignalBase sig = objData.IOSignal;
                            if (sig != null)
                            {
                                if (sig.Comment == null)
                                    throw new ArgumentNullException("Comment", _PositionMsg + ": не задан комментарий к сигналу " + objData.SignalReference);
                                shifr = sig.Comment.Code;
                                comment = sig.Comment.Text;
                                //shifr = objData._codeSign;
                                //comment = objData._diagParamsPath;
                            }
                        }
                        cmd.CommandText += "(" +_ObjSystem.SiteID + "," + obj.Number + "," + objData.ParamID + "," + objData.ParamType + ",'" + shifr + "','" + comment + "',NULL,NULL,NULL," + (objData.IsNumeric ? "1" : "NULL") + ",NULL)";
                        if (++cnt > 100)// Защита от слишком большого запроса
                        {
                            cmd.CommandText += ";";
                            res = cmd.ExecuteNonQuery();
                            cmd.CommandText = "INSERT INTO dbo.PRJ_Params VALUES ";
                            Console.WriteLine("PRJ_Params Inserted " + res);
                            cnt = 0;
                        }
                        else
                            cmd.CommandText += ",";
                    }
                }
            }
            if (cnt > 0)
            {
                cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1);
                cmd.CommandText += ";";
                res = cmd.ExecuteNonQuery();
                Console.WriteLine("PRJ_Params Inserted " + res);
            }*/
            CreateLocalTableParams();
            AddParamsData();
        }
        
        /// <summary>
        /// Запись в базу отношений между объектами
        /// </summary>
        protected void SynсhRelations()
        {
            _PositionMsg = "Синхронизация отношений. Формирование словаря типов отношений";
            SqlCommand cmd = _DBConnector.SqlConn.CreateCommand();
            // формируем словарь типов отношений для получения ID типа отношения по имени
            RelationTypes relTypes = new RelationTypes();
            relTypes.Load(cmd);

            // удаляем все записанные в БД измерения
            _PositionMsg = "Синхронизация отношений. Очищение таблицы отношений";
            cmd.CommandText = "DELETE FROM dbo.PRJ_Relations Where Site_ID=" + _ObjSystem.SiteID + ";";
            int res = cmd.ExecuteNonQuery();
            Console.WriteLine("PRJ_Relations Deleted " + res);

            // добавляем все отношения из xml
            cmd.CommandText = "INSERT INTO dbo.PRJ_Relations VALUES ";
            int cnt = 0;
            int ind = 0;
            // для всех объектов получаем отношения между объектами и формируем запрос к БД на добавление записей
            foreach (UniObject obj in _ObjSystem.SubElements.Objects)
            {
                foreach (Tdm.ObjData od in obj.ObjDataCollection.Objects)
                {
                    // только для данных типа объект
                    if (od.IsControlObj)
                    {
                        _PositionMsg = "Синхронизация отношения " + obj.Name + "/" + od.Name;
                        BaseControlObj refObj = od.ControlObj;
                        int typeID = relTypes.Find(od.Name, obj.Type.SubGroupID);
                        if (typeID < 0)
                            throw new Exception("Не найден тип отношения " + od.Name + " ob" + obj.Type.SubGroupID);
                        cmd.CommandText += "(" + ind++ + "," + _ObjSystem.SiteID + "," + obj.Number + "," + typeID + "," + refObj.Number + ")";
                        
                        if (++cnt > 100)// Защита от слишком большого запроса
                        {
                            cmd.CommandText += ";";
                            res = cmd.ExecuteNonQuery();
                            cmd.CommandText = "INSERT INTO dbo.PRJ_Relations VALUES ";
                            Console.WriteLine("PRJ_Relations Inserted " + res);
                            cnt = 0;
                        }
                        else
                            cmd.CommandText += ",";
                    }
                }
            }
            if (cnt > 0)
            {
                cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1);
                cmd.CommandText += ";";
                res = cmd.ExecuteNonQuery();
                Console.WriteLine("PRJ_Relations Inserted " + res);
            }
        }

        /// <summary>
        /// Подсистема объектов
        /// </summary>
        UniObjSystem _ObjSystem;

        /// <summary>
        /// Подсистема ввода
        /// </summary>
        IOSystem _IOSystem;
    }

    /// <summary>
    /// Хранилище коллекции типов отношений
    /// </summary>
    public class RelationTypes
    {
        /// <summary>
        /// Параметры типового отношения
        /// </summary>
        public class TPD_Relation
        {
            public int ID;
            public string Name;
            public int ObjTypeID;
        }
        /// <summary>
        /// Массив с типами отношений
        /// </summary>
        protected TPD_Relation[] _Relations;

        /// <summary>
        /// Загрузка типов отношений из таблицы
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public bool Load(SqlCommand cmd)
        {
            SqlDataReader dr = null;
            try
            {
                cmd.CommandText = "SELECT * FROM dbo.TPD_Relations;";
                dr = cmd.ExecuteReader();

                int fldID = dr.GetOrdinal("RelationType_ID");
                int fldName = dr.GetOrdinal("ObjData");
                int fldObjTypeID = dr.GetOrdinal("Uni_Type_ID");
                int fldRelObjTypeID = dr.GetOrdinal("RelObj_Type_ID");
                List<TPD_Relation> relationList = new List<TPD_Relation>();
                while (dr.Read())
                {
                    TPD_Relation rel = new TPD_Relation();
                    rel.ID = dr.GetInt32(fldID);
                    rel.Name = dr.GetString(fldName);
                    rel.ObjTypeID = dr.GetInt32(fldObjTypeID);
                    relationList.Add(rel);
                }

                _Relations = relationList.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                dr.Close();
                return false;
            }
            dr.Close();
            return true;
        }

        /// <summary>
        /// Найти подходящий тип отношения
        /// </summary>
        /// <param name="name">Обозначение отношения из конфигурации (например NextSec)</param>
        /// <param name="objTypeID">Идентификатор типа объекта, у которого устанлено отношение</param>
        /// <returns>Идентификатор типа отношения</returns>
        public int Find(string name, int objTypeID)
        {
            foreach(TPD_Relation rel in _Relations)
            {
                if (rel.Name == name && rel.ObjTypeID == objTypeID)
                    return rel.ID;
            }
            return -1;
        }
    }
}
