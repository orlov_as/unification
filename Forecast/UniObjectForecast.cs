﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using Ivk.IOSys;
using Tdm;
using AfxEx;

namespace Tdm.Unification.Forecast
{
    /// <summary>
    /// Объект прогнозирования
    /// </summary>
    class UniObjectForecast: UniObjectUpDiag
    {
        public uint ID;
        /// <summary>
		/// Базовый класс задачи диагностирования
		/// </summary>
        public class ForecastTask : TechTask
        {
            public bool IsDiagStateSeted
            {
                get 
                {
                    UniObjectForecast obj = Owner as UniObjectForecast;
                    foreach (DiagState ds in obj.DiagStates)
                    {
                        if (ds.Id == _Template._DiagSateUni)
                            return true;
                    }
                    return false;
                }
            }
            /// <summary>
            /// сигнал для диагностики
            /// </summary>
            public uint _ParamID;
            /// <summary>
            /// история изменения сигнала
            /// </summary>
            public ForecastARH _History;
            /// <summary>
            /// Шаблон задачи с параметрами диагностирования
            /// </summary>
            public ForecastTaskTmpl _Template;
            /// <summary>
            /// Значение нормали
            /// </summary>
            public double _NormalValue;

            public override string ToString()
            {
                string rslt = "Param" + _ParamID + " DS" + _Template._DiagSateUni + " NT" + _Template._NormalType + " NV" + _NormalValue.ToString("F"); ;
                rslt += " \n\t" + _History;
                return rslt;
            }
         
            public bool _bFirstValue = true;

            public bool IsOver(double value, double normal)
            {
                if( (_Template._NormalType & 1) > 0 )
                    return value > normal;// Завышение 
                return value < normal;// Занижение
            }

            public void WriteLog(string msg)
            {
                /*UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                if (sys != null)
                    sys.WriteLog(msg);*/
            }

            public void OnPeriodFinished(DateTime curtime, float paramValue)
            {
                WriteLog("OnPeriod " + curtime + " " + ToString());
                // Eсли небыло значений или
                // Если обнаруженный экстремум вышел за норму, то сбрасываем параметры прогнозирования
                if (_History.CollectValue == 0 || IsOver(_History.CollectValue, _NormalValue))
                {
                    _History.CollectValue = 0;
                    _History.LastS = 0;
                    _History.LastB = 0;
                    _History.LastValue = 0;
                    _History.ForecastDate = DateTime.MinValue;
                    _History.ForecastValue = 0;
                    WriteLog("Reset forecast " + curtime + " " + ToString());
                }
                else
                {
                    if (_History.LastValue != 0)
                    {
                        double fValue = 0;
                        if (_History.LastS == 0)
                        {
                            _History.LastS = _History.LastValue;
                            _History.LastB = _History.CollectValue - _History.LastS;
                        }
                        else
                        {
                            // Ищем параметры эволюции, дающие прогноз наиболее близкий к экстремуму за прошедший период
                            ForecastMath.FindForecastParams(_History.LastValue, _History.CollectValue, ref _History.LastS, ref _History.LastB, ref _History.p1, ref _History.p2);
                        }
                        // расчитываем прогноз на следующий период
                        double S = _History.LastS, B = _History.LastB;
                        fValue = ForecastMath.CalcForecast(_History.CollectValue, ref S, ref B, _History.p1, _History.p2);
                        WriteLog("FCST " + fValue.ToString("F") + " LV" + _History.LastValue.ToString("F") + " CV" + _History.CollectValue.ToString("F") + " S" + _History.LastS.ToString("F") + " B" + _History.LastB.ToString("F") + " p" + _History.p1.ToString("F") + " " + _History.p2.ToString("F"));

                        _History.ForecastDate = curtime;
                        _History.ForecastValue = fValue;

                        // если прогноз выходит за нормаль - Устанавливаем диагноз и пишем в базу (таблица отказов)
                        if (IsOver(fValue, _NormalValue))
                        {
                            (Owner.Owner as UniObjSystemForecast).ExecuteRequest(_History.Insert_ARH_StatesForecast);
                        }
                    }
                }
                // Перебрасываем экстремум пред периода и запоминаем текущее значение как экстремум нового периода
                _History.LastValue = _History.CollectValue;
                
                _History.CollectValue = paramValue;
                _History.CurDate = curtime;
                UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                sys.ExecuteRequest(_History.FullInsertSqlString);
            }

            /// <summary>
            /// Обновление состояния объекта
            /// </summary>
            public void OnUpdateObjectState()
            {
                // дублируем последние данные с последним временем
                // Если есть условие состояния объекта
                if (_Template._ConditionStatesUni.Count > 0)
                {
                    DateTime paramTime = DateTime.MinValue;
                    float paramValue = 0;
                    int objState = 0;
                    if (_MesureBuffer.LastMeasure(ref paramValue, ref paramTime, ref objState))
                    {
                        UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                        UniObjectForecast obj = Owner as UniObjectForecast;
                        WriteLog("New state " + obj.ObjState.Current + ". LastMeasure " + paramValue + " st" + objState);
                        UpdateObjectParam(_ParamID, sys.DataTime, paramValue, objState);
                        UpdateObjectParam(_ParamID, sys.DataTime, paramValue, obj.ObjState.Current);
                    }
                }
            }

            /// <summary>
            /// Очередь для выдержки значений
            /// </summary>
            protected MeasureQueue _MesureBuffer = new MeasureQueue(3, 20);

            public virtual void UpdateObjectParam(UInt32 paramID, DateTime paramTime, float paramValue, int objState)
            {
                WriteLog("UpdateObjectParam " + _Template._DiagSateUni + " " + paramID + " " + paramTime + " " + paramValue + " " + objState);
                UniObjectForecast obj = Owner as UniObjectForecast;
                // Если есть условие состояния объекта
                if (_Template._ConditionStatesUni.Count > 0)
                {
                    // объект в нужном состоянии
                    if (_Template._ConditionStatesUni.Contains(objState))
                    {
                        // добавляем значение в очередь
                        _MesureBuffer.AddNewValue(paramValue, paramTime, objState);
                        // Для всех выдержаных значений вызываем обработку
                        while (_MesureBuffer.GetProcessValue(ref paramValue, ref paramTime, ref objState))
                            ProcessNewParamValue(paramID, paramTime, paramValue);
                    }
                    else// Объект не в нужном состоянии
                    {
                        // сбрасываем буферизацию
                        //LastParamValue = float.NaN;
                        _MesureBuffer.Clear();
                        // и выходим из обработки
                    }
                    return;
                }
                ProcessNewParamValue(paramID, paramTime, paramValue);
            }

            public virtual void ProcessNewParamValue(UInt32 paramID, DateTime paramTime, float paramValue)
            {
                WriteLog("ProcessNewParamValue " + paramID + " " + paramTime + " " + paramValue);
                // Если первое измерение 
                if (_History == null)
                {
                    UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                    if (_History == null)
                        _History = new ForecastARH(sys.SiteID, (Owner as UniObjectForecast).ID, _ParamID, (uint)_Template._DiagSateUni);
                    _History.CollectValue = paramValue;
                    _History.CurDate = paramTime;
                    sys.ExecuteRequest(_History.CurExtrUpdateSqlStringToday);
                }
                else if (paramTime.Date != _History.CurDate.Date)
                {
                    // Если период времени изменился на 1, значит надо сохранить экстремум в базу
                    if (paramTime.Date - _History.CurDate.Date == TimeSpan.FromDays(1))
                    {
                        OnPeriodFinished(paramTime, paramValue);
                    }
                    else// иначе - был перерыв больше периода и надо сбросить расчет
                    {
                        _History.LastS = 0;
                        _History.LastB = 0;
                        _History.LastValue = 0;
                        _History.CollectValue = paramValue;
                        _History.CurDate = paramTime;
                        UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                        sys.ExecuteRequest(_History.FullInsertSqlString);
                    }
                }
                else// внутри текущего периода сохраняем только экстремумы
                {
                    if (IsOver(paramValue, _History.CollectValue))
                    {
                        _History.CollectValue = paramValue;
                        _History.CurDate = paramTime;
                        UniObjSystemForecast sys = (Owner.Owner as UniObjSystemForecast);
                        sys.ExecuteRequest(_History.CurExtrUpdateSqlStringToday);
                    }
                }
            }
        }

        public void Load(uint id, DlxObject owner)
        {
            ID = id;
            m_Name = "Obj" + ID;
            Owner = owner;
        }

        public override void Create()
        {
            _objData = new DlxCollection("ObjData");
            _techTasks = new TechTaskCollection();
            base.Create();
        }

        public void OnNewDate(DateTime LastDate)
        {
            foreach (ForecastTask task in _techTasks.Objects)
            {
                task._bFirstValue = true;
            }
        }

        public void AddTask(ForecastTaskTmpl tmpl, uint Param, double normal)
        {
            ForecastTask task = new ForecastTask();
            task._ParamID = Param;
            task._Template = tmpl;
            task._NormalValue = normal;
            _techTasks.AddObject(task);
            task.Owner = this;
        }

        public ForecastTask FindTask(uint DS_ID, uint ParamID)
        {
            foreach (ForecastTask task in _techTasks.Objects)
            {
                if (task._Template._DiagSateUni == DS_ID && task._ParamID == ParamID)
                    return task;
            }
            return null;
        }

        public void AddParam(uint Param)
        {
            UniObjData objData = new UniObjData(Param, true);
            objData.Create();
            _objData.AddObject(objData);
            _objSystem.AddObjParamToMap(objData);
        }

        /// <summary>
        /// Обновление состояния объекта
        /// </summary>
        public override void SetObjState(int state)
        {
            base.SetObjState(state);

            foreach (DlxObject dlx in _techTasks.Objects)
            {
                ForecastTask task = dlx as ForecastTask;
                if (task != null )
                    task.OnUpdateObjectState();
            }
        }

        public override void ActivateObjDiagState(int state, DateTime detectTime)
        {
        }

        public virtual void UpdateObjectParam(UInt32 paramID, DateTime paramTime, float paramValue)
        {
            foreach (ForecastTask task in _techTasks.Objects)
            {
                if (task != null && task._ParamID == paramID)
                    task.UpdateObjectParam(paramID, paramTime, paramValue, ObjState.Current);
            }
        }
    }
}
