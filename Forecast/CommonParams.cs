﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AfxEx;
using LibEx;
using System.Diagnostics;

namespace Tdm.Unification
{
    public class StringValue : DlxObject
    {
        protected string _Value;
        public string Value
        {
            get { return _Value; }
        }
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _Value = Loader.GetAttributeString("Value", "");
        }
    }
}
