﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Net;

using Ivk.IOSys;
using Tdm;
using AfxEx;

namespace Tdm.Unification.Forecast
{
    /// <summary>
    /// Подсистема объектов прогнозирования
    /// </summary>
    public class UniObjSystemForecast: UniObjSystem
    {
        /// <summary>
        /// Словарь объектов. Служит для быстрого доступа к объекту по ID
        /// </summary>
        Dictionary<uint, UniObjectForecast> objInfoDict = new System.Collections.Generic.Dictionary<uint, UniObjectForecast>();
        Dictionary<uint, UniObjectForecast> ParamsToObjDict = new System.Collections.Generic.Dictionary<uint, UniObjectForecast>();
        StringValue _ConnectionString;

        /// <summary>
        /// Загрузка
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _ConnectionString = Loader.GetObjectFromAttribute("ConnString") as StringValue;
        }

        // запрашиваем в БД параметры прогнозирования
        SqlConnection sqlcon;/* = new SqlConnection("user id=sa;" +
                                   "password=1234;server=127.0.0.1;" +
                                   "Trusted_Connection=yes;" +
                                   "database=MainDB; " +
                                   "connection timeout=5");*/
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <returns>удачная ли инициализация</returns>
        public override void Create()
        {
            base.Create();

            Trace.WriteLine("Create " + _siteID);
            try
            {
                sqlcon = new SqlConnection(_ConnectionString.Value);
                sqlcon.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlcon;
                // Запрашиваем и формируем шаблоны задач
                cmd.CommandText = "SELECT * FROM dbo.TPD_ForecastTasks;";
                SqlDataReader dr = cmd.ExecuteReader();
                HashSet<int> parTypes = new HashSet<int>();
                Dictionary<int, ForecastTaskTmpl> taskTemplates = new Dictionary<int, ForecastTaskTmpl>();
                while (dr.Read())
                {
                    ForecastTaskTmpl item = new ForecastTaskTmpl();
                    item.Read(dr);
                    taskTemplates.Add(item._ID, item);
                    parTypes.Add(item._ParamTypeUni);// добавляем параметр
                }
                dr.Close();
                // Добавляем условия состояния объектов в шаблоны задач
                cmd.CommandText = "SELECT * FROM dbo.TPD_ForecastTasksConditions";
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int taskTmplID = Convert.ToInt32(dr["TaskID"]);
                    int cond = Convert.ToInt32(dr["ConditionStateUni"]);
                    taskTemplates[taskTmplID]._ConditionStatesUni.Add(cond);
                }
                dr.Close();
                // запрашиваем в БД все объекты, по которым надо выполнять прогнозирование
                cmd.CommandText = "SELECT * FROM dbo.PRJ_Params WHERE Site_ID=" + _siteID + " AND Param_Type_ID IN (";
                foreach (int n in parTypes)
                    cmd.CommandText += n + ",";
                cmd.CommandText = cmd.CommandText.Remove(cmd.CommandText.Length - 1);
                cmd.CommandText += ") ";//AND (Normal_Max > 0 OR Normal_Min > 0);";

                dr = cmd.ExecuteReader();
                SubElements = new DlxCollection();
                int nFldNoramMax = dr.GetOrdinal("Normal_Max");
                int nFldNoramMin = dr.GetOrdinal("Normal_Min");
                int nFldNoramExt = dr.GetOrdinal("Normal_Ext");
                while (dr.Read())
                {
                    UniObjectForecast obj;
                    uint objID = Convert.ToUInt32(dr["Object_ID"]);
                    uint paramID = Convert.ToUInt32(dr["Param_ID"]);
                    int paramTypeID = Convert.ToInt32(dr["Param_Type_ID"]);
                    double normalMax = 0;
                    double normalMin = 0;
                    double normalExt = 0;
                    if (dr.IsDBNull(nFldNoramMax) == false)
                        normalMax = Convert.ToDouble(dr[nFldNoramMax]);
                    if (dr.IsDBNull(nFldNoramMin) == false)
                        normalMin = Convert.ToDouble(dr[nFldNoramMin]);
                    if (dr.IsDBNull(nFldNoramExt) == false)
                        normalExt = Convert.ToDouble(dr[nFldNoramExt]);

                    if (objInfoDict.ContainsKey(objID) == false)
                    {   // Создаем объект
                        obj = new UniObjectForecast();
                        SubElements.AddObject(obj);// Добавляем в масссив объектов
                        //WriteLog("Add obj " + objID);
                        obj.Load(objID, this);
                        obj.Create();
                        objInfoDict.Add(objID, obj);// Добавляем в словарь
                        _idobjTOrefMAPuni.Add((int)obj.ID, obj);
                    }
                    else
                        obj = objInfoDict[objID];
                    // добавлем параметр объекту
                    obj.AddParam(paramID);
                    //WriteLog("Add param " + objID + " " + paramID);
                    ParamsToObjDict.Add(paramID, obj);
                    // добавляем задачи из шаблона по типу параметра
                    foreach (ForecastTaskTmpl tmpl in taskTemplates.Values)
                    {
                        if (tmpl._ParamTypeUni == paramTypeID)
                        {
                            // 0 - занижение мин, 1 - завышение макс, 2 - занижение экс, 3 - завышение экс
                            double normal = 0;
                            if(tmpl._NormalType == 0)
                                normal = normalMin;
                            else if(tmpl._NormalType == 1)
                                normal = normalMax;
                            else if((tmpl._NormalType & 2)>0)
                                normal = normalExt;

                            if (normal > 0)
                            {
                                obj.AddTask(tmpl, paramID, normal);
                                //WriteLog("Task o" + obj.ID + " p" + paramID + " n" + normal + " tt" + tmpl._ParamTypeUni + " tn" + tmpl._NormalType + " ds" + tmpl._DiagSateUni);
                            }
                        }
                    }
                }
                dr.Close();
                // Загружаем сохраненные параметры прогнозирования
                LoadForecatsARH(sqlcon);
                sqlcon.Close();
            }
            catch (Exception e)
            {
                throw new Exception(Name + ":Create: " + e.Message);
            }
        }

        /// <summary>
        /// Оповещение об изменении параметра объекта
        /// </summary>
        /// <param name="objectID"></param>
        /// <param name="paramID"></param>
        /// <param name="paramTime"></param>
        /// <param name="paramValue"></param>
        public override void OnObjectParamState(UInt32 objectID, UInt32 paramID, DateTime paramTime, float paramValue)
        {
            try
            {
                //paramTime = paramTime.ToLocalTime();
                if (float.IsNaN(paramValue) || paramValue == 0)
                    return;
                UniObjectForecast obj = ParamsToObjDict[paramID];
                if (obj != null)
                {
                    //WriteLog("par-" + paramID + " v-" + paramValue + " t-" + paramTime);
                    obj.UpdateObjectParam(paramID, paramTime, paramValue);
                }
            }
            catch (Exception ex)
            {
                //WriteLog("Ex: OnObjectParamState" + ex.Message);
            }
        }

        public override void OnObjectState(UInt32 objectID, byte uniState, byte uniPrevState)
        {
            base.OnObjectState(objectID, uniState, uniPrevState);
        }

        public void ExecuteRequest(string sqlRequest)
        {
            try
            {
                if(sqlcon.State != System.Data.ConnectionState.Open)
                    sqlcon.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlcon;
                cmd.CommandText = sqlRequest;
                //WriteLog(sqlRequest);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //WriteLog("Ex: ExecuteRequest: " + e.Message);
            }
        }

        public void LoadForecatsARH(SqlConnection sqlcon)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlcon;
                // Запрашиваем историю экстремумов параметров
                cmd.CommandText = "SELECT * FROM dbo.ARH_Forecast WHERE Site_ID=" + _siteID + " AND DATEDIFF(day, CurExtrTime, '" + DateTime.Now + "')=0;";
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    try
                    {
                        uint ObjectID = Convert.ToUInt32(dr["Object_ID"]);
                        UniObjectForecast obj = objInfoDict[ObjectID];
                        if (obj != null)
                        {
                            uint DS_ID = Convert.ToUInt32(dr["DS_ID"]);
                            uint ParamID = Convert.ToUInt32(dr["Param_ID"]);

                            UniObjectForecast.ForecastTask task = obj.FindTask(DS_ID, ParamID);
                            if (task != null)
                            {
                                ForecastARH fcst = new ForecastARH(dr);
                                task._History = fcst;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //WriteLog("LoadForecatsARH SqlDataReader " + e.Message);
                    }
                }
                dr.Close();
            }
            catch (Exception e)
            {
                //WriteLog("LoadForecatsARH " + e.Message);
            }
        }
    }

    /// <summary>
    /// Загружает данные из файла 
    /// obj     param   time                    value   state
    /// 1091	109103	2014-10-20 00:00:08.000	4,782	NULL
    /// 1093	109301	2014-10-20 00:00:08.000	1,235	NULL
    /// 1406	NULL	2014-10-20 00:00:08.000	NULL	3
    /// </summary>
    public class UniObjSystemForecastLoadFile : UniObjSystemForecast
    {
        public override void Create()
        {
            base.Create();

            ProcessDynamicFromFile();
        }

        public override void SetInvalidObjectsState()
        {
            //SetInvalidSubObjectsState(this, (int)_unknownStateID);
            //SetInvalidData();
        }

        private void ProcessDynamicFromFile()
        {
            try
            {
                FileStream ldFile = File.OpenRead(".\\" + _siteID + ".txt");
                StreamReader sr = new StreamReader(ldFile);
                UInt32 objID = 0, param = 0, state = 0;
                float value = float.NaN;
                DateTime tm = DateTime.MinValue;
                DateTime ltm = DateTime.MinValue;
                UInt32 pos = 0;
                while (sr.EndOfStream == false)
                {
                    string ln = sr.ReadLine();
                    if (ParseDynamicString(ln, ref objID, ref param, ref tm, ref value, ref state))
                    {
                        if (tm.Minute != ltm.Minute)
                            Console.WriteLine(pos++ + "\t-" + ln);
                        // фиксируем время данных
                        DataTime = tm;
                        //передаем на обработку
                        if (float.IsNaN(value))// состояние
                        {
                            IOBaseElement obj = null;
                            if (_idobjTOrefMAPuni.TryGetValue((int)objID, out obj) && (obj is UniObject))
                            {
                                (obj as UniObject).SetObjState((int)state);
                            }
                        }
                        else// измерение
                        {
                            OnObjectParamState(objID, param, tm, value);
                        }
                    }
                    ltm = tm;
                }
                throw new Exception("End of load");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ProcessDynamicFromFile: " + ex.Message);
            }
        }
        private bool ParseDynamicString(string line, ref UInt32 ObjectID, ref UInt32 ParamID, ref DateTime time, ref float Value, ref UInt32 StateID)
        {
            try
            {
                // ObjectID
                int pos = line.IndexOf('\t');
                string s = line.Remove(pos);
                ObjectID = Convert.ToUInt32(s);
                if (ObjectID != 971)
                    return false;
                line = line.Remove(0, pos + 1);
                // ParamID
                pos = line.IndexOf('\t');
                s = line.Remove(pos);
                if (s == "NULL")
                    ParamID = UInt32.MaxValue;
                else
                    ParamID = Convert.ToUInt32(s);
                line = line.Remove(0, pos + 1); ;
                // time
                pos = line.IndexOf('\t');
                s = line.Remove(pos);
                time = Convert.ToDateTime(s);
                line = line.Remove(0, pos + 1); ;
                // value
                pos = line.IndexOf('\t');
                s = line.Remove(pos);
                if (s == "NULL")
                    Value = float.NaN;
                else
                {
                    Value = Convert.ToSingle(s);
                    if (Value > 1000000000000)
                        return false;
                }
                line = line.Remove(0, pos + 1); ;
                // state
                s = line;
                if (s == "NULL")
                    StateID = UInt32.MaxValue;
                else
                    StateID = Convert.ToUInt32(s);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
