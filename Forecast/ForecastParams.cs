﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Tdm.Unification.Forecast
{
    /// <summary>
    /// Класс расчета прогнозных значений
    /// </summary>
    public class ForecastMath
    {
        public static float Param(float P)
        {
            if (P > 0.95F)
                return 0.95F;
            if (P < 0.05F)
                return 0.05F;
            return P;
        }
        public static void CalcForecastDelta(double PrevV, double CurV, float p1, float p2, double LastS, double LastB, ref double S, ref double B, ref float res_p1, ref float res_p2, ref double minDelta)
        {
            double fcstV = CalcForecast(PrevV, ref LastS, ref LastB, p1, p2);
            double Delta = Math.Abs(CurV - fcstV);
            //Console.WriteLine("f=" + fcstV.ToString("F") + " D=" + Delta.ToString("F") + " S=" + LastS.ToString("F") + " B=" + LastB.ToString("F") + " a" + p1.ToString("F") + " b" + p2.ToString("F"));
            if (Delta < minDelta)
            {
                res_p1 = p1;
                res_p2 = p2;
                minDelta = Delta;
                S = LastS;
                B = LastB;
            }
        }
        public static void FindForecastParams(double PrevV, double CurV, ref double LastS, ref double LastB, ref float a1, ref float a2)
        {
            double minDelta = 100000000;
            float p1 = 0F, p2 = 0F;
            double S = LastS, B = LastB;
            // Ищем эволюционные параметры с наименьшей погрешностью
            CalcForecastDelta(PrevV, CurV, Param(a1), Param(a2), S, B, ref LastS, ref LastB, ref p1, ref p2, ref minDelta);
            CalcForecastDelta(PrevV, CurV, Param(a1 + 0.05F), Param(a2 + 0.05F), S, B, ref LastS, ref LastB, ref p1, ref p2, ref minDelta);
            CalcForecastDelta(PrevV, CurV, Param(a1 + 0.05F), Param(a2 - 0.05F), S, B, ref LastS, ref LastB, ref p1, ref p2, ref minDelta);
            CalcForecastDelta(PrevV, CurV, Param(a1 - 0.05F), Param(a2 + 0.05F), S, B, ref LastS, ref LastB, ref p1, ref p2, ref minDelta);
            CalcForecastDelta(PrevV, CurV, Param(a1 - 0.05F), Param(a2 - 0.05F), S, B, ref LastS, ref LastB, ref p1, ref p2, ref minDelta);
            a1 = p1;
            a2 = p2;
        }
        public static double CalcForecast(double V, ref double LastS, ref double LastB, float p1, float p2)
        {
            //Console.WriteLine("calc V=" + V.ToString("F") + " S=" + LastS.ToString("F") + " B=" + LastB.ToString("F") + " a=" + p1.ToString("F") + " b=" + p2.ToString("F"));
            double S = p1 * V + (1 - p1) * (LastS + LastB);
            LastB = p2 * (S - LastS) + (1 - p2) * LastB;
            LastS = S;
            return S + LastB;
        }
    }
    /// <summary>
    /// Запись параметров прогнозирования
    /// </summary>
    public class ForecastARH
    {
        public uint SiteID;
        public uint ObjectID;
        public uint DS_ID;
        public uint ParamID;
        public double LastValue;
        public double CollectValue;
        public DateTime CurDate = DateTime.MinValue;
        public double ForecastValue;
        public DateTime ForecastDate = DateTime.MinValue;
        public double LastS;
        public double LastB;
        public float p1;
        public float p2;

        public ForecastARH(uint Site, uint objectID, uint paramID, uint diagStateUni)
        {
            Trace.WriteLine("ForecastARH " + Site+" "+objectID+" "+paramID+" "+diagStateUni);
            SiteID = Site;
            DS_ID = diagStateUni;
            ObjectID = objectID;
            ParamID = paramID;
            p1 = 0.2F;
            p2 = 0.1F;
        }

        public override string ToString()
        {
            return "ARH LV=" + LastValue.ToString("F") + " CV=" + CollectValue.ToString("F") + "S=" + LastS.ToString("F") + " B=" + LastB.ToString("F") + " p1=" + p1.ToString("F") + " p2=" + p2.ToString("F");
        }

        /// <summary>
        /// Конструктор. Формирует запись прогнозирования на основе данных SQL 
        /// </summary>
        /// <param name="dr">Хранилище данных для инициализации</param>
        public ForecastARH(SqlDataReader dr)
        {
            DS_ID = Convert.ToUInt32(dr["DS_ID"]);
            SiteID = Convert.ToUInt32(dr["Site_ID"]);
            ObjectID = Convert.ToUInt32(dr["Object_ID"]);
            ParamID = Convert.ToUInt32(dr["Param_ID"]);
            LastValue = Convert.ToDouble(dr["LastExtr"]);
            CollectValue = Convert.ToDouble(dr["CurExtr"]);
            CurDate = Convert.ToDateTime(dr["CurExtrTime"]);
            LastS = Convert.ToDouble(dr["LastS"]);
            LastB = Convert.ToDouble(dr["LastB"]);
            p1 = Convert.ToSingle(dr["p1"]);
            p2 = Convert.ToSingle(dr["p2"]);
        }

        /// <summary>
        /// Обновить все данные прогнозирования. Не добавляет новую строку.
        /// </summary>
        public string FullUpdateSqlString
        {
            get
            {
                return "UPDATE dbo.ARH_Forecast SET CurExtr='" + FloatSQL(CollectValue) +
                "', CurExtrTime='" + CurDate +
                "', LastExtr='" + FloatSQL(LastValue) +
                "', Forecast='" + FloatSQL(ForecastValue) +
                "', ForecastDate='" + ForecastDate +
                "', LastS='" + FloatSQL(LastS) +
                "', LastB='" + FloatSQL(LastB) +
                "', p1='" + FloatSQL(p1) +
                "', p2='" + FloatSQL(p2) +
                "' WHERE Site_ID=" + SiteID + " AND Object_ID=" + ObjectID +
                " AND DS_ID=" + DS_ID + " AND Param_ID=" + ParamID;
            }
        }

        public string FullInsertSqlString
        {
            get
            {
                return "INSERT INTO dbo.ARH_Forecast VALUES(" + SiteID + "," + ObjectID + "," + ParamID + "," + DS_ID +
                "," + FloatSQL(LastValue) +
                "," + FloatSQL(CollectValue) +
                ",'" + CurDate +
                "'," + FloatSQL(ForecastValue) +
                "," + ((ForecastDate == DateTime.MinValue) ? "NULL" : "'"+ForecastDate.ToString()+"'") +
                "," + FloatSQL(LastS) +
                "," + FloatSQL(LastB) +
                "," + FloatSQL(p1) +
                "," + FloatSQL(p2) + ")";
            }
        }

        public int id = 0;
        public string Insert_ARH_StatesForecast
        {
            get
            {
                if (ForecastDate == DateTime.MinValue)
                    return "";
                return "INSERT INTO dbo.ARH_StatesForecast VALUES(" + SiteID + "," + ObjectID + "," +  DS_ID +
                ",'" + ForecastDate +
                "','" + (ForecastDate + TimeSpan.FromDays(1)) +
                "',1,0,0," + ParamID + ",NULL)";
            }
        }

        /// <summary>
        /// Строка SQL обновления информации текущего эктремума. Обновляет значение и время. Добавляет запись в случае отсутствия
        /// </summary>
        public string CurExtrUpdateSqlString
        {
            get
            {
                return "UPDATE dbo.ARH_Forecast SET CurExtr='" + FloatSQL(CollectValue) +
                "', CurExtrTime='" + CurDate +
                "' WHERE Site_ID=" + SiteID + " AND Object_ID=" + ObjectID +
                " AND DS_ID=" + DS_ID + " AND Param_ID=" + ParamID +
                " IF (@@ROWCOUNT = 0 ) INSERT INTO dbo.ARH_Forecast VALUES (" + SiteID +
                "," + ObjectID + "," + ParamID + "," + DS_ID + ",0," +
                FloatSQL(CollectValue) + ",'" + CurDate + "',0,'10.10.2000',0,0,0.2,0.1)";
            }
        }

        public string CurExtrUpdateSqlStringToday
        {
            get
            {
                return "UPDATE dbo.ARH_Forecast SET CurExtr='" + FloatSQL(CollectValue) +
                "', CurExtrTime='" + CurDate +
                "' WHERE Site_ID=" + SiteID + " AND Object_ID=" + ObjectID +
                " AND DS_ID=" + DS_ID + " AND Param_ID=" + ParamID +
                " AND DATEDIFF(day, CurExtrTime, '" + CurDate + "')=0" +
                " IF (@@ROWCOUNT = 0 ) INSERT INTO dbo.ARH_Forecast VALUES (" + SiteID +
                "," + ObjectID + "," + ParamID + "," + DS_ID + ",0," +
                FloatSQL(CollectValue) + ",'" + CurDate + "',0,NULL,0,0,0.2,0.1)";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        static public string FloatSQL(double value)
        {
            return value.ToString("F5").Replace(',', '.');
        }
    }

    /// <summary>
    /// Шаблон задачи диагностирования. Собдержит общие параметры из базы
    /// </summary>
    class ForecastTaskTmpl
    {
        public int _ID;
        /// <summary>
        /// контролируемая диагностическая ситуация dbo.TPD_DiagStatesUni
        /// </summary>
        public int _DiagSateUni;
        /// <summary>
        /// Тип параметра диагностирования dbo.TPD_ParamTypesUni
        /// </summary>
        public int _ParamTypeUni;
        /// <summary>
        /// Условия состояния объекта dbo.TDP_StatesUni
        /// </summary>
        public List<int> _ConditionStatesUni = new List<int>();
        /// <summary>
        /// 0 - занижение 1 - завышение
        /// </summary>
        public int _NormalType;
        /// <summary>
        /// Количество буферизируемых значений
        /// </summary>
        public int _BufferingSize;

        public void Read(SqlDataReader dr)
        {
            _ID = Convert.ToInt32(dr["ID"]);
            _DiagSateUni = Convert.ToInt32(dr["DiagStateUni"]);
            _ParamTypeUni = Convert.ToInt32(dr["ParamTypeUni"]);
            _NormalType = Convert.ToInt32(dr["NormalType"]);
            //_BufferingSize = Convert.ToInt32(dr["Buffer"]);
        }
    }

    /// <summary>
    /// Класс очереди для буферизации измерений
    /// </summary>
    public class MeasureQueue
    {
        private class MeasureItem
        {
            public MeasureItem(float val, DateTime tm, int objState)
            {
                Value = val;
                Time = tm;
                ObjState = objState;
            }
            public float Value;
            public DateTime Time;
            public int ObjState;
        }
        private Queue<MeasureItem> _Items = new Queue<MeasureItem>(2);
        private int _Size;
        private int _Delay;
        public MeasureQueue(int size, int delay)
        {
            _Size = size;
            _Delay = delay;
        }

        /// <summary>
        /// Добавить новое значение в очередь
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Time"></param>
        public void AddNewValue(float Value, DateTime Time, int objState)
        {
            // Добавляем новое измерение
            _Items.Enqueue(new MeasureItem(Value, Time, objState));
        }

        /// <summary>
        /// Получить выдержанное значение для обрработки
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Time"></param>
        /// <returns>true, если значние установлено в параметрах</returns>
        public bool GetProcessValue(ref float Value, ref DateTime Time, ref int objState)
        {
            // Если разница первого и последнего элемента > необходимой выдержки (состояния объекта)
            if ((_Items.Count > 1) &&
                (_Items.Last().Time - _Items.First().Time > TimeSpan.FromSeconds(_Delay)))
            {
                // извлекаем последний элемент
                MeasureItem mi = _Items.Dequeue();
                Value = mi.Value;
                Time = mi.Time;
                objState = mi.ObjState;
                return true;
            }
            // ждем заполнения очереди
            return false;
        }

        public bool LastMeasure(ref float Value, ref DateTime Time, ref int objState)
        {
            // Если разница первого и последнего элемента > необходимой выдержки (состояния объекта)
            if ( _Items.Count > 0 )
            {
                // извлекаем последний элемент
                MeasureItem mi = _Items.Last();
                Value = mi.Value;
                Time = mi.Time;
                objState = mi.ObjState;
                return true;
            }
            return false;
        }

        public void Clear()
        {
            _Items.Clear();
        }
    }
}
