﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Tdm.Unification
{
    public struct DiagScbItem
    {
        public byte GroupObj;
        public UInt32 TaskObject;
        public UInt16 ObjectID;
        public DateTime Time;
        public bool End;

        public DiagScbItem(byte groupObj, UInt32 taskObject, UInt16 objID, DateTime dt)
        {
            GroupObj = groupObj;
            TaskObject = taskObject;
            ObjectID = objID;
            Time = dt;
            End = false;
        }
    }

    /// <summary>
    /// Диагностические ситуации в унифицированном формате из прочтенного протокола
    /// </summary>
    public class ProtDiags
    {
        public int ObjectID { get; set; }
        public Tdm.BaseControlObj.DiagState UniDiagState { get; set; }
        public DateTime ProtocolTime { get; set; }
        public bool IsEnd = false;
        public UInt16 _adkID { get; set; }
        public UInt32 _adkState { get; set; }
        public byte _adkGroup { get; set; }
        public uint? _subGroup { get; set; }
        public uint? _group { get; set; } 

        public ProtDiags(int objectID, Tdm.BaseControlObj.DiagState ds, DateTime dt, bool isEnd)
        {
            ObjectID = objectID;
            UniDiagState = ds;
            ProtocolTime = dt;
            IsEnd = isEnd;
        }

        public ProtDiags(int objectID, Tdm.BaseControlObj.DiagState ds, DateTime dt, bool isEnd, UInt16 adkID, UInt32 adkState, byte adkGr):
            this(objectID, ds, dt, isEnd, adkID, adkState, adkGr, null, null){}

        public ProtDiags(int objectID, Tdm.BaseControlObj.DiagState ds, DateTime dt, bool isEnd, UInt16 adkID, UInt32 adkState, byte adkGr, uint? group, uint? subGroup)
        {
            ObjectID = objectID;
            UniDiagState = ds;
            ProtocolTime = dt;
            IsEnd = isEnd;

            _adkID = adkID;
            _adkState = adkState;
            _adkGroup = adkGr;

            _group = group;
            _subGroup = subGroup; 
        }
    }

    class BulkAdd
    {
        DataTable table;
        SqlConnection connection;
        protected string tableString;
        protected string cmndString;
        protected string sprocString;
        protected int SiteID;
        protected List<ProtDiags> _ProtDiagValues;

        protected void CreateLocalTable()
        {
            table = new DataTable("_TmpProtocolData");

            table.Columns.Add("ID", typeof(Int32));
            table.Columns.Add("adkGroup", typeof(Int32));
            table.Columns.Add("adkID", typeof(Int32));
            table.Columns.Add("adkState", typeof(Int64));
            table.Columns.Add("Site_ID", typeof(Int32));
            table.Columns.Add("ObjectID", typeof(Int32));
            table.Columns.Add("UniDStateID", typeof(Int32));
            table.Columns.Add("ProtocolTime", typeof(DateTime));
            table.Columns.Add("IsEnd", typeof(Boolean));
            table.Columns.Add("GroupID", typeof(Int32));
            table.Columns.Add("SubGroupID", typeof(Int32));
        }

        private string tableNameFind(string createString)
        {
            int begPosition = createString.ToUpper().IndexOf("TABLE #");
            for (int i = begPosition + 7; i < createString.Length; i++)
                if ((createString[i].ToString() == " ") || (createString[i].ToString() == "(") || (createString[i].ToString() == ";") || ((createString[i].ToString() == "\r") && (createString[i].ToString() == "\n")))
                {
                    return createString.Substring(begPosition + 7 - 1, i - (begPosition + 7 - 1));
                }
            return string.Empty;
        }

        public BulkAdd(ref List<ProtDiags> _ProtDiagValues, string cmndString, string sprocString, int SiteID)
        {
            CreateLocalTable();

            this._ProtDiagValues = _ProtDiagValues;
            this.cmndString = cmndString;
            this.tableString = tableNameFind(cmndString);
            this.sprocString = sprocString;
            this.SiteID = SiteID;
        }

        /// <summary>
        /// Создает объект BulkAdd для работы с таблицей #TechProt_{Site_ID}
        /// Для отсутствия пересечения по станциям
        /// </summary>
        /// <param name="_ProtDiagValues">Записи для передачи</param>
        /// <param name="sprocString">Имя хранимой процедуры</param>
        /// <param name="SiteID">ID станции</param>
        public BulkAdd(ref List<ProtDiags> _ProtDiagValues, string sprocString, int SiteID)
        {
            CreateLocalTable();

            this._ProtDiagValues = _ProtDiagValues;
            tableString = "#TechProt_" + SiteID.ToString();// tableNameFind(cmndString);
            cmndString = String.Format("if object_id('TempDB..{0}') is not null begin truncate table {0}; end else begin " +
                              "create table {0} (ID int, adkGroup int not null, adkID int not null, adkState bigint not null, Site_ID int not null, ObjectID int not null, UniDStateID int not null, ProtocolTime datetime not null, IsEnd bit not null) end;", tableString);//cmndString;
            this.sprocString = sprocString + " " + SiteID.ToString();

            this.SiteID = SiteID;
        }


        private void CreateServerTable()
        {
            SqlCommand crtTable = new SqlCommand(cmndString, connection);
            crtTable.CommandTimeout = 3600;
            crtTable.ExecuteNonQuery();
        }

        private void AddToServer()
        {
            DataRow row;
        
            int id = 1;
            foreach (ProtDiags elem in _ProtDiagValues)
            {
                row = table.NewRow();

                row["ID"] = id++;
                row["adkGroup"] = elem._adkGroup;
                row["adkID"] = elem._adkID;
                row["adkState"] = elem._adkState;
                row["Site_ID"] = SiteID;
                row["ObjectID"] = elem.ObjectID;
                row["UniDStateID"] = elem.UniDiagState.Id;
                row["ProtocolTime"] = elem.ProtocolTime;
                row["IsEnd"] = elem.IsEnd;
                row["GroupID"] = elem._group;
                row["SubGroupID"] = elem._subGroup;

                table.Rows.Add(row);
            }

            using (SqlBulkCopy loader = new SqlBulkCopy(connection))
            {
                loader.BulkCopyTimeout = 3600;
                loader.DestinationTableName = tableString;
                loader.WriteToServer(table);
            }
        }

        private void StoreToServer()
        {
            SqlCommand execProc = new SqlCommand("exec " + sprocString, connection);
            execProc.CommandTimeout = 3600;
            execProc.ExecuteNonQuery();
        }
        
        public void AddRecord(SqlConnection connection)
        {
            this.connection = connection;
            using (this.connection)
            {
                try
                {
                    CreateServerTable();
                    AddToServer();
                    Console.WriteLine("BulkAdd : Предано записей " + _ProtDiagValues.Count.ToString());
                    StoreToServer();

                    Console.WriteLine("BulkAdd : Данные обработаны ");
                }
                catch (Exception ex) 
                {
                    Console.WriteLine("BulkAdd : Ошибка записи данных в MS SQL: " + ex.Message);
                }
            }
        }
    }
    
}
