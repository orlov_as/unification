﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

using Ivk.IOSys;
using AfxEx;

namespace Tdm.Unification
{
    /// <summary>
    /// Класс обеспечивает обновление данных об отказах в базе данных MS SQL Server.
    /// Использование:
    /// 1) вызвать метод ConnectToDB с параметрами соединения с базой данных,
    /// 2) от метода GetFaultsFromSQL() получить список отказов, сохраненных в БД
    /// 3) методом SaveChangesToDB() - сохранять изменения в списке отказов, 
    ///    параметр - типа ResOfComparsion.
    /// </summary>

    public class DataProvider : DlxObject
    {
		/// <summary>
		/// Информация по обновлению данных в БД
		/// </summary>
		class DataUpdateInfo
		{
			/// <summary>
			/// Изменения
			/// </summary>
			public ResOfComparsion _res;
			/// <summary>
			/// Идентификатор станции
			/// </summary>
			public UInt32 _siteID;
		}

		#region Данные
        // параметры соединения с БД
        private string _strServer;
        private string _strSQLserver;
        private string _strDBname;
        private string _strTblName;
        private string _strUser;
        private string _strPswd;

		private int _reconnectCounter = int.MaxValue;

		/// <summary>
		/// Локальная таблдица по привязанным станциям
		/// </summary>
		protected DataTable _localTable = new DataTable();

        /// <summary>
        /// Соединение с БД
        /// </summary>
		private SqlConnection sqlConn = new SqlConnection();

		/// <summary>
		/// Список привязанных станций
		/// </summary>
		protected ArrayList _boundSites = new ArrayList();
		/// <summary>
		/// Таймер для выполнения синхронизации
		/// </summary>
		protected Timer _syncTimer = null;
		/// <summary>
		/// Очередь обновленя данных
		/// </summary>
		protected Queue _dataUpdateQueue = new Queue();
		#endregion

		#region Переопределенные методы из DlxObject
		
		/// <summary>
		/// Переопределена загрузка для получения информации о БД
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			// загрузка данных о БД
			// все парметры обязательные
			if (!Loader.GetAttributeValue("Server", ref _strServer))
				throw new ArgumentException("Не задан параметр Server для " + m_Name);

			if (!Loader.GetAttributeValue("SQLServer", ref _strSQLserver))
				throw new ArgumentException("Не задан параметр SQLServer для " + m_Name);

			_strDBname = Loader.GetAttributeString("DBName", "TestDB");
			_strTblName = Loader.GetAttributeString("TblName", "CurrentFaults");
			_strUser = Loader.GetAttributeString("User", "sa");
			_strPswd = Loader.GetAttributeString("Pswd", "1234567890");
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnSyncTimer);
			_syncTimer = new Timer(timerDelegate, null, 1000, 5000);

            // подготовка структуры локальной таблицы (добавление полей)
            _localTable = new DataTable("LocalCurrentFaults");
            
            _localTable.Columns.Add("SiteID", Type.GetType("System.UInt32"));
            _localTable.Columns.Add("ObjectID", Type.GetType("System.UInt16"));
            _localTable.Columns.Add("State", Type.GetType("System.UInt32"));
            _localTable.Columns.Add("Time", Type.GetType("System.DateTime"));

 			base.Create();
		}
		/// <summary>
		/// Завершение работы
		/// </summary>
		public override void NotifyDestroy()
		{
			// останавливаем таймер
			if( _syncTimer != null )
				_syncTimer.Dispose();
			// закрываем соединение
			Disconnect();
            base.NotifyDestroy();
		}

		#endregion

		#region Методы, вызываемые на потоке
		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
        /// Выполняет синхронизацию локальной таблицы и БД 
		/// </summary>
		public void OnSyncTimer(Object stateInfo)
		{
			DataUpdateInfo dataInfo = null;
			bool needUpdate = _dataUpdateQueue.Count > 0;
			// пока в очереди есть данные
			while (_dataUpdateQueue.Count > 0)
			{
				// блокируемся на очереди
				Monitor.Enter(_dataUpdateQueue);
				try
				{
					// получаем первый элемент в очереди
					dataInfo = _dataUpdateQueue.Dequeue() as DataUpdateInfo;
				}
				finally
				{
					// разблокируемся
					Monitor.Exit(_dataUpdateQueue);
				}
				// применяем изменения к локальной таблице
				SaveChangesToLocalTable(dataInfo._res, dataInfo._siteID);
			}

			if (!IsConnected && _reconnectCounter > 60)
			{
				if( !Connect() )
					_reconnectCounter++;
				else
					_reconnectCounter = 0;
			}
			// если были изменения или отсутствует подключение
			if (needUpdate && IsConnected)
				UpdateBDbyLocalTable(); // синхронизируем с БД
		}
		/// <summary>
		/// соединение с БД
		/// </summary>
		protected bool Connect()
		{
			if (IsConnected)
				return true;

			Monitor.Enter(sqlConn);
			try
			{
				if (IsConnected)
					return true;
			
				// настраиваем соединение с базой 
				sqlConn.ConnectionString = "server=" + _strServer + "\\" + _strSQLserver + "; user id=" + _strUser +
											   "; password=" + _strPswd + "; initial catalog = " + _strDBname;
				// откываем соединение
				sqlConn.Open();
				// очищаем данные в БД
				ClearBoundSitesDiagInfo();
			}
			catch (Exception ex)
			{
				Trace.WriteLine("DataProvider " + Name + ": ERROR open BD " + _strDBname + ": " + ex.Message);
				Disconnect();
			}
			finally
			{
				Monitor.Exit(sqlConn);
			}

			return IsConnected;
		}
		/// <summary>
		/// Закрыть соединение
		/// </summary>
		protected void Disconnect()
		{
			// закываем соединение
			sqlConn.Close();
		}
		/// <summary>
		/// Открыто ли соединение
		/// </summary>
		protected bool IsConnected
		{
			get
			{
				return sqlConn.State == ConnectionState.Open;
			}
		}

		/// <summary>
		/// Сохранение изменений в локальную таблицу
		/// </summary>
		/// <param name="res"></param>
		/// <param name="SiteID"></param>
		protected void SaveChangesToLocalTable(ResOfComparsion res, UInt32 SiteID)
		{
			// если нет данных
			if (res == null)
			{
				ClearLocalTable(SiteID);
				return;
			}

			Monitor.Enter(_localTable);
			try
			{
				// удаляем строки с ушедшими отказами
				int len = res.oldFaults.GetLength(0);
				for (int i = 0; i < len; i++)
				{
					//удаляем из _localTable
                    DataRow row = _localTable.NewRow();
                    row["SiteID"] = SiteID;
                    row["ObjectID"] = res.oldFaults[i].ID;
                    row["State"] = res.oldFaults[i].State;
                    row["Time"] = res.oldFaults[i].Time;
                    _localTable.Rows.Remove(row);
				}

				// добавляем строки с новыми отказами
				len = res.newFaults.GetLength(0);
				for (int i = 0; i < len; i++)
				{
					//добавдяем строку в _localTable
                    DataRow row = _localTable.NewRow();
                    row["SiteID"] = SiteID;
                    row["ObjectID"] = res.newFaults[i].ID;
                    row["State"] = res.newFaults[i].State;
                    row["Time"] = res.newFaults[i].Time;
                    _localTable.Rows.Add(row);                    
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("DataProvider " + Name + ": SaveChangesToLocalTable ex: " + ex.ToString());
			}
			finally
			{
				Monitor.Exit(_localTable);
			}
		}

		/// <summary>
		/// Выполнение синхронизации локальной таблицы с таблицей в БД
		/// </summary>
		protected void UpdateBDbyLocalTable()
		{
			if (!IsConnected)
				return;
			
			Monitor.Enter(sqlConn);

			try
			{
				if (!IsConnected)
					return;

				// очищаем БД от отказов по связанным станциям
				ClearBoundSitesDiagInfo();
				// переносим данные из _localTable в БД
				SqlBulkCopy bulk = new SqlBulkCopy(sqlConn);
				bulk.DestinationTableName = _strTblName;
				bulk.BulkCopyTimeout = 5;
				bulk.WriteToServer(_localTable);
				bulk.Close();
			}
			catch (Exception ex)
			{
				Trace.WriteLine("DataProvider " + Name + ": ERROR update BD " + _strDBname + ": " + ex.Message);
				Disconnect();
			}
			finally
			{
				Monitor.Exit(sqlConn);
			}
		}
		
		/// <summary>
		/// Удаление записей из локальной таблицы по SiteID
		/// </summary>
		/// <param name="SiteID">идентификатор станции</param>
		protected void ClearLocalTable(UInt32 SiteID)
		{
			Monitor.Enter(_localTable);
			try
			{
                // ищем строки по критерию
                string criteria = "SiteID = " + SiteID;
                DataRow[] foundRows;
                foundRows = _localTable.Select(criteria);

                // удаляем строки из таблицы _localTable
                for (int i = 0; i < foundRows.GetLength(0); i++)
                {
                    _localTable.Rows.Remove(foundRows[i]);
                }
            }
			catch (Exception ex)
			{
				Debug.WriteLine("DataProvider " + Name + ": ClearLocalTable ex: " + ex.ToString());
			}
			finally
			{
				Monitor.Exit(_localTable);
			}
		}

		/// <summary>
		/// Очистка таблицы отказов по привязанным станциям
		/// </summary>
		protected void ClearBoundSitesDiagInfo()
		{
			if (!IsConnected)
				return;

			foreach (UInt32 siteID in _boundSites)
				ClearTable(_strTblName, siteID);
		}

		/// <summary>
		/// Очистка таблицы TableName от записей с SiteID
		/// </summary>
		/// <param name="TableName"></param>
		/// <param name="SiteID"></param>
        protected void ClearTable(string TableName, UInt32 SiteID)
        {
			try
			{
				if (!IsConnected)
					return;

				SqlCommand cmd = sqlConn.CreateCommand();
				// формируем Delete-запрос
				cmd.Parameters.Add("@SiteID", SqlDbType.Int);
				cmd.Parameters["@SiteID"].Value = SiteID;
				string delString = "DELETE FROM " + TableName +
								   " WHERE SiteID = @SiteID";
				cmd.CommandText = delString;
				cmd.ExecuteNonQuery();            
			}
			catch(Exception ex)
			{
				Trace.WriteLine("DataProvider " + Name + ": ERROR clear BD " + _strDBname + ": " + ex.Message);
				// разрыв
				Disconnect();
			}
        }

		#endregion

		#region Методы для пользователей
		/// <summary>
		/// Связать со станцией
		/// </summary>
		/// <param name="SiteID"></param>
		public void BindSite(UInt32 SiteID)
		{
			_boundSites.Add(SiteID);
		}

		/// <summary>
		/// Добавление изменеий в данных в очередь обработки
		/// </summary>
		/// <param name="res"></param>
		/// <param name="SiteID"></param>
		public void SaveChanges(ResOfComparsion res, UInt32 SiteID)
        {
			DataUpdateInfo dataInfo = new DataUpdateInfo();
			dataInfo._res = res;
			dataInfo._siteID = SiteID;

			Monitor.Enter(_dataUpdateQueue);
			try
			{
				_dataUpdateQueue.Enqueue(dataInfo);
			}
			catch(Exception ex)
			{
				Debug.WriteLine("DataProvider " + Name + ": SaveChanges ex: " + ex.ToString());
			}
			finally
			{
				Monitor.Exit(_dataUpdateQueue);
			}
        }

		#endregion
	}

}
