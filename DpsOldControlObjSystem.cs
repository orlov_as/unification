﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using AfxEx;
using Ivk.IOSys;
using LibEx;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Tdm.Unification
{
	#region Структуры объектов СЦБ
	/// <summary>
	/// состояние объекта
	/// </summary>
	public class DpsOldDiagState
	{
		/// <summary>
		/// Диагностическое состояние
		/// </summary>
		public uint State;
		/// <summary>
		/// Идентификатор объекта
		/// </summary>
		public ushort ID;
		/// <summary>
		/// Время
		/// </summary>
		public DateTime Time;

        public override string ToString()
        {
            return string.Format("ID={0},\tState={1}, \tTime={2}", ID, State, Time);
        }
	}

	/// <summary>
	/// структура для результатов сравнения текущих отказов и сохраненных
	/// в БД MS SQL Server
	/// </summary>
	public class ResOfComparsion
	{
		/// <summary>
		/// Новые сбои
		/// </summary>
		public DpsOldDiagState[] newFaults;
		/// <summary>
		/// Прекратившиеся сбои
		/// </summary>
		public DpsOldDiagState[] oldFaults;
        /// <summary>
        /// Длящиеся сбои
        /// </summary>
        public DpsOldDiagState[] curFaults;
    }
	/// <summary>
	/// Описание конвертации диангностичекого состояния
	/// </summary>
	public class UniDiagInfo
	{
		/// <summary>
		/// Диагностическое состояние по-умолчанию
		/// </summary>
		public uint UniDiag;
		/// <summary>
		/// Условие выбора сосотяния
		/// </summary>
		public class UniDiagCondition
		{
            /// <summary>
            /// Условие на унифицированный тип объекта
            /// </summary>
            public uint UniType;
            /// <summary>
			/// Диагностическое состояния по условию
			/// </summary>
			public uint UniDiag;
			/// <summary>
			/// Имя objdata для сравнения значения
			/// </summary>
			public string SignalName;
			/// <summary>
			/// Тип сравнения: less if Compare == -1; more if Compare == 1; equal if Compare == 0
			/// </summary>
			public int Compare;
			/// <summary>
            /// Номер нормали для сравнения: 0 - Ext, 1 - Min; 2 - Max
			/// </summary>
			public int NormIndex;
            /// <summary>
            /// Значение для сравнения с сигналом
            /// </summary>
            public int Value = 0;
		}
		/// <summary>
		/// Массив дополнительных условий на выбор диагностического состояния
		/// </summary>
		public List<UniDiagCondition> DiagConditions;
	}
	/// <summary>
	/// Информация о соответствии к унифицированному объекту
	/// </summary>
	public class UniObjInfo
	{
		/// <summary>
		/// Объект по умолчанию
		/// </summary>
		public UniObject Obj;
		/// <summary>
		/// Идентификатор унифицрованного объекта
		/// </summary>
		public int UniObjID;
        /// <summary>
        /// Имя унифицрованного объекта
        /// </summary>
        public string UniObjName;
        /// <summary>
		/// Класс проверки условия на выбор объекта
		/// </summary>
		public class UniObjCondition
		{
			/// <summary>
			/// Объект
			/// </summary>
			public UniObject Obj;
			/// <summary>
			/// Идентификатор унифицрованного объекта
			/// </summary>
			public int UniObjID;
			/// <summary>
			/// по диагностике
			/// </summary>
			public bool ByDiag;
			/// <summary>
			/// ID сбоя АДК
			/// </summary>
			public uint AdkDiag;

			/// <summary>
			/// по сигналу
			/// </summary>
			public bool BySig;
			/// <summary>
			/// Имя сигнала
			/// </summary>
			public string SigName;
			/// <summary>
			/// Тип сравнения: less if Compare == -1; more if Compare == 1; equal if Compare == 0
			/// </summary>
			public int Compare;
			/// <summary>
			/// Значение
			/// </summary>
			public double Value;
		}
		/// <summary>
		/// Список дополнительных ассоциаций
		/// </summary>
		public List<UniObjCondition> ObjConditions;
	}

	#endregion

	/// <summary>
	/// Класс получения соотвествия диагностики АДК-СЦБ и унифицированной
	/// </summary>
	public class AdkUniDiagAssociations : DlxObject
	{
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			// загружаем соответствие типов объектов и отказов АДК с унифицированными ситуациями
			LoadTypeAssociations(Loader);
		}
		/// <summary>
		/// Получить информацию по подходящим диагностическим ситуациям
		/// </summary>
		/// <param name="AdkType">тип объекта АДК-СЦБ</param>
		/// <param name="diagState">диагностическое состояние АДК-СЦБ</param>
		/// <returns></returns>
		public virtual UniDiagInfo GetDiagAssociation(uint AdkType, uint diagState)
		{
			return _diagAdkTOdiagUniMAP[AdkType][diagState];
		}
        public virtual UniDiagInfo TryGetDiagAssociation(uint AdkType, uint diagState)
        {
            try
            {
                return GetDiagAssociation(AdkType, diagState);
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        /// <summary>
        /// загрузка соответствия типов объектов и отказов АДК с унифицированными ситуациями
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        protected void LoadTypeAssociations(DlxLoader Loader)
		{
			string FilePath = Loader.GetAttributeString("TypeAssociation", string.Empty);
			if (FilePath.Length != 0)
			{
				FilePath = Path.Combine(Loader.CurXmlElement.GetFilePath().Substring(0, Loader.CurXmlElement.GetFilePath().LastIndexOf("\\")), FilePath);
				FilePath = Path.GetFullPath(FilePath);

				DlxLoader subLoader = new DlxLoader();
				XmlElement StartElem = subLoader.XmlRoot.ParseFile(FilePath);
				subLoader.CurXmlElement = StartElem;

				List<XmlElement> Elemets;
				subLoader.CurXmlElement.EnumInhElements(out Elemets);

				uint AdkType = 0, AdkDiag = 0, UniDiag = 0;
				Dictionary<uint, UniDiagInfo> mapforType;
				foreach (XmlElement elem in Elemets)
				{
					if (elem.GetAttributeValue("AdkGroup", ref AdkType)
						&& elem.GetAttributeValue("AdkDiag", ref AdkDiag)
						&& elem.GetAttributeValue("UniDiag", ref UniDiag))
					{
						if (!_diagAdkTOdiagUniMAP.TryGetValue(AdkType, out mapforType))
						{
							mapforType = new Dictionary<uint, UniDiagInfo>();
							_diagAdkTOdiagUniMAP[AdkType] = mapforType;
						}

						UniDiagInfo di = new UniDiagInfo();
						di.UniDiag = UniDiag;
						mapforType[AdkDiag] = di;

						List<XmlElement> SubElemets;
						elem.EnumInhElements(out SubElemets);
						foreach (XmlElement addelem in SubElemets)
						{
							UniDiagInfo.UniDiagCondition dc = new UniDiagInfo.UniDiagCondition();
							//if (!addelem.GetAttributeValue("When", ref dc.SignalName) && !addelem.GetAttributeValue("Where", ref dc.SignalName))
                            if (!addelem.GetAttributeValue("When", ref dc.SignalName) && !addelem.GetAttributeValue("when", ref dc.SignalName))
                            {
                                if (!addelem.GetAttributeValue("UniType", ref dc.UniType))
                                    continue;
                            }
                            if (!addelem.GetAttributeValue("UniDiag", ref dc.UniDiag) && !addelem.GetAttributeValue("unidiag", ref dc.UniDiag))
								continue;

							bool compare = false;
							if (addelem.GetAttributeValue("less", ref compare) && compare)
								dc.Compare = -1;
							else if (addelem.GetAttributeValue("more", ref compare) && compare)
								dc.Compare = 1;
							else
								dc.Compare = 0;

							switch (addelem.GetAttribute("norm"))
							{
								case "ext": dc.NormIndex = 0; break;
								case "min": dc.NormIndex = 1; break;
								case "max": dc.NormIndex = 2; break;
							}

                            addelem.GetAttributeValue("Value", ref dc.Value);

							if (di.DiagConditions == null)
								di.DiagConditions = new List<UniDiagInfo.UniDiagCondition>();

							di.DiagConditions.Add(dc);
						}
					}
					else
						Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to load item of diag association");
				}
			}
		}
		/// <summary>
		/// Создание обратного словаря (унифицированные -> АДК-СЦБ)
		/// </summary>
		public void MakeInversDictionary()
		{
			if (_diagAdkTOdiagUniMAP != null)
			{
				_diagUniTOdiagAdkMAP = new Dictionary<uint, uint>();
				_diagUniTOdiagAdkGroupMAP = new Dictionary<uint, uint>();
				foreach (KeyValuePair<uint, Dictionary<uint, UniDiagInfo>> didic in _diagAdkTOdiagUniMAP)
				{
					foreach (KeyValuePair<uint,UniDiagInfo> diagAdk in didic.Value)
					{
						UniDiagInfo di = diagAdk.Value;
						_diagUniTOdiagAdkMAP[di.UniDiag] = diagAdk.Key;
						_diagUniTOdiagAdkGroupMAP[di.UniDiag] = didic.Key;
						if (di.DiagConditions != null)
						{
							foreach (UniDiagInfo.UniDiagCondition dc in di.DiagConditions)
							{
								_diagUniTOdiagAdkMAP[dc.UniDiag] = diagAdk.Key;
								_diagUniTOdiagAdkGroupMAP[di.UniDiag] = didic.Key;
							}
						}
					}
				}
			}
		}
		/// <summary>
		/// Получить сбой АДК по унифицированному
		/// </summary>
		/// <param name="UniDiagID">унифицированная ситуация</param>
		/// <returns></returns>
		public virtual uint GetAdkDiag(uint UniDiagID, out uint AdkGroup)
		{
			AdkGroup = 0;
			if (_diagUniTOdiagAdkMAP == null || _diagUniTOdiagAdkGroupMAP == null)
				return 0;
			uint AdkId;
			if (_diagUniTOdiagAdkMAP.TryGetValue(UniDiagID, out AdkId) && _diagUniTOdiagAdkGroupMAP.TryGetValue(UniDiagID, out AdkGroup))
				return AdkId;
			else
				return 0;
		}

		/// <summary>
		/// Map для соответствия диагностики АДК в унифицированную
		/// </summary>
		public Dictionary<uint, Dictionary<uint, UniDiagInfo>> _diagAdkTOdiagUniMAP = new Dictionary<uint, Dictionary<uint, UniDiagInfo>>();
		/// <summary>
		/// Map для соответствия диагностики унифицированную в АДК
		/// </summary>
		protected Dictionary<uint, uint> _diagUniTOdiagAdkMAP = null;
		/// <summary>
		/// Map для соответствия диагностики унифицированную в группу сбоев АДК
		/// </summary>
		protected Dictionary<uint, uint> _diagUniTOdiagAdkGroupMAP = null;
	}

	/// <summary>
	/// Класс получения соотвествия диагностики АДК-СЦБ и унифицированной
	/// </summary>
	public class AdkUniObjsAssociations : DlxObject
	{
		/// <summary>
		/// Установить подсистему объектов
		/// </summary>
		/// <param name="ObjsOwner"></param>
		public void SetObjsOwner(IOBaseElement objsOwner)
		{
			_objsOwner = objsOwner;
		}
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			// загружаем признак прямых ассоциаций - без преобразования
			_isSafeObjsAssociation = Loader.GetAttributeBool("isSafeObjsAssociation", false);

			if (_isSafeObjsAssociation)
				return;

			string FilePath = Loader.GetAttributeString("ObjsAssociation", "ObjectsAssociation.xml");
			// загружаем соответствие типов объектов и отказов АДК с унифицированными ситуациями
			LoadObjsAssociations(Loader, _objsOwner, FilePath);
		}
		/// <summary>
		/// Инициализация - если есть признак прямых ассоциаций - без преобразования, то формируем словари самостоятельно
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			if (_isSafeObjsAssociation && _objsOwner != null)
			{
				_objsAdkTOtypeAdkMAP = new Dictionary<uint, uint>();
				_objsAdkTOobjsUniMAP = new Dictionary<uint, UniObjInfo>();
				_adkObjList = new Dictionary<uint, string>();
				foreach (IOBaseElement el in _objsOwner.SubElements.Objects)
				{
					UniObject obj = el as UniObject;
					if (obj != null )
					{
						UniObjInfo oi = new UniObjInfo();

						oi.Obj = obj;
						oi.UniObjID = obj.Number;
						oi.UniObjName = obj.Name;

						_adkObjList.Add((uint)obj.Number, obj.Name);

						_objsAdkTOtypeAdkMAP[(uint)obj.Number] = (uint)obj.Type.SubGroupID;
						_objsAdkTOobjsUniMAP[(uint)obj.Number] = oi;
					}
				}
                RebuildObjIdByNameList();
			}

			base.Create();
		}
		/// <summary>
		/// загрузка соответствия объектов
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		/// <param name="ObjsOwner">подсистема ввода</param>
		/// <param name="FilePath">путь к файлу ассоциаций</param>
		public void LoadObjsAssociations(DlxLoader Loader, IOBaseElement ObjsOwner, string FilePath)
		{
			if (FilePath.Length != 0 )
			{
				FilePath = Path.Combine(Loader.CurXmlElement.GetFilePath().Substring(0, Loader.CurXmlElement.GetFilePath().LastIndexOf("\\")), FilePath);
				FilePath = Path.GetFullPath(FilePath);

				if (!File.Exists(FilePath))
					return;

				DlxLoader subLoader = new DlxLoader();
				XmlElement StartElem = subLoader.XmlRoot.ParseFile(FilePath);
				subLoader.CurXmlElement = StartElem;

				List<XmlElement> Elemets;
				subLoader.CurXmlElement.EnumInhElements(out Elemets);

				_objsAdkTOobjsUniMAP = new Dictionary<uint, UniObjInfo>();
				_objsAdkTOtypeAdkMAP = new Dictionary<uint, uint>();
                _adkObjList = new Dictionary<uint, string>();

				uint AdkObj = 0, AdkType = 0, UniObj = 0;
                string AdkName = string.Empty;
                string UniObjRef = string.Empty;
				foreach (XmlElement elem in Elemets)
				{
					if (elem.GetAttributeValue("AdkGroup", ref AdkType)
						&& elem.GetAttributeValue("AdkID", ref AdkObj))
					{
                        if (elem.GetAttributeValue("AdkName", ref AdkName))
                            if (!_adkObjList.ContainsKey(AdkObj))
                                _adkObjList.Add(AdkObj, AdkName);

						UniObjInfo oi = null;
						if (!_objsAdkTOobjsUniMAP.TryGetValue(AdkObj, out oi))
							oi = new UniObjInfo();

						if (elem.GetAttributeValue("UniID", ref UniObj))
						{
							oi.UniObjID = (int)UniObj;
							if (ObjsOwner != null)
							{
								foreach (IOBaseElement el in ObjsOwner.SubElements.Objects)
								{
									UniObject obj = el as UniObject;
									if (obj != null && obj.Number == UniObj)
									{
										oi.Obj = obj;
										break;
									}
								}
								if (oi.Obj == null)
								{
									Trace.WriteLine("Не найден унифицированный объект ID=" + UniObj.ToString() + " на станции " + Name);
								}
							}
						}
						else
							oi.UniObjID = -1;

						_objsAdkTOtypeAdkMAP[AdkObj] = AdkType;
						_objsAdkTOobjsUniMAP[AdkObj] = oi;

						List<XmlElement> SubElemets;
						elem.EnumInhElements(out SubElemets);
						foreach (XmlElement addelem in SubElemets)
						{
							UniObjInfo.UniObjCondition oc = new UniObjInfo.UniObjCondition();

							uint id = 0;
							if (addelem.GetAttributeValue("UniID", ref id))
							{
								oc.UniObjID = (int) id;
								if (ObjsOwner != null)
								{
									foreach (IOBaseElement el in ObjsOwner.SubElements.Objects)
									{
										UniObject obj = el as UniObject;
										if (obj != null && obj.Number == id)
										{
											oc.Obj = obj;
											break;
										}
									}
									if (oc.Obj == null)
									{
										Trace.WriteLine("Не найден унифицированный объект ID=" + UniObj.ToString() + " на станции " + Name);
										continue;
									}
								}
							}
							else
								continue;

							if (addelem.GetAttributeValue("bydiag", ref oc.AdkDiag))
								oc.ByDiag = true;

                            if (addelem.GetAttributeValue("When", ref oc.SigName) || addelem.GetAttributeValue("Where", ref oc.SigName))
							{
                                oc.BySig = true;
								bool compare = false;
								if (addelem.GetAttributeValue("less", ref compare) && compare)
									oc.Compare = -1;
								else if (addelem.GetAttributeValue("more", ref compare) && compare)
									oc.Compare = 1;
								else
									oc.Compare = 0;

								addelem.GetAttributeValue("value", ref oc.Value);
							}

							if (oi.ObjConditions == null)
								oi.ObjConditions = new List<UniObjInfo.UniObjCondition>();

							oi.ObjConditions.Add(oc);
						}
					}
					else
						Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to load item of diag association");
				}
                RebuildObjIdByNameList();
			}
		}

        /// <summary>
        /// Построить список соответствий имён объектов АДК их идентификаторам
        /// </summary>
        public void RebuildObjIdByNameList()
        {
            _adkObjIdByNameList = new Dictionary<string, uint>();
            foreach (var item in _adkObjList)
            {
                if (!_adkObjIdByNameList.ContainsKey(item.Value))
                    _adkObjIdByNameList.Add(item.Value, item.Key);
            }
        }

		/// <summary>
		/// Найти все унифицированные объекты, для которых нужно активировать диагностичекое состоянияе
		/// </summary>
		/// <param name="objId"></param>
		/// <param name="diagState"></param>
		/// <returns></returns>
		public List<UniObject> GetUniObjects(UInt16 objId, UInt32 diagState, FaultActionType action)
		{
			List<UniObject> List = new List<UniObject>();
			UniObjInfo oi;
			if (_objsAdkTOobjsUniMAP.TryGetValue(objId, out oi))
			{
				if (oi.ObjConditions == null)
				{
					if (oi.Obj != null)
						List.Add(oi.Obj);
				}
				else
				{
					foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
					{
						if (oc.ByDiag && diagState != oc.AdkDiag)
							continue;
						if (oc.BySig)
						{
                            if (action == FaultActionType.Reset)
                            {
                                if (oc.Obj != null)
                                    List.Add(oc.Obj);
                                continue;
                            }

                            //UniObjData data = oc.Obj.ObjDataCollection.GetObject(oc.SigName) as UniObjData;
                            ObjData data = oc.Obj.ObjDataCollection.GetObject(oc.SigName) as ObjData;
                            // если такое член данное есть и это аналоговый сигнал
							if (data != null && data.IsSigAnalog)
							{
								if ((data.SigAnalog.Double.CompareTo(oc.Value) * oc.Compare < 0) || (data.SigAnalog.Double.CompareTo(oc.Value) - oc.Compare == 0))
									continue;
							}
							else if (data != null && data.IsSigDiscretLogic)
							{
								double curvalue = 0;
								if (data.SigDiscreteLogic.On)
									curvalue = 1;
								else if (data.SigDiscreteLogic.Blink)
									curvalue = 2;

                                if (curvalue.CompareTo(oc.Value) != oc.Compare)
                                    continue;
							}
							else
								continue;
						}
						if( oc.Obj != null)
							List.Add(oc.Obj);
					}
					if (List.Count == 0 && oi.Obj != null)
						List.Add(oi.Obj);
				}
			}
			return List;
		}
        /// <summary>
        /// Найти все унифицированные объекты, для которых нужно активировать диагностичекое состоянияе
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="diagState"></param>
        /// <returns></returns>
        public List<UniObject> GetPossibleUniObjects(UInt16 objId, UInt32 diagState)
        {
            List<UniObject> List = new List<UniObject>();
            UniObjInfo oi;
            if (_objsAdkTOobjsUniMAP.TryGetValue(objId, out oi))
            {
                if (oi.ObjConditions == null)
                {
                    if (oi.Obj != null)
                        List.Add(oi.Obj);
                }
                else
                {
                    foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
                    {
                        if (oc.Obj != null)
                            List.Add(oc.Obj);
                    }
                    if (List.Count == 0 && oi.Obj != null)
                        List.Add(oi.Obj);
                }
            }
            return List;
        }

		/// <summary>
		/// Получить тип объекта АДК-СЦБ
		/// </summary>
		/// <param name="AdkObjID">идентификатор объекта АДК-СЦБ</param>
		/// <returns></returns>
		public uint GetAdkType(uint AdkObjID)
		{
			return _objsAdkTOtypeAdkMAP[AdkObjID];
		}
		/// <summary>
		/// Проверка наличия ассоциаций
		/// </summary>
		public bool HaveAssociations
		{
			get { return _objsAdkTOobjsUniMAP != null; }
		}
		/// <summary>
		/// Создание обратного словаря соответсвтия объектов
		/// </summary>
		public void MakeInversDictionary()
		{
			if (HaveAssociations)
			{
				_objsUniToobjAdkMAP = new Dictionary<uint, uint>();

				foreach (uint AdkID in _objsAdkTOobjsUniMAP.Keys)
				{
					UniObjInfo oi = _objsAdkTOobjsUniMAP[AdkID];
					if (oi.UniObjID != -1)
						_objsUniToobjAdkMAP[(uint) oi.UniObjID] = AdkID;

					if (oi.ObjConditions != null)
					{
						foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
						{
							if (oc.ByDiag && oc.AdkDiag == 0)
								continue;
							_objsUniToobjAdkMAP[(uint)oc.UniObjID] = AdkID;
						}
					}
				}
			}
		}
		/// <summary>
        /// Получить ID объекта АДК по ID унифицированного объекта
		/// </summary>
		/// <param name="UniObjID">идентификатор унифицированного объекта</param>
		/// <returns></returns>
		public int GetAdkObjectID(uint UniObjID)
		{
            // сначала ищем точное соответствие
            int FindAdkID = -1;
            foreach (uint AdkID in _objsAdkTOobjsUniMAP.Keys)
            {
                UniObjInfo oi = _objsAdkTOobjsUniMAP[AdkID];
                // если идентификатор объекта совпадает
                if (oi.UniObjID == UniObjID)
                {
                    return (int)AdkID;
                }
            }
            // теперь ищем только по условиям
            foreach (uint AdkID in _objsAdkTOobjsUniMAP.Keys)
            {
                UniObjInfo oi = _objsAdkTOobjsUniMAP[AdkID];
                // проверяем доп условия
                if (oi.ObjConditions != null)
                {
                    foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
                    {
                        // если идентификаторы не совпадают, то дальше
                        if (oc.UniObjID != UniObjID)
                            continue;
                        // если по условию и оно совпадает
                        if (oc.ByDiag)
                            return (int)AdkID;
                        else if (!oc.ByDiag)// не по условию - возвращаем объект
                            return (int)AdkID;
                    }
                }
            }
            return FindAdkID;
		}
		/// <summary>
        /// Получить ID объекта АДК по ID унифицированного объекта
		/// </summary>
		/// <param name="UniObjID">идентификатор унифицированного объекта</param>
		/// <returns></returns>
        public string GetAdkObjectName(uint AdkID)
        {
            string res = string.Empty;
            if (_adkObjList.ContainsKey(AdkID))
                res = _adkObjList[AdkID];
            return res;
        }

        /// <summary>
        /// Получить ID объекта АДК по его имени
        /// </summary>
        /// <param name="adkName">имя АДК объекта</param>
        /// <returns>идентификатор объекта АДК</returns>
        public uint GetAdkObjectIdByName(string adkName)
        {
            if (_adkObjIdByNameList.ContainsKey(adkName))
                return _adkObjIdByNameList[adkName];
            return 0;
        }

		/// <summary>
		/// Получить объект АДК по унифицированному
		/// </summary>
		/// <param name="UniObjID">идентификатор унифицированного объекта</param>
		/// <param name="AdkDiag">идентификатор сбоя АДК</param>
		/// <returns></returns>
		public int GetAdkObjectID(uint UniObjID, uint AdkDiag)
		{
			int FindAdkID = -1;
			foreach (uint AdkID in _objsAdkTOobjsUniMAP.Keys)
			{
				UniObjInfo oi = _objsAdkTOobjsUniMAP[AdkID];
				if (oi.UniObjID == UniObjID)
					FindAdkID = (int)AdkID;

				if (oi.ObjConditions != null)
				{
					foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
					{
                        if( oc.UniObjID != UniObjID )
                            continue;
						if (oc.ByDiag && oc.AdkDiag == AdkDiag)
							return (int)AdkID;
					}
				}
			}
			return FindAdkID;
		}
		/// <summary>
		/// Получить объект АДК по унифицированному с учетом типа объекта АДК-СЦБ
		/// </summary>
		/// <param name="UniObjID">идентификатор унифицированного объекта</param>
		/// <param name="AdkDiag">идентификатор сбоя АДК</param>
		/// <returns></returns>
        public int GetAdkObjectID(uint UniObjID, uint AdkDiag, uint AdkGroup)
		{
			int FindAdkID = -1;
			foreach (uint AdkID in _objsAdkTOobjsUniMAP.Keys)
			{
				UniObjInfo oi = _objsAdkTOobjsUniMAP[AdkID];
				// если тип объекта совпадает

                if (!_objsAdkTOtypeAdkMAP.ContainsKey(AdkID) || AdkGroup != _objsAdkTOtypeAdkMAP[AdkID])
					continue;
				// если идентификатор объекта совпадает
				if (oi.UniObjID == UniObjID)
					FindAdkID = (int)AdkID;
				// проверяем доп условия
				if (oi.ObjConditions != null)
				{
					foreach (UniObjInfo.UniObjCondition oc in oi.ObjConditions)
					{
						// если идентификаторы не совпадают, то дальше
						if (oc.UniObjID != UniObjID)
							continue;
						// если по условию и оно совпадает
						if (oc.ByDiag && oc.AdkDiag == AdkDiag)
							return (int)AdkID;
						else if(!oc.ByDiag)// не по условию - возвращаем объект
							return (int)AdkID;
					}
				}
			}
			return FindAdkID;
		}
		/// <summary>
		/// MAP для соответствия объектов АДК с унифицированными объектами
		/// </summary>
		public Dictionary<uint, UniObjInfo> _objsAdkTOobjsUniMAP = null;
		/// <summary>
		/// MAP для соответствия объектов АДК и типов объектов АДК
		/// </summary>
		protected Dictionary<uint, uint> _objsAdkTOtypeAdkMAP = null;
		/// <summary>
		/// Подсистема объектов
		/// </summary>
		protected IOBaseElement _objsOwner = null;
		/// <summary>
		/// Признак использования прямых ассоциаций - без конвертации
		/// </summary>
		protected bool _isSafeObjsAssociation = false;
		/// <summary>
		/// MAP для обратного соответствия объектов
		/// </summary>
		protected Dictionary<uint, uint> _objsUniToobjAdkMAP = null;
        /// <summary>
        /// Словарь ID и имен объектов АДК
        /// </summary>
        protected Dictionary<uint, string> _adkObjList = null;
        /// <summary>
        /// Словарь АДК ID по именам объектов АДК
        /// </summary>
        protected Dictionary<string, uint> _adkObjIdByNameList = null;
        /// <summary>
        /// Тип действия над сбоем АДК
        /// </summary>
        public enum FaultActionType
        {
            /// <summary>
            /// Начало
            /// </summary>
            Set = 0,
            /// <summary>
            /// Завершение
            /// </summary>
            Reset,
            /// <summary>
            /// Проверка
            /// </summary>
            Check
        }
    }

	/// <summary>
	/// Класс получения соответсвующего унифицированному объекту объекта АДК-СЦБ
	/// </summary>
	public class UniAdkAssociations : DlxObject
	{
		/// <summary>
		/// словарь станций в АДК-СЦБ по именам станций в ДДЦ 
		/// </summary>
		protected Dictionary<string, string> _sitenameToadknameMAP = new Dictionary<string, string>();
		/// <summary>
		/// словарь станций в АДК-СЦБ по идентификаторам станций в ДДЦ 
		/// </summary>
		protected Dictionary<int, string> _siteidToadknameMAP = new Dictionary<int,string>();
		/// <summary>
		/// словарь ассоциаций объектов по именам станций в ДДЦ 
		/// </summary>
		protected Dictionary<string, AdkUniObjsAssociations> _sitenameToobjaccordMAP = new Dictionary<string, AdkUniObjsAssociations>();
		/// <summary>
		/// словарь ассоциаций объектов по идентификаторам станций в ДДЦ 
		/// </summary>
		protected Dictionary<int, AdkUniObjsAssociations> _siteidToobjaccordMAP = new Dictionary<int, AdkUniObjsAssociations>();
		/// <summary>
		/// ссылка на коллекцию станций
		/// </summary>
		protected DlxObject _siteList = null;
		/// <summary>
		/// Словарь ассоциаций унифицированных диагностических ситуаций
		/// </summary>
		protected AdkUniDiagAssociations _diagAssoc = null;
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxObject;

			string value = Loader.GetAttributeString("AdkUniDiagAssociations", "/App/Common/AdkUniDiagAssociations");
			_diagAssoc = Loader.GetObject(value, typeof(AdkUniDiagAssociations), null, false) as AdkUniDiagAssociations;
			if (_diagAssoc != null)
				_diagAssoc.MakeInversDictionary();

			List<XmlElement> Subs;
			Loader.CurXmlElement.EnumInhElements(out Subs, "site", null, null);
			foreach (XmlElement elem in Subs)
			{
				// <site adkname="Ачкасово" ddcname="Ачкасово" dir="Ачкасово" >
				// если dir не задано, то dir совпадает с ddcname,
				// если ddcname не задано, то ddcname сопадает с name
				string adkname = string.Empty, ddcname = string.Empty, dir = string.Empty;
				if (elem.GetAttributeValue("adkname", ref adkname))
				{
					if (!elem.GetAttribute("ddcname", out ddcname))
						ddcname = adkname;
					if (!elem.GetAttribute("dir", out dir))
						dir = ddcname;

					_sitenameToadknameMAP[ddcname] = adkname;
					AdkUniObjsAssociations auo = new AdkUniObjsAssociations();
					auo.LoadObjsAssociations(Loader, null, dir + "/" + "ObjectsAssociation.xml");
					auo.MakeInversDictionary();
					_sitenameToobjaccordMAP[ddcname] = auo;
				}
			}
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>true - в случае успеха</returns>
		public override void Create()
		{
			foreach (string ddcname in _sitenameToadknameMAP.Keys)
			{
				DlxObject siteobj = _siteList.FindObject(ddcname);
				Site site = siteobj as Site;
				if (site != null)
				{
					_siteidToadknameMAP[site.ID] = _sitenameToadknameMAP[ddcname];
					_siteidToobjaccordMAP[site.ID] = _sitenameToobjaccordMAP[ddcname];
				}
			}

			_sitenameToadknameMAP.Clear();
			_sitenameToadknameMAP = null;
			_sitenameToobjaccordMAP.Clear();
			_sitenameToobjaccordMAP = null;

			//string name;
			//int id;
			//bool bres = GetAdkObject(11279, 175, out name, out id);

			//uint diag;
			//bres = GetAdkObjectAndDiag(11279, 175, 1, out name, out id, out diag);


			base.Create();
		}
		/// <summary>
		/// Получить объект АДК по унифицированному
		/// </summary>
		/// <param name="SiteID">идентификатор станции унифиц</param>
		/// <param name="UniObjID">идентификаор объекта унифиц</param>
		/// <param name="AdkSiteName">имя станции в системе АДК-СЦБ</param>
		/// <param name="AdkObjID">идентификатор объекта на станции в системе АДК-СЦБ</param>
		/// <returns>true - в случае успеха</returns>
        public bool GetAdkObject(int SiteID, int UniObjID, out string AdkSiteName, out int AdkObjID)
		{
			try
			{
				AdkSiteName = _siteidToadknameMAP[SiteID];
				AdkUniObjsAssociations oa = _siteidToobjaccordMAP[SiteID];
                AdkObjID = oa.GetAdkObjectID((uint)UniObjID);

                if (AdkObjID >= 0)
					return true;
				else
					return false;
			}
			catch
			{
				AdkSiteName = string.Empty;
				AdkObjID = -1;
                return false;
			}
		}
        /// <summary>
        /// Получить объект АДК по унифицированному
        /// </summary>
        /// <param name="SiteID">идентификатор станции унифиц</param>
        /// <param name="UniObjID">идентификаор объекта унифиц</param>
        /// <param name="AdkSiteName">имя станции в системе АДК-СЦБ</param>
        /// <param name="AdkObjID">идентификатор объекта на станции в системе АДК-СЦБ</param>
        /// <param name="AdkObjName">Имя объекта на станции в системе АДК-СЦБ</param>
        /// <returns>true - в случае успеха</returns>
        public bool GetAdkObject(int SiteID, int UniObjID, out string AdkSiteName, out int AdkObjID, out string AdkObjName)
        {
            try
            {
                AdkSiteName = _siteidToadknameMAP[SiteID];
                AdkUniObjsAssociations oa = _siteidToobjaccordMAP[SiteID];
                AdkObjID = oa.GetAdkObjectID((uint)UniObjID);
                AdkObjName = oa.GetAdkObjectName((uint)AdkObjID);

                if (AdkObjID >= 0)
                    return true;
                else
                    return false;
            }
            catch
            {
                AdkSiteName = string.Empty;
                AdkObjID = -1;
                AdkObjName = string.Empty;
                return false;
            }
        }
        /// <summary>
		/// Получить объект АДК и сбой по унифицированному
		/// </summary>
		/// <param name="SiteID">идентификатор станции унифиц</param>
		/// <param name="UniObjID">идентификаор объекта унифиц</param>
		/// <param name="UniDiag">идентификаор ситуации унифиц</param>
		/// <param name="AdkSiteName">имя станции в системе АДК-СЦБ</param>
		/// <param name="AdkObjID">идентификатор объекта на станции в системе АДК-СЦБ</param>
		/// <param name="AdkDiag">идентификатор сбоя в системе АДК-СЦБ</param>
		/// <returns></returns>
		public bool GetAdkObjectAndDiag(int SiteID, int UniObjID, uint UniDiag, out string AdkSiteName, out int AdkObjID, out uint AdkDiag, out uint AdkGroup)
		{
			AdkSiteName = string.Empty;
			AdkObjID = -1;
			AdkDiag = 0;
			AdkGroup = 0;
			try
			{
				AdkSiteName = _siteidToadknameMAP[SiteID];
				AdkUniObjsAssociations oa = _siteidToobjaccordMAP[SiteID];
				
				if (_diagAssoc != null)
					AdkDiag = _diagAssoc.GetAdkDiag(UniDiag, out AdkGroup);
				else
					AdkDiag = 0;

				if (AdkDiag != 0)
				{
					AdkObjID = oa.GetAdkObjectID((uint)UniObjID, AdkDiag, AdkGroup);
					if (AdkObjID < 0)
						AdkObjID = oa.GetAdkObjectID((uint)UniObjID);
				}
				else
					AdkObjID = oa.GetAdkObjectID((uint)UniObjID);
				
				if (AdkObjID >= 0)
					return true;
				else
					return false;
			}
			catch
			{
				return false;
			}
		}

        /// <summary>
        /// Получить идентификатор унифицированного объекта по идентификатору объекта АДК
        /// </summary>
        /// <param name="siteId">идентификатор станции</param>
        /// <param name="adkObjId">идентификатор объекта АДК</param>
        /// <returns>идентификатор унифицированного объекта</returns>
        public int GetUniIdFromAdkId(int siteId, uint adkObjId)
        {
            if ((_siteidToobjaccordMAP != null) && (_siteidToobjaccordMAP.ContainsKey(siteId)))
            {
                var adkUniObjAssoc = _siteidToobjaccordMAP[siteId];
                if ((adkUniObjAssoc != null) && (adkUniObjAssoc._objsAdkTOobjsUniMAP != null))
                {
                    if (adkUniObjAssoc._objsAdkTOobjsUniMAP.ContainsKey(adkObjId))
                        return adkUniObjAssoc._objsAdkTOobjsUniMAP[adkObjId].UniObjID;
                }
            }
            return -1;
        }

	    /// <summary>
	    /// Получить идентификатор объекта АДК по его имени
	    /// </summary>
	    /// <param name="siteId">идентификатор станции</param>
        /// <param name="adkObjName">имя объекта АДК</param>
	    /// <returns>идентификатор объекта АДК</returns>
	    public int GetAdkIdFromAdkName(int siteId, string adkObjName)
        {
            if ((_siteidToobjaccordMAP != null) && (_siteidToobjaccordMAP.ContainsKey(siteId)))
            {
                var adkUniObjAssoc = _siteidToobjaccordMAP[siteId];
                if ((adkUniObjAssoc != null) && (adkUniObjAssoc._objsAdkTOobjsUniMAP != null))
                {
                    return (int)adkUniObjAssoc.GetAdkObjectIdByName(adkObjName);
                }
            }
            return -1;
        }
	}

	public interface IDpsAssociations
	{
		AdkUniObjsAssociations ObjsAdktoUniAssoc { get; }
	}
	/// <summary>
	/// Класс подсистемы объектов при взаимодействии со старым Dps
	/// </summary>
	public class DpsOldControlObjSystem : UniObjSystem, IDpsAssociations
	{
		/// <summary>
		/// Переопределена загрузка для получения информации о БД
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
            base.Load(Loader);

			string value = string.Empty;
			// загрузка данных о БД
			if (Loader.GetAttributeValue("DataProvider", ref value))
			{
				_dataProvider = Loader.GetObjectFromAttribute("DataProvider") as DataProvider;
				_isBDStoring = true;
			}
            
            // соответствия диагностических состояний
            value = Loader.GetAttributeString("AdkUniDiagAssociations", "/App/Common/AdkUniDiagAssociations");
            _diagAssoc = Loader.GetObject(value, typeof(AdkUniDiagAssociations), null, false) as AdkUniDiagAssociations;

			_isConvertToUni = Loader.GetAttributeBool("ConvertToUni", _isConvertToUni);
            if (_isConvertToUni)
            {   // загружаем соответствие объектов
				_objsAdktoUniAssoc.SetObjsOwner(this);
				_objsAdktoUniAssoc.Load(Loader);
            }
        }
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void PreCreate()
		{
            _objsAdktoUniAssoc.PreCreate();

			if (_dataProvider != null)
				_dataProvider.BindSite(_siteID);
 			base.PreCreate();
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			_objsAdktoUniAssoc.Create();

			base.Create();
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		public override void NotifyDestroy()
		{
            _objsAdktoUniAssoc.NotifyDestroy();
            base.NotifyDestroy();
		}
		/// <summary>
		/// После разрушения
		/// </summary>
        public override void NotifyPostDestroy()
		{
            _objsAdktoUniAssoc.NotifyPostDestroy();
            base.NotifyPostDestroy();
		}

		#region Обработка данных
		/// <summary>
		/// Переопределен разбор данных (базовый вызов не делается)
		/// </summary>
		/// <param name="reader">поток данных</param>
		public override void ProcessData(BinaryReader reader)
		{
			// пропускаем 14 байт
			reader.ReadBytes(14);
            // разбор данных от ДПС-сервера
			DpsOldDiagState[] Faults = GetObjectsSCB(reader, DataTime);

            LogFaults(Faults, SiteID, "proc");

            // сравниваем данные ДПС-сервера с данными из БД
            ResOfComparsion res = _comparer.CompareData(Faults, LocalDiagCopy);

            // проверяем наличие изменений, вносим их в базу данных
            if ((res.newFaults.Length > 0) || (res.oldFaults.Length > 0) || res.curFaults.Length > 0)
            {
				SaveChangesToLocalCopy(res);
				if (_dataProvider != null)
					_dataProvider.SaveChanges(res, _siteID);
            }
			_localDiagCopy = Faults;
		    IsValidData = false;
		}
        /// <summary>
        /// Запись в лог-файл полученных сбоев АДК
        /// </summary>
        /// <param name="faults"></param>
        protected void LogFaults(DpsOldDiagState[] faults, uint siteID, string action)
        {
            return;

            string fName = string.Format("_AdkFaults_{0}_{1}.txt", siteID, action);
            System.IO.FileInfo fi = new System.IO.FileInfo(fName);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(fName, true);
            sw.WriteLine("-------------------------\n" + (faults.Length > 0 ? faults[0].Time.ToString() : "0"));
            try
            {
                foreach (DpsOldDiagState f in faults)
                        sw.WriteLine("{0}\t{1}\t{2}", f.ID, f.State, action);
            }
            catch (Exception ex)
            {
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
            }
            sw.Flush();
            sw.Close();
        }
        /// <summary>
        /// Подготовка перечня сбоев АДК, которые не должны приводить к завершению унифицированных ДС
        /// </summary>
        /// <param name="gone">Массив завершившихся сбоев АДК</param>
        /// <param name="present">Массив длящихся сбоев АДК</param>
        /// <returns>Перечень сбоев АДК, которые не должны приводить к завершению унифицированных ДС</returns>
        protected List<DpsOldDiagState> SelectFaultsToIgnore(DpsOldDiagState[] gone, DpsOldDiagState[] present)
        {
            List<DpsOldDiagState> res = new List<DpsOldDiagState>();
            // словарь сбоев по объектам АДК
            Dictionary<ushort, List<DpsOldDiagState>> goneByObj = GetFaultsByObjDictionary(gone);
            Dictionary<ushort, List<DpsOldDiagState>> presentByObj = GetFaultsByObjDictionary(gone);

            //foreach (ushort objID in goneByObj.Keys)
            //{
            //    uint AdkType = _objsAdktoUniAssoc.GetAdkType(objID);
            //    foreach (DpsOldDiagState fault in goneByObj[objID])
            //    {
            //        UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, fault.State);
                    
            //        _diagAssoc.GetAdkDiag(di.UniDiag, 


            //    }
            //}
            return res;
        }
        protected Dictionary<ushort, List<DpsOldDiagState>> GetFaultsByObjDictionary(DpsOldDiagState[] faults)
        {
            Dictionary<ushort, List<DpsOldDiagState>> res = new Dictionary<ushort, List<DpsOldDiagState>>();
            foreach (DpsOldDiagState f in faults)
            {
                if (!res.ContainsKey(f.ID))
                    res.Add(f.ID, new List<DpsOldDiagState>());
                res[f.ID].Add(f);
            }
            return res;      
        }

		/// <summary>
		/// Установить неизвестное состояние
		/// </summary>
		public override void SetInvalidData()
		{
			base.SetInvalidData();
			//_localDiagCopy = new DpsOldDiagState[0];; 
			if (_dataProvider != null)
				_dataProvider.SaveChanges(null, _siteID);
		}
		/// <summary>
		/// Сохранение изменений в локальную копию
		/// </summary>
		/// <param name="res"></param>
		public void SaveChangesToLocalCopy(ResOfComparsion res)
		{
			// сбрасываем ДС по завершившимся сбоям
            for (int i = 0; i < res.oldFaults.Length; i++)
				ResetObjectDiagState(res.oldFaults[i].ID, res.oldFaults[i].State);

			// создаем ДС по появившимся сбоям
            for (int i = 0; i < res.newFaults.Length; i++)
				SetObjectDiagState(res.newFaults[i].ID, res.newFaults[i].State);

            // проверяем ДС по длящимся сбоям
            for (int i = 0; i < res.curFaults.Length; i++)
                CheckObjectDiagState(res.curFaults[i].ID, res.curFaults[i].State, res.curFaults);
        }

		/// <summary>
		/// Принимает объект типа BinaryReader и время прихода данных
		/// возвращает массив структур с данными о, отказах объектов СЦБ
		/// </summary>
        static public DpsOldDiagState[] GetObjectsSCB(BinaryReader bnReader, DateTime RecieveTime)
        {
            // размер одного блока данных SCB (об одном объекте)
            int BlockSizeSCB = 6;

            // проверяем число структур ObjectSCB
            int ObjectSCB_Num;
            // считываем размер блока байт со структурами ObjectSCB
            int SCBDataSize = bnReader.ReadUInt16(); // размер всех данных SCB
            if ((SCBDataSize % BlockSizeSCB) == 0)
            {
                ObjectSCB_Num = SCBDataSize / BlockSizeSCB;
            }
            else
            {
                throw new Exception("Данные SCB содержат нецелое число структур ObjectSCB /n SCBDataSize=" + Convert.ToString(SCBDataSize) + " не делится на " + BlockSizeSCB + " без остатка");
            }

            // формируем массив структур ObjectSCB
            DpsOldDiagState[] temp_obj = new DpsOldDiagState[ObjectSCB_Num];
            for (int i = 0; i < ObjectSCB_Num; i++)
            {
                // данные из структур - считывать в порядке State, ID
                temp_obj[i] = new DpsOldDiagState();
                temp_obj[i].State = bnReader.ReadUInt32();
                temp_obj[i].ID = bnReader.ReadUInt16();
            }

            // Анализируем .State на наличие нескольких отказов у одного объекта
            // инициализируем временные массивы
            ArrayList tempID = new ArrayList();
            ArrayList tempState = new ArrayList();
            // считываем данные
            for (int i = 0; i < ObjectSCB_Num; i++)
            {
                uint number = temp_obj[i].State;
                uint mask = 1;
                while (number >= mask && mask > 0)
                {
                    if ((number & mask) > 0)
                    {
                        // записываем в отказы
                        tempID.Add(temp_obj[i].ID);
                        tempState.Add(mask);
                    }
                    mask <<= 1;
                }
            }

            // инициализируем результирующий массив
            int count = tempID.Count;
            DpsOldDiagState[] obj = new DpsOldDiagState[count];
            for (int i = 0; i < count; i++)
            {
                obj[i] = new DpsOldDiagState();
                obj[i].ID = (ushort)tempID[i];
                obj[i].State = (uint)tempState[i];
                obj[i].Time = RecieveTime;
            }
            return obj;
        }

		/// <summary>
		/// Установить диагностичекие состояние у объектов по заданной конвертации
		/// </summary>
		/// <param name="obj">объект</param>
		/// <param name="di">параметры конвертации</param>
		static public void SetUniObjDiag(UniObject obj, UniDiagInfo di)
		{
            try
            {           
                if (di.DiagConditions == null )
                {   // если нет дополнительных условий
				    obj.ActivateObjDiagState((int) di.UniDiag);
			    }
			    else
			    {
				    foreach (UniDiagInfo.UniDiagCondition dc in di.DiagConditions)
                    {   // цикл по условиям активации ДС
                    
                        if (dc.UniType == obj.Type.SubGroupID)
                        {   // если в условии указан униф.тип, и он совпадает с типом данного объекта
                            obj.ActivateObjDiagState((int)dc.UniDiag);
                            return;
                        }
                    
                        ObjData data = null;
					    if (dc.SignalName == "FIRST")
						    foreach (DlxObject od in obj.ObjDataCollection.Objects)
						    {
							    data = od as ObjData;
							    if (data != null && data.IsSigAnalog)
								    break;
						    }
					    else
						    data = obj.ObjDataCollection.GetObject(dc.SignalName) as ObjData;
					
                        if (data != null)
                        {   // если элемент с заданным именем найден...
                            if (data.IsSigAnalog)
                            {   // ...и это унифицированный параметр
                                UniObjData ud = data as UniObjData;
                                try
                                {
                                    if (ud._normalMin == float.MaxValue || ud._normalMax == float.MaxValue)
                                    {
                                        #region Отладочный лог
                                        #endregion
                                    }

                                    // в зависимости от нормали проверяем значение
                                    switch (dc.NormIndex)
                                    {
                                        case 0:
                                            if (data.SigAnalog.Double.CompareTo(ud._normalExt) * dc.Compare > 0 || data.SigAnalog.Double.CompareTo(ud._normalExt) == dc.Compare)
                                            {   // ...то активируем ДС, если выполнено условие для нормали Ext
                                                obj.ActivateObjDiagState((int)dc.UniDiag);
                                                return;
                                            }
                                            break;
                                        case 1:
                                            if (data.SigAnalog.Double.CompareTo(ud._normalMin) * dc.Compare > 0 || data.SigAnalog.Double.CompareTo(ud._normalMin) == dc.Compare)
                                            {   // ...то активируем ДС, если выполнено условие для нормали Min
                                                obj.ActivateObjDiagState((int)dc.UniDiag);
                                                return;
                                            }
                                            break;
                                        case 2:
                                            if (data.SigAnalog.Double.CompareTo(ud._normalMax) * dc.Compare > 0 || data.SigAnalog.Double.CompareTo(ud._normalMax) == dc.Compare)
                                            {   // ...то активируем ДС, если выполнено условие для нормали Max
                                                obj.ActivateObjDiagState((int)dc.UniDiag);
                                                return;
                                            }
                                            break;
                                    }
                                }
                                catch
                                { }
                            }
                            else if (data.IsSigDiscretLogic)
                            {   // ...и это подзадача контроля...
                                if ((data.SigDiscreteLogic.On ? 1 : 0) == dc.Value && dc.Compare == 0)
                                {   // ...то активируем ДС, если на выходе ПЗК - значение, равное заданному
                                    obj.ActivateObjDiagState((int)dc.UniDiag);
                                    return;
                                }
                            }
                        }
				    }
				    // устанавливаем ДС по умолчанию
				    obj.ActivateObjDiagState((int) di.UniDiag);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Can't set DS {0} for {1}!", di.UniDiag, obj.Name));
            }
        }

		/// <summary>
		/// Снять диагностичекие состояние у объектов по заданной конвертации
		/// </summary>
		/// <param name="obj">объект</param>
		/// <param name="di">параметры конвертации</param>
		static public void RemoveUniObjDiag(UniObject obj, UniDiagInfo di)
		{
			// если нет дополнительных условий
			if (di.DiagConditions == null)
			{
				obj.DeactivateObjDiagState((int) di.UniDiag);
			}
			else
			{
				DateTime time;
				// если ситуация по умолчанию активна, то снимаем её
				if (obj._diagStateTOtimeMAPuni.TryGetValue((int)di.UniDiag, out time))
					obj.DeactivateObjDiagState((int) di.UniDiag);
				// цикл по условиям
				foreach (UniDiagInfo.UniDiagCondition dc in di.DiagConditions)
				{
					// если такая ситуация активна, то снимаем её
					if (obj._diagStateTOtimeMAPuni.TryGetValue((int) dc.UniDiag, out time))
						obj.DeactivateObjDiagState((int) dc.UniDiag);
				}
			}
		}
		/// <summary>
		/// Распечатать диагностическое состояние в лог
		/// </summary>
		/// <param name="objId">объект</param>
		/// <param name="diagState">диагностическое состояние</param>
		static public void TraceDiagToLog(DateTime time, UInt16 objId, UInt32 diagState, string filepostfix)
		{
            return;
            StreamWriter sw = new StreamWriter("необработаны" + filepostfix + ".txt", true, System.Text.Encoding.Default);
			sw.WriteLine(time.ToString("yyyy:MM:dd HH:mm:ss\t") + objId + "\t" + diagState);
			sw.Flush();
			sw.Close();
		}

		/// <summary>
		/// Создать ДС
		/// </summary>
		/// <param name="objId">идентификатор объекта</param>
		/// <param name="diagState">диагностическое состояние</param>
        public virtual void SetObjectDiagState(UInt16 objId, UInt32 diagState)
		{
            if (_isConvertToUni)
            {
                if (_diagAssoc == null || !_objsAdktoUniAssoc.HaveAssociations)
                {
                    TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                    return;
                }

                try
                {   // получаем словарь диагностики АДК по типу АДК
                    uint AdkType = _objsAdktoUniAssoc.GetAdkType(objId);
                    // получаем описание конвертации по типу диагностики АДК
                    UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, diagState);
                    // получаем унифицированные объекты по объекту АДК и диагностике АДК
                    List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(objId, diagState, AdkUniObjsAssociations.FaultActionType.Set);
                    // для каждого объекта устанавливаем диагностическое состояние
                    foreach (UniObject obj in objs)
                    {
                        SetUniObjDiag(obj, di);
                    }
                    if (objs.Count == 0)
                        TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                }
                catch (KeyNotFoundException)
                {
                    //Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to find association to obj or diag: " + ex.Message);
                    TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                }
            }
            else
			{
				IOBaseElement elem = null;
				if (_idobjTOrefMAPuni.TryGetValue(objId, out elem))
				{
                    if (elem is UniObject)
                    {
                        if (_diagAssoc == null)
                            (elem as UniObject).ActivateObjDiagState((int)diagState);
                        else
                            try
                            {   // получаем описание конвертации по типу диагностики АДК
                                UniDiagInfo di = _diagAssoc.GetDiagAssociation((uint)(elem as UniObject).Type.SubGroupID, diagState);
                                SetUniObjDiag((elem as UniObject), di);
                            }
                            catch (KeyNotFoundException)
                            {
                                //Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to find association to obj or diag: " + ex.Message);
                                TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                            }
                    }
                    else if (elem is BaseControlObj)
                    {
                        BaseControlObj.DiagState diag = new BaseControlObj.DiagState();
                        diag.Id = (int)diagState;
                        if ((elem as BaseControlObj).Site != null)
                            diag.Sign = (elem as BaseControlObj).Site.TypeInfo.DiagStateSign(elem as BaseControlObj, (int)diagState);
                        else
                            diag.Sign = 5;
                        (elem as BaseControlObj).AddDiagState(diag, DataTime);

                        // передаем подсистеме
                        OnObjectDiagState((UInt32)objId, (UInt16)diagState, DataTime, DateTime.MinValue);
                    }
				}
			}

		}

        /// <summary>
        /// Завершить ДС
        /// </summary>
        /// <param name="objId">идентификатор объекта</param>
        /// <param name="diagState">диагностическое сосотяние</param>
        public virtual void ResetObjectDiagState(UInt16 objId, UInt32 diagState)
        {
            if (_isConvertToUni)
            {
                if (_diagAssoc != null && _objsAdktoUniAssoc.HaveAssociations)
                    try
                    {
                        // получаем словарь диагностики АДК по типу АДК
                        uint AdkType = _objsAdktoUniAssoc.GetAdkType(objId);
                        // получаем описание конвертации по типу диагностики АДК
                        UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, diagState);
                        // получаем унифицированные объекты по объекту АДК и диагностике АДК
                        List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(objId, diagState, AdkUniObjsAssociations.FaultActionType.Reset);
                        // для каждого объекта устанавливаем диагностическое состояние
                        foreach (UniObject obj in objs)
                        {
                            //if (obj.DiagStates.Count(ds => ds.Id == di.UniDiag) > 0)
                                RemoveUniObjDiag(obj, di);
                        }
                    }
                    catch (KeyNotFoundException)
                    {
                        //Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to find association to obj or diag: " + ex.Message);
                    }
            }
            else
            {
                IOBaseElement elem = null;
                if (_idobjTOrefMAPuni.TryGetValue(objId, out elem))
                {
                    if (elem is UniObject)
                    {
                        if (_diagAssoc == null)
                            (elem as UniObject).DeactivateObjDiagState((int)diagState);
                        else
                        {
                            try
                            {
                                // получаем описание конвертации по типу диагностики АДК
                                UniDiagInfo di = _diagAssoc.GetDiagAssociation((uint)(elem as UniObject).Type.SubGroupID, diagState);
                                RemoveUniObjDiag((elem as UniObject), di);
                            }
                            catch (KeyNotFoundException)
                            {
                                //Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to find association to obj or diag: " + ex.Message);
                                TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                            }
                        }
                    }
                    else if (elem is BaseControlObj)
                    {
                        (elem as BaseControlObj).RemoveDiagState((int)diagState);
                        // передаем подсистеме
                        OnObjectDiagState((UInt32)objId, (UInt16)diagState, DateTime.MinValue, DataTime);
                    }
                }
            }

        }

        /// <summary>
        /// Проверить ДС
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="diagState"></param>
        public virtual void CheckObjectDiagState(UInt16 objId, UInt32 diagState, DpsOldDiagState[] allFaults)
        {
            if (_isConvertToUni)
            {
                if (_diagAssoc == null || !_objsAdktoUniAssoc.HaveAssociations)
                {
                    TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                    return;
                }

                try
                {   // получаем словарь диагностики АДК по типу АДК
                    uint AdkType = _objsAdktoUniAssoc.GetAdkType(objId);
                    // получаем описание конвертации по типу диагностики АДК
                    UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, diagState);
                    // получаем все возможные унифицированные объекты по объекту АДК
                    List<UniObject> allObjs = _objsAdktoUniAssoc.GetPossibleUniObjects(objId, diagState);
                    // получаем унифицированные объекты по объекту АДК и диагностике АДК
                    List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(objId, diagState, AdkUniObjsAssociations.FaultActionType.Check);
                    // проверяем каждый объект
                    foreach (UniObject obj in allObjs)
                    {   
                        List<UniObject> uObjs = FindDiagObjects(objId, diagState, allFaults);

                        if (obj.DiagStates.Count(ds => ds.Id == di.UniDiag) > 0 && objs.Count(o => obj.Number == o.Number) == 0 && uObjs.Count(o => obj.Number == o.Number) == 0)
                        {   // если ДС активна на этом объекте, 
                            // но сейчас формироваться не должна (не выполняются условия преобразования сбоев в ДС)
                            // - завершаем ДС
                            RemoveUniObjDiag(obj, di);
                        }
                        else if (obj.DiagStates.Count(ds => ds.Id == di.UniDiag) == 0 && objs.Count(o => obj.Number == o.Number) > 0)
                        {   // если ДС не активна на этом объекте, 
                            // но сейчас формироваться должна (выполняются условия преобразования сбоев в ДС)
                            // - активизируем ДС
                            SetUniObjDiag(obj, di);
                        }
                    }
                }
                catch (KeyNotFoundException)
                {
                    //Debug.WriteLine("DpsOldControlObjSystem: " + Owner.Name + ": Error to find association to obj or diag: " + ex.Message);
                    TraceDiagToLog(DataTime, objId, diagState, Owner.Name);
                }
            }
            
        }

        protected List<UniObject> FindDiagObjects(UInt16 objId, UInt32 diagState, DpsOldDiagState[] allFaults)
        {
            List<UniObject> res = new List<UniObject>();
            foreach (DpsOldDiagState f in allFaults)
            {
                if (f.ID == objId && f.State != diagState)
                {
                    List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(f.ID, f.State, AdkUniObjsAssociations.FaultActionType.Check);
                    foreach (UniObject u in objs)
                        if (res.Count(r => r.Number == u.Number) == 0)
                            res.Add(u);
                }
            }
            return res;
        }

		#endregion

		#region Данные
		/// <summary>
		/// Признак необходимости записи данных в БД
		/// </summary>
		protected bool _isBDStoring;
		/// <summary>
		/// признак конвертации идентификаторов в унифицированный формат
		/// </summary>
		protected bool _isConvertToUni = true;

		/// <summary>
        /// локальная копия списка отказов
        /// </summary>
        protected DpsOldDiagState[] _localDiagCopy = new DpsOldDiagState[0];
		/// <summary>
		/// Локальная копия данных об отказах
		/// </summary>
		public DpsOldDiagState[] LocalDiagCopy
		{
			get { return _localDiagCopy; }
		}

		/// <summary>
		/// провайдер к базе данных
		/// </summary>
		protected DataProvider _dataProvider = null;
		/// <summary>
		/// Компаратор
		/// </summary>
		protected ComparerDPS _comparer = new ComparerDPS();

		/// <summary>
		/// Словарь соответсвия объектов
		/// </summary>
		public AdkUniObjsAssociations _objsAdktoUniAssoc = new AdkUniObjsAssociations();
		public AdkUniObjsAssociations ObjsAdktoUniAssoc
		{
			get { return _objsAdktoUniAssoc; }
		}

		/// <summary>
		/// Словарь соответсвия диагностики
		/// </summary>
		public AdkUniDiagAssociations _diagAssoc = null;

		#endregion
    }
	/// <summary>
	/// Класс станциии АДК-СЦБ - проверяет корректность типов объектов
	/// </summary>
	public class DpsOldSite : Tdm.Site
	{
		public override void CheckType(ObjType type)
		{
			if(type.GroupID != 1001 )
				throw new ArgumentException("Недопустимый тип группы объекта - " + type.GroupID.ToString());
		}
	}
    /// <summary>
    /// Класс сравнения данных, полученных от ДПС-сервера и от БД SQL.
    /// Возвращает структуру с массивами новых (пришедших) и устаревших (ушедших) отказов
    /// </summary>
    public class ComparerDPS
    {
        // флаг появления изменений в списке отказов
        private bool[] IsChanged = new bool[2];

        // возвращает массив несовпадающих значений,
        // которые есть в master-массиве, но отсутствуют в slave-массиве
        // param=0 при определении новых отказов, param=1 при определении устаревших отказов
        private DpsOldDiagState[] Comparator(DpsOldDiagState[] master, DpsOldDiagState[] slave, byte param)
        {
            // кол-во отказов 1)в памяти, 2)прочитанных с сервера MS SQL
            int NumOfMasterObjs = master.GetLength(0);
            int NumOfSlaveObjs = slave.GetLength(0);
            int NumOfRes = 0;

            // результирующий массив
            DpsOldDiagState[] result = new DpsOldDiagState[NumOfMasterObjs];

            for (int i = 0; i < NumOfMasterObjs; i++)
            {
                // счетчик числа несовпадений
                int count = 0;

                for (int j = 0; j < NumOfSlaveObjs; j++)
                {
                    if ((master[i].ID == slave[j].ID) &&
                        (master[i].State == slave[j].State))
                    {
                        count++;
                    }
                }

                // проверяем наличие несовпадений в массивах
                if (count == 0)
                {
                    // такого отказа нет, -> добавляем его в результирующий массив
					result[NumOfRes] = new DpsOldDiagState();
                    result[NumOfRes].ID = master[i].ID;
                    result[NumOfRes].State = master[i].State;
                    result[NumOfRes].Time = master[i].Time;
                    NumOfRes++;
                }
                // если есть несовпадения, устанавливаем IsChanged[param]
                if (NumOfRes != 0)
                {
                    IsChanged[param] = true;
                }
            }

            // сравниваем число результатов и длину результирующего массива,
            // возвращаем результат
            if (NumOfRes < NumOfMasterObjs)
            {
                // новый результирующий массив
                DpsOldDiagState[] newresult = new DpsOldDiagState[NumOfRes];
                for (int i = 0; i < NumOfRes; i++)
                {
                    newresult[i] = result[i];
                }
                // возвращаем структуры с данными
                return newresult;
            }
            else
            {
                // возвращаем структуры с данными
                return result;
            }
        }

        // возвращает результаты сравнения данных от СКД и от БД
        // (определяются появившиеся и ушедшие отказы)
        public ResOfComparsion CompareData(DpsOldDiagState[] adkFaults, DpsOldDiagState[] localFaults)
        {
			IsChanged[0] = false;
			IsChanged[1] = false;

            // массивы для записи в структуру
            DpsOldDiagState[] newFaults;
            DpsOldDiagState[] oldFaults;

            newFaults = Comparator(adkFaults, localFaults, 0);
            oldFaults = Comparator(localFaults, adkFaults, 1);

            // формируем структуру с результатами сравнения
            ResOfComparsion res = new ResOfComparsion();
            res.newFaults = newFaults;
            res.oldFaults = oldFaults;
            res.curFaults = GetCurFaults(adkFaults, localFaults);

            // возвращаем структуры с данными
            return res;
        }
        /// <summary>
        /// Список длящихся сбоев АДК
        /// </summary>
        /// <param name="adkFaults"></param>
        /// <param name="localFaults"></param>
        /// <returns></returns>
        protected DpsOldDiagState[] GetCurFaults(DpsOldDiagState[] adkFaults, DpsOldDiagState[] localFaults)
        {
            List<DpsOldDiagState> cur = new List<DpsOldDiagState>();
            foreach (DpsOldDiagState l in localFaults)
                foreach (DpsOldDiagState a in adkFaults)
                    if (l.ID == a.ID && l.State == a.State)
                    {
                        cur.Add(a);
                        break;
                    }
            return cur.ToArray();
        }
    }

	/// <summary>
	/// Класс вложенной подсистемы объектов при взаимодействии со старым Dps - по сути не подсистема
	/// а только обработка отказов АДК и передача старшей подсистеме
	/// </summary>
	public class DpsOldSubControlObjSystem : UniObject, IDpsAssociations
	{
		/// <summary>
		/// Переопределена загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			IOSysName = Loader.GetAttributeString("IOSys", string.Empty);
			string value = Loader.GetAttributeString("AdkUniDiagAssociations", "/App/Common/AdkUniDiagAssociations");

			_diagAssoc = Loader.GetObject(value, typeof(AdkUniDiagAssociations), null, false) as AdkUniDiagAssociations;
			// загружаем соответствие объектов
			_objsAdktoUniAssoc.SetObjsOwner(this);
			_objsAdktoUniAssoc.Load(Loader);
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void PreCreate()
		{
            _objsAdktoUniAssoc.PreCreate();

			base.PreCreate();
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
            base.Create();

            _objsAdktoUniAssoc.Create();

			if (IOSysName.Length > 0)
			{
				BaseIOSystem iosys = FindObject(IOSysName) as BaseIOSystem;
				if (iosys != null && _objSystem.Concentrator != null)
					_objSystem.Concentrator.AddUpdateDependence(iosys, _objSystem);
			}
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		/*public override void NotifyDestroy()
		{
            _objsAdktoUniAssoc.NotifyDestroy();
            base.NotifyDestroy();
		}
		/// <summary>
		/// После разрушения
		/// </summary>
		public override void NotifyPostDestroy()
		{
            _objsAdktoUniAssoc.NotifyPostDestroy();
            base.NotifyPostDestroy();
		}*/

		#region Обработка данных
		/// <summary>
		/// Переопределен разбор данных (базовый вызов не делается)
		/// </summary>
		/// <param name="reader">поток данных</param>
		public override void ProcessData(BinaryReader reader)
		{
			// пропускаем 14 байт
			reader.ReadBytes(14);
			// разбор данных от ДПС-сервера
			DpsOldDiagState[] Faults = DpsOldControlObjSystem.GetObjectsSCB(reader, ObjSys.DataTime);

			// сравниваем данные ДПС-сервера с данными из БД
			ResOfComparsion res = _comparer.CompareData(Faults, LocalDiagCopy);

			// проверяем наличие изменений, вносим их в базу данных
			if ((res.newFaults.GetLength(0) > 0) || (res.oldFaults.GetLength(0) > 0))
			{
				SaveChangesToLocalCopy(res);
			}
			_localDiagCopy = Faults;
			//_bValidData = false;
		}
		/// <summary>
		/// Сохранение изменений в локальную копию
		/// </summary>
		/// <param name="res"></param>
		public void SaveChangesToLocalCopy(ResOfComparsion res)
		{
			int len = res.oldFaults.GetLength(0);
			for (int i = 0; i < len; i++)
				ResetObjectDiagState(res.oldFaults[i].ID, res.oldFaults[i].State);

			// добавляем строки с новыми отказами
			len = res.newFaults.GetLength(0);
			for (int i = 0; i < len; i++)
				SetObjectDiagState(res.newFaults[i].ID, res.newFaults[i].State);
		}

		/// <summary>
		/// Выполнить дополнительные действия для состояния объекта
		/// </summary>
		/// <param name="objId">идентификатор объекта</param>
		/// <param name="diagState">диагностическое состояние</param>
		public virtual void SetObjectDiagState(UInt16 objId, UInt32 diagState)
		{
			if (_diagAssoc == null || !_objsAdktoUniAssoc.HaveAssociations)
			{
				DpsOldControlObjSystem.TraceDiagToLog(ObjSys.DataTime, objId, diagState, Name);
				return;
			}

			try
			{
				// получаем словарь диагностики АДК по типу АДК
				uint AdkType = _objsAdktoUniAssoc.GetAdkType(objId);
				// получаем описание конвертации по типу диагностики АДК
				UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, diagState);
				// получаем унифицированные объекты по объекту АДК и диагностике АДК
				List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(objId, diagState, AdkUniObjsAssociations.FaultActionType.Set);
				// для каждого объекта устанавливаем диагностическое состояние
				foreach (UniObject obj in objs)
				{
					DpsOldControlObjSystem.SetUniObjDiag(obj, di);
				}

				if (objs.Count == 0)
					DpsOldControlObjSystem.TraceDiagToLog(ObjSys.DataTime, objId, diagState, Name);
			}
			catch (KeyNotFoundException)
			{
				DpsOldControlObjSystem.TraceDiagToLog(ObjSys.DataTime, objId, diagState, Name);
			}

		}

		/// <summary>
		/// Выполнить дополнительные дейсвия для состояния объекта
		/// </summary>
		/// <param name="objId">идентификатор объекта</param>
		/// <param name="diagState">диагностическое сосотяние</param>
		public virtual void ResetObjectDiagState(UInt16 objId, UInt32 diagState)
		{
			if (_diagAssoc == null || !_objsAdktoUniAssoc.HaveAssociations)
				return;

			try
			{
				// получаем словарь диагностики АДК по типу АДК
				uint AdkType = _objsAdktoUniAssoc.GetAdkType(objId);
				// получаем описание конвертации по типу диагностики АДК
				UniDiagInfo di = _diagAssoc.GetDiagAssociation(AdkType, diagState);
				// получаем унифицированные объекты по объекту АДК и диагностике АДК
				List<UniObject> objs = _objsAdktoUniAssoc.GetUniObjects(objId, diagState, AdkUniObjsAssociations.FaultActionType.Reset);
				// для каждого объекта устанавливаем диагностическое состояние
				foreach (UniObject obj in objs)
				{
					DpsOldControlObjSystem.RemoveUniObjDiag(obj, di);
				}
			}
			catch (KeyNotFoundException)
			{
			}
		}

		#endregion

		#region Данные

		protected string IOSysName;
		/// <summary>
		/// локальная копия списка отказов
		/// </summary>
		protected DpsOldDiagState[] _localDiagCopy = new DpsOldDiagState[0];
		/// <summary>
		/// Локальная копия данных об отказах
		/// </summary>
		public DpsOldDiagState[] LocalDiagCopy
		{
			get { return _localDiagCopy; }
		}

		/// <summary>
		/// Компаратор
		/// </summary>
		protected ComparerDPS _comparer = new ComparerDPS();

		/// <summary>
		/// Словарь соответсвия объектов
		/// </summary>
		public AdkUniObjsAssociations _objsAdktoUniAssoc = new AdkUniObjsAssociations();

		/// <summary>
		/// Словарь соответсвия диагностики
		/// </summary>
		public AdkUniDiagAssociations _diagAssoc = null;
		public AdkUniObjsAssociations ObjsAdktoUniAssoc
		{
			get { return _objsAdktoUniAssoc; }
		}
		#endregion
	}

    /// <summary>
    /// Словарь типов диагнозов
    /// Переопределены некоторые фйункции для отвязки от AppEx
    /// </summary>
    public class TypeInfo : Tdm.Arm.TypeInfo
    {
        /// <summary>
        /// Получение тревожности диагностического состояния объекта контроля
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <param name="diagState">Идентификатор диагностического состояния</param>
        /// <returns>Тревожность диагностического состояния</returns>
        override public byte DiagStateSign(BaseControlObj obj, Int32 diagState)
        {
            Debug.Assert(obj != null);
            // проверяем - есть ли тревожность в таблице
            if (DiagStates.Columns.Contains(@"DiagSign"))
            {
                string selectString = string.Format(@"GroupID = {0} AND SubGroupID = {1} AND DiagState = {2}", obj.Type.GroupID, obj.Type.SubGroupID, diagState);
                DataRow[] foundRows = DiagStates.Select(selectString);
                if (foundRows.Length == 0)
                {
                    Console.WriteLine(string.Format(Name + ": В словаре не найден элемент с ключом dstate={0} (для объекта {1})", diagState, obj.Name), diagState);
                    return 5;
                }
                return Convert.ToByte(foundRows[0][@"DiagSign"]);
            }
            Console.WriteLine(string.Format(Name + ": В словаре не найден элемент с ключом dstate={0} (для объекта {1})", diagState, obj.Name), diagState);
            return 5;
        }
    }

    /// <summary>
    /// Класс-контейнер типов объектов ДДЦ.v2
    /// </summary>
    public class TypeInfoDDC1000 : Tdm.Unification.TypeInfo
	{
		protected DBConnector _connector = new DBConnector();
        protected string _sqlQuery = "select 1000 as GroupID, Uni_Type_ID as SubGroupID, Uni_DiagState_ID as DiagState, Name_UniDiagState as Name, DiagSignMeasure as DiagSign from TPD_DiagStatesUni;" +
                                    "select 1000 as GroupID, Uni_Type_ID as SubGroupID, Uni_State_ID as State, Name_UniState as Name from TPD_StatesUni;" +
                                    "select 1000 as GroupID, Uni_Type_ID as SubGroupID, Name_UniType as Name from TPD_ObjtypesUni;" +
                                    "select DiagSignMeasure as Code, DiagSign as Description from TPD_DiagSigns;" +
                                    "select 1000 as GroupID, 'ДДЦ' as Name;";
        protected string _softType = "ДДЦ";
        protected int _softID = 1000;

        public override void Load(DlxLoader loader)
		{
			try
			{
				base.Load(loader);
			}
			catch (Exception ex)
            {   // стандартная загрузка типов не прошла
				_connector.Load(loader);
				LoadFromDB();
			}
		}

        private void LoadFromDB()
        {
            if (_connector.Connect())
            {
                SqlDataAdapter sda = new SqlDataAdapter(_sqlQuery, _connector.SqlConn);
                DataSet ds = new DataSet();
                sda.Fill(ds);

                ds.Tables[0].TableName = "DiagStates";
                ds.Tables[1].TableName = "States";
                ds.Tables[2].TableName = "Types";
                ds.Tables[3].TableName = "DiagStatesCode";
                ds.Tables[4].TableName = "Groups";

                _typeInfo = ds;

                AddGroups();
                AddTypes();
            }
            //else
            //    throw new ApplicationException("Нет доступа к базе данных ЦДМ");
        }

		protected void AddTypes()
		{
            DataRow row49 = _typeInfo.Tables[2].NewRow();
            row49.ItemArray = new object[] { 1000, 49, "Станция" };
            _typeInfo.Tables[2].Rows.Add(row49);

            DataRow row149 = _typeInfo.Tables[2].NewRow();
            row149.ItemArray = new object[] { 1000, 149, "ШЧ" };
            _typeInfo.Tables[2].Rows.Add(row149);

            DataRow row150 = _typeInfo.Tables[2].NewRow();
            row150.ItemArray = new object[] { 1000, 150, "ДОРОГА" };
            _typeInfo.Tables[2].Rows.Add(row150);
        }

        protected void AddGroups()
		{
			for(int i = 0; i < 150; i++)
			{
				DataRow row = _typeInfo.Tables[4].NewRow();
                row.ItemArray = new object[] { i, _softType};
				_typeInfo.Tables[4].Rows.Add(row);
			}
		}
	}
    /// <summary>
    /// Класс-контейнер типов объектов ДДЦ.v1
    /// </summary>
	public class TypeInfoDDCold : TypeInfoDDC1000
	{
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);

			ReplaceGroups();
		}

		private void ReplaceGroups()
		{
            if (_typeInfo == null)
            {
                MessageBox.Show("Отсутствует подключение к базе данных.", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                //throw new Exception("Нет доступа к базе данных ЦДМ");
            }
            else
            {
                ReplaceGroups(_typeInfo.Tables[0]);
                ReplaceGroups(_typeInfo.Tables[1]);
                ReplaceGroups(_typeInfo.Tables[2]);
            }
		}

		private void ReplaceGroups(DataTable dt)
		{
			foreach (DataRow row in dt.Rows)
			{
				switch ((int)row["SubGroupID"])
				{
					case 1: row["GroupID"] = 33; break;
					case 2: row["GroupID"] = 2; break;
					case 3: row["GroupID"] = 39; break;
					case 4: row["GroupID"] = 39; break;
					case 5: row["GroupID"] = 39; break;
					case 6: row["GroupID"] = 39; break;
					case 7: row["GroupID"] = 12; break;
					case 8: row["GroupID"] = 32; break;
					case 9: row["GroupID"] = 52; break;
					case 10: row["GroupID"] = 7; break;
					case 11: row["GroupID"] = 2; break;
					
					case 101: row["GroupID"] = 214; break;
					case 102: row["GroupID"] = 214; break;
					case 103: row["GroupID"] = 214; break;
					case 104: row["GroupID"] = 214; break;
					case 105: row["GroupID"] = 214; break;

					case 49: row["GroupID"] = 49; break;
					case 149: row["GroupID"] = 149; break;
					case 150: row["GroupID"] = 150; break;
				}
			}
		}
	}

    /// <summary>
    /// Класс-контейнер типов объектов ПО КДК
    /// </summary>
    public class TypeInfoKDK : TypeInfoDDC1000
    {
        public override void Load(DlxLoader loader)
        {
            _sqlQuery = loader.GetAttributeString("QueryTypes", string.Empty);
            
            base.Load(loader);
        }
    }






}

