﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using AfxEx;
using Ivk.IOSys;
using Tdm.Unification.v2;

namespace Tdm.Unification
{
	#region Диагностика по данным от ДЦ

    /// <summary>
    /// Диагностика состояния сигнала. Диагностическое состояние взводится, 
    /// когда наблюдаемый сигнал переходти в импульс
    /// </summary>
    public class OneSigDiag : UniObjectUpDiag.ObjectTask
    {
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            if (Loader.GetAttribute("Param", out _SigName))
            {
                _DiagState = Loader.GetAttributeInt("ParamExt", int.MaxValue);
            }
        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            if (_SigName.Length == 0 || _DiagState == int.MaxValue)
            {
                throw new ArgumentException(_controlObj.Name + "/OneSigDiag: Недостаточно параметров для создания.");
            }

            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // получаем наблюдаемый сигнал по имени, полученному из Param
            ObjData sigObjData = _controlObj.GetObject(_SigName) as ObjData;
            if (sigObjData != null && sigObjData.IsSigDiscretLogic)
            {
                _Signal = sigObjData.SigDiscreteLogic;
                foreach (IOSignal sig in _Signal.IOSignals)
                {
                    AddWatchToSignal(sig);
                }
            }
            else
            {
                throw new ArgumentException(_controlObj.Name + "/OneSigDiag: Не удаётся получить сигнал " + _SigName);
            }
        }

        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();
            try
            {
                if (_Signal.On && !_bWasSignalImpulse)
                {
                    // На сигнале установился импульс
                    ActivateObjDiagState(_DiagState);
                    _bWasSignalImpulse = true;
                }
                else if (!_Signal.On && _bWasSignalImpulse)
                {
                    // На сигнале пропал импульс
                    DeactivateObjDiagState(_DiagState);
                    _bWasSignalImpulse = false;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                _bWasSignalImpulse = false;
            }
        }

        #region Data
        /// <summary>
        /// Имя сигнала, по которому производится диагностика
        /// </summary>
        private string _SigName;
        /// <summary>
        /// Наблюдаемый сигнал
        /// </summary>
        protected ISigDiscreteLogic _Signal;
        /// <summary>
        /// Признак того, что на наблюдаемом сигнале была единица
        /// </summary>
        private bool _bWasSignalImpulse = false;

        /// <summary>
        /// Диагностическое состояние, взводимое при переходе наблюдаемого сигнала в единицу
        /// </summary>
        protected int _DiagState;
        #endregion
    }

    /// <summary>
    /// Диагностика состояния сигнала. Диагностическое состояние взводится, 
    /// когда наблюдаемый сигнал переходти в паузу
    /// </summary>
    public class OneSig0Diag : UniObjectUpDiag.ObjectTask
    {
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            if (Loader.GetAttribute("Param", out _SigName))
            {
                _DiagState = Loader.GetAttributeInt("ParamExt", int.MaxValue);
            }
        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            if (_SigName.Length == 0 || _DiagState == int.MaxValue)
            {
                throw new ArgumentException(_controlObj.Name + "/OneSigDiag: Недостаточно параметров для создания.");
            }

            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // получаем наблюдаемый сигнал по имени, полученному из Param
            ObjData sigObjData = _controlObj.GetObject(_SigName) as ObjData;
            if (sigObjData != null && sigObjData.IsSigDiscretLogic)
            {
                _Signal = sigObjData.SigDiscreteLogic;
                foreach (IOSignal sig in _Signal.IOSignals)
                {
                    AddWatchToSignal(sig);
                }
            }
            else
            {
                throw new ArgumentException(_controlObj.Name + "/OneSigDiag: Не удаётся получить сигнал " + _SigName);
            }
        }

        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();
            try
            {
                if (_Signal.Off && !_bWasSignalPause)
                {
                    // На сигнале пауза
                    ActivateObjDiagState(_DiagState);
                    _bWasSignalPause = true;
                }
                else if (_Signal.On && _bWasSignalPause)
                {
                    // На сигнале появился импульс
                    DeactivateObjDiagState(_DiagState);
                    _bWasSignalPause = false;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                _bWasSignalPause = false;
            }
        }

        #region Data
        /// <summary>
        /// Имя сигнала, по которому производится диагностика
        /// </summary>
        private string _SigName;
        /// <summary>
        /// Наблюдаемый сигнал
        /// </summary>
        protected ISigDiscreteLogic _Signal;
        /// <summary>
        /// Признак того, что на наблюдаемом сигнале была пауза
        /// </summary>
        private bool _bWasSignalPause = false;

        /// <summary>
        /// Диагностическое состояние, взводимое при переходе наблюдаемого сигнала в единицу
        /// </summary>
        protected int _DiagState;
        #endregion
    }

    /// <summary>
    /// Диагностика отказа светофора(Перегорание нити красного огня). Диагностическое состояние взводится, 
    /// когда наблюдаемый светофор переходит в состояние "Отказ на светофоре"
    /// </summary>
    public class FailSignalDiag : UniObjectUpDiag.ObjectTask
    {
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            if (Loader.GetAttribute("Param", out _SigName))
            {
                _DiagState = Loader.GetAttributeInt("ParamExt", int.MaxValue);
            }
        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            if (_SigName.Length == 0 || _DiagState == int.MaxValue)
            {
                throw new ArgumentException(_controlObj.Name + "/FailSignalOSigDiag: Недостаточно параметров для создания.");
            }

            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // получаем наблюдаемый сигнал по имени, полученному из Param
            ObjData sigObjData = _controlObj.GetObject(_SigName) as ObjData;
            if (sigObjData == null || !sigObjData.IsSigDiscretLogic)
            {
                throw new ArgumentException(_controlObj.Name + "/FailSignalOSigDiag: Не удаётся получить сигнал " + _SigName);
            }
        }

        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();
            try
            {
                if (_bWasFail == false && _controlObj.ObjState.Current == (int)Uni2States.StateSignalStates.Fail)
                {
                    // Появился отказ на светофоре
                    ActivateObjDiagState(_DiagState);
                    _bWasFail = true;
                }
                else if (_bWasFail == true && _controlObj.ObjState.Current != (int)Uni2States.StateSignalStates.Fail)
                {
                    // Отказ пропал
                    DeactivateObjDiagState(_DiagState);
                    _bWasFail = false;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                _bWasFail = false;
            }
        }

        #region Data
        /// <summary>
        /// Имя сигнала, по которому производится диагностика
        /// </summary>
        private string _SigName;
        /// <summary>
        /// Признак того, что на светофоре был отказ
        /// </summary>
        private bool _bWasFail = false;

        /// <summary>
        /// Диагностическое состояние, взводимое при переходе наблюдаемого сигнала в единицу
        /// </summary>
        protected int _DiagState;
        #endregion
    }

    /// <summary>
    /// Диагностика состояния устройства электропитания
    /// </summary>
    public class PowerControlObjectDiag : UniObjectUpDiag.ObjectTask
    {
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            if (Loader.GetAttribute("Param", out _SigName))
            {
                _DiagState = Loader.GetAttributeInt("ParamExt", int.MaxValue);
            }
        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            if (_SigName.Length == 0 || _DiagState == int.MaxValue)
            {
                throw new ArgumentException(_controlObj.Name + "/PowerControlObjectDiag: Недостаточно параметров для создания.");
            }

            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // получаем наблюдаемый сигнал по имени, полученному из Param
            ObjData sigObjData = _controlObj.GetObject(_SigName) as ObjData;
            if (sigObjData != null && sigObjData.IsSigDiscretLogic)
            {
                ISigDiscreteLogic Signal = sigObjData.SigDiscreteLogic;
                foreach (IOSignal sig in Signal.IOSignals)
                {
                    AddWatchToSignal(sig);
                }
            }
            else
            {
                throw new ArgumentException(_controlObj.Name + "/OneSigDiag: Не удаётся получить сигнал " + _SigName);
            }
        }

        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();

            if (_controlObj.ObjState.Current == (int)Uni2States.PowerSupplyStates.NoPower && !_bDiagStateActivated)
            {
                // Нет питания - установить диагностическое состояние
                ActivateObjDiagState(_DiagState);
                _bDiagStateActivated = true;
            }
            else if (_controlObj.ObjState.Current != (int)Uni2States.PowerSupplyStates.NoPower && _bDiagStateActivated)
            {
                // снять диагностическое состояние
                DeactivateObjDiagState(_DiagState);
                _bDiagStateActivated = false;
            }
        }

        #region Data
        /// <summary>
        /// Имя сигнала, по которому производится диагностика
        /// </summary>
        private string _SigName;
        /// <summary>
        /// Признак того, что диагностическое состояние выставлено
        /// </summary>
        private bool _bDiagStateActivated = false;

        /// <summary>
        /// Диагностическое состояние, взводимое при переходе объекта контроля в наблюдаемое состояние
        /// </summary>
        protected int _DiagState;
        #endregion
    }

    public class LogicClose : UniObjectUpDiag.ObjectTask
    {
        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);
        }

        /*#region Data
        /// <summary>
        /// Секция перед светофором
        /// </summary>
        private UniObject _PrevSection;
        /// <summary>
        /// Секция после светофора
        /// </summary>
        private UniObject _PostSection;
        /// <summary>
        /// секция через одну после светофора
        /// </summary>
        private UniObject _PostPostSection;
        #endregion*/
    }

	/// <summary>
	/// Конечный автомат на делегатах
	/// </summary>
	public class FiniteStateMachine
	{
		/// <summary>
		/// Конструктор с количнством состояний (начальное состояние нулевое)
		/// </summary>
		/// <param name="amount">количество состояний</param>
		public FiniteStateMachine(int amount)
		{ 
			Init(amount);
			_curState = 0;
		}
		/// <summary>
		/// Конструктор с количеством состояний и начальным состоянием
		/// </summary>
		/// <param name="amount">количество состояний</param>
		/// <param name="initialstate">начальное состояние</param>
		public FiniteStateMachine(int amount, int initialstate)
		{
			Init(amount);
			_curState = initialstate;
		}

		/// <summary>
		/// делегат проверки состояния
		/// </summary>
		/// <returns>да, если это состояние активно</returns>
		public delegate bool State();
		/// <summary>
		/// Делегат перехода между состояниями
		/// </summary>
		public delegate void Pass();

		/// <summary>
		/// массив состояний
		/// </summary>
		private State[] _states;
		/// <summary>
		/// матрица переходов
		/// </summary>
		private Pass[,] _passages;
		/// <summary>
		/// текущее состояние
		/// </summary>
		private int _curState;
		/// <summary>
		/// инициализация
		/// </summary>
		/// <param name="amount"></param>
		private void Init(int amount)
		{			
			_states = new State[amount];
			_passages = new Pass[amount,amount];
		}
		/// <summary>
		/// установить делегат состояния
		/// </summary>
		/// <param name="index">индекс состояния</param>
		/// <param name="state">функция</param>
		public void setStateDelegate(int index, State state)
		{
			if (index >= _states.Length)
				throw new ArgumentOutOfRangeException("index");
			
			_states[index] = state;
		}
		/// <summary>
		/// установить делегат перехода между состояниями
		/// </summary>
		/// <param name="index1">предыдущее состояние</param>
		/// <param name="index2">новое состояние</param>
		/// <param name="pass">функция перехода</param>
		public void setPassageDelegate(int index1, int index2, Pass pass)
		{
			if (index1 >= _states.Length)
				throw new ArgumentOutOfRangeException("index1");

			if (index2 >= _states.Length)
				throw new ArgumentOutOfRangeException("index1");

			_passages[index1, index2] = pass;
		}
		/// <summary>
		/// Установить все состояния
		/// </summary>
		/// <param name="states">состояния</param>
		public void setStates(State[] states)
		{
			if (states.Length != _states.Length)
				throw new ArgumentOutOfRangeException("states.Length");
			for (int i = 0; i < _states.Length; i++)
				_states[i] = states[i];
		}
		/// <summary>
		/// Установить все переходы
		/// </summary>
		/// <param name="passages">переходы</param>
		public void setPassages(Pass[,] passages)
		{
			if (passages.GetLength(0) != _states.Length || passages.GetLength(1) != _states.Length)
				throw new ArgumentOutOfRangeException("states.Length");

			for (int i = 0; i < _states.Length; i++)
				for (int j = 0; j < _states.Length; j++)
					_passages[i, j] = passages[i, j];
		}

		/// <summary>
		/// метод проверки состояния и осуществление перехода
		/// </summary>
		/// <returns></returns>
		public bool TestState()
		{
			for (int i = 0; i < _states.Length; i++)
			{
				if (_states[i] != null && _states[i]())
				{
					if (_passages[_curState, i] != null)
						_passages[_curState, i]();
					_curState = i;
					return true;
				}
			}
			return false;
		}
	}

	public class DiagTask_RC : UniObjectUpDiag.ObjectTask
	{
		#region FiniteStateMachine
		private bool IsState0(){ return IsAllFree; }
		private bool IsState1() { return (IsNearBusy && !IsThisBusy); }
		private bool IsState2() { return IsOnlyThisBusy; }
		private bool IsState3() { return (IsNearBusy && IsThisBusy); }
		private bool IsState4() { return IsOneUnknown; }

		private void Pass0to1() { _time1 = _controlObj.ObjSys.DataTime; }
		private void Pass0to2() { if (!IsLogBusy) LogBusy(true); }
		private void Pass0to3() { if (!IsLogFree) IsoFail(true); }

		private void Pass1to2()
		{
			if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds > _isofailperiodMS && IsIsoFail) IsoFail(false);
			else if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds < _isofailperiodMS && !IsIsoFail) IsoFail(true);
		}

		private void Pass1to3()
		{
			if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds > _isofailperiodMS && IsIsoFail) IsoFail(false);
			else if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds < _isofailperiodMS && !IsIsoFail) IsoFail(true);
			if (IsLogBusy) LogBusy(false);
		}

		private void Pass2to0()
		{
			if (!IsLogFree && !IsLogBusy) LogFree(true);
		}

		private void Pass2to1()
		{
			if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds > _logfreeperiodMS && IsLogFree) LogFree(false);
			else if (((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds < _logfreeperiodMS && !IsLogFree && !IsLogBusy) LogFree(true);
		}

		private void Pass3to0() { if (!IsLogFree && !IsLogBusy) LogFree(true); }
		private void Pass3to1() { if (IsLogFree) LogFree(false); }
		private void Pass3to2() { _time2 = _controlObj.ObjSys.DataTime; }

		private void ToFirst() { IsFirst = true; }
		private void NotToFirst() { IsFirst = false; }

		private void InitAutomat()
		{
			_automat = new FiniteStateMachine(5, 4);

			_automat.setStates(new FiniteStateMachine.State[5] {IsState0, IsState1, IsState2, IsState3, IsState4});
			_automat.setPassages(new FiniteStateMachine.Pass[5, 5]
					{	{null,			Pass0to1,	Pass0to2,	Pass0to3,	ToFirst},
						{null,			null,		Pass1to2,	Pass1to2,	ToFirst},
						{Pass2to0,		Pass2to1,	null,		null,		ToFirst},
						{Pass3to0,		Pass3to1,	Pass3to2,	null,		ToFirst},
						{NotToFirst,	NotToFirst,	NotToFirst,	NotToFirst,	null}});
		}
		#endregion

		/// <summary>
		/// Обновление состояния
		/// </summary>
		public override void UpdateObjectDiagState()
		{
			base.UpdateObjectDiagState();

			_automat.TestState();
		}

		
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_isofailperiodMS = Loader.GetAttributeInt("isofailperiod", _isofailperiodMS);
			_logfreeperiodMS = Loader.GetAttributeInt("logfreeperiod", _logfreeperiodMS);
		}

		/// <summary>
		/// Инициализация алгоритма
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			InitAutomat();

            base.Create();
			// подписываемся на изменение этого объекта
			AddWatchToObject(_controlObj);

			// вытаемся получить соседние РЦ
			BaseControlObj prevSec = AssignToRefObj("PrevSec");
			BaseControlObj nextSec = AssignToRefObj("NextSec");

			// если заданы соседние, то РЦ это путь
			if (prevSec != null && nextSec != null)
			{
				// проверяем тип
				if (prevSec.Type.GroupID != _controlObj.Type.GroupID || prevSec.Type.SubGroupID != _controlObj.Type.SubGroupID
					|| nextSec.Type.GroupID != _controlObj.Type.GroupID || nextSec.Type.SubGroupID != _controlObj.Type.SubGroupID
					|| _controlObj.Type.SubGroupID != 1)
				{
                    throw new ArgumentException("DiagTask_LogBusy: Типы объектов не являются рельсовыми цепями: " + _controlObj.Name);
				}
				// добавляем в массив соседних
				_nearSec.Add(prevSec);
				_nearSec.Add(nextSec);
			}
			// иначе - задан хотябы один указатель - диагностирование не возможно
			else if (prevSec != null || nextSec != null)
			{
                throw new ArgumentException("DiagTask_LogBusy: не задан полный набор отношений для: " + _controlObj.Name);
			}
			// иначе - возможно это стрелочная РЦ
			else
			{
				/*// находим все стрелки входящие в эту стрелочную РЦ
				List<BaseControlObj> thisSwitches = new List<BaseControlObj>();
				FindAllSwithInthisRC(_controlObj.ObjSys.SubElements, _controlObj, thisSwitches);
				if (thisSwitches.Count == 0)
				{
					Trace.WriteLine("DiagTask_LogBusy: не задан набор отношений и не найдена ни одна стрелка в этой РЦ " + _controlObj.Name);
					return false;
				}

				// находим по стрелкам все соседние РЦ
				if (!FindNearRCbySwitches(thisSwitches, _controlObj.Type))
				{
					Trace.WriteLine("DiagTask_LogBusy: найдена стрелка, которая не имеет всех отношений, в РЦ " + _controlObj.Name);
					return false;
				}*/

                // находим соседние РЦ
                if (!CollectAllNearRC())
                {
                    throw new ArgumentException("DiagTask_LogBusy: найдена РЦ(" + _controlObj.Name + "), которая имеет некорректные отношения NearSec!");
                }

				foreach (BaseControlObj sec in _nearSec)
					AddWatchToObject(sec);
			}
		}
		/// <summary>
		/// Посик всех стрелок, входящих в стрелочную РЦ
		/// </summary>
		/// <param name="coll">коллекция объектов</param>
		/// <param name="rc">РЦ</param>
		/// <param name="rcswitches">список стрелок</param>
		protected void FindAllSwithInthisRC(DlxCollection coll, BaseControlObj rc, List<BaseControlObj> rcswitches)
		{
			if (coll == null)
				return;
			foreach (DlxObject obj in coll.Objects)
			{
				BaseControlObj ctrlObj = obj as BaseControlObj;
				if (ctrlObj != null)
				{
					// пытаемся найти SwSection среди член данных
					ObjData swref = ctrlObj.GetObject("SwSection") as ObjData;
					// проверяем что он совпадает с нужной РЦ
					if (swref != null && swref.IsControlObj && swref.ControlObj == rc)
						rcswitches.Add(ctrlObj);
					// поиск в дочерних элементах
					FindAllSwithInthisRC(ctrlObj.SubElements, rc, rcswitches);
				}
			}
		}
		/// <summary>
		/// Поиск смежных РЦ по стрелкам
		/// </summary>
		/// <param name="rcswitches">список стрелок</param>
		/// <param name="rctype">тип РЦ</param>
		/// <returns>false - есть стрелки с неполным набором отношений</returns>
		protected bool FindNearRCbySwitches(List<BaseControlObj> rcswitches, ObjType rctype)
		{
			foreach (BaseControlObj sw in rcswitches)
			{
				ObjData pr = sw.GetObject("PrevSec") as ObjData;
				ObjData np = sw.GetObject("NextPl") as ObjData;
				ObjData nm = sw.GetObject("NextMi") as ObjData;

				// диагностирование не возможно
				if (pr == null || np == null || nm == null)
					return false;

				if (!pr.IsControlObj || !np.IsControlObj || !nm.IsControlObj)
					return false;

				if (pr.ControlObj == null || np.ControlObj == null || nm.ControlObj == null)
					return false;

				/////////////////////////////////
				// если это РЦ или стрелки
				if (pr.ControlObj.Type.GroupID == rctype.GroupID && pr.ControlObj.Type.SubGroupID == rctype.SubGroupID)
					_nearSec.Add(pr.ControlObj);
				else if (pr.ControlObj.Type.GroupID == sw.Type.GroupID && pr.ControlObj.Type.SubGroupID == sw.Type.SubGroupID)
				{
					ObjData sec = pr.ControlObj.GetObject("SwSection") as ObjData;
					if (sec != null && sec.IsControlObj && sec.ControlObj.Type.GroupID == rctype.GroupID && sec.ControlObj.Type.SubGroupID == rctype.SubGroupID)
						_nearSec.Add(sec.ControlObj);
					else
						return false;
				}
				else// неизвестный тип
					return false;

				if (np.ControlObj.Type.GroupID == rctype.GroupID && np.ControlObj.Type.SubGroupID == rctype.SubGroupID)
					_nearSec.Add(np.ControlObj);
				else if (np.ControlObj.Type.GroupID == sw.Type.GroupID && np.ControlObj.Type.SubGroupID == sw.Type.SubGroupID)
				{
					ObjData sec = np.ControlObj.GetObject("SwSection") as ObjData;
					if (sec != null && sec.IsControlObj && sec.ControlObj.Type.GroupID == rctype.GroupID && sec.ControlObj.Type.SubGroupID == rctype.SubGroupID)
						_nearSec.Add(sec.ControlObj);
					else
						return false;
				}
				else// неизвестный тип
					return false;

				if (nm.ControlObj.Type.GroupID == rctype.GroupID && nm.ControlObj.Type.SubGroupID == rctype.SubGroupID)
					_nearSec.Add(nm.ControlObj);
				else if (nm.ControlObj.Type.GroupID == sw.Type.GroupID && nm.ControlObj.Type.SubGroupID == sw.Type.SubGroupID)
				{
					ObjData sec = nm.ControlObj.GetObject("SwSection") as ObjData;
					if (sec != null && sec.IsControlObj && sec.ControlObj.Type.GroupID == rctype.GroupID && sec.ControlObj.Type.SubGroupID == rctype.SubGroupID)
						_nearSec.Add(sec.ControlObj);
					else
						return false;
				}
				else// неизвестный тип
					return false;
			}

			return true;
		}

        /// <summary>
        /// Поиск смежных РЦ по ссылкам near и помещаем их в список _nearSec
        /// </summary>
        /// <param name="rcswitches">список стрелок</param>
        /// <param name="rctype">тип РЦ</param>
        /// <returns>false - есть </returns>
        protected bool CollectAllNearRC()
        {
            int counter = 1;
            bool bRes = true;
            ObjData nearsec = _controlObj.GetObject("NearSec") as ObjData;
            while (nearsec != null)
            {
                if (nearsec.IsControlObj)
                    _nearSec.Add(nearsec.ControlObj);
                else
                    bRes = false;

                string objdataname = "NearSec" + counter++.ToString();
                nearsec = _controlObj.GetObject(objdataname) as ObjData;
            }
            return bRes;
        }

		#region States
		/// <summary>
		/// Являются все РЦ свободными
		/// </summary>
		protected bool IsAllFree
		{
			get
			{
				if ((_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock_EmergencyRelease))
				{
					foreach (BaseControlObj near in _nearSec)
					{
						if ((near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free)
							&& (near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock)
							&& (near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock_EmergencyRelease))
							return false;
					}
					return true;
				}
				else
					return false;
			}
		}
		/// <summary>
		/// Есть ли среди этой РЦ и смежных хотябы одна с неизвестным состоянием
		/// </summary>
		protected bool IsOneUnknown
		{
			get
			{
				if ((_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.ObjectConserve))
					return true;

				foreach (BaseControlObj near in _nearSec)
				{
					if ((near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
						|| (near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
						|| (near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.GeneralStates.ObjectConserve))
						return true;
				}

				return false;
			}
		}
		/// <summary>
		/// Является ли только эта РЦ занятой
		/// </summary>
		protected bool IsOnlyThisBusy
		{
			get
			{
				// если эта РЦ занята
				if ((_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease))
				{
					// А соседние свободны
					foreach (BaseControlObj near in _nearSec)
					{
						if ((near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free)
							&& (near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock)
							&& (near.ObjState.Current != (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Free_Lock_EmergencyRelease))
							return false;
					}
					return true;
				}
				else
					return false;
			}
		}
		/// <summary>
		/// Является ли текущая РЦ занятой
		/// </summary>
		protected bool IsThisBusy
		{
			get
			{
				// если эта РЦ занята
				if ((_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock)
					|| (_controlObj.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease))
				{
					return true;
				}
				else
					return false;
			}
		}
		/// <summary>
		/// Являются ли хоть одна смежная РЦ занятой
		/// </summary>
		protected bool IsNearBusy
		{
			get
			{
				foreach (BaseControlObj near in _nearSec)
				{
					if ((near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy)
						|| (near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock)
						|| (near.ObjState.Current == (int)Tdm.Unification.v2.Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease))
						return true;
				}
				return false;
			}
		}
		#endregion

		#region DiagStates
		protected void LogBusy(bool active)
		{
			if (active)
			{
				ActivateObjDiagState(DiagState_LogBusy);
				_state |= TaskStates.logbusy;
			}
			else
			{
				DeactivateObjDiagState(DiagState_LogBusy);
				_state &= ~TaskStates.logbusy;
			}
		}

		protected void LogFree(bool active)
		{
			if (active)
			{
				ActivateObjDiagState(DiagState_LogFree);
				_state |= TaskStates.logfree;
			}
			else
			{
				DeactivateObjDiagState(DiagState_LogFree);
				_state &= ~TaskStates.logfree;
			}
		}

		protected void IsoFail(bool active)
		{
			if (active)
			{
				ActivateObjDiagState(DiagState_IsoFail);
				_state |= TaskStates.isofail;
			}
			else
			{
				DeactivateObjDiagState(DiagState_IsoFail);
				_state &= ~TaskStates.isofail;
			}
		}

		protected bool IsLogBusy
		{
			get { return (_state & TaskStates.logbusy) != 0; }
		}

		protected bool IsLogFree
		{
			get { return (_state & TaskStates.logfree) != 0; }
		}

		protected bool IsIsoFail
		{
			get { return (_state & TaskStates.isofail) != 0; }
		}

		protected bool IsFirst
		{
			get { return (_state & TaskStates.first) != 0; }
			set { if (value) _state |= TaskStates.first; else _state &= ~TaskStates.first; }
		}
		
		/// <summary>
		/// Состояния алгоритма
		/// </summary>
		[Flags]
		public enum TaskStates : uint
		{
			first = 1,
			logbusy = 2,
			logfree = 4,
			isofail = 8
		}
		/// <summary>
		/// Текущее состояние алгоритма
		/// </summary>
		protected TaskStates _state = TaskStates.first;
		/// <summary>
        /// Контролируемое диагностическое состояние - Кратковременная логическая занятость
		/// </summary>
		public virtual int DiagState_LogBusy
		{
			get { return 1; }
		}
		/// <summary>
        /// Контролируемое диагностическое состояние - Логическая свободность
		/// </summary>
		public virtual int DiagState_LogFree
		{
			get { return 2; }
		}
		/// <summary>
        /// Контролируемое диагностическое состояние - Пробой изолирующих стыков
		/// </summary>
		public virtual int DiagState_IsoFail
		{
			get { return 3; }
		}
		#endregion

		#region Data
		/// <summary>
		/// Список смежных РЦ
		/// </summary>
		protected List<BaseControlObj> _nearSec = new List<BaseControlObj>();
	
		protected FiniteStateMachine _automat = null;
		protected DateTime _time1 = DateTime.MinValue;
		protected DateTime _time2 = DateTime.MinValue;
		protected int _isofailperiodMS = 1000;
		protected int _logfreeperiodMS = 1000;

		#endregion
	}

	public class DiagTask_Switch : UniObjectUpDiag.ObjectTask
	{
		#region FiniteStateMachine

		private bool IsState0() { return IsPK && !IsLockOrBusy; }
		private bool IsState1() { return IsMK && !IsLockOrBusy; }
		private bool IsState2() { return IsLC && !IsLockOrBusy; }
		private bool IsState3() { return IsPK && IsLockOrBusy; }
		private bool IsState4() { return IsMK && IsLockOrBusy; }
		private bool IsState5() { return IsLC && IsLockOrBusy; }
		private bool IsState6() { return IsUnknown; }

		private void Pass03to2()
		{
			// из ПК в нетК: запоминаем время _time1 и просим оповестить через _periodLossControl
			_time1 = _controlObj.ObjSys.DataTime; AddNotificationEvent(_periodLossControl);
			if (LossBusyOrLock) LossBusyOrLock = false;
            
            // если был "перевод при занятой РЦ" и РЦ не занята, то снимаем "перевод при занятой РЦ";
            if (BusyRCSwitch && !IsSwitchBusy)
                BusyRCSwitch = false;
		}
		private void Pass03to5()
		{
			// из ПК в нетКиЗам: потеря конроля при замкн. или занятой РЦ, запоминаем время _time1
			if (!LossBusyOrLock) LossBusyOrLock = true; _time1 = _controlObj.ObjSys.DataTime; AddNotificationEvent(_periodLossControl);

            // если трекла заянта, то запоминаем что пришли сюда из плюсового контроля (для диагностики перевода при занятой РЦ)
            if (IsSwitchBusy)
                IsLastPK = true;
		}

		private void Pass14to2()
		{
			// из МК в нетК: запоминаем время _time2 и просим оповестить через _periodLossControl
			_time2 = _controlObj.ObjSys.DataTime; AddNotificationEvent(_periodLossControl);
			if (LossBusyOrLock) LossBusyOrLock = false;

            // если был "перевод при занятой РЦ" и РЦ не занята, то снимаем "перевод при занятой РЦ"
            if (BusyRCSwitch && !IsSwitchBusy)
                BusyRCSwitch = false;
		}
		private void Pass14to5()
		{
			// из МК в нетКиЗам: потеря конроля при замкн. или занятой РЦ, запоминаем время _time2
			if (!LossBusyOrLock) LossBusyOrLock = true; _time2 = _controlObj.ObjSys.DataTime; AddNotificationEvent(_periodLossControl);

            // если трекла заянта, то запоминаем что пришли сюда из минусовго контроля (для диагностики перевода при занятой РЦ)
            if (IsSwitchBusy)
                IsLastMK = true;
		}

		private void Pass25to03()
		{
			// из нетК в ПК
			LossCtrl = false;
			// если получили противоположный контроль, то снимаем кратковр потерю и отсутствие перевода
            if (_time2 != DateTime.MinValue)
            {
                MinLossCtrl = false;
                AbsenceSwitch = false;
            }
			// если был МК и время перевода более нормального
			if (_time2 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds > _periodNormOperation && !LongPeriod)
				LongPeriod = true;
			// если был МК и время перевода нормальное
			else if (_time2 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds <= _periodNormOperation && LongPeriod)
				LongPeriod = false;
			_time2 = DateTime.MinValue;
			// если был ПК и время потери контроля малое
			if (_time1 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds < _periodMinLossCtrl && !MinLossCtrl)
				MinLossCtrl = true;
			// если был ПК и время потери контроля большое
            else if (_time1 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds >= _periodMinLossCtrl)
            {
                MinLossCtrl = false;
                AbsenceSwitch = true;
            }

            // если был МК и РЦ была занята нри потере контроля
            // и сейчас РЦ занята, то перевод при занятой РЦ = true;
            if (IsSwitchBusy && IsLastBusy && IsLastMK)
                BusyRCSwitch = true;

			_time1 = DateTime.MinValue;
		}
		private void Pass25to14()
		{
			// из нетК в МК
			LossCtrl = false;
            // если получили противоположный контроль, то снимаем кратковр потерю и отсутствие перевода
            if (_time1 != DateTime.MinValue)
            {
                MinLossCtrl = false;
                AbsenceSwitch = false;
            }
			// если был ПК и время перевода более нормального
			if (_time1 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds > _periodNormOperation && !LongPeriod)
				LongPeriod = true;
			// если был ПК и время перевода нормальное
			else if (_time1 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds <= _periodNormOperation && LongPeriod)
				LongPeriod = false;
			_time1 = DateTime.MinValue;
            // если был МК и время потери контроля мало
			if (_time2 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds < _periodMinLossCtrl && !MinLossCtrl)
				MinLossCtrl = true;
            // если был МК и время потери контроля не мало
            else if (_time2 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds >= _periodMinLossCtrl )
            {
                MinLossCtrl = false;
                AbsenceSwitch = true;
            }

            // если был ПК и РЦ была занята нри потере контроля
            // и сейчас РЦ занята, то перевод при занятой РЦ = true;
            if (IsLastPK && IsLastBusy && IsSwitchBusy)
                BusyRCSwitch = true;

			_time2 = DateTime.MinValue;
		}
		private void Pass25to25()
		{
			// из нетК в нетПК
			// если был ПК и время более времени потери контроля
			if (_time1 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time1)).TotalMilliseconds > _periodLossControl && !LossCtrl)
				LossCtrl = true;
			// если был МК и время более времени потери контроля
			if (_time2 != DateTime.MinValue && ((TimeSpan)(_controlObj.ObjSys.DataTime - _time2)).TotalMilliseconds > _periodLossControl && !LossCtrl)
				LossCtrl = true;
			//_time1 = DateTime.MinValue;
			//_time2 = DateTime.MinValue;
		}

        private void Pass4to3_3to4()
        {
            // Из З+МК в З+ПК и наоборот
            if (!LossBusyOrLock) LossBusyOrLock = true;

            //если РЦ была занята и сейчас РЦ занята, то перевод при занятой РЦ = true;
            if (IsLastBusy && IsSwitchBusy)
                BusyRCSwitch = true;
        }

        private void Pass0to1_1to0()
        {
            // Из ПК в МК и наоборот
            if (LossBusyOrLock) LossBusyOrLock = false;
            
            // снмаем "перевод при занятой РЦ" 
            BusyRCSwitch = false;

            // снимаем "Кратковременная потеря контроля"
            MinLossCtrl = false;

            // снимаем "Отсутствие перевода"
            AbsenceSwitch = false;
        }

        private void Pass3to1_4to0()
        {
            if (LossBusyOrLock) LossBusyOrLock = false;
            
            // перевод при занятой РЦ = false; ?????
            if (BusyRCSwitch)
                BusyRCSwitch = false;
        }

		private void ToFirst() { IsFirst = true; }
		private void NotToFirst() { IsFirst = false; }

		private void InitAutomat()
		{
			_automat = new FiniteStateMachine(7, 6);

			_automat.setStates(new FiniteStateMachine.State[7] { IsState0, IsState1, IsState2, IsState3, IsState4, IsState5, IsState6 });
			_automat.setPassages(new FiniteStateMachine.Pass[7, 7]
					{	{null,			Pass0to1_1to0,	Pass03to2,	null,		    null,		    Pass03to5,	ToFirst},
						{Pass0to1_1to0,	null,		    Pass14to2,	null,		    null,		    Pass14to5,	ToFirst},
						{Pass25to03,	Pass25to14,	    Pass25to25,	Pass25to03,	    Pass25to14,	    Pass25to25,	ToFirst},
						{null,			Pass3to1_4to0,	Pass03to2,	null,		    Pass4to3_3to4,	Pass03to5,	ToFirst},
						{Pass3to1_4to0,	null,       	Pass14to2,	Pass4to3_3to4,  null,		    Pass14to5,	ToFirst},
						{Pass25to03,	Pass25to14,	    Pass25to25,	Pass25to03,	    Pass25to14,	    Pass25to25,	ToFirst},
						{NotToFirst,	NotToFirst,	    NotToFirst,	NotToFirst,	    NotToFirst,	    NotToFirst,	null}});
		}
		#endregion


		/// <summary>
		/// Загрузка алгоритма
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_periodNormOperation = Loader.GetAttributeInt("normalperiod", _periodNormOperation);
			_periodLossControl = Loader.GetAttributeInt("lossctrlperiod", _periodLossControl);
			_periodMinLossCtrl = Loader.GetAttributeInt("minlossctrlperiod", _periodMinLossCtrl);
		}
		/// <summary>
		/// Инициализация алгоритма
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			InitAutomat();

            base.Create();
			// подписываемся на изменение этого объекта
			AddWatchToObject(_controlObj);
		}

		/// <summary>
		/// Обновление состояния
		/// </summary>
		public override void UpdateObjectDiagState()
		{
			base.UpdateObjectDiagState();

			_automat.TestState();

            // Запомнинаем занята ли стрелка
            if (IsSwitchBusy)
                _bLastBusy = true;
            else
                _bLastBusy = false;

		}

		#region State
		protected bool IsPK
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Free)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Free_Lock_EmergencyRelease);
			}
		}

		protected bool IsMK
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Free)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Free_Lock_EmergencyRelease);
			}
		}

		protected bool IsLC
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Free)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Free_Lock_EmergencyRelease);
			}
		}

		protected bool IsLockOrBusy
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Busy_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.LC_Free_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.MI_Free_Lock_EmergencyRelease)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Free_Lock)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.SwitchStates.PL_Free_Lock_EmergencyRelease);
			}
		}

		protected bool IsUnknown
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
						|| (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve);
			}
		}
		#endregion

		#region DiagState

		/// <summary>
		/// Состояния алгоритма
		/// </summary>
		[Flags]
		public enum TaskStates : uint
		{
			first = 1,
			lossctrl = 2,
			lossbusyorlock = 4,
			longperiod = 8,
			minlossctrl = 16,
            busyRCswitch = 32,
            absenceSwitch = 64
		}
		/// <summary>
		/// Текущее состояние алгоритма
		/// </summary>
		protected TaskStates _state = TaskStates.first;
        /// <summary>
        /// Контролируемое диагностическое состояние - Отсутствие перевода
        /// </summary>
        public virtual int DiagState_AbsenceSwitch
        {
            get { return 300; }
        }
		/// <summary>
        /// Контролируемое диагностическое состояние - Потеря контроля (более 30 сек.)
		/// </summary>
		public virtual int DiagState_LossCtrl
		{
			get { return 301; }
		}
		/// <summary>
        /// Контролируемое диагностическое состояние - Потеря контроля при занятой или замкнутой РЦ
		/// </summary>
		public virtual int DiagState_LossBusyOrLock
		{
			get { return 302; }
		}
        /// <summary>
        /// Контролируемое диагностическое состояние - Перевод при занятой РЦ
        /// </summary>
        public virtual int DiagState_BusyRCSwitch
        {
            get { return 303; }
        }
        /// <summary>
        /// Контролируемое диагностическое состояние - Кратковременная потеря контроля
        /// </summary>
        public virtual int DiagState_MinLossCtrl
        {
            get { return 304; }
        }
		/// <summary>
        /// Контролируемое диагностическое состояние - Увеличенное время перевода
		/// </summary>
		public virtual int DiagState_LongPeriod
		{
			get { return 305; }
		}

        /// <summary>
        /// Установить/снять/проверить диагностическое состояние "Отсутствие перевода"
        /// </summary>
        protected bool AbsenceSwitch
        {
            get { return (_state & TaskStates.absenceSwitch) != 0; }
            set
            {
                if (value)
                {
                    if ((_state & TaskStates.absenceSwitch) == 0)
                    {
                        _state |= TaskStates.absenceSwitch;
                        ActivateObjDiagState(DiagState_AbsenceSwitch);
                    }
                }
                else
                {
                    if ((_state & TaskStates.absenceSwitch) != 0)
                    {
                        _state &= ~TaskStates.absenceSwitch;
                        DeactivateObjDiagState(DiagState_AbsenceSwitch);
                    }
                }
            }
        }

		protected bool LossCtrl
		{
			get { return (_state & TaskStates.lossctrl) != 0; }
			set
			{
				if (value)
				{
                    if ((_state & TaskStates.lossctrl) == 0)
                    {
                        _state |= TaskStates.lossctrl;
                        ActivateObjDiagState(DiagState_LossCtrl);
                    }
				}
				else
				{
                    if ((_state & TaskStates.lossctrl) != 0)
                    {
                        _state &= ~TaskStates.lossctrl;
                        DeactivateObjDiagState(DiagState_LossCtrl);
                    }
				}
			}
		}

		protected bool LossBusyOrLock
		{
			get { return (_state & TaskStates.lossbusyorlock) != 0; }
			set
			{
				if (value)
				{
                    if ((_state & TaskStates.lossbusyorlock) == 0)
                    {
                        _state |= TaskStates.lossbusyorlock;
                        ActivateObjDiagState(DiagState_LossBusyOrLock);
                    }
				}
				else
				{
                    if ((_state & TaskStates.lossbusyorlock) != 0)
                    {
                        _state &= ~TaskStates.lossbusyorlock;
                        DeactivateObjDiagState(DiagState_LossBusyOrLock);
                    }
				}
			}
		}

        /// <summary>
        /// Установить/снять/проверить диагностическое состояние "перевод при занятой РЦ"
        /// </summary>
        protected bool BusyRCSwitch
        {
            get { return (_state & TaskStates.busyRCswitch) != 0; }
            set
            {
                if (value)
                {
                    if ((_state & TaskStates.lossbusyorlock) == 0)
                    {
                        _state |= TaskStates.busyRCswitch;
                        ActivateObjDiagState(DiagState_BusyRCSwitch);
                    }
                }
                else
                {
                    if ((_state & TaskStates.lossbusyorlock) != 0)
                    {
                        _state &= ~TaskStates.busyRCswitch;
                        DeactivateObjDiagState(DiagState_BusyRCSwitch);
                    }
                }
            }
        }

		protected bool LongPeriod
		{
			get { return (_state & TaskStates.longperiod) != 0; }
			set
			{
				if (value)
				{
                    if ((_state & TaskStates.longperiod) == 0)
                    {
                        _state |= TaskStates.longperiod;
                        ActivateObjDiagState(DiagState_LongPeriod);
                    }
				}
				else
				{
                    if ((_state & TaskStates.longperiod) != 0)
                    {
                        _state &= ~TaskStates.longperiod;
                        DeactivateObjDiagState(DiagState_LongPeriod);
                    }
				}
			}
		}

		protected bool IsFirst
		{
			get { return (_state & TaskStates.first) != 0; }
			set
			{
				if (value) _state |= TaskStates.first; else _state &= ~TaskStates.first;
				_time1 = DateTime.MinValue;
				_time2 = DateTime.MinValue;
			}
		}

		protected bool MinLossCtrl
		{
			get { return (_state & TaskStates.minlossctrl) != 0; }
			set
			{
                if (value)
				{
                    if ((_state & TaskStates.minlossctrl) == 0)
                    {
                        _state |= TaskStates.minlossctrl;
                        ActivateObjDiagState(DiagState_MinLossCtrl);
                    }
				}
                else 
				{
                    if ((_state & TaskStates.minlossctrl) != 0)
                    {
                        _state &= ~TaskStates.minlossctrl;
                        DeactivateObjDiagState(DiagState_MinLossCtrl);
                    }
				}
			}
		}

		#endregion

		#region Data
		protected int _periodNormOperation = 9000;
		protected int _periodLossControl = 20000;
		protected int _periodMinLossCtrl = 1500;
		protected FiniteStateMachine _automat = null;
		protected DateTime _time1 = DateTime.MinValue;
		protected DateTime _time2 = DateTime.MinValue;

        /// <summary>
        /// Признак того, что при предыдущих данных стрелка была занята.
        /// </summary>
        private bool _bLastBusy = false;
        /// <summary>
        /// Проверка того, что при предыдущих данных стрелка была занята.
        /// </summary>
        protected bool IsLastBusy
        {
            get
            {
                return _bLastBusy;
            }
        }

        /// <summary>
        /// Признак того, что был плюсовой кеонтроль
        /// </summary>
        private bool _bLastPK = false;
        /// <summary>
        /// Проверка/установка того, что был плюсовой контроль
        /// </summary>
        protected bool IsLastPK
        {
            set
            {
                // устанавливаем признак плюсового контроля
                _bLastPK = true;
                // снимаем признак минусового контроля
                _bLastMK = false;
            }
            get
            {
                return _bLastPK;
            }
        }

        /// <summary>
        /// Признак того, что был минусовой контроль
        /// </summary>
        private bool _bLastMK = false;
        /// <summary>
        /// Проверка/установка того, что был минусовой контроль
        /// </summary>
        protected bool IsLastMK
        {
            set
            {
                // устанавливаем признак минусового контроля
                _bLastMK = true;
                // снимаем признак плюсового контроля
                _bLastPK = false;
            }
            get
            {
                return _bLastMK;
            }
        }

        /// <summary>
        /// Проверка занятости стрелки
        /// </summary>
        protected bool IsSwitchBusy
        {
            get
            {
                return _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.LC_Busy ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.LC_Busy_Lock ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.LC_Busy_Lock_EmergencyRelease ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.MI_Busy ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.MI_Busy_Lock ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.MI_Busy_Lock_EmergencyRelease ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.PL_Busy ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.PL_Busy_Lock ||
                   _controlObj.ObjState.Current == (int)Uni2States.SwitchStates.PL_Busy_Lock_EmergencyRelease;
            }
        }
		#endregion

	}

    /// <summary>
    /// Диагностирование устройств электропитания (ФИДЕР)
    /// Диагностические состояния:                       
    /// 01 - Отсутствие питания на основном фидере      (Контроль наличия фидера1)
    /// 03 - Переключение фидеров                       (Контроль активности ф1, контроль активности ф2)
    /// 04 - Отсутствие основного и резервного питания  (Контроь наличия ф1, контроль наличия ф2)
    /// </summary>
    public class MainFiderDiag : UniObjectUpDiag.ObjectTask
    {
        #region FiniteStateMachine

        #region States
        /// <summary>
        /// Отсутствие обоих фидеров
        /// </summary>
        /// <returns></returns>
        private bool IsState0() { return IsBothFidNotInPower; }
        /// <summary>
        /// Отсутствие фидера
        /// </summary>
        /// <returns></returns>
        private bool IsState1() { return IsFidNotInPower; }
        /// <summary>
        /// Основной фидер активен
        /// </summary>
        /// <returns></returns>
        private bool IsState2() { return IsFidInPowerAndActive; }
        /// <summary>
        /// Резервный фидер активен
        /// </summary>
        /// <returns></returns>
        private bool IsState3() { return IsResFidInPowerAndActive; }
        /// <summary>
        /// ДГА фидер активен
        /// </summary>
        /// <returns></returns>
        private bool IsState4() { return IsResDFidInPowerAndActive; }
        /// <summary>
        /// Состояние системы не влияющее на диагностику
        /// </summary>
        /// <returns></returns>
        private bool IsState5() { return IsUnknown; }
        #endregion

        #region Passages
        private void Pass0to1() { NoPower = false; FidNotInPower = true; }
        private void Pass0to2() { NoPower = false; }
        private void Pass0to3() { NoPower = false; }
        private void Pass0to4() { NoPower = false; }
        private void Pass0to5() { NoPower = false; }

        private void Pass1to0() { FidNotInPower = false; NoPower = true; }
        private void Pass1to2() { FidNotInPower = false; }
        private void Pass1to3() { FidNotInPower = false; }
        private void Pass1to4() { FidNotInPower = false; }
        private void Pass1to5() { FidNotInPower = false; }

        private void Pass2to0() { NoPower = true; }
        private void Pass2to1() { FidNotInPower = true; }
        private void Pass2to3() { SwitchFider = true; }
        private void Pass2to4() { SwitchFider = true; }

        private void Pass3to0() { NoPower = true; }
        private void Pass3to1() { FidNotInPower = true; }
        private void Pass3to2() { SwitchFider = true; }
        private void Pass3to4() { SwitchFider = true; }

        private void Pass4to0() { NoPower = true; }
        private void Pass4to1() { FidNotInPower = true; }
        private void Pass4to2() { SwitchFider = true; }
        private void Pass4to3() { SwitchFider = true; }

        private void Pass5to0() { NoPower = true; }
        private void Pass5to1() { FidNotInPower = true; }
        #endregion    

        private void InitAutomat()
        {
            _automat = new FiniteStateMachine(6, 5);

            _automat.setStates(new FiniteStateMachine.State[6] { IsState0, IsState1, IsState2, IsState3, IsState4, IsState5 });
            _automat.setPassages(new FiniteStateMachine.Pass[6, 6]

					{	{null,			Pass0to1,	    Pass0to2,	    Pass0to3,	        Pass0to4,           Pass0to5},
						{Pass1to0,		null,		    Pass1to2,	    Pass1to3,	        Pass1to4,           Pass1to5},
						{Pass2to0,		Pass2to1,       null,		    Pass2to3,		    Pass2to4,           null},
						{Pass3to0,	    Pass3to1,   	Pass3to2,	    null,		        Pass3to4,           null},
                        {Pass4to0,		Pass4to1,   	Pass4to2,	    Pass4to3,	        null,               null},
                        {Pass5to0,      Pass5to1,       null,       	null,	            null,               null}});
        }
        #endregion

        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();

            _automat.TestState();
        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            InitAutomat();

            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // пытаемся получить резервный фидер
            _resFider = AssignToRefObj("ResFider");

            // пытаемся получить второй резервный фидер
            _resDFider = AssignToRefObj("ResFider1");
            
            // проверяем, что есть ссылка на резервный фидер
            if (_resFider != null)
            {
                // проверяем тип
                if (_resFider.Type.GroupID != _controlObj.Type.GroupID || 
                    _resFider.Type.SubGroupID != _controlObj.Type.SubGroupID || 
                    _controlObj.Type.SubGroupID != 8)
                {
                    throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: ResFider(" + _resFider.Name + ") не является устройством электропитания");
                }

                // проверяем корректность ссылки на второй резервный фидер
                if (_resDFider != null)
                {
                    // проверяем тип
                    if (_resDFider.Type.GroupID != _controlObj.Type.GroupID || 
                        _resDFider.Type.SubGroupID != _controlObj.Type.SubGroupID || 
                        _controlObj.Type.SubGroupID != 8)
                    {
                        throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: ResFider1(" + _resDFider.Name + ") не является устройством электропитания");
                    }
                }
            }
            // иначе - не задан хотябы один указатель - диагностирование не возможно
            else if (_resFider == null)
            {
                throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: не задано отношение на резервный фидер");
            }
            else
            {
                AddWatchToObject(_resFider);
                AddWatchToObject(_resDFider);
            }
        }

        #region State
        /// <summary>
        /// Проверка отсутствия фидера
        /// </summary>
        protected bool IsFidNotInPower
        {
            get
            {
                return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.NoPower);
            }
        }

        /// <summary>
        /// Проверка отсутствия всех фидеров
        /// </summary>
        protected bool IsBothFidNotInPower
        {
            get
            {
                return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.NoPower) &&
                            (_resFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.NoPower) &&
                            (_resDFider != null ? _resDFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.NoPower : true);
            }
        }

        /// <summary>
        /// Проверка активности фидера
        /// </summary>
        protected bool IsFidInPowerAndActive
        {
            get
            {
                return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resDFider != null ? _resDFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : true);
            }
        }

        /// <summary>
        /// Проверка активности резервного фидера 
        /// </summary>
        protected bool IsResFidInPowerAndActive
        {
            get
            {
                return (_controlObj.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resDFider != null ? _resDFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : true);
            }
        }

        /// <summary>
        /// Проверка активности фидера ДГА
        /// </summary>
        protected bool IsResDFidInPowerAndActive
        {
            get
            {
                return (_controlObj.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
                            (_resDFider != null ? _resDFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : false);
            }
        }

        /// <summary>
        /// Проверка неизвестного состояния (Если хоть один из объектов в неизвестном состоянии)
        /// </summary>
        protected bool IsUnknown
        {
            get
            {
                return true;/* (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                         || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                         || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
                         || (_resFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                         || (_resFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                         || (_resFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
                         || (_resDFider != null
                             && ((_resDFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                             || (_resDFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                             || (_resDFider.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
                         ));*/
            }
        }
        #endregion

        #region DiagStates
        /// <summary>
        /// Состояния алгоритма
        /// </summary>
        [Flags]
        public enum TaskStates : uint
        {
            /// <summary>
            /// Флаг установленного диагностического состояние "Отсутствие основного фидера"
            /// </summary>
            fl_FidNotInPower = 1,
            /// <summary>
            /// Флаг установленного диагностического состояние "Отсутствие основного и резервного питания"
            /// </summary>
            fl_NoPower = 2,
        }
        /// <summary>
        /// Текущее состояние алгоритма
        /// </summary>
        protected TaskStates _state = 0;

        /// <summary>
        /// Контролируемое диагностическое состояние: Отсутствие питания на фидере
        /// </summary>
        public virtual int DiagState_FidNotInPower
        {
            get { return 2110; }
        }
        /// <summary>
        /// Контролируемое диагностическое состояние: Переключение фидеров
        /// </summary>
        public virtual int DiagState_SwitchPower
        {
            get { return 2112; }
        }
        /// <summary>
        /// Контролируемое диагностическое состояние: Отсутствие основного и резервного электропитания
        /// </summary>
        public virtual int DiagState_NoPower
        {
            get { return 2101; }
        }

        /// <summary>
        /// Отсутствие питания на основном фидере
        /// </summary>
        protected bool FidNotInPower
        {
            set
            {
                if (value)
                {
                    if ((_state & TaskStates.fl_FidNotInPower) == 0)
                    {
                        ActivateObjDiagState(DiagState_FidNotInPower);
                        _state |= TaskStates.fl_FidNotInPower;
                    }
                }
                else
                {
                    if ((_state & TaskStates.fl_FidNotInPower) != 0)
                    {
                        DeactivateObjDiagState(DiagState_FidNotInPower);
                        _state &= ~TaskStates.fl_FidNotInPower;
                    }
                }
            }
        }

        /// <summary>
        /// Отсутствие основного и резервного электро-питания
        /// </summary>
        protected bool NoPower
        {
            set
            {
                if (value)
                {
                    if ((_state & TaskStates.fl_NoPower) == 0)
                    {
                        ActivateObjDiagState(DiagState_NoPower);
                        _state |= TaskStates.fl_NoPower;
                    }
                }
                else
                {
                    if ((_state & TaskStates.fl_NoPower) != 0)
                    {
                        DeactivateObjDiagState(DiagState_NoPower);
                        _state &= ~TaskStates.fl_NoPower;
                    }
                }
            }
        }

        /// <summary>
        /// Переключение фидеров
        /// </summary>
        protected bool SwitchFider
        {
            set
            {
                ActivateObjDiagState(DiagState_SwitchPower);
                DeactivateObjDiagState(DiagState_SwitchPower);
            }
        }

        #endregion

        #region Data
        /// <summary>
        /// Структура переходов
        /// </summary>
        protected FiniteStateMachine _automat = null;

        /// <summary>
        /// Резервный фидер
        /// </summary>
        BaseControlObj _resFider;

        /// <summary>
        /// Второй резервный фидер (дизель)
        /// </summary>
        BaseControlObj _resDFider;
        #endregion
    }

    /// <summary>
    /// Диагностирование устройств электропитания (резервный ФИДЕР)
    /// Диагностические состояния:                       
    /// 02 - Отсутствие питания на резервном фидере     (Контроль наличия фидера2)
    /// </summary>
    public class ResFiderDiag : UniObjectUpDiag.ObjectTask
    {
        /// <summary>
        /// Обновление состояния
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            base.UpdateObjectDiagState();

            if (IsResFidNotInPower)
                ResFidNoPower = true;
            else
                ResFidNoPower = false;

        }

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);
        }

        #region State
       
        /// <summary>
        /// Проверка отсутствия фидера
        /// </summary>
        protected bool IsResFidNotInPower
        {
            get
            {
                return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.NoPower);
            }
        }
        #endregion

        #region DiagStates
        /// <summary>
        /// Состояния алгоритма
        /// </summary>
        [Flags]
        public enum TaskStates : uint
        {
            fl_resfidnopower = 1,
        }
        /// <summary>
        /// Текущее состояние алгоритма
        /// </summary>
        protected TaskStates _state = 0;

        /// <summary>
        /// Контролируемое диагностическое состояние: Отсутствие питания на резервном фидере
        /// </summary>
        public virtual int DiagState_ResFidNotInPower
        {
            get { return 2111; }
        }

        /// <summary>
        /// Отсутствие резервного фидера
        /// </summary>
        protected bool ResFidNoPower
        {
            set
            {
                if (value) 
                {
                    if ((_state & TaskStates.fl_resfidnopower) == 0)
                    {
                        ActivateObjDiagState(DiagState_ResFidNotInPower);
                        _state |= TaskStates.fl_resfidnopower;
                    }
                }
                else 
                {
                    if ((_state & TaskStates.fl_resfidnopower) != 0)
                    {
                        _state &= ~TaskStates.fl_resfidnopower;
                        DeactivateObjDiagState(DiagState_ResFidNotInPower);
                    }
                }
            }
        }

        #endregion

        #region Data
        /// <summary>
        /// Резервный фидер
        /// </summary>
        BaseControlObj _resFider;
        #endregion
    }
    
    /// <summary>
    /// Проезд запрещающего показания
    /// - Контроль открытия светофора
    /// - Контроль занятости РЦ перед светофором
    /// - Контроль занятости РЦ за светофором
    /// - Контроль замыкания в маршруте РЦ перед светофором???
    /// - Контроль замыкания в маршруте РЦ за светофором???
    /// - Контроль категории маршрута???
    /// </summary>
   /* public class DiagTask_BunPassage : UniObjectUpDiag.ObjectTask
    {
        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override bool Create()
        {
            InitAutomat();

            if (!base.Create())
                return false;
            // подписываемся на изменение этого объекта
            AddWatchToObject(_controlObj);

            // пытаемся получить рельсовую цепь до светофора
            _preRC = AssignToRefObj("PrevRC");

            // пытаемся получить рельсовую цепь после светофора
            _postRC = AssignToRefObj("NextRC");

            // иначе - не задан хотябы один указатель - диагностирование не возможно
            if (_preRC == null || _postRC == null)
            {
                Trace.WriteLine("DiagTask_LogBusy: не задан полный набор отношений для: " + _controlObj.Name);
                return false;
            }
            else
            {
                AddWatchToObject(_preRC);
                AddWatchToObject(_postRC);
            }
            // Если РЦ за светофором является РЦ, то пытаемся получить следующую за ней
            if (_postRC.Type)
            {
                // Пытаемся получить РЦ после той, которая за светофором
                _postPostRC = AssignToRefObj(_postRC, "NextRC");
                // если она является нашим объектом контроля, то необходимо брать РЦ до _postRC
                if (_postPostRC == _controlObj)
                    _postPostRC = AssignToRefObj(_postRC, "PrevRC");
            }
            else if (_postRC.Type)//Если это стрелочная секция, то всё сложно(
            {
                // Получаем все стрелки, находящиеся в этой стрелочной секции
                // У каждой смотрим примыкающие объекты(+,-,начало) и определяем к какой стрелке примыкает наша РЦ и какой стороной стрелка к РЦ примыкает
                // 
            }
            else //Если это ещё что-то - то плохо запроэктировано
                return false;
                


            return true;
        }

        #region State
       

        /// <summary>
        /// Проверка неизвестного состояния системы (хотябы один из объектов-участников в неопределенном состояни)
        /// </summary>
        protected bool IsUnknown
        {
            get
            {
                return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                        || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                        || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
                        || (_preRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                        || (_preRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                        || (_preRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve)
                        || (_postRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                        || (_postRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                        || (_postRC.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve);
            }
        }
        #endregion

        #region DiagState
        /// <summary>
        /// Состояния алгоритма
        /// </summary>
        [Flags]
        public enum TaskStates : uint
        {
            /// <summary>
            /// Начальное состояние алгоритма
            /// </summary>
            first = 1,
           
        }
        /// <summary>
        /// Текущее состояние алгоритма
        /// </summary>
        protected TaskStates _state = TaskStates.first;
        #endregion

        #region Data
        protected FiniteStateMachine _automat = null;

        /// <summary>
        /// РЦ перед светофором
        /// </summary>
        BaseControlObj _preRC;

        /// <summary>
        /// РЦ после светофора
        /// </summary>
        BaseControlObj _postRC;

        /// <summary>
        /// РЦ после РЦ после светофора
        /// </summary>
        BaseControlObj _postPostRC;
        #endregion
    }*/


	/// <summary>
	/// Диагностирование переключения фидеров
	/// </summary>
	public class MainFiderSwitchDiag : UniObjectUpDiag.ObjectTask
	{
		#region FiniteStateMachine

		#region States
		
		/// <summary>
		/// Основной фидер активен
		/// </summary>
		/// <returns></returns>
		private bool IsState2() { return IsFidInPowerAndActive; }
		/// <summary>
		/// Резервный фидер активен
		/// </summary>
		/// <returns></returns>
		private bool IsState3() { return IsResFidInPowerAndActive; }
		/// <summary>
		/// ДГА фидер активен
		/// </summary>
		/// <returns></returns>
		private bool IsState4() { return IsResDFidInPowerAndActive; }
		/// <summary>
		/// Состояние системы не влияющее на диагностику
		/// </summary>
		/// <returns></returns>
		private bool IsState5() { return IsUnknown; }
		#endregion

		#region Passages

		private void Switch() { SwitchFider = true; }

		#endregion

		private void InitAutomat()
		{
			_automat = new FiniteStateMachine(4, 3);

			_automat.setStates(new FiniteStateMachine.State[4] { IsState2, IsState3, IsState4, IsState5 });
			_automat.setPassages(new FiniteStateMachine.Pass[4, 4]
					{	
						{null,		    Switch,		    Switch,           null},
						{Switch,	    null,		    Switch,           null},
                        {Switch,	    Switch,	        null,             null},
                        {null,       	null,	        null,             null}});
		}
		#endregion

		/// <summary>
		/// Обновление состояния
		/// </summary>
		public override void UpdateObjectDiagState()
		{
			base.UpdateObjectDiagState();

			_automat.TestState();
		}

		/// <summary>
		/// Инициализация алгоритма
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			InitAutomat();

            base.Create();
			// подписываемся на изменение этого объекта
			AddWatchToObject(_controlObj);

			// пытаемся получить резервный фидер
			_resFider = AssignToRefObj("ResFider");

			// пытаемся получить второй резервный фидер
			_resDFider = AssignToRefObj("ResFider1");

			// проверяем, что есть ссылка на резервный фидер
			if (_resFider != null)
			{
				// проверяем тип
				if (_resFider.Type.GroupID != _controlObj.Type.GroupID ||
					_resFider.Type.SubGroupID != _controlObj.Type.SubGroupID ||
					_controlObj.Type.SubGroupID != 8)
				{
                    throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: ResFider(" + _resFider.Name + ") не является устройством электропитания");
				}

				AddWatchToObject(_resFider);

			}
			// проверяем корректность ссылки на второй резервный фидер
			if (_resDFider != null)
			{
				// проверяем тип
				if (_resDFider.Type.GroupID != _controlObj.Type.GroupID ||
					_resDFider.Type.SubGroupID != _controlObj.Type.SubGroupID ||
					_controlObj.Type.SubGroupID != 8)
				{
                    throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: ResFider1(" + _resDFider.Name + ") не является устройством электропитания");
				}

				AddWatchToObject(_resDFider);
			}
			
			// не задан хотябы один указатель - диагностирование не возможно
			if (_resFider == null)
			{
                throw new ArgumentException(_controlObj.Name + "/MainFiderDiag: не задано отношение на резервный фидер");
			}
		}

		#region State
		
		/// <summary>
		/// Проверка активности фидера
		/// </summary>
		protected bool IsFidInPowerAndActive
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resDFider != null ? _resDFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : true);
			}
		}

		/// <summary>
		/// Проверка активности резервного фидера 
		/// </summary>
		protected bool IsResFidInPowerAndActive
		{
			get
			{
				return (_controlObj.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resDFider != null ? _resDFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : true);
			}
		}

		/// <summary>
		/// Проверка активности фидера ДГА
		/// </summary>
		protected bool IsResDFidInPowerAndActive
		{
			get
			{
				return (_controlObj.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resFider.ObjState.Current != (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate) &&
							(_resDFider != null ? _resDFider.ObjState.Current == (int)Unification.v2.Uni2States.PowerSupplyStates.InPowerAndActivate : false);
			}
		}

		/// <summary>
		/// Проверка неизвестного состояния (Если хоть один из объектов в неизвестном состоянии)
		/// </summary>
		protected bool IsUnknown
		{
			get
			{
				return (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
                         || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
                         || (_controlObj.ObjState.Current == (int)Unification.v2.Uni2States.GeneralStates.ObjectConserve);
                         
			}
		}
		#endregion

		#region DiagStates
		

		/// <summary>
		/// Контролируемое диагностическое состояние: Переключение фидеров
		/// </summary>
		public virtual int DiagState_SwitchPower
		{
			get { return 2112; }
		}
		
		/// <summary>
		/// Переключение фидеров
		/// </summary>
		protected bool SwitchFider
		{
			set
			{
				ActivateObjDiagState(DiagState_SwitchPower);
				DeactivateObjDiagState(DiagState_SwitchPower);
			}
		}

		#endregion

		#region Data
		/// <summary>
		/// Структура переходов
		/// </summary>
		protected FiniteStateMachine _automat = null;

		/// <summary>
		/// Резервный фидер
		/// </summary>
		BaseControlObj _resFider;

		/// <summary>
		/// Второй резервный фидер (дизель)
		/// </summary>
		BaseControlObj _resDFider;
		#endregion
	}
	#endregion

}