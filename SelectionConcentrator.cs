﻿using System;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

using AfxEx;
using Tdm;
using Ivk.IOSys;
using Dlx.Extensions;

namespace Tdm.Unification
{
	/// <summary>
	/// Выборка сигналов по одной станции
	/// </summary>
	public class Selection : DlxObject
	{
		/// <summary>
		/// Путь к подсистеме - источнику
		/// </summary>
		protected string _iosyssrc;
		/// <summary>
		/// Путь к подсистеме - потребителю
		/// </summary>
		protected string _iosystrg;
		/// <summary>
		/// Подсистема - источник
		/// </summary>
		public BaseIOSystem IOSysSrc;
		/// <summary>
		/// Подсистема - потребитель
		/// </summary>
		public BaseIOSystem IOSysTrg;
		/// <summary>
		/// Соответсвие сигналов (строки)
		/// </summary>
		protected Dictionary<string, string> _signalsStr = new Dictionary<string,string>();
		/// <summary>
		/// Соответствие сигналов (указатели)
		/// </summary>
		protected Dictionary<IOSignal, IOSignal> _signals = new Dictionary<IOSignal,IOSignal>();
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_iosyssrc = Loader.GetAttributeString("iosyssrc", string.Empty);
			_iosystrg = Loader.GetAttributeString("iosystrg", string.Empty);

			// загружаем соответсвие сигналов
			List<XmlElement> Src;
			Loader.CurXmlElement.EnumInhElements(out Src, "SIGNAL", null, null);
			string src, trg;
			foreach (XmlElement elem in Src)
			{
				if (elem.GetAttribute("source", out src) && elem.GetAttribute("target", out trg))
				{
					_signalsStr[src] = trg;
				}
			}
		}
		/// <summary>
		/// Инициализация - разрешаем ссылки на сигналы
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
            if (Owner.Owner is SelectionConcentrator)
            {
                // получаем подсистемы
                IOSysSrc = (Owner.Owner as SelectionConcentrator).SiteList.FindObject(Name + "/" + _iosyssrc) as BaseIOSystem;
                IOSysTrg = (Owner.Owner as SelectionConcentrator).SiteList.FindObject(Name + "/" + _iosystrg) as BaseIOSystem;
                if (IOSysSrc == null || IOSysTrg == null)
                {
                    throw DlxExceptionCreator.ArgumentException(this, null, "Не найдены подсистемы: " + Name + "/" + _iosyssrc + " или " + Name + "/" + _iosystrg);
                }
                // разрешаем ссылки на сигналы
                IOSignal s1, s2;
                foreach (KeyValuePair<string, string> keyv in _signalsStr)
                {
                    s1 = IOSysSrc.FindObject(keyv.Key) as IOSignal;
                    s2 = IOSysTrg.FindObject(keyv.Value) as IOSignal;
                    if (s1 != null && s2 != null)
                        _signals[s1] = s2;
                    else
                    {
                        throw DlxExceptionCreator.ArgumentException(this, null, "Ненайдены сигналы: " + Name + "/" + _iosyssrc + "/" + keyv.Key + " или " + Name + "/" + _iosystrg + "/" + keyv.Value);
                    }
                }
                // строки нам уже не нужны
                _signalsStr.Clear();
                // добавляем зависимость
                (Owner.Owner as SelectionConcentrator).AddUpdateDependence(IOSysSrc, this);
            }
            else
                throw DlxExceptionCreator.ArgumentException(this, null, "Owner is not SelectionConcentrator");

			base.Create();
		}
		/// <summary>
		/// Произвести синхронизацию сигналов
		/// </summary>
		/// <returns>true если были изменения</returns>
		public bool ApplyToTarget()
		{
			bool bChanged = false;
			foreach (KeyValuePair<IOSignal, IOSignal> keyv in _signals)
			{
				try
				{
					// был отказ и сейчас отказ
					if (keyv.Key.IsErrorState && keyv.Value.IsErrorState)
						continue;
					// сейчас отказ, а раньше нет
					else if (keyv.Key.IsErrorState && !keyv.Value.IsErrorState)
					{
						keyv.Value.SetToInitialState();
						bChanged = true;
					}
					// теперь не отказ, а был отказ
					else if (keyv.Value.IsErrorState)
					{
						keyv.Value.State = keyv.Key.State;
						bChanged = true;
					}
					// нет и небыло отказа
					else if (keyv.Value.State != keyv.Key.State)
					{
						keyv.Value.State = keyv.Key.State;
						bChanged = true;
					}
				}
				catch
				{
					keyv.Value.SetToInitialState();
					bChanged = true;
				}
			}
			IOSysTrg.DataTime = IOSysSrc.DataTime;
			return bChanged;
		}
	}
	/// <summary>
	/// Концентратор с выборкой сигналов
	/// </summary>
	public class SelectionConcentrator : ProcessConcentrator
	{
		/// <summary>
		/// Выбранные данные по подсистеме
		/// </summary>
		public class IOData
		{
			public IOData(DateTime time, byte[] data, BaseIOSystem iosys)
			{
				_time = time;
				_data = data;
				_iosys = iosys;
			}
			public DateTime _time;
			public byte[] _data;
			public BaseIOSystem _iosys;
		}
		/// <summary>
		/// Коллекция выборок
		/// </summary>
		public class SelectionCollection : DlxCollection
		{
			public SelectionCollection(string Name, SelectionConcentrator concentrator)
				:base(Name)
			{
				Owner = concentrator;
			}
		}

		/// <summary>
		/// Карта зависимости подсистем для обновления
		/// </summary>
		protected Dictionary<BaseIOSystem, Selection> _updateDependence = new Dictionary<BaseIOSystem, Selection>();
		/// <summary>
		/// Список станций
		/// </summary>
		protected DlxObject _siteList;
		/// <summary>
		/// Список селекции
		/// </summary>
		protected DlxCollection _selections;
		/// <summary>
		/// Список станций
		/// </summary>
		public DlxObject SiteList
		{
			get { return _siteList; }
		}

		/// <summary>
		/// Добавить зависимость обновления
		/// </summary>
		/// <param name="iosys">подсистема ввода</param>
		/// <param name="uniObjs">подсистема ввода</param>
		public void AddUpdateDependence(BaseIOSystem iosyssrc, Selection selection)
		{
			if (iosyssrc != null && selection != null)
				_updateDependence.Add(iosyssrc, selection);
		}
		
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			// загружаем список станций
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxObject;
						
			// получаем элменты с именем тега Selection
			List<XmlElement> Src;
			Loader.CurXmlElement.EnumInhElements(out Src, "Selection", null, null);
			// загружаем в коллекцию по типу Selection
			_selections = new SelectionCollection("SELECTIONS", this);
			_selections.LoadChildren(Loader, Src, typeof(Selection));
		}

		public override void Create()
		{
            _selections.Create();
			base.Create();
		}

		public override void PreCreate()
		{
            _selections.PreCreate();
			base.PreCreate();
		}

		/*public override void Destroy()
		{
			_selections.Destroy();
			base.Destroy();
		}

		public override void PostDestroy()
		{
			_selections.PostDestroy();
			base.PostDestroy();
		}*/

		protected override void UpdateDataBy(BaseIOSystem iosys)
		{
			Selection selection;
			if (!_updateDependence.TryGetValue(iosys, out selection))
				return;

			bool bChanged = false;
			BaseIOSystem iosystrg = selection.IOSysTrg;
			//выполняем цикл переноса значений сигналов для iosys в iosys_sel
			bChanged = selection.ApplyToTarget();

			// если есть изменения
			if (bChanged)
			{
				//производим обновления данных модуля 
				foreach (DlxObject obj in iosystrg.SubElements.Objects)
				{
					if (obj is IOModule)
						(obj as IOModule).Update();
				}
				// упаковываем данные
				object data = PackDataForIOSys(iosystrg);
				// отдаем потребителям
				AddDataToConsumers(data, iosystrg);
			}
		}

		protected virtual object PackDataForIOSys(BaseIOSystem iosys)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			// упаковываем данные elem.RawData
			foreach (IOBaseElement elem in iosys.SubElements.Objects)
			{
				writer.Write(elem.RawData);
			}
			writer.Flush();
			writer.Close();

			byte[] data = ms.ToArray();
			ms.Close();
			return new IOData(iosys.DataTime, data, iosys);
		}

		public override object GetOverallData(IOBaseElement requiredElement)
		{
			if (requiredElement is BaseIOSystem)
			{
				//производим обновления данных модуля 
				foreach (DlxObject obj in requiredElement.SubElements.Objects)
				{
					if (obj is IOModule)
						(obj as IOModule).Update();
				}
				// отдаем потребителям
				return PackDataForIOSys(requiredElement as BaseIOSystem);
			}
			else
				return null;
		}
	}
}
