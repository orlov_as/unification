﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AfxEx;
using LibEx;
using Dlx.Extensions;
using Tdm.Arm.Prot.Model;

namespace Tdm.Unification
{
    class UniSite : Tdm.Site, IMenuItemNameProvider
    {
        public string LocalName;

        /// <summary>
        /// Загружает и создаёт все необходимые данные для работы форм АТО
        /// </summary>
        /// <param name="loader">объект-загрузчик</param>
        public override void Load(DlxLoader loader)
        {
            base.Load(loader);

            LocalName = loader.GetAttributeString("NAME", string.Empty);
        }

        public string UpdateItemName(string itemName)
        {
            return itemName == string.Empty ? itemName : itemName.Replace("%NAME%", LocalName);
        }
    }
}
