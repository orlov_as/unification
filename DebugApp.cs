﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using AdvCon;
using AfxEx;
using Tdm;
using Tdm.Unification;
using Ivk;
using Ivk.IOSys;

namespace UnificationDebug
{
	public class DebugApp : Application
	{
		protected string _CmdFileName = string.Empty;
		public override void Load(AfxEx.DlxLoader Loader)
		{
			base.Load(Loader);

			_concentrator = Loader.GetObjectFromAttribute("Concentrator") as IDebugConcentrator;
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxCollection;
		}
		public override int Run(string[] args)
		{
			for (int Pos = 0; Pos + 1 < args.Length; Pos++)
			{
				string Str = args[Pos];
				if (((Str[0] == '-') || (Str[0] == '/')) && (Str.Substring(1) == "c"))
				{
					_CmdFileName = args[Pos + 1];
					break;
				}
			}

			return base.Run(args);
		}

		public override int Execute()
		{
			// если задан командный файл
			if (_CmdFileName.Length != 0)
				OnUseFile(_CmdFileName);// обработка командного файла

			return base.Execute();
		}

		public override void Create()
		{
			if (_concentrator == null)
			{
				throw new ArgumentException("Невозможно использовать UnificationDebug.DebugApp без концентратора DebugConcentrator!");
			}

			if (_siteList != null)
			{
				// поиск в первом уровне вложенности
				foreach (DlxObject obj in _siteList.Objects)
				{
					UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
					if (objSys != null)
					{
						_idobjsysTOrefMAP.Add(objSys.SiteID, objSys);

						BaseIOSystem ioSys = obj.GetObject("IOSys") as BaseIOSystem;
						if (ioSys != null)
							_idiosysTOrefMAP.Add(objSys.SiteID, ioSys);
					}
				}
				// если ничего не нашли
				if (_idobjsysTOrefMAP.Count == 0)
				{
					// поиск во втором уровне вложенности
					foreach (DlxCollection subcoll in _siteList.Objects)
					{
						if (subcoll != null)
						{
							foreach (DlxObject obj in subcoll.Objects)
							{
								UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
								if (objSys != null)
								{
									_idobjsysTOrefMAP.Add(objSys.SiteID, objSys);

									BaseIOSystem ioSys = obj.GetObject("IOSys") as BaseIOSystem;
									if (ioSys != null)
										_idiosysTOrefMAP.Add(objSys.SiteID, ioSys);
								}
							}
						}
					}
				}
			}

            base.Create();
		}
		
		protected override bool OnCommand(string cmd, string param)
		{
			if ( cmd == "ss" )
				return OnSignalChangeCommand(param);
			else if( cmd == "os" )
				return OnObjectChangeCommand(param, false);
			else if (cmd == "ods")
				return OnObjectChangeCommand(param, true);
			else if (cmd == "oss")
				return OnObjectDataCommand(param);
			else if (cmd == "guardall" && param.Length == 0 )
				return OnGuardAll();
			else if (cmd == "guard" )
				return OnGuard(param);
			else if (cmd == "file")
				return OnUseFile(param);
			else if (cmd == "delay")
				return OnDelay(param);
			else if (cmd == "storediag")
				return OnStroreDiag(param);
			else if (cmd == "echo")
			{
				Console.WriteLine(param);
				return true;
			}

			return base.OnCommand(cmd, param);
		}

		protected bool OnStroreDiag(string param)
		{
			if (param == null || param.Length == 0)
				return false;
			uint SiteID = uint.Parse(param);
			try
			{
				UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
				StreamWriter sw = new StreamWriter("diags.txt", false, Encoding.Default);

				foreach (IOBaseElement elem in sys.UniObjsDictionary.Values)
				{
					UniObject obj;
					if (elem is UniObject)
						obj = elem as UniObject;
					else
						continue;

					foreach (int id in obj._diagStateTOtimeMAPuni.Keys)
					{
						sw.WriteLine(string.Format("Site={0}, Obj={1}, State={2}, Time={3}", sys.SiteID, obj.Number, id, obj._diagStateTOtimeMAPuni[id].ToString("yyyy.MM.dd HH:mm:ss")));
					}
				}
				sw.Flush();
				sw.Close();
			}
			catch (KeyNotFoundException)
			{
				Console.WriteLine("*** site {0} not found!", SiteID);
			}
			return true;
		}

		protected bool OnSignalChangeCommand(string param)
		{
			string[] pars = param.Split(' ');
			if (pars.Length == 3)
			{
				uint SiteID = uint.Parse(pars[0]);
				string SignalName = pars[1];

				uint State = 0;
				if (pars[2] == "M" || pars[2] == "М")
					State = 2;
				else
					State= uint.Parse(pars[2]);
				

				try
				{
					BaseIOSystem iosys = _idiosysTOrefMAP[SiteID];
					IOSignal sig = iosys.FindObject(SignalName) as IOSignal;
					if (sig != null)
					{
						sig.State = State;
						//sig.GetOwnerModule().__DEBUG_setdatachanged();
						Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", sig.Owner.Owner.Name, sig.Owner.Name, sig.Name, State);
					}
					else
					{
						Console.WriteLine("*** signal {0} not found!", SignalName);
					}
				}
				catch (KeyNotFoundException)
				{
					Console.WriteLine("*** site {0} not found!", SiteID);
				}
			}
			else
				Console.WriteLine("*** use format: SiteID SignalName State");

			return true;
		}

		protected bool OnObjectChangeCommand(string param, bool isDiag)
		{
			string[] pars = param.Split(' ');
			if (pars.Length == 3)
			{
				uint SiteID = uint.Parse(pars[0]);
				string ObjName = pars[1];
				ushort State = ushort.Parse(pars[2]);

				try
				{
					UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
					UniObject obj = sys.FindObject(ObjName) as UniObject;
					sys.DataTime = DateTime.Now;
					if (obj != null)
					{
						if (!isDiag)
						{
							obj.SetObjState(State);
							Console.WriteLine("*** object {0} to state {1}", ObjName, State);
						}
						else
						{
							try
							{
								DateTime time = obj._diagStateTOtimeMAPuni[State];
								obj.DeactivateObjDiagState(State);
								Console.WriteLine("*** object {0} deactive diag {1}", ObjName, State);
							}
							catch (KeyNotFoundException)
							{
								obj.ActivateObjDiagState(State);
								Console.WriteLine("*** object {0} active diag {1}", ObjName, State);
							}
						}
					}
					else
					{
						Console.WriteLine("*** object {0} not found!", ObjName);
					}
				}
				catch (KeyNotFoundException)
				{
					Console.WriteLine("*** site {0} not found!", SiteID);
				}
			}
			else
				Console.WriteLine("*** use format: SiteID ObjName State");
			return true;
		}

		protected bool OnObjectDataCommand(string param)
		{
			string[] pars = param.Split(' ');
			if (pars.Length == 3)
			{
				uint SiteID = uint.Parse(pars[0]);
				string ObjDataName = pars[1];
				uint State = 0;
				if (pars[2] == "M" || pars[2] == "М")
					State = 2;
				else
					State = uint.Parse(pars[2]);

				try
				{
					UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
					Tdm.ObjData objdata = sys.FindObject(ObjDataName) as Tdm.ObjData;
					if (objdata != null)
					{
						Console.WriteLine("*** objdata {0} signals to state {1}", ObjDataName, State);

						if (objdata.IsIOSignal)
						{
							objdata.IOSignal.StateBitCount = 2;
							objdata.IOSignal.State = State;
							objdata.IOSignal.GetOwnerModule().GeneralState.Current = IOGeneralState.Normal;
							//objdata.IOSignal.GetOwnerModule().__DEBUG_setdatachanged();
							Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", objdata.IOSignal.Owner.Owner.Name, objdata.IOSignal.Owner.Name, objdata.IOSignal.Name, State);
						}
						else if (objdata.IsSigDiscretLogic)
						{
							
							foreach (IOSignal sig in objdata.SigDiscreteLogic.IOSignals)
							{
								sig.StateBitCount = 2;
								sig.State = State;
								sig.GetOwnerModule().GeneralState.Current = IOGeneralState.Normal;
								//sig.GetOwnerModule().__DEBUG_setdatachanged();
								Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", sig.Owner.Owner.Name, sig.Owner.Name, sig.Name, State);
							}
						}
						else
							Console.WriteLine("*** unknown type of object data {0}", ObjDataName);
					}
					else
					{
						Console.WriteLine("*** object data {0} not found!", ObjDataName);
					}
				}
				catch (KeyNotFoundException)
				{
					Console.WriteLine("*** site {0} not found!", SiteID);
				}
			}
			else
				Console.WriteLine("*** use format: SiteID ObjName State");
			return true;
		}

		protected bool OnGuardAll()
		{
			foreach (UniObjSystem objsys in _idobjsysTOrefMAP.Values)
			{
				_concentrator.GuardElement(objsys);
			}

			foreach (BaseIOSystem iosys in _idiosysTOrefMAP.Values)
			{
				_concentrator.GuardElement(iosys);
			}
			Console.WriteLine("*** выключаем потоки получения данных");
			_concentrator.StopAllClients();

			return true;
		}

		protected bool OnGuard(string param)
		{
			IOBaseElement elem = FindObject(param) as IOBaseElement;
			if (elem != null)
				_concentrator.GuardElement(elem);
			else
				Trace.WriteLine("элемент не найден!");

			return true;
		}

		protected bool OnDelay(string param)
		{
			int MSec = int.Parse(param);
			Console.WriteLine("*** пауза {0} миллисекунд...", MSec);
			Thread.Sleep(MSec);
			return true;
		}

		protected bool OnUseFile(string param)
		{
			try
			{
				using (StreamReader sr = new StreamReader(param) )
				{
					Console.WriteLine("***************************************************************");
					Console.WriteLine("*** использование комманд из файла " + param);
					Console.WriteLine("***************************************************************");

					String line;
					while ((line = sr.ReadLine()) != null)
					{
						if (line.Length > 0 && !line.StartsWith("//"))
						{
							int pos = line.IndexOf(' ');
							if( pos > 0 )
								OnCommand(line.Substring(0, pos), line.Substring(pos + 1));
							else
								OnCommand(line, string.Empty);
						}
					}

					Console.WriteLine("***************************************************************");
					Console.WriteLine("*** окончание обработки комманд из файла " + param);
					Console.WriteLine("***************************************************************");
				}
			}
			catch (Exception e)
			{
				// Let the user know what went wrong.
				Console.WriteLine();
				Console.WriteLine("*** Error to process file:");
				Console.WriteLine(e.Message);
			}
			return true;
		}
		/// <summary>
		/// ссылка на коллекцию контролируемых станций
		/// </summary>
		protected DlxCollection _siteList = null;
		protected Dictionary<uint, UniObjSystem> _idobjsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
		protected Dictionary<uint, BaseIOSystem> _idiosysTOrefMAP = new Dictionary<uint, BaseIOSystem>();
		protected IDebugConcentrator _concentrator = null;
	}
}
