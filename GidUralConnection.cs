﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using AfxEx;
using Ivk.IOSys;
using Tdm;
using Dps.UniRequest;
using Dps.UniTcpIp;

namespace Tdm.Unification
{
	/// <summary>
	/// Класс соединения по TcpIp с ГИД Урал
	/// </summary>
	class GidUralConnection : TcpConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="sock">tcp клиент</param>
		public GidUralConnection(Socket sock)
			: base(sock, true) 
		{
		}

		/// <summary>
		/// Переопределен метод для отправки заголовка
		/// </summary>
		/// <param name="Hd">заголовк </param>
		protected override void OnSendHeader(Header Hd)
		{
			if (Hd == null)
				throw new ArgumentNullException("Header");
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);

			_Stream.WriteTimeout = _Timeout;
			//ничего не отправляем
		}
		/// <summary>
		/// Переопределен метод для получения заголовка 
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();

			_Stream.ReadTimeout = _Timeout;

			// как-будто получили запрос
			Hd.Code = (RequestCode)0x01;
			Hd.SubCode = 0;
			Hd.ID = 0;
			Hd.Size = 0;
			
			return Hd;
		}
	}

	/// <summary>
	/// Серверное соединения по протоколу с ГИД Урал
	/// </summary>
	public class GidUralServerConnection : ServerConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public GidUralServerConnection()
		{
			_concentrator = null;
		}
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="concentrator">концентратор</param>
		/// <param name="idsysTOrefMAP">словарь станций</param>
		public GidUralServerConnection(Concentrator concentrator, Dictionary<int, BaseIOSystem> idsysTOrefMAP)
		{
			_concentrator = concentrator;
			_idsysTOrefMAP = idsysTOrefMAP;

			// для всех станций собираем начальное состояние
			foreach (KeyValuePair<int, BaseIOSystem> keyv in _idsysTOrefMAP)
			{
				// находим подсистему 
				BaseIOSystem iosys = keyv.Value;
				// если нашли, то пописываемся на изменения
				if (iosys != null)
				{
					Monitor.Enter(iosys);
					try
					{
						// записываем начальное сосотяние объектов
						object dataobj = _concentrator.GetOverallData(iosys);
						if (dataobj != null && dataobj is SelectionConcentrator.IOData)
						{
							_idsysTOlastdata[keyv.Key] = (dataobj as SelectionConcentrator.IOData)._data;
							_idsysTOlastdatatime[keyv.Key] = (dataobj as SelectionConcentrator.IOData)._time;
						}

						// подписываемся на извещение о наличии данных
						concentrator.AddConsumerQueue(_dataQueue, iosys, _eventOnData);
					}
					catch (Exception e)
					{
						Debug.WriteLine("GidUralServerConnection: ERROR to write initial state for " + iosys.Owner.Name + ": " + e.ToString());
					}
					finally
					{
						Monitor.Exit(iosys);
					}
				}
			}
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			_concentrator.RemoveConsumerQueue(_dataQueue);
			_dataQueue.Clear();
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)701, "Закрытие соединения с ГИД Урал");
			base.Dispose(disposing);
		}
		/// <summary>
		/// Обработать запрос
		/// </summary>
		/// <param name="Msg">Обрабатывать входящее сообщение ненужно</param>
		protected override void ExcecuteRequest(Message Msg)
		{
			return;
		}
		/// <summary>
		/// получить ответ
		/// </summary>
		/// <returns>сообщение ответа</returns>
		protected override Message GetAnswer()
		{
			Message Answer = new Message();
			Answer.Code = (RequestCode)0x1;
			Answer.PartMessage = true;

			// ждем изменения в течении 58 секунд
			if (!_bFirst && (_dataQueue.Count > 0 || _eventOnData.WaitOne(58000/2, false)))
			{
				// создаем поток для записи
				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);

				// пока есть хотябы один элемент в очереди
				while (_dataQueue.Count > 0)
				{
					Object data = null;
					// блокируем очередь данных
					Monitor.Enter(_dataQueue);
					try
					{
						// получаем элемент
						data = _dataQueue.Peek();
					}
					finally
					{
						// разблокируем очередь
						Monitor.Exit(_dataQueue);
					}

					// вызываем обработку
					if (data != null)
					{
						// находим станцию данных в writer
						if (data is SelectionConcentrator.IOData)
						{
							foreach(KeyValuePair<int, BaseIOSystem> keyv in _idsysTOrefMAP)
							{
								// нашли станцию
								if (keyv.Value == (data as SelectionConcentrator.IOData)._iosys)
								{
									// запоминаем данные и время
									_idsysTOlastdata[keyv.Key] = (data as SelectionConcentrator.IOData)._data;
									_idsysTOlastdatatime[keyv.Key] = (data as SelectionConcentrator.IOData)._time;
									// пишем в поток
									WriteMsgForSite(writer, keyv.Key);
									break;
								}
							}
						}
					}

					// блокируем очередь данных
					Monitor.Enter(_dataQueue);
					try
					{
						// удаляем элемент из очереди
						_dataQueue.Dequeue();
					}
					finally
					{
						// разблокируем очередь
						Monitor.Exit(_dataQueue);
					}
				}

				// проверяем время отправки по станциям
				foreach (int i in _idsysTOrefMAP.Keys)
				{
					// отправляем для станций, отправка для которой была более 58 секунд назад
					if (((TimeSpan)(DateTime.Now - _idsysTOlastsendtime[i])).TotalSeconds > 58/2)
					{
						WriteMsgForSite(writer, i);
					}
				}

				writer.Flush();
				writer.Close();
				// получаем массив данных для ответа
				Answer.Data = ms.ToArray();
				// закрываем поток
				ms.Close();
			}
			else // либо первое сообщение, либо нет изменений в течении 58 секунд
			{
				// создаем поток для записи
				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);
				
				// для всех станций 
				foreach (KeyValuePair<int, BaseIOSystem> keyv in _idsysTOrefMAP)
				{
					WriteMsgForSite(writer, keyv.Key);
				}

				writer.Flush();
				writer.Close();
				// получаем массив данных для ответа
				Answer.Data = ms.ToArray();
				// закрываем поток
				ms.Close();
			}

			_bFirst = false;
			Debug.WriteLine("GidUralServerConnection: Отправляем size = " + Answer.Data.Length);
			// возвращаем ответ
			return Answer;
		}
		/// <summary>
		/// Записать сообщение ГИД Урал
		/// </summary>
		/// <param name="writer"></param>
		/// <param name="siteid"></param>
		protected void WriteMsgForSite(BinaryWriter writer, int siteid)
		{
			long pos1, pos2, pos3;
			byte CRC;
			pos1 = writer.BaseStream.Position;
			// пишем заголовок
			writer.Write((byte)0x54);
			writer.Write((byte)0x53);
			writer.Write((UInt32)_ID++);
			writer.Write((UInt32)siteid);
			WriteDateTime(writer, _idsysTOlastdatatime[siteid]);
			writer.Write((UInt16)_idsysTOlastdata[siteid].Length);
			// подсчитываем CRC
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			writer.BaseStream.Seek(pos1, SeekOrigin.Begin);
			CRC = 0;
			while (writer.BaseStream.Position < pos2)
			{
				CRC += (byte)writer.BaseStream.ReadByte();
				//writer.BaseStream.
			}
			// записываем CRC
			writer.Write(CRC);
			// пишем данные
			writer.Write(_idsysTOlastdata[siteid]);
			// подсчитываем CRC
			writer.Flush();
			pos3 = writer.BaseStream.Position;
			writer.BaseStream.Seek(pos2 + 1, SeekOrigin.Begin);
			CRC = 0;
			while (writer.BaseStream.Position < pos3)
				CRC += (byte)writer.BaseStream.ReadByte();
			// записываем CRC
			writer.Write(CRC);
			writer.Flush();
			
			// сохраняем последние время отправки
			_idsysTOlastsendtime[siteid] = DateTime.Now;
		}
		/// <summary>
		/// Запись времени для ГИД Урал
		/// </summary>
		/// <param name="writer"></param>
		/// <param name="dateTime"></param>
		protected void WriteDateTime(BinaryWriter writer, DateTime dateTime)
		{
			writer.Write((byte)dateTime.Day);
			writer.Write((byte)dateTime.Month);
			writer.Write((UInt16)dateTime.Year);
			writer.Write((byte)dateTime.Hour);
			writer.Write((byte)dateTime.Minute);
			writer.Write((byte)dateTime.Second);
			writer.Write((UInt16)dateTime.Millisecond);
		}

		#region Данные
		/// <summary>
		/// ссылка на концентратор, который обрабатывает данные
		/// </summary>
		protected Concentrator _concentrator = null;
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<int, BaseIOSystem> _idsysTOrefMAP = null;
		/// <summary>
		/// очередь для данных 
		/// </summary>
		protected Queue _dataQueue = new Queue();
		/// <summary>
		/// событие для извещения о поступлении данных для отправки
		/// </summary>
		protected AutoResetEvent _eventOnData = new AutoResetEvent(false);

		/// <summary>
		/// MAP данных
		/// </summary>
		private Dictionary<int, byte[]> _idsysTOlastdata = new Dictionary<int,byte[]>();
		/// <summary>
		/// MAP времен данных
		/// </summary>
		private Dictionary<int, DateTime> _idsysTOlastdatatime = new Dictionary<int, DateTime>();
		/// <summary>
		/// MAP времен отправки
		/// </summary>
		private Dictionary<int, DateTime> _idsysTOlastsendtime = new Dictionary<int, DateTime>();
		/// <summary>
		/// Признак первого сообщения
		/// </summary>
		protected bool _bFirst = true;
		#endregion
	}

	/// <summary>
	/// Фабрика серверных соединений по протоколу ГИД Урал
	/// </summary>
	public class GidUralServerConnectionFactory : UnificServerConnectionFactory
	{
		/// <summary>
		/// констркутор
		/// </summary>
		public GidUralServerConnectionFactory()
		{
		}
		/// <summary>
		/// Загрузка соответствия идентификаторов ГИД Урал
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			List<XmlElement> kpAccordance;
			Loader.CurXmlElement.EnumInhElements(out kpAccordance, "kp", null, null);
			int kpid = 0;
			string sitepath;
			foreach (XmlElement elem in kpAccordance)
			{
				sitepath = string.Empty;
				if (elem.GetAttributeValue("kpid", ref kpid) && elem.GetAttribute("sitepath", out sitepath))
					_idsysTOpathMAP.Add(kpid, sitepath);
			}
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>true при удачной инициализации</returns>
		public override void Create()
		{
			DlxObject obj;
			foreach (int kpid in _idsysTOpathMAP.Keys)
			{
				obj = _siteList.FindObject(_idsysTOpathMAP[kpid] + "/IOSysGid" );
				if (obj != null && obj is BaseIOSystem)
					_idsysTOrefMAP.Add(kpid, obj as BaseIOSystem);
				else
				{
					throw new ArgumentException("DCkrugServerConnectionFactory " + Name + ": Can't find site by name " + _idsysTOpathMAP[kpid]);
				}
			}
			base.Create();
		}
		/// <summary>
		/// Создание серверного соединения
		/// </summary>
		/// <returns>интерфейс соединения</returns>
		public override AConnection CreateConnection()
		{
			return new GidUralServerConnection(_concentrator, _idsysTOrefMAP);
		}
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<int, BaseIOSystem> _idsysTOrefMAP = new Dictionary<int, BaseIOSystem>();
		/// <summary>
		/// MAP путей к станциям
		/// </summary>
		private Dictionary<int, string> _idsysTOpathMAP = new Dictionary<int, string>();
	}

	/// <summary>
	/// Сервер обмена с ГИД Урал
	/// </summary>
	public class GidUralServer : Server
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		public GidUralServer()
		{
		}
		/// <summary>
		/// Создание соединения
		/// </summary>
		/// <param name="client">tcp клиент</param>
		/// <returns>соединение</returns>
		public override AConnection CreateConnection(Socket client)
		{
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)700, "Подключение ГИД Урал с удаленного адреса " + IPAddress.Parse(((IPEndPoint)client.RemoteEndPoint).Address.ToString()));
			// создаем соединение 
			return new GidUralConnection(client);
		}
	}

	/// <summary>
	/// Класс модуля для увязки с ГИД урал
	/// </summary>
	public class GidUralModule : IOSigModule
	{
		public override void Create()
		{
			base.Create();
		}
		/// <summary>
		/// Переопределяем и выполняем упаковку данных в RawData
		/// </summary>
		public override void Update()
		{
			int nbyte;
			byte mask, state;
			if (RawData == null)
				RawData = new byte[_size];
			foreach (IOSignal sig in Signals.Objects)
			{
				sig.StateBitCount = 2;
				if (sig is IOSignal1Ch)
				{
					nbyte = ((sig as IOSignal1Ch).Channel.Number - 1) / 4;
					if (nbyte < RawData.Length)
					{
						mask = (byte)(3 << (2 * (3 - ((sig as IOSignal1Ch).Channel.Number-1) % 4)));
						try
						{
							state = (byte)((3 & sig.State) << (2 * (3 - ((sig as IOSignal1Ch).Channel.Number - 1) % 4)));
                            RawData[nbyte] &= (byte)(~mask);
                            RawData[nbyte] |= state;
						}
						catch// неизвестное состояние сигнала
						{
                            RawData[nbyte] |= mask;
						}
					}
				}
			}
		}
	}
}
