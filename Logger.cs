﻿using AfxEx;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Tdm.Unification
{
    /// <summary>
    /// Класс для логирования
    /// </summary>
    public static class Logger
    {
        private static ILog log = LogManager.GetLogger("LOGGER");

        static Logger()
        {
            if (log==null)
                log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            InitLogger();
        }

        public static ILog Log
        {
            get { return log; }
        }

        /// <summary>
        /// Инициализация логгера (создание папок, файлов)
        /// </summary>
        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
