﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using AfxEx;
using Tdm;
using Tdm.Unification;
using Ivk;
using Ivk.IOSys;

namespace Tdm.DpsOld
{
    public class DebugConcentrator : UnificationConcentrator, IDebugConcentrator
    {
        public override void Create()
        {
            // заводим таймер
            TimerCallback timerDelegate = new TimerCallback(this.OnUpdateTimer);
            _updateTimer = new Timer(timerDelegate, null, 1000, 1000);

            base.Create();
        }
        protected override void OnData(DataCollector.Data data)
        {
            // если не никаких данных => ошибка обмена
            if (data._dataObj == null && data._data == null && data._targetElement != null)
            {
                if (!_guardedElements.Contains(data._targetElement))
                    base.OnData(data);
                else if (data._targetElement is BaseIOSystem)
                {
                    (data._targetElement as BaseIOSystem).SetValidData();
                }
            }
            else
                base.OnData(data);
        }

        public void OnUpdateTimer(Object stateInfo)
        {
            foreach (IOBaseElement elem in _guardedElements)
            {
                if (elem is BaseIOSystem)
                {
                    (elem as BaseIOSystem).DataTime = DateTime.Now;
                    (elem as BaseIOSystem).RaiseDataChangedEvent(DateTime.Now);
                }

                if (elem is UniObjSystem)
                {
                    (elem as UniObjSystem).DataTime = DateTime.Now;
                    (elem as UniObjSystem).UpdateObjectStates();
                    (elem as UniObjSystem).RaiseDataChangedEvent(DateTime.Now);

                    (elem as UniObjSystem).UpdateObjectStates();
                    (elem as UniObjSystem).RaiseDataChangedEvent(DateTime.Now);
                }
            }
        }

        public void GuardElement(IOBaseElement elem)
        {
            _guardedElements.Add(elem);
        }

        public void StopAllClients()
        {
            foreach (DlxObject obj in Objects)
            {
                obj.NotifyDestroy();
                obj.NotifyPostDestroy();
            }
        }

        protected Timer _updateTimer = null;
        protected List<IOBaseElement> _guardedElements = new List<IOBaseElement>();
    }

    public class DebugSelConcentrator : SelectionConcentrator, IDebugConcentrator
    {
        public override void Create()
        {
            // заводим таймер
            TimerCallback timerDelegate = new TimerCallback(this.OnUpdateTimer);
            _updateTimer = new Timer(timerDelegate, null, 1000, 1000);

            base.Create();
        }
        protected override void OnData(DataCollector.Data data)
        {
            // если не никаких данных => ошибка обмена
            if (data._dataObj == null && data._data == null && data._targetElement != null)
            {
                if (!_guardedElements.Contains(data._targetElement))
                    base.OnData(data);
                else if (data._targetElement is BaseIOSystem)
                {
                    (data._targetElement as BaseIOSystem).SetValidData();
                }
            }
            else
                base.OnData(data);
        }

        public void OnUpdateTimer(Object stateInfo)
        {
            foreach (IOBaseElement elem in _guardedElements)
            {
                if (elem is BaseIOSystem)
                {
                    (elem as BaseIOSystem).DataTime = DateTime.Now;
                    (elem as BaseIOSystem).RaiseDataChangedEvent(DateTime.Now);

                    UpdateDataBy(elem as BaseIOSystem);
                }
            }
        }

        public void GuardElement(IOBaseElement elem)
        {
            _guardedElements.Add(elem);
        }

        public void StopAllClients()
        {
            foreach (DlxObject obj in Objects)
            {
                obj.NotifyDestroy();
                obj.NotifyPostDestroy();
            }
        }

        protected Timer _updateTimer = null;
        protected List<IOBaseElement> _guardedElements = new List<IOBaseElement>();
    }

    public class BDUnificationSimulation : Collector.ClientBaseOnThread
    {
        DBConnector _connector = new DBConnector();
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);

            base.Load(Loader);
            _findRoot = Loader.GetObjectFromAttribute("FindRoot") as DlxObject;

            _connector.Load(Loader);

            string time = Loader.GetAttributeString("StartTime", "");
            if (!DateTime.TryParse(time, out _startArhTime))
                _startArhTime = DateTime.Today;

            time = Loader.GetAttributeString("EndTime", "");
            if (!DateTime.TryParse(time, out _endArhTime))
                _endArhTime = DateTime.Now;
        }

        public override bool OnInitThread()
        {
            _connector.Connect();
            return true;
        }

        protected override void Run()
        {
            DateTime RealTime = DateTime.Now;
            DateTime ArhTime = _startArhTime;
            var span = new TimeSpan(0, 0, 3);
            // Всегда
            while (true)
            {
                try
                {
                    // если есть сессия
                    if (_connector.IsConnected)
                    {
                        List<UnificationConcentrator.UniDataWithObjSys> dataList = new List<UnificationConcentrator.UniDataWithObjSys>();
                        /////////////////////////////////////////////
                        // читаем из базы

                        SqlCommand sql = new SqlCommand();
                        sql.Connection = _connector.SqlConn;
                        sql.CommandText = "SELECT Site_ID, Object_ID, State_ID, Time FROM ARH_StatesDYN WHERE Time>=@StartTime AND Time<@EndTime";

                        sql.Parameters.Add("@StartTime", SqlDbType.DateTime);
                        sql.Parameters["@StartTime"].Value = ArhTime;
                        sql.Parameters.Add("@EndTime", SqlDbType.DateTime);
                        sql.Parameters["@EndTime"].Value = ArhTime + span;

                        SqlDataReader reader = sql.ExecuteReader();
                        try
                        {

                            int SiteID, ObjID, StateID;
                            DateTime BegTime;
                            while (reader.Read())
                            {
                                SiteID = reader.GetInt32(0);

                                UniObjSystem objsSys = GetObjectsSys((uint)SiteID);

                                if (objsSys == null)
                                    continue;

                                ObjID = reader.GetInt32(1);
                                StateID = reader.GetInt32(2);
                                BegTime = reader.GetDateTime(3);

                                UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
                                state._dataTime = BegTime;
                                state._siteID = (uint)SiteID;
                                state._objectID = (uint)ObjID;
                                state._uniState = (byte)StateID;

                                dataList.Add(new UnificationConcentrator.UniDataWithObjSys(state, objsSys));
                            }

                        }
                        finally
                        {
                            reader.Close();
                        }

                        sql.CommandText = "SELECT Site_ID, Param_ID, Value, Time FROM ARH_ParamsNumericUni WHERE Time>=@StartTime AND Time<@EndTime";
                        reader = sql.ExecuteReader();
                        try
                        {

                            int SiteID, ParamID;
                            DateTime BegTime;
                            float val;
                            while (reader.Read())
                            {
                                SiteID = reader.GetInt32(0);

                                UniObjSystem objsSys = GetObjectsSys((uint)SiteID);

                                if (objsSys == null)
                                    continue;

                                ParamID = reader.GetInt32(1);
                                val = reader.GetFloat(2);
                                BegTime = reader.GetDateTime(3);

                                UnificationConcentrator.UniNumericParamState state = new UnificationConcentrator.UniNumericParamState();
                                state._dataTime = BegTime;
                                state._paramTime = BegTime;
                                state._siteID = (uint)SiteID;
                                state._paramID = (uint)ParamID;
                                state._paramValue = (float)val;

                                dataList.Add(new UnificationConcentrator.UniDataWithObjSys(state, objsSys));
                            }

                        }
                        finally
                        {
                            reader.Close();
                        }

                        SetData(dataList, RealTime, null);

                        ArhTime += span;
                        RealTime += span;

                        Thread.Sleep((int)span.TotalMilliseconds);
                        if ((ArhTime - _endArhTime).TotalSeconds >= 0)
                            ArhTime = _startArhTime;
                    }
                }
                catch (ThreadInterruptedException)
                {
                    return;
                }
                catch (ThreadAbortException)
                {
                    return;
                }
                catch (Exception e)
                {
                    // закрываем сессию
                    _connector.Disconnect();
                }
                // в любом случае проверяем сесию
                if (!_connector.IsConnected)
                {
                    // если нет полученных сообщений и не требуется перезапуск, то выдерживаем время
                    Thread.Sleep(10000);

                    _connector.Connect();
                }
            }
        }

        /// <summary>
        /// Переопределено для поиска подсистемы объектов
        /// </summary>
        /// <returns>удачно ли прошло создание</returns>
        public override void Create()
        {
            if (_findRoot != null)
            {
                if (typeof(DlxCollection).IsInstanceOfType(_findRoot))
                {
                    DlxCollection coll = _findRoot as DlxCollection;
                    // поиск в первом уровне вложенности
                    foreach (DlxObject obj in coll.Objects)
                    {
                        UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
                        if (objSys != null)
                            _idsysTOrefMAP.Add(objSys.SiteID, objSys);
                    }
                    // если ничего не нашли
                    if (_idsysTOrefMAP.Count == 0)
                    {
                        // поиск во втором уровне вложенности
                        foreach (DlxCollection subcoll in coll.Objects)
                        {
                            if (subcoll != null)
                            {
                                foreach (DlxObject obj in subcoll.Objects)
                                {
                                    UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
                                    if (objSys != null)
                                        _idsysTOrefMAP.Add(objSys.SiteID, objSys);
                                }
                            }
                        }
                    }
                }
            }

            if (_idsysTOrefMAP.Count == 0)
            {
                throw new ArgumentException(Name + ": карта ТО пустая");
            }

            base.Create();
        }

        /// <summary>
        /// Получить подсистему объектов по идентификатору
        /// </summary>
        /// <param name="SiteID">идентификатор</param>
        /// <returns>подсистема объектов</returns>
        public UniObjSystem GetObjectsSys(uint SiteID)
        {
            UniObjSystem iosys = null;
            if (_idsysTOrefMAP.TryGetValue(SiteID, out iosys))
                return iosys;
            else
                return null;
        }

        /// <summary>
        /// Карта идентификаторов к указателям на подсистемы ввода
        /// </summary>
        protected Dictionary<uint, UniObjSystem> _idsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
        /// <summary>
        /// начало требуемого интервала
        /// </summary>
        protected DateTime _startArhTime;
        /// <summary>
        /// конец требуемого интервала
        /// </summary>
        protected DateTime _endArhTime;
        /// <summary>
        /// Объект - корень для поиска фабрики соединений, подсистемы ввода и подсистемы объектов
        /// </summary>
        protected DlxObject _findRoot = null;
    }

    public interface IDebugConcentrator
    {
        void GuardElement(Ivk.IOSys.IOBaseElement elem);
        void StopAllClients();
    }

    /// Закомментирован за ненадобностью, чтобы не тянуть ссылку на AdvCon.1.0.1.dll
    /// using AdvCon;
    //public class DebugApp : Application
    //{
    //    protected string _CmdFileName = string.Empty;
    //    public override void Load(AfxEx.DlxLoader Loader)
    //    {
    //        base.Load(Loader);

    //        _concentrator = Loader.GetObjectFromAttribute("Concentrator") as IDebugConcentrator;
    //        _siteList = Loader.GetObjectFromAttribute("SiteList") as DlxCollection;
    //    }
    //    public override int Run(string[] args)
    //    {
    //        for (int Pos = 0; Pos + 1 < args.Length; Pos++)
    //        {
    //            string Str = args[Pos];
    //            if (((Str[0] == '-') || (Str[0] == '/')) && (Str.Substring(1) == "c"))
    //            {
    //                _CmdFileName = args[Pos + 1];
    //                break;
    //            }
    //        }

    //        return base.Run(args);
    //    }

    //    public override int Execute()
    //    {
    //        // если задан командный файл
    //        if (_CmdFileName.Length != 0)
    //            OnUseFile(_CmdFileName);// обработка командного файла

    //        return base.Execute();
    //    }

    //    public override bool Create()
    //    {
    //        if (_concentrator == null)
    //        {
    //            Trace.WriteLine("Невозможно использовать UnificationDebug.DebugApp без концентратора DebugConcentrator!");
    //            Console.WriteLine("Невозможно использовать UnificationDebug.DebugApp без концентратора DebugConcentrator!");
    //            return false;
    //        }

    //        if (_siteList != null)
    //        {
    //            // поиск в первом уровне вложенности
    //            foreach (DlxObject obj in _siteList.Objects)
    //            {
    //                UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
    //                if (objSys != null)
    //                {
    //                    _idobjsysTOrefMAP.Add(objSys.SiteID, objSys);

    //                    BaseIOSystem ioSys = obj.GetObject("IOSys") as BaseIOSystem;
    //                    if (ioSys != null)
    //                        _idiosysTOrefMAP.Add(objSys.SiteID, ioSys);
    //                }
    //            }
    //            // если ничего не нашли
    //            if (_idobjsysTOrefMAP.Count == 0)
    //            {
    //                // поиск во втором уровне вложенности
    //                foreach (DlxCollection subcoll in _siteList.Objects)
    //                {
    //                    if (subcoll != null)
    //                    {
    //                        foreach (DlxObject obj in subcoll.Objects)
    //                        {
    //                            UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
    //                            if (objSys != null)
    //                            {
    //                                _idobjsysTOrefMAP.Add(objSys.SiteID, objSys);

    //                                BaseIOSystem ioSys = obj.GetObject("IOSys") as BaseIOSystem;
    //                                if (ioSys != null)
    //                                    _idiosysTOrefMAP.Add(objSys.SiteID, ioSys);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        bool bRes = false;
    //        try
    //        {
    //            bRes = base.Create();
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine();
    //            Console.WriteLine(ex.Message);
    //        }

    //        return bRes;
    //    }

    //    protected override bool OnCommand(string cmd, string param)
    //    {
    //        if (cmd == "ss")
    //            return OnSignalChangeCommand(param);
    //        else if (cmd == "os")
    //            return OnObjectChangeCommand(param, false);
    //        else if (cmd == "ods")
    //            return OnObjectChangeCommand(param, true);
    //        else if (cmd == "oss")
    //            return OnObjectDataCommand(param);
    //        else if (cmd == "guardall" && param.Length == 0)
    //            return OnGuardAll();
    //        else if (cmd == "guard")
    //            return OnGuard(param);
    //        else if (cmd == "file")
    //            return OnUseFile(param);
    //        else if (cmd == "delay")
    //            return OnDelay(param);
    //        else if (cmd == "storediag")
    //            return OnStroreDiag(param);
    //        else if (cmd == "echo")
    //        {
    //            Console.WriteLine(param);
    //            return true;
    //        }

    //        return base.OnCommand(cmd, param);
    //    }

    //    protected bool OnStroreDiag(string param)
    //    {
    //        if (param == null || param.Length == 0)
    //            return false;
    //        uint SiteID = uint.Parse(param);
    //        try
    //        {
    //            UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
    //            StreamWriter sw = new StreamWriter("diags.txt", false, Encoding.Default);

    //            foreach (IOBaseElement elem in sys.UniObjsDictionary.Values)
    //            {
    //                UniObject obj;
    //                if (elem is UniObject)
    //                    obj = elem as UniObject;
    //                else
    //                    continue;

    //                foreach (int id in obj._diagStateTOtimeMAPuni.Keys)
    //                {
    //                    sw.WriteLine(string.Format("Site={0}, Obj={1}, State={2}, Time={3}", sys.SiteID, obj.Number, id, obj._diagStateTOtimeMAPuni[id].ToString("yyyy.MM.dd HH:mm:ss")));
    //                }
    //            }
    //            sw.Flush();
    //            sw.Close();
    //        }
    //        catch (KeyNotFoundException)
    //        {
    //            Console.WriteLine("*** site {0} not found!", SiteID);
    //        }
    //        return true;
    //    }

    //    protected bool OnSignalChangeCommand(string param)
    //    {
    //        string[] pars = param.Split(' ');
    //        if (pars.Length == 3)
    //        {
    //            uint SiteID = uint.Parse(pars[0]);
    //            string SignalName = pars[1];

    //            uint State = 0;
    //            if (pars[2] == "M" || pars[2] == "М")
    //                State = 2;
    //            else
    //                State = uint.Parse(pars[2]);


    //            try
    //            {
    //                BaseIOSystem iosys = _idiosysTOrefMAP[SiteID];
    //                IOSignal sig = iosys.FindObject(SignalName) as IOSignal;
    //                if (sig != null)
    //                {
    //                    sig.State = State;
    //                    //sig.GetOwnerModule().__DEBUG_setdatachanged();
    //                    Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", sig.Owner.Owner.Name, sig.Owner.Name, sig.Name, State);
    //                }
    //                else
    //                {
    //                    Console.WriteLine("*** signal {0} not found!", SignalName);
    //                }
    //            }
    //            catch (KeyNotFoundException)
    //            {
    //                Console.WriteLine("*** site {0} not found!", SiteID);
    //            }
    //        }
    //        else
    //            Console.WriteLine("*** use format: SiteID SignalName State");

    //        return true;
    //    }

    //    protected bool OnObjectChangeCommand(string param, bool isDiag)
    //    {
    //        string[] pars = param.Split(' ');
    //        if (pars.Length == 3)
    //        {
    //            uint SiteID = uint.Parse(pars[0]);
    //            string ObjName = pars[1];
    //            ushort State = ushort.Parse(pars[2]);

    //            try
    //            {
    //                UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
    //                UniObject obj = sys.FindObject(ObjName) as UniObject;
    //                sys.DataTime = DateTime.Now;
    //                if (obj != null)
    //                {
    //                    if (!isDiag)
    //                    {
    //                        obj.SetObjState(State);
    //                        Console.WriteLine("*** object {0} to state {1}", ObjName, State);
    //                    }
    //                    else
    //                    {
    //                        try
    //                        {
    //                            DateTime time = obj._diagStateTOtimeMAPuni[State];
    //                            obj.DeactivateObjDiagState(State);
    //                            Console.WriteLine("*** object {0} deactive diag {1}", ObjName, State);
    //                        }
    //                        catch (KeyNotFoundException)
    //                        {
    //                            obj.ActivateObjDiagState(State);
    //                            Console.WriteLine("*** object {0} active diag {1}", ObjName, State);
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    Console.WriteLine("*** object {0} not found!", ObjName);
    //                }
    //            }
    //            catch (KeyNotFoundException)
    //            {
    //                Console.WriteLine("*** site {0} not found!", SiteID);
    //            }
    //        }
    //        else
    //            Console.WriteLine("*** use format: SiteID ObjName State");
    //        return true;
    //    }

    //    protected bool OnObjectDataCommand(string param)
    //    {
    //        string[] pars = param.Split(' ');
    //        if (pars.Length == 3)
    //        {
    //            uint SiteID = uint.Parse(pars[0]);
    //            string ObjDataName = pars[1];
    //            uint State = 0;
    //            if (pars[2] == "M" || pars[2] == "М")
    //                State = 2;
    //            else
    //                State = uint.Parse(pars[2]);

    //            try
    //            {
    //                UniObjSystem sys = _idobjsysTOrefMAP[SiteID];
    //                Tdm.ObjData objdata = sys.FindObject(ObjDataName) as Tdm.ObjData;
    //                if (objdata != null)
    //                {
    //                    Console.WriteLine("*** objdata {0} signals to state {1}", ObjDataName, State);

    //                    if (objdata.IsIOSignal)
    //                    {
    //                        objdata.IOSignal.StateBitCount = 2;
    //                        objdata.IOSignal.State = State;
    //                        objdata.IOSignal.GetOwnerModule().GeneralState.Current = IOGeneralState.Normal;
    //                        //objdata.IOSignal.GetOwnerModule().__DEBUG_setdatachanged();
    //                        Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", objdata.IOSignal.Owner.Owner.Name, objdata.IOSignal.Owner.Name, objdata.IOSignal.Name, State);
    //                    }
    //                    else if (objdata.IsSigDiscretLogic)
    //                    {

    //                        foreach (IOSignal sig in objdata.SigDiscreteLogic.IOSignals)
    //                        {
    //                            sig.StateBitCount = 2;
    //                            sig.State = State;
    //                            sig.GetOwnerModule().GeneralState.Current = IOGeneralState.Normal;
    //                            //sig.GetOwnerModule().__DEBUG_setdatachanged();
    //                            Console.WriteLine("*** signal {0}/{1}/{2} to state {3}", sig.Owner.Owner.Name, sig.Owner.Name, sig.Name, State);
    //                        }
    //                    }
    //                    else
    //                        Console.WriteLine("*** unknown type of object data {0}", ObjDataName);
    //                }
    //                else
    //                {
    //                    Console.WriteLine("*** object data {0} not found!", ObjDataName);
    //                }
    //            }
    //            catch (KeyNotFoundException)
    //            {
    //                Console.WriteLine("*** site {0} not found!", SiteID);
    //            }
    //        }
    //        else
    //            Console.WriteLine("*** use format: SiteID ObjName State");
    //        return true;
    //    }

    //    protected bool OnGuardAll()
    //    {
    //        foreach (UniObjSystem objsys in _idobjsysTOrefMAP.Values)
    //        {
    //            _concentrator.GuardElement(objsys);
    //        }

    //        foreach (BaseIOSystem iosys in _idiosysTOrefMAP.Values)
    //        {
    //            _concentrator.GuardElement(iosys);
    //        }
    //        Console.WriteLine("*** выключаем потоки получения данных");
    //        _concentrator.StopAllClients();

    //        return true;
    //    }

    //    protected bool OnGuard(string param)
    //    {
    //        IOBaseElement elem = FindObject(param) as IOBaseElement;
    //        if (elem != null)
    //            _concentrator.GuardElement(elem);
    //        else
    //            Trace.WriteLine("элемент не найден!");

    //        return true;
    //    }

    //    protected bool OnDelay(string param)
    //    {
    //        int MSec = int.Parse(param);
    //        Console.WriteLine("*** пауза {0} миллисекунд...", MSec);
    //        Thread.Sleep(MSec);
    //        return true;
    //    }

    //    protected bool OnUseFile(string param)
    //    {
    //        try
    //        {
    //            using (StreamReader sr = new StreamReader(param))
    //            {
    //                Console.WriteLine("***************************************************************");
    //                Console.WriteLine("*** использование комманд из файла " + param);
    //                Console.WriteLine("***************************************************************");

    //                String line;
    //                while ((line = sr.ReadLine()) != null)
    //                {
    //                    if (line.Length > 0 && !line.StartsWith("//"))
    //                    {
    //                        int pos = line.IndexOf(' ');
    //                        if (pos > 0)
    //                            OnCommand(line.Substring(0, pos), line.Substring(pos + 1));
    //                        else
    //                            OnCommand(line, string.Empty);
    //                    }
    //                }

    //                Console.WriteLine("***************************************************************");
    //                Console.WriteLine("*** окончание обработки комманд из файла " + param);
    //                Console.WriteLine("***************************************************************");
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            // Let the user know what went wrong.
    //            Console.WriteLine();
    //            Console.WriteLine("*** Error to process file:");
    //            Console.WriteLine(e.Message);
    //        }
    //        return true;
    //    }
    //    /// <summary>
    //    /// ссылка на коллекцию контролируемых станций
    //    /// </summary>
    //    protected DlxCollection _siteList = null;
    //    protected Dictionary<uint, UniObjSystem> _idobjsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
    //    protected Dictionary<uint, BaseIOSystem> _idiosysTOrefMAP = new Dictionary<uint, BaseIOSystem>();
    //    protected IDebugConcentrator _concentrator = null;
    //}


}
