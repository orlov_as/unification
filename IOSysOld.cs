﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using AfxEx;
using Ivk.IOSys;

namespace Tdm.DpsOld
{
    #region Элементы подсистемы ввода
    /// <summary>
    /// Заголовок данных по линии(БА) в формате текущего состояния DpsSt (23 байта)
    /// </summary>
    class DSigLineData
    {
        public UInt16 m_Id;
        public UInt16 m_Length; 
        public byte m_LineNumber;
        public UInt32 m_Time;
        public UInt16 m_Ms;
        public UInt16 m_Count;

        public UInt16 m_CheckSumm;
        public UInt16 m_ExData;
        public UInt16 m_Overrun;
        public UInt16 m_Frame;
        public UInt16 m_LineState;

        /// <summary>
        /// Обработка заголовка данных по линии(БА) формата текущего состояния DpsSt
        /// </summary>
        /// <param name="reader">читатель</param>
        /// <exception cref="System.ArgumentException">Выбрасывается при прочтении некорректного маркера начала</exception>
        public void Read( BinaryReader reader )
        {
            m_Id = reader.ReadUInt16();
            if( m_Id != 0x55AA )
                throw new ArgumentException( "Line data head marker is not 0x55AA!", m_Id.ToString() );
            m_Length = reader.ReadUInt16();
            m_LineNumber = reader.ReadByte();
            m_Time = reader.ReadUInt32();
            m_Ms = reader.ReadUInt16();
            m_Count = reader.ReadUInt16();
            m_CheckSumm =reader.ReadUInt16();
            m_ExData = reader.ReadUInt16();
            m_Overrun = reader.ReadUInt16();
            m_Frame = reader.ReadUInt16();
            m_LineState = reader.ReadUInt16();
        }
        
        //BYTE	m_Mask[nModuls];
        //BYTE	m_ModeMask[nModuls * 2];
        //BYTE	m_Data[];
    }

    /// <summary>
    /// Класс подсистемы ввода, обрабатывающий данные в формате DpsSt
    /// </summary>
    class IOSysOld: IOSystem
    {
        /// <summary>
        /// Создание объекта
        /// </summary>
        /// <returns>true - в случае успеха</returns>
        public override void Create()
        {
            base.Create();
            
            // проверяем, что все линии типа IOLineOld
            foreach(IOBaseElement line in SubElements.Objects )
                if (!(line is IOLineOld))
					throw new ArgumentException("Все подмодули 1го уровня подсистемы ввода должны быть типа IOLineOld!",line.ToString());

			GeneralState.Current = IOGeneralState.Unknown;
			SetSubsToUnknownState();
			IsValidData = false;
        }

        /// <summary>
        /// Обработка данных подсистемы, формата текущего состояния DpsSt
        /// </summary>
        /// <param name="reader">читатель</param>
        /// <exception cref="System.Exception">Выбрасывается при прочтении некорректных данных</exception>
        public override void ProcessData(BinaryReader reader)
        {
            // Первый байт - количество линий
            byte nLinesCount = reader.ReadByte();

            // Если полученное количество линий не совпадает с ожидаемым
            // *Убрана проверка количества линий из-за спецлиний, которых нет в ИО GroupsStates, VirtualOutput... 
            /*if( nLinesCount != SubElements.Objects.Count )
                throw new ArgumentException("Полученное количество линий не совпадает с ожидаемым для станции " + Owner.Name, nLinesCount.ToString());*/

            //Каждая линия обрабатывает свои данные
            foreach (IOBaseElement Line in SubElements.Objects)
            {
                // Разбор данных по линии
                Line.ProcessData(reader);
            }

            // Пытаемся читать данные дополнительных линий, которых нет в конфигурации (GroupsStates и тп)
            ReadExtLinesData(reader);

			IsValidData = false;
        }

        /// <summary>
        /// Прочитать данные всех дополнительных линий (которых нет в конфигурации)
        /// </summary>
        /// <param name="reader"></param>
        private void ReadExtLinesData(BinaryReader reader)
        {
            DSigLineData header = new DSigLineData();
            long pos = 0;
            try
            {
                do
                {
                    // Запомнили позицию на случай исключения
                    pos = reader.BaseStream.Position;
                    // Читаем заголовок
                    header.Read(reader);
                    // Заголовок корректный
                    if (header.m_Id == 0x55AA)
                        // Читаем данные линиии
                        reader.ReadBytes(header.m_Length);
                    else
                    {
                        // возвращаемся на позицию
                        reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                        // выход из цикла
                        break;
                    }
                }
                while (pos > 0);
            }
            catch(Exception ex)
            {
                // возвращаемся на позицию
                reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Устанвоить отказное состояние
        /// </summary>
        public override void SetInvalidData()
        {
            IsValidData = false;
            GeneralState.Current = IOGeneralState.Unknown;
            SetSubsToUnknownState();
            RaiseDataChangedEvent(IOSystem.DataTime, true);
        }
    }

    /// <summary>
    /// Класс линии(БА), разбирающий данные в формате принятом в DpsSt
    /// </summary>
	class IOLineOld : IOSimpleCBS
    {
        /// <summary>
        /// Создание объекта линии(БА). Расчет размера данных, занимаемых диагностическими масками.
        /// </summary>
        /// <returns>true - в случае успеха</returns>
        public override void Create()
        {
            try
            {
                base.Create();
                // расчитываем размер маски
			    if (SubElements == null)
				    MaskSize = 1;
			    else
				    MaskSize = ((SubElements.Objects.Count + 1) / 8) + (((SubElements.Objects.Count + 1) % 8)!=0 ? 1 : 0);
            }
            catch (Exception ex )
            {
                throw new Exception(Name + " IOLineOld.Create: " + ex.Message);
            }
        }

        /// <summary>
        /// Размера данных, занимаемых диагностическими масками.
        /// </summary>
        int MaskSize;

        /// <summary>
        /// Обработка данных линии(БА), формата текущего состояния DpsSt
        /// </summary>
        /// <param name="reader">читатель</param>
        public override void ProcessData( BinaryReader reader)
        {
            try {
                long firstpos = reader.BaseStream.Position;
                // читаем заголовок данных по линии
                DSigLineData header = new DSigLineData();
                header.Read(reader);

                // читаем диагностические маски 
                BitArray mask1 = new BitArray(reader.ReadBytes(MaskSize));
                BitArray mask2 = new BitArray(reader.ReadBytes(MaskSize * 2));

                // Проверяем, что линия работает (старший бит маски)
                if (mask1[MaskSize * 8 - 1] == false)
                {// линия в отказе
                    GeneralState.Current = IOGeneralState.Refusal;
                    SetSubsToUnknownState();
                    //Читаем данные модулей (неправильные) и ничего не делаем.
                    int dataSize = 0;
                    if (SubElements != null)
                    {
                        foreach (IOModule module in SubElements.Objects)
                        {
                            dataSize += module.Size;
                            if (module is IOMav10WithUgr)
                                dataSize += (module as IOMav10WithUgr).UgrsDataSize;
                        }
                    }
                    reader.BaseStream.Seek(dataSize, SeekOrigin.Current);
                }
                else
                {// линия работает
                    int oldState = 0;
                    // проверка младшего (из двух) бита состояния линии
                    if (mask2[MaskSize * 8 * 2 - 2])
                        oldState |= 1;
                    // проверка старшего (из двух) бита состояния линии
                    if (mask2[MaskSize * 8 * 2 - 1])
                        oldState |= 2;
                    //преобразем состояние из старого в новое
                    GeneralState.Current = Convert2GS(OldStateToNewQNX(oldState));

                    // если данным можно доверять, раздаем данные модулям
                    int maskIndex = 0;
                    if (SubElements != null)
                    {
                        foreach (IOModule module in SubElements.Objects)
                        {
                            //Если по первой маске модуль работает - обновляем данные
                            if (mask1[maskIndex])
                            {
                                // состояние модуля по второй маске
                                oldState = 0;
                                if (mask2[maskIndex * 2])
                                    oldState |= 1;
                                if (mask2[maskIndex * 2 + 1])
                                    oldState |= 2;

                                // Преобразуем состояние из старого в новое
                                module.GeneralState.Current = Convert2GS(OldStateToNewQNX(oldState));
                            }
                            else
                                // Модуль не работает
                                module.GeneralState.Current = IOGeneralState.Refusal;

                            // Разбираем данные
                            module.SetRawData(module.Size, reader);

                            //Увеличиваем индекс модуля для получения диагностической маски
                            maskIndex++;
                        }
                    }
                }

                long endpos = reader.BaseStream.Position;

                if (endpos - firstpos != header.m_Length)
                    throw new OverflowException(string.Format("Ошибка размера данных по линии {0}: ожидалось {1} байт, пришло {2}!", Name, endpos - firstpos, header.m_Length));
            }
            catch(Exception)
            {
                Console.WriteLine(Name + ": Ошибка обработки данных!");
                throw;
            }
        }

        /// <summary>
        /// Функция преобразования значения состояния старой системы в значения новой для QNX
        /// </summary>
        /// <param name="state">Старое состояние</param>
        /// <returns>Новое состояние QNX</returns>
        public int OldStateToNewQNX( int state )
        {
	        switch( state )
	        {
                //Unknown
		        case 0: return 0;
                //Normal
		        case 1: return 3;
                //Unstable
		        case 2: return 1;
                //refusal
		        case 3: return 2;
		        default: return 0;
	        }
        }
    }

    /// <summary>
    /// Класс модуля с сигналами, который обрабатывает сырые данные
    /// </summary>
    public class IOSigModuleRaw: IOSigModule
    {
        /// <summary>
        /// Загрузка модуля с сигналами. Для работы с сырыми данными загружаются интерфейсы получения и проверки значения
        /// </summary>
        /// <param name="Loader">Загрузщик</param>
        override public void Load(DlxLoader Loader)
        {
	        base.Load(Loader);

            // получаем интерфейсы
	        string Str;
	        if( Loader.GetAttribute("IGetValue", out Str) )
	        {
		        _IGetValue = Loader.GetObjectFromAttribute("IGetValue") as IIOGetValue;
		        if( _IGetValue == null )
		        {
			        Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + ": error to get IGetValue object!\n");
			        throw new ArgumentException("CIOSigModule::Load module " + Name + " - error to get IGetValue object '" + Str + "'" );
		        }
	        }
	        else
	        {
				Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + ": no attribute IGetValue!\n");
		        throw new ArgumentException("CIOSigModule::Load module " + Name + " - no attribute IGetValue");
	        }

	        if( Loader.GetAttribute("ICheckCode", out Str) )
	        {
		        _ICheckCode = Loader.GetObjectFromAttribute("ICheckCode") as IIOCheckCode;
		        if( _ICheckCode == null )
		        {
					Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + ": error to get ICheckCode object!\n");
			        throw new ArgumentException("CIOSigModule::Load module " + Name + " - error to get ICheckCode object '" + Str + "'" );
		        }
	        }
	        else
	        {
				Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + ": no attribute ICheckCode!\n");
		        throw new ArgumentException("CIOSigModule::Load module " + Name + " - no attribute ICheckCode");
	        }
        }

        /// <summary>
        /// Создание модуля. Для всех сигналов формируем адреса их данных в буфере данных модуля
        /// </summary>
        /// <returns>true - создание прошло успешно; false - ошибка</returns>
        override public void Create()
        {
            base.Create();

            foreach( IOSignal1Ch signal in Signals.Objects )
            {
            	uint startByte = 0, startBit = 0, bitscount = 0;
				if (_IGetValue.GetChannelInfo(signal.Channel.Number, ref startByte, ref startBit, ref bitscount))
				{
					if (bitscount == 0)
						Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + " WRANING: Количество бит для канала " + Name + "/" + signal.Name + " = 0!!!");

					if( bitscount == 1)
						signal.StateBitCount = 2;
					else
						signal.StateBitCount = bitscount;
				}

            	// формируем адрес данных канала в массиве данных модуля
                if( signal is IODiscreteSignalRaw )
                    (signal as IODiscreteSignalRaw).Addr = _IGetValue.MakeAddr(signal.Channel.Number);
				else if (signal is IOAnalogSignalRaw)
                    (signal as IOAnalogSignalRaw).Addr = _IGetValue.MakeAddr(signal.Channel.Number);
				else// проверяем, что сигнал типа IOSignalRaw
					throw new ArgumentException("Signal '" + Name + "/" + signal.Name + "' is not IOSignalRaw.");
            }
        }

        /// <summary>
        /// Установка сырых данных. Данные разбираются и устанавливаются до каналов
        /// </summary>
        /// <param name="size">Размер сырых данных для модуля</param>
        /// <param name="reader">Поток чтения</param>
        /// <returns>Возвращает ИСТИНА, если установка данных прошла без ошибок</returns>
        public override bool SetRawData(int size, BinaryReader reader)
        {
			// копируем данные в буфер
            if( !base.SetRawData( size, reader ) )
                return false;

            // разбираем данные, обновляя значения каналов
            UpdateRaw();

        	IsDataSet = true;
            return true;
        }

        /// <summary>
        /// Разобрать сырые данные (RawData) и установить значения для всех каналов модуля
        /// </summary>
        protected void UpdateRaw()
        {
            // для всех каналов формируем адрес в массиве и получаем значение
            foreach( IOSignal1Ch signal in Signals.Objects )
            {
				if (GeneralState.Current == IOGeneralState.Refusal || GeneralState.Current == IOGeneralState.Unknown)
				{
					signal.State = 0xFFFFFFFF;
					if (signal.IsChangedState)
						IsDataChanged = true;
					continue;
				}
				if (signal is IODiscreteSignalRaw)
				{
					IODiscreteSignalRaw dsig = signal as IODiscreteSignalRaw;
					// получаем значение канала, используя адрес, созданный в Create
					UInt32 value = _IGetValue.GetValue(RawData, dsig.Addr);
					// проверяем корректность значения( отказ или значение )
					if (_ICheckCode.CheckCode(value, dsig.Addr))
						dsig.State = value;
					else
						dsig.State = 0xFFFFFFFF;
				}
				else if (signal is IOAnalogSignalRaw)
				{
					IOAnalogSignalRaw asig = signal as IOAnalogSignalRaw;
					// получаем значение канала, используя адрес, созданный в Create
					UInt32 value = _IGetValue.GetValue(RawData, asig.Addr);
					// проверяем корректность значения( отказ или значение )
					if (!_ICheckCode.CheckCode(value, asig.Addr))
						asig.State = 0xFFFFFFFF;
					else if (asig.ClbParams != null)
					{
						double dvalue = _IGetValue.GetValueDiap(RawData, asig.Addr);

						IOClbParamsDirectRaw clbParams = asig.ClbParams as IOClbParamsDirectRaw;
						if (clbParams != null)
							asig.State = clbParams.Evaluate(dvalue);
						else
							throw new ArgumentException(
								"Калибровочные параметры должны буть типа IOClbParamsDirectRaw или IOClbParamsNegRaw",
								asig.ClbParams.ToString());
					}
					else
						asig.State = value;
				}
				else
					Debug.WriteLine("CIOSigModule " + Owner.Name + "/" + Name + " WARNING: Сигнал " + Owner.Name + "/" + signal.Name + "не обрабатывает сырые данные!!!!");

				if (signal.IsChangedState)
				{
					//Debug.WriteLineIf(signal.IsChangedState, "SIGNAL " + signal.Owner.Name + "/" + signal.Name + " CHANGED TO " + signal.State2String(signal.State));
					IsDataChanged = true;
				}
            }
        }

        /// <summary>
        /// ссылка на интерфейс проверки значения в канале
        /// </summary>
        protected IIOCheckCode _ICheckCode;
        /// <summary>
        /// ссылка на интерфейс получения значения в канале
        /// </summary>
	    protected IIOGetValue _IGetValue;

        /// <summary>
        /// Устанавливает для вложенных элементов неизвестное состояние
        /// </summary>
        public override void SetSubsToUnknownState()
        {
            base.SetSubsToUnknownState();
            
            // для всех каналов формируем состояние отказа
            foreach (IOSignal signal in Signals.Objects)
            {
                signal.State = 0xffffffff;
            }
        }
    }

    #region Аналоговые модули, сигналы
    /// <summary>
    /// Класс Мав с каналами 10 бит и угр
    /// </summary>
    public class IOMav10WithUgr : IOSigModuleRaw
    {
		/// <summary>
		/// Создание модуля c угр. Формируем размер данных для угр
		/// </summary>
		/// <returns>true - создание прошло успешно; false - ошибка</returns>
		public override void Create()
		{
            base.Create();

            SetUgrParams();
		}
        /// <summary>
        /// Параметры УГР
        /// </summary>
        public virtual void SetUgrParams()
        {
            if (SubElements != null)
            {
                _UgrsDataSize = SubElements.Objects.Count + 1;
                _oldUgrsData = new byte[_UgrsDataSize];
            }
            else
                _UgrsDataSize = 0;
        }
        /// <summary>
        /// Установка сырых данных. Данные разбираются и устанавливаются до каналов
        /// </summary>
        /// <param name="size">Размер сырых данных для модуля</param>
        /// <param name="reader">Поток чтения</param>
        /// <returns>Возвращает ИСТИНА, если установка данных прошла без ошибок</returns>
        public override bool SetRawData(int size, BinaryReader reader)
        {
            // копируем данные в буфер
            if (!base.SetRawData(size, reader))
                return false;

            if (SubElements != null)
            {
                // читаем данные УГРов
                if (_ugrsData != null)
                    _ugrsData.CopyTo(_oldUgrsData, 0);
                _ugrsData = reader.ReadBytes(UgrsDataSize);
                // сравниваем данные с предидущими
                for (int i = 0; i < UgrsDataSize; i++)
                    if (_oldUgrsData[i] != _ugrsData[i])
                    {
                        IsDataChanged = true;
                        break;
                    }

                // обрабатываем
                foreach (IOBaseElement elem in SubElements.Objects)
                {
                    int number = elem.Number;
                    if (UgrsDataSize > number + 1)
                    {
                        byte[] ugrData = {_ugrsData[1 + number], (byte) ((_ugrsData[0] >> (number << 1)) & 3)};
                        ugrData[0] = _ugrsData[1 + number];

                        // устанавливаем обобщенное соостняие подмодуля таким же как и у этого
                        elem.GeneralState.Current = GeneralState.Current;

                        // устанавливаем данные
                        BinaryReader ugrReader = new BinaryReader(new MemoryStream(ugrData));
                        elem.SetRawData(2, ugrReader);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException(size.ToString(),
                                                              "IOMav10WithUgr::SetSubData - in " + Name +
                                                              " module no data for sub module " + elem.Name + "!");
                    }
                }
            }
            return true;
        }

		/// <summary>
		/// Получение объекта среди дочерних и внучатых по имени.
		/// </summary>
		/// <param name="pName">Имя объекта</param>
		/// <returns>ССылка на объект либо null, если объект не найден</returns>
		public override DlxObject GetObject(string pName)
		{
			// поиск объекта среди дочерних
			DlxObject obj = base.GetObject(pName);
			if(obj != null)
				return obj;

			// поиск среди внучатых
			foreach (DlxObject el in SubElements.Objects)
			{
				obj = el.GetObject(pName);
				if( obj != null )
					return obj;
			}

			return null;
		}

    	/// <summary>
    	/// Данные УГР
    	/// </summary>
    	protected byte[] _ugrsData;

    	/// <summary>
    	/// Данные УГР, полученные в пред пакете
    	/// </summary>
        protected byte[] _oldUgrsData;

		/// <summary>
		/// Размер данных УГР. Формируется в Create
		/// </summary>
    	protected int _UgrsDataSize;
		/// <summary>
		/// Свойство получения размера данных УГР
		/// </summary>
    	public int UgrsDataSize
    	{
			get { return _UgrsDataSize; }
    	}
    }
    /// <summary>
    /// Класс Мав с каналами 10 бит и угр
    /// </summary>
    public class IOMav10WithUgrTK : IOMav10WithUgr
    {
        /// <summary>
        /// Параметры УГР
        /// </summary>
        public override void SetUgrParams()
        {
            if (SubElements != null)
            {
                _UgrsDataSize = SubElements.Objects.Count * 2 + 1;
                _oldUgrsData = new byte[_UgrsDataSize];
            }
            else
                _UgrsDataSize = 0;
        }
    }
    /// <summary>
    /// Класс Мав с каналами 10 бит и угр
    /// </summary>
    public class IOMavUgrHub10bit : IOSigModuleRaw
    {
        /// <summary>
        /// Установка сырых данных. Данные разбираются и устанавливаются до каналов
        /// </summary>
        /// <param name="size">Размер сырых данных для модуля</param>
        /// <param name="reader">Поток чтения</param>
        /// <returns>Возвращает ИСТИНА, если установка данных прошла без ошибок</returns>
        public override bool SetRawData(int size, BinaryReader reader)
        {
            // копируем данные в буфер
            if (!base.SetRawData(size, reader))
                return false;

            if (SubElements != null)
            {
                // читаем данные УГРов
                if (_ugrsData != null)
                    _ugrsData.CopyTo(_oldUgrsData, 0);
                _ugrsData = reader.ReadBytes(UgrsDataSize);
                // сравниваем данные с предидущими
                for (int i = 0; i < UgrsDataSize; i++)
                    if (_oldUgrsData[i] != _ugrsData[i])
                    {
                        IsDataChanged = true;
                        break;
                    }

                // обрабатываем
                foreach (IOBaseElement elem in SubElements.Objects)
                {
                    int number = elem.Number;
                    if (number < 8)
                    {
                        byte[] ugrData = { _ugrsData[number], (byte)((_ugrsData[8+number/4] >> ((number%4) << 1)) & 3) };

                        // устанавливаем обобщенное соостняие подмодуля таким же как и у этого
                        elem.GeneralState.Current = GeneralState.Current;

                        // устанавливаем данные
                        BinaryReader ugrReader = new BinaryReader(new MemoryStream(ugrData));
                        elem.SetRawData(2, ugrReader);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException(size.ToString(),
                                                              "IOMavUgrHub10bit::SetSubData - in " + Name +
                                                              " module no data for sub module " + elem.Name + "!");
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Получение объекта среди дочерних и внучатых по имени.
        /// </summary>
        /// <param name="pName">Имя объекта</param>
        /// <returns>ССылка на объект либо null, если объект не найден</returns>
        public override DlxObject GetObject(string pName)
        {
            // поиск объекта среди дочерних
            DlxObject obj = base.GetObject(pName);
            if (obj != null)
                return obj;

            // поиск среди внучатых
            foreach (DlxObject el in SubElements.Objects)
            {
                obj = el.GetObject(pName);
                if (obj != null)
                    return obj;
            }

            return null;
        }

        /// <summary>
        /// Данные УГР
        /// </summary>
        protected byte[] _ugrsData = new byte[10];

        /// <summary>
        /// Данные УГР, полученные в пред пакете
        /// </summary>
        private byte[] _oldUgrsData = new byte[10];

        public const int UgrsDataSize = 10;
    }

	/// <summary>
	/// Класс Мав с каналами 8 бит и угр
	/// </summary>
	public class IOMav8WithUgr : IOSigModuleRaw
	{
		/// <summary>
		/// Создание модуля c угр. Формируем размер данных для угр
		/// </summary>
		/// <returns>true - создание прошло успешно; false - ошибка</returns>
		public override void Create()
		{
            base.Create();

			int nUgrs = 0;
			if (SubElements != null)
			{
				nUgrs = SubElements.Objects.Count;
				_UgrsDataSize = nUgrs;
				_oldUgrsData = new byte[_UgrsDataSize];
			}
			else
				_UgrsDataSize = 0;
		}

		/// <summary>
		/// Установка сырых данных. Данные разбираются и устанавливаются до каналов
		/// </summary>
		/// <param name="size">Размер сырых данных для модуля</param>
		/// <param name="reader">Поток чтения</param>
		/// <returns>Возвращает ИСТИНА, если установка данных прошла без ошибок</returns>
		public override bool SetRawData(int size, BinaryReader reader)
		{
			// копируем данные в буфер
			if (!base.SetRawData(size, reader))
				return false;

			if (SubElements != null && UgrsDataSize != 0)
			{
				// читаем данные УГРов
				if (_ugrsData != null)
					_ugrsData.CopyTo(_oldUgrsData, 0);
				_ugrsData = reader.ReadBytes(UgrsDataSize);
				// сравниваем данные с предидущими
				for (int i = 0; i < UgrsDataSize; i++)
					if (_oldUgrsData[i] != _ugrsData[i])
					{
						IsDataChanged = true;
						break;
					}

				// обрабатываем
				foreach (IOBaseElement elem in SubElements.Objects)
				{
					int number = elem.Number;
					if (UgrsDataSize > number)
					{
						byte[] ugrData = new byte[1];
						ugrData[0] = _ugrsData[number];

						// устанавливаем обобщенное соостняие подмодуля таким же как и у этого
						elem.GeneralState.Current = GeneralState.Current;

						// устанавливаем данные
						BinaryReader ugrReader = new BinaryReader(new MemoryStream(ugrData));
						elem.SetRawData(1, ugrReader);
					}
					else
					{
						throw new ArgumentOutOfRangeException(size.ToString(),
															  "IOMav8WithUgr::SetSubData - in " + Name +
															  " module no data for sub module " + elem.Name + "!");
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Получение объекта среди дочерних и внучатых по имени.
		/// </summary>
		/// <param name="pName">Имя объекта</param>
		/// <returns>ССылка на объект либо null, если объект не найден</returns>
		public override DlxObject GetObject(string pName)
		{
			// поиск объекта среди дочерних
			DlxObject obj = base.GetObject(pName);
			if (obj != null)
				return obj;

			// поиск среди внучатых
			foreach (DlxObject el in SubElements.Objects)
			{
				obj = el.GetObject(pName);
				if (obj != null)
					return obj;
			}

			return null;
		}

		/// <summary>
		/// Данные УГР
		/// </summary>
		protected byte[] _ugrsData;

		/// <summary>
		/// Данные УГР, полученные в пред пакете
		/// </summary>
		private byte[] _oldUgrsData;

		/// <summary>
		/// Размер данных УГР. Формируется в Create
		/// </summary>
		protected int _UgrsDataSize;
		/// <summary>
		/// Свойство получения размера данных УГР
		/// </summary>
		public int UgrsDataSize
		{
			get { return _UgrsDataSize; }
		}
	}

    /// <summary>
    /// Класс аналог. сигнала, который обрабатывает сырые данные
    /// </summary>
    public class IOAnalogSignalRaw: IOAnalogSignal
    {
        /// <summary>
        /// Загрузка объекта. Загружает базовые параметра + признак необходимости
        /// поправочных коэффициентов
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            NeedClbParams = Loader.GetAttributeBool("NeedClbParams", false);
        }
        /// <summary>
        /// Создание объекта
        /// </summary>
        /// <returns>true - создание прошло успешно; false - ошибка</returns>
        override public void Create()
        {
            base.Create();
        	StateBitCount = 32;

            // проверяем, что были загружены поправочные коэффициенты
            if (ClbParams == null)
            {
                // Получение калибровочных данных
                string ChannelPath;
                IOSystem iosys = GetIOSystem(out ChannelPath);
                if (ChannelPath.Contains(","))
                {
                    if ((iosys != null) && (iosys.Calibration != null))
                        ClbParams = iosys.Calibration.FindObject(ChannelPath.Substring(0, ChannelPath.LastIndexOf(','))) as IOClbParams;
                }
            }

            // Если не удалось загрузить коэффициенты и стоит признак необходимости их загрузки
            if (ClbParams == null && NeedClbParams == true)
            {
                throw new ArgumentException((Owner as IOModule).IOSystem.Owner.Name + ": " + Owner.Name + "/" + Name + " IOAnalogSignalRaw: can't get reference to calibration params!");
            }
        }

        /// <summary>
        /// Получить состояние сигнала
        /// </summary>
        public override uint State
        {
            get
            {
                if (IsErrorState)
                    throw new ArgumentOutOfRangeException(string.Format("{0}/{1}/State", Owner.Name, Name));

                return _state.Current;
            }
            set
            {
                _state.Current = value;
            }
        }

        /// <summary>
        /// Адрес данных этого канала в масиве данных модуля
        /// </summary>
        protected Addr _Addr;
        /// <summary>
        /// Адрес данных этого канала в масиве данных модуля
        /// </summary>
        public Addr Addr
        {
            get {return _Addr;}
            set 
            {
                _Addr = value;
            }
        }
        /// <summary>
        /// Признак необходимости загрузки поправочных коэффициентов.
        /// В случае установки в true прерывает запуск при отсутствии коэффициентов
        /// </summary>
        protected bool NeedClbParams = false;
        /// <summary>
        /// свойство получения признака необходимости поправочных коэффциентов
        /// </summary>
        public bool IsNeedClbParams { get { return NeedClbParams; } }
    }

    #region Поправочные коэффициенты
    /// <summary>
    /// Класс калибровочных параметров одного канала
    /// Калибровочный коэффициент: Ax+B
    /// </summary>
    public class IOClbParamsDirectRaw : IOClbParamsDirect
    {
        /// <summary>
        /// Расчитать значение канала Ax+B
        /// </summary>
        /// <param name="ChannelValue">Значение в канале (сырые данные)</param>
        /// <returns>Расчитанное по коэффициентам значение</returns>
        public virtual UInt32 Evaluate(double ChannelValue)
        {
            ChannelValue = m_AParam * ChannelValue + m_BParam;
            if (ChannelValue < 0)
                return 0;

            return (UInt32)(ChannelValue * 1000.0 + 0.5);
        }
    }

    /// <summary>
    /// Класс калибровочных параметров одного канала
    /// Калибровочный коэффициент: A/x+B
    /// </summary>
    public class IOClbParamsNegRaw : IOClbParamsDirectRaw
    {
        /// <summary>
        /// Расчитать значение канала A/x+B
        /// </summary>
        /// <param name="ChannelValue">Значение в канале (сырые данные)</param>
        /// <returns>Расчитанное по коэффициентам значение</returns>
        public override UInt32 Evaluate(double ChannelValue)
        {
            ChannelValue = m_AParam / ChannelValue + m_BParam;
            if (ChannelValue < 0)
                return 0;

            return (UInt32)(ChannelValue * 1000.0 + 0.5);
        }
    }
    #endregion
    #endregion

    #region Дискретные сигналы
    /// <summary>
    /// Класс дискр. сигнала, который обрабатывает сырые данные
    /// </summary>
    public class IODiscreteSignalRaw : IODiscreteSignal
    {
        /// <summary>
        /// Адрес данных этого канала в масиве данных модуля
        /// </summary>
        protected Addr _Addr;
        /// <summary>
        /// Адрес данных этого канала в масиве данных модуля
        /// </summary>
        public Addr Addr
        {
            get {return _Addr;}
            set 
            {
                _Addr = value;
            }
        }

        /// <summary>
        /// Получить состояние сигнала
        /// </summary>
        public override uint State
        {
            get
            {
                if (IsErrorState)
                    throw new ArgumentOutOfRangeException(string.Format("{0}/{1}/State", Owner.Name, Name));
                        
                return _state.Current;
            }
            set
            {
                _state.Current = value;
            }
        }
    }


    public class IODiscreteSignalStub : IODiscreteSignal
    {
        public override void Create()
        {
            GetOwnerModule().GeneralState.Current = IOGeneralState.Normal;
            StateBitCount = 2;
            State = 3;
            base.Create();
        }

        public override string State2String(uint state)
        {
            return "Не передается";
        }
    }

    #endregion

    #region Параметризованные каналы
    /// <summary>
    /// Класс модуля с сигналами, который обрабатывает сырые данные
    /// </summary>
    public class IOParamSigModuleRaw : IOSigModule
    {
        protected int _channelCount;
        public override void Create()
        {
            base.Create();

            // Проверяем типы сигналов
            foreach (IOSignal signal in Signals.Objects)
            {
                if ((signal as IParamSigModuleSignal) == null)
                {
                    Debug.WriteLine("Signal " + signal.Name + " has wrong type " + signal.GetType() + " (need IParamSigModuleSignal)");
                }
            }
        }

        /// <summary>
        /// Загрузка параметров модуля
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _channelCount = Loader.GetAttributeInt("IMSIChannelsCount", 0);
        }

        public int ImsiChannelsCount
        {
            get { return _channelCount; }
        }

        /// <summary>
        /// Установка сырых данных. Данные разбираются и устанавливаются до каналов
        /// </summary>
        /// <param name="size">Размер сырых данных для модуля</param>
        /// <param name="reader">Поток чтения</param>
        /// <returns>Возвращает ИСТИНА, если установка данных прошла без ошибок</returns>
        public override bool SetRawData(int size, BinaryReader reader)
        {
            // копируем данные в буфер
            if (!base.SetRawData(size, reader))
                return false;

            // разбираем данные, обновляя значения каналов
            UpdateRaw();

            IsDataSet = true;
            return true;
        }

        /// <summary>
        /// Разобрать сырые данные (RawData) и установить значения для всех каналов модуля
        /// </summary>
        protected void UpdateRaw()
        {
            // для всех каналов формируем адрес в массиве и получаем значение
            //foreach (IOParamCodeSignal signal in Signals.Objects)
            foreach (IParamSigModuleSignal signal in Signals.Objects)
            {
                if (GeneralState.Current == IOGeneralState.Refusal || GeneralState.Current == IOGeneralState.Unknown)
                {
                    signal.State = 0xFFFFFFFF;
                    if (signal.IsChangedState)
                        IsDataChanged = true;
                    continue;
                }

                signal.UpdateState(RawData);

                if (signal.IsChangedState)
                {
                    IsDataChanged = true;
                }
            }
        }
    }

    public interface IParamSigModuleSignal
    {
        uint UpdateState(byte[] RawData);
        uint State { get; set; }
        bool IsChangedState { get; }
    }

    /// <summary>
    /// Класс сигнала, имеющий функциональность получения значения из блока данных модуля,
    /// а также возможность вычисления значений для виртуальных сигналов.
    /// </summary>
    public class IOParamCodeSignal : IODiscreteSignal, IParamSigModuleSignal
    {
        #region Сравнение значения сигнала
        /// <summary>
        /// Массив констант для сравнения со значением сигналов
        /// </summary>
        protected ArrayList _EqualValues = new ArrayList();
        /// <summary>
        /// Функция сравнения значения сигнала с константами массива _EqualValues. Возвращает 1 при успешном сравнении
        /// </summary>
        protected Func<int, uint> _EqualFunc;
        /// <summary>
        /// Сравненние на соответствие (значение сигнала совпадает с одной из констант массива) 
        /// </summary>
        /// <param name="value">Значение сигнала</param>
        /// <returns>1 при успешном сравнении</returns>
        protected uint Equal(int value)
        {
            foreach (int eqVal in _EqualValues)
                if (value == eqVal)
                    return 1;
            return 0;
        }
        /// <summary>
        /// Сравнение на битовое вхождение
        /// </summary>
        /// <param name="value">Значение сигнала</param>
        /// <returns>1 при успешном сравнении</returns>
        protected uint BitEqual(int value)
        {
            foreach (int eqVal in _EqualValues)
                if ((value & eqVal) > 0)
                    return 1;
            return 0;
        }
        /// <summary>
        /// Сравнение на несоответствие (значение сигнала не совпадает ни с одной константой массива)
        /// </summary>
        /// <param name="value">Значение сигнала</param>
        /// <returns>1 при успешном сравнении</returns>
        protected uint NotEqual(int value)
        {
            foreach (int eqVal in _EqualValues)
                if (value == eqVal)
                    return 0;
            return 1;
        }

        /// <summary>
        /// Вернуть значение сигнала без изменения
        /// </summary>
        /// <param name="value">Значение сигнала</param>
        /// <returns>Значение сигнала</returns>
        protected uint SelfValue(int value)
        {
            return (uint)value;
        }
        #endregion

        #region Параметры положения сигнала в блоке данных
        /// <summary>
        /// Количество бит сигнала
        /// </summary>
        protected int _BitsCount;
        /// <summary>
        /// Смещение битах относительно начала байта
        /// </summary>
        protected int _BitsOffset;
        /// <summary>
        /// Смещение в байтах относительно начала блока данных
        /// </summary>
        protected int _ByteOffset;
        /// <summary>
        /// Колиество байт, занимаемое значением сигнала (возможно неполных)
        /// </summary>
        protected int _BytesCount;
        #endregion

        /// <summary>
        /// Загрузка параметров сигнала
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            try
            {
                base.Load(Loader);

                _BitsCount = Loader.GetAttributeInt("BitsCount", 1);
                int bitsOffset = Loader.GetAttributeInt("BitsOffset", 0);
                _ByteOffset = (byte)(bitsOffset / 8);
                _BitsOffset = (byte)(bitsOffset % 8);// Смещение в байте
                _BytesCount = 1 + (_BitsOffset + _BitsCount - 1) / 8;
                // Загружаем сравниваемые константы
                string equals = Loader.GetAttributeString("EqValues", string.Empty);
                // Загружаем тип сравнения
                string eqtype = Loader.GetAttributeString("EqType", equals == string.Empty ? string.Empty : "EQ");
                // задаем значение по умолчанию
                if (equals == string.Empty)
                    equals = "1";
                // Выбираем функцию обработки значения
                _EqualFunc = SelfValue;
                if (eqtype == "EQ")
                    _EqualFunc = Equal;
                else if (eqtype == "BitEQ")
                    _EqualFunc = BitEqual;
                else if (eqtype == "NotEQ")
                    _EqualFunc = NotEqual;

                // Разбираем список значений для сравнения
                //int start = 0, pos = 0;
                //do
                //{
                //    pos = equals.IndexOf(';', start);
                //    if (pos == -1 && equals.Length-start > 0)
                //        pos = equals.Length;
                //    string eqval = equals.Substring(start, pos - start);
                //    _EqualValues.Add(Convert.ToInt32(eqval));
                //    start = pos + 1;
                //}
                //while (pos > 0);

                string[] ss = equals.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in ss)
                    _EqualValues.Add(Convert.ToInt32(s));
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Ошибка загрузки сигнала " + Owner.Name + "/" + Name + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Обновить состояние сигнала по новым данным модуля
        /// </summary>
        /// <param name="RawData">Данные модуля для обновления</param>
        public virtual uint UpdateState(byte[] RawData)
        {
            // Проверяем хватает ли данных
            if (_ByteOffset + _BytesCount > RawData.Length)
                throw new IndexOutOfRangeException("Не хватает данных для обработки сигнала " + Name);
            // Получаем блок данных с нашим сигналом
            int infSig = 0;
            for (int i = 0; i < _BytesCount; i++)
                infSig |= RawData[_ByteOffset + i] << i * 8;
            // выделяем значение сигнала
            int st = (int)((infSig >> _BitsOffset) & ~(UInt32.MaxValue << (int)_BitsCount));

            State = _EqualFunc(st);

            // Обрабатываем значение через выбранную функцию
            return _EqualFunc(st);
        }

        /// <summary>
        /// Получить состояние сигнала
        /// </summary>
        public override uint State
        {
            get
            {
                if (_BitsCount == 0 || IsErrorState)
                    throw new ArgumentOutOfRangeException("State");

                return _state.Current;
            }
            set
            {
                _state.Current = value;
            }
        }

        /// <summary>
        /// Возвращает или задает количество бюит на канал
        /// </summary>
        public override uint StateBitCount
        {
            get { return (uint)_BitsCount; }
            set { _BitsCount = (int)value; }
        }

        /// <summary>
        /// Признак отказа сигнала
        /// </summary>
        public override bool IsErrorState
        {
            get
            {
                return false;
            }
        }
    }

    public class IOParamAnalogSignal2Ch : IOAnalogSignal, IParamSigModuleSignal
    {
        protected string _Channel2Ref;
        protected IOChannelDef _Channel2;
        protected int m_Ch2Number;

        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _Channel2Ref = Loader.GetAttributeString("CHREF2", String.Empty);
        }

        public override void Create()
        {
            base.Create();
            // получаем ссылку на родительский модуль
            var ownerModule = Owner as IOModule;
            if (ownerModule != null)
            {
                _Channel2 = ownerModule.GetChannelDef(_Channel2Ref);
                if (_Channel2 == null)
                {
                    throw new ArgumentException("IOParamAnalogSignal2Ch: no reference to channel in signal " + Name);
                }
                m_Ch2Number = _Channel2.Number;
            }
            else
            {
                throw new ArgumentException("IOParamAnalogSignal2Ch: no owner module for " + Name);
            }
            _Channel2Ref = string.Empty;

            StateBitCount = 32;
        }

        public virtual uint UpdateState(byte[] RawData)
        {
            var ownerModule = Owner as IOParamSigModuleRaw;
            if (ownerModule != null)
            {
                int dimension = ownerModule.ImsiChannelsCount;
                int chNum = ChannelNumber - 1;
                int ch2Num = m_Ch2Number != 0 ? m_Ch2Number - 2 : dimension - 1;

                // версия с qnx
                //int position = dimension * chNum - FactSumm(chNum) + ch2Num;
                
                int position = dimension * chNum - ArithProgressionSumm(1, 1, chNum) + ch2Num;
                if (position * 4 + 13 < RawData.Length)
                {
                    var stream = new MemoryStream(RawData);
                    stream.Seek(position * 4 + 13, SeekOrigin.Begin);
                    var reader = new BinaryReader(stream);

                    float f = reader.ReadSingle();
                    State = float.IsNaN(f) ? 0xFFFFFFFF : (uint)f;
                    return 0;
                }
            }
            State = 0xFFFFFFFF;
            return 0;
        }

        int FactSumm(int value)
        {
            if (value > 1)
                return value + FactSumm(value - 1);
            return value;
        }
        int ArithProgressionSumm(int start, int diff, int n)
        {
            return (2 * start + (n - 1) * diff) * n / 2;
        }

    }
    #endregion
    #endregion
    //////////////////////////////////////////////////////////////////////////
    #region Интерфейсы получения и проверки значения
    /// <summary>
    /// Адрес данных сигнала
    /// </summary>
    public class Addr
    {
		/// <summary>
		/// Номер байта
		/// </summary>
        public int ByteNumber;
		/// <summary>
		/// Сдвиг 
		/// </summary>
        public int Offset;
		/// <summary>
		/// Резерв 1
		/// </summary>
        public int res1;
		/// <summary>
		/// Резерв 2
		/// </summary>
        public int res2;
    }
    /// <summary>
    /// интерфейс проверки значения в канале
    /// </summary>
    public interface IIOCheckCode
    {
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        bool CheckCode(UInt32 Code, Addr addr);
    }

    /// <summary>
    /// интерфейс получения значения в канале
    /// </summary>
    public interface IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        Addr MakeAddr(int nChNumber);

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        UInt32 GetValue(byte[] data, Addr addr);

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        double GetValueDiap(byte[] data, Addr addr);

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nChNumber">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        bool GetChannelInfo(int nChNumber, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel);
    }

    /// <summary>
    /// интерфейс без проверки значения в канале
    /// </summary>
    public class CheckCodeNothing : DlxObject, IIOCheckCode
    {
        #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return true;
        }

        #endregion
    }

    /// <summary>
    /// интерфейс проверки 2-х бит на 11 
    /// </summary>
    public class CheckCode3 : DlxObject, IIOCheckCode
    {
        #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return !((Code & 3) == 3);
        }

        #endregion
    }

    /// <summary>
    /// интерфейс проверки байта на FF
    /// </summary>
    public class CheckCodeFF : DlxObject, IIOCheckCode
    {
	    #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return !((Code & 0xFF) == 0xFF);
        }

        #endregion
	}

    /// <summary>
    /// интерфейс проверки значения на 1FF
    /// </summary>
    public class CheckCode1FF : DlxObject, IIOCheckCode
    {
	    #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return !((Code & 0x1FF) == 0x1FF);
        }

        #endregion
    }

    /// <summary>
    /// интерфейс проверки значения на 3FF
    /// </summary>
    public class CheckCode3FF : DlxObject, IIOCheckCode
    {
        #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return !((Code & 0x3FF) == 0x3FF);
        }

        #endregion
    }

    /// <summary>
    /// интерфейс проверки значения на -1
    /// </summary>
    public class CheckCodeDword : DlxObject, IIOCheckCode
    {
        #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public virtual bool CheckCode(uint Code, Addr addr)
        {
            return Code != 0xFFFFFFFF;
        }

        #endregion
    }

    /// <summary>
    /// универсальный интерфейс проверки значения 
    /// </summary>
    public class CheckCodeUni : DlxObject, IIOCheckCode
    {   
        #region IIOCheckCode Members
        /// <summary>
        /// проверка корректности значение в канале
        /// </summary>
        /// <param name="Code">значение в канале</param>
        /// <param name="addr">сформированный адрес  данных канала</param>
        /// <returns>true - если значение Code есть значение канала, false - отказ канала</returns>
        public bool  CheckCode(uint Code, Addr addr)
        {
            if( Code == m_ErrorValue )
		        return false;
        
	        return true;
        }

        #endregion

        #region IDlx Members
        /// <summary>
        /// ЗАгрузка обйекта
        /// </summary>
        /// <param name="Loader">Загрузчик</param>
        public override void  Load(DlxLoader Loader)
        {
            // загружаем код ошибочного значения
	        m_ErrorValue = Loader.GetAttributeUInt("ErrorCode", 0xFFFFFFFF);
        }
        #endregion

        /// <summary>
        /// код ошибочного значения
        /// </summary>
        protected UInt32 m_ErrorValue;
    }

    /// <summary>
    /// интерфейс получения значения в канале 1 бит
    /// </summary>
    public class GetValue1bit : DlxObject, IIOGetValue
    {
	   
        #region IIOGetValue Members

        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = nChNumber/8;
            addr.Offset = nChNumber%8;
	        return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
	    return (uint)( (data[addr.ByteNumber] >> addr.Offset) & 1 );
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
	        //Стартовый байт данных
	        nStartByte = (uint)--nCh/8;
	        //Стартовый бит в байте
	        nStartBit = (uint)nCh%8;
	        //Количество бит на значение канала
	        nBitsPerChannel = 1;
	        return true;
        }

        #endregion
    }

    /// <summary>
    /// интерфейс получения значения в канале 2 бит
    /// </summary>
    public class GetValue2bit : DlxObject, IIOGetValue
    {
        #region IIOGetValue Members

        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");
	
            Addr addr = new Addr();
            addr.ByteNumber = nChNumber/4;
            addr.Offset = (nChNumber % 4)*2;
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
			uint value = (uint)( (data[addr.ByteNumber] >> addr.Offset) & 3 );
			// Проверка на корректность уже выполнена, значит 11 - нормальное значение:
			// мигание и мгновенное значение сигнала = 1.
			// Поэтому гасим мгновенное значение 11 -> 10,
			// так чтобы 00 - ноль, 01 - единица, 10 - мигание, 11 - отказ
			if (value == 0x03)
				value = 0x02;
        	return value;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
	        //Стартовый байт данных
	        nStartByte = (uint)--nCh/4;
	        //Стартовый бит в байте
	        nStartBit = (uint)(nCh*2)%8;
	        //Количество бит на значение канала
	        nBitsPerChannel = 2;
	        return true;
        }

        #endregion
    }

    /// <summary>
    /// интерфейс получения значения в канале 1 байт
    /// </summary>
    public class GetValue8bit : DlxObject, IIOGetValue
    {
        #region IIOGetValue Members

        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");
			
            Addr addr = new Addr();
            addr.ByteNumber = nChNumber;
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            return data[addr.ByteNumber];
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            //uint Code = GetValue(data, addr);

            //double value = (Code & 0x1FF);

            //if ((Code & 0x200) == 0)
            //    value /= 4.0;

            //return value;
            
            return GetValue(data, addr);
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
	        //Нумерация каналов с единицы - приводим к нулю
            if (nCh == 0)
                throw new ArgumentNullException(nCh.ToString(), "Номер канала не может быть нулевым!");
	        
	        //Стартовый байт данных
	        nStartByte = (uint)--nCh;
	        //Стартовый бит в байте
	        nStartBit = 0;
	        //Количество бит на значение канала
	        nBitsPerChannel = 8;
	        return true;
        }

        #endregion
    }

    public class GetValue32bit : GetValue8bit
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public override Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = nChNumber * 4;
            return addr;
        }
        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public override uint GetValue(byte[] data, Addr addr)
        {
            return BitConverter.ToUInt32(data, addr.ByteNumber);
        }
        public override double GetValueDiap(byte[] data, Addr addr)
        {
            return GetValue(data, addr) / 1000;
        }
        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public override bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
            if (nCh == 0)
                throw new ArgumentNullException(nCh.ToString(), "Номер канала не может быть нулевым!");

            //Стартовый байт данных
            nStartByte = (uint)--nCh * 4;
            //Стартовый бит в байте
            nStartBit = 0;
            //Количество бит на значение канала
            nBitsPerChannel = 32;
            return true;
        }
    }

    /// <summary>
    /// интерфейс получения значения в канале 10 бит
    /// </summary>
    public class GetValue10bit : DlxObject, IIOGetValue
    {
	    #region IIOGetValue Members

        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = nChNumber;
            addr.res1 = 8 + nChNumber / 4;
            addr.res2 = (nChNumber % 4) * 2;
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            uint Vl = data[addr.ByteNumber];
	        Vl |= (uint)((data[addr.res1] >> addr.res2) & 3) << 8;
	        return Vl;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            uint Code = GetValue(data, addr);
	        
		    double value = (Code & 0x1FF );

		    if( (Code & 0x200) == 0 )
			    value /= 4.0;

	        return value;
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nChNumber">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nChNumber, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
        	return false;
        }

        #endregion
	}

    /// <summary>
    /// интерфейс получения значения в канале 10 бит
    /// </summary>
    public class GetValue10bit_TK : GetValue10bit
    {
        #region IIOGetValue Members

        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = nChNumber;
            addr.res1 = 8 + nChNumber / 4;
            addr.res2 = (nChNumber % 4) * 2;
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            uint Vl = data[addr.ByteNumber];
            Vl |= (uint)((data[addr.res1] >> addr.res2) & 3) << 8;
            return Vl;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            uint Code = GetValue(data, addr);

            double value = (Code & 0x1FF);

            if ((Code & 0x200) == 0)
                value /= 4.0;

            return value;
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nChNumber">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nChNumber, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            return false;
        }

        #endregion
    }

    /// <summary>
    /// интерфейс получения значения в канале 10 бит в одном диапазоне
    /// </summary>
    public class GetValue8bitF : GetValue8bit_Factor
    {
        public override uint GetValue(byte[] data, Addr addr)
        {
            if (addr.ByteNumber > 2 && addr.ByteNumber < 6)
            {
                double curPhase = base.GetValue(data, addr);
                double nextPhase = base.GetValue(data, GetNextPhaseByteNumber(addr));
                double diff = nextPhase - curPhase;
                diff = diff >= 0 ? diff : diff + 128;
                diff *= 2812.5;
                // перодически разница значений не перекрывается константой "128", => на выходе отрицательное значение :-0
                return diff > 0 ? Convert.ToUInt32(diff) : 12000;
            }
            return base.GetValue(data, addr);
        }

        protected Addr GetNextPhaseByteNumber(Addr cur)
        {
            Addr next = cur;
            // номера байтов фаз - 3, 4, 5
            switch (cur.ByteNumber)
            {
                case 3: next.ByteNumber = 4; break;
                case 4: next.ByteNumber = 5; break;
                case 5: next.ByteNumber = 3; break;
                default: break;
            }
            return next;
        }
    }

    /// <summary>
    /// интерфейс получения значения в канале 10 бит в одном диапазоне
    /// </summary>
    public class GetValue10bitF : GetValue10bit
    {
        public override uint GetValue(byte[] data, Addr addr)
        {
            if (addr.ByteNumber > 2 && addr.ByteNumber < 6)
            {
                double curPhase = base.GetValue(data, addr);
                double nextPhase = base.GetValue(data, GetNextPhaseByteNumber(addr));
                double diff = nextPhase - curPhase;
                diff = diff >= 0 ? diff : diff + 128;
                diff *= 2812.5;
                // перодически разница значений не перекрывается константой "128", => на выходе отрицательное значение :-0
                return diff > 0 ? Convert.ToUInt32(diff) : 0;
            }
            return base.GetValue(data, addr);
        }

        protected Addr GetNextPhaseByteNumber(Addr cur)
        {
            Addr next = cur;
            // номера байтов фаз - 3, 4, 5
            switch (cur.ByteNumber)
            {
                case 3: next.ByteNumber = 4; break;
                case 4: next.ByteNumber = 5; break;
                case 5: next.ByteNumber = 3; break;
                default: break;
            }
            return next;
        }
    }

    /// <summary>
    /// интерфейс получения значения в канале 10 бит в одном диапазоне
    /// </summary>
    public class GetValue2_10bit : GetValue10bit
    {
        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public override double GetValueDiap(byte[] data, Addr addr)
        {
            return GetValue(data, addr);
        }
    }

	/// <summary>
	/// интерфейс получения значения в канале 10 бит в одном диапазоне
	/// </summary>
	public class GetValue8bit_Factor : GetValue8bit
	{
		/// <summary>
		/// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
		/// </summary>
		/// <param name="data">массив сырых данных модуля</param>
		/// <param name="addr">адрес данных канала в массиве сырых данных</param>
		/// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
		public override double GetValueDiap(byte[] data, Addr addr)
		{
			return _Factor * GetValue(data, addr);
		}

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_Factor = Loader.GetAttributeDouble("Factor", _Factor);
		}

		protected double _Factor = 1;
	}

    /// <summary>
    /// интерфейс получения значения в канале 10 бит для угр (Байт + 2 бита в след. байте)
    /// </summary>
    public class GetValue10bitNewUGR : GetValue10bit
    {
		/// <summary>
		/// получение значения канала из данных модуля по адресу 
		/// </summary>
		/// <param name="data">массив сырых данных модуля</param>
		/// <param name="addr">адрес данных канала в массиве сырых данных</param>
		/// <returns>Значение полученное из сырых данных</returns>
		public override uint GetValue(byte[] data, Addr addr)
		{
			BinaryReader reader = new BinaryReader(new MemoryStream(data));
			uint Vl = reader.ReadByte();
			Vl |= (uint)(reader.ReadByte() & 3) << 8;
			return Vl;
		}
    }

    /// <summary>
    /// интерфейс получения значения в канале 4 байт
    /// </summary>
    public class GetValueFloat : DlxObject, IIOGetValue
    {
        #region IIOGetValue Members
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = nChNumber * 4;
            return addr;
        }
        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            double dv = GetValueDiap(data, addr);
            if (dv > uint.MinValue && dv < uint.MaxValue)
                return Convert.ToUInt32(dv * 1000);
            else
                return 0xFFFFFFFF;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            return BitConverter.ToSingle(data, addr.ByteNumber);
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
            if (nCh == 0)
                throw new ArgumentNullException(nCh.ToString(), "Номер канала не может быть нулевым!");

            //Стартовый байт данных
            nStartByte = (uint)--nCh;
            //Стартовый бит в байте
            nStartBit = 0;
            //Количество бит на значение канала
            nBitsPerChannel = 32;
            return true;
        }

        #endregion


    }

    #region Интерфейсы модулей АБТЦМ
    /// <summary>
    /// интерфейс получения значения сигналов для модуля БУСС увязки с АБТЦ-М
    /// </summary>
    public class GetValueBUSS : DlxObject, IIOGetValue
    {
	    /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
	        // Номер байта (7 сигналов в байте)
	        addr.ByteNumber = nChNumber/7;
	        // Смещение в байте
	        addr.Offset = nChNumber%7;

            if (addr.Offset == 6)
                addr.res1 = 3;// Последние сигналы в группе 2х-битовые
            else
                addr.res1 = 1;
                        
	        return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            // addr.res1 - маска
            return (uint)((data[addr.ByteNumber] >> addr.Offset) & addr.res1);
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
	        //Стартовый байт данных
	        nStartByte = (uint)--nCh/7;
	        //Стартовый бит в байте
	        nStartBit = (uint)nCh%7;
	        //Количество бит на значение канала
            if (nStartBit == 6)
	            nBitsPerChannel = 2;
            else
                nBitsPerChannel = 1;
	        return true;
        }

    }

    /// <summary>
    /// интерфейс получения значения сигналов для модуля БИЭЦ увязки с АБТЦ-М
    /// </summary>
    public class GetValueBIEC : DlxObject, IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();

            if (nChNumber == 0)// Перый сигнал 8 бит
            {
                addr.ByteNumber = 0;
                addr.Offset = 0;
                addr.res1 = 0xFF;
            }
            else// остальные сигналы - 1 бит
            {
                nChNumber += 7;
                addr.ByteNumber = nChNumber / 8;
                addr.Offset = nChNumber % 8;
                addr.res1 = 1;
            }
            
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            // addr.res1 - маска
            return (uint)((data[addr.ByteNumber] >> addr.Offset) & addr.res1);
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
            if (--nCh == 0)
            {
                nStartByte = 0;
                nStartBit = 0;
                nBitsPerChannel = 8;
            }
            else
            {
                nStartByte = (uint)--nCh / 8;
                nStartBit = (uint)nCh % 8;
                nBitsPerChannel = 1;
            }

            return true;
        }       
    }

    /// <summary>
    /// интерфейс получения значения сигналов для модуля БКРЦ увязки с АБТЦ-М
    /// </summary>
    public class GetValueBKRC : DlxObject, IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            addr.ByteNumber = 0;
            switch (nChNumber)
            {
                case 0:
                    addr.Offset = 0;// бит
                    addr.res1 = 3;  // маска
                    break;
                case 1:
                    addr.Offset = 2;// бит
                    addr.res1 = 3;  // маска
                    break;
                case 2:
                    addr.Offset = 4;// бит
                    addr.res1 = 1;  // маска
                    break;
                case 3:
                    addr.Offset = 5;// бит
                    addr.res1 = 3;  // маска
                    break;
                case 4:
                    addr.Offset = 7;// бит
                    addr.res1 = 1;  // маска
                    break;
                case 5: //ПА
                    addr.Offset = 0;//бит
                    addr.res1 = 1;  // маска
                    addr.res2 = 0;  //сравнение
                    break;
                case 6: //ПБ 
                    addr.Offset = 2;//бит
                    addr.res1 = 1;  // маска
                    addr.res2 = 0;  //сравнение
                    break;
                case 7: //ЗА
                    addr.Offset = 0;//бит
                    addr.res1 = 3;  // маска
                    addr.res2 = 2;  //сравнение
                    break;
                case 8: //ЗБ
                    addr.Offset = 2;//бит
                    addr.res1 = 3;  // маска
                    addr.res2 = 2;  //сравнение
                    break;
                case 9: //ЛЗА
                    addr.Offset = 0;//бит
                    addr.res1 = 3;  // маска
                    addr.res2 = 3;  //сравнение
                    break;
                case 10://ЛЗБ
                    addr.Offset = 2;//бит
                    addr.res1 = 3;  // маска
                    addr.res2 = 3;  //сравнение
                    break;
                default:
                    throw new ArgumentException("GetValueBKRC:MakeAddr Неверный номер канала БКРЦ", nChNumber.ToString());
            }

            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            // addr.res1 - маска
            return (uint)((data[addr.ByteNumber] >> addr.Offset) & addr.res1);
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            nStartByte = 0;
            nBitsPerChannel = 1;
            switch (--nCh)
            {
                case 0:
                    nStartBit = 0;
                    nBitsPerChannel = 2;
                    break;
                case 1:
                    nStartBit = 2;
                    nBitsPerChannel = 2;
                    break;
                case 2:
                    nStartBit = 4;
                    break;
                case 3:
                    nStartBit = 5;
                    nBitsPerChannel = 2;
                    break;
                case 4:
                    nStartBit = 7;
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    nBitsPerChannel = 1;
                    break;
                default:
                    throw new ArgumentException("GetValueBKRC:GetChannelInfo Неверный номер канала БКРЦ", nCh.ToString());
            }

            return true;
        }
    }
    /// <summary>
    /// интерфейс получения значения сигналов для модуля ПМИРЦ увязки с АБТЦ-М
    /// </summary>
    public class GetValuePMIRC : DlxObject, IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// Addr.ByteNumber - Номре байта
        /// Addr.res1 - Количество байт
        /// Addr.res2 - делитель
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            Addr addr = new Addr();

            switch (nChNumber)
            {        
                // Каналы КРЛ (ТРЦ)
                case 1://Несущая частота (КРЛ1, ТРЦ1)
                    addr.ByteNumber = 0;
                    addr.res1 = 2;
                    addr.res2 = 10;
                    break;
                case 2://Частота девиации (КРЛ1, ТРЦ1)
                    addr.ByteNumber = 2;
                    addr.res1 = 2;
                    addr.res2 = 10;
                    break;
                case 3://СКЗ сигнала мВ (КРЛ1, ТРЦ1)
                    addr.ByteNumber = 4;
                    addr.res1 = 4;
                    addr.res2 = 100;
                    break;
                case 4://Номер кодовой комбинации (КРЛ1)
                    addr.ByteNumber = 8;
                    addr.res1 = 1;
                    addr.res2 = 1;
                    break;
                // Каналы АЛСН
                case 5://Код сигнала (АЛСН)
                    addr.ByteNumber = 9;
                    addr.res1 = 1;
                    addr.res2 = 1;
                    break;
                case 6://Период сигнала (АЛСН)
                    addr.ByteNumber = 10;
                    addr.res1 = 2;
                    addr.res2 = 1;
                    break;
                case 7://Длительность импульса (АЛСН)
                    addr.ByteNumber = 12;
                    addr.res1 = 2;
                    addr.res2 = 1;
                    break;
                case 8://Длительность паузы (АЛСН)
                    addr.ByteNumber = 14;
                    addr.res1 = 2;
                    addr.res2 = 1;
                    break;
                case 9://СКЗ сигнала мВ (АЛСН)
                    addr.ByteNumber = 16;
                    addr.res1 = 4;
                    addr.res2 = 100;
                    break;
                case 10://Несущая частота (АЛСН)
                    addr.ByteNumber = 20;
                    addr.res1 = 2;
                    addr.res2 = 10;
                    break;
                case 11://Тип трансмиттера (АЛСН)
                    addr.ByteNumber = 22;
                    addr.res1 = 1;
                    addr.res2 = 1;
                    break;
                // коды КРЛ2
                case 12:
                case 13:
                case 14:
                case 15:
                // коды АЛСН-ЕН
                case 16:
                case 17:
                case 18:
                case 19:
                    break;
                default:
                    throw new ArgumentNullException(nChNumber.ToString(), "Не допустимый номер канала!");
            }
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            try
            {
                uint res = 0;
                // Читаем заданные число байт из массива
                BinaryReader reader = new BinaryReader(new MemoryStream(data, addr.ByteNumber, (int)addr.res1));
                byte[] btres = BitConverter.GetBytes(res);
                reader.ReadBytes(addr.res1).CopyTo(btres,0);
                res = BitConverter.ToUInt32(btres, 0);
                if (addr.res2 > 1) // делим     
                {   // умножение на 1000 удалено
                    res = (uint)((res / addr.res2));
                }
                if (addr.res2 == 1 && res == 255)
                    res = 0;

                return res;
            }
            catch(Exception)
            {
                return 0xFFFFFFFF;
            }
            
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            uint res = 0;
            try
            {
                // Читаем заданные число байт из массива
                BinaryReader reader = new BinaryReader(new MemoryStream(data, addr.ByteNumber, (int)addr.res1));
                byte[] btres = BitConverter.GetBytes(res);
                reader.ReadBytes(addr.res1).CopyTo(btres, 0);
                res = BitConverter.ToUInt32(btres, 0);
            }
            catch(Exception)
            {
                return -1;
            }

            // Если задан делитель - делим значение
            if(addr.res2 > 1)
            {
                return res / addr.res2;
            }
            else 
                return (double)res;
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            nStartBit = 0;
            switch (nCh)
            {
                // Каналы КРЛ (ТРЦ)
                case 1://Несущая частота (КРЛ1, ТРЦ1)
                    nStartByte = 0;
                    nBitsPerChannel = 2*8;
                    break;
                case 2://Частота девиации (КРЛ1, ТРЦ1)
                    nStartByte = 2;
                    nBitsPerChannel = 2*8;
                    break;
                case 3://СКЗ сигнала мВ (КРЛ1, ТРЦ1)
                    nStartByte = 4;
                    nBitsPerChannel = 4*8;
                    break;
                case 4://Номер кодовой комбинации (КРЛ1)
                    nStartByte = 8;
                    nBitsPerChannel = 1*8;
                    break;
                // Каналы АЛСН
                case 5://Код сигнала (АЛСН)
                    nStartByte = 9;
                    nBitsPerChannel = 1*8;
                    break;
                case 6://Период сигнала (АЛСН)
                    nStartByte = 10;
                    nBitsPerChannel = 2*8;
                    break;
                case 7://Длительность импульса (АЛСН)
                    nStartByte = 12;
                    nBitsPerChannel = 2*8;
                    break;
                case 8://Длительность паузы (АЛСН)
                    nStartByte = 14;
                    nBitsPerChannel = 2*8;
                    break;
                case 9://СКЗ сигнала мВ (АЛСН)
                    nStartByte = 16;
                    nBitsPerChannel = 4*8;
                    break;
                case 10://Несущая частота (АЛСН)
                    nStartByte = 20;
                    nBitsPerChannel = 2*8;
                    break;
                case 11://Тип трансмиттера (АЛСН)
                    nStartByte = 22;
                    nBitsPerChannel = 1*8;
                    break;
                // коды КРЛ2
                case 12:
                case 13:
                case 14:
                case 15:
                // коды АЛСН-ЕН
                case 16:
                case 17:
                case 18:
                case 19:
                    nStartByte = 0;
                    nBitsPerChannel = 1;
                    break;
                default:
                    return false;
            }
            return true;
        }
    }
    #endregion

    /// <summary>
    /// интерфейс получения значения сигналов для модуля ПИ8ТП увязки с СЗИЦ-Д
    /// </summary>
    public class GetValuePI : DlxObject, IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// Addr.ByteNumber - Номер байта
        /// Addr.Offset - номер стартового бита
        /// Addr.res2 - маска
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            Addr addr = new Addr();
            nChNumber -= 1;

            int nGroupNumber = (int)((double)nChNumber / 10);
            int nChNumberInGroup = nChNumber % 10;

            addr.ByteNumber = nGroupNumber * 2;
            addr.Offset = 0;
            addr.res1 = 1;

            if (nChNumberInGroup > 7)
            {
                // старший байт слова
                addr.ByteNumber++;

                // старшая тетрада (ражим)
                addr.res1 = 0x0F;
                addr.Offset = 4;// mode
                
                // младшая тетрада (символ)
                if (nChNumberInGroup == 9)
                    addr.Offset = 0;// symb
            }
            else
                addr.Offset = 7 - nChNumberInGroup;// Обратный порядок бит
            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            try
            {
                return (uint)((data[addr.ByteNumber] & (addr.res1 << addr.Offset )) >> addr.Offset);
            }
            catch(Exception)
            {
                return 0xFFFFFFFF;
            }
            
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            if (nCh > 80)
                return false;

            nCh -= 1;

            int nGroupNumber = (int)((double)nCh / 10);
            int nChNumberInGroup = nCh % 10;

            nStartByte = (uint)nGroupNumber * 2;
            nStartBit = 0;
            nBitsPerChannel = 1;

            if (nChNumberInGroup > 7)
            {
                // старший байт слова
                nStartByte++;

                // старшая тетрада (ражим)
                nBitsPerChannel = 4;
                nStartBit = 4;// mode

                // младшая тетрада (символ)
                if (nChNumberInGroup == 9)
                    nStartBit = 0;// symb
            }
            else
                nStartBit = (uint)(7 - nChNumberInGroup);// Обратный порядок бит
            
            return true;
        }
    }

    /// <summary>
    /// Получение данных ИСИ
    /// </summary>
    public class GetValueISI : DlxObject, IIOGetValue
    {
        #region IIOGetValue Members
        /// <summary>
        /// Формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            Addr addr = new Addr();
            // в младшее слово записываем номер канала | 0x10, если это аналоговый канал (один из 32)
            // в старшее слово записываем номер байта и номер бита для дискретного канала
            if (nChNumber < 32)
            {
                addr.ByteNumber = nChNumber;
                addr.res1 = 1;
            }
            nChNumber -= 32;

            // если в первых 16 каналах(дискретных), то это признак снижения... Бит 4
            addr.Offset = 4;
            if (nChNumber >= 16)
            {// если во вторых  16 каналах(дискретных), 
                nChNumber -= 16;
                addr.Offset = 0;
            }
            addr.ByteNumber = 128 + nChNumber;

            return addr;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            if (addr.res1 == 1)
                return BitConverter.ToUInt32(data, 4 * addr.ByteNumber);
            else
                return (uint)(data[addr.ByteNumber] & (1 << addr.Offset)) >> addr.Offset;
        }

        /// <summary>
        /// Получение аналогового значения канала из данных модуля по адресу.
        /// Преобразует код в значение делением на 1000
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное в аналоговое значение</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            return GetValue(data, addr) / 1000;
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            return false;
        }
        #endregion
    }

    /// <summary>
    /// Класс получения значения сигналов модуля ВММА8
    /// </summary>
    public class GetValueVMMA8 : DlxObject, IIOGetValue
    {
        /// <summary>
        /// формирование адреса канала по номеру (номер с единицы)
        /// Addr.ByteNumber - Номер байта
        /// Addr.Offset - номер стартового бита
        /// Addr.res2 - маска
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            Addr addr = new Addr();
            addr.ByteNumber = nChNumber - 1;

            return addr;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            try
            {
                int ch = addr.ByteNumber;
                if (addr.ByteNumber < 8)// аналоговый
                {
                    return Convert.ToUInt32(BitConverter.ToSingle(data, 4 * addr.ByteNumber) * 1000);
                }
                else if (addr.ByteNumber < 48)// дискретный 2 бит
                {
                    int offset = 32;
                    ch -= 8;

                    return (uint)(data[offset + (ch >> 2)] >> ((ch << 1) % 4) & 3);
                }
                else if (addr.ByteNumber < 56)// дискретный 6 бит (Кптш)
                {
                    ulong val = BitConverter.ToUInt64(data, 32 + 10);
                    ch -= 48;

                    return (uint)(val >> (ch * 6)) & 0x3f;
                }

                return 0xFFFFFFFF;
            }
            catch (Exception)
            {
                return 0xFFFFFFFF;
            }

        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            if (addr.ByteNumber < 8)// аналоговый
            {
                return BitConverter.ToSingle(data, 4 * addr.ByteNumber);
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            return false;
        }
    }

    /// <summary>
    /// Интерфейс получения значения сигналов для модуля ШУДГА_И увязки с ШУ ДГА
    /// </summary>
    public class GetValueShudgaI : DlxObject, IIOGetValue
    {
        #region IIOGetValue Members
        /// <summary>
        /// Формирование адреса канала по номеру (номер с единицы)
        /// </summary>
        public virtual Addr MakeAddr(int nChNumber)
        {
            if (--nChNumber < 0)
                throw new ArgumentNullException(nChNumber.ToString(), "Номер канала не может быть нулевым!");

            return new Addr {ByteNumber = nChNumber*4, res1 = nChNumber};
        }

        /// <summary>
        /// Получение значения канала из данных модуля по адресу 
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных</returns>
        public virtual uint GetValue(byte[] data, Addr addr)
        {
            var val = BitConverter.ToUInt32(data, addr.ByteNumber);
            if ((addr.res1 == 6) || (addr.res1 == 7) || (addr.res1 == 13))
                return 0xFFFFFFFF;
            switch (addr.res1)
            {
                case 10:
                    val *= 1000000;
                    break;
                case 11:
                    val *= 100;
                    break;
                case 15:
                    val /= 3600;
                    break;
            }
            return val;
        }

        /// <summary>
        /// получение значения канала из данных модуля по адресу с учетом диапазона(МАВ)
        /// </summary>
        /// <param name="data">массив сырых данных модуля</param>
        /// <param name="addr">адрес данных канала в массиве сырых данных</param>
        /// <returns>Значение полученное из сырых данных и преобразованное для диапазона</returns>
        public virtual double GetValueDiap(byte[] data, Addr addr)
        {
            var val = GetValue(data, addr);
            return (val == 0xFFFFFFFF) ? val : val / 1000.0;
        }

        /// <summary>
        /// получение информации о расположении данных канала в буффере модуля (TRUE - информация известна)
        /// </summary>
        /// <param name="nCh">Номер канала</param>
        /// <param name="nBitsPerChannel">Количетво бит для кодирования значения</param>
        /// <param name="nStartBit">Номер бита, с которого начинается значение</param>
        /// <param name="nStartByte">Номер байта, с которого начинается значение</param>
        /// <returns>TRUE - информация известна</returns>
        public virtual bool GetChannelInfo(int nCh, ref uint nStartByte, ref uint nStartBit, ref uint nBitsPerChannel)
        {
            //Нумерация каналов с единицы - приводим к нулю
            if (nCh == 0)
                throw new ArgumentNullException(nCh.ToString(), "Номер канала не может быть нулевым!");

            //Стартовый байт данных
            nStartByte = (uint)(--nCh * 4);
            //Стартовый бит в байте
            nStartBit = 0;
            //Количество бит на значение канала
            nBitsPerChannel = 32;
            return true;
        }

        #endregion
    }

    #endregion
}