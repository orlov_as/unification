﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Afx;
using AfxEx;
using Dps.UniRequest;
using Ivk.IOSys;
using Tdm.Unification.v2;

namespace Tdm.Unification
{
	/// <summary>
	/// Нормали объектов
	/// </summary>
	public class NPObject
	{
		/// <summary>
		/// Максимум по значению
		/// </summary>
		public Int32 MaxY;
		/// <summary>
		/// Минимум по значению
		/// </summary>
		public Int32 MinY;
		/// <summary>
		/// Минимум
		/// </summary>
		public Int32 Min;
		/// <summary>
		/// Максимум
		/// </summary>
		public Int32 Max;
		/// <summary>
		/// Дополнительный минимум
		/// </summary>
		public Int32 PrMin;
		/// <summary>
		/// Дополнительный максимум
		/// </summary>
		public Int32 PrMax;
		/// <summary>
		/// Шаг по Y
		/// </summary>
		public Int32 StepY;
		/// <summary>
		/// Комментарий
		/// </summary>
		public string Comment;
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="reader"></param>
		public void Load(BinaryReader reader)
		{
			MaxY = reader.ReadInt32();
			MinY = reader.ReadInt32();
			Min = reader.ReadInt32();
			Max = reader.ReadInt32();
			PrMin = reader.ReadInt32();
			PrMax = reader.ReadInt32();
			StepY = reader.ReadInt32();
			Comment = Afx.CArchive.ReadString(reader);
		}
	}
	/// <summary>
	/// Группа нормалей
	/// </summary>
	public class NPTag
	{
		/// <summary>
		/// Идентификатор объекта
		/// </summary>
		public UInt16 _objid;
		/// <summary>
		/// Имя объекта
		/// </summary>
		public string _objName;
		/// <summary>
		/// Группа объектов
		/// </summary>
		public string _group;
		/// <summary>
		/// Задача
		/// </summary>
		public string _task;
		/// <summary>
		/// Нормали
		/// </summary>
		public NPObject _onp = new NPObject();
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="reader"></param>
		public void Load(BinaryReader reader)
		{
			_objid = reader.ReadUInt16();
			_objName = Afx.CArchive.ReadString(reader);
			_group = Afx.CArchive.ReadString(reader);
			_task = Afx.CArchive.ReadString(reader);

			_onp.Load(reader);
		}
		public override string ToString()
		{
			return string.Format("{0} ({1})", _objName, _objid);
		}

	}
	/// <summary>
	/// Поток запроса нормалей
	/// </summary>
	class NormsRequestClient : DlxObject, ISuivComponent
	{
		#region Данные

		/// <summary>
		/// Фабрика соединений
		/// </summary>
		protected ConnectionFactory _connFactory = null;
		/// <summary>
		/// Сессия открытого соединения
		/// </summary>
		protected ClientSession _session = null;
		/// <summary>
		/// Сообщение для запроса
		/// </summary>
		protected Message _requestMsg = new Message();
		/// <summary>
		/// Частота запросов в миллисекундах
		/// </summary>
		protected int _rateQuery;
		/// <summary>
		/// Предв.задержка в миллисекундах
		/// </summary>
		protected int _initDelay;
		/// <summary>
		/// Признак первого запуска (загрузка норм из БД)
		/// </summary>
		protected bool _firstRun = true;
		/// <summary>
		/// Делитель значений нормалей
		/// </summary>
		protected float _divider = 10.0f;
		protected DlxObject _siteList;

		/// <summary>
		/// Таймер для выполнения синхронизации
		/// </summary>
		protected Timer _syncTimer = null;

		protected Dictionary<string, string> _destToSiteMap = new Dictionary<string, string>();
		protected Dictionary<string, IDpsAssociations> _destToObjSysMap = new Dictionary<string, IDpsAssociations>();
		protected Dictionary<string, DateTime> _destToNPTime = new Dictionary<string, DateTime>();

		/// <summary>
		/// Словарь перегонных объектов
		/// </summary>
		protected Dictionary<int, Dictionary<int, int>> _stageObjsMap = new Dictionary<int, Dictionary<int, int>>();

		protected DBConnector _dbconnector = new DBConnector();

		public event Action<DlxObject, string, int, DateTime?, string> SendState;
		public event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;
		#endregion

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_dbconnector.Load(Loader);

			_connFactory = Loader.GetObjectFromAttribute("Connection") as ConnectionFactory;
			// считываем частоту запросов
			_rateQuery = Loader.GetAttributeInt("QueryRate", 3600) * 1000;  // 1 час
			_initDelay = Loader.GetAttributeInt("InitDelay", 0) * 1000;     // 0 секунд
			_divider = (float)Loader.GetAttributeInt("Divider", (int)_divider);
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxObject;
			XmlElement cur = Loader.CurXmlElement;
			List<XmlElement> Elemets;
			Loader.CurXmlElement.EnumInhElements(out Elemets, "dest", null, null);
			string dest = string.Empty;
			string Site = string.Empty;
			foreach(XmlElement elem in Elemets)
			{
				Loader.CurXmlElement = elem;
				if(elem.GetAttributeValue("name", ref dest)
					&& (elem.GetAttributeValue("Site", ref Site)))
				{
					string Objs = elem.GetAttribute("Objects");
					_destToSiteMap[dest] = Site + "%" + Objs;
				}
				else
					throw new ArgumentException("NormsRequestClient: " + Owner.Name + ": Error to load item of dest association");
			}
		}
		public override void Create()
		{
			foreach(string dest in _destToSiteMap.Keys)
			{
				string[] path = _destToSiteMap[dest].Split('%');
				if(path[1].Length == 0)
					path[1] = "Objects";
				IDpsAssociations ObjSys = _siteList.FindObject(path[0] + "/" + path[1]) as IDpsAssociations;
				if(ObjSys == null)
					throw new ArgumentException("NormsRequestClient: " + Owner.Name + ": Error to load item of dest association");
				_destToObjSysMap[dest] = ObjSys;
			}

			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnTimer);
			_syncTimer = new Timer(timerDelegate, null, _initDelay + 10000, _rateQuery);

			base.Create();
		}

		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// </summary>
		public void OnTimer(Object stateInfo)
		{
			List<int> processedSites = new List<int>();
			int SiteID = 0;
			try
			{
				// если сконфигурировано подключение и мы отключены, то подключаемся
				if(_dbconnector.IsConfigurated && !_dbconnector.IsConnected)
					_dbconnector.Connect();

				InitNormals();

				foreach(string dest in _destToObjSysMap.Keys)
				{
					SiteID = GetSiteID(_destToObjSysMap[dest]);

					LoadStageObjectsFromDB(SiteID);

					bool overwriteAdkNorms = !processedSites.Contains(SiteID);
					if(overwriteAdkNorms)
						processedSites.Add(SiteID);

					_requestMsg.Code = RequestCode.FTP;
					_requestMsg.SubCode = 0;
					_requestMsg.Dest = dest;

					// цикл по требуемым станциям
					try
					{
						// открываем соединение, если оно закрыто
						if(_session == null)
							OpenSession();

						if(_session != null)
						{
							uint FileLen = 0;
							DateTime FileTime = GetNPFileStat(out FileLen);
							DateTime PrevTime = DateTime.MinValue;
							if(!_destToNPTime.TryGetValue(dest, out PrevTime) || FileTime != PrevTime)
							{
								byte[] npfiledata = new byte[FileLen];
								GetNPFileData(FileLen, ref npfiledata);

								string[] ss = dest.Split(new char[] { '/' });
								string filename = ss[ss.Length - 1];

								ProcessAnswer(filename, SiteID, _destToObjSysMap[dest], npfiledata, overwriteAdkNorms);
								SendState?.Invoke(this, "", SiteID, FileTime, "");
							}
						}
					}
					catch(Exception e)
					{
						// закрываем сессию
						_session?.Close();
						_session = null;
						string err = "NormsRequestClient " + Name + " catch exception for " + dest + ": " + e.ToString();
						Trace.WriteLine(err);
						Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)301, err);
						SendState?.Invoke(this, "", SiteID, null, e.Message);
					}
				}
				// если подключены к базе, то отключаемся
				if(_dbconnector.IsConnected)
					_dbconnector.Disconnect();
			}
			catch(Exception ex)
			{
				Trace.WriteLine("NormsRequestClient " + Name + " catch exception: " + ex.ToString());
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)301, "Ошибка на потоке запроса нормалей: " + ex.Message);
				Dps.AppDiag.EventLog.SysEventLog.WriteException(ex);
				SendState?.Invoke(this, "", SiteID, null, ex.Message);
			}
		}

		/*public override void Destroy()
		{
			_syncTimer.Dispose();
			base.Destroy();
		}*/

		protected DateTime GetNPFileStat(out uint FileLen)
		{
			var ExStr = "NormsRequestClient: Ошибка получения данных от ПК при запросе информации о файле нормалей";
			int tryN = 0;
			const int tryMax = 30;
			FileLen = 0;

			for(int i = 0; i < tryMax; i++)
			{
				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);
				writer.Write((UInt16)1);

				string[] ss = _requestMsg.Dest.Split(new char[] { '/' });
				string filename = ss[ss.Length - 1];

				Afx.CArchive.WriteString(writer, filename + ".np");
				writer.Flush();
				writer.Close();

				_requestMsg.Data = ms.ToArray();
				// отправляем запрос
				_session.SendMessage(_requestMsg);

				// полчаем ответ
				Message answerMsg = _session.ReceiveMessage();
				if(answerMsg.Data == null || answerMsg.Code == RequestCode.Error)
					throw new FileLoadException(ExStr);

				ms = new MemoryStream(answerMsg.Data);
				BinaryReader reader = new BinaryReader(ms);
				UInt16 code = reader.ReadUInt16();
				if(code == 1)
				{
					FileLen = reader.ReadUInt32();
					reader.ReadUInt32();
					reader.ReadUInt32();
					int time = reader.ReadInt32();
					DateTime Time = Afx.CTime.ToDateTime(time);
					return Time;
				}
				else if(code == 0)
				{
					reader.ReadInt32();
					ExStr = Afx.CArchive.ReadString(reader);
				}
			}
			throw new FileLoadException(ExStr);
		}

		protected void GetNPFileData(uint FileLen, ref byte[] npfiledata)
		{
			int offset = 0;
			UInt16 bufflen = 8192;
			int tryN = 0;
			const int tryMax = 30;

			do
			{
				if(FileLen - offset < 8192)
					bufflen = (UInt16)(FileLen - offset);

				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);
				writer.Write((UInt16)2);

				string[] ss = _requestMsg.Dest.Split(new char[] { '/' });
				string filename = ss[ss.Length - 1];

				Afx.CArchive.WriteString(writer, filename + ".np");

				writer.Write((UInt32)offset);
				writer.Write((UInt16)bufflen);
				writer.Flush();
				writer.Close();

				_requestMsg.Data = ms.ToArray();
				// отправляем запрос
				_session.SendMessage(_requestMsg);

				var ExStr = "NormsRequestClient: Ошибка получения файла нормалей от промышленного компьютера";

				// полчаем ответ
				Message answerMsg = _session.ReceiveMessage();
				if(answerMsg.Data == null)
					throw new FileLoadException(ExStr);

				ms = new MemoryStream(answerMsg.Data);
				BinaryReader reader = new BinaryReader(ms);
				UInt16 code = reader.ReadUInt16();
				if(code == 2)
				{
					UInt16 len = reader.ReadUInt16();
					if(len != bufflen)
						throw new FileLoadException(ExStr);

					Array.Copy(answerMsg.Data, 4, npfiledata, offset, bufflen);
					offset += bufflen;
					tryN = 0;
				}
				else
				{
					if(tryN < tryMax)
					{
						tryN++;
					}
					else
					{
						ExStr = "NormsRequestClient: Ошибка получения данных от ПК при запросе файла нормалей";
						if(code == 0)
						{
							reader.ReadInt32();
							ExStr = Afx.CArchive.ReadString(reader);
						}
						throw new FileLoadException(ExStr);
					}
				}
			}
			while(offset < FileLen);
		}

		protected void OpenSession()
		{
			try
			{
				_session = new ClientSession(_connFactory.CreateConnection());
				Trace.WriteLine("NormsRequestClient " + Name + ": OK to open connection!");
			}
			catch(Exception e)
			{
				_session = null;
				Trace.WriteLine("NormsRequestClient " + Name + " open client session exception : " + e.ToString());
				throw new Exception("Не удалось подключиться к DpsServer");
			}
		}

		protected void ProcessAnswer(string dest, int SiteID, IDpsAssociations objSys, byte[] data, bool overwriteAdkNorms)
		{
			// загрузка нормалей
			BinaryReader reader = new BinaryReader(new MemoryStream(data));
			reader.ReadBytes(40);
			List<NPTag> NParray = new List<NPTag>();
			try
			{
				while(true)
				{
					NPTag nptag = new NPTag();
					nptag.Load(reader);
					NParray.Add(nptag);
				}
			}
			catch(Exception)
			{ }

			// установка нормалей
			StreamWriter sw0 = new StreamWriter("неувязались" + dest + this.Name + ".txt", false, Encoding.Default);
			sw0.WriteLine(DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss")); sw0.Flush(); sw0.Close();

			if(objSys.ObjsAdktoUniAssoc == null || !objSys.ObjsAdktoUniAssoc.HaveAssociations)
				return;

			// подмена для нормалей путевых приемников и генераторов (в т.IO_СигналыЗадач БД САПР не поместилось полное название нормали (nvarchar(50)))
			string tmpSubs = "Stantsiya/Napryazhenie pitaniya putevyih ";

			UniObjInfo oi;
			bool adkNormsCleaned = !overwriteAdkNorms;
			foreach(NPTag nptag in NParray)
			{
				bool bFind = false;
				string npNormPath = nptag._group + "/" + nptag._task;
				if(objSys.ObjsAdktoUniAssoc._objsAdkTOobjsUniMAP.TryGetValue(nptag._objid, out oi))
				{
					if(oi.Obj != null)
					{
						foreach(ObjData odata in oi.Obj.ObjDataCollection.Objects)
						{
							UniObjData objdata = odata as UniObjData;
							if(objdata != null
								&& (objdata._diagParamsPath == npNormPath
									|| (npNormPath.StartsWith(objdata._diagParamsPath) && objdata._diagParamsPath.StartsWith(tmpSubs))
									|| (IsAmbiguousNormalPath(nptag._group) && AreEqualAmbiguousNormalPaths(nptag._task, objdata._diagParamsPath))))
							{
								if(npNormPath == "Izolyatsiya/Soprotivlenie")
								{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
									objdata._normalMax = (float)((nptag._onp.Max / _divider) * 1000);
									objdata._normalMin = (float)((nptag._onp.Min / _divider) * 1000);
								}
								else if(npNormPath == "IMSI/Soprotivlenie")
								{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
									objdata._normalMax = (float)((nptag._onp.Max / _divider) * 1000000);
									objdata._normalMin = (float)((nptag._onp.Min / _divider) * 1000000);
								}
								else
								{
									objdata._normalMax = (float)(nptag._onp.Max / _divider);
									objdata._normalMin = (float)(nptag._onp.Min / _divider);
								}
								StoreNormalsToDB(dest, oi.Obj, objdata);
								bFind = true;
								Trace.WriteLine("NormsRequestClient " + Name + " set min max normals for : " + objdata.FullName);
							}
							else if(objdata != null
								&& (objdata._diagParamsPathExt == npNormPath || (npNormPath.StartsWith(objdata._diagParamsPathExt)
																				  && objdata._diagParamsPathExt.StartsWith(tmpSubs))))
							{
								if(npNormPath == "Izolyatsiya/Soprotivlenie")
								{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
									objdata._normalExt = (float)((nptag._onp.Max / _divider) * 1000);
								}
								else
								{
									objdata._normalExt = (float)(nptag._onp.Max / _divider);
								}
								StoreNormalsToDB(dest, oi.Obj, objdata);
								bFind = true;
								Trace.WriteLine("NormsRequestClient " + Name + " set ext normal for : " + objdata.FullName);
							}
						}
					}

					if(oi.ObjConditions != null)
					{
						foreach(UniObjInfo.UniObjCondition objcond in oi.ObjConditions)
						{
							if(objcond.Obj != null)
							{
								foreach(ObjData odata in objcond.Obj.ObjDataCollection.Objects)
								{
									UniObjData objdata = odata as UniObjData;
									if(objdata != null
										&& (objdata._diagParamsPath == npNormPath
											|| (npNormPath.StartsWith(objdata._diagParamsPath) && objdata._diagParamsPath.StartsWith(tmpSubs))
											|| (IsAmbiguousNormalPath(nptag._group) && AreEqualAmbiguousNormalPaths(nptag._task, objdata._diagParamsPath))))
									{
										if(npNormPath == "Izolyatsiya/Soprotivlenie")
										{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
											objdata._normalMax = (float)((nptag._onp.Max / _divider) * 1000);
											objdata._normalMin = (float)((nptag._onp.Min / _divider) * 1000);
										}
										else if(npNormPath == "IMSI/Soprotivlenie")
										{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
											objdata._normalMax = (float)((nptag._onp.Max / _divider) * 1000000);
											objdata._normalMin = (float)((nptag._onp.Min / _divider) * 1000000);
										}
										else
										{
											objdata._normalMax = (float)(nptag._onp.Max / _divider);
											objdata._normalMin = (float)(nptag._onp.Min / _divider);
										}
										StoreNormalsToDB(dest, objcond.Obj, objdata);
										bFind = true;
										Trace.WriteLine("NormsRequestClient " + Name + " set min max normals for : " + objdata.FullName);
									}
									else if(objdata != null
										&& (objdata._diagParamsPathExt == npNormPath || (npNormPath.StartsWith(objdata._diagParamsPathExt)
																				  && objdata._diagParamsPathExt.StartsWith(tmpSubs))))
									{
										if(npNormPath == "Izolyatsiya/Soprotivlenie")
										{   // в файле нормалей значения в кОм, в унификации - должны быть в Ом
											objdata._normalExt = (float)((nptag._onp.Max / _divider) * 1000);
										}
										else
										{
											objdata._normalExt = (float)(nptag._onp.Max / _divider);
										}
										StoreNormalsToDB(dest, objcond.Obj, objdata);
										bFind = true;
										Trace.WriteLine("NormsRequestClient " + Name + " set ext normal for : " + objdata.FullName);
									}
								}
							}
						}
					}
				}
				if(!bFind)
				{
					StreamWriter sw = new StreamWriter("неувязались" + dest + this.Name + ".txt", true, Encoding.Default);
					sw.WriteLine(nptag._objName + "\t" + nptag._group + "/" + nptag._task);
					sw.Flush();
					sw.Close();
				}
				// слив нормалей АДК в БД
				if(!adkNormsCleaned)
				{
					DropADKNormals(SiteID);
					adkNormsCleaned = true;
				}
				StoreADKNormalsToDB(SiteID, nptag);
			}
		}

		/// <summary>
		/// Перечень классов устройств, которые может перепутать адаптация
		/// </summary>
		protected string[] synonyms = new string[] { "Panel", "Batareya", "Luch", "MYE", "Strelochnyij_privod", "DYA" };

		/// <summary>
		/// Признак неоднозначного пути к нормалям
		/// </summary>
		/// <param name="path">Путь к нормалям</param>
		/// <returns>Признак неоднозначного пути к нормалям</returns>
		protected bool IsAmbiguousNormalPath(string path)
		{
			foreach(string s in synonyms)
				if(path.StartsWith(s))
					return true;
			return false;
		}
		/// <summary>
		/// Признак равнозначности путей к нормалям
		/// </summary>
		/// <param name="adk">Путь в АДК</param>
		/// <param name="uni">Путь в унификации</param>
		/// <returns>Признак равнозначности путей к нормалям</returns>
		protected bool AreEqualAmbiguousNormalPaths(string adk, string uni)
		{
			try
			{
				uni = uni.Split(new char[] { '/' })[1];
			}
			catch { }
			return adk == uni;
		}
		/// <summary>
		/// Сохранение нормалей в БД
		/// </summary>
		protected void StoreNormalsToDB(string dest, UniObject obj, UniObjData objdata)
		{
			// если подключены то записываем в базу
			if(_dbconnector.IsConnected)
			{
				int SiteID = 0;
				try
				{
					SiteID = _stageObjsMap.ContainsKey((int)obj._objSystem.SiteID)
						? _stageObjsMap[(int)obj._objSystem.SiteID][(objdata.Owner.Owner as UniObjectUpSt).Number]
						: (int)obj._objSystem.SiteID;

					SqlCommand cmd = new SqlCommand(string.Format("UPDATE PRJ_Params SET Normal_Max = @Max, Normal_Min = @Min, Normal_Ext = @Ext WHERE Site_ID = {0} AND Param_ID = {1}"
						, SiteID, objdata.ParamID), _dbconnector.SqlConn);
					cmd.Parameters.Add("@Max", SqlDbType.Real);
					cmd.Parameters["@Max"].Value = objdata._normalMax;
					cmd.Parameters.Add("@Min", SqlDbType.Real);
					cmd.Parameters["@Min"].Value = objdata._normalMin;
					cmd.Parameters.Add("@Ext", SqlDbType.Real);
					cmd.Parameters["@Ext"].Value = objdata._normalExt;
					cmd.ExecuteNonQuery();

					cmd = new SqlCommand(string.Format("UPDATE PRJ_Params SET [UpdateTime]=GETDATE() WHERE Site_ID = {0} AND Param_ID = {1}"
						, SiteID, objdata.ParamID), _dbconnector.SqlConn);
					cmd.ExecuteNonQuery();
				}
				catch(Exception ex)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
					SendState?.Invoke(this, "", SiteID, null, ex.Message);
				}

			}
		}
		/// <summary>
		/// Сохранение нормалей АДК в БД
		/// </summary>
		protected void StoreADKNormalsToDB(int Site_ID, NPTag np)
		{
			// если подключены то записываем в базу
			if(_dbconnector.IsConnected)
			{
				try
				{
					string normalPath = (np._group + "/" + np._task).Replace("'", "' + char(39) + '");
					string q = string.Format(@"INSERT INTO PRJ_ParamsADK ([Site_ID], [Object_ID], [Object_Name], [NormalPath], [Normal_Max], [Normal_Min], [UpdateTime]) 
                                                        VALUES ({0}, {1}, '{2}', '{3}', @Max, @Min, GETDATE())"
										, Site_ID, np._objid, np._objName, normalPath);
					SqlCommand cmd = new SqlCommand(q, _dbconnector.SqlConn);
					cmd.Parameters.Add("@Max", SqlDbType.Real);
					cmd.Parameters["@Max"].Value = np._onp.Max / _divider;
					cmd.Parameters.Add("@Min", SqlDbType.Real);
					cmd.Parameters["@Min"].Value = np._onp.Min / _divider;
					cmd.ExecuteNonQuery();
				}
				catch(Exception /*ex*/)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
				}
			}
		}
		/// <summary>
		/// Сохранение нормалей АДК в БД
		/// </summary>
		protected void DropADKNormals(int Site_ID)
		{
			// если подключены то записываем в базу
			if(_dbconnector.IsConnected)
			{
				try
				{
					SqlCommand cmd = new SqlCommand(string.Format("DELETE FROM PRJ_ParamsADK WHERE Site_ID = {0}", Site_ID), _dbconnector.SqlConn);
					cmd.ExecuteNonQuery();
				}
				catch(Exception /*ex*/)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
				}
			}
		}

		protected int GetSiteID(IDpsAssociations dps)
		{
			int SiteID = 0;
			if(dps is DpsOldControlObjSystem)
				SiteID = (int)((DpsOldControlObjSystem)dps).SiteID;
			else if(dps is DpsOldSubControlObjSystem)
				SiteID = (int)((UniObject)dps).Site.ID;
			return SiteID;
		}

		/// <summary>
		/// Инициализация норм значениями из БД
		/// </summary>
		/// <param name="SiteID">ID станции</param>
		/// <param name="ios">Подсистема объектов контроля</param>
		protected void InitNormals()
		{
			foreach(string dest in _destToObjSysMap.Keys)
			{
				int SiteID = GetSiteID(_destToObjSysMap[dest]);
				BaseIOSystem ios = _destToObjSysMap[dest] as BaseIOSystem;
				if(_firstRun && ios != null)
				{
					List<UniObjData> dbParams = LoadParamsFromDB(SiteID);

					foreach(DlxObject d in ios.SubElements.Objects)
					{
						UniObjectUpSt obj = d as UniObjectUpSt;
						if(obj != null)
							foreach(DlxObject dd in obj.ObjData)
							{
								UniObjData uod = dd as UniObjData;
								if(uod != null)
								{
									UniObjData db = dbParams.Where(u => u.ObjectID == obj.Number && u.ParamID == uod.ParamID).FirstOrDefault();
									if(db != null)
									{
										uod._normalMin = db._normalMin;
										uod._normalMax = db._normalMax;
										uod._normalExt = db._normalExt;
									}
								}
							}
					}
				}
			}
			_firstRun = false;
		}
		/// <summary>
		/// Загрузка униф.параметров станции
		/// </summary>
		/// <param name="SiteID">ID станции</param>
		/// <returns>Список униф.параметров станции</returns>
		protected List<UniObjData> LoadParamsFromDB(int SiteID)
		{
			List<UniObjData> res = new List<UniObjData>();
			if(_dbconnector.IsConnected)
			{
				try
				{
					string q = string.Format("select * from PRJ_Params where Site_ID={0}", SiteID);
					SqlCommand cmd = new SqlCommand(q, _dbconnector.SqlConn);
					SqlDataReader rdr = null;
					try
					{
						rdr = cmd.ExecuteReader();
						if(rdr.HasRows)
						{
							while(rdr.Read())
							{
								UniObjData u = new UniObjData(Convert.ToUInt32(rdr["Object_ID"]), Convert.ToUInt32(rdr["Param_ID"]), true);
								if(!rdr.IsDBNull(rdr.GetOrdinal("Normal_Min")))
									u._normalMin = Convert.ToSingle(rdr["Normal_Min"]);
								if(!rdr.IsDBNull(rdr.GetOrdinal("Normal_Max")))
									u._normalMax = Convert.ToSingle(rdr["Normal_Max"]);
								if(!rdr.IsDBNull(rdr.GetOrdinal("Normal_Ext")))
									u._normalExt = Convert.ToSingle(rdr["Normal_Ext"]);
								res.Add(u);
							}
						}
					}
					catch { }
					finally { if(rdr != null && !rdr.IsClosed) rdr.Close(); }
				}
				catch(Exception /*ex*/)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
				}
			}
			return res;
		}
		/// <summary>
		/// Загрузка соответствий для перегонных объектов
		/// </summary>
		/// <param name="StageID">ID перегона</param>
		/// <returns>Список униф.параметров станции</returns>
		protected Dictionary<int, int> LoadStageObjectsFromDB(int StageID)
		{
			Dictionary<int, int> res = new Dictionary<int, int>();
			if(_dbconnector.IsConnected)
			{
				try
				{
					string q = string.Format("select distinct Object_ID, Site_ID from PRJ_Objects where ISNULL(Site_ID_peregon, Site_ID)={0}", StageID);
					SqlCommand cmd = new SqlCommand(q, _dbconnector.SqlConn);
					SqlDataReader rdr = null;
					try
					{
						rdr = cmd.ExecuteReader();
						if(rdr.HasRows)
						{
							while(rdr.Read())
							{
								int objID = Convert.ToInt32(rdr["Object_ID"]);
								int siteID = Convert.ToInt32(rdr["Site_ID"]);
								res.Add(objID, siteID);

								if(!_stageObjsMap.ContainsKey(StageID))
									_stageObjsMap.Add(StageID, new Dictionary<int, int>());
								if(!_stageObjsMap[StageID].ContainsKey(objID))
									_stageObjsMap[StageID].Add(objID, siteID);
							}
						}
					}
					catch { }
					finally { if(rdr != null && !rdr.IsClosed) rdr.Close(); }
				}
				catch(Exception /*ex*/)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
				}
			}
			return res;
		}
	}
	/// <summary>
	/// Поток записи запроектированных диагностических состояний
	/// </summary>
	public class PrjDiagStateWriter : DlxObject
	{
		/// <summary>
		/// Объект соединения с БД
		/// </summary>
		protected DBConnector _dbconnector = new DBConnector();
		/// <summary>
		/// Список станций
		/// </summary>
		protected DlxCollection _siteList;
		/// <summary>
		/// Словарь диагностических ситуаций
		/// </summary>
		protected Dictionary<int, List<PrjDiag>> _diagPRJ = new Dictionary<int, List<PrjDiag>>();

		/// <summary>
		/// Одно соответствие
		/// </summary>
		protected class PrjDiag
		{
			/// <summary>
			/// Конструктор
			/// </summary>
			/// <param name="ObjID"></param>
			/// <param name="DiagID"></param>
			public PrjDiag(int ObjID, ushort DiagID)
			{
				UniObjID = ObjID;
				UniDiagID = DiagID;
			}
			/// <summary>
			/// Идентификатор объекта
			/// </summary>
			public int UniObjID;
			/// <summary>
			/// Идентификатор состояния
			/// </summary>
			public ushort UniDiagID;
		}
		/// <summary>
		/// Функциональная задача АДК
		/// </summary>
		protected class AdkTask
		{
			public readonly int ObjID = -1;
			public readonly string ObjName = string.Empty;
			public readonly string TaskName = string.Empty;
			public readonly long AdkGroupID = -1;
			public readonly long FaultID = -1;

			public AdkTask(int objID, string objName, string taskName, long groupID, long faultID)
			{
				ObjID = objID;
				ObjName = objName;
				TaskName = taskName;
				AdkGroupID = groupID;
				FaultID = faultID;
			}
			/// <summary>
			/// Загрузка списка задач объектов станции из БД
			/// </summary>
			/// <param name="connStr">Строка подключения</param>
			/// <returns></returns>
			public static List<AdkTask> LoadFromDB(string connStr, int site_id)
			{
				List<AdkTask> tasks = new List<AdkTask>();
				using(SqlConnection conn = new SqlConnection(connStr))
				{
					try
					{
						conn.Open();
						SqlCommand cmd = new SqlCommand("select obj_id, obj_name, task_name, adk_group_id, adk_fault_id from [txt_adk_tasks] where [site_id]=" + site_id, conn);
						SqlDataReader rdr = cmd.ExecuteReader();
						if(rdr.HasRows)
							while(rdr.Read())
							{
								tasks.Add(new AdkTask(Convert.ToInt32(rdr[0])
													, Convert.ToString(rdr[1])
													, Convert.ToString(rdr[2])
													, Convert.ToInt64(rdr[3])
													, Convert.ToInt64(rdr[4])));
							}
					}
					catch { }
				}
				return tasks;
			}
		}
		/// <summary>
		/// Таймер для выполнения записи
		/// </summary>
		protected Timer _writeTimer = null;
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_dbconnector.Load(Loader);
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxCollection;
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			if(!_dbconnector.IsConfigurated)
			{
				throw new ArgumentException("PrjDiagStateWriter: Не определены параметры подключения к БД");
			}

			foreach(DlxObject site in _siteList.Objects)
			{
				DlxObject obj = site.FindObject("Objects");
				if(obj == null)
					continue;
				DpsOldControlObjSystem objSys = obj as DpsOldControlObjSystem;
				if(objSys == null)
					continue;

				try
				{
					_diagPRJ[(int)objSys.SiteID] = GetDiagInfoList(objSys);
				}
				catch(Exception)
				{
					throw new ArgumentException("PrjDiagStateWriter - ошибка получения контролируемых ситуаций по станции " + objSys.Owner.Name);
				}
			}

			// заводим таймер
			//TimerCallback timerDelegate = new TimerCallback(this.OnTimer);
			//_writeTimer = new Timer(timerDelegate, null, 120000, 6000000);

			OnTimer(null);

			base.Create();
		}
		/// <summary>
		/// Добавить в список запроектированный диагноз
		/// </summary>
		/// <param name="prjDiagList"></param>
		/// <param name="UniObjID"></param>
		/// <param name="UniDiag"></param>
		protected void AddPrjDiagToList(List<PrjDiag> prjDiagList, int UniObjID, ushort UniDiag)
		{
			foreach(PrjDiag pd in prjDiagList)
			{
				if(pd.UniObjID == UniObjID && pd.UniDiagID == UniDiag)
					return;
			}

			prjDiagList.Add(new PrjDiag(UniObjID, UniDiag));
		}

		/// <summary>
		///  Получить список контролируемых диагностических ситуаций на станции
		/// </summary>
		/// <param name="ObjsOwner">Подсистема объектов</param>
		/// <returns></returns>
		protected List<PrjDiag> GetDiagInfoList(DpsOldControlObjSystem objSys)
		{
			string curDbName = _dbconnector.ConnectionString.Split(new string[] { "atalog=" }, StringSplitOptions.None)[1].Split(new char[] { ';' })[0];
			string tstDbName = "_tstdb";
			List<AdkTask> tasks = AdkTask.LoadFromDB(_dbconnector.ConnectionString.Replace(curDbName, tstDbName), (int)objSys.SiteID);

			List<PrjDiag> prjDiagList = new List<PrjDiag>();
			if(objSys._diagAssoc == null || objSys._objsAdktoUniAssoc == null
					|| !objSys._objsAdktoUniAssoc.HaveAssociations)
				return prjDiagList;

			AdkUniObjsAssociations objAssoc = objSys._objsAdktoUniAssoc;
			// проходим по всем ассоциациям объектов
			foreach(uint AdkID in objAssoc._objsAdkTOobjsUniMAP.Keys)
			{
				UniObjInfo oi = null;
				try
				{
					oi = objSys._objsAdktoUniAssoc._objsAdkTOobjsUniMAP[AdkID];
				}
				catch { }
				//uint AdkType = objAssoc.GetAdkType(AdkID);
				// получаем выявляемые сбои АДК для соответсвующего объекта
				var objTasks = tasks.Where(t => t.ObjID == AdkID);
				// проходим по всем выявляемым сбоям этого объекта АДК
				foreach(AdkTask t in objTasks)
				{
					uint AdkType = 0;
					try
					{ AdkType = Convert.ToUInt32(t.AdkGroupID); }
					catch { continue; }

					Dictionary<uint, UniDiagInfo> didic = new Dictionary<uint, UniDiagInfo>();
					try
					{   // получаем тип группы сбоев
						didic = objSys._diagAssoc._diagAdkTOdiagUniMAP[AdkType];
					}
					catch { }

					#region Простые соответствия
					// если ассоциация на унифицированный объект прямая
					if(oi.UniObjID != -1)
					{   // проверяем - есть ли соответствие для сбоя АДК
						ProcessTypedAssociations(didic, (uint)t.FaultID, prjDiagList, oi.UniObjID);
					}
					#endregion

					#region Соответствия с условиями (или вложенные xml-элементы)
					// если ассоциация не прямаю (с условиями или на несколько унифицированных объектов)
					if(oi.ObjConditions != null)
					{
						// цикл по всем условиям (связям)
						foreach(UniObjInfo.UniObjCondition oc in oi.ObjConditions)
						{
							if(!oc.ByDiag || oc.AdkDiag == t.FaultID)
							{   // если условие ассоциации не по типу сбоя, или он совпадает с проверемым типом сбоя АДК,
								// проверяем - есть ли соответствие для сбоя АДК
								ProcessTypedAssociations(didic, (uint)t.FaultID, prjDiagList, oc.UniObjID);
							}
						}
					}
					#endregion

				}
			}
			return prjDiagList;
		}
		/// <summary>
		/// Обработка типовых ассоциаций между сбоями АДК и унифицированными ДС
		/// </summary>
		/// <param name="diags">Список ДС</param>
		/// <param name="UniObjID">ID объекта</param>
		/// <param name="UniDiag">ID ДС</param>
		/// <param name="di"></param>
		protected void ProcessTypedAssociations(Dictionary<uint, UniDiagInfo> didic, uint adkDiag, List<PrjDiag> diags, int UniObjID)
		{
			UniDiagInfo di = null;
			if(didic.TryGetValue((uint)adkDiag, out di))
			{
				AddPrjDiagToList(diags, UniObjID, (ushort)di.UniDiag);
				if(di.DiagConditions != null)
				{
					foreach(UniDiagInfo.UniDiagCondition dc in di.DiagConditions)
						AddPrjDiagToList(diags, UniObjID, (ushort)dc.UniDiag);
				}
			}
		}
		/*/// <summary>
		/// Деинициализация
		/// </summary>
		public override void Destroy()
		{
			if (_writeTimer != null)
				_writeTimer.Dispose();
			base.Destroy();
		}*/

		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// </summary>
		public void OnTimer(Object stateInfo)
		{
			// если сконфигурировано подключение и мы отключены, то подключаемся
			if(!_dbconnector.IsConnected)
				_dbconnector.Connect();

			if(!_dbconnector.IsConnected)
				return;

			try
			{
				WritePrjDiagStates();

				_diagPRJ.Clear();
				_diagPRJ = null;
				if(_writeTimer != null)
					_writeTimer.Dispose();
				_dbconnector.Disconnect();
			}
			catch
			{
				return;
			}
		}
		/// <summary>
		/// Записать в базу контролируемые диагностические ситуации
		/// </summary>
		protected void WritePrjDiagStates()
		{
			foreach(int SiteID in _diagPRJ.Keys)
			{
				// удаляем контролируемые ситуации по станции
				SqlCommand sql = new SqlCommand();
				sql.Connection = _dbconnector.SqlConn;
				sql.CommandText = string.Format("DELETE FROM PRJ_DiagStates WHERE Site_ID={0};", SiteID);
				sql.ExecuteNonQuery();

				List<PrjDiag> siteDiags = _diagPRJ[SiteID];
				foreach(PrjDiag pd in siteDiags)
				{
					sql.CommandText = string.Format("INSERT INTO PRJ_DiagStates (Site_ID, Object_ID, DiagState_ID) VALUES ({0}, {1}, {2});", SiteID, pd.UniObjID, pd.UniDiagID);
					sql.ExecuteNonQuery();
				}
			}
		}
	}

	public class TdmNormsRequestClient : DlxObject, ISuivComponent
	{
		/// <summary>
		/// Фабрика соединений
		/// </summary>
		protected ConnectionFactory _connFactory = null;
		/// <summary>
		/// Сессия открытого соединения
		/// </summary>
		protected ClientSession _session = null;
		/// <summary>
		/// Сообщение для запроса
		/// </summary>
		protected Message _requestMsg = new Message();
		/// <summary>
		/// Частота запросов в миллисекундах
		/// </summary>
		protected int _rateQuery;
		protected DlxObject _siteList;

		/// <summary>
		/// Таймер для выполнения синхронизации
		/// </summary>
		protected Timer _syncTimer = null;

		protected Dictionary<string, string> _destToSiteMap = new Dictionary<string, string>();
		protected Dictionary<string, IDpsAssociations> _destToObjSysMap = new Dictionary<string, IDpsAssociations>();
		protected Dictionary<string, DateTime> _destToNPTime = new Dictionary<string, DateTime>();
		protected Dictionary<string, UniObjData> _sigaddrToObjData = new Dictionary<string, UniObjData>();

		protected DBConnector _dbconnector = new DBConnector();

		public event Action<DlxObject, string, int, DateTime?, string> SendState;
		public event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_dbconnector.Load(Loader);

			_connFactory = Loader.GetObjectFromAttribute("Connection") as ConnectionFactory;
			// считываем частоту запросов
			_rateQuery = Loader.GetAttributeInt("QueryRate", 3600) * 1000;  // 1 час
			_siteList = Loader.GetObjectFromAttribute("SiteList") as DlxObject;
			XmlElement cur = Loader.CurXmlElement;
			List<XmlElement> elements;
			Loader.CurXmlElement.EnumInhElements(out elements, "dest", null, null);
			string dest = string.Empty;
			string site = string.Empty;
			foreach(XmlElement elem in elements)
			{
				Loader.CurXmlElement = elem;
				if(elem.GetAttributeValue("name", ref dest)
					&& (elem.GetAttributeValue("Site", ref site)))
				{
					_destToSiteMap[dest] = site + "%" + elem.GetAttribute("IOSys") + "%" + elem.GetAttribute("Objects");
				}
				else
					throw new ArgumentException("MultiRequestClient: " + Owner.Name + ": Error to load item of dest association");
			}
		}

		public override void Create()
		{
			foreach(string dest in _destToSiteMap.Keys)
			{
				string[] path = _destToSiteMap[dest].Split('%');
				if(path[1].Length == 0)
					path[1] = "IOSys";
				if(path[2].Length == 0)
					path[2] = "Objects";
				BaseIOSystem objsys = (BaseIOSystem)_siteList.FindObject(path[0] + "/" + path[2]);
				var r = objsys.SubElements.Objects.Cast<UniObject>();
				foreach(var obj in r)
				{
					foreach(var objdata in obj.ObjData)
					{
						var sig = objdata as UniObjData;
						if(sig != null)
						{
							//_sigaddrToObjData.Add(sig.SignalReference, sig);
							_sigaddrToObjData[sig.SignalReference] = sig;
						}
					}
				}
			}

			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnTimer);
			_syncTimer = new Timer(timerDelegate, null, 60000, _rateQuery);
			base.Create();
		}

		protected int GetSiteID(string dest)
		{
			string siteName;
			try
			{
				siteName = _destToSiteMap[dest].Split('%').FirstOrDefault();
			}
			catch
			{
				siteName = dest;
			}

			foreach(Tdm.Site site in (_siteList as DlxCollection).Objects)
			{
				if(site.Name == siteName)
				{
					return site.ID;
				}
			}
			return 0;
		}

		protected void OpenSession()
		{
			try
			{
				_session = new ClientSession(_connFactory.CreateConnection());
				Trace.WriteLine("NormsRequestClient " + Name + ": OK to open connection!");
			}
			catch(Exception e)
			{
				_session = null;
				Trace.WriteLine("NormsRequestClient " + Name + " open client session exception : " + e.ToString());
				return;
			}
		}

		protected void StoreNormalsToDB(string dest, UniObject obj, UniObjData objdata)
		{
			StreamWriter sw = new StreamWriter("увязались" + dest + ".txt", true, Encoding.Default);
			sw.WriteLine(obj.Name + "\t" + objdata.Name + "\t" + objdata._diagParamsPath);
			sw.Flush();
			sw.Close();
			// если подключены то записываем в базу
			if(_dbconnector.IsConnected && (objdata._normalMax != float.MaxValue || objdata._normalMin != float.MaxValue || objdata._normalExt != float.MaxValue))
			{
				try
				{
					string updText = "[UpdateTime]=GETDATE()";
					SqlCommand cmd = new SqlCommand("", _dbconnector.SqlConn);
					if(objdata._normalMax != float.MaxValue)
					{
						updText += ", Normal_Max = @Max";
						cmd.Parameters.Add("@Max", SqlDbType.Real);
						cmd.Parameters["@Max"].Value = objdata._normalMax;
					}
					if(objdata._normalMin != float.MaxValue)
					{
						updText += ", Normal_Min = @Min";
						cmd.Parameters.Add("@Min", SqlDbType.Real);
						cmd.Parameters["@Min"].Value = objdata._normalMin;
					}
					if(objdata._normalExt != float.MaxValue)
					{
						updText += ", Normal_Ext = @Ext";
						cmd.Parameters.Add("@Ext", SqlDbType.Real);
						cmd.Parameters["@Ext"].Value = objdata._normalExt;
					}
					cmd.CommandText = string.Format("UPDATE PRJ_Params SET " + updText + " WHERE Site_ID = {0} AND Param_ID = {1}"
													, obj._objSystem.SiteID, objdata.ParamID);
					cmd.ExecuteNonQuery();
				}
				catch(Exception /*ex*/)
				{
					_dbconnector.Disconnect();
					_dbconnector.Connect();
				}
			}
		}

		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// </summary>
		public void OnTimer(Object stateInfo)
		{
			try
			{
				// если сконфигурировано подключение и мы отключены, то подключаемся
				if(_dbconnector.IsConfigurated && !_dbconnector.IsConnected)
					_dbconnector.Connect();

				foreach(string dest in _destToSiteMap.Keys)
				{
					var SiteID = GetSiteID(dest);

					_requestMsg.Code = RequestCode.CurrentState;
					_requestMsg.SubCode = 0;
					_requestMsg.Dest = dest;
					// цикл по требуемым станциям
					try
					{
						// открываем соединение, если оно закрыто
						if(_session == null)
							OpenSession();

						if(_session != null)
						{
							DiagParams curNormals = DoExchange();
							SetNormals(_destToSiteMap[dest], curNormals);
						}
						else
						{
							SendState?.Invoke(this, "", SiteID, null, "Не удалось установить соединение с сервером нормалей");
						}
					}
					catch(Exception ex)
					{
						// закрываем сессию
						_session.Close();
						_session = null;
						Trace.WriteLine("NormsRequestClient " + Name + " catch exception for " + dest + ": " + ex.ToString());
						SendState?.Invoke(this, "", SiteID, null, ex.Message);
					}
				}
				// если подключены к базе, то отключаемся
				if(_dbconnector.IsConnected)
					_dbconnector.Disconnect();
			}
			catch(Exception ex)
			{
				Trace.WriteLine("NormsRequestClient " + Name + " catch exception: " + ex.ToString());
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)301, "Ошибка на потоке запроса нормалей: " + ex.Message);
				Dps.AppDiag.EventLog.SysEventLog.WriteException(ex);
				SendAllSitesState?.Invoke(this, "", (_siteList as DlxCollection).Objects, null, ex.Message);
			}
		}

		/// <summary>
		/// Установить значения нормалей
		/// </summary>
		/// <param name="destString">Строка dest</param>
		/// <param name="curNormals">Текущие нормали</param>
		private void SetNormals(string destString, DiagParams curNormals)
		{
			string[] path = destString.Split('%');
			if(path[1].Length == 0)
				path[1] = "IOSys";
			if(path[2].Length == 0)
				path[2] = "Objects";
			BaseIOSystem objsys = (BaseIOSystem)_siteList.FindObject(path[0] + "/" + path[2]);
			DateTime lastTime = DateTime.MinValue;

			ProcessNormals(path[0], objsys, curNormals.Objects, ref lastTime);
			foreach(var tdmobj in curNormals.Objects)
			{   // ищем вложенные
				DiagParams el = tdmobj as DiagParams;
				if(el != null)
					ProcessNormals(path[0], objsys, el.Objects, ref lastTime);
			}
			SendState?.Invoke(this, "", GetSiteID(path[0]), lastTime, "");
		}

		private void ProcessNormals(string dest, BaseIOSystem objsys, List<DlxObject> objs, ref DateTime lastTime)
		{
			var r = objsys.SubElements.Objects.Cast<UniObject>();

			foreach(var tdmobj in objs)
			{
				// находим соотв. объект в подсистеме Objects (пока рассматриваем случай только с совпадающими именами)
				var uniobj = r.Where(e => e.Name == tdmobj.Name.Replace("|a", "-")).FirstOrDefault();
				if(uniobj == null)
					continue;
				foreach(var objdata in uniobj.ObjData.Where(o => o is UniObjData).Cast<UniObjData>())
				{
					var tdmdata = tdmobj.FindObject(objdata.Name);
					if(tdmdata == null)
					{
						tdmdata = tdmobj.FindObject(objdata.Name.Replace("/", "|a"));
						if(tdmdata == null)
						{
							tdmdata = tdmobj.FindObject(objdata.Name.Replace("-", "|a"));
							if(tdmdata == null)
								tdmdata = tdmobj.FindObject(objdata.Name.Replace("RизИМСИ", "Снижение сопротивления изоляции (отказ)"));
						}
					}

					if(tdmdata != null)
					{
						SetObjData(objdata, tdmdata);
						StoreNormalsToDB(dest, uniobj, objdata);

						if(objdata._lastSetParamTime > lastTime)
							lastTime = objdata._lastSetParamTime;
					}
				}
			}
		}

		/// <summary>
		/// Установить данные по нормалям
		/// </summary>
		/// <param name="objdata">Унифицированная objdata</param>
		/// <param name="tdmdata">Прочитанная objdata</param>
		private void SetObjData(UniObjData objdata, DlxObject tdmdata)
		{
			float N1 = 0;
			float N2 = 0;
			float N3 = 0;
			float N4 = 0;
			int norms = 0;
			var obj = tdmdata.FindObject("N1");

			if(obj != null)
			{   // ИВК-ТДМ
				norms++;
				N1 = Convert.ToSingle(obj.ToString());
				obj = tdmdata.FindObject("N2");
				if(obj != null)
				{
					norms++;
					N2 = Convert.ToSingle(obj.ToString());
					obj = tdmdata.FindObject("N3");
					if(obj != null)
					{
						norms++;
						N3 = Convert.ToSingle(obj.ToString());
						obj = tdmdata.FindObject("N4");
						if(obj != null)
						{
							N4 = Convert.ToSingle(obj.ToString());
							norms++;
						}
					}
				}
			}
			else
			{   // ЛПД
				obj = tdmdata.FindObject("N_min");
                if (obj != null)
                    objdata._normalMin = Convert.ToSingle((obj as Tdm.ObjData).Double);//Convert.ToSingle(obj.ToString());

 
                obj = tdmdata.FindObject("N_max");
				if(obj != null)
					objdata._normalMax = Convert.ToSingle((obj as Tdm.ObjData).Double);//Convert.ToSingle(obj.ToString());

                #region Особые случаи
                obj = tdmdata.FindObject("Nдень_min");
				if(obj != null)
					objdata._normalMin = Convert.ToSingle((obj as Tdm.ObjData).Double);

				obj = tdmdata.FindObject("Nдень_max");
				if(obj != null)
					objdata._normalMax = Convert.ToSingle((obj as Tdm.ObjData).Double);

				obj = tdmdata.FindObject("Снижение сопротивления изоляции (отказ)");
				if(obj != null)
					objdata._normalMin = Convert.ToSingle((obj as Tdm.ObjData).Double);
				#endregion

				FindSimilarNorm(objdata, tdmdata);

				return;
			}

			switch(norms)
			{
				case 1:
					{
						objdata._normalMin = N1;
					}
					break;
				case 2:
					{
						objdata._normalMin = N1;
						objdata._normalMax = N2;
					}
					break;
				case 3:
					{
						objdata._normalExt = N1;
						objdata._normalMin = N2;
						objdata._normalMax = N3;
					}
					break;
				case 4:
					{
						objdata._normalMin = N1;
						objdata._normalMax = N4;
					}
					break;
				default:
					break;
			}
		}

		protected void FindSimilarNorm(UniObjData objdata, DlxObject tdmdata)
		{
			List<string> NextNorms = new List<string> { "день", "гарант", "код_цикл_515", "код_цикл_715", "ТД", "фр", "UсРТД", "UпРТД", "Uп.зан.РЦ", "Uп.св.РЦ", "вх_защ_ф", "UсИПД", "UзИПД", "ост", "In", "Ожидаемый часовой пояс", "Временной порог", "Максимально допустимая погрешность времени", "Допустимый логический уровень источника синхронизации", "Ожидаемый идентификатор источника", "Uпер.", "Uнач.", "Out" };

			DlxCollection dc = tdmdata as DlxCollection;
			if(dc != null)
			{
				foreach(DlxObject o in dc.Objects)
					if(NextNorms.Count(n => o.Name.Contains(n)) > 0)
					{
						objdata._normalExt = Convert.ToSingle((o as Tdm.ObjData).Double);
						return;
					}
			}
		}

		DiagParams DoExchange()
		{
			_session.SendMessage(_requestMsg);
			var answerMsg = _session.ReceiveMessage();
			if(answerMsg.Data == null)
				throw new FileLoadException("NormsRequestClient: Ошибка получения информации о файле от промышленного компьютера");
			if(answerMsg.Code == RequestCode.Error)
				throw new Exception("У сервера нормалей нет данных по станции");

			var ms = new MemoryStream(answerMsg.Data);
			var reader = new BinaryReader(ms);
			var prms = DiagParams.LoadParams(reader);
			return prms;
		}

		/*public override void Destroy()
        {
            _syncTimer.Dispose();
            base.Destroy();
        }*/

	}
}
