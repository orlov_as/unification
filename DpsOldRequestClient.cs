﻿// DpsOldRequestClient.cs  : получение данных от Dps старой версии
// Создан: 16/3/2007
// Copyright (c) 2007 НПП ЮгПромАвтоматизация Прищепа М.В.
/////////////////////////////////////////////////////////////////////////////
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using AfxEx;
using AfxEx.Net;
using Ivk.IOSys;
using Tdm;
using Dps.UniRequest;
using Dps.UniTcpIp;

namespace Tdm.DpsOld
{
	/// <summary>
	/// Класс для синхронизации потоков запросов к станциям
	/// Инициируется при обработке тега SyncObject 
	/// </summary>
	class DpsOldRequestSync : DlxObject
	{
		private System.Timers.Timer syncTimer;

		private int _rateQuery;
		private int _timeOut;

		protected Dictionary<string, AutoResetEvent> syncEvents = new Dictionary<string, AutoResetEvent>();

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_rateQuery = Loader.GetAttributeInt("QueryRate", 2800);
			_timeOut = Loader.GetAttributeInt("WaitTimeOut", 3000);
		}

		/// <summary>
		/// Старт таймера после запуска всех потоков для станций
		/// </summary>
		public void AddClient(string dest)
		{
			if(!syncEvents.ContainsKey(dest))
				syncEvents.Add(dest, new AutoResetEvent(false));
			//            if (syncEvents[dest] == null)
			//                syncEvents[dest] = new AutoResetEvent(false);
		}

		/// <summary>
		/// Инициализация таймера
		/// </summary>
		public override void Create()
		{
			syncTimer = new System.Timers.Timer();
			syncTimer.Interval = _rateQuery;
			syncTimer.Elapsed += OnTimedEvent;
			syncTimer.Start();
		}

		/// <summary>
		/// Таймер устанавливает дискрипторы ожидания для всех потоков (станций) 
		/// </summary>
		private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
		{
			foreach(KeyValuePair<string, AutoResetEvent> syncEvent in syncEvents)
				syncEvent.Value.Set();
		}

		/// <summary>
		/// Ожидание установки дискриптор ожидания для потока (станции)
		/// </summary>
		public bool waitOne(string dest)
		{
			return syncEvents[dest].WaitOne(_timeOut);
		}

	}

	/// <summary>
	/// Переопределен заголовк сообщения для использования при обмене со старыми приложениями
	/// </summary>
	class OldHeader : Header
	{
		/// <summary>
		/// Конструтор по-умолчанию
		/// </summary>
		public OldHeader() { }
		/// <summary>
		/// Конструктор с параметрами
		/// </summary>
		/// <param name="Msg">Сообщение</param>
		/// <param name="sDest">Имя получателя</param>
		/// <param name="uId">Идентификатор</param>
		public OldHeader(Message Msg, string sDest, UInt32 uId) : base(Msg, sDest, uId) { }
		/// <summary>
		/// Упаковка заголовка в старом формате 
		/// </summary>
		/// <param name="Wr">поток записи</param>
		public void SerializeOld(BinaryWriter Wr)
		{
			// проверяем поток
			if(Wr == null)
				throw new ArgumentNullException("BinaryWriter");
			// записываем стртовые два байта
			UInt16 StartID = 0xA55A;
			Wr.Write(StartID);
			// пишем код запроса
			Wr.Write(m_Code);
			// проверяем размер
			if(m_Size > UInt16.MaxValue)
				throw new ArgumentOutOfRangeException("m_Size");
			// приводим к 2-м байтам
			UInt16 Size16 = (UInt16)m_Size;
			// пишем размер
			Wr.Write(Size16);
			// пишем счетчик
			Wr.Write(m_ID);
			// конвертируем Dest
			byte[] DestArray = null;
			if(m_Dest.Length > 0)
				DestArray = System.Text.Encoding.UTF8.GetBytes(m_Dest);
			else
				DestArray = new byte[0];

			if(DestArray.Length < 32)
			{
				// пишем Dest
				Wr.Write(DestArray);
				// пишем ноль
				byte[] addbuf = new byte[32 - DestArray.Length];
				addbuf[0] = 0;
				Wr.Write(addbuf);
			}
			else
			{
				// пишем Dest
				Wr.Write(DestArray, 0, 32);
			}
		}
		/// <summary>
		/// Распаковка заголовка в старом формате
		/// </summary>
		/// <param name="Rd">поток чтения</param>
		public void DeserializeOld(BinaryReader Rd)
		{
			// проверяем поток
			if(Rd == null)
				throw new ArgumentNullException("BinaryReader");

			// Читаем стртовые два байта
			UInt16 StartID = Rd.ReadUInt16();
			if(StartID != 0xA55A)
				throw new ArgumentException("StartID != 0xA55A", "StartID");

			// читаем код
			m_Code = Rd.ReadByte();
			// устанавливаем дополнительный код в ноль
			m_SubCode = 0;
			// читаем размер
			m_Size = Rd.ReadUInt16();
			// читаем счетчик
			m_ID = Rd.ReadUInt32();
			// устанавливаем без флагов
			m_Flags = 0;
			// размер Dest 32 байта
			int DestLen = 32;

			m_Dest = string.Empty;
			// читаем 32 байта в массив
			byte[] DestArray = Rd.ReadBytes(DestLen);
			// если меньше прочитали - исключение
			if(DestArray.Length < 32)
				throw new EndOfStreamException();
			// находим символ конца строки
			for(int i = 0; i < DestArray.Length; i++)
			{
				if(DestArray[i] == 0)
					DestLen = i;
			}
			// преобразуем к строке
			m_Dest = System.Text.Encoding.UTF8.GetString(DestArray, 0, DestLen);
		}

		/// <summary>
		/// Упаковка заголовка в старом формате 
		/// </summary>
		/// <param name="Wr">поток записи</param>
		/// <param name="Hd">заголовок</param>
		static public void SerializeOld(Header Hd, BinaryWriter Wr)
		{
			// проверяем поток
			if(Wr == null)
				throw new ArgumentNullException("BinaryWriter");
			// записываем стртовые два байта
			UInt16 StartID = 0xA55A;
			Wr.Write(StartID);
			// пишем код запроса
			byte Code = (byte)Hd.Code;
			Wr.Write(Code);
			// проверяем размер
			if(Hd.Size > UInt16.MaxValue)
				throw new ArgumentOutOfRangeException("m_Size");
			// приводим к 2-м байтам
			UInt16 Size16 = (UInt16)Hd.Size;
			// пишем размер
			Wr.Write(Size16);
			// пишем счетчик
			Wr.Write(Hd.ID);
			// конвертируем Dest
			byte[] DestArray = null;
			if(Hd.Dest.Length > 0)
				DestArray = System.Text.Encoding.UTF8.GetBytes(Hd.Dest);
			else
				DestArray = new byte[0];

			if(DestArray.Length < 32)
			{
				// пишем Dest
				Wr.Write(DestArray);
				// пишем ноль
				byte[] addbuf = new byte[32 - DestArray.Length];
				addbuf[0] = 0;
				Wr.Write(addbuf);
			}
			else
			{
				// пишем Dest
				Wr.Write(DestArray, 0, 32);
			}
		}
		/// <summary>
		/// Распаковка заголовка в старом формате
		/// </summary>
		/// <param name="Rd">поток чтения</param>
		/// <param name="Hd">заголовок</param>
		static public void DeserializeOld(Header Hd, BinaryReader Rd)
		{
			// проверяем поток
			if(Rd == null)
				throw new ArgumentNullException("BinaryReader");

			// Читаем стртовые два байта
			UInt16 StartID = Rd.ReadUInt16();
			if(StartID != 0xA55A)
				throw new ArgumentException("StartID != 0xA55A", "StartID");

			// читаем код
			byte Code = Rd.ReadByte();
			Hd.Code = (RequestCode)Code;
			// устанавливаем дополнительный код в ноль
			Hd.SubCode = 0;
			// читаем размер
			UInt16 Size = Rd.ReadUInt16();
			Hd.Size = Size;
			// читаем счетчик
			UInt32 ID = Rd.ReadUInt32();
			Hd.ID = ID;
			// устанавливаем без флагов
			Hd.Flags = (RequestFlags)0;
			// размер Dest 32 байта
			int DestLen = 32;

			Hd.Dest = string.Empty;
			// читаем 32 байта в массив
			byte[] DestArray = Rd.ReadBytes(DestLen);
			// если меньше прочитали - исключение
			if(DestArray.Length < 32)
				throw new EndOfStreamException();
			// находим символ конца строки
			for(int i = 0; i < DestArray.Length; i++)
			{
				if(DestArray[i] == 0)
					DestLen = i;
			}
			// преобразуем к строке
			Hd.Dest = System.Text.Encoding.UTF8.GetString(DestArray, 0, DestLen);
		}
	}

	/// <summary>
	/// Класс соединения по TcpIp с использованием старого заголовка
	/// </summary>
	class TcpConnectionDpsOld : TcpConnection
	{
		public TcpConnectionDpsOld(Socket sock) : base(sock, false) { }

		public TcpConnectionDpsOld(Socket sock, bool bSporadic) : base(sock, false) { _bSporadic = bSporadic; }

		/// <summary>
		/// Переопределен метод для отправки старого заголовка
		/// </summary>
		/// <param name="Hd">заголовк в старом формате (должен приводится к типу OldHeader)</param>
		protected override void OnSendHeader(Header Hd)
		{
			if(Hd == null)
				throw new ArgumentNullException("Header");
			if(_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			//_Stream.SetTotalTimeout(_Timeout);
			_Stream.WriteTimeout = _Timeout;
			OldHeader.SerializeOld(Hd, Writer);

			_lastSendedID = Hd.ID;
		}
		/// <summary>
		/// Переопределен метод для получения заголовка в старом формате
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if(_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();

			//Установка глобального taimout
			//if (IsPartMessage || (Reader == null))
			//_Stream.SetTotalTimeout(_Timeout);
			_Stream.ReadTimeout = _Timeout;

			OldHeader.DeserializeOld(Hd, Reader);

			if(_bSporadic)
			{
				Hd.Flags |= RequestFlags.PartMsg;
				Hd.ID = _lastSendedID;
			}

			return Hd;
		}

		protected bool _bSporadic = false;
		protected uint _lastSendedID = 0;
	}

	/// <summary>
	/// Класс фабрики клиентских Tcp соединений с использованием старого заголовка
	/// </summary>
	public class ClientDpsOld : Client
	{
		/// <summary>
		/// Переопределен метод создания соединения для создания клиентского соединения с использованием старого заголовка
		/// </summary>
		/// <returns>Соединение</returns>
		public override AConnection CreateConnection()
		{
			Socket sock = SocketHelper.Connect(Host, Port);
			sock.ReceiveTimeout = _ReceiveTimeout;
			sock.SendTimeout = _SendTimeout;
			TcpConnectionDpsOld conn = new TcpConnectionDpsOld(sock, _bSporadic);
			conn.Timeout = _ReceiveTimeout;
			return conn;
		}
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_bSporadic = Loader.GetAttributeBool("Sporadic", false);
			_ReceiveTimeout = Loader.GetAttributeInt("ReceiveTimeout", _ReceiveTimeout);
			_SendTimeout = Loader.GetAttributeInt("SendTimeout", _SendTimeout);
		}
		/// <summary>
		/// Признак спорадического режима обмена
		/// </summary>
		protected bool _bSporadic = false;
		protected int _ReceiveTimeout = 1000 * 60;
		protected int _SendTimeout = 1000 * 5;
	}

	/// <summary>
	/// Клиент получения данных от старой системы
	/// </summary>
	class DpsOldRequestClient : RequestClientBase
	{
		#region Методы работы на потоке

		public override bool OnInitThread()
		{
			Thread.Sleep(20000);
			return base.OnInitThread();
		}
		/// <summary>
		/// Подготовка запроса
		/// </summary>
		protected override void PrepareRequest()
		{
			_requestMsg.Code = RequestCode.CurrentState;
			_requestMsg.Dest = _dest;
		}
		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected override void ProcessAnswer()
		{

			if(_answerMsg.Data.Length == 0 || _answerMsg.Code != _requestMsg.Code || _answerMsg.SubCode != _requestMsg.SubCode)
			{
				string message = null;
				if(_answerMsg.Code == RequestCode.Error)
				{
					message = "Ошибка при запросе данных у DpsServer";
				}

				if(_targetIOsys != null)
					SetData(message, DateTime.MinValue, _targetIOsys);
				SetData(message, DateTime.MinValue, _targetObjects);

				Trace.WriteLine("DpsOldRequestClient " + Name + ": Получен ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetObjects.Owner.Name + ": размер данных = " + _answerMsg.Data.Length);
				//LogIOSystemRecieveTime(this.Name, DateTime.Now, "Empty");
				return;
			}
			try
			{
				// расчитываем размер предназначенный для подсистемы ввода
				int LinesCount = _answerMsg.Data[0];
				int offset = 1;
				DateTime lineTime = DateTime.MinValue;
				DateTime maxTime = DateTime.MinValue;

				for(int i = 0; i < LinesCount; i++)
				{
					lineTime = GetLineTime(_answerMsg.Data, offset);
					if(lineTime > maxTime)
						maxTime = lineTime;
					offset = GetNextLineDataOffset(_answerMsg.Data, offset);
				}

				if(maxTime == DateTime.MinValue)
				{
					Trace.WriteLine("DpsOldRequestClient " + Name + ": Получен ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetObjects.Owner.Name + ": время равно нулю!!!");
					//LogIOSystemRecieveTime(this.Name, DateTime.Now, "MinValue");

					//SetData(null, DateTime.MinValue, _targetIOsys);
					//SetData(null, DateTime.MinValue, _targetObjects);
					//return;
					maxTime = DateTime.Now;
				}

				if((((TimeSpan)(DateTime.Now - maxTime)).Hours > 0))
					maxTime = maxTime.ToLocalTime();

				// проверяем, что время не повторяется
				DateTime lastTime;
				if(!_lastElementTime.TryGetValue(_targetObjects, out lastTime) || lastTime < maxTime)
				{
					_lastElementTime[_targetObjects] = maxTime;
					if(_elementTimeNotChangedCount.ContainsKey(_targetObjects) && _elementTimeNotChangedCount[_targetObjects] > 30)
					{   // если связь восстановилась после полутора минут (30х3) или более - добавляем сообщение
						Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info
							, (Dps.AppDiag.EventLogID)302
							, string.Format("Возобновилось получение данных ({0})", _targetObjects.Owner.Name));
					}
					_elementTimeNotChangedCount[_targetObjects] = 0;
				}
				else
				{
					_elementTimeNotChangedCount[_targetObjects]++;
					// если время не менялось более 30 раз, то неизвестное состояние
					if(_elementTimeNotChangedCount[_targetObjects] > 30)
					{
						if(_targetIOsys != null)
							SetData(null, maxTime, _targetIOsys);
						SetData(null, maxTime, _targetObjects);
						// добавлено сообщение о пропадании связи
						if(_elementTimeNotChangedCount[_targetObjects] == 31)
							Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info
								, (Dps.AppDiag.EventLogID)302
								, string.Format("Прекратилось получение данных ({0})", _targetObjects.Owner.Name));

						return;
					}
				}

				Trace.WriteLine("DpsOldRequestClient " + Name + ": Получен ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetObjects.Owner.Name + ": time = " + maxTime.ToString("dd.MM.yyyy HH:mm:ss") + "размер данных = " + _answerMsg.Data.Length);
				//LogIOSystemRecieveTime(this.Name, DateTime.Now, "Data");

				// выделяем память
				byte[] subData = new byte[offset];
				// копируем
				Array.Copy(_answerMsg.Data, subData, offset);
				// передаем данные коллектору
				if(_targetIOsys != null)
					SetData(subData, maxTime, _targetIOsys);

				if(offset != _answerMsg.Data.Length)
				{
					// выделяем память под данные диагностики
					subData = new byte[_answerMsg.Data.Length - offset];
					// копируем
					Array.Copy(_answerMsg.Data, offset, subData, 0, _answerMsg.Data.Length - offset);
					// передаем данные коллектору
					SetData(subData, maxTime, _targetObjects);
				}
			}
			catch(Exception ex)
			{
				Trace.WriteLine("DpsOldRequestClient " + Name + ": ошибка обработки данных для " + _targetObjects.Owner.Name + ": " + ex.Message);
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)302, Name + ": ошибка обработки данных для " + _targetObjects.Owner.Name + ": " + ex.Message + "\nSTACK:\n" + ex.StackTrace);
				// произошла ошибка обработки - пустые данные коллектору
				if(_targetIOsys != null)
					SetData(null, DateTime.MinValue, _targetIOsys);
				SetData(null, DateTime.MinValue, _targetObjects);
			}
		}
		/// <summary>
		/// Запись в лог-файл полученных сбоев АДК
		/// </summary>
		/// <param name="faults"></param>
		protected void LogIOSystemRecieveTime(string siteName, DateTime t, string s)
		{
			string fName = string.Format("_IOSystem_{0}.txt", siteName);
			System.IO.FileInfo fi = new System.IO.FileInfo(fName);
			System.IO.StreamWriter sw = new System.IO.StreamWriter(fName, true);
			sw.WriteLine(t.ToString() + "\t" + s);
			sw.Flush();
			sw.Close();
		}
		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected override void OnExchengeError()
		{
			// произошла ошибка - пустые данные коллектору
			if(_targetIOsys != null)
				SetData(null, DateTime.MinValue, _targetIOsys);
			SetData(null, DateTime.MinValue, _targetObjects);
		}

		protected override void OnWrongAnswerCode()
		{
		}
		#endregion

		/// <summary>
		/// Переопределена загрузка для получения фабрики соединений
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_onlydiag = Loader.GetAttributeBool("onlydiags", false);

			if(_findRoot != null)
			{
				if(!_onlydiag)// загружаем ссылку на подсистему ввода
					_iosysPath = Loader.GetAttributeString("IOSys", string.Empty);
				// загружаем ссылку на объекты
				_objectsPath = Loader.GetAttributeString("Objects", string.Empty);
			}
			else
			{
				// загружаем ссылку на подсистему ввода
				if(!_onlydiag)
					_targetIOsys = Loader.GetObjectFromAttribute("IOSys") as IOBaseElement;
				// загружаем ссылку на объекты
				_targetObjects = Loader.GetObjectFromAttribute("Objects") as IOBaseElement;
			}

		}
		/// <summary>
		/// Переопределено для поиска фабрики соединений, подсистемы ввода и подсистемы объектов
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if(!_onlydiag)
			{
				if(_targetIOsys == null && _findRoot != null)
					_targetIOsys = _findRoot.FindObject(_iosysPath) as IOBaseElement;


				if(_targetIOsys == null)
				{
					throw new ArgumentException("DpsOldRequestClient " + Name + ": Не найдена подсистема ввода по значению атрибута IOSys = " + _iosysPath);
				}
			}

			if(_targetObjects == null && _findRoot != null)
				_targetObjects = _findRoot.FindObject(_objectsPath) as IOBaseElement;

			if(_targetObjects == null)
			{
				throw new ArgumentException("DpsOldRequestClient " + Name + ": Не найдена подсистема ввода по значению атрибута IOSys = " + _iosysPath);
			}

			_iosysPath = string.Empty;
			_objectsPath = string.Empty;

			base.Create();
		}

		#region Данные

		/// <summary>
		/// Элемент подсистемы ввода
		/// </summary>
		protected IOBaseElement _targetIOsys;
		/// <summary>
		/// Элемент объектов
		/// </summary>
		protected IOBaseElement _targetObjects;
		/// <summary>
		/// Путь от корня для поиска подсистемы ввода
		/// </summary>
		protected string _iosysPath = string.Empty;
		/// <summary>
		/// Путь от корня для поиска подсистемы объектов
		/// </summary>
		protected string _objectsPath = string.Empty;
		/// <summary>
		/// только диагностика
		/// </summary>
		protected bool _onlydiag;

		protected Dictionary<IOBaseElement, DateTime> _lastElementTime = new Dictionary<IOBaseElement, DateTime>();
		protected Dictionary<IOBaseElement, int> _elementTimeNotChangedCount = new Dictionary<IOBaseElement, int>();

		#endregion

		/// <summary>
		/// метод получения смещения до данных следующей линии ЦБС
		/// </summary>
		/// <param name="data">данные сообщения</param>
		/// <param name="curpos">позиция начала данных линии</param>
		/// <returns>позиция данных следующей линии</returns>
		protected static int GetNextLineDataOffset(byte[] data, int curpos)
		{
			if(curpos + 3 < data.Length)
				return curpos + (data[curpos + 2] | (data[curpos + 3] << 8));
			return
				data.Length;
		}

		/// <summary>
		/// метод получения времени линии ЦБС
		/// </summary>
		/// <param name="data">данные сообщения</param>
		/// <param name="curpos">позиция начала данных линии</param>
		/// <returns>время данных линии</returns>
		protected static DateTime GetLineTime(byte[] data, int curpos)
		{
			if((curpos + 10) > (data.Length - 1))
				return DateTime.MinValue;

			try
			{
				int time = (data[curpos + 5] | (data[curpos + 6] << 8) | (data[curpos + 7] << 16) | (data[curpos + 8] << 24));
				UInt16 msec = (UInt16)(data[curpos + 9] | (data[curpos + 10] << 8));
				DateTime dateTime = Afx.CTime.ToDateTime(time);
				dateTime.AddMilliseconds(msec);
				return dateTime;
			}
			catch
			{
				return DateTime.MinValue;
			}
		}
	}

	class DpsOldMultiRequestClient : DpsOldRequestClient
	{
		protected Dictionary<string, string> _destToSiteMap = new Dictionary<string, string>();
		protected List<IOBaseElement> _listIOSys = new List<IOBaseElement>();
		protected List<IOBaseElement> _listObjects = new List<IOBaseElement>();
		protected List<string> _listDest = new List<string>();
		protected int _curSiteIndex = 0;
		protected int _rateQueryInitial = 2000;

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_rateQueryInitial = _rateQuery;

			List<XmlElement> Elemets;
			Loader.CurXmlElement.EnumInhElements(out Elemets, "dest", null, null);
			string dest = string.Empty;
			string Site = string.Empty;
			foreach(XmlElement elem in Elemets)
			{
				Loader.CurXmlElement = elem;
				if(elem.GetAttributeValue("name", ref dest)
					&& (elem.GetAttributeValue("Site", ref Site)))
				{
					_destToSiteMap[dest] = Site + "%" + elem.GetAttribute("IOSys") + "%" + elem.GetAttribute("Objects");
				}
				else
					throw new ArgumentException("DpsOldMultiRequestClient: " + Owner.Name + ": Error to load item of dest association");
			}
		}

		public override void Create()
		{
			foreach(KeyValuePair<string, string> keyv in _destToSiteMap)
			{
				string[] path = keyv.Value.Split('%');
				if(path[1].Length == 0)
					path[1] = "IOSys";
				if(path[2].Length == 0)
					path[2] = "Objects";

				if(!_onlydiag)
				{
					IOBaseElement IOSys = _findRoot.FindObject(path[0] + "/" + path[1]) as IOBaseElement;
					if(IOSys == null)
						throw new ArgumentException("DpsOldMultiRequestClient: " + Owner.Name + ": Error to load item of dest association");
					_listIOSys.Add(IOSys);
				}
				else
					_listIOSys.Add(null);

				IOBaseElement ObjSys = _findRoot.FindObject(path[0] + "/" + path[2]) as IOBaseElement;
				if(ObjSys == null)
					throw new ArgumentException("DpsOldMultiRequestClient: " + Owner.Name + ": Error to load item of dest association");
				_listObjects.Add(ObjSys);

				_listDest.Add(keyv.Key);
			}

			if(_listDest.Count > 0)
			{
				_targetIOsys = _listIOSys[0];
				_targetObjects = _listObjects[0];
				_dest = _listDest[0];
			}

			base.Create();
		}

		protected override void ProcessAnswer()
		{
			base.ProcessAnswer();

			ToNextSite();
		}

		protected override void OnExchengeError()
		{
			base.OnExchengeError();

			/*/ произошла ошибка обмена - пустые данные коллектору по всем станциям
			foreach( IOBaseElement iosys in _listIOSys)
				SetData(null, DateTime.MinValue, iosys);

			foreach (IOBaseElement objsys in _listObjects)
				SetData(null, DateTime.MinValue, objsys);*/

			ToNextSite();
		}

		protected void ToNextSite()
		{
			// переходим к следующей станции
			_curSiteIndex++;
			if(_curSiteIndex >= _listDest.Count)
			{
				_curSiteIndex = 0;
				_rateQuery = _rateQueryInitial;
			}
			else
				_rateQuery = 0;

			if(_listDest.Count > 0)
			{
				_targetIOsys = _listIOSys[_curSiteIndex];
				_targetObjects = _listObjects[_curSiteIndex];
				_dest = _listDest[_curSiteIndex];
			}
			// изменяем запрос
			PrepareRequest();
		}
	}

	/// <summary>
	/// Клиент получения данных от старой системы в сихронизированном потоке
	/// Инициируется при обработке тега Client в теге Concentrator
	/// В методе Load регистрируется в элементе синхронизации DpsOldRequestSync
	/// </summary>
	class DpsOldRequestClientThread : DpsOldRequestClient
	{
		#region Методы работы на потоке

		/// <summary>
		/// Определение основного метода работы потока
		/// </summary>
		protected override void Run()
		{
			DateTime prevTime = DateTime.Now;
			DateTime currTime = DateTime.Now;
			// открываем сессию
			if(IsNeedRequest())
			{
				Trace.WriteLine("RequestClientBase " + Name + ": Try to open connection first...");
				OpenResource();
			}
			// счетчик полученных сообщений в рамках одного соединения
			_recieveMsgCounter = 0;
			// Всегда
			while(true)
			{
				//string s = string.Format("Start at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);
				try
				{
					// если есть сессия
					if(_session != null)
					{
						//                        if ((!_answerMsg.PartMessage) && (_syncObject.waitOne(_dest)))
						if(_syncObject.waitOne(_dest))
						{
							prevTime = currTime;
							currTime = DateTime.Now;
							// отправляем запрос
							if(!_answerMsg.PartMessage)
							{
								// обновляем таймаут на отправку
								UpdateSessionTimeout(false);
								// получаем сообщение
								_session.SendMessage(_requestMsg);

								/////////////////////////////////////
								//Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)42, "ClientBase sent message");
								/////////////////////////////////////
								//s += string.Format("Sent at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);

								// сообщение отправлено
								OnMessageSended();
							}

							// обновляем таймаут на получение
							UpdateSessionTimeout(true);
							// получаем ответ
							_answerMsg = _session.ReceiveMessage();
							if(_answerMsg.Code != _requestMsg.Code || _answerMsg.SubCode != _requestMsg.SubCode)
								OnWrongAnswerCode();
							// инкрементируем счетчик
							_recieveMsgCounter++;
							//s += string.Format("Rec at \t{0}.{1} ({2})\n", DateTime.Now, DateTime.Now.Millisecond, _recieveMsgCounter);

							if(Convert.ToInt32((currTime - prevTime).TotalMilliseconds) > 3000)
								Trace.WriteLine("Контроль периода в 3 секунды : " + _dest + " -- prev-curr -- " + prevTime.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + " ---- " + currTime.ToString("yyyy-MM-dd hh:mm:ss.fff tt"));

							// если нужно запрашивать, то вызываем обработку ответа
							if(IsNeedRequest())
								ProcessAnswer();
							else // иначе закрываем сессию
								_session = null;
						}
					}
				}
				catch(ThreadInterruptedException)
				{
					return;
				}
				catch(ThreadAbortException)
				{
					return;
				}
				/*catch (TimeoutException e)
                {
                    Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)102, "Исключение на потоке клиента " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
                    // произошла ошибка - пустые данные коллектору
                    OnExchengeError();
                }*/
				catch(Exception e)
				{
					//Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)102, "Исключение на потоке клиента " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					// произошла ошибка - пустые данные коллектору
					OnExchengeError();
					// закрываем сессию
					_session.Close();
					_session = null;

					Trace.WriteLine("RequestClientBase " + Name + " catch exception : " + e.ToString());
				}
				// если нужно перезапустить обмен
				if(_bRestart && _session != null)
				{
					_session.Close();
					_session = null;
				}
				// в любом случае проверяем сесию
				if(_session == null)
				{
					// если нет полученных сообщений и не требуется перезапуск, то выдерживаем время
					if(_recieveMsgCounter < 1 && !_bRestart)
						_restartEvent.WaitOne(_rateOpen, false);

					_recieveMsgCounter = 0;
					_bRestart = false;
					// открываем сессию
					if(IsNeedRequest())
					{
						Trace.WriteLine("RequestClientBase " + Name + ": Try to reopen connection...");
						OpenResource();
					}
				}
				//LogRequestRun(Name, s);
			}
		}

		#endregion

		/// <summary>
		/// Переопределена загрузка для получения фабрики соединений
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_dest = Loader.GetAttributeString("Dest", string.Empty);

			_syncObject = Loader.GetObjectFromAttribute("SyncObject") as DpsOldRequestSync;
			if(_syncObject != null)
				_syncObject.AddClient(_dest);
		}

		#region Данные

		/// <summary>
		/// Элемент синхронизации выполнения потока
		/// </summary>
		protected DpsOldRequestSync _syncObject;

		#endregion
	}
}
