﻿// DpsOldRequestClient.cs  : получение данных от Dps старой версии
// Создан: 16/3/2007
// Copyright (c) 2007 НПП ЮгПромАвтоматизация Прищепа М.В.
/////////////////////////////////////////////////////////////////////////////
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;

using AfxEx;
using AfxEx.Net;
using Ivk.IOSys;
using Dps.UniRequest;
using Dps.UniTcpIp;
using log4net;

namespace Tdm.Unification
{
	/// <summary>
	/// Переопределен заголовк сообщения для использования при обмене со старыми приложениями
	/// </summary>
	public class UnificHeader
	{
		/// <summary>
		/// Упаковка заголовка в старом формате 
		/// 	BYTE m_SrcType;
		///		BYTE m_MsgType;
		///		DWORD m_Size;
		///		BYTE m_MsgNumber;
		/// 
 		/// </summary>
		/// <param name="Wr">поток записи</param>
		/// <param name="Hd">заголовок</param>
		static public void SerializeOld(Header Hd, BinaryWriter Wr)
		{
			// проверяем поток
			if (Wr == null)
				throw new ArgumentNullException("BinaryWriter");
			// записываем стртовые два байта
			byte SrcType = 0x19;
			Wr.Write(SrcType);
			// пишем код запроса
			byte Code = (byte)Hd.Code;
			Wr.Write(Code);
			// учитывает 1 байт на номер пакета 
			UInt32 Size = Hd.Size + 1;
			// пишем размер
			Wr.Write(Size);
			// пишем счетчик
			byte MsgNumber = (byte)Hd.ID;
			Wr.Write(MsgNumber);
			// Dest не пишем
		}
		/// <summary>
		/// Распаковка заголовка в старом формате
		/// </summary>
		/// <param name="Rd">поток чтения</param>
		/// <param name="Hd">заголовок</param>
		static public void DeserializeOld(Header Hd, BinaryReader Rd)
		{
			// проверяем поток
			if (Rd == null)
				throw new ArgumentNullException("BinaryReader");

			// Читаем стртовые два байта
			byte SrcType = Rd.ReadByte();
			if (SrcType != 0x19)
				throw new ArgumentException("SrcType != 0x19", "SrcType");

			// читаем код
			byte Code = Rd.ReadByte();
			Hd.Code = (RequestCode)Code;
			// устанавливаем дополнительный код в ноль
			Hd.SubCode = 0;
			// читаем размер
			Hd.Size = Rd.ReadUInt32();
			// учитывает 1 байт на номер пакета 
			Hd.Size--;
			// читаем счетчик
			byte ID = Rd.ReadByte();
			Hd.ID = ID;
			// устанавливаем признак не последнего сообщения
			Hd.Flags = RequestFlags.PartMsg;
			// Dest пустой
			Hd.Dest = string.Empty;
		}

		/// <summary>
		/// Упаковка заголовка в старом формате 
		/// 	BYTE m_SrcType;
		///		BYTE m_MsgType;
		///		DWORD m_Size;
		///		BYTE m_MsgNumber;
		/// 
		/// </summary>
		/// <param name="Wr">поток записи</param>
		/// <param name="Hd">заголовок</param>
		static public void Serialize(Header Hd, BinaryWriter Wr)
		{
			// проверяем поток
			if (Wr == null)
				throw new ArgumentNullException("BinaryWriter");
			// записываем стртовые два байта
			byte Sync = 0xA5;
			Wr.Write(Sync);
			// пишем код запроса
			byte Code = (byte)Hd.Code;
			Wr.Write(Code);

			// пишем счетчик
			byte MsgNumber = (byte)Hd.ID;
			Wr.Write(MsgNumber);
			// пишем резерв
			Wr.Write((UInt64)0);
			// пишем размер
			UInt32 Size = Hd.Size;
			// пишем размер
			Wr.Write(Size);
			// Dest не пишем
		}
		/// <summary>
		/// Распаковка заголовка в старом формате
		/// </summary>
		/// <param name="Rd">поток чтения</param>
		/// <param name="Hd">заголовок</param>
		static public void Deserialize(Header Hd, BinaryReader Rd)
		{
			// проверяем поток
			if (Rd == null)
				throw new ArgumentNullException("BinaryReader");

			// Читаем стртовые два байта
			byte Sync = Rd.ReadByte();
			if (Sync != 0xA5)
				throw new ArgumentException("Sync != 0xA5", "Sync");

			// читаем код
			byte Code = Rd.ReadByte();
			Hd.Code = (RequestCode)Code;
			// устанавливаем дополнительный код в ноль
			Hd.SubCode = 0;
			// читаем счетчик
			byte ID = Rd.ReadByte();
			Hd.ID = ID;

			// читаем резерв
			Rd.ReadUInt64();
			
			// читаем размер
			Hd.Size = Rd.ReadUInt32();
			
			// Dest пустой
			Hd.Dest = string.Empty;
		}
	}

	#region Старая версия унификации
	
	/// <summary>
	/// Класс соединения по TcpIp с использованием старого заголовка
	/// </summary>
	class UnificConnectionOld : TcpConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="sock">tcp клиент</param>
		/// <param name="bServer">признак того, что это серверная сторона</param>
		public UnificConnectionOld(Socket sock, bool bServer)
			: base(sock, true) 
		{
			_bServer = bServer;
			if (!_bServer)
				IsReceiveMode = true;
		}

		/// <summary>
		/// Переопределен метод для отправки старого заголовка
		/// </summary>
		/// <param name="Hd">заголовк в старом формате (должен приводится к типу OldHeader)</param>
		protected override void OnSendHeader(Header Hd)
		{
			if (Hd == null)
				throw new ArgumentNullException("Header");
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);

			if (_bServer)
			{
				//Установка глобального taimout
				//_Stream.SetTotalTimeout(_Timeout);
				_Stream.WriteTimeout = _Timeout;
				UnificHeader.SerializeOld(Hd, Writer);
			}

			Debug.WriteLine("UnificConnectionOld: Отправлен заголовок...");
		}
		/// <summary>
		/// Переопределен метод для получения заголовка в старом формате
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();
			if (!_bServer)
			{
				//Установка глобального taimout
				//if (IsPartMessage || (Reader == null))
				//	_Stream.SetTotalTimeout(_Timeout);
				_Stream.ReadTimeout = _Timeout;

				UnificHeader.DeserializeOld(Hd, Reader);
			}
			else// как-будто получили запрос
			{
				Hd.Code = (RequestCode)0x01;
				Hd.SubCode = 0;
				Hd.ID = 0;
				Hd.Size = 0;
			}
			return Hd;
		}
		/// <summary>
		/// признак серверного соединения
		/// </summary>
		protected bool _bServer;
	}

	/// <summary>
	/// Класс фабрики клиентских Tcp соединений с использованием старого заголовка
	/// </summary>
	public class UnificClientOld : Client
	{
		/// <summary>
		/// Переопределен метод создания соединения для создания клиентского соединения с использованием старого заголовка
		/// </summary>
		/// <returns>Соединение</returns>
		public override AConnection CreateConnection()
		{
			return new UnificConnectionOld(SocketHelper.Connect(Host, Port), false);
		}
	}

	/// <summary>
	/// Серверное соединения по унифицированному протоколу первой редакции
	/// </summary>
	public class UnificServerConnectionOld : ServerConnection
	{
		/// <summary>
		/// Конструктор - производит подключение к концентратору
		/// </summary>
		/// <param name="concentrator">концентратор</param>
		/// <param name="siteList">коллекция станций</param>
		public UnificServerConnectionOld(Concentrator concentrator, DlxCollection siteList)
		{ 
			_concentrator = concentrator;
			_siteList = siteList;

			// создаем поток для записи начального состояния
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			_lastTime = 0;
			writer.Write(_lastTime);

			// для всех станций 
			foreach( DlxObject site in siteList.Objects)
			{
				// находим подсистему объектов
				UniObjSystem objects = site.GetObject("Objects") as UniObjSystem;
				// если нашли, то пописываемся на изменения
				if (objects != null)
				{
					Monitor.Enter(objects);
					try
					{
						// записываем начальное сосотяние объектов
						objects.WriteOverallState_old(writer);
						// запоминаем время
						int stateTime = Afx.CTime.ToTimeT(objects.DataTime);
						if (stateTime > _lastTime)
							_lastTime = stateTime;
						// подписываемся на извещение о наличии данных
						concentrator.AddConsumerQueue(_dataQueue, objects, _eventOnData);
					}
					catch (Exception e)
					{
						Debug.WriteLine("UnificServerConnectionOld: ERROR to write initial state for " + objects.Owner.Name + ": " + e.ToString());
						Trace.WriteLine("UnificServerConnectionOld: ERROR to write initial state for " + objects.Owner.Name + ": " + e.ToString());
					}
					finally
					{
						Monitor.Exit(objects);
					}
				}
			}

			writer.Flush();
			ms.Seek(0, SeekOrigin.Begin);
			writer.Write(_lastTime);
			writer.Flush();
			writer.Close();
			// получаем массив данных для первого сообщения
			_firstData = ms.ToArray();
			// закрываем поток
			ms.Close();
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			_concentrator.RemoveConsumerQueue(_dataQueue);
			_dataQueue.Clear();
			base.Dispose(disposing);
		}
		/// <summary>
		/// получить заголовок для передачи клиенту
		/// </summary>
		/// <returns></returns>
		protected override Header OnReceiveHeader()
		{

			Debug.WriteLine("UnificServerConnectionOld: Отправляем ответ " + _ID);
			Header Hd = base.OnReceiveHeader();
			_ID++;
			return Hd;
		}
		/// <summary>
		/// Обработать запрос
		/// </summary>
		/// <param name="Msg">Обрабатываемое сообщение</param>
		protected override void ExcecuteRequest(Message Msg)
		{
		}
		/// <summary>
		/// получить ответ
		/// </summary>
		/// <returns>сообщение ответа</returns>
		protected override Message GetAnswer()
		{
			Message Answer = new Message();
			Answer.PartMessage = true;
			if (_firstData != null)
			{
				Debug.WriteLine("UnificServerConnectionOld: Отправляем начальное состояние size = " + _firstData.Length);
				// сообщение с полным сосотянием
				Answer.Code = (RequestCode)0x1;
				Answer.Data = _firstData;
				_firstData = null;
				return Answer;
			}

			// сообщение с изменениями
			Answer.Code = (RequestCode) 0x2;
			// ждем изменения в течении 9 секунд
			if (_dataQueue.Count > 0 || _eventOnData.WaitOne(9000, false))
			{
				int recTime = 0;
				// создаем поток для записи времени
				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);

				// пока есть хотябы один элемент в очереди
				while (_dataQueue.Count > 0)
				{
					Object data = null;
					// блокируем очередь данных
					Monitor.Enter(_dataQueue);
					try
					{
						// получаем элемент
						data = _dataQueue.Peek();
					}
					finally
					{
						// разблокируем очередь
						Monitor.Exit(_dataQueue);
					}

					// вызываем обработку
					if (data != null)
					{
						UnificationConcentrator.UniObjState state = data as UnificationConcentrator.UniObjState;
						// если тип данных - состояние
						if (state != null)
						{
							int stateTime = Afx.CTime.ToTimeT(state._dataTime);
							if (recTime == 0 || recTime == stateTime)
							{
								// если первая запись, то пишем время
								if (recTime == 0)
									writer.Write(stateTime);
								// запоминам время
								recTime = stateTime;
								// пишем ID станции
								writer.Write(state._siteID);
								// пишем ID объекта
								writer.Write(state._objectID);
								// пишем состояние
								writer.Write(state._uniState);
							}
							else // за текущую секунду все обработали => обработаем эти данные в следующем GetAnswer
								break;
						}

					}

					// блокируем очередь данных
					Monitor.Enter(_dataQueue);
					try
					{
						// удаляем элемент из очереди
						_dataQueue.Dequeue();
					}
					finally
					{
						// разблокируем очередь
						Monitor.Exit(_dataQueue);
					}
				}
				// если записей не было пишем просто последнее время
				if (recTime == 0)
					writer.Write(_lastTime);
				else// запоминаем записанное время
					_lastTime = recTime;
				
				writer.Flush();
				writer.Close();
				// получаем массив данных для ответа
				Answer.Data = ms.ToArray();
				// закрываем поток
				ms.Close();
			}
			else // нет изменений в течении 9 секунд
			{
				// создаем поток для записи времени
				MemoryStream ms = new MemoryStream();
				BinaryWriter writer = new BinaryWriter(ms);
				// пишем время
				writer.Write(_lastTime);
				//
				writer.Flush();
				writer.Close();
				// получаем массив данных для ответа
				Answer.Data = ms.ToArray();
				// закрываем поток
				ms.Close();
			}
			Debug.WriteLine("UnificServerConnectionOld: Отправляем изменения size = " + Answer.Data.Length);
			// возвращаем ответ
			return Answer;
		}
		
		#region Данные
		/// <summary>
		/// ссылка на концентратор, который обрабатывает данные
		/// </summary>
		protected Concentrator _concentrator = null;
		/// <summary>
		/// ссылка на коллекцию контролируемых станций
		/// </summary>
		protected DlxCollection _siteList = null;
		/// <summary>
		/// очередь для данных 
		/// </summary>
		protected Queue _dataQueue = new Queue();
		/// <summary>
		/// событие для извещения о поступлении данных для отправки
		/// </summary>
		protected AutoResetEvent _eventOnData = new AutoResetEvent(false);
		/// <summary>
		/// последнее записанное в сообщении время в формате time_t
		/// </summary>
		protected int _lastTime;
		/// <summary>
		/// данные для первого сообщения
		/// </summary>
		protected byte[] _firstData = null;
		#endregion
	}
	
	/// <summary>
	/// Фабрика соединений по унифицированному протоколу первой редакции
	/// </summary>
	public class UnificServerConnectionOldFactory : ConnectionFactory
	{
		/// <summary>
		/// констркутор
		/// </summary>
		public UnificServerConnectionOldFactory()
		{ 
		}
		/// <summary>
		/// Создание серверного соединения
		/// </summary>
		/// <returns></returns>
		public override AConnection CreateConnection()
		{
			return new UnificServerConnectionOld(_concentrator, _siteList);
		}
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			// получаем концентратор
			_concentrator = Loader.GetObjectFromAttribute("Concentrator", typeof(Concentrator)) as Concentrator;
			// получаем коллекцию станций
			_siteList = Loader.GetObjectFromAttribute("SiteList", typeof(DlxCollection)) as DlxCollection;
		}
		/// <summary>
		/// Концентратор
		/// </summary>
		protected Concentrator _concentrator;
		/// <summary>
		/// Коллекция станций
		/// </summary>
		protected DlxCollection _siteList;
	}

	/// <summary>
	/// Сервер унифицированного информационного обмена - старая версия
	/// </summary>
	public class UnificationOldServer : Server
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		public UnificationOldServer()
		{ 
		}
		/// <summary>
		/// Создание соединения
		/// </summary>
		/// <param name="client">tcp клиент</param>
		/// <returns>соединение</returns>
		public override AConnection CreateConnection(Socket client)
		{
			// создаем старое соединение унификации
			return new UnificConnectionOld(client, true);
		}
	}
	
	#endregion

	#region Новая версия унификации
	
	/// <summary>
	/// Класс соединения по TcpIp с использованием нового заголовка
	/// </summary>
	public class UnificConnection : TcpConnection
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		/// <param name="sock">tcp клиент</param>
		public UnificConnection(Socket sock)
			: base(sock, false) 
		{
			_bServer = false;
		}

		/// <summary>
		/// Констркутор
		/// </summary>
		/// <param name="sock">tcp клиент</param>
		/// <param name="bServer">признак серверного соединения</param>
		public UnificConnection(Socket sock, bool bServer)
			: base(sock, bServer)
		{
			_bServer = bServer;
		}

		/// <summary>
		/// Переопределен метод для отправки старого заголовка
		/// </summary>
		/// <param name="Hd">заголовк в старом формате (должен приводится к типу OldHeader)</param>
		protected override void OnSendHeader(Header Hd)
		{
			if (Hd == null)
				throw new ArgumentNullException("Header");
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);

			//Установка глобального taimout
			//_Stream.SetTotalTimeout(_Timeout);
			_Stream.WriteTimeout = _Timeout;

			UnificHeader.Serialize(Hd, Writer);

			// если клиент, то запоминаем отправленный счетчик
			if (!_bServer)
			{
				_LastSendMsgId = Hd.ID;
				_WaitRecvMsgId = (byte) Hd.ID;
			}
		}
		/// <summary>
		/// Переопределен метод для получения заголовка в старом формате
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if (_Stream == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();

			//Установка глобального taimout
			//if (IsPartMessage || (Reader == null))
				//_Stream.SetTotalTimeout(_Timeout);
			_Stream.ReadTimeout = _Timeout;

			UnificHeader.Deserialize(Hd, Reader);

			/*/ если сервер и спорадическая передача, то устанавливаем признак не последнего сообщения
			if (_bServer && (Hd.Code == (RequestCode) 0x1 || Hd.Code == (RequestCode) 0x11) )
			{
				Hd.Flags = RequestFlags.PartMsg;
			}//*/
			// если клиент и спорадическая передача, то проверяем счетчик
			if (!_bServer && (Hd.Code == (RequestCode) 0x1 || Hd.Code == (RequestCode) 0x11) )
			{
				Hd.Flags = RequestFlags.PartMsg;
				// проверяем счетчик
				if (_WaitRecvMsgId != Hd.ID)
					throw new RequestException(ErrorCode.MsgHeader, string.Format("Получено сообщение содержащее ошибочный ID: {0} (ожидаемый: {1})", Hd.ID, _WaitRecvMsgId));
				// инкрементируем ожидаемый идентификатор
				_WaitRecvMsgId++;
				// устанавливаем счетчик в значение отправленного
				Hd.ID = _LastSendMsgId;
			}

			return Hd;
		}
		/// <summary>
		/// значение счетчика последнего отправленного сообщения
		/// </summary>
		protected uint _LastSendMsgId = 0;
		/// <summary>
		/// ожидаемый счетчик - введен т.к. в унифицированном обмене сервер инкрементирует счетчик при спорадической передаче
		/// а в нашей системе сервер передает серию ответов с одним и тем же счетчиком (эта проверка выполняется в ClientSession)
		/// </summary>
		protected byte _WaitRecvMsgId = 0;

		/// <summary>
		/// признак серверного соединения
		/// </summary>
		protected bool _bServer;
	}

	/// <summary>
	/// Класс фабрики клиентских Tcp соединений с использованием нового заголовка
	/// </summary>
	public class UnificClient : Client
	{
		/// <summary>
		/// Переопределен метод создания соединения для создания клиентского соединения с использованием нового заголовка
		/// </summary>
		/// <returns>Соединение</returns>
		public override AConnection CreateConnection()
		{
			return new UnificConnection(SocketHelper.Connect(Host, Port));
		}
	}

	/// <summary>
	/// Серверное соединения по унифицированному протоколу второй редакции
	/// </summary>
	public class UnificServerConnection : ServerConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public UnificServerConnection()
		{
			_concentrator = null;
			_siteList = null;
		}
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="concentrator">концентратор</param>
		/// <param name="siteList">коллекция станций</param>
		public UnificServerConnection(Concentrator concentrator, DlxCollection siteList)
		{
			_concentrator = concentrator;
			_siteList = siteList;
			CreateCurAnswerDictionaries();
		}
		/// <summary>
		/// Разрушение
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)401, "Закрытие соединения с клиентом");
			if (_concentrator != null)
				_concentrator.RemoveConsumerQueue(_dataQueue);
			if (_dataQueue != null)
				_dataQueue.Clear();
			base.Dispose(disposing);
		}
		/// <summary>
		/// получить заголовок для передачи клиенту
		/// </summary>
		/// <returns></returns>
		protected override Header OnReceiveHeader()
		{
			Header Hd = base.OnReceiveHeader();
			// если включен спорадический режим, то сервер должен инкрементировать счетчик в отправляемых сообщениях
			if (_bSporadic)
				_ID++;

			return Hd;
		}
		/// <summary>
		/// Обработать запрос
		/// </summary>
		/// <param name="Msg">Обрабатываемое сообщение</param>
		protected override void ExcecuteRequest(Message Msg)
		{
			BinaryReader reader = new BinaryReader(new MemoryStream(Msg.Data));
			_Answer = null;

			if (_bWaitFirstRequest && !CheckIdentification(reader))
					throw new RequestException(ErrorCode.WrongMode);

			if (Msg.Code == (RequestCode)0)// запрос идентификации
			{
				_Answer = new Message();
				_Answer.Code = (RequestCode)0;
			}
			else if ( Msg.Code == (RequestCode)1)// запрос текущего состояния
			{
				// считываем требования
				ReadRequirements(reader);
				// подготавливаем первый ответ
				PrepareAnswerFirstCurState();
			}
			else if ( Msg.Code == (RequestCode)2)// запрос версии НСИ
			{
				// подготавливаем ответ
				PrepareAnswerNSIVersion();
			}
			else if ( Msg.Code == (RequestCode)3)// запрос состава НСИ
			{
				// считываем требования
				ReadRequirements(reader);
				// подготавливаем ответ
				PrepareAnswerNSI();
			}
			else if ( Msg.Code == (RequestCode)0x11)// запрос архива
			{
				throw new RequestException(ErrorCode.WrongCode);
			}
			else
				throw new RequestException(ErrorCode.WrongCode);
		}

        ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// получить ответ
        /// </summary>
        /// <returns>сообщение ответа</returns>
        protected override Message GetAnswer()
		{
			try
			{
				// проверяем - был ли подготовлен ответ
				if (_Answer != null)
				{
					Message temp = _Answer;
					_Answer = null;
					return temp;
				}
				// ответ не подготовлен - проверяем включен ли спорадический режим 
				if (!_bSporadic)
					throw new RequestException(ErrorCode.WrongMode);

				// ответ содержится в очереди - обрабатываем
				Message Answer = new Message();
				// устанавливаем код ответа
				Answer.Code = (RequestCode)_sporadicAnswerCode;
				Answer.PartMessage = true;
				// проверяем, что если мы врежиме передачи архива и в очереди нет данных, то закрываем соединение
				if (_bArchiveMode && _dataQueue.Count == 0)
					throw new RequestException(ErrorCode.Timeout);

				// ждем изменения в течении 9 секунд
				if (_dataQueue.Count > 0 || _eventOnData.WaitOne(9000, false) || _dataQueue.Count > 0)
				{
					Debug.WriteLine("UnificServerConnection: Либо есть данные либо извещение count = " + _dataQueue.Count);
					int recTime = 0, stateTime = 0;
					// создаем поток для записи времени
					MemoryStream ms = new MemoryStream();
					BinaryWriter writer = new BinaryWriter(ms);
					// подготовливаем элементы
					UnificationConcentrator.UniDataItem item;
					UnificationConcentrator.UniObjState state;
					UnificationConcentrator.UniDiagState diag;
					UnificationConcentrator.UniNumericParamState numpar;
					UnificationConcentrator.UniDataParamState datapar;
					UnificationConcentrator.UniObjWork work;

					// пока есть хотябы один элемент в очереди
					while (_dataQueue.Count > 0)
					{
						Object data = null;
						// блокируем очередь данных
						Monitor.Enter(_dataQueue);
						try
						{
							// получаем элемент
							data = _dataQueue.Peek();
						}
						finally
						{
							// разблокируем очередь
							Monitor.Exit(_dataQueue);
						}

                        // вызываем обработку
                        if (data != null)
                        {
                            item = data as UnificationConcentrator.UniDataItem;
                            // проверяем требуются ли эти данные клиенту
                            if (item != null && (_bAllSites || _reqSites.Contains(item._siteID)))
                            {
                                // проверяем, что это не следующая секунда
                                stateTime = Afx.CTime.ToTimeT(item._dataTime);
                                if (recTime != 0 && recTime != stateTime)
                                    break;// за текущую секунду все обработали => обработаем эти данные в следующем GetAnswer

                                try
                                {
                                    // если требуются состояния и тип данных состояние
                                    if (IsNeedObjectStates && (state = data as UnificationConcentrator.UniObjState) != null)
                                    {
                                        _curAnswerStates[state._siteID].Enqueue(state);
                                        log.Info(string.Format("Состояние SiteID={0}; ObjectID={1}; StateID={2}; PrevState={3}; Time={4}"
                                            , state._siteID, state._objectID, state._uniState, state._uniPrevState, state._dataTime));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                    // если требуются ситуации и тип данных ситуации
                                    else if (IsNeedObjectDiags && (diag = data as UnificationConcentrator.UniDiagState) != null)
                                    {
                                        _curAnswerDiags[diag._siteID].Enqueue(diag);
                                        log.Info(string.Format("Диагностическая ситуация SiteID={0}; ObjectID={1}; StateID={2}; BeginTime={3}; EndTime={4}; DateTime={5}"
                                            , diag._siteID, diag._objectID, diag._uniDiagState, diag._detectTime, diag._endTime, diag._dataTime));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                    // если требуются числовые параметры и тип данных числовые параметры
                                    else if (IsNeedObjectsNumericParams && (numpar = data as UnificationConcentrator.UniNumericParamState) != null)
                                    {
                                        _curAnswerNumParams[numpar._siteID].Enqueue(numpar);
                                        log.Info(string.Format("Числовой параметр SiteID={0}, ObjectID={1}, ParamID={2}, DateTime={3}, ParamTime={4}, ParamValue={5}"
                                            , numpar._siteID, numpar._objectID, numpar._paramID, numpar._dataTime, numpar._paramTime, numpar._paramValue));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                    // если требуются нечисловые параметры и тип данных нечисловые параметры
                                    else if (IsNeedObjectsDataParams && (datapar = data as UnificationConcentrator.UniDataParamState) != null)
                                    {
                                        _curAnswerDataParams[datapar._siteID].Enqueue(datapar);
                                        log.Info(string.Format("Нечисловой параметр SiteID={0}; ObjectID={1}; ParamID={2}; ParamValue={3}; Time={4}"
                                            , datapar._siteID, datapar._objectID, datapar._paramID, datapar._paramValue, datapar._dataTime));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                    // если требуются факты работ и тип данных факты работ
                                    else if (IsNeedFactWorks && (work = data as UnificationConcentrator.UniObjWork) != null && work._flag == UnificationConcentrator.UniObjWork.Kind.Complete)
                                    {
                                        _curAnswerWorks[work._siteID].Enqueue(work);
                                        log.Info(string.Format("Факт работы SiteID={0}; WorkID={1}; PunktID={2}; ObjectID={3}; Diags={4}; BeginTime={5}; EndTime={6}; Time={7}"
                                            , work._siteID, work._workID, work._punktID, work._objectID, work._diags, work._beginTime, work._endTime, work._dataTime));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                    // если требуется план работ и тип данных план работ
                                    else if (IsNeedPlanWorks && (work = data as UnificationConcentrator.UniObjWork) != null
                                        && (work._flag == UnificationConcentrator.UniObjWork.Kind.Add || work._flag == UnificationConcentrator.UniObjWork.Kind.Remove))
                                    {
                                        _curAnswerWorks[work._siteID].Enqueue(work);
                                        log.Info(string.Format("Факт работы SiteID={0}; WorkID={1}; PunktID={2}; ObjectID={3}; Diags={4}; BeginTime={5}; EndTime={6}; Time={7}"
                                            , work._siteID, work._workID, work._punktID, work._objectID, work._diags, work._beginTime, work._endTime, work._dataTime));
                                        // запоминам время
                                        recTime = stateTime;
                                    }
                                }
                                catch (KeyNotFoundException)
                                {
                                    Debug.WriteLine("UnificServerConnection: Unknown site id " + item._siteID.ToString());
                                    log.Error("UnificServerConnection: Unknown site id " + item._siteID.ToString());
                                }
                            }
                            else
                            {
                                Debug.WriteLine("UnificServerConnection: skip data for " + item._siteID);
                                log.Debug("UnificServerConnection: skip data for " + item._siteID);
                            }
                        }
                        else
                        {
                            Debug.WriteLine("UnificServerConnection: data == null");
                            log.Debug("UnificServerConnection: data == null");
                        }

						// блокируем очередь данных
						Monitor.Enter(_dataQueue);
						try
						{
							// удаляем элемент из очереди
							_dataQueue.Dequeue();
						}
						finally
						{
							// разблокируем очередь
							Monitor.Exit(_dataQueue);
						}
					}
					// если есть хотябы одна запись
					if (recTime != 0)
					{
						// записываем версию
						writer.Write((uint)_versionNSI);

						_lastTime = recTime;
						// пишем время
						writer.Write(recTime);
						// пишем все данные
						WriteCurAnswerData(writer);
						// копируем данные из потока в ответ
						writer.Flush();
						writer.Close();
						// получаем массив данных для ответа
						Answer.Data = ms.ToArray();
						// закрываем поток
						ms.Close();
					}
					else
					{
						Debug.WriteLine("UnificServerConnection: Нет данных для отправки - пустое сообщение...");
                        log.Debug("UnificServerConnection: Нет данных для отправки - пустое сообщение...");
                    }
				}
				else
				{
                    if (_dataQueue.Count == 0)
                    {
                        Debug.WriteLine("UnificServerConnection: Нет данных в очереди - пустое сообщение...");
                        log.Debug("UnificServerConnection: Нет данных в очереди - пустое сообщение...");
                    }
                }

				_eventOnData.Reset();
				// возвращаем ответ
				return Answer;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		/// <summary>
		/// проверяем идентификацию клиента
		/// </summary>
		/// <param name="reader">данные запроса</param>
		/// <returns>имеет ли клиент право на доступ</returns>
		protected bool CheckIdentification(BinaryReader reader)
		{
			// читаем имя
			byte Len = reader.ReadByte();
			byte[] name = reader.ReadBytes(Len);
			// читаем пароль
			Len = reader.ReadByte();
			byte[] pswd = reader.ReadBytes(Len);
			// больше не ждем иде
			_bWaitFirstRequest = false;
			// пока не проверяем пароль
			return true;
		}
		/// <summary>
		/// Прочитать требования по передаче данных
		/// </summary>
		/// <param name="reader"></param>
		protected void ReadRequirements(BinaryReader reader)
		{
			// флаг требований
			_ReqType = reader.ReadUInt32();
			UInt32 SiteID, SitesCount = reader.ReadUInt32();
			// список станций
			if (SitesCount == 0)
				_bAllSites = true;
			else
				_bAllSites = false;
			// очищаем массив
			_reqSites.Clear();
			// заполняем массив
			for (int i = 0; i < SitesCount; i++)
			{
				SiteID = reader.ReadUInt32();
				_reqSites.Add(SiteID);
			}
			// сортируем массив
			_reqSites.Sort();
		}
		/// <summary>
		/// Подготовка первого ответа на запрос за текущим состоянием
		/// </summary>
		protected void PrepareAnswerFirstCurState()
		{
			_sporadicAnswerCode = 0x1;
			_bArchiveMode = false;
			_bSporadic = true;
			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write(_versionNSI);
			// пишем время
			DateTime time = GetFirstAnswerTime();
			writer.Write((UInt32)Afx.CTime.ToTimeT(time));

			/////////////////////////////////////////////
			#region Наполняем очереди данных всеми требуемыми данными
			foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
			{ 
				// если требуются по станции
				if (_bAllSites || _reqSites.Contains(ObjSys.SiteID))
				{
					Queue<UnificationConcentrator.UniObjState> queueState;
					Queue<UnificationConcentrator.UniDiagState> queueDiag;
					Queue<UnificationConcentrator.UniNumericParamState> queueNumPar;
					Queue<UnificationConcentrator.UniDataParamState> queueDataPar;
					Queue<UnificationConcentrator.UniObjWork> queueWork;

					if (_curAnswerStates.TryGetValue(ObjSys.SiteID, out queueState)
						&& _curAnswerDiags.TryGetValue(ObjSys.SiteID, out queueDiag)
						&& _curAnswerNumParams.TryGetValue(ObjSys.SiteID, out queueNumPar)
						&& _curAnswerDataParams.TryGetValue(ObjSys.SiteID, out queueDataPar)
						&& _curAnswerWorks.TryGetValue(ObjSys.SiteID, out queueWork))
					{

						Monitor.Enter(ObjSys);
						try
						{
							// подписываемся на извещение о наличии данных
							_concentrator.AddConsumerQueue(_dataQueue, ObjSys, _eventOnData);

							foreach (IOBaseElement elem in ObjSys.UniObjsDictionary.Values)
							{
								UniObject obj;
								if (elem is UniObject)
									obj = elem as UniObject;
								else
									continue;

								// не передаем служебные объекты с отрицательным идентификатором
								if (obj.Number < 0)
									continue;

								if (IsNeedObjectStates)
								{
									UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
									state._siteID = ObjSys.SiteID;
									state._objectID = (UInt32)obj.Number;
									state._uniState = (byte)obj.ObjState.Current;
									state._dataTime = time;
                                    queueState.Enqueue(state);
								}
								if (IsNeedObjectDiags)
								{
									foreach (int id in obj._diagStateTOtimeMAPuni.Keys)
									{
										UnificationConcentrator.UniDiagState diag = new UnificationConcentrator.UniDiagState();
										diag._siteID = ObjSys.SiteID;
										diag._objectID = (UInt32)obj.Number;
										diag._uniDiagState = id;
										diag._dataTime = time;
										diag._detectTime = obj._diagStateTOtimeMAPuni[id];
										diag._endTime = DateTime.MinValue;
										queueDiag.Enqueue(diag);
									}
								}

								if (IsNeedFactWorks)
								{
								}
							}
							
							
							foreach (DlxObject objdatadlx in ObjSys.UniParamsDictionary.Values)
							{
								if (IsNeedObjectsNumericParams)
								{
									UniObjData objdata = objdatadlx as UniObjData;
									if (objdata != null && objdata.IsNumeric)
									{
										UnificationConcentrator.UniNumericParamState numpar = new UnificationConcentrator.UniNumericParamState();
										numpar._siteID = ObjSys.SiteID;
										numpar._objectID = objdata.ObjectID;
										numpar._dataTime = time;
										numpar._paramID = objdata.ParamID;
										numpar._paramTime = objdata._lastSetParamTime;
										numpar._paramValue = objdata._lastSetParamState;
										queueNumPar.Enqueue(numpar);
									}
								}
									
								if (IsNeedObjectsDataParams)
								{
									UniObjData objdata = objdatadlx as UniObjData;
									if (objdata != null && !objdata.IsNumeric && objdata.IsString)
									{
										UnificationConcentrator.UniDataParamState datapar = new UnificationConcentrator.UniDataParamState();
										datapar._siteID = ObjSys.SiteID;
										datapar._objectID = objdata.ObjectID;
										datapar._dataTime = time;
										datapar._paramID = objdata.ParamID;
										datapar._paramValue = System.Text.Encoding.Default.GetBytes(objdata.String);
										queueDataPar.Enqueue(datapar);
									}
								}
							}
						}
						catch (Exception ex)
						{
							Debug.WriteLine("UnificServerConnection exception during get first state: " + ex.Message);
						}
						finally
						{
							Monitor.Exit(ObjSys);
						}
					}
				}
			}
			#endregion
			/////////////////////////////////////////////

			// записываем данные в поток
			WriteCurAnswerData(writer);

			/////////////////////////////////////////////
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)_sporadicAnswerCode;
			_Answer.PartMessage = true;
			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();
		}
		/// <summary>
		/// Расчитать время для первого ответа текущего состояния
		/// </summary>
		/// <returns>время</returns>
		protected DateTime GetFirstAnswerTime()
		{
			DateTime time = DateTime.MinValue;
			foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
			{
				// если требуются НСИ по станции
				if (_bAllSites || _reqSites.Contains(ObjSys.SiteID) )
				{
					if (time < ObjSys.DataTime)
						time = ObjSys.DataTime;
				}
			}

			if (time == DateTime.MinValue)
				time = DateTime.Now;

			return time;
		}
		/// <summary>
		/// Подготовка ответа версии НСИ
		/// </summary>
		protected virtual void PrepareAnswerNSIVersion()
		{
			_bSporadic = false;
			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)2;
			// записываем данные
			_Answer.Data = ms.ToArray();
			ms.Close();
		}
		/// <summary>
		/// Подготовка ответа состава НСИ
		/// </summary>
		protected virtual void PrepareAnswerNSI()
		{
			_bSporadic = false;
			// пишем версию
			MemoryStream ms = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(ms);
			writer.Write((uint)_versionNSI);
			////////////////////////////////////////////
			UInt32 SiteCount;
			UInt32 Size = 0;
			long pos1, pos2, pos3, pos4;

			////////////////////////////////////////////
			#region запись НСИ объектов

			if (IsNeedNSIObjects)
			{
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;
				foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
				{
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(ObjSys.SiteID))
					{
						// пишем идентификатор
						writer.Write((UInt32)ObjSys.SiteID);
                        // указываем количество парков - т.к их нет - то 1
                        writer.Write(1);
                        //Указываем длину наименования
                        writer.Write((byte)0);
                        //Указываем пустое наименование 
                        byte[] park_name = Encoding.Default.GetBytes(string.Empty);
                        writer.Write(park_name);
                        // увеличиваем счетчик
                        SiteCount++;
						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ObjCount = 0;
						// пишем количество записей
						writer.Write(ObjCount);
						// по всем объектам в словаре
						foreach (IOBaseElement elem in ObjSys.UniObjsDictionary.Values)
						{
							UniObject obj;
							if (elem is UniObject && elem.Number >= 0)
								obj = elem as UniObject;
							else
								continue;

							// пишем идентификатор объекта
							writer.Write((UInt32)obj.Number);
							// пишем идентификатор типа
							writer.Write((UInt32)obj.Type.SubGroupID);
							// пишем имя объекта
							byte[] name = Encoding.Default.GetBytes(obj.Name);
							writer.Write((UInt16)name.Length);
							writer.Write(name);

                            ObjCount++;
						}
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ObjCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ диагностики
			Size = 0;
			if (IsNeedNSIDiag)
			{
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;
				foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
				{
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(ObjSys.SiteID))
					{
						// пишем идентификатор
						writer.Write((UInt32)ObjSys.SiteID);
                        // указываем количество парков - т.к их нет - то 1
                        writer.Write(1);
                        //Указываем длину наименования
                        writer.Write((byte)0);
                        //Указываем пустое наименование 
                        byte[] park_name = Encoding.Default.GetBytes(string.Empty);
                        writer.Write(park_name);
                        // увеличиваем счетчик
                        SiteCount++;
						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 DiagCount = 0;
						// пишем количество записей
						writer.Write(DiagCount);
						// по всем объектам в словаре
						foreach (IOBaseElement elem in ObjSys.UniObjsDictionary.Values)
						{
							UniObject obj;
							if (elem is UniObject && elem.Number >= 0)
								obj = elem as UniObject;
							else
								continue;

							//TODO: будет полная коллекци всех возможных диагнозов вместо obj.DiagStates
							foreach (BaseControlObj.DiagState diag in obj.InitialDiagStates)
							{
								DiagCount++;
								// пишем идентификатор объекта
								writer.Write((UInt32)obj.Number);
								// пишем идентификатор типа
								writer.Write((UInt16)diag.Id);
							}
						}
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(DiagCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ параметров
			Size = 0;
			if (IsNeedNSIParams)
			{
				///////////////////////////////////////
				#region записываем НСИ числовых парметров
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;
				foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
				{
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(ObjSys.SiteID))
					{
						// пишем идентификатор
						writer.Write((UInt32)ObjSys.SiteID);
                        // указываем количество парков - т.к их нет - то 1
                        writer.Write(1);
                        //Указываем длину наименования
                        writer.Write((byte)0);
                        //Указываем пустое наименование 
                        byte[] park_name = Encoding.Default.GetBytes(string.Empty);
                        writer.Write(park_name);
                        // увеличиваем счетчик
                        SiteCount++;
						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ParamsCount = 0;
						// пишем количество записей
						writer.Write(ParamsCount);
						// по всем объектам в словаре
						foreach (UniObjData par in ObjSys.UniParamsDictionary.Values)
						{
							if (!par.IsNumeric)
								continue;
							ParamsCount++;
							// пишем идентификатор объекта
							writer.Write((UInt32)(par.Owner.Owner as BaseControlObj).Number);
							// пишем идентификатор параметра
							writer.Write((UInt32)par.ParamID);
							// пишем идентификатор типа
							writer.Write((UInt32)par.ParamType);
							// пишем имя параметра
							byte[] name = Encoding.Default.GetBytes(par.ShortDescription);
							writer.Write((UInt16)name.Length);
							writer.Write(name);
							// пишем описание параметра
							name = Encoding.Default.GetBytes(par.Description);
							writer.Write((UInt16)name.Length);
							writer.Write(name);
							// пишем норамли
							writer.Write((float)par._normalMax);
							writer.Write((float)par._normalMin);
							writer.Write((float)par._normalExt);
						}

						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ParamsCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);

				#endregion
				///////////////////////////////////////

				///////////////////////////////////////
				#region записываем НСИ нечисловых парметров
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;
				foreach (UniObjSystem ObjSys in _idsysTOrefMAP)
				{
					// если требуются НСИ по станции
					if (_bAllSites || _reqSites.Contains(ObjSys.SiteID))
					{
						// пишем идентификатор
						writer.Write((uint)ObjSys.SiteID);
                        // указываем количество парков - т.к их нет - то 1
                        writer.Write(1);
                        //Указываем длину наименования
                        writer.Write((byte)0);
                        //Указываем пустое наименование 
                        byte[] park_name = Encoding.Default.GetBytes(string.Empty);
                        writer.Write(park_name);
                        // увеличиваем счетчик
                        SiteCount++;
						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 ParamsCount = 0;
						// пишем количество записей
						writer.Write(ParamsCount);
						// по всем объектам в словаре
						foreach (UniObjData par in ObjSys.UniParamsDictionary.Values)
						{
							if (par.IsNumeric)
								continue;
							ParamsCount++;
							// пишем идентификатор объекта
							writer.Write((UInt32)(par.Owner as BaseControlObj).Number);
							// пишем идентификатор параметра
							writer.Write((UInt32)par.ParamID);
							// пишем идентификатор типа
							writer.Write((UInt32)par.ParamType);
							// пишем имя параметра
							byte[] name = System.Text.Encoding.Default.GetBytes(par.ShortDescription);
							writer.Write((UInt16)name.Length);
							writer.Write(name);
							// пишем описание параметра
							name = System.Text.Encoding.Default.GetBytes(par.Description);
							writer.Write((UInt16)name.Length);
							writer.Write(name);
						}

						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(ParamsCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				///////////////////////////////////////
				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32)(pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);

				#endregion
				///////////////////////////////////////
			}
			else
			{
				writer.Write(Size);
				writer.Write(Size);
			}
			#endregion
			////////////////////////////////////////////

			////////////////////////////////////////////
			#region запись НСИ работ
			////////////////////////////////////////////
			Size = 0;
			if (IsNeedNSIWorks)
			{ 
				//записываем размер данных 
				writer.Write(Size);
				pos1 = writer.BaseStream.Position;
				// записываем количество станций
				writer.Write(Size);
				// цикл по станциям
				SiteCount = 0;
				foreach( UniObjSystem ObjSys in _idsysTOrefMAP )
				{
					// если требуются НСИ по станции
					if( _bAllSites || _reqSites.Contains(ObjSys.SiteID) )
					{
						// пишем идентификатор
						writer.Write((UInt32)ObjSys.SiteID);
                        // указываем количество парков - т.к их нет - то 1
                        writer.Write(1);
                        //Указываем длину наименования
                        writer.Write((byte)0);
                        //Указываем пустое наименование 
                        byte[] park_name = Encoding.Default.GetBytes(string.Empty);
                        writer.Write(park_name);
                        // увеличиваем счетчик
                        SiteCount++;
						// запоминаем позицию для записи количества элементов
						pos3 = writer.BaseStream.Position;
						UInt32 WorkCount = 0;
						// пишем количество записей
						writer.Write(WorkCount);
						// по всем объектам в словаре
						foreach (IOBaseElement elem in ObjSys.UniObjsDictionary.Values)
						{
							UniObject obj;
							if (elem is UniObject)
								obj = elem as UniObject;
							else
								continue;

							//TODO: будет полная коллекци всех возможных диагнозов вместо obj.DiagStates
							foreach( UInt32 punkt in obj.Punkts)
							{
								WorkCount++;
								// пишем идентификатор объекта
								writer.Write((UInt32) obj.Number);
								// пишем идентификатор типа
								writer.Write(punkt);
							}
						}
						writer.Flush();
						pos4 = writer.BaseStream.Position;
						writer.Seek((int)(pos3 - pos4), SeekOrigin.Current);
						writer.Write(WorkCount);
						writer.Flush();
						writer.Seek((int)(pos4 - pos3 - 4), SeekOrigin.Current);
					}
				}

				// записываем размеры и счетчик станций
				writer.Flush();
				pos2 = writer.BaseStream.Position;
				Size = (UInt32) (pos2 - pos1);
				writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
				writer.Write(Size);
				writer.Write(SiteCount);
				writer.Flush();
				writer.Seek((int)(pos2 - pos1), SeekOrigin.Current);
			}
			else
				writer.Write(Size);
			/////////////////////////////////////////
			#endregion
			////////////////////////////////////////////

			writer.Flush();
			writer.Close();
			_Answer = new Message();
			// устанавливаем код ответа
			_Answer.Code = (RequestCode)3;
			// записываем данные
			_Answer.Data = ms.ToArray();
			// Запись данных для отладки
			File.WriteAllBytes(string.Format("{0}.bin", _reqSites[0]), _Answer.Data);
			ms.Close();
		}
		/// <summary>
		/// Записать данные для текущего ответа
		/// </summary>
		/// <param name="writer">поток записи</param>
		protected void WriteCurAnswerData(BinaryWriter writer)
		{
			UInt32 SiteCount;
			UnificationConcentrator.UniObjState state;
			UnificationConcentrator.UniDiagState diag;
			UnificationConcentrator.UniNumericParamState numpar;
			UnificationConcentrator.UniDataParamState datapar;
			UnificationConcentrator.UniObjWork work;
			UInt32 Size = 0;
			long pos1, pos2;

			///////////////////////////////////////
			#region записываем данные по состояниям
			Size = 0;
			//записываем размер данных 
			writer.Write(Size);
			pos1 = writer.BaseStream.Position;
			// записываем количество станций
			writer.Write(Size);
			// цикл по станциям
			SiteCount = 0;
			foreach( UInt32 SiteID in _curAnswerStates.Keys )
			{
				Queue<UnificationConcentrator.UniObjState> queue = _curAnswerStates[SiteID];
				// если есть данные по станции
				if( queue.Count > 0 )
				{
					// пишем идентификатор
					writer.Write(SiteID);
					// увеличиваем счетчик
					SiteCount++;
					// пишем количество записей
					writer.Write((UInt32) queue.Count);
					// пока есть данные в очереди
					while( queue.Count > 0 )
					{
						// получаем состояние
						state = queue.Dequeue();
						// пишем идентификатор объекта
						writer.Write((uint)state._objectID);
						// пишем идентификатор состояния
						writer.Write((byte)state._uniState);

						Debug.WriteLine("\tпередано: obj " + state._siteID + ":" + state._objectID + " in " + state._uniState);
					}
				}
			}
			///////////////////////////////////////
			// записываем размеры и счетчик станций
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			Size = (UInt32) (pos2 - pos1);
			writer.Seek((int) (pos1 - pos2 - 4), SeekOrigin.Current);
			writer.Write(Size);
			writer.Write(SiteCount);
			writer.Flush();
			writer.Seek((int) (pos2 - pos1 - 4), SeekOrigin.Current);
			#endregion
			///////////////////////////////////////

			///////////////////////////////////////
			#region записываем данные по диагностике
			Size = 0;
			//записываем размер данных 
			writer.Write(Size);
			pos1 = writer.BaseStream.Position;
			// записываем количество станций
			writer.Write(Size);
			// цикл по станциям
			SiteCount = 0;
			foreach(UInt32 SiteID in _curAnswerDiags.Keys )
			{
				Queue<UnificationConcentrator.UniDiagState> queue = _curAnswerDiags[SiteID];
				// если есть данные по станции
				if( queue.Count > 0 )
				{
					// пишем идентификатор
					writer.Write(SiteID);
					// увеличиваем счетчик
					SiteCount++;
					// пишем количество записей
					writer.Write((UInt32)queue.Count);
					// пока есть данные в очереди
					while( queue.Count > 0 )
					{
						// получаем состояние
						diag = queue.Dequeue();
						// пишем идентификатор объекта
						writer.Write((uint)diag._objectID);
						// пишем идентификатор состояния
						writer.Write((UInt16)diag._uniDiagState);
						// пишем время
						writer.Write((Int32) Afx.CTime.ToTimeT(diag._detectTime) );
						writer.Write((Int32) Afx.CTime.ToTimeT(diag._endTime) );

						Debug.WriteLine(string.Format("\tпередано: obj {0}:{1} diag {2} started at {3}, ended at {4}"
											, diag._siteID
											, diag._objectID
											, diag._uniDiagState
											, diag._detectTime
											, diag._endTime));

					}
				}
			}
			///////////////////////////////////////
			// записываем размеры и счетчик станций
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			Size = (UInt32) (pos2 - pos1);
			writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
			writer.Write(Size);
			writer.Write(SiteCount);
			writer.Flush();
			writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			#endregion
			///////////////////////////////////////

			///////////////////////////////////////
			#region записываем данные по числовым параметрам
			Size = 0;
			//записываем размер данных 
			writer.Write(Size);
			pos1 = writer.BaseStream.Position;
			// записываем количество станций
			writer.Write(Size);
			// цикл по станциям
			SiteCount = 0;
			foreach(UInt32 SiteID in _curAnswerNumParams.Keys )
			{
				Queue<UnificationConcentrator.UniNumericParamState> queue = _curAnswerNumParams[SiteID];
				// если есть данные по станции
				if( queue.Count > 0 )
				{
					// пишем идентификатор
					writer.Write(SiteID);
					// увеличиваем счетчик
					SiteCount++;
					// пишем количество записей
					writer.Write((UInt32) queue.Count);
					// пока есть данные в очереди
					while( queue.Count > 0 )
					{
						// получаем состояние
						numpar = queue.Dequeue();
						// пишем идентификатор объекта
						writer.Write((uint)numpar._paramID);
						// пишем время
						writer.Write((Int32) Afx.CTime.ToTimeT(numpar._paramTime) );
						// пишем значение
						if (numpar._paramValue == float.MaxValue || float.IsNaN(numpar._paramValue) || float.IsInfinity(numpar._paramValue))
							writer.Write((UInt32) 0xFFFFFFFF);
						else
							writer.Write((float)numpar._paramValue);
					}
				}
			}
			///////////////////////////////////////
			// записываем размеры и счетчик станций
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			Size = (UInt32) (pos2 - pos1);
			writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
			writer.Write(Size);
			writer.Write(SiteCount);
			writer.Flush();
			writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			#endregion
			///////////////////////////////////////

			///////////////////////////////////////
			#region записываем данные по нечисловым параметрам
			Size = 0;
			//записываем размер данных 
			writer.Write(Size);
			pos1 = writer.BaseStream.Position;
			// записываем количество станций
			writer.Write(Size);
			// цикл по станциям
			SiteCount = 0;
			foreach(UInt32 SiteID in _curAnswerDataParams.Keys )
			{
				Queue<UnificationConcentrator.UniDataParamState> queue = _curAnswerDataParams[SiteID];
				// если есть данные по станции
				if( queue.Count > 0 )
				{
					// пишем идентификатор
					writer.Write(SiteID);
					// увеличиваем счетчик
					SiteCount++;
					// пишем количество записей
					writer.Write((UInt32) queue.Count);
					// пока есть данные в очереди
					while( queue.Count > 0 )
					{
						// получаем состояние
						datapar = queue.Dequeue();
						// пишем идентификатор объекта
						writer.Write((uint)datapar._paramID);
						// пишем длину данных
						writer.Write((UInt16) datapar._paramValue.Length);
						// пишем данные
						writer.Write(datapar._paramValue);
					}
				}
			}
			///////////////////////////////////////
			// записываем размеры и счетчик станций
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			Size = (UInt32) (pos2 - pos1);
			writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
			writer.Write(Size);
			writer.Write(SiteCount);
			writer.Flush();
			writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
			#endregion
			///////////////////////////////////////

			///////////////////////////////////////
			#region записываем данные по работам
			Size = 0;
			//записываем размер данных 
			writer.Write(Size);
			pos1 = writer.BaseStream.Position;
			// записываем количество станций
			writer.Write(Size);
			// цикл по станциям
			SiteCount = 0;
			foreach (UInt32 SiteID in _curAnswerWorks.Keys)
			{
				Queue<UnificationConcentrator.UniObjWork> queue = _curAnswerWorks[SiteID];
				// если есть данные по станции
				if( queue.Count > 0 )
				{
					// пишем идентификатор
					writer.Write(SiteID);
					// увеличиваем счетчик
					SiteCount++;
					// пишем количество записей
					writer.Write((UInt32) queue.Count);
					// пока есть данные в очереди
					while( queue.Count > 0 )
					{
						// получаем состояние
						work = queue.Dequeue();
						// пишем флаг
						writer.Write((byte)work._flag);
						// пишем идентификатор объекта
						writer.Write(work._objectID);
						// пишем идентификатор работы
						writer.Write(work._workID);
						// пишем идентификатор инструкции
						writer.Write(work._punktID);
						// пишем время
						writer.Write((Int32) Afx.CTime.ToTimeT(work._beginTime) );
						writer.Write((Int32) Afx.CTime.ToTimeT(work._endTime) );

						// пишем количество диагнозов
						writer.Write((byte)work._diags.Count);
						foreach(UInt16 id in work._diags.Keys )
						{
							writer.Write(id);
							writer.Write((Int32) Afx.CTime.ToTimeT(work._diags[id]));
						}
					}
				}
			}
			///////////////////////////////////////
			// записываем размеры и счетчик станций
			writer.Flush();
			pos2 = writer.BaseStream.Position;
			Size = (UInt32) (pos2 - pos1);
			writer.Seek((int)(pos1 - pos2 - 4), SeekOrigin.Current);
			writer.Write(Size);
			writer.Write(SiteCount);
			writer.Flush();
			writer.Seek((int)(pos2 - pos1 - 4), SeekOrigin.Current);
            #endregion
            ///////////////////////////////////////
            #region Данные о состоянии сигналов ТС (пишем 0)
            Size = 0;
            //записываем размер данных 0
            writer.Write(Size);

            #endregion
            ///////////////////////////////////////
            #region Блок дополнительных данных источника
            Size = 0;
            //записываем размер данных (0)
            writer.Write(Size);
            #endregion
        }

        /// <summary>
        /// создать списки по идентификатору
        /// </summary>
        /// <param name="SiteID"></param>
        protected void AddSiteToDictionaries(UInt32 SiteID)
		{
			Queue<UnificationConcentrator.UniObjState> queue;
			if (!_curAnswerStates.TryGetValue(SiteID, out queue))
			{
				_curAnswerStates.Add(SiteID, new Queue<UnificationConcentrator.UniObjState>());
				_curAnswerDiags.Add(SiteID, new Queue<UnificationConcentrator.UniDiagState>());
				_curAnswerNumParams.Add(SiteID, new Queue<UnificationConcentrator.UniNumericParamState>());
				_curAnswerDataParams.Add(SiteID, new Queue<UnificationConcentrator.UniDataParamState>());
				_curAnswerWorks.Add(SiteID, new Queue<UnificationConcentrator.UniObjWork>());
			}
		}
		/// <summary>
		/// Создать списки для накопления передаваемых данных в текущем ответе
		/// </summary>
		protected void CreateCurAnswerDictionaries()
		{
			// поиск в первом уровне вложенности
			foreach (DlxObject obj in _siteList.Objects)
			{
				Site s = obj as Site;
				foreach(DlxObject sub in s.Objects)
				{
					UniObjSystem objSys = sub as UniObjSystem;
					if (objSys != null && objSys.Name.StartsWith("Objects"))
					{
						AddSiteToDictionaries(objSys.SiteID);
						_idsysTOrefMAP.Add(objSys);
					}
				}
			}
			// если ничего не нашли
			if (_curAnswerStates.Count == 0)
			{
				// поиск во втором уровне вложенности
				foreach (DlxCollection subcoll in _siteList.Objects)
				{
					if (subcoll != null)
					{
						foreach (DlxObject obj in subcoll.Objects)
						{
							Site s = obj as Site;
							foreach (DlxObject sub in s.Objects)
							{
								UniObjSystem objSys = sub as UniObjSystem;
								if (objSys != null && objSys.Name.StartsWith("Objects"))
								{
									AddSiteToDictionaries(objSys.SiteID);
									_idsysTOrefMAP.Add(objSys);
								}
							}
						}
					}
				}
			}
		}

		#region Проверка требований НСИ
		/// <summary>
		/// Требуется ли НСИ объектов
		/// </summary>
		public bool IsNeedNSIObjects
		{
			get{ return (_ReqType & 1) == 1;}
		}
		/// <summary>
		/// Требуется ли НСИ ситуация
		/// </summary>
		public bool IsNeedNSIDiag
		{
			get{ return (_ReqType & 2) == 2;}
		}
		/// <summary>
		/// Требуется ли НСИ параметров
		/// </summary>
		public bool IsNeedNSIParams
		{
			get{ return (_ReqType & 4) == 4;}
		}
		/// <summary>
		/// Требуется ли НСИ работ
		/// </summary>
		public bool IsNeedNSIWorks
		{
			get{ return (_ReqType & 8) == 8;}
		}

		#endregion

		#region Проверка требований текущего состояния и архива
		/// <summary>
		/// Требуется ли состояния
		/// </summary>
		public bool IsNeedObjectStates
		{
			get{ return (_ReqType & 1) == 1;}
		}
		/// <summary>
		/// Требуется ли ситуации
		/// </summary>
		public bool IsNeedObjectDiags
		{
			get{ return (_ReqType & 2) == 2;}
		}
		/// <summary>
		/// Требуется ли измерения
		/// </summary>
		public bool IsNeedObjectsNumericParams
		{
			get{ return (_ReqType & 4) == 4;}
		}
		/// <summary>
		/// Требуется ли данные
		/// </summary>
		public bool IsNeedObjectsDataParams
		{
			get{ return (_ReqType & 8) == 8;}
		}
		/// <summary>
		/// Требуется ли факты работ
		/// </summary>
		public bool IsNeedFactWorks
		{
			get{ return (_ReqType & 0x10) == 0x10;}
		}
		/// <summary>
		/// Требуется ли планы работ
		/// </summary>
		public bool IsNeedPlanWorks
		{
			get{ return (_ReqType & 0x20) == 0x20;}
		}

		#endregion

		#region Данные
		/// <summary>
		/// ссылка на концентратор, который обрабатывает данные
		/// </summary>
		protected Concentrator _concentrator = null;
		/// <summary>
		/// ссылка на коллекцию контролируемых станций
		/// </summary>
		protected DlxCollection _siteList = null;
		/// <summary>
		/// очередь для данных 
		/// </summary>
		protected Queue _dataQueue = new Queue();
		/// <summary>
		/// событие для извещения о поступлении данных для отправки
		/// </summary>
		protected AutoResetEvent _eventOnData = new AutoResetEvent(false);
		/// <summary>
		/// последнее записанное в сообщении время в формате time_t
		/// </summary>
		protected int _lastTime;
		/// <summary>
		/// признак включения спорадического режима
		/// </summary>
		protected bool _bSporadic = false;
		/// <summary>
		/// Признак ожидания первого запроса - для идентификации клиента
		/// </summary>
		protected bool _bWaitFirstRequest = true;
		/// <summary>
		/// Признак запроса всех станций
		/// </summary>
		protected bool _bAllSites = false;
		/// <summary>
		/// Признак режима запроса архива
		/// </summary>
		protected bool _bArchiveMode = false;
		/// <summary>
		/// Заранее подготовленный ответ
		/// </summary>
		protected Message _Answer = null;
		/// <summary>
		/// Требования клиента по спорадической передаче
		/// </summary>
		protected UInt32 _ReqType = 0;
		/// <summary>
		/// Затребованные клиентом станции
		/// </summary>
		protected List<uint> _reqSites = new List<uint>();
		/// <summary>
		/// Код ответа при спорадиченском режиме
		/// </summary>
		protected byte _sporadicAnswerCode = 0x1;
		/// <summary>
		/// Номер версии НСИ
		/// </summary>
		protected UInt32 _versionNSI = 10;
		/// <summary>
		/// MAP для накопления состояний по станциям для передачи в текущем ответе
		/// </summary>
		protected Dictionary<UInt32, Queue<UnificationConcentrator.UniObjState>> _curAnswerStates = new Dictionary<uint, Queue<UnificationConcentrator.UniObjState>>();
		/// <summary>
		/// MAP для накопления ситуаций по станциям для передачи в текущем ответе
		/// </summary>
		protected Dictionary<UInt32, Queue<UnificationConcentrator.UniDiagState>> _curAnswerDiags = new Dictionary<uint, Queue<UnificationConcentrator.UniDiagState>>();
		/// <summary>
		/// MAP для накопления измерений по станциям для передачи в текущем ответе
		/// </summary>
		protected Dictionary<UInt32, Queue<UnificationConcentrator.UniNumericParamState>> _curAnswerNumParams = new Dictionary<uint, Queue<UnificationConcentrator.UniNumericParamState>>();
		/// <summary>
		/// MAP для накопления данных по станциям для передачи в текущем ответе
		/// </summary>
		protected Dictionary<UInt32, Queue<UnificationConcentrator.UniDataParamState>> _curAnswerDataParams = new Dictionary<uint, Queue<UnificationConcentrator.UniDataParamState>>();
		/// <summary>
		/// MAP для накопления работ по станциям для передачи в текущем ответе
		/// </summary>
		protected Dictionary<UInt32, Queue<UnificationConcentrator.UniObjWork>> _curAnswerWorks = new Dictionary<uint, Queue<UnificationConcentrator.UniObjWork>>();
		/// <summary>
		/// MAP станций
		/// </summary>
		private List<UniObjSystem> _idsysTOrefMAP = new List<UniObjSystem>();

		#endregion
	}

	/// <summary>
	/// Фабрика соединений по унифицированному протоколу второй редакции
	/// </summary>
	public class UnificServerConnectionFactory : UnificServerConnectionOldFactory
	{
		/// <summary>
		/// констркутор
		/// </summary>
		public UnificServerConnectionFactory()
		{
		}
		/// <summary>
		/// Создание серверного соединения
		/// </summary>
		/// <returns></returns>
		public override AConnection CreateConnection()
		{
			return new UnificServerConnection(_concentrator, _siteList);
		}
	}

	/// <summary>
	/// Сервер унифицированного информационного обмена - новая версия
	/// </summary>
	public class UnificationServer : Server
	{
		/// <summary>
		/// Констркутор
		/// </summary>
		public UnificationServer()
		{
		}
		public override void Load(AfxEx.DlxLoader Loader)
		{
			base.Load(Loader);

			//Загрузка интервала времени актуальности данных
			StartTimeout = Loader.GetAttributeInt("StartTimeout", StartTimeout);
		}
		/// <summary>
		/// Создание соединения
		/// </summary>
		/// <param name="client">tcp клиент</param>
		/// <returns>соединение</returns>
		public override AConnection CreateConnection(Socket client)
		{
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)400, "Подключение клиента с удаленного адреса " + IPAddress.Parse(((IPEndPoint)client.RemoteEndPoint).Address.ToString()));
			// создаем соединение унификации
			return new UnificConnection(client, true);
		}
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			// задержка старта сервера на 30 секунд
			Thread.Sleep(30000 + StartTimeout * 1000);
			base.Create();
		}
		/// <summary>
		/// Дополнительный таймаут для разнесения моментов запуска служб
		/// </summary>
		protected int StartTimeout = 0;
	}

	#endregion
}
