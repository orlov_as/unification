﻿using System;

namespace UnificationDebug
{
	public interface IDebugConcentrator
	{
		void GuardElement(Ivk.IOSys.IOBaseElement elem);
		void StopAllClients();
	}
}
