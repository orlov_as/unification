﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Linq;
using AfxEx;
using Ivk.IOSys;

namespace Tdm.Unification
{
	#region Стрелки

	/// <summary>
	/// Базовый класс состояния
	/// </summary>
	public class BaseObjState : UniObjectUpSt.ObjectState
	{
		/// <summary>
		/// Получаем сигнал у объекта контроля и подписываемся на рассылку его изменения
		/// </summary>
		/// <param name="sigName">Имя сигнала</param>
		/// <param name="signdl">Логика сигналов для инициализации</param>
		/// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
		public bool AssignToSig(string sigName, out ISigDiscreteLogic signdl)
		{
			signdl = null;
			if (sigName.Length == 0)
			{
				Console.WriteLine("BaseObjState: Can't get reference for signal without name for object " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			// Получаем и подписываемся на рассылку изменения для сигнала sigName
			ObjData objData = (_controlObj.GetObject(sigName) as ObjData);
			if (objData == null)
			{
				Console.WriteLine("BaseObjState: Can't find " + sigName + " in object " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			if (!objData.IsSigDiscretLogic)
			{
				Console.WriteLine("BaseObjState: " + sigName + " is not 'SigDiscretLogic' in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			foreach (IOSignal sig in objData.SigDiscreteLogic.IOSignals)
			{
				AddWatchToSignal(sig);
			}
			signdl = objData.SigDiscreteLogic;

			return true;
		}

        /// <summary>
        /// Получаем сигнал у объекта контроля и подписываемся на рассылку его изменения
        /// </summary>
        /// <param name="sigName">Имя сигнала</param>
        /// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
        protected bool AssignToSig(string sigName)
        {
            if (sigName.Length == 0)
            {
				Console.WriteLine("BaseObjState: Can't get reference for signal without name for object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            // Получаем и подписываемся на рассылку изменения для сигнала sigName
            ObjData objData = (_controlObj.GetObject(sigName) as ObjData);
            if (objData == null)
            {
				Console.WriteLine("BaseObjState: Can't find " + sigName + " in object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            if (!objData.IsSigDiscretLogic)
            {
				Console.WriteLine("BaseObjState: " + sigName + " is not 'SigDiscretLogic' in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            foreach (IOSignal sig in objData.SigDiscreteLogic.IOSignals)
            {
                AddWatchToSignal(sig);
            }

            return true;
        }

		/// <summary>
		/// Получаем сигнал Sig1 или Sig2 у объекта контроля и подписываемся на рассылку его изменения
		/// </summary>
		/// <param name="signdl">Логика сигналов для инициализации</param>
		/// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
		protected bool AssignToSig1orSig2(out ISigDiscreteLogic signdl)
		{
			signdl = null;
			// Получаем и подписываемся на рассылку изменения для сигнала Ышп1
			string sigName = "Sig1";
			ObjData objData = (_controlObj.GetObject(sigName) as ObjData);
			if (objData == null)
			{
				Console.WriteLine("BaseObjState: Can't find task Sig1 in object " + _controlObj.Site.Name + "/" + _controlObj.Name + ". Try to find Sig2...");
                sigName = "Sig2";
                objData = (_controlObj.GetObject(sigName) as ObjData);
				if (objData == null)
				{
                    Console.WriteLine("BaseObjState: Can't find task Sig2 in object " + _controlObj.Site.Name + "/" + _controlObj.Name + ". Try to find Sig2...");
                    sigName = "Sig*";
                    objData = (_controlObj.GetObject(sigName) as ObjData);
                    if (objData == null)
                    {
                        Console.WriteLine("BaseObjState: Can't find task Sig* in object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                        return false;
                    }
                }
			}
			if (!objData.IsSigDiscretLogic)
			{
				Console.WriteLine("BaseObjState: " + sigName + " is not 'SigDiscretLogic' in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			foreach (IOSignal sig in objData.SigDiscreteLogic.IOSignals)
			{
				AddWatchToSignal(sig);
			}
			signdl = objData.SigDiscreteLogic;

			return true;
		}

		/// <summary>
		/// Получить сигнал sig у объекта obj и подписаться на рассылку изменения его состояния
		/// </summary>
		/// <param name="obj">Имя объекта содержащего сигнал</param>
		/// <param name="sig">Имя сигнала</param>
		/// <param name="sigdl">Сюда запишется полученая логика</param>
		/// <returns>true в случае успеха</returns>
		protected bool AssignToSigByObj(string obj, string sig, out ISigDiscreteLogic sigdl)
		{
			sigdl = null;
			// Получаем объект, содержащий сигнал
			ObjData objData = (_controlObj.GetObject(obj) as ObjData);
			if (objData == null)
			{
				Console.WriteLine("BaseObjState: Can't get object " + obj + " as ObjData of " + _controlObj.Site.Name + "/" + _controlObj.Name);
				Trace.WriteLine("BaseObjState: Can't get object " + obj + " as ObjData of " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			// получаем сигнал у объекта
			UniObject controlObject = objData.Object as UniObject;
			if (controlObject == null)
			{
				Console.WriteLine("BaseObjState: Can't get object " + obj + " as ObjData of " + _controlObj.Site.Name + "/" + _controlObj.Name);
				Trace.WriteLine("BaseObjState: Can't get object " + obj + " as ObjData of " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			ObjData sigObjData = (controlObject.GetObject(sig) as ObjData);
			if (sigObjData == null)
			{
				Console.WriteLine("BaseObjState: Can't get object " + sig + " as ObjData of " + obj + " in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				Trace.WriteLine("BaseObjState: Can't get object " + sig + " as ObjData of " + obj + " in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			// подписываемся на рассылку и запоминаем сигнал
			if (!sigObjData.IsSigDiscretLogic)
			{
				Console.WriteLine("BaseObjState: " + sig + " is not 'SigDiscretLogic' " + _controlObj.Site.Name + "/" + _controlObj.Name);
				Trace.WriteLine("BaseObjState: " + sig + " is not 'SigDiscretLogic' " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			foreach (IOSignal signal in sigObjData.SigDiscreteLogic.IOSignals)
			{
				AddWatchToSignal(signal);
			}
			sigdl = sigObjData.SigDiscreteLogic;

			return true;
		}

        /// <summary>
        /// Получить объект, на который ссылается член-данное objDataName у объекта контроля
        /// </summary>
        /// <param name="objDataName">Имя член-данного, которой ссылается на объект</param>
        /// <returns>true, если член-данное objDataName есть у объекта контроля и ссылается оно на объект.
        /// в остальных случаях - false</returns>
        protected IOBaseElement GetObjectRef(string objDataName)
        {
            return GetObjectRef(_controlObj, objDataName);
        }

        /// <summary>
        /// Получить объект, на который ссылается член-данное objDataName у объекта ControlObject
        /// </summary>
        /// <param name="ControlObject">Ссылка на объект контроля, по член-данному которого пытаемся получить объект</param>
        /// <param name="objDataName">Имя член-данного</param>
        /// <returns>true, если член-данное objDataName есть у объекта контроля ControlObject и ссылается оно на объект.
        /// в остальных случаях - false</returns>
        protected IOBaseElement GetObjectRef(IOBaseElement ControlObject, string objDataName)
        {
            // Получаем объект как член-данное объекта контроля
            ObjData objData = (ControlObject.GetObject(objDataName) as ObjData);
            if (objData == null)
            {
                //Console.WriteLine("BaseObjState: Can't get object " + objDataName + " as ObjData of " + ControlObject.Name);
				Trace.WriteLine("BaseObjState: Can't get object " + objDataName + " as ObjData of " + ControlObject.Name + " in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return null;
            }

            // Преобразуем член-данное в объект
            IOBaseElement objElement = objData.Object as IOBaseElement;
            if (objElement == null)
            {
				Trace.WriteLine("BaseObjState: Can't convert objdata " + objDataName + " to IOBaseElement in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return null;
            }

            return objElement;
        }

        /// <summary>
        /// Подписаться на изменение состояния объекта, указанного в член-данном объекта контроля
        /// </summary>
        /// <param name="objDataName">Имя член-данного</param>
        /// <returns>true, если член-данное objDataName есть у объекта и ссылается оно на объект.
        /// в остальных случаях - false</returns>
		protected bool AssignToObj(string objDataName)
		{
			return AssignToObj(_controlObj, objDataName);
		}

        /// <summary>
        /// Подписаться на изменение состояния объекта ControlObj
        /// </summary>
        /// <param name="ControlObj">Ссылка на объект</param>
        protected void AssignToObj(IOBaseElement ControlObj)
        {
            // Подписываемся на изменение его состояния
            ControlObj.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
        }

        /// <summary>
        /// Подписаться на изменение состояния объекта, указанного в член-данном objDataName объекта контроля ControlObj
        /// </summary>
        /// <param name="ControlObj">Ссылка на объект контроля</param>
        /// <param name="objDataName">Имя член-данного</param>
        /// <returns>true, если член-данное objDataName есть у объекта ControlObj и ссылается оно на объект.
        /// в остальных случаях - false</returns>
        protected bool AssignToObj(IOBaseElement ControlObj, string objDataName)
        {
            // Получаем объект по член-данному
            IOBaseElement objElement = GetObjectRef(ControlObj, objDataName);
            if (objElement == null)
                return false;

            // Подписываемся на изменение его состояния
            objElement.DataChanged += new IOBaseElement.OnDataChangedHandler(OnChanged);
            return true;
        }

		/// <summary>
		/// Получаем первй аналоговый сигнал у объекта контроля и подписываемся на рассылку его изменения
		/// </summary>
		/// <param name="sig">Логика сигналов для инициализации</param>
		/// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
		protected bool AssignToFirstAnalogSig(out ISigAnalog sig)
		{
			sig = null;
			foreach (AfxEx.DlxObject obj in _controlObj.ObjDataCollection.Objects)
			{
                ObjData data = obj as ObjData;
                if (data != null && data.IsSigAnalog)
                {
                    sig = data.SigAnalog;
                    break;
                }
			}
            if (sig == null)
            {
                return false;
            }
			foreach (IOSignal signal in sig.IOSignals)
			{
				AddWatchToSignal(signal);
			}
			return true;
		}
        /// <summary>
        /// Получаем первй аналоговый сигнал у объекта контроля и подписываемся на рассылку его изменения
        /// </summary>
        /// <param name="sig">Логика сигналов для инициализации</param>
        /// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
        protected bool AssignToUniObjData(string Name, out UniObjData sig)
        {
            sig = null;
            foreach (AfxEx.DlxObject obj in _controlObj.ObjDataCollection.Objects)
            {
                UniObjData data = obj as UniObjData;
                if (data != null && data.Name == Name)
                {
                    sig = data;
                    break;
                }
            }
            if (sig == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Получаем первый аналоговый или дискретный сигнал у объекта контроля и подписываемся на рассылку его изменения
        /// </summary>
        /// <param name="sig">Логика сигналов для инициализации</param>
        /// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
        protected bool AssignToFirstAnalogOrDiscreteSig(out ISigAnalog aSig, out ISigDiscreteLogic dSig)
        {
            aSig = null;
            dSig = null;
            foreach (AfxEx.DlxObject obj in _controlObj.ObjDataCollection.Objects)
            {
                ObjData data = obj as ObjData;
                Trace.WriteLine("Есть сигнал " + _controlObj.Name + ",  " + data.FullName + ", аналоговый=" + data.IsSigAnalog.ToString() + ", IsSigDiscretLogic=" + data.IsSigDiscretLogic.ToString() + ", IsSigCode=" + data.IsSigCode.ToString());
                if (data != null && data.IsSigAnalog)
                {
                    aSig = data.SigAnalog;
                    break;
                }
                else if (data != null && data.IsSigDiscretLogic)
                {
                    dSig = data.SigDiscreteLogic;
                    break;
                }
            }
            if (aSig == null && dSig == null)
            {
                Trace.WriteLine("Беда, нет ни аналогового, ни дискретного, из " + _controlObj.ObjDataCollection.Objects.Count.ToString() + " сигналов объекта контроля");
                return false;
            }
            if(aSig != null)
                foreach (IOSignal signal in aSig.IOSignals)
                {
                    AddWatchToSignal(signal);
                }
            else if (dSig != null)
                foreach (IOSignal signal in dSig.IOSignals)
                {
                    AddWatchToSignal(signal);
                }

            return true;
        }
        /// <summary>
        /// Получаем ссылку на модуль, которому принадлежит сигнал
        /// </summary>
        /// <param name="sigName">Имя сигнала</param>
        /// <param name="baseElem">Ссылка на модуль</param>
        protected bool AssignToModule(string sigName, out IOBaseElement baseElem)
        {
            baseElem = null;
            ISigAnalog aSig = null;
            ISigDiscreteLogic dSig = null;
            foreach (AfxEx.DlxObject obj in _controlObj.ObjDataCollection.Objects)
            {
                ObjData data = obj as ObjData;
                Trace.WriteLine("Есть сигнал " + _controlObj.Name + ",  " + data.FullName + ", аналоговый=" + data.IsSigAnalog.ToString() + ", IsSigDiscretLogic=" + data.IsSigDiscretLogic.ToString() + ", IsSigCode=" + data.IsSigCode.ToString());
                if (data != null && data.IsSigAnalog)
                {
                    aSig = data.SigAnalog;
                    break;
                }
                else if (data != null && data.IsSigDiscretLogic)
                {
                    dSig = data.SigDiscreteLogic;
                    break;
                }

                object o = data.Object as IOModule;

            }
            if (aSig == null && dSig == null)
            {
                Trace.WriteLine("Беда, нет ни аналогового, ни дискретного, из " + _controlObj.ObjDataCollection.Objects.Count.ToString() + " сигналов объекта контроля");
                return false;
            }
            if (aSig != null)
                foreach (IOSignal signal in aSig.IOSignals)
                {
                    AddWatchToSignal(signal);
                    baseElem = signal.Owner as IOBaseElement;
                }
            else if (dSig != null)
                foreach (IOSignal signal in dSig.IOSignals)
                {
                    AddWatchToSignal(signal);
                    baseElem = signal.Owner as IOBaseElement;
                }

            return true;
        }
        /// <summary>
        /// Получаем ссылку на модуль, которому принадлежит сигнал
        /// </summary>
        /// <param name="sigName">Имя сигнала</param>
        /// <param name="baseElem">Ссылка на модуль</param>
        protected bool AssignToModule(BaseControlObj bco, out IOBaseElement baseElem)
        {
            baseElem = null;
            // получаем ссылку на подсистему ввода (вариант конфигурации для тиражируемого ПО)
            IOSystem iosys = bco.Owner.Owner.GetObject("IOSys") as IOSystem;
            if (iosys == null)
            {   // получаем ссылку на подсистему ввода (вариант конфигурации для ПО ЛПД и ПО ИВК-ТДМ)
                DlxObject inner = bco.Owner.Owner.GetObject("Site");
                if (inner == null)
                    inner = bco.Owner.Owner.GetObject("Stage");

                if (inner == null)
                    return false;
                else
                    iosys = inner.GetObject("IOSys") as IOSystem;
            }
            baseElem = FindModule(iosys, GetIOSysLevels(bco.Name));

            AddWatchToModule(baseElem);

            #region ToSignal
            //IOModule m = baseElem as IOModule;
            //if (m != null)
            //{
            //    AddWatchToModule(m);

                //IOSignal sig = null;
                //if (m.Signals != null)
                //    sig = m.Signals.Objects.FirstOrDefault() as IOSignal;
                //else if (m.SubElements != null)
                //{
                //    m = m.SubElements.Objects.FirstOrDefault() as IOModule;
                //    if (m != null)
                //        sig = m.Signals.Objects.FirstOrDefault() as IOSignal;
                //}
                //if (sig != null)
                //    AddWatchToSignal(sig);
            //}
            #endregion
            return true;
        }
        /// <summary>
        /// Получение списка элементов подсистемы ввода, содержащихся в пути к модулю
        /// </summary>
        /// <param name="modObjName">Имя объекта самодиагностики (содержит имя диагностируемого модуля)</param>
        /// <returns>Список элементов подсистемы ввода</returns>
        protected List<string> GetIOSysLevels(string modObjName)
        {
            return modObjName.Trim(new char [] { '\\' }).Split('\\').ToList();
        }
        
        protected IOBaseElement FindModule(IOSystem iosys, List<string> lvls)
        {
            IOBaseElement res = iosys;
            foreach (string s in lvls)
            {
                object o = res.GetObject(s);
                res = o == null ? res : o as IOBaseElement;
            }
            return res;
        }

		protected virtual void TraceSwitchState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if ((state & 0x1) != 0)
				traceStr += "[Нет контроля]";
			if ((state & 0x2) != 0)
				traceStr += "[Занятие]";
			if ((state & 0x4) != 0)
				traceStr += "[Замыкание]";
			if ((state & 0x8) != 0)
				traceStr += "[Исскуственная разделка]";
			if ((state & 0x20) != 0)
				traceStr += "[Плюсовое положение]";
			if ((state & 0x40) != 0)
				traceStr += "[Минусовое положение]";

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
			else
				Trace.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
		}
		protected virtual void TraceRCState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if ((state & 0x1) != 0)
				traceStr += "[Нет контроля]";
			if ((state & 0x2) != 0)
				traceStr += "[Занятие]";
			if ((state & 0x4) != 0)
				traceStr += "[Замыкание]";
			if ((state & 0x8) != 0)
				traceStr += "[Исскуственная разделка]";

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
			else
				Trace.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
		}
		protected virtual void TraceSignalState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if ((state & 0x1) != 0)
				traceStr += "[Нет контроля]";
			if ((state & 0x2) != 0)
				traceStr += "[Закрыт (красный указатель)]";
			if ((state & 0x4) != 0)
				traceStr += "[Открыт (поездной)]";
			if ((state & 0x8) != 0)
				traceStr += "[Открыт маневровый]";
			if ((state & 0x10) != 0)
				traceStr += "[Горит пригласительный]";
			if ((state & 0x20) != 0)
				traceStr += "[Горит ускоренные маневры]";
			if ((state & 0x80) != 0)
				traceStr += "[Отказ на светофоре (огневое) ]";

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
			else
				Trace.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
		}
		protected virtual void TraceIndicatorState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if ((state & 0x1) != 0)
				traceStr += "[Нет контроля]";
			if ((state & 0x2) != 0)
				traceStr += "[Горит]";
			if ((state & 0x4) != 0)
				traceStr += "[Мигает]";
			if ((state & 0x8) != 0)
				traceStr += "[Горит вторым цветом]";
			if ((state & 0x10) != 0)
				traceStr += "[Мигает вторым цветом]";
			if ((state & 0x20) != 0)
				traceStr += "[Горит третьим цветом]";
			if ((state & 0x40) != 0)
				traceStr += "[Мигает третьим цветом]";

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
			else
				Trace.WriteLine("BaseObjState: Сотояние " + _controlObj.Name + ": " + traceStr);
		}
	}

	/// <summary>
	/// Класс состояния, который при создании устанавливает отказное состояние
	/// </summary>
	public class DefObjState : BaseObjState 
	{
		/// <summary>
		/// Создание объекта состояния, с установкой его в отказ
		/// </summary>
		/// <param name="obj">Объект унификации</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if( !base.Create(obj) )
				return false;
			SetState(0x01);
			return true;
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи только с занятостья РЦ(RCSig)
	/// </summary>
	public class RCState_onlyRC : DefObjState
	{
		/// <summary>
		/// Сигнал занятости РЦ
		/// </summary>
		protected ISigDiscreteLogic _RCSig;
		
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("RCSig", out _RCSig))
				return false;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_RCSig}]
			int state = 0;
			try
			{
				// 1:[{m_RCSig}];
				if (_RCSig.On)
					state |= 0x2;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState_onlyRC " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceRCState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи с игналами RCSig и ZSig
	/// </summary>
	public class RCState_RC_Z : DefObjState
	{
		protected ISigDiscreteLogic _RCSig;
		protected ISigDiscreteLogic _ZSig;


		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("RCSig", out _RCSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для ZSig
			if (!AssignToSig("ZSig", out _ZSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_RCSig}];2:[{m_ZSig}];
			int state = 0;
			try
			{
				// 1:[{m_RCSig}];
				if ( _RCSig.On )
					state |= 0x2;
				// 2:[{m_ZSig}];
				if ( _ZSig.On )
					state |= 0x4;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState_RC_Z " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceRCState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи CSectionKvZKri с сигналами RCSig, ZSig, KRISig
	/// </summary>
	public class RCState : RCState_RC_Z
	{
		protected ISigDiscreteLogic _KRISig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("KRISig", out _KRISig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_RCSig}];2:[{m_ZSig}];3:[{m_KRISig}];
			int state = 0;
			try
			{
				// 1:[{m_RCSig}];
				if ( _RCSig.On )
					state |= 0x2;
				// 2:[{m_ZSig}];
				if ( _ZSig.On )
					state |= 0x4;
				// 3:[{m_KRISig}];
				if ( _KRISig.On )
					state |= 0x8;		
			}
			catch(ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceRCState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи CPathSection2uKvIAv с сигналами RCSig, NISig, ChISig, KRISig
	/// </summary>
	public class ObjStateRC_RC_NI_CHI_KRI : DefObjState
	{
		protected ISigDiscreteLogic _RCSig;
		protected ISigDiscreteLogic _NISig;
		protected ISigDiscreteLogic _ChISig;
		protected ISigDiscreteLogic _KRISig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("RCSig", out _RCSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для NISig
			if (!AssignToSig("NISig", out _NISig))
				return false;
			// Получаем и подписываемся на рассылку изменения для ChISig
			if (!AssignToSig("ChISig", out _ChISig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("KRISig", out _KRISig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_RCSig}];2:[{m_NISig} OR {m_ChISig}];3:[{m_KRISig}];
			int state = 0;
			try
			{
				// 1:[{m_RCSig}];
				if ( _RCSig.On )
					state |= 0x2;
				// 2:[{m_NISig} OR {m_ChISig}];
				if ( _NISig.On || _ChISig.On )
					state |= 0x4;
				// 3:[{m_KRISig}]
				if ( _KRISig.On )
					state |= 0x8;	
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateRC_RC_NI_CHI_KRI " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceRCState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние осноыной стрелочной секции
	/// </summary>
	public class SwitchState : DefObjState
	{
		protected ISigDiscreteLogic _PKSig;
		protected ISigDiscreteLogic _MKSig;
		protected UniObject _SwSection;
        
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			ObjData objData = (_controlObj.GetObject("SwSection") as ObjData);
			if (objData == null)
			{
				Debug.WriteLine("Can't get object SwSection as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			if( objData.Object is UniObject )
				_SwSection = objData.Object as UniObject;
			else
			{
				Debug.WriteLine("Can't convert ObjData for SwSection to UniObject!");
				return false;
			}

			// Получаем и подписываемся на рассылку изменения для _PKSig
			if (!AssignToSig("PKSig", out _PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _MKSig
			if (!AssignToSig("MKSig", out _MKSig))
				return false;

            // Получаем и подписываемся на рассылку изменения для SwSection
		    AssignToObj("SwSection");

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// SwSectionState | 5:[{m_PKSig}];6:[{m_MKSig}];
			int state;
			try
			{
				_SwSection.UpdateObjectState();
				state = _SwSection.ObjState.Current;
				// 5:[{m_PKSig}];
				if ( _PKSig.On )
					state |= 0x20;
				// 6:[{m_MKSig}];
				if ( _MKSig.On )
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("SwitchState " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние осноыной стрелочной секции
	/// </summary>
	public class SwitchSlaveState : DefObjState
	{
		protected UniObject _MainSwitch;
        protected UniObject _SwSection;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
            //получаем секцию
            ObjData objData = (_controlObj.GetObject("SwSection") as ObjData);
            if (objData != null)
            {
                if (objData.Object is UniObject)
                    _SwSection = objData.Object as UniObject;
                else
                {
                    Debug.WriteLine("Can't convert ObjData for SwSection to UniObject!");
                }    
            }
            
            //получаем основную стрелку
			objData = (_controlObj.GetObject("Main") as ObjData);
			if (objData == null)
			{
				Debug.WriteLine("Can't get object MainSwitch as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			if (objData.Object is UniObject)
				_MainSwitch = objData.Object as UniObject;
			else
			{
				Debug.WriteLine("Can't convert ObjData for MainSwitch to UniObject!");
				return false;
			}

            // Получаем и подписываемся на рассылку изменения для SwSection
            AssignToObj("SwSection");

            // Получаем и подписываемся на рассылку изменения для Main
            AssignToObj("Main");

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// SwSectionState | 5:[{m_PKSig}];6:[{m_MKSig}];
			int state = 0;
			try
			{
                if (_SwSection != null)
                {
                    _SwSection.UpdateObjectState();
                    state |= _SwSection.ObjState.Current;
                }

                // получаем плюс\минус у основной стрелки. (5,6 бит)
			    _MainSwitch.UpdateObjectState();
				state |= _MainSwitch.ObjState.Current & 0x60;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("SwitchSlaveState " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlave. Базовый. Создает SwSection_RCSig, SwSection_ZSig, SwSection_KRISig
	/// </summary>
	public class ObjStateSwitchSlave : DefObjState
	{
		protected ISigDiscreteLogic _SwSection_RCSig;
		protected ISigDiscreteLogic _SwSection_ZSig;
		protected ISigDiscreteLogic _SwSection_KRISig;
		/// <summary>
		/// Создание состояния. Получаем сигналы объекта SwSection и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SwitchSection_RCSig
			if (!AssignToSigByObj("SwSection", "RCSig", out _SwSection_RCSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SwitchSection_ZSig
			if (!AssignToSigByObj("SwSection", "ZSig", out _SwSection_ZSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SwitchSection_KRISig
			if (!AssignToSigByObj("SwSection", "KRISig", out _SwSection_KRISig))
				return false;
			return true;
		}
	}

	/// <summary>
	/// Состояние основной стрелочной секции с алгоритмом определения замыкания в стрелочной секции
	/// </summary>
	public class SwitchStateAnalyse : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _PKSig;
		protected ISigDiscreteLogic _MKSig;
		protected ISigDiscreteLogic _PreSecPKSig;
		protected ISigDiscreteLogic _PreSecMKSig;
		/// <summary>
		/// Стрелочная секция
		/// </summary>
		protected UniObject _SwSection;
		protected UniObject SwSection
		{
			get
			{
				if (_SwSection != null)
					return _SwSection;
				ObjData objData = (_controlObj.GetObject("SwSection") as ObjData);
				if (objData != null)
				{
					_SwSection = objData.Object as UniObject;
					return _SwSection;
				}
				return null;
			}
		}
		/// <summary>
		/// следующий объект по плюсу
		/// </summary>
		protected UniObject _NextPl;
		protected UniObject NextPl
		{
			get
			{
				if (_NextPl != null)
					return _NextPl;
				ObjData objData = (_controlObj.GetObject("NextPl") as ObjData);
				if (objData != null)
				{
					_NextPl = objData.Object as UniObject;
					return _NextPl;
				}
				return null;
			}
		}
		/// <summary>
		/// Следующий объект по минусу
		/// </summary>
		protected UniObject _NextMi;
		protected UniObject NextMi
		{
			get
			{
				if (_NextMi != null)
					return _NextMi;
				ObjData objData = (_controlObj.GetObject("NextMi") as ObjData);
				if (objData != null)
				{
					_NextMi = objData.Object as UniObject;
					return _NextMi;
				}
				return null;
			}
		}
		/// <summary>
		/// Предыдущий объект
		/// </summary>
		protected UniObject _PrevSec;
		protected UniObject PrevSec
		{
			get
			{
				if (_PrevSec != null)
					return _PrevSec;
				ObjData objData = (_controlObj.GetObject("PrevSec") as ObjData);
				if (objData != null)
				{
					_PrevSec = objData.Object as UniObject;
					return _PrevSec;
				}
				return null;
			}
		}

		/// <summary>
		/// Вариант расположения стрелки
		/// </summary>
		protected int _Variant;

		/// <summary>
		/// Возможные варианты расположения стрелки
		/// </summary>
		protected enum Variants
		{
			/// <summary>
			/// Стрелка является предыдущей для следующих. Для определения её занятости 
			/// смотрим на предыдущую стрелку. Если её нет, то замыкаем по состоянию рельсовой цепи
			/// Если она есть и переключена в наше направление, то устанавливаем состояние такое же как у неё.
			/// Если переключена в другое направление, значит наша стрелка не учавствует в маршруте (не замыкаем)
			/// </summary>
			swt1 = 0x01,
			/// <summary>
			/// Стрелка варианта swt1, причем крайняя в секции.
			/// </summary>
			swt1a = 0x02,
			/// <summary>
			/// Стрелка варианта swt1, причем не крайняя в секции и находится по плюсу
			/// от предыдущей. Если этот бит не установлен, то по минусу
			/// </summary>
			swt1P = 0x04,
			swt2 = 0x10
		}
		/// <summary>
		/// Принадлежит ли стрелка первому варианту (для всех следующих она - предыдущая)
		/// </summary>
		/// <returns>true, если условие выполняется</returns>
		protected bool IsFirstVariant()
		{
			// подучаем предыдущую стрелку следующей по плюсу в этой секции
			if( NextPl != null )
			{
				// определяем, находится ли следущая по плюсу в той же секции
				ObjData objDataSection = NextPl.GetObject("SwSection") as ObjData;
				if (objDataSection != null)
				{
					UniObject objSection = objDataSection.Object as UniObject;
					if (objSection == null)
						throw new ArgumentException("Не удалось получить SwSection для объекта " + NextPl.Name, "SwSection");
					//если объект находится в той же стрелочной секции
					if (objSection == _SwSection)
					{
						// получаем объект
						ObjData objData = (NextPl.GetObject("PrevSec") as ObjData);
						if (objData == null)
						{
							Debug.WriteLine("Не удалось получить PreSec для объекта " + NextMi.Name);
							return false;
						}
						UniObject obj = objData.Object as UniObject;
						// если предыдущая стрелка следующей по плюсу не является нашей стрелкой, то это не то.
						if (obj != _controlObj)
							return false;
					}
				}
				else 
					Debug.WriteLine("Не удалось получить SwSection для объекта " + NextPl.Name, "SwSection");
			}
		

			// получаем предыдущую стрелку следующей по минусу
			if (NextMi != null)
			{
				// определяем, находится ли следущая по минусу в той же секции
				ObjData objDataSection = NextMi.GetObject("SwSection") as ObjData;
				if (objDataSection != null)
				{
					UniObject objSection = objDataSection.Object as UniObject;
					if (objSection == null)
						throw new ArgumentException("Не удалось получить SwSection для объекта " + NextMi.Name);
					//если объект находится в той же стрелочной секции
					if (objSection == _SwSection)
					{
						ObjData objData = (NextMi.GetObject("PrevSec") as ObjData);
						if (objData == null)
						{
							Debug.WriteLine("Не удалось получить PreSec для объекта " + NextMi.Name);
							return false;
						}
						UniObject obj = objData.Object as UniObject;
						// если предыдущая стрелка следующей по плюсу не является нашей стрелкой, то это не то.
						if (obj != _controlObj)
							return false;
					}
				}
				else 
					Debug.WriteLine("Не удалось получить SwSection для объекта " + NextMi.Name);
			}

			return true;
		}

		/// <summary>
		/// Является ли стрелка крайней в стрелочной секции
		/// </summary>
		/// <returns>true, если крайняя стрелка</returns>
		protected bool IsLastInSection()
		{
			// получаем секцию предыдущего объекта. если он совпадает с нашей,
			// то это не крайняя стрелка
			if( PrevSec != null )
			{
				ObjData objData = (PrevSec.GetObject("SwSection") as ObjData);
				if (objData == null)
				{
					Debug.WriteLine("Не удалось получить секцию объекта " + PrevSec.Name);
					return true;
				}
				UniObject obj = objData.Object as UniObject;
				if( obj == null )
					throw new ArgumentException("Не удалось получить секцию объекта " + PrevSec.Name);
				//если секция предыдущего объекта такая же как текущего, то эта стрелка не последняя
				if( obj != _SwSection )
					return true;
				// если эта стрелка не последняя, то проверяем плюсовой контроль. если его нет, 
				// то предыдущая стрелка спаренная (наверно) и считаем текущую последней
				ObjData pkSig = (PrevSec.GetObject("PKSig") as ObjData);
				if( pkSig == null )
					return true;
				else
					return false;
			}
			return true;
		}
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			//определяем вариант расположения стрелки
			// если перед стрелкой секция заканчивается и для каждой следующей она является предидущей, то первый вариант

			if( SwSection == null )
			{
				Debug.WriteLine("Не удалось получить стрелочную секцию для {0}", _controlObj.Name);
				return false;
			}

			if (IsFirstVariant())
			{
				_Variant |= (int) Variants.swt1;
				if( IsLastInSection() )
					_Variant |= (int)Variants.swt1a;
				else
				{// Не крайняя стрелка. Получаем сигналы контроля предыдущей и кем мы ей приходжимся (плюс\минус)
					// плюсовой контроль
					ObjData objData = PrevSec.GetObject("PKSig") as ObjData;
					if (objData == null)
						throw new ArgumentException(_controlObj.Name + ": Не удалось получить плюсовой контроль объекта " + PrevSec.Name,"PreSec->PKSig");
					if (objData.IsSigDiscretLogic)
						_PreSecPKSig = objData.SigDiscreteLogic;
					else
						throw new ArgumentException(_controlObj.Name + ": Не удалось получить плюсовой контроль объекта " + PrevSec.Name, "PreSec->PKSig");
					// минусовой контроль
					objData = PrevSec.GetObject("MKSig") as ObjData;
					if (objData == null)
						throw new ArgumentException(_controlObj.Name + ": Не удалось получить минусовой контроль объекта " + PrevSec.Name, "PreSec->MKSig");
					if (objData.IsSigDiscretLogic)
						_PreSecMKSig = objData.SigDiscreteLogic;
					else
						throw new ArgumentException(_controlObj.Name + ": Не удалось получить минусовой контроль объекта " + PrevSec.Name, "PreSec->MKSig");

					// Плюс предыдущей стрелки
					objData = PrevSec.GetObject("NextPl") as ObjData;
					if (objData != null)
					{
						UniObject objNextPl = objData.Object as UniObject;
						if (objNextPl == null)
							throw new ArgumentException(_controlObj.Name + ": Не удалось получить объект по плюсу стрелки " + PrevSec.Name,
							                            "PreSec->NextP");
						if (objNextPl == _controlObj)
							_Variant |= (int) Variants.swt1P;
					}
					else
						Debug.WriteLine(_controlObj.Name + ": Не удалось получить объект по плюсу стрелки " + PrevSec.Name,"PreSec->NextPl");
				}
			}

			// Получаем и подписываемся на рассылку изменения для _PKSig
			if (!AssignToSig("PKSig", out _PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _MKSig
			if (!AssignToSig("MKSig", out _MKSig))
				return false;

			// Подписываемся на рассылку изменения для сигналов стрелочной секции

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// SwSectionState | 5:[{m_PKSig}];6:[{m_MKSig}];
			int state = 0;
			try
			{
				SwSection.UpdateObjectState();
				int swSecState = SwSection.ObjState.Current;
				if( (_Variant & (int)Variants.swt1) == 1 )
				{
					// если стрелка крайняя в секции
					if ((_Variant & (int)Variants.swt1a) != 0)
						state = swSecState;
					else
					{
						// стрелка не крайняя. Смотрим по предыдущей.
						// если она переключена в нашем направлении, то берем её состояние занятия-замыкания
						if( ((_Variant & (int)Variants.swt1P ) != 0 && _PreSecPKSig.On)  ||  ( (_Variant & (int)Variants.swt1P ) == 0 && _PreSecMKSig.On))
							state = PrevSec.ObjState.Current & 6;
					}
				}
				// 5:[{m_PKSig}];
				if ( _PKSig.On )
					state |= 0x20;
				// 6:[{m_MKSig}];
				if ( _MKSig.On )
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("SwitchStateAnalyse " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlave с сигналами SecMl & (NOT IsState({m_dwFlag}, 16384)) AND ("{m_NameObject}" != "pmain-")
	/// </summary>
	public class ObjStateSwitchSlave_SecMlPKMK : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _SecMl_PKSig;
		protected ISigDiscreteLogic _SecMl_MKSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_PKSig
			if (!AssignToSigByObj("SecMl", "PKSig", out _SecMl_PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_MKSig
			if (!AssignToSigByObj("SecMl", "MKSig", out _SecMl_MKSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_pSwSection->m_RCSig}];2:[{m_pSwSection->m_ZSig}];3:[{m_pSwSection->m_KRISig}];5:[{m_pSecMl->m_PKSig}];6:[{m_pSecMl->m_MKSig}];
			int state = 0;
			try
			{
				// 1:[{m_pSwSection->m_RCSig}];
				if (_SwSection_RCSig.On)
					state |= 0x2;
				// 2:[{m_pSwSection->m_ZSig}];
				if (_SwSection_ZSig.On)
					state |= 0x4;
				// 3:[{m_pSwSection->m_KRISig}];
				if (_SwSection_KRISig.On)
					state |= 0x8;
				// 5:[{m_pSecMl->m_PKSig}];
				if (_SecMl_PKSig.On)
					state |= 0x20;
				// 6:[{m_pSecMl->m_MKSig}];
				if (_SecMl_MKSig.On)
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateSwitchSlave_SecMlPKMK " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlave с PreSec & "{m_NameObject}" = "pmain-"
	/// </summary>
	public class ObjStateSwitchSlave_PreSecPKMK : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _PreSec_PKSig;
		protected ISigDiscreteLogic _PreSec_MKSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_PKSig
			if (!AssignToSigByObj("PreSec", "PKSig", out _PreSec_PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_MKSig
			if (!AssignToSigByObj("PreSec", "MKSig", out _PreSec_MKSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_pSwSection->m_RCSig}];2:[{m_pSwSection->m_ZSig}];3:[{m_pSwSection->m_KRISig}];5:[{m_pPreSec->m_PKSig}];6:[{m_pPreSec->m_MKSig}];
			int state = 0;
			try
			{
				// 1:[{m_pSwSection->m_RCSig}];
				if (_SwSection_RCSig.On)
					state |= 0x2;
				// 2:[{m_pSwSection->m_ZSig}];
				if (_SwSection_ZSig.On)
					state |= 0x4;
				// 3:[{m_pSwSection->m_KRISig}];
				if (_SwSection_KRISig.On)
					state |= 0x8;
				// 5:[{m_pPreSec->m_PKSig}];
				if (_PreSec_PKSig.On)
					state |= 0x20;
				// 6:[{m_pPreSec->m_MKSig}];
				if (_PreSec_MKSig.On)
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateSwitchSlave_PreSecPKMK " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlave с SecPLSec & IsState({m_dwFlag}, 16384)
	/// </summary>
	public class ObjStateSwitchSlave_SecPLPKMK : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _SecPL_PKSig;
		protected ISigDiscreteLogic _SecPL_MKSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_PKSig
			if (!AssignToSigByObj("SecPL", "PKSig", out _SecPL_PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _SecMl_MKSig
			if (!AssignToSigByObj("SecPL", "MKSig", out _SecPL_MKSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_pSwSection->m_RCSig}];2:[{m_pSwSection->m_ZSig}];3:[{m_pSwSection->m_KRISig}];5:[{m_pPreSec->m_PKSig}];6:[{m_pPreSec->m_MKSig}];
			int state = 0;
			try
			{
				// 1:[{m_pSwSection->m_RCSig}];
				if (_SwSection_RCSig.On )
					state |= 0x2;
				// 2:[{m_pSwSection->m_ZSig}];
				if ( _SwSection_ZSig.On )
					state |= 0x4;
				// 3:[{m_pSwSection->m_KRISig}];
				if (_SwSection_KRISig.On )
					state |= 0x8;
				// 5:[{m_pPreSec->m_PKSig}];
				if ( _SecPL_PKSig.On )
					state |= 0x20;
				// 6:[{m_pPreSec->m_MKSig}];
				if ( _SecPL_MKSig.On )
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateSwitchSlave_SecPLPKMK " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlave с SelfPKMK
	/// </summary>
	public class ObjStateSwitchSlave_SelfPKMK : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _PKSig;
		protected ISigDiscreteLogic _MKSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _PKSig
			if (!AssignToSig("PKSig", out _PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _MKSig
			if (!AssignToSig("MKSig", out _MKSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_pSwSection->m_RCSig}];2:[{m_pSwSection->m_ZSig}];3:[{m_pSwSection->m_KRISig}];5:[{m_PKSig}];6:[{m_MKSig}];
			int state = 0;
			try
			{
				// 1:[{m_pSwSection->m_RCSig}];
				if ( _SwSection_RCSig.On )
					state |= 0x2;
				// 2:[{m_pSwSection->m_ZSig}];
				if ( _SwSection_ZSig.On )
					state |= 0x4;
				// 3:[{m_pSwSection->m_KRISig}];
				if ( _SwSection_KRISig.On )
					state |= 0x8;
				// 5:[{m_PKSig}];
				if ( _PKSig.On )
					state |= 0x20;
				// 6:[{m_MKSig}];
				if ( _MKSig.On )
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateSwitchSlave_SelfPKMK " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние стрелочной секции SwitchSlaveP с MainSwitchPKMK
	/// </summary>
	public class ObjStateSwitchSlaveP_MainSwitchPKMK : ObjStateSwitchSlave
	{
		protected ISigDiscreteLogic _MainSwitch_PKSig;
		protected ISigDiscreteLogic _MainSwitch_MKSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _PKSig
			if (!AssignToSigByObj("MainSwitch", "PKSig", out _MainSwitch_PKSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _MKSig
			if (!AssignToSigByObj("MainSwitch", "MKSig", out _MainSwitch_MKSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_pSwSection->m_RCSig}];2:[{m_pSwSection->m_ZSig}];3:[{m_pSwSection->m_KRISig}];5:[{m_pMainSwitch->m_PKSig}];6:[{m_pMainSwitch->m_MKSig}];
			int state = 0;
			try
			{
				// 1:[{m_pSwSection->m_RCSig}];
				if ( _SwSection_RCSig.On )
					state |= 0x2;
				// 2:[{m_pSwSection->m_ZSig}];
				if ( _SwSection_ZSig.On )
					state |= 0x4;
				// 3:[{m_pSwSection->m_KRISig}];
				if ( _SwSection_KRISig.On )
					state |= 0x8;
				// 5:[{m_pMainSwitch->m_PKSig}];
				if ( _MainSwitch_PKSig.On )
					state |= 0x20;
				// 6:[{m_pMainSwitch->m_MKSig}];
				if ( _MainSwitch_MKSig.On )
					state |= 0x40;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateSwitchSlaveP_MainSwitchPKMK " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSwitchState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	
	#endregion

	#region Светофоры
	/// <summary>
	/// Состояние светофора CInSignal с сигналами OSig, SSig и без сигнала WhiteSig
	/// </summary>
	public class ObjStateInSignal_WhiteNone_OExist_SExist : DefObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле
		/// </summary>
		protected ISigDiscreteLogic _SSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
		
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[(NOT {m_SSig}) OR IsBlink({m_OSig})];2:[{m_SSig} AND NOT IsBlink({m_OSig})];7:[IsBlink({m_OSig})];
			int state = 0;
			try 
			{
				// 1:[(NOT {m_SSig}) OR IsBlink({m_OSig})];
				if ( _SSig.On || _IntegritySig.On )
					state |= 0x2;
				// 2:[{m_SSig} AND NOT IsBlink({m_OSig})];
				if ( _SSig.On || !_IntegritySig.Blink )
					state |= 0x4;
				// 7:[IsBlink({m_OSig})];
				if ( _IntegritySig.Blink )
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal без сигнала OSig
	/// </summary>
	public class ObjStateInSignal_ONone : DefObjState
	{
		protected ISigDiscreteLogic _WSig;
		protected ISigDiscreteLogic _SSig;
		protected ISigDiscreteLogic _IntegrityRSig;
        //protected ISigDiscreteLogic _IntegrityGSig;
		//protected ISigDiscreteLogic _Yellow2Sig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для целостности красного
            if (!AssignToSig("IntegrityR2YSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для целостности разрешающих
            //if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
			//	return false;
			// Получаем и подписываемся на рассылку изменения для Yellow2Sig
			/*if (!AssignToSig("Yellow2Sig", out _Yellow2Sig))
				return false;*/

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[(NOT {m_SSig}) AND (NOT {m_WhiteSig}) AND {m_CNLRedSig}];2:[{m_SSig} AND {m_CNLROSig} AND NOT {m_WhiteSig}];4:[{m_WhiteSig} AND {m_Yellow2Sig} AND NOT {m_SSig}];7:[((NOT {m_SSig}) AND NOT {m_CNLRedSig}) OR ({m_SSig} AND NOT {m_CNLROSig})];
			int state = 0;
			try 
			{
				// 1:[(NOT {m_SSig}) AND (NOT {m_WhiteSig}) AND {m_CNLRedSig}];
				if (_SSig.Off && _WSig.Off && _IntegrityRSig.On)
					state |= 0x2;
				// 2:[{m_SSig} AND {m_CNLROSig} AND NOT {m_WhiteSig}];
                if (_SSig.On && _WSig.Off)
					state |= 0x4;
				// 4:[{m_WhiteSig} AND {m_Yellow2Sig} AND NOT {m_SSig}];
				if ( _WSig.On /*&& _Yellow2Sig.On*/ && _SSig.Off )
					state |= 0x10;
				// 7:[((NOT {m_SSig}) AND NOT {m_CNLRedSig}) OR ({m_SSig} AND NOT {m_CNLROSig})];
                if (_SSig.Off && _IntegrityRSig.Off)
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateInSignal_ONone " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal без сигнала OSig
	/// </summary>
	/*public class ObjStateInSignal : DefObjState
	{
		/// <summary>
		/// Контроль пригласительного сигнала
		/// </summary>
		protected ISigDiscreteLogic _WhiteSig;
		/// <summary>
		/// Контроль сигнального реле (открыт\закрыт)
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль огневого реле (отказ)
		/// </summary>
		protected ISigDiscreteLogic _OSig;
		/// <summary>
		/// Контроль целостности нити красного
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRedSig;
		/// <summary>
		/// Контроль целостности нити разрешающих огней
		/// </summary>
		protected ISigDiscreteLogic _IntegrityROSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;
		
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("OSig", out _OSig))
				_OSig = null;
			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WhiteSig", out _WhiteSig))
				_WhiteSig = null;
			// Получаем и подписываемся на рассылку изменения для целостности красного
			if (!AssignToSig("IntegrityRedSig", out _IntegrityRedSig))
				_IntegrityRedSig = null;
			// Получаем и подписываемся на рассылку изменения для целостности разрешающих
			if (!AssignToSig("IntegrityROSig", out _IntegrityROSig))
				_IntegrityROSig = null;
			// Получаем и подписываемся на рассылку изменения для целостности белого
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				_IntegrityWSig = null;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:Закрыт 
				/*if (_SSig.Off && _WhiteSig.Off && _IntegrityRedSig.On)
					state |= 0x2;
				// 2:[{m_SSig} AND {m_CNLROSig} AND NOT {m_WhiteSig}];
				if (_SSig.On && _InterityROSig.On && _WhiteSig.Off)
					state |= 0x4;
				// 4:[{m_WhiteSig} AND {m_Yellow2Sig} AND NOT {m_SSig}];
				if (_WhiteSig.On /*&& _Yellow2Sig.On* && _SSig.Off)
					state |= 0x10;
				// 7:[((NOT {m_SSig}) AND NOT {m_CNLRedSig}) OR ({m_SSig} AND NOT {m_CNLROSig})];
				if ((_SSig.Off && _IntegrityRedSig.Off) || (_SSig.On && _InterityROSig.Off))
					state |= 0x80;*
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}*/

	/// <summary>
	/// Состояние светофора MSignal без сигнала SSig
	/// </summary>
	public class ObjStateMSignal_SNone : DefObjState
	{
		/// <summary>
		/// Контроль огневого реле
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[{m_OSig}];7:[IsBlink({m_OSig})];
			int state = 0;
			try 
			{
				// 1:[{m_OSig}];
				if (_IntegritySig.On)
					state |= 0x2;
				// 7:[IsBlink({m_OSig})];
				if (_IntegritySig.Blink)
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateMSignal_SNone " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора MSignal с сигналом SSig
	/// </summary>
	public class ObjStateMSignal_SExist : DefObjState
	{
		/// <summary>
		/// Открытие на маневровое показание
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("MSig", out _MSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT IsBlink({m_SSig})];3:[{m_SSig} AND NOT IsBlink({m_SSig})];7:[IsBlink({m_SSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[NOT {m_SSig} AND NOT IsBlink({m_SSig})];3:[{m_SSig} AND NOT IsBlink({m_SSig})];7:[IsBlink({m_SSig})];
			int state = 0;
			try 
			{
				// 1:[NOT {m_SSig} AND NOT IsBlink({m_SSig})];
				if (_MSig.Off && !_MSig.Blink )
					state |= 0x2;
				// 3:[{m_SSig} AND NOT IsBlink({m_SSig})];
				if ( _MSig.On && !_MSig.Blink)
					state |= 0x8;
				// 7:[IsBlink({m_SSig})];
				if ( _MSig.Blink )
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateMSignal_SExist " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal с сигналами OSig, SSig и без сигнала WhiteSig
	/// </summary>
	public class ObjStateMSignal_WhiteNone_OExist_SExist : DefObjState
	{
		/// <summary>
		/// Контроль огневого реле
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("MSig", out _MSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:Закрыт (сигнальное 0)
                if (_MSig.Off && _IntegritySig.Off)
					state |= 0x2;
				// 2:Открыт (сигнальное 1)
				if (_MSig.On && _IntegritySig.Off)
					state |= 0x8;
				// 7:Отказ (огневое 1)
				if (_IntegritySig.On)
					state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateMSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора OutSignalPS с сигналом WhiteSig
	/// </summary>
	public class ObjStateOutSignalPS_WhiteExist : DefObjState
	{
		protected ISigDiscreteLogic _SSig;
		protected ISigDiscreteLogic _MSig;
		protected ISigDiscreteLogic _WhiteSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WSig", out _WhiteSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[(NOT {m_SSig}) AND (NOT {m_KMSSig}) AND (NOT {m_WhiteSig})];2:[{m_SSig} AND (NOT {m_KMSSig}) AND NOT {m_WhiteSig}];3:[{m_KMSSig} AND (NOT {m_SSig}) AND NOT {m_WhiteSig}];4:[{m_WhiteSig} AND NOT IsBlink({m_KMSSig})];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[(NOT {m_SSig}) AND (NOT {m_KMSSig}) AND (NOT {m_WhiteSig})];2:[{m_SSig} AND (NOT {m_KMSSig}) AND NOT {m_WhiteSig}];3:[{m_KMSSig} AND (NOT {m_SSig}) AND NOT {m_WhiteSig}];4:[{m_WhiteSig} AND NOT IsBlink({m_KMSSig})];7:[IsBlink({m_KMSSig})];
			int state = 0;
			try 
			{
				// 1:[(NOT {m_SSig}) AND (NOT {m_KMSSig}) AND (NOT {m_WhiteSig})];
				if (_SSig.Off && _MSig.Off && _WhiteSig.Off)
					state |= 0x2;
				// 2:[{m_SSig} AND (NOT {m_KMSSig}) AND NOT {m_WhiteSig}];
				if (_SSig.On && _MSig.Off && _WhiteSig.Off)
					state |= 0x4;
				// 3:[{m_KMSSig} AND (NOT {m_SSig}) AND NOT {m_WhiteSig}];
				if (_MSig.On && _SSig.Off && _WhiteSig.Off)
					state |= 0x8;
				// 4:[{m_WhiteSig} AND NOT IsBlink({m_KMSSig})];
				if (_WhiteSig.On && !_MSig.Blink)
					state |= 0x10;
				// 7:[IsBlink({m_KMSSig})];
				if (_MSig.Blink)
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateOutSignalPS_WhiteExist " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние светофора OutSignalPS без сигнала WhiteSig
	/// </summary>
	public class ObjStateOutSignalPS_WhiteNone : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора (сигнальное реле)
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на маневровое показание
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("MSig", out _MSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
			int state = 0;
			try 
			{			
				// 1:[NOT {m_SSig} AND NOT {m_KMSSig}];
				if (_SSig.Off && _MSig.Off )
					state |= 0x2;
				// 2:[{m_SSig} AND NOT {m_KMSSig}];
				if (_SSig.On && _MSig.Off )
					state |= 0x4;
				// 3:[{m_KMSSig} AND NOT {m_SSig}];
				if (_MSig.On && _SSig.Off )
					state |= 0x8;
				// 7:[IsBlink({m_KMSSig})];
				if (_MSig.Blink)
					state |= 0x80;
			}
			catch( ArgumentOutOfRangeException ex )
			{
				Debug.WriteLine("ObjStateOutSignalPS_WhiteNone " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Входной основной: откр зеленый, желтый мигающий, пригласительный, + целостность жз, кр, б
	/// </summary>
	public class ObjStateInSignal_S_G_YB_Wh_ClnR_GY_W : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль открытия светофора на желтый мигающий
		/// </summary>
		protected ISigDiscreteLogic _YBlSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для YBlSig
			if (!AssignToSig("YBlSig", out _YBlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		///		закрыт		с=0 и з=0 и жм=0 и пр=0 && ц.кр=1
		///		открыт		(с=1 или з=1 или жм=1) и пр=0 && ц.жз=1
		///		маневр		-
		///		пригласит   (с=0 и з=0 и жм=0) и пр=1 && ц.б=1
        ///		ОТКАЗ(ОГНЕВОЕ) ц.кр=0
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:с=0 и з=0 и жм=0 и пр=0 && ц.кр=1
				if (_SSig.Off && _GSig.Off && _YBlSig.Off && _WSig.Off && _IntegrityRSig.On)
					state |= 0x2;
				// 2:(с=1 или з=1 или жм=1) и пр=0 && ц.жз=1
				if ( (_SSig.On || _GSig.On || _YBlSig.On ) && _WSig.Off)
					state |= 0x4;
				// 4:(с=0 и з=0 и жм=0) и пр=1 && ц.б=1
				if (_SSig.Off && _GSig.Off && _YBlSig.Off && _WSig.On)
					state |= 0x10;
                // 7:ОТКАЗ(ОГНЕВОЕ) ц.кр=0
                if (_IntegrityRSig.Off)
                    state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal_S_G_YB_Wh_ClnR_GY_W " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Входной основной: откр желтый миг, пригласительный + целостность кр, б, ж1, ж2
	/// </summary>
	public class ObjStateInSignal_S_YB_Wh_ClnR_Y1_Y2_W : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия светофора на желтый мигающий
		/// </summary>
		protected ISigDiscreteLogic _YBlSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы первого желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY1Sig;
		/// <summary>
		/// Контроль целостности нити лампы второго желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY2Sig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для YBlSig
			if (!AssignToSig("YBlSig", out _YBlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityY1Sig
			if (!AssignToSig("IntegrityY1Sig", out _IntegrityY1Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityY2Sig
			if (!AssignToSig("IntegrityY2Sig", out _IntegrityY2Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		///		закрыт		с=0 и жм=0 и пр=0 && ц.кр=1
		///		открыт		(с=1 или жм=1) и пр=0 && ц.ж1=1 && ц.ж2=1
		///		маневр		-
		///		пригласит   (с=0 и жм=0) и пр=1 && ц.б=1
		///		отказ		(с=1 и ц.жз=0) или (жм=1 и (ц.ж1=0 || ц.ж1=0)) или (пр=1 и ц.б=0)
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:с=0 и з=0 и жм=0 и пр=0 && ц.кр=1
				if (_SSig.Off && _YBlSig.Off && _WSig.Off && _IntegrityRSig.On)
					state |= 0x2;
				// 2:открыт		(с=1 или жм=1) и пр=0 && ц.ж1=1 && ц.ж2=1
				if ((_SSig.On || _YBlSig.On) && _WSig.Off)
					state |= 0x4;
				// 4:(с=0 и з=0 и жм=0) и пр=1 && ц.б=1
				if (_SSig.Off && _YBlSig.Off && _WSig.On)
					state |= 0x10;
				// 7:ОТКАЗ(ОГНЕВОЕ) ц.кр=0
                if (_IntegrityRSig.Off)
					state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal_S_YB_Wh_ClnR_Y1_Y2_W " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		///		закрыт		с=0 и з=0 и пр=0 && ц.кр=1
		///		открыт		с=1
		///		маневр		-
		///		пригласит   (с=0 и пр=1 
		///		отказ		(с=1 или з или 2ж или миг и ц.разр=0) или (пр=1 и ц.б=0)
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:закрыт     с=0 и з=0 и пр=0 && ц.кр=1
				if (_SSig.Off && _GSig.Off && _WSig.Off && _IntegrityRSig.On)
					state |= 0x2;
				// 2:открыт		(с=1)
				else if (_SSig.On)
					state |= 0x4;
				// 4:пригласительный (с=0 и пр=1 && ц.б=1
				else if (_SSig.Off && _GSig.Off && _WSig.On)
					state |= 0x10;
                // 7:ОТКАЗ(ОГНЕВОЕ) ц.кр=0
                if (_IntegrityRSig.Off)
                    state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Входной дополнительный: откр,  пригласительный,горение 2го желтого и белого + целостность кр, б, ж1
	/// </summary>
	public class ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы первого желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY1Sig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityY1Sig
			if (!AssignToSig("IntegrityY1Sig", out _IntegrityY1Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		///		закрыт		с=0 и з=0 и пр=0 && ц.кр=1
		///		открыт		с=1 и ц.разр)
		///		маневр		-
		///		пригласит   (с=0 и пр=1 && ц.б=1
		///		отказ		(миг и ц.1ж=0) или (пр=1 и ц.б=0)
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:закрыт     с=0 и з=0 и пр=0 && ц.кр=1
				if (_SSig.Off && _WSig.Off && _IntegrityRSig.On)
					state |= 0x2;
				// 2:открыт		(с=1 
				else if (_SSig.On)
					state |= 0x4;
				// 4:пригласительный (с=0 и пр=1
				else if (_SSig.Off && _WSig.On)
					state |= 0x10;
                // 7:ОТКАЗ(ОГНЕВОЕ) ц.кр=0
                if (_IntegrityRSig.Off)
                    state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Входной дополнительный: откр + целостность 2ж, з
	/// </summary>
	public class ObjStateInSignal_S_Cln_2Y_G : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль целостности нити лампы второго желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY2Sig;
		/// <summary>
		/// Контроль целостности нити лампы зеленого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityGSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityY2Sig", out _IntegrityY2Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
				return false;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// 1:закрыт     с=0
				if (_SSig.Off)
					state |= 0x2;
				// 2:открыт		(с=1 )
				else if (_SSig.On )
					state |= 0x4;
				// 4:пригласительный нет
				
				// 7:отказ не контролируется
				//if ((_SSig.On && _IntegrityGSig.Off) || (_WSig.On && _IntegrityWSig.Off))
				//	state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateInSignal_S_Cln_2Y_G " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceSignalState(state);
			SetState(state);

			base.UpdateObjectState();
		}
	}
	#endregion

	#region Индикаторы
	
	#region Индикаторы с одним сигнлом
	/// <summary>
	/// Состояние ГОРИТ индикатора с одним сигналом. 
	/// </summary>
	public class IndObjState1Sig_Alight : DefObjState
	{
		protected ISigDiscreteLogic _Sig1;
		
		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("Sig1", out _Sig1))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Если в Sig1 импульс, то устанавливаем значение 'горит первым цветом'
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// если в сигнале импульс - состояние "горит"
				if (_Sig1.On)
					state |= 0x02;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjState1Sig_Alight " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

    /// <summary>
    /// Состояние индикатора с одним сигналом.(негорит\мигает1) 
    /// </summary>
    public class IndObjState1Sig_B1 : IndObjState1Sig_Alight
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Если в Sig1 импульс, то устанавливаем значение 'мигает первым цветом'
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = 0;
            try
            {
                // если в сигнале импульс - состояние "мигает"
                if (_Sig1.On)
                    state |= 0x04;
            }
            catch (ArgumentOutOfRangeException ex)
            {
				Debug.WriteLine("IndObjState1Sig_B1 " + _controlObj.Name + ":" + ex.Message);
                state = 0x1;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние ГОРИТ или МИГАЕТ индикатора с одним сигналом. 
	/// </summary>
	public class IndObjState1Sig_AlightBlink : IndObjState1Sig_Alight
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Если в Sig1 импульс, то устанавливаем состояние 'горит первым цветом'
		/// Если Sig1 мигает - состояние "мигает"
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// если сигнал в импульсе - состояние "горит"
				if (_Sig1.On)
					state |= 0x02;
				// если сигнал мигает - состояние "мигает"
				if (_Sig1.Blink)
					state |= 0x04;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjState1Sig_AlightBlink " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние ГОРИТ или ГОРИТ2 индикатора с одним сигналом. 
	/// </summary>
	public class IndObjState1Sig_AlightAlight2 : IndObjState1Sig_Alight
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Если в Sig1 импульс, то устанавливаем значение "горит первым цветом"
		/// Если Sig1 мигает - состояние "горит вторым цветом"
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				// если сигнал в импульсе - состояние "горит"
				if (_Sig1.On)
					state |= 0x02;
				// если сигнал мигает - состояние "горит2"
				if (_Sig1.Blink)
					state |= 0x08;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjState1Sig_AlightAlight2 " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}
	#endregion

	#region Индикаторы с двумя сигналами
    /// <summary>
    /// Состояние индикатора с двумя сигналоми (ИЛИ). 
    /// </summary>
    public class IndObjState2Sig_L1_OR : IndObjState1Sig_Alight
    {
        protected ISigDiscreteLogic _Sig2;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            // действия для Sig1
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для Sig2
            if (!AssignToSig("Sig2", out _Sig2))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// 0 0	- не горит; 1 0 - горит1; 1 1 - горит1; 0 1 - горит1
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = 0;
            try
            {
                if (_Sig1.On || _Sig2.On)
                    state |= 0x02;
            }
            catch (ArgumentOutOfRangeException ex)
            {
				Debug.WriteLine("IndObjState2Sig_L1_OR " + _controlObj.Name + ":" + ex.Message);
                state = 0x1;
            }
            TraceIndicatorState(state);

            SetState(state);

            //base.UpdateObjectState();
            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми (10 - горит1, 01 - мигает1). 
    /// </summary>
    public class IndObjState2Sig_10L1_01B1 : IndObjState2Sig_L1_OR
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// 0 0	- не горит; 1 0 - горит1; 1 1 - мигает1; 0 1 - мигает1
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = 0;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state |= 0x02;
                else if (_Sig2.On)
                    state |= 0x04;
            }
            catch (ArgumentOutOfRangeException ex)
            {
				Debug.WriteLine("IndObjState2Sig_10L1_01B1 " + _controlObj.Name + ":" + ex.Message);
                state = 0x1;
            }
            TraceIndicatorState(state);

            SetState(state);

            //base.UpdateObjectState();
            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми (10 - горит2, 0M - мигает1, 01 - горит1). 
    /// </summary>
    public class IndObjState2Sig_01L1_0BB1_10L2 : IndObjState2Sig_L1_OR
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// (10 - горит2, 0M - мигает1, 01 - горит1). 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = 0;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state |= 0x08;
                else if (_Sig2.On)
                    state |= 0x02;
                else if (_Sig2.Blink)
                    state |= 0x04;
            }
            catch (ArgumentOutOfRangeException ex)
            {
				Debug.WriteLine("IndObjState2Sig_01L1_0BB1_10L2 " + _controlObj.Name + ":" + ex.Message);
                state = 0x1;
            }
            TraceIndicatorState(state);

            SetState(state);

            //base.UpdateObjectState();
            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с двумя сигналоми (фидер). 
	/// </summary>
	public class IndObjState2Sig_fid : IndObjState1Sig_Alight
	{
		protected ISigDiscreteLogic _Sig2;

		/// <summary>
		/// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			// действия для Sig1
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig2
			if (!AssignToSig("Sig2", out _Sig2))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// 0 0	- не горит; 1 0 - горит1; 1 1 - горит2; 0 1 - горит2
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state |= 0x02;
				else if (_Sig2.On)
					state |= 0x08;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjState2Sig_fid " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с двумя сигналоми (фидер). 
	/// </summary>
	public class IndObjState2Sig_fid2 : IndObjState2Sig_fid
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// 0 0	- горит1; 1 0 - горит2; 1 1 - горит3; 0 1 - горит3
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				if (_Sig1.Off && _Sig2.Off)
					state |= 0x02;
				else if (_Sig1.On && _Sig2.Off)
					state |= 0x08;
				else if (_Sig2.On)
					state |= 0x20;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjState2Sig_fid2 " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора перегона. Без отправления
	/// </summary>
	public class IndObjStateWayApproach2 : DefObjState
	{
		/// <summary>
		/// Установление направления прием
		/// </summary>
		protected ISigDiscreteLogic _InDirSig;
		/// <summary>
		/// Свободность перегона
		/// </summary>
		protected ISigDiscreteLogic _FreeSig;
		/// <summary>
		/// Заятость перегона
		/// </summary>
		protected ISigDiscreteLogic _ApproachSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OutDirSig
			if (!AssignToSig("FreeSig", out _FreeSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для InDirSig
			if (!AssignToSig("ApproachSig", out _ApproachSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для OutDirSig
			if (!AssignToSigByObj("InDirObj", "Sig1", out _InDirSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				if (_InDirSig.On && _FreeSig.On)
					state |= 0x02;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjStateWayApproach2 " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}

	/// <summary>
	/// Состояние индикатора перегона. 
	/// </summary>
	public class IndObjStateWayApproach1 : IndObjStateWayApproach2
	{
		/// <summary>
		/// Установление направления отправление
		/// </summary>
		protected ISigDiscreteLogic _OutDirSig;
	
		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OutDirSig
			if (!AssignToSigByObj("OutDirObj","Sig1", out _OutDirSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				if ( (_InDirSig.On && _FreeSig.Off) || (_OutDirSig.On && _ApproachSig.Off) )
					state |= 0x02;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("IndObjStateWayApproach1 " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			//base.UpdateObjectState();
            ResetModify();
		}
	}
	#endregion

	#endregion

	#region Переезд
	/// <summary>
	/// Состояние индикатора переезда. 
	/// </summary>
	public class CrossingObjState : DefObjState
	{
		/// <summary>
		/// Контроль закрытия переезда
		/// </summary>
		protected ISigDiscreteLogic _KzSig;
		/// <summary>
		/// Контроль включения заграждения переезда
		/// </summary>
		protected ISigDiscreteLogic _KzagrSig;
		/// <summary>
		/// Контроль извещения на переезд
		/// </summary>
		protected ISigDiscreteLogic _KPlSig;
		/// <summary>
		/// Контроль нормальной работы переезда
		/// </summary>
		protected ISigDiscreteLogic _KNormSig;
		/// <summary>
		/// Контроль неисправности переезда
		/// </summary>
		protected ISigDiscreteLogic _KNSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для KzSig
			if (!AssignToSig("KzSig", out _KzSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для _KzagrSig
			if (!AssignToSig("KzagrSig", out _KzagrSig))
				_KzagrSig = null;
			// Получаем и подписываемся на рассылку изменения для _KPlSig
			if (!AssignToSig("KPlSig", out _KPlSig))
				_KPlSig = null;
			// Получаем и подписываемся на рассылку изменения для _KNormSig
			if (!AssignToSig("KNormSig", out _KNormSig))
				_KNormSig = null;
			// Получаем и подписываемся на рассылку изменения для _KNSig
			if (!AssignToSig("KNSig", out _KNSig))
				_KNSig = null;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = 0;
			try
			{
				if (_KzSig.On)
					state |= 0x02;
				if (_KPlSig != null && _KPlSig.On)
					state |= 0x04;
				if (_KzagrSig != null && _KzagrSig.On)
					state |= 0x20;
				if ((_KNSig != null && _KNSig.On) || (_KNormSig != null && _KNormSig.Off))
					state |= 0x80;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
				state = 0x1;
			}
			TraceIndicatorState(state);

			SetState(state);

			base.UpdateObjectState();
		}
	}
	#endregion
}

namespace Tdm.Unification.v2
{
    /// <summary>
    /// Состояние объекта самодиагностики
    /// </summary>
    public class SelfDiagObjStateCPUBS : AbsenceObjState
    {
        /// <summary>
        /// Контроль кодового сигнала исправности устройства
        /// </summary>
        protected List<ISigDiscreteLogic> _OkSigs = new List<ISigDiscreteLogic>();

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            bool bRes = false;
            // Получаем и подписываемся на рассылку изменения для _KPlSig
            for (int i = 0; i <= 8; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                if (AssignToSig("OkSig" + (i > 0 ? i.ToString() : string.Empty), out tmpSig))
                {
                    _OkSigs.Add(tmpSig);
                    bRes = true;
                }
            }
            return bRes;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                state = (int)Uni2States.GeneralStates.ObjectConserve;
                foreach (ISigDiscreteLogic l in _OkSigs)
                {
                    if (l.On)
                    {
                        state = (int)Uni2States.SelfDiagStates.HasControl;
                        break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Возможные состояния объектов контроля для унификации 2
	/// </summary>
	public class Uni2States
	{
		/// <summary>
		/// Общие состояния объектов
		/// </summary>
		public enum GeneralStates
		{
			/// <summary>
			/// Неопределенное состояние (отсутствие ТС)
			/// </summary>
			IndefiniteState_AbsenceCS = 0,
			/// <summary>
			/// Неопределенное состояние (несоответствие ТС)
			/// </summary>
			IndefiniteState_DiscrepancyCS,
			/// <summary>
			/// Объект законсервирован
			/// </summary>
			ObjectConserve
		} ;

		/// <summary>
		/// Рельсовая цепь (РЦ)
		/// </summary>
		public enum PathSectionStates
		{
			/// <summary>
			/// РЦ свободна, не замкнута
			/// </summary>
			Free = 3,
			/// <summary>
			/// РЦ свободна, замкнута
			/// </summary>
			Free_Lock,
			/// <summary>
			/// РЦ свободна, замкнута, искусственно размыкается
			/// </summary>
			Free_Lock_EmergencyRelease,
			/// <summary>
			/// РЦ занята, не замкнута
			/// </summary>
			Busy,
			/// <summary>
			/// РЦ занята, замкнута
			/// </summary>
			Busy_Lock,
			/// <summary>
			/// РЦ занята, замкнута, искусственно размыкается
			/// </summary>
			Busy_Lock_EmergencyRelease
		} ;

		public enum SwitchStates
		{
            /// <summary>
            /// Стрелка в плюсовом положении, свободна, не замкнута
            /// </summary>
            PL_Free = 3,
            /// <summary>
            /// Стрелка в плюсовом положении, свободна, замкнута
            /// </summary>
            PL_Free_Lock,
            /// <summary>
            /// Стрелка в плюсовом положении, свободна, замк-нута, искусственно размыкается
            /// </summary>
            PL_Free_Lock_EmergencyRelease,
            /// <summary>
            /// Стрелка в плюсовом положении, занята, не замк-нута
            /// </summary>
            PL_Busy,
            /// <summary>
            /// Стрелка в плюсовом положении, занята, замкнута
            /// </summary>
            PL_Busy_Lock,
            /// <summary>
            /// Стрелка в плюсовом положении, занята, замкнута, искусственно размыкается
            /// </summary>
            PL_Busy_Lock_EmergencyRelease,
            /// <summary>
            /// Стрелка в минусовом положении, свободна, не замкнута
            /// </summary>
            MI_Free,
            /// <summary>
            /// Стрелка в минусовом положении, свободна, замк-нута
            /// </summary>
            MI_Free_Lock,
            /// <summary>
            /// Стрелка в минусовом положении, свободна, замк-нута, искусственно размыкается
            /// </summary>
            MI_Free_Lock_EmergencyRelease,
            /// <summary>
            /// Стрелка в минусовом положении, занята, не замк-нута
            /// </summary>
            MI_Busy,
            /// <summary>
            /// Стрелка в минусовом положении, занята, замкну-та
            /// </summary>
            MI_Busy_Lock,
            /// <summary>
            /// Стрелка в минусовом положении, занята, замкну-та, искусственно размыкается
            /// </summary>
            MI_Busy_Lock_EmergencyRelease,
            /// <summary>
            /// Стрелка потеряла контроль, свободна, не замкнута
            /// </summary>
            LC_Free,
            /// <summary>
            /// Стрелка потеряла контроль, свободна, замкнута
            /// </summary>
            LC_Free_Lock,
            /// <summary>
            /// Стрелка потеряла контроль, занята, не замкнута
            /// </summary>
            LC_Busy,
            /// <summary>
            /// Стрелка потеряла контроль, занята, замкнута
            /// </summary>
            LC_Busy_Lock,
            /// <summary>
            /// Стрелка потеряла контроль, свободна, замкнута, искусственно размыкается
            /// </summary>
            LC_Free_Lock_EmergencyRelease,
            /// <summary>
            /// Стрелка потеряла контроль, занята, замкнута, ис-кусственно размыкается
            /// </summary>
            LC_Busy_Lock_EmergencyRelease,
		} ;

		public enum MSignalStates
		{
			/// <summary>
			/// Запрет маневров (синий огонь)
			/// </summary>
			Close = 3,
			/// <summary>
			/// Маневровое показание (белый огонь)
			/// </summary>
			Open,
			/// <summary>
			/// Ускоренные маневры
			/// </summary>
			QuickOpen,
			/// <summary>
			/// Отказ на светофоре
			/// </summary>
            Fail,
            /// <summary>
            /// Запрет маневров (красный огонь)
            /// </summary>
            CloseRed
		} ;

        /// <summary>
        /// Состояния проходного светофора
        /// </summary>
        public enum StageSignalStates
        {
            /// <summary>
            /// Один зеленый огонь (открыт)
            /// </summary>
            Open_Green = 3,
            /// <summary>
            /// Один желтый огонь
            /// </summary>
            Yellow,
            /// <summary>
            /// Один красный огонь (закрыт)
            /// </summary>
            Close_Red,
            /// <summary>
            /// Один зеленый мигающий огонь
            /// </summary>
            GreenBlink,
            /// <summary>
            /// Один желтый мигающий огонь
            /// </summary>
            YellowBlink,
            /// <summary>
            /// Один зеленый и один желтый огни
            /// </summary>
            Green_Yellow,
            /// <summary>
            /// Отказ на светофоре
            /// </summary>
            Fail,
            /// <summary>
            /// Погашен
            /// </summary>
            Canceled
        } ;

		/// <summary>
		/// Устройства электропитания
		/// </summary>
		public enum PowerSupplyStates
		{
			/// <summary>
			/// Нет питания
			/// </summary>
			NoPower = 3,
			/// <summary>
			/// Есть питание
			/// </summary>
			InPower,
            /// <summary>
            /// Есть питание и устройство активно
            /// </summary>
            InPowerAndActivate,
            /// <summary>
            /// Запуск
            /// </summary>
            Start,
            /// <summary>
			/// Заряд
			/// </summary>
			Charge,
			/// <summary>
			/// Неисправность
			/// </summary>
			Fault
		} ;

		/// <summary>
		/// Тормозной упор (УТС - упор тормозной стационарный)
		/// </summary>
		public enum BrakeStates
		{
			/// <summary>
			/// УТС снят и нет колесной пары в зоне УТС
			/// </summary>
			UTS_TakeAway_WheelPairNone = 3,
			/// <summary>
			/// УТС установлен и нет колесной пары в зоне УТС
			/// </summary>
			UTS_Mounted_WheelPairNone,
			/// <summary>
			/// Нет контроля положения УТС и нет колесной па-ры в зоне УТС
			/// </summary>
			UTS_Unknown_WheelPairNone,
			/// <summary>
			/// УТС снят и колесная пара в зоне УТС
			/// </summary>
			UTS_TakeAway_WheelPairInZone,
			/// <summary>
			/// УТС установлен и колесная пара в зоне УТС
			/// </summary>
			UTS_Mounted_WheelPairInZone,
			/// <summary>
			/// Нет контроля положения УТС и колесная пара в зоне УТС
			/// </summary>
			UTS_Unknown_WheelPairInZone
		} ;

        /// <summary>
        /// Состояния переключателя контактной сети
        /// </summary>
	    public enum CatenaryStates
	    {
            /// <summary>
            /// Автономная тяга
            /// </summary>
            AutonomousTraction=3,
            /// <summary>
            /// Включение постоянного напряжения
            /// </summary>
            DCVoltageSwitchingOn,
            /// <summary>
            /// Включение переменного напряжения
            /// </summary>
            ACVoltageSwitchingOn,
            /// <summary>
            /// Наличие постоянного напряжения
            /// </summary>
            DCVoltageOn,
            /// <summary>
            /// Наличие переменного напряжения
            /// </summary>
            ACVoltageOn,
            /// <summary>
            /// Наличие постоянного напряжения, включение переменного напряжения
            /// </summary>
            DCVoltageOn_ACVoltageSwitchingOn,
            /// <summary>
            /// Наличие переменного напряжения, включение постоянного напряжения
            /// </summary>
            ACVoltageOn_DCVoltageSwitchingOn,
            /// <summary>
            /// Наличие постоянного напряжения, сеть замкнута
            /// </summary>
            DCVoltageOn_NetCircuit,
            /// <summary>
            /// Наличие переменного напряжения, сеть замкнута
            /// </summary>
            ACVoltageOn_NetCirсuit,
            /// <summary>
            /// Наличие постоянного напряжения, сеть замкнута, наличие электровоза
            /// </summary>
            DCVoltageOn_NetCircuit_Train,
            /// <summary>
            /// Наличие переменного напряжения, сеть замкнута, наличие электровоза
            /// </summary>
            ACVoltageOn_NetCircuit_Train,
            /// <summary>
            /// Наличие постоянного напряжения, сеть замкнута, наличие электровоза, искусственное размыкание
            /// </summary>
            DCVoltageOn_NetCircuit_Train_ManualBreak,
            /// <summary>
            /// Наличие переменного напряжения, сеть замкнута, наличие электровоза, искусственное размыкание
            /// </summary>
            ACVoltageOn_NetCircuit_Train_ManualBreak,
            /// <summary>
            /// Наличие постоянного напряжения, сеть замкнута, наличие автономного локомотива
            /// </summary>
            DCVoltageOn_NetCircuit_DieselTrain,
            /// <summary>
            /// Наличие переменного напряжения, сеть замкнута, наличие автономного локомотива
            /// </summary>
            ACVoltageOn_NetCircuit_DieselTrain,
            /// <summary>
            /// Наличие постоянного напряжения, сеть замкнута, наличие автономного локомотива, искусственное размыкание
            /// </summary>
            DCVoltageOn_NetCircuit_DieselTrain_ManualBreak,
            /// <summary>
            /// Наличие переменного напряжения, сеть замкнута, наличие автономного локомотива, искусственное размыкание
            /// </summary>
            ACVoltageOn_NetCircuit_DieselTrain_ManualBreak,
	    } ;

		public enum SignalStates
		{
		} ;

		/// <summary>
		/// Светофор станционный поездной (входной, выходной, маршрутный)
		/// </summary>
		public enum StateSignalStates
		{
			/// <summary>
			/// Один зеленый огонь (открыт)
			/// </summary>
			Green = 3,
			/// <summary>
			/// Один зеленый мигающий огонь
			/// </summary>
			GreenBlink,
			/// <summary>
			/// Один желтый огонь
			/// </summary>
			Yellow,
			/// <summary>
			/// Один желтый мигающий огонь
			/// </summary>
			YellowBlink,
			/// <summary>
			/// Два желтых огня
			/// </summary>
			_2Yellow,
			/// <summary>
			/// Два желтых огня, верхний мигающий
			/// </summary>
			_2Yellow_UpperBlink,
			/// <summary>
			/// Три желтых огня
			/// </summary>
			_3Yellow,
			/// <summary>
			/// Один зеленый мигающий и один желтый огни
			/// </summary>
			GreenBlink_Yellow,
			/// <summary>
			/// Один зелёный огонь, один белый огонь
			/// </summary>
			Green_White,
			/// <summary>
			/// Один жёлтый огонь, один белый огонь
			/// </summary>
			Yellow_White,
			/// <summary>
			/// Два зелёных огня
			/// </summary>
			_2Green,
			/// <summary>
			/// Один жёлтый мигающий и один белый огни
			/// </summary>
			YellowBlink_White,
			/// <summary>
			/// Один красный огонь (закрыт)
			/// </summary>
			Red,
			/// <summary>
			/// Один белый (маневровое показание)
			/// </summary>
			White,
			/// <summary>
			/// Два белых (ускоренные маневры)
			/// </summary>
			_2White,
			/// <summary>
			/// Один белый мигающий и красный огни (пригласительный)
			/// </summary>
			WhiteBlink_Red,
			/// <summary>
			/// Отказ на светофоре
			/// </summary>
			Fail,
            /// <summary>
            /// Зеленый и желтый огни
            /// </summary>
            GreenYellow,
            /// <summary>
            /// Не горит
            /// </summary>
            NoLight
        } ;

        /// <summary>
        /// Состояния заградительного светофора
        /// </summary>
        public enum BarrageSignalStates
        {
            /// <summary>
            /// Нормально погашен
            /// </summary>
            Canceled = 3,
            /// <summary>
            /// Включение заграждения (красный огонь)
            /// </summary>
            Red,
            /// <summary>
            /// Отказ на светофоре
            /// </summary>
            Fail
        } ;

		/// <summary>
		/// Переезд
		/// </summary>
		public enum CrossingStates
		{
			/// <summary>
			/// Переезд открыт
			/// </summary>
			Open = 3,
			/// <summary>
			/// Переезд открыт и включено извещение
			/// </summary>
			Open_Notification,
			/// <summary>
			/// Переезд закрыт
			/// </summary>
			Close,
			/// <summary>
			/// Переезд закрыт и шлагбаум закрыт
			/// </summary>
			Close_Barrier,
			/// <summary>
			/// Переезд закрыт, шлагбаум закрыт и УЗП поднят
			/// </summary>
			Close_Barrier_UZP
		} ;

		/// <summary>
		/// Состояния индикаторов состояния устройства
		/// </summary>
		public enum IndObjStates
		{
			/// <summary>
			/// Индикатор не горит
			/// </summary>
			NotAlight = 3,
			/// <summary>
			/// Индикатор горит красным цветом
			/// </summary>
			RedAlight,
			/// <summary>
			/// Индикатор мигает красным цветом
			/// </summary>
			RedBlink,
			/// <summary>
			/// Индикатор горит белым цветом
			/// </summary>
			WhiteAlight,
			/// <summary>
			/// Индикатор мигает белым цветом
			/// </summary>
			WhiteBlink,
			/// <summary>
			/// Индикатор горит зеленым цветом
			/// </summary>
			GreenAlight,
			/// <summary>
			/// Индикатор мигает зеленым цветом
			/// </summary>
			GreenBlink,
			/// <summary>
			/// Индикатор горит желтым цветом
			/// </summary>
			YellowAlight,
			/// <summary>
			/// Индикатор мигает желтым цветом
			/// </summary>
			YellowBlink,
			/// <summary>
			/// Индикатор горит синим цветом
			/// </summary>
			BlueAlight,
			/// <summary>
			/// Индикатор мигает синим цветом
			/// </summary>
			BlueBlink,
			/// <summary>
			/// Индикатор горит красным и желтым цветами
			/// </summary>
			RedYellowAlight,
			/// <summary>
			/// Индикатор мигает красным и желтым цветами
			/// </summary>
			RedYellowBlink
		};

        /// <summary>
        /// Состояния объектов кодирования
        /// </summary>
        public enum CodeStates
        {
            /// <summary>
            /// Нет кода
            /// </summary>
            NoCode = 3,
            /// <summary>
            /// КЖ - 5
            /// </summary>
            RedYellow5,
            /// <summary>
            /// Ж - 5
            /// </summary>
            Yellow5,
            /// <summary>
            /// З - 5
            /// </summary>
            Green5,
            /// <summary>
            /// КЖ - 7
            /// </summary>
            RedYellow7,
            /// <summary>
            /// Ж - 7
            /// </summary>
            Yellow7,
            /// <summary>
            /// З - 7
            /// </summary>
            Green7
        };

        /// <summary>
        /// Состояния контроллеров, концентраторов, промышленников, линий связи
        /// </summary>
        public enum SelfDiagStates
        {
            /// <summary>
            /// Есть контроль
            /// </summary>
            HasControl = 3
        };

        /// <summary>
        /// Пешеходный переход
        /// </summary>
        public enum CrossingWalkStates
        {
            /// <summary>
            /// Переход открыт
            /// </summary>
            Open = 3,
            /// <summary>
            /// Переход открыт и включено извещение
            /// </summary>
            Open_Notification,
            /// <summary>
            /// Переход закрыт
            /// </summary>
            Close,
            /// <summary>
            /// Переход закрыт и шлагбаум закрыт
            /// </summary>
            Close_Barrier,
            /// <summary>
            /// Переход закрыт, шлагбаум закрыт и УЗП поднят
            /// </summary>
            Close_Barrier_UZP
        } ;

    }

    public class Uni2DiagStates
    {
        /// <summary>
        /// Состояния контроллеров, концентраторов, промышленников, линий связи
        /// </summary>
        public enum SelfDiagStates
        {
            /// <summary>
            /// Нестабильная работа контроллера
            /// </summary>
            Controller_Unstable = 30004,
            /// <summary>
            /// Отказ контроллера
            /// </summary>
            Controller_Fail = 30006,
            /// <summary>
            /// Нестабильная работа концентратора
            /// </summary>
            Concentrator_Unstable = 30300,
            /// <summary>
            /// Отказ концентратора
            /// </summary>
            Concentrator_Fail = 30302,
            /// <summary>
            /// Нестабильная работа линии связи
            /// </summary>
            Link_Unstable = 30901,
            /// <summary>
            /// Отказ линии связи
            /// </summary>
            Link_Fail = 30903
        };
    }

	/// <summary>
	/// Класс состояния, устанавливающий при создании Отсутствие ТС (униф2)
	/// </summary>
	public class AbsenceObjState : BaseObjState
	{
		/// <summary>
		/// Создание объекта состояния, с установкой его в отказ
		/// </summary>
		/// <param name="obj">Объект унификации</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			SetState((int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS);
			return true;
		}

		/// <summary>
		/// Получить строковую интерпретацию общего состояния
		/// </summary>
		/// <param name="state">Состояние</param>
		/// <returns>Строка, содержащая информацию о состоянии. Пустая, если state - не общее состояние.</returns>
		protected static string GetGeneralStateStr(int state)
		{
			if (state == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS)
				return "Неопр.сост (отсутствие ТС)";
			else if (state == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS)
				return "Неопр.сост (несоответствие ТС)";
			else if (state == (int)Uni2States.GeneralStates.ObjectConserve)
				return "Объект законсервирован";
			return "";
		}

		protected override void TraceSwitchState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			//if( state == (int)Uni2States.SwitchStates. )
			traceStr = GetGeneralStateStr(state);

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
			else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
        }
		/// <summary>
		/// Вывод в TRACE состояния рельсовой цепи
		/// </summary>
		/// <param name="state">Состояние рельсовой цепи</param>
		protected override void TraceRCState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if (state == (int)Uni2States.PathSectionStates.Free)
				traceStr = "РЦ свободна, не замкнута";
			else if (state == (int)Uni2States.PathSectionStates.Free_Lock)
				traceStr = "РЦ свободна, замкнута";
			else if (state == (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease)
				traceStr = "РЦ свободна, замкнута, искусственно размыкается";
			else if (state == (int)Uni2States.PathSectionStates.Busy)
				traceStr = "РЦ занята, не замкнута";
			else if (state == (int)Uni2States.PathSectionStates.Busy_Lock)
				traceStr = "РЦ занята, замкнута";
			else if (state == (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease)
				traceStr = "РЦ занята, замкнута, искусственно размыкается";
			else
				traceStr = GetGeneralStateStr(state);

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
            else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
        }

		protected override void TraceSignalState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			traceStr = GetGeneralStateStr(state);
			
			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
            else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
        }

		protected void TraceCrossingState(int state)
		{
			// трассируем изменение состояния переезда
			string traceStr = "";
			if (state == (int)Uni2States.CrossingStates.Open)
				traceStr = "Переезд открыт";
			else if (state == (int)Uni2States.CrossingStates.Open_Notification)
				traceStr = "Переезд открыт и включено извещение";
			else if (state == (int)Uni2States.CrossingStates.Close)
				traceStr = "Переезд закрыт";
			else if (state == (int)Uni2States.CrossingStates.Close_Barrier)
				traceStr = "Переезд закрыт и шлагбаум закрыт";
			else if (state == (int)Uni2States.CrossingStates.Close_Barrier_UZP)
				traceStr = "Переезд закрыт, шлагбаум закрыт и УЗП поднят";
			else 
				traceStr = GetGeneralStateStr(state);
			
			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
            else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
        }

		protected void TracePowerSupplyState(int state)
		{
			// трассируем изменение состояния переезда
			string traceStr = "";
			if (state == (int)Uni2States.PowerSupplyStates.NoPower)
				traceStr = "Нет питания";
			else if (state == (int)Uni2States.PowerSupplyStates.InPower)
				traceStr = "Есть питание";
			else if (state == (int)Uni2States.PowerSupplyStates.InPowerAndActivate)
				traceStr = "Есть питание и устройство активно";
			else 
				traceStr = GetGeneralStateStr(state);
			
			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
            else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
        }

		protected override void TraceIndicatorState(int state)
		{
			// трассируем изменение состояния стрелки
			string traceStr = "";
			if (state == (int)Uni2States.IndObjStates.NotAlight)
				traceStr = "Индикатор не горит";
			else if (state == (int)Uni2States.IndObjStates.RedAlight)
				traceStr = "Индикатор горит красным цветом";
			else if (state == (int)Uni2States.IndObjStates.RedBlink)
				traceStr = "Индикатор мигает красным цветом";
			else if (state == (int)Uni2States.IndObjStates.WhiteAlight)
				traceStr = "Индикатор горит белым цветом";
			else if (state == (int)Uni2States.IndObjStates.WhiteBlink)
				traceStr = "Индикатор мигает белым цветом";
			else if (state == (int)Uni2States.IndObjStates.GreenAlight)
				traceStr = "Индикатор горит зеленым цветом";
			else if (state == (int)Uni2States.IndObjStates.GreenBlink)
				traceStr = "Индикатор мигает зеленым цветом";
			else if (state == (int)Uni2States.IndObjStates.YellowAlight)
				traceStr = "Индикатор горит желтым цветом";
			else if (state == (int)Uni2States.IndObjStates.YellowBlink)
				traceStr = "Индикатор мигает желтым цветом";
			else if (state == (int)Uni2States.IndObjStates.BlueAlight)
				traceStr = "Индикатор горит синий цветом";
			else if (state == (int)Uni2States.IndObjStates.BlueBlink)
				traceStr = "Индикатор мигает синий цветом";
			else 
				traceStr = GetGeneralStateStr(state);

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
			else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
		}

		protected void TraceBrakeState(int state)
		{
			// трассируем изменение состояния тормозного упора
			string traceStr = "";
			if (state == (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairNone)
				traceStr = "УТС снят и нет колесной пары в зоне УТС";
			else if (state == (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairNone)
				traceStr = "УТС установлен и нет колесной пары в зоне УТС";
			else if (state == (int)Uni2States.BrakeStates.UTS_Unknown_WheelPairNone)
				traceStr = "Нет контроля положения УТС и нет колесной пары в зоне УТС";
			else if (state == (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairInZone)
				traceStr = "УТС снят и колесная пара в зоне УТС";
			else if (state == (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairInZone)
				traceStr = "УТС установлен и колесная пара в зоне УТС";
			else if (state == (int)Uni2States.BrakeStates.UTS_Unknown_WheelPairInZone)
				traceStr = "Нет контроля положения УТС и колесная пара в зоне УТС";
			else 
				traceStr = GetGeneralStateStr(state);

			if (state == _controlObj.ObjState.Current)
				Debug.WriteLine(_controlObj.Name + ":\t" + traceStr);
			else
				Trace.WriteLine(_controlObj.Name + ":\t" + traceStr);
		}

        /// <summary>
        /// Получить состояние занятости:замкнутости:ИР объекта контроля. Работает для стрелок и рельсовых цепей
        /// Для объектов контроля других типов возвращает отсутствие ТС
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>состояние РЦ</returns>
	    public static int GetSwSectionState(UniObject obj)
        {
            if (obj.Type.GroupID == 1)//Рельсовая цепь
            {
                return obj.ObjState.Current;
            }
            else if (obj.Type.GroupID == 2)//Стрелка
            {
                switch(obj.ObjState.Current)
                {
                    case (int)Uni2States.SwitchStates.MI_Busy:
                    case (int)Uni2States.SwitchStates.PL_Busy:
                    case (int)Uni2States.SwitchStates.LC_Busy:
                        return (int) Uni2States.PathSectionStates.Busy;
                    case (int)Uni2States.SwitchStates.MI_Busy_Lock:
                    case (int)Uni2States.SwitchStates.PL_Busy_Lock:
                    case (int)Uni2States.SwitchStates.LC_Busy_Lock:
                        return (int)Uni2States.PathSectionStates.Busy_Lock;
                    case (int)Uni2States.SwitchStates.MI_Busy_Lock_EmergencyRelease:
                    case (int)Uni2States.SwitchStates.PL_Busy_Lock_EmergencyRelease:
                    case (int)Uni2States.SwitchStates.LC_Busy_Lock_EmergencyRelease:
                        return (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease;
                    case (int)Uni2States.SwitchStates.MI_Free:
                    case (int)Uni2States.SwitchStates.PL_Free:
                    case (int)Uni2States.SwitchStates.LC_Free:
                        return (int)Uni2States.PathSectionStates.Free;
                    case (int)Uni2States.SwitchStates.MI_Free_Lock:
                    case (int)Uni2States.SwitchStates.PL_Free_Lock:
                    case (int)Uni2States.SwitchStates.LC_Free_Lock:
                        return (int)Uni2States.PathSectionStates.Free_Lock;
                    case (int)Uni2States.SwitchStates.MI_Free_Lock_EmergencyRelease:
                    case (int)Uni2States.SwitchStates.PL_Free_Lock_EmergencyRelease:
                    case (int)Uni2States.SwitchStates.LC_Free_Lock_EmergencyRelease:
                        return (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease;
                    default:
                        return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            else
                return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
        }

        /// <summary>
        /// Сформировать состояние стрелки по состоянию стрелочной секции, плюсовому контролю и минусовому контролю
        /// </summary>
        /// <param name="SwSectionState">Состояние стрелочной секции</param>
        /// <param name="PKState">Плюсовой контроль</param>
        /// <param name="MKState">Минусовой контроль</param>
        /// <returns>Состояние стрелки. При двойном контроле, неопределенном состоянии секции возвращается несоответствие ТС</returns>
        public static int MakeSwitchState(int SwSectionState, bool PKState, bool MKState)
        {
            //если у секции общее состояние
            if (SwSectionState <= 2)
                return SwSectionState;

            //плюсовой контроль
            if (PKState && !MKState)
            {
                switch (SwSectionState)
                {
                    case (int)Uni2States.PathSectionStates.Free:
                        return (int)Uni2States.SwitchStates.PL_Free;
                    case (int)Uni2States.PathSectionStates.Free_Lock:
                        return (int)Uni2States.SwitchStates.PL_Free_Lock;
                    case (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.PL_Free_Lock_EmergencyRelease;
                    case (int)Uni2States.PathSectionStates.Busy:
                        return (int)Uni2States.SwitchStates.PL_Busy;
                    case (int)Uni2States.PathSectionStates.Busy_Lock:
                        return (int)Uni2States.SwitchStates.PL_Busy_Lock;
                    case (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.PL_Busy_Lock_EmergencyRelease;
                    default:
                        return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            //минусовой контроль
            else if (MKState && !PKState)
            {
                switch (SwSectionState)
                {
                    case (int)Uni2States.PathSectionStates.Free:
                        return (int)Uni2States.SwitchStates.MI_Free;
                    case (int)Uni2States.PathSectionStates.Free_Lock:
                        return (int)Uni2States.SwitchStates.MI_Free_Lock;
                    case (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.MI_Free_Lock_EmergencyRelease;
                    case (int)Uni2States.PathSectionStates.Busy:
                        return (int)Uni2States.SwitchStates.MI_Busy;
                    case (int)Uni2States.PathSectionStates.Busy_Lock:
                        return (int)Uni2States.SwitchStates.MI_Busy_Lock;
                    case (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.MI_Busy_Lock_EmergencyRelease;
                    default:
                        return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            //двойной контроль
            else if (PKState && MKState)
            {
                return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            //нет контроля
            else if (!PKState && !MKState)
            {
                switch (SwSectionState)
                {
                    case (int)Uni2States.PathSectionStates.Free:
                        return (int)Uni2States.SwitchStates.LC_Free;
                    case (int)Uni2States.PathSectionStates.Free_Lock:
                        return (int)Uni2States.SwitchStates.LC_Free_Lock;
                    case (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.LC_Free_Lock_EmergencyRelease;
                    case (int)Uni2States.PathSectionStates.Busy:
                        return (int)Uni2States.SwitchStates.LC_Busy;
                    case (int)Uni2States.PathSectionStates.Busy_Lock:
                        return (int)Uni2States.SwitchStates.LC_Busy_Lock;
                    case (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease:
                        return (int)Uni2States.SwitchStates.LC_Busy_Lock_EmergencyRelease;
                    default:
                        return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            else
                return (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
        }
        /// <summary>
        /// Получить список унифицированных объектов по ссылкам объекта
        /// </summary>
        /// <param name="o">Объект контроля</param>
        /// <returns>Список унифицированных объектов</returns>
        public static List<UniObject> GetUniObjectsByRefs(BaseControlObj o)
        {
            List<UniObject> objs = new List<UniObject>();
            foreach (DlxObject dlx in o.ObjData)
            {
                UniObject uni = dlx as UniObject;
                if (uni != null)
                    objs.Add(uni);
            }
            return objs;
        }

    }
    /// <summary>
    /// Класс определяющий состояние объекта по его ДС
    /// </summary>
    public class AbsenceObjState_Diag : UniObjectUpTO.TOTask
    {
        /// <summary>
        /// Обработка обновления перечня ДС
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            UpdateObjectState();
        }
        /// <summary>
        /// Обработка обновления значений сигналов
        /// </summary>
        public override void UpdateObjectSignals()
        {
            UpdateObjectState();
        }

        public virtual void UpdateObjectState()
        { }

        /// <summary>
        /// вызвать при изменении состояния
        /// </summary>
        /// <param name="state">новое состояние</param>
        protected void SetState(int state)
        {
            _controlObj.SetObjState(state);
        }
    }

	#region Общие состояния объектов
	/// <summary>
	/// Состояние законсервированного объекта
	/// </summary>
	public class ConserveObjState : AbsenceObjState
	{
		/// <summary>
		/// обработка исходных данных
		/// </summary>
		public override void UpdateObjectState()
		{
			SetState((int)Uni2States.GeneralStates.ObjectConserve);

            Debug.WriteLine("Conserved object " + _controlObj.Name + ": state updated");

			ResetModify();
		}

		public override int GetInvalidObjState(int defaultInvalidState)
		{
			return (int)Uni2States.GeneralStates.ObjectConserve;
		}
        /// <summary>
        /// Метод переопределен с целью установки законсервированного состояния после восстановления связи (т.к. состояние объекта не зависит от изменения сигналов)
        /// </summary>
        /// <returns>Признак необходимости обновления состояния</returns>
        public override bool IsModify()
        {
            return _controlObj.ObjState.Current == 0 || base.IsModify();
        }
    }
	#endregion

	#region Рельсовая цепь (РЦ)
	/// <summary>
	/// Состояние рельсовой цепи только с занятостью РЦ(RCSig)
	/// </summary>
	public class RCState_onlyRC : AbsenceObjState
	{
		/// <summary>
		/// Сигнал занятости РЦ
		/// </summary>
		protected ISigDiscreteLogic _RCSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("RCSig", out _RCSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных
		/// Если RCSig в 1, то занята (6), иначе свободна
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_RCSig.On)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
				else
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState_onlyRC " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние рельсовой цепи только с занятостью РЦ(RCSig)
    /// </summary>
    public class RCState_onlyRC_2 : RCState_onlyRC
    {
        /// <summary>
        /// Сигнал занятости РЦ
        /// </summary>
        protected ISigDiscreteLogic _RCSig2;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            // Получаем и подписываемся на рассылку изменения для RCSig2
            if (!AssignToSig("RCSig2", out _RCSig2))
                return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных
        /// Если RCSig в 1, то занята (6), иначе свободна
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_RCSig.On || _RCSig2.On)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
                else
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.WriteLine("RCState_onlyRC " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceRCState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние рельсовой цепи только с замыканием РЦ(ZSig)
	/// </summary>
	public class RCState_onlyZ : AbsenceObjState
	{
		/// <summary>
		/// Сигнал замыкания РЦ в маршруте
		/// </summary>
		protected ISigDiscreteLogic _ZSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("ZSig", out _ZSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных
		/// Если RCSig в 1, то занята (6), иначе свободна
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_ZSig.On)
					state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
				else
					state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState_onlyZ " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи с сигналами RCSig(заянтость) и ZSig(замыкание)
	/// </summary>
	public class RCState_RC_Z : RCState_onlyRC
	{
		/// <summary>
		/// Замыкание
		/// </summary>
		protected ISigDiscreteLogic _ZSig;
        /// <summary>
        /// Унифицированный объект-владелец экземпляра алгоритма
        /// </summary>
        protected UniObject _obj = null;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для ZSig
			if (!AssignToSig("ZSig", out _ZSig))
				return false;

            _obj = obj;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_RCSig.On && _ZSig.On)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock; //РЦ занята, замкнута
				else if (_RCSig.On && _ZSig.Off)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
				else if (_RCSig.Off && _ZSig.Off)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
				else if (_RCSig.Off && _ZSig.On)
                    state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
				else
					state = (int) Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;

                #region Отладочный лог сигналов на входе подзадач контроля
                //if (_obj != null
                //    && (_obj.Name == "1П" || _obj.Name == "IIП" || _obj.Name == "IIIП")
                //    && _obj.Site.ID == 11167)
                //{
                //    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\_signalTrace.txt", true);
                //    sw.WriteLine(string.Format("Object={0}, RCSig={1} (в АДК - {3}), ZSig={2}  (в АДК - {4})"
                //        , _obj.Name
                //        , _RCSig.On
                //        , _ZSig.On
                //        , ConcatSigValues(_RCSig.IOSignals)
                //        , ConcatSigValues(_ZSig.IOSignals)));
                //    sw.Flush();
                //    sw.Close();
                //}
                #endregion
            }
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("RCState_RC_Z " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}

        public static string ConcatSigValues(IOSignal[] sigs)
        {
            string res = string.Empty;
            foreach (IOSignal s in sigs)
                res += s.SignalState + ",";
            return res.TrimEnd(new char[] { ',' });
        }
        public static string ConcatSigCurrent(IOSignal[] sigs)
        {
            string res = string.Empty;
            foreach (IOSignal s in sigs)
                res += s.GetCurrent.ToString() + ",";
            return res.TrimEnd(new char[] { ',' });
        }
	}

    /// <summary>
    /// Состояние рельсовой цепи с сигналами RCSig(заянтость) и ZSig(замыкание)
    /// </summary>
    public class RCStateRC_Z : RCState_RC_Z { }

	/// <summary>
	/// Состояние рельсовой цепи с сигналами RCSig(Занятость), ZSig(Замкнутость), KRISig(искусственная разделка)
	/// </summary>
	public class RCState : RCState_RC_Z
	{
		/// <summary>
		/// Искусственная разделка
		/// </summary>
		protected ISigDiscreteLogic _KRISig;
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("KRISig", out _KRISig))
				return false;
			return true;
		}
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (!_RCSig.On && !_ZSig.On)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
				else if (_RCSig.On && !_ZSig.On)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
				else if (!_RCSig.On && _ZSig.On && !_KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
				else if (_RCSig.On && _ZSig.On && !_KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock; //РЦ занята, замкнута
				else if (!_RCSig.On && _ZSig.On && _KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease; //РЦ свободна, замкнута, искусственно размыкается
				else if (_RCSig.On && _ZSig.On && _KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease; //РЦ занята, замкнута, искусственно размыкается
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS; //Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}
	/// <summary>
	/// Состояние рельсовой цепи с сигналами RCSig(Занятость), ZSig(Замкнутость), KRISig(искусственная разделка), KRISigZ(искусственная разделка занятой РЦ)
	/// </summary>
	public class RCState_KRISigZ : RCState
	{
		/// <summary>
		/// Искусственная разделка занятой РЦ
		/// </summary>
		protected ISigDiscreteLogic _KRISigZ;

		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("KRISigZ", out _KRISigZ))
				return false;
			return true;
		}
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_RCSig.On && _ZSig.On && (_KRISig.On || _KRISigZ.On))
                    state = (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease; //РЦ занята, замкнута, искусственно размыкается
				else if (_RCSig.On && _ZSig.On && _KRISig.Off && _KRISigZ.Off)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock; //РЦ занята, замкнута
				else if (_RCSig.On && _ZSig.Off && _KRISig.Off && _KRISigZ.Off)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
				else if (_RCSig.Off && _ZSig.On && (_KRISig.On || _KRISigZ.On))
                    state = (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease; //РЦ свободна, замкнута, искусственно размыкается
				else if (_RCSig.Off && _ZSig.On && _KRISig.Off && _KRISigZ.Off)
                    state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
				else if (_RCSig.Off && _ZSig.Off && _KRISig.Off && _KRISigZ.Off)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS; //Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи с сигналами RCSig(Занятость), ZSig(Замкнутость), KRISig(искусственная разделка)
	/// </summary>
	public class RCState_MPC_I : RCState_RC_Z
	{
		/// <summary>
		/// Наличие информации
		/// </summary>
		protected ISigDiscreteLogic _InfoSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("InfoSig", out _InfoSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_InfoSig.Off)
					state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
				else
				{
					base.UpdateObjectState();
					return;
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("RCState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние рельсовой цепи с сигналами RCSig(занятость), NISig(искл реле нечетное), ChISig(искл реле четное), KRISig(искусственная разделка)
	/// </summary>
	public class ObjStateRC_RC_NI_CHI_KRI : AbsenceObjState
	{
		/// <summary>
		/// Занятость РЦ
		/// </summary>
		protected ISigDiscreteLogic _RCSig;
		/// <summary>
		/// Исключающее реле (нечетное)
		/// </summary>
		protected ISigDiscreteLogic _NISig;
		/// <summary>
		/// Исключающее реле (четное)
		/// </summary>
		protected ISigDiscreteLogic _ChISig;
		/// <summary>
		/// Искусственная разделка
		/// </summary>
		protected ISigDiscreteLogic _KRISig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig("RCSig", out _RCSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для NISig
			if (!AssignToSig("NISig", out _NISig))
				return false;
			// Получаем и подписываемся на рассылку изменения для ChISig
			if (!AssignToSig("ChISig", out _ChISig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KRISig
			if (!AssignToSig("KRISig", out _KRISig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_RCSig.On && (_NISig.On || _ChISig.On) && _KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease;// РЦ занята, замкнута, искусственно размыкается
				else if(_RCSig.On && (_NISig.On || _ChISig.On) && _KRISig.Off)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock; //РЦ занята, замкнута
				else if (_RCSig.On && !(_NISig.On || _ChISig.On) && _KRISig.Off)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
				else if (_RCSig.Off && (_NISig.On || _ChISig.On) && _KRISig.On)
                    state = (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease; //РЦ свободна, замкнута, искусственно размыкается
				else if (_RCSig.Off && (_NISig.On || _ChISig.On) && _KRISig.Off)
                    state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
				else if (_RCSig.Off && !(_NISig.On || _ChISig.On) && _KRISig.Off)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
				else
					state = 1; //Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Debug.WriteLine("ObjStateRC_RC_NI_CHI_KRI " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceRCState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора переезда. Подразумевает наличие хотя бы одного из сиганлов.
    /// </summary>
    public class RCState_MultiSig : AbsenceObjState
    {
        /// <summary>
        /// Максимальный индекс, дополняющий шаблон в имени сигнала
        /// </summary>
        protected byte _max = 8;
        /// <summary>
        /// Сигналы занятости РЦ
        /// </summary>
        protected List<ISigDiscreteLogic> _RCSigs = new List<ISigDiscreteLogic>();
        /// <summary>
        /// Сигналы замыкания РЦ в маршруте
        /// </summary>
        protected List<ISigDiscreteLogic> _ZSigs = new List<ISigDiscreteLogic>();
        /// <summary>
        /// Сигналы искусственной разделки
        /// </summary>
        protected List<ISigDiscreteLogic> _KRISigs = new List<ISigDiscreteLogic>();

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            bool bRes = true;

            if (!MultiSig.AssignToSignals(this, "RCSig", _max, out _RCSigs))
                bRes = false;
            if (!MultiSig.AssignToSignals(this, "ZSig", _max, out _ZSigs))
                bRes = false;
            if (!MultiSig.AssignToSignals(this, "KRISig", _max, out _KRISigs))
                bRes = false;

            return bRes;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                bool rcSig = MultiSig.Or(_RCSigs);
                bool zSig = MultiSig.Or(_ZSigs);
                bool kriSig = MultiSig.Or(_KRISigs);

                if (rcSig && zSig && kriSig)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock_EmergencyRelease; //РЦ занята, замкнута, искусственно размыкается
                else if (rcSig && zSig && !kriSig)
                    state = (int)Uni2States.PathSectionStates.Busy_Lock; //РЦ занята, замкнута
                else if (rcSig && !zSig && !kriSig)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
                else if (!rcSig && zSig && kriSig)
                    state = (int)Uni2States.PathSectionStates.Free_Lock_EmergencyRelease; //РЦ свободна, замкнута, искусственно размыкается
                else if (!rcSig && zSig && !kriSig)
                    state = (int)Uni2States.PathSectionStates.Free_Lock; //РЦ свободна, замкнута
                else if (!rcSig && !zSig && !kriSig)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS; //Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceRCState(state);
            SetState(state);
            ResetModify();
        }
    }

    /// <summary>
    /// Класс с функциями для обработки наборов сигналов
    /// </summary>
    public class MultiSig : AbsenceObjState
    {
        /// <summary>
        /// Получение набора сигналов у объекта контроля
        /// </summary>
        /// <param name="bs">Состояние</param>
        /// <param name="pattern">Шаблон имени сигналов</param>
        /// <param name="n">Максимальный индекс, дополняющий шаблон в имени сигнала</param>
        /// <param name="sigs">Набор полученных сигналов</param>
        /// <returns>Получен хотя бы один сигнал</returns>
        public static bool AssignToSignals(BaseObjState bs, string pattern, byte n, out List<ISigDiscreteLogic> sigs)
        {
            sigs = new List<ISigDiscreteLogic>();
            for (int i = 1; i <= n; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                if (bs.AssignToSig(pattern + i.ToString(), out tmpSig))
                    sigs.Add(tmpSig);
            }
            return sigs.Count > 0;
        }
        /// <summary>
        /// Операция "ИЛИ" (хотя бы один из сигналов в состоянии 1)
        /// </summary>
        /// <param name="Sigs">Набор дискретных сигналов</param>
        /// <returns>Результат операции</returns>
        public static bool Or(List<ISigDiscreteLogic> Sigs)
        {
            foreach (ISigDiscreteLogic Sig in Sigs)
                if (Sig.On)
                    return true;
            return false;
        }
        /// <summary>
        /// Операция "И" (все сигналы в состоянии 1)
        /// </summary>
        /// <param name="Sigs">Набор дискретных сигналов</param>
        /// <returns>Результат операции</returns>
        public static bool And(List<ISigDiscreteLogic> Sigs)
        {
            foreach (ISigDiscreteLogic Sig in Sigs)
                if (Sig.Off)
                    return false;
            return true;
        }
    }

    /// <summary>
    /// Состояние рельсовой цепи АБТЦМ с кодовым сигналом (RCabtcmSig)
    /// </summary>
    public class RCState_ABTCM_Code : AbsenceObjState
    {
        /// <summary>
        /// Кодовый сигнал БКРЦ
        /// </summary>
        protected ISigDiscreteLogic _RCabtcmSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для _RCabtcmSig
            if (!AssignToSig("RCabtcmSig", out _RCabtcmSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных
        /// Если RCSig в 1, то занята (6), иначе свободна
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {

                if (_RCabtcmSig.IOSignals.Length > 0)
                {
                    uint code = _RCabtcmSig.IOSignals[0].GetCurrent;

                    if (code == 0 || code == 2)
                        state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
                    else if (code == 1 || code == 3)
                        state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.WriteLine("RCState_onlyRC " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceRCState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние рельсовой цепи АБТЦМ только с сигналами обычной, логической и ложной занятости, а также сигналом свободности РЦ
    /// </summary>
    public class RCState_ABTCM : AbsenceObjState
    {
        /// <summary>
        /// Контроль занятости РЦ
        /// </summary>
        protected ISigDiscreteLogic _RCSig;
        /// <summary>
        /// Контроль логической занятости РЦ
        /// </summary>
        protected ISigDiscreteLogic _RCLogicSig;
        /// <summary>
        /// Контроль ложной занятости РЦ
        /// </summary>
        protected ISigDiscreteLogic _RCFalseSig;
        /// <summary>
        /// Контроль логической занятости РЦ
        /// </summary>
        protected ISigDiscreteLogic _RCFreeSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RCSig", out _RCSig))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RCLogicSig", out _RCLogicSig))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RCFalseSig", out _RCFalseSig))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RCFreeSig", out _RCFreeSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных
        /// Если RCSig в 1, то занята (6), иначе свободна
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {

                if (_RCSig.Off && _RCLogicSig.Off && _RCFalseSig.Off && _RCFreeSig.On)
                    state = (int)Uni2States.PathSectionStates.Free; //РЦ свободна, не замкнута
                else if (_RCSig.On && _RCLogicSig.Off && _RCFalseSig.Off && _RCFreeSig.Off
                      || _RCSig.Off && _RCLogicSig.On && _RCFalseSig.Off && _RCFreeSig.Off
                      || _RCSig.Off && _RCLogicSig.Off && _RCFalseSig.On && _RCFreeSig.Off)
                    state = (int)Uni2States.PathSectionStates.Busy; //РЦ занята, не замкнута
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.WriteLine("RCState_onlyRC " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceRCState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние рельсовой цепи только с занятостью РЦ(RCSig)
    /// </summary>
    public class RCState_NoDiscrSigs : AbsenceObjState
    {
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            SetState((int)Uni2States.PathSectionStates.Free);
            return true;
        }
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.PathSectionStates.Free;

            TraceRCState(state);

            SetState(state);

            ResetModify();
        }
    }


    
    #endregion

	#region Стрелка
	/// <summary>
	/// Состояние основной стрелочной секции
	/// </summary>
	public class SwitchState : AbsenceObjState
	{
		/// <summary>
		/// Плюсовой контроль стрелки
		/// </summary>
		protected ISigDiscreteLogic _PKSig;
		/// <summary>
		/// Минусовой котроль стрелки
		/// </summary>
        protected ISigDiscreteLogic _MKSig;
		/// <summary>
		/// Стрелочная секция
		/// </summary>
        protected UniObject _SwSection;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!CreateBase(obj))
				return false;

			ObjData objData = (_controlObj.GetObject("SwSection") as ObjData);
			if (objData == null)
			{
				Console.WriteLine("Can't get object SwSection as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
				return false;
			}
			if (objData.Object is UniObject)
				_SwSection = objData.Object as UniObject;
			else
			{
				Console.WriteLine("Can't convert ObjData for SwSection to UniObject!");
				return false;
			}

			// Получаем и подписываемся на рассылку изменения для _PKSig
		    AssignToSig("PKSig", out _PKSig);

			// Получаем и подписываемся на рассылку изменения для _MKSig
			AssignToSig("MKSig", out _MKSig);

			// Получаем и подписываемся на рассылку изменения для SwSection
			AssignToObj("SwSection");

			return true;
		}

        /// <summary>
        /// Создание состояние базового класса
        /// </summary>
        /// <param name="obj">Унифицированный объект</param>
        /// <returns>true, в случае успеха</returns>
        protected bool CreateBase(UniObject obj)
        {
            return base.Create(obj);
        }

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				//обновляем состяние стрелочной секции
				_SwSection.UpdateObjectState();
				// формируем состояние стрелки
				state = MakeSwitchState(_SwSection.ObjState.Current, _PKSig != null && _PKSig.On, _MKSig != null && _MKSig.On);
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("SwitchState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("SwitchState error update: " + ex.ToString());
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSwitchState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние slave стрелки
    /// </summary>
    public class SwitchSlaveState : SwitchState
    {
        /// <summary>
        /// Основная стрелка
        /// </summary>
        protected UniObject _MainSwitch;
        
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!CreateBase(obj))
                return false;

            //получаем основную стрелку
            ObjData objData = (_controlObj.GetObject("Main") as ObjData);
            if (objData == null)
            {
				Console.WriteLine("Can't get object MainSwitch as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            if (objData.Object is UniObject)
                _MainSwitch = objData.Object as UniObject;
            else
            {
                Console.WriteLine("Can't convert ObjData for MainSwitch to UniObject!");
                return false;
            }
            
            // Получаем и подписываемся на рассылку изменения для _PKSig
            AssignToSigByObj("Main", "PKSig", out _PKSig);

           // Получаем и подписываемся на рассылку изменения для _MKSig
            AssignToSigByObj("Main", "MKSig", out _MKSig);
            
            // Получаем и подписываемся на рассылку изменения для Main
            AssignToObj("Main");

            // Пытаемся получить
            _SwSection = GetObjectRef("SwSection") as UniObject;
            if (_SwSection == null)
            {
                // Пытаемся получить SwSection  у основной стрелки
                _SwSection = GetObjectRef(_MainSwitch, "SwSection") as UniObject;
                if (_SwSection == null)
                {
					Console.WriteLine("Can't get object SwSection as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                    return false;
                }
            }
            AssignToObj(_SwSection);

            return true;
        }
    }

    /// <summary>
    /// Состояние основной стрелочной секции с алгоритмом определения замыкания в стрелочной секции
    /// </summary>
    public class SwitchStateAnalyse : SwitchState
    {
        protected ISigDiscreteLogic _PreSecPKSig;
        protected ISigDiscreteLogic _PreSecMKSig;
       
        /// <summary>
        /// следующий объект по плюсу
        /// </summary>
        protected UniObject _NextPl;
        protected UniObject NextPl
        {
            get
            {
                if (_NextPl != null)
                    return _NextPl;
                ObjData objData = (_controlObj.GetObject("NextPl") as ObjData);
                if (objData != null)
                {
                    _NextPl = objData.Object as UniObject;
                    return _NextPl;
                }
                return null;
            }
        }
        /// <summary>
        /// Следующий объект по минусу
        /// </summary>
        protected UniObject _NextMi;
        protected UniObject NextMi
        {
            get
            {
                if (_NextMi != null)
                    return _NextMi;
                ObjData objData = (_controlObj.GetObject("NextMi") as ObjData);
                if (objData != null)
                {
                    _NextMi = objData.Object as UniObject;
                    return _NextMi;
                }
                return null;
            }
        }
        /// <summary>
        /// Предыдущий объект
        /// </summary>
        protected UniObject _PrevSec;
        protected UniObject PrevSec
        {
            get
            {
                if (_PrevSec != null)
                    return _PrevSec;
                ObjData objData = (_controlObj.GetObject("PrevSec") as ObjData);
                if (objData != null)
                {
                    _PrevSec = objData.Object as UniObject;
                    return _PrevSec;
                }
                return null;
            }
        }

        /// <summary>
        /// Вариант расположения стрелки
        /// </summary>
        protected int _Variant;

        /// <summary>
        /// Возможные варианты расположения стрелки
        /// </summary>
        protected enum Variants
        {
            /// <summary>
            /// Стрелка является предыдущей для следующих. Для определения её занятости 
            /// смотрим на предыдущую стрелку. Если её нет, то замыкаем по состоянию рельсовой цепи
            /// Если она есть и переключена в наше направление, то устанавливаем состояние такое же как у неё.
            /// Если переключена в другое направление, значит наша стрелка не учавствует в маршруте (не замыкаем)
            /// </summary>
            swt1 = 0x01,
            /// <summary>
            /// Стрелка варианта swt1, причем крайняя в секции.
            /// </summary>
            swt1a = 0x02,
            /// <summary>
            /// Стрелка варианта swt1, причем не крайняя в секции и находится по плюсу
            /// от предыдущей. Если этот бит не установлен, то по минусу
            /// </summary>
            swt1P = 0x04,
            swt2 = 0x10
        }
        /// <summary>
        /// Принадлежит ли стрелка первому варианту (для всех следующих она - предыдущая)
        /// </summary>
        /// <returns>true, если условие выполняется</returns>
        protected bool IsFirstVariant()
        {
            // подучаем предыдущую стрелку следующей по плюсу в этой секции
            if (NextPl != null)
            {
                // определяем, находится ли следущая по плюсу в той же секции
                ObjData objDataSection = NextPl.GetObject("SwSection") as ObjData;
                if (objDataSection != null)
                {
                    UniObject objSection = objDataSection.Object as UniObject;
                    if (objSection == null)
                        throw new ArgumentException("Не удалось получить SwSection для объекта " + NextPl.Name, "SwSection");
                    //если объект находится в той же стрелочной секции
                    if (objSection == _SwSection)
                    {
                        // получаем объект
                        ObjData objData = (NextPl.GetObject("PrevSec") as ObjData);
                        if (objData == null)
                        {
                            Debug.WriteLine("Не удалось получить PreSec для объекта " + NextMi.Name);
                            return false;
                        }
                        UniObject obj = objData.Object as UniObject;
                        // если предыдущая стрелка следующей по плюсу не является нашей стрелкой, то это не то.
                        if (obj != _controlObj)
                            return false;

                        // если получили предыдущую следующей по плюсу и это наша стрелка, то - наш вариант
                        return true;
                    }
                }
                else
                    Debug.WriteLine("Не удалось получить SwSection для объекта " + NextPl.Name, "SwSection");
            }


            // получаем предыдущую стрелку следующей по минусу
            if (NextMi != null)
            {
                // определяем, находится ли следущая по минусу в той же секции
                ObjData objDataSection = NextMi.GetObject("SwSection") as ObjData;
                if (objDataSection != null)
                {
                    UniObject objSection = objDataSection.Object as UniObject;
                    if (objSection == null)
                        throw new ArgumentException("Не удалось получить SwSection для объекта " + NextMi.Name);
                    //если объект находится в той же стрелочной секции
                    if (objSection == _SwSection)
                    {
                        ObjData objData = (NextMi.GetObject("PrevSec") as ObjData);
                        if (objData == null)
                        {
                            Debug.WriteLine("Не удалось получить PreSec для объекта " + NextMi.Name);
                            return false;
                        }
                        UniObject obj = objData.Object as UniObject;
                        // если предыдущая стрелка следующей по плюсу не является нашей стрелкой, то это не то.
                        if (obj != _controlObj)
                            return false;

                        return true;
                    }
                }
                else
                    Debug.WriteLine("Не удалось получить SwSection для объекта " + NextMi.Name);
            }

            return true;
        }

        /// <summary>
        /// Является ли стрелка крайней в стрелочной секции
        /// </summary>
        /// <returns>true, если крайняя стрелка</returns>
        protected bool IsLastInSection()
        {
            // получаем секцию предыдущего объекта. если он совпадает с нашей,
            // то это не крайняя стрелка
            if (PrevSec != null)
            {
                ObjData objData = (PrevSec.GetObject("SwSection") as ObjData);
                if (objData == null)
                {
                    Debug.WriteLine("Не удалось получить секцию объекта " + PrevSec.Name);
                    return true;
                }
                UniObject obj = objData.Object as UniObject;
                if (obj == null)
                    throw new ArgumentException("Не удалось получить секцию объекта " + PrevSec.Name);
                //если секция предыдущего объекта такая же как текущего, то эта стрелка не последняя
                if (obj != _SwSection)
                    return true;
                // проверяем, что наша стрелка предыдущая для предыдущей -> считаем нашу крайней
                objData = (PrevSec.GetObject("PrevSec") as ObjData);
                if( objData != null )
                {
                    obj = objData.Object as UniObject;
                    if (obj != null && obj == _controlObj)
                        return true;
                }
                return false;
            }
            return true;
        }
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            //определяем вариант расположения стрелки
            // если перед стрелкой секция заканчивается и для каждой следующей она является предидущей, то первый вариант

            if (_SwSection == null)
            {
                Debug.WriteLine("Не удалось получить стрелочную секцию для {0}", _controlObj.Name);
                return false;
            }

            if (IsFirstVariant())
            {
                _Variant |= (int)Variants.swt1;
                if (IsLastInSection())
                    _Variant |= (int)Variants.swt1a;
                else
                {// Не крайняя стрелка. Получаем сигналы контроля предыдущей и кем мы ей приходжимся (плюс\минус)
                    // плюсовой контроль
                    ObjData objData = PrevSec.GetObject("PKSig") as ObjData;
                    if (objData == null)
                    {
                        objData = PrevSec.GetObject("Main") as ObjData;
                        if( objData != null )
                        {
                            UniObject mainsw = objData.Object as UniObject;
                            if(mainsw != null)
                                objData = mainsw.GetObject("PKSig") as ObjData;
                        }
                    }
                    if (objData != null && objData.IsSigDiscretLogic)
                        _PreSecPKSig = objData.SigDiscreteLogic;
                    
                    // минусовой контроль
                    objData = PrevSec.GetObject("MKSig") as ObjData;
                    if (objData == null)
                    {
                        objData = PrevSec.GetObject("Main") as ObjData;
                        if (objData != null)
                        {
                            UniObject mainsw = objData.Object as UniObject;
                            if (mainsw != null)
                                objData = mainsw.GetObject("MKSig") as ObjData;
                        }
                    }
                    if (objData != null && objData.IsSigDiscretLogic)
                        _PreSecMKSig = objData.SigDiscreteLogic;
                    
                    // Плюс предыдущей стрелки
                    objData = PrevSec.GetObject("NextPl") as ObjData;
                    if (objData != null)
                    {
                        UniObject objNextPl = objData.Object as UniObject;
                        if (objNextPl != null && objNextPl == _controlObj)
                            _Variant |= (int)Variants.swt1P;
                    }
                    else
                        Debug.WriteLine(_controlObj.Name + ": Не удалось получить объект по плюсу стрелки " + PrevSec.Name, "PreSec->NextPl");
                }
            }

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                _SwSection.UpdateObjectState();
                int swSecState = _SwSection.ObjState.Current;
                if ((_Variant & (int)Variants.swt1) == 1)
                {
                    // если стрелка крайняя в секции, то берем состояния стрелочной секции
                    if ((_Variant & (int)Variants.swt1a) != 0)
                        state = MakeSwitchState( swSecState, _PKSig.On, _MKSig.On);
                    else
                    {
                        // стрелка не крайняя. Смотрим по предыдущей.
                        PrevSec.UpdateObjectState();
                        // если она переключена в нашем направлении, то берем её состояние занятия-замыкания
                        if (((_Variant & (int)Variants.swt1P) != 0 && _PreSecPKSig.On) || ((_Variant & (int)Variants.swt1P) == 0 && _PreSecMKSig.On))
                        {
                            swSecState = GetSwSectionState(PrevSec);
                            state = MakeSwitchState(swSecState, _PKSig.On, _MKSig.On);
                        }
                        else
                        {
                            //если предыдущая стрелка не переключена в нашем направлении, то формируем состояние по свободной незамкнутой РЦ
                            state = MakeSwitchState((int)Uni2States.PathSectionStates.Free, _PKSig.On, _MKSig.On);
                        }
                    }
                }
                else
                    state = MakeSwitchState(swSecState, _PKSig.On, _MKSig.On);
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSwitchState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние slave стрелки с алгоритмом определения замыкания в стрелочной секции
    /// </summary>
    public class SwitchSlaveStateAnalyse : SwitchStateAnalyse
    {
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для _PKSig
            AssignToSigByObj("Main", "PKSig", out _PKSig);

            // Получаем и подписываемся на рассылку изменения для _MKSig
            AssignToSigByObj("Main", "MKSig", out _MKSig);

            // Получаем и подписываемся на рассылку изменения для Main
            AssignToObj("Main");
           
            return true;
        }
    }

	/// <summary>
	/// Новый алгоритм формирования состояния стрелки, учитывающий положение
	/// стрелок текущей РЦ, при котором анализируемая стрелка оказалась в маршруте
	/// </summary>
	public class SwitchStateNewAlg : SwitchState
	{
        /// <summary>
        /// Имитация значения ПК для стрелки МПЦ
        /// </summary>
        public class PKDiscreteLogic : ISigDiscreteLogic
		{
			ISigDiscreteLogic _PKMKSig;
			ISigDiscreteLogic _LoseCtrlSig;
			public PKDiscreteLogic(ISigDiscreteLogic PKMKSig, ISigDiscreteLogic LoseCtrlSig)
			{
				_PKMKSig = PKMKSig;
				_LoseCtrlSig = LoseCtrlSig;
			}

			#region ISigDiscreteLogic Members

			public bool On
			{
				get { return _PKMKSig.Off && _LoseCtrlSig.Off; }
			}

			public bool Off
			{
				get { return _PKMKSig.On && _LoseCtrlSig.Off; }
			}

			public bool Blink
			{
				get { return false; }
			}

			#endregion

			#region ISignalUnion Members

			public IOSignal[] IOSignals
			{
				get { return new IOSignal[] { _PKMKSig.IOSignals[0], _LoseCtrlSig.IOSignals[0] }; }
			}

			#endregion
		}
        /// <summary>
        /// Имитация значения МК для стрелки МПЦ
        /// </summary>
        public class MKDiscreteLogic : ISigDiscreteLogic
		{
			ISigDiscreteLogic _PKMKSig;
			ISigDiscreteLogic _LoseCtrlSig;
			public MKDiscreteLogic(ISigDiscreteLogic PKMKSig, ISigDiscreteLogic LoseCtrlSig)
			{
				_PKMKSig = PKMKSig;
				_LoseCtrlSig = LoseCtrlSig;
			}

			#region ISigDiscreteLogic Members

			public bool On
			{
				get { return _PKMKSig.On && _LoseCtrlSig.Off; }
			}

			public bool Off
			{
				get { return _PKMKSig.Off && _LoseCtrlSig.Off; }
			}

			public bool Blink
			{
				get { return false; }
			}

			#endregion

			#region ISignalUnion Members

			public IOSignal[] IOSignals
			{
				get { return new IOSignal[] { _PKMKSig.IOSignals[0], _LoseCtrlSig.IOSignals[0] }; }
			}

			#endregion
		}

        /// <summary>
        /// Имитация значения ПК по состоянию стрелки
        /// </summary>
        class PKDiscreteLogicFromSwitch : ISigDiscreteLogic
        {
            public PKDiscreteLogicFromSwitch(UniObject obj)
            {
                _sw = obj;
            }
            protected UniObject _sw;
            protected List<int> targetStates = new List<int> { 3, 4, 5, 6, 7, 8 };

            public bool On { get { return targetStates.Contains(_sw.ObjState.Current); } }
            public bool Off { get { return !On; } }
            public bool Blink { get { return false; } }
            public IOSignal[] IOSignals { get { return new IOSignal[] { }; } }
        }
        /// <summary>
        /// Имитация значения МК по состоянию стрелки
        /// </summary>
        class MKDiscreteLogicFromSwitch : ISigDiscreteLogic
        {
            public MKDiscreteLogicFromSwitch(UniObject obj)
            {
                _sw = obj;
            }
            protected UniObject _sw;
            protected List<int> targetStates = new List<int> { 9, 10, 11, 12, 13, 14 };

            public bool On { get { return targetStates.Contains(_sw.ObjState.Current); } }
            public bool Off { get { return !On; } }
            public bool Blink { get { return false; } }
            public IOSignal[] IOSignals { get { return new IOSignal[] { }; } }
        }
        
        /// <summary>
		/// Маршрут из стрелок
		/// </summary>
		public class Route
		{
			/// <summary>
			/// узел маршрута
			/// </summary>
			public class Node
			{
				public Node(UniObject sw, bool PK)
				{
					_switch = sw;
					_PK = PK;

					ObjData objData = (sw.GetObject("PKSig") as ObjData);
					if (objData == null && (sw.GetObject("Main") as ObjData) != null)
						objData = ((sw.GetObject("Main") as ObjData).Object as UniObject).GetObject("PKSig") as ObjData;
                    if (objData != null && objData.IsSigDiscretLogic)
                    {   // вариант для тиража
                        _PKSig = objData.SigDiscreteLogic;
                    }
                    else
                    {   // вариант для разраба
                        _PKSig = new PKDiscreteLogicFromSwitch(sw);
                    }
					
					objData = (sw.GetObject("MKSig") as ObjData);
					if (objData == null && (sw.GetObject("Main") as ObjData) != null)
						objData = ((sw.GetObject("Main") as ObjData).Object as UniObject).GetObject("MKSig") as ObjData;
					if (objData != null && objData.IsSigDiscretLogic)
                    {   // вариант для тиража
						_MKSig = objData.SigDiscreteLogic;
                    }
                    else
                    {   // вариант для разраба
                        _MKSig = new MKDiscreteLogicFromSwitch(sw);
                    }

					// на случай стрелки МПЦ
					if (_PKSig == null && _MKSig == null)
					{
						ObjData objDataPKMK = (sw.GetObject("PKMKSig") as ObjData);
						ObjData objDataLoseCtrl = (sw.GetObject("LoseCtrlSig") as ObjData);
						if (objDataPKMK != null && objDataLoseCtrl != null)
						{
							_PKSig = new PKDiscreteLogic(objDataPKMK.SigDiscreteLogic, objDataLoseCtrl.SigDiscreteLogic);
							_MKSig = new MKDiscreteLogic(objDataPKMK.SigDiscreteLogic, objDataLoseCtrl.SigDiscreteLogic);
						}
					}
				}
				/// <summary>
				/// Стрелка
				/// </summary>
				protected UniObject _switch;
				/// <summary>
				/// Плюсовой контроль стрелки
				/// </summary>
				protected ISigDiscreteLogic _PKSig;
				/// <summary>
				/// Минусовой котроль стрелки
				/// </summary>
				protected ISigDiscreteLogic _MKSig;
				/// <summary>
				/// Признак необходимого положения контроля (1 - в плюсе; 0 - в минусе)
				/// </summary>
				protected bool _PK;
				/// <summary>
				/// В нужном ли положении стрелка
				/// </summary>
				/// <returns></returns>
				public bool IsTrue()
				{
					if (_PK && _PKSig.On && _MKSig.Off)
						return true;
					else if (!_PK && _PKSig.Off && _MKSig.On)
						return true;
					else
						return false;
				}

                public override string ToString()
                {
                    return _switch != null ? _switch.Name : base.ToString();
                }

            }

			/// <summary>
			/// список узлов маршрута
			/// </summary>
			protected List<Node> _PrevNodes = new List<Node>();
			/// <summary>
			/// список узлов маршрута
			/// </summary>
			protected List<Node> _NextPlNodes = new List<Node>();
			/// <summary>
			/// список узлов маршрута
			/// </summary>
			protected List<Node> _NextMiNodes = new List<Node>();
            /// <summary>
            /// список узлов маршрута другой РЦ
            /// </summary>
            protected List<Node> _PrevNodesOther = new List<Node>();
            /// <summary>
            /// список узлов маршрута другой РЦ
            /// </summary>
            protected List<Node> _NextPlNodesOther = new List<Node>();
            /// <summary>
            /// список узлов маршрута другой РЦ
            /// </summary>
            protected List<Node> _NextMiNodesOther = new List<Node>();

			/// <summary>
			/// Добавить узел в маршрут
			/// </summary>
			/// <param name="sw">стрелка</param>
			/// <param name="PK">требуемое положение</param>
			public void AddPrevNode(UniObject sw, bool PK, bool fromOtherRC)
			{
                if (fromOtherRC)
                    _PrevNodesOther.Add(new Node(sw, PK));
                else
                    _PrevNodes.Add(new Node(sw, PK));
			}

			/// <summary>
			/// Добавить узел в маршрут
			/// </summary>
			/// <param name="sw">стрелка</param>
			/// <param name="PK">требуемое положение</param>
			public void AddNextPlNode(UniObject sw, bool PK, bool fromOtherRC)
			{
                if (fromOtherRC)
                    _NextPlNodesOther.Add(new Node(sw, PK));
                else
                    _NextPlNodes.Add(new Node(sw, PK));
			}

			/// <summary>
			/// Добавить узел в маршрут
			/// </summary>
			/// <param name="sw">стрелка</param>
			/// <param name="PK">требуемое положение</param>
            public void AddNextMiNode(UniObject sw, bool PK, bool fromOtherRC)
			{
                if (fromOtherRC)
                    _NextMiNodesOther.Add(new Node(sw, PK));
                else
                    _NextMiNodes.Add(new Node(sw, PK));
			}

            public void AddNode(UniObject sw, bool PK, int direction, bool fromOtherRC)
            {
                if (direction > 0)
                    AddNextPlNode(sw, PK, fromOtherRC);
                else if (direction < 0)
                    AddNextMiNode(sw, PK, fromOtherRC);
                else
                    AddPrevNode(sw, PK, fromOtherRC);
            }

			/// <summary>
			/// Удовлетворяют ли положения предыдущих стрелок этому маршруту
			/// </summary>
			/// <returns></returns>
			public bool IsPrevTrue()
			{ 
                //foreach(Node node in _PrevNodes)
                //{
                //    if (!node.IsTrue())
                //        return false;
                //}
                //return true;

                bool res = true;
                if (_PrevNodes.Count > 0)   // т.к. в prev-маршрут не включается сама стрелка
                {
                    foreach (Node node in _PrevNodes)
                    {
                        res &= node.IsTrue();
                    }
                    if (!res && _PrevNodesOther.Count > 0)
                    {   // учитываем положения перекрестных стрелок по минусу
                        res = true;
                        foreach (Node n in _PrevNodesOther)
                        {
                            res &= n.IsTrue();
                        }
                    }
                    return res;
                }
                return false;
            }
			/// <summary>
			/// Удовлетворяют ли положения следующих по плюсу стрелок в той же РЦ этому маршруту
			/// </summary>
			/// <returns></returns>
			public bool IsNextPlTrue()
			{
                //foreach (Node node in _NextPlNodes)
                //{
                //    if (!node.IsTrue())
                //        return false;
                //}
                //return true;

                bool res = true;
                if (_NextPlNodes.Count > 1)
                {
                    foreach (Node node in _NextPlNodes)
                    {
                        res &= node.IsTrue();
                    }
                    return res;
                }
                return IsNextPlTrue_OtherRC();
            }
            /// <summary>
            /// Удовлетворяют ли положения следующих по плюсу стрелок этому маршруту
            /// </summary>
            /// <returns></returns>
            public bool IsNextPlTrue_OtherRC()
            {
                if (_NextPlNodesOther.Count > 1)
                {
                    bool res = true;
                    foreach (Node node in _NextPlNodesOther)
                    {
                        res &= node.IsTrue();
                    }
                    return res;
                }
                return true; //_NextPlNodes.Count < 2;// если по плюсу нет стрелок в данной РЦ (кроме текущей), и в следующих РЦ нет стрелок
            }
            /// <summary>
            /// Удовлетворяют ли положения следующих по минусу стрелок в той же РЦ этому маршруту
			/// </summary>
			/// <returns></returns>
			public bool IsNextMiTrue()
			{
                //foreach (Node node in _NextMiNodes)
                //{
                //    if (!node.IsTrue())
                //        return false;
                //}
                //return true;

                if (_NextMiNodes.Count > 1)
                {
                    bool res = true;
                    foreach (Node node in _NextMiNodes)
                    {
                        res &= node.IsTrue();
                    }
                    return res;
                }
                return IsNextMiTrue_OtherRC();
            }
            /// <summary>
            /// Удовлетворяют ли положения следующих по минусу стрелок в той же РЦ этому маршруту
            /// </summary>
            /// <returns></returns>
            public bool IsNextMiTrue_OtherRC()
            {
                if (_NextMiNodesOther.Count > 1)
                {
                    bool res = true;
                    foreach (Node node in _NextMiNodesOther)
                    {
                        res &= node.IsTrue();
                    }
                    return res;
                }
                return true;  //_NextMiNodes.Count < 2;// если по минусу нет стрелок в данной РЦ (кроме текущей), и в следующих РЦ нет стрелок
            }

            SwitchStateNewestAlg _alg;

            /// <summary>
			/// Удовлетворяют ли положения стрелок этому маршруту
			/// </summary>
			/// <returns></returns>
			public bool IsTrue(SwitchStateNewAlg alg)
			{
                _alg = alg as SwitchStateNewestAlg;

                bool isPrev = IsPrevTrue();
                bool isPlus = IsNextPlTrue();
                bool isMinus = IsNextMiTrue();

                if (IsEmpty())
                {   // если маршрут пуст - состояние стрелочной РЦ учитывается
                    return true;
                }
                else if (IsSidingByMinus(alg) && !isMinus
                    || IsSidingByPlus(alg) && !isPlus
                    || IsSidingByPrev(alg) && !isPrev)
                {   // если стрелка - на подъездной путь,  - учитываем РЦ только при соответствующем положении её, и стрелок её маршрута
                    return false;
                }
                else if (alg._ObjByPlus == null && isPlus && alg._ObjByPrev != null && alg._ObjByPrev.Type.SubGroupID == 1 && alg._ObjByPrev.ObjState.Current == 3
                     || alg._ObjByMinus == null && isMinus && alg._ObjByPrev != null && alg._ObjByPrev.Type.SubGroupID == 1 && alg._ObjByPrev.ObjState.Current == 3)
                {   // если стрелка - на подъездной путь - учитываем РЦ только при соответствующем её положении относительно спаренной (соседней)
                    //return false;
                    bool res = (_PrevNodes.Count > 0)
                        ? isPrev && (isPlus || isMinus)
                        : isPrev || isPlus || isMinus;
                    return res;
                }
                else
                {
                    bool res = (_PrevNodes.Count > 0)
                        ? isPrev && (isPlus || isMinus)
                        : isPrev || isPlus || isMinus;
                    return res;
                }
			}
            /// <summary>
            /// Признак пустого маршрута
            /// </summary>
            /// <returns>Признак пустого маршрута</returns>
            public bool IsEmpty()
            {
                return _NextMiNodes.Count == 0
                    && _NextMiNodesOther.Count == 0
                    && _NextPlNodes.Count == 0
                    && _NextPlNodesOther.Count == 0
                    && _PrevNodes.Count == 0
                    && _PrevNodesOther.Count == 0;
            }
            /// <summary>
            /// Признак стрелки на подъездной путь (контролируемые объекты только по минусу)
            /// </summary>
            /// <returns>Признак стрелки на подъездной путь</returns>
            public bool IsSidingByMinus(SwitchStateNewAlg alg)
            {
                return alg._ObjByMinus != null && alg._ObjByPlus == null && (alg._ObjByPrev == null || _PrevNodes.Count == 0 && _PrevNodesOther.Count == 0);
            }
            /// <summary>
            /// Признак стрелки на подъездной путь (контролируемые объекты только по плюсу)
            /// </summary>
            /// <returns>Признак стрелки на подъездной путь</returns>
            public bool IsSidingByPlus(SwitchStateNewAlg alg)
            {
                return alg._ObjByMinus == null && alg._ObjByPlus != null && (alg._ObjByPrev == null || _PrevNodes.Count == 0 && _PrevNodesOther.Count == 0);
            }
            /// <summary>
            /// Признак стрелки на подъездной путь (контролируемые объекты только по хвосту ;-)
            /// </summary>
            /// <returns>Признак стрелки на подъездной путь</returns>
            public bool IsSidingByPrev(SwitchStateNewAlg alg)
            {
                return alg._ObjByMinus == null && alg._ObjByPlus == null && alg._ObjByPrev != null;
            }
            /// <summary>
            /// Признак единственной стрелки в РЦ
            /// </summary>
            /// <returns>Признак стрелки на подъездной путь</returns>
            public bool IsOnlySwitch(SwitchStateNewAlg alg)
            {
                if (_IsOnlySwitchDetected == null)
                {
                    ObjData objData;
                    UniObject swCur = (objData = alg._curSw.GetObject("SwSection") as ObjData) != null ? objData.Object as UniObject : null;
                    UniObject swPl = alg._ObjByPlus == null ? null : (objData = alg._ObjByPlus.GetObject("SwSection") as ObjData) != null ? objData.Object as UniObject : alg._ObjByPlus.Type.SubGroupID == 1 ? alg._ObjByPlus : null;
                    UniObject swMi = alg._ObjByMinus == null ? null : (objData = alg._ObjByMinus.GetObject("SwSection") as ObjData) != null ? objData.Object as UniObject : alg._ObjByMinus.Type.SubGroupID == 1 ? alg._ObjByMinus : null;
                    UniObject swPrev = alg._ObjByPrev == null ? null : (objData = alg._ObjByPrev.GetObject("SwSection") as ObjData) != null ? objData.Object as UniObject : alg._ObjByPrev.Type.SubGroupID == 1 ? alg._ObjByPrev : null;

                    _IsOnlySwitchDetected = (swPl == null || swCur.Number != swPl.Number)
                                         && (swMi == null || swCur.Number != swMi.Number)
                                         && (swPrev == null || swCur.Number != swPrev.Number);

                    //if (_IsOnlySwitchDetected.Value) sws.Add(alg._curSw.Name);
                }
                return _IsOnlySwitchDetected.Value;
            }
            /// <summary>
            /// Признак единственной стрелки в РЦ
            /// </summary>
            protected bool? _IsOnlySwitchDetected = null;
        }

		protected Route _route = new Route();

		/// <summary>
		/// следующий объект по плюсу
		/// </summary>
		protected UniObject _NextPl;
		protected UniObject NextPl
		{
			get
			{
				if (_NextPl != null)
					return _NextPl;
				ObjData objData = (_controlObj.GetObject("NextPl") as ObjData);
				if (objData != null)
				{
					_NextPl = objData.Object as UniObject;
					return _NextPl;
				}
				return null;
			}
		}
		/// <summary>
		/// Следующий объект по минусу
		/// </summary>
		protected UniObject _NextMi;
		protected UniObject NextMi
		{
			get
			{
				if (_NextMi != null)
					return _NextMi;
				ObjData objData = (_controlObj.GetObject("NextMi") as ObjData);
				if (objData != null)
				{
					_NextMi = objData.Object as UniObject;
					return _NextMi;
				}
				return null;
			}
		}
		/// <summary>
		/// Предыдущий объект
		/// </summary>
		protected UniObject _PrevSec;
		protected UniObject PrevSec
		{
			get
			{
				if (_PrevSec != null)
					return _PrevSec;
				ObjData objData = (_controlObj.GetObject("PrevSec") as ObjData);
				if (objData != null)
				{
					_PrevSec = objData.Object as UniObject;
					return _PrevSec;
				}
				return null;
			}
		}

		/// <summary>
		/// собрать маршрут
		/// </summary>
		/// <param name="rc">РЦ</param>
		/// <param name="prevsw">предыдущая стрелка</param>
		/// <param name="sw">стрелка, к которой перешли</param>
		/// <param name="route">маршрут для исходной аналируемой стрелки</param>
		/// <param name="direction">направление от исходной анализируемой стрелки к sw: 0 двигались по предыдущей, -1 - двигались по минусу, 1 двигались по плюсу</param>
        static protected void CollectRouteForSwitch(UniObject rc, UniObject prevsw, UniObject sw, ref Route route, int direction, bool fromOtherRC, bool stopRecursion)
        {
            if (sw == null)
                return;
            try
            {
                UniObject prevSec = null, nextPl = null, nextMi = null;
                ObjData objData;

                // получаем предыдущую секцию
                objData = (sw.GetObject("PrevSec") as ObjData);
                if (objData != null)
                {
                    prevSec = objData.Object as UniObject;
                    if (prevSec.Type.SubGroupID == 1)
                    {   // в ЛПД ссылка на стрелку из другой секции подменяется ссылкой на секцию
                        prevSec = FindNextSwitch(prevSec, (sw.GetObject("SwSection") as ObjData).Object as UniObject);
                    }
                }
                // получаем следующую секцию по плюсу
                objData = (sw.GetObject("NextPl") as ObjData);
                if (objData != null)
                {
                    nextPl = objData.Object as UniObject;
                    if (nextPl.Type.SubGroupID == 1)
                    {   // в ЛПД ссылка на стрелку из другой секции подменяется ссылкой на секцию
                        nextPl = FindNextSwitch(nextPl, (sw.GetObject("SwSection") as ObjData).Object as UniObject);
                    }
                }
                // получаем следующую секцию по минусу
                objData = (sw.GetObject("NextMi") as ObjData);
                if (objData != null)
                {
                    nextMi = objData.Object as UniObject;
                    if (nextMi.Type.SubGroupID == 1)
                    {   // в ЛПД ссылка на стрелку из другой секции подменяется ссылкой на секцию
                        nextMi = FindNextSwitch(nextMi, (sw.GetObject("SwSection") as ObjData).Object as UniObject);
                    }
                }

                // если предыдущая есть и есть ссылка на SwSection и в тойже РЦ и для текущей РЦ prevsw по как предыдущая
                if (prevSec != null)
                {
                    if ((objData = prevSec.GetObject("SwSection") as ObjData) != null && objData.Object == rc
                        && prevSec == prevsw)
                    {
                        UniObject tmpPl = (objData = sw.GetObject("NextPl") as ObjData) != null ? objData.Object as UniObject : null;
                        UniObject tmpMi = (objData = sw.GetObject("NextMi") as ObjData) != null ? objData.Object as UniObject : null;
                        if (tmpPl != null && (objData = tmpPl.GetObject("SwSection") as ObjData) != null && objData.Object as UniObject == rc
                                          && (objData = tmpPl.GetObject("PrevSec") as ObjData) != null && objData.Object as UniObject != sw)
                        { }
                        else if (tmpMi != null && (objData = tmpMi.GetObject("SwSection") as ObjData) != null && objData.Object as UniObject == rc
                                               && (objData = tmpMi.GetObject("PrevSec") as ObjData) != null && objData.Object as UniObject != sw)
                        { }
                        else
                            // положение стрелки sw никак не влияет на маршрут prevsw -> останавливаемся
                            return;
                    }
                    else
                        stopRecursion = true;
                }
                // если следующая по плюсу есть и есть ссылка на SwSection и в тойже РЦ и для текущей РЦ prevsw по как по плюсу
                if (nextPl != null && nextPl.Number == prevsw.Number) // добавлена проверка равенства номеров объектов
                {
                    if ((objData = nextPl.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                    {
                        // добавляем узел - стрелка sw должна быть в плюсе, чтобы prevsw оказалась в маршруте
                        route.AddNode(sw, true, direction, fromOtherRC);
                        if (nextPl == prevsw)
                        {   // далее смотрим на предыдущую для sw стрелку
                            CollectRouteForSwitch(rc, sw, prevSec, ref route, direction, fromOtherRC, stopRecursion);
                        }
                        else if ((objData = prevsw.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                        {   // далее смотрим на предыдущую для sw стрелку
                            CollectRouteForSwitch(rc, sw, nextPl, ref route, direction, fromOtherRC, true);
                        }
                    }
                }

                // если предыдущая есть и есть ссылка на SwSection и в тойже РЦ и для текущей РЦ prevsw по как по минусу
                if (nextMi != null && nextMi.Number == prevsw.Number)
                {
                    if ((objData = nextMi.GetObject("SwSection") as ObjData) != null)
                    {
                        if (objData.Object == rc)
                        {
                            // добавляем узел - стрелка sw должна быть в минусе, чтобы prevsw оказалась в маршруте
                            route.AddNode(sw, false, direction, fromOtherRC);
                            if (nextMi == prevsw)
                            {   // далее смотрим на предыдущую для sw стрелку
                                CollectRouteForSwitch(rc, sw, prevSec, ref route, direction, fromOtherRC, stopRecursion);
                            }
                            //else if ((objData = prevsw.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                            //{   // далее смотрим на предыдущую для sw стрелку
                            //    CollectRouteForSwitch(rc, sw, nextMi, ref route, direction, fromOtherRC);
                            //}
                        }
                        else if (direction == 0 && !stopRecursion)
                        {
                            // добавляем узел - стрелка sw должна быть в минусе, чтобы prevsw оказалась в маршруте
                            route.AddNode(sw, false, direction, true);
                            if ((objData = prevsw.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                            {   // далее смотрим на предыдущую для sw стрелку
                                CollectRouteForSwitch(rc, sw, nextMi, ref route, direction, true, true);
                            }
                        }
                    }
                }


            }
            catch
            {
                return;
            }
        }

        protected UniObject _curSw = null;
        
        public UniObject _ObjByMinus = null;
        public UniObject _ObjByPlus = null;
        public UniObject _ObjByPrev = null; 

		public override bool Create(UniObject obj)
		{
            _curSw = obj;

			if (!base.Create(obj))
				return false;

			UniObject rc = null;
            UniObject nextSw = null;
			ObjData objData = _controlObj.GetObject("SwSection") as ObjData;
			if (objData != null && objData.Object is UniObject)
				rc = objData.Object as UniObject;

            if (NextMi != null)
            {
                _ObjByMinus = NextMi as UniObject;
                
                // если есть следующая по минусу в этой же РЦ
                if ((objData = NextMi.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                {
                    _route.AddNextMiNode(_controlObj, false, false);
                    CollectRouteForSwitch(rc, _controlObj, NextMi, ref _route, -1, false, false);
                }
                // если есть следующая по минусу НЕ в этой же РЦ (тираж)
                if ((objData = NextMi.GetObject("SwSection") as ObjData) != null && objData.Object != rc)
                {
                    _route.AddNextMiNode(_controlObj, false, true);
                    CollectRouteForSwitch(rc, _controlObj, NextMi, ref _route, -1, true, false);
                }
                // если есть следующая по минусу НЕ в этой же РЦ (разраб)
                if (NextMi.Type.SubGroupID == 1)
                {
                    nextSw = FindNextSwitch(NextMi, rc);
                    if (nextSw != null)
                    {
                        _route.AddNextMiNode(_controlObj, false, true);
                        CollectRouteForSwitch(rc, _controlObj, nextSw, ref _route, -1, true, false);
                    }
                }
            }


            if (NextPl != null)
            {
                _ObjByPlus = NextPl as UniObject;

                // если есть следующая по плюсу в этой же РЦ
                if ((objData = NextPl.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                {
                    _route.AddNextPlNode(_controlObj, true, false);
                    CollectRouteForSwitch(rc, _controlObj, NextPl, ref _route, 1, false, false);
                }
                // если есть следующая по плюсу НЕ в этой же РЦ
                if (NextPl != null && (objData = NextPl.GetObject("SwSection") as ObjData) != null && objData.Object != rc)
                {
                    _route.AddNextPlNode(_controlObj, true, true);
                    CollectRouteForSwitch(rc, _controlObj, NextPl, ref _route, 1, true, false);
                }
                // если есть следующая по минусу НЕ в этой же РЦ (разраб)
                if (NextPl.Type.SubGroupID == 1)
                 {
                    nextSw = FindNextSwitch(NextPl, rc);
                    if (nextSw != null)
                    {
                        _route.AddNextPlNode(_controlObj, true, true);
                        CollectRouteForSwitch(rc, _controlObj, nextSw, ref _route, 1, true, false);
                    }
                }
            }
            
            // если есть предыдущая в этой же РЦ
            if (PrevSec != null)
            {
                _ObjByPrev = PrevSec as UniObject;

                if ((objData = PrevSec.GetObject("SwSection") as ObjData) != null && objData.Object == rc)
                {
                    CollectRouteForSwitch(rc, _controlObj, PrevSec, ref _route, 0, false, false);
                }
            }

            AssignTo(obj);

			return true;

		}

        public static UniObject FindNextSwitch(UniObject nextRC, UniObject curRC)
        {
            UniObject res = null;
            ObjData objData = null;
            for (int i = 1; i <= 5; i++)
            {
                if ((objData = nextRC.GetObject("Switch" + i.ToString()) as ObjData) != null)
                {
                    UniObject sw = objData.Object as UniObject;
                    if (sw != null)
                    {
                        if ((objData = sw.GetObject("NextPl") as ObjData) != null && objData.Object == curRC
                         || (objData = sw.GetObject("NextMi") as ObjData) != null && objData.Object == curRC
                         || (objData = sw.GetObject("PrevSec") as ObjData) != null && objData.Object == curRC)
                        {
                            res = sw;
                            break;
                        }
                    }
                }
            }
            return res;
        }

        protected virtual void AssignTo(UniObject obj)
        {
            // Получаем и подписываемся на рассылку изменения для _PKSig
            if (_PKSig == null)
                AssignToSigByObj("Main", "PKSig", out _PKSig);

            // Получаем и подписываемся на рассылку изменения для _MKSig
            if (_MKSig == null)
                AssignToSigByObj("Main", "MKSig", out _MKSig);

            // на случай стрелки МПЦ
            if (_PKSig == null && _MKSig == null)
            {
                ISigDiscreteLogic PKMKSig;
                ISigDiscreteLogic LoseCtrlSig;
                // Получаем и подписываемся на рассылку изменения для _PKSig
                AssignToSig("PKMKSig", out PKMKSig);
                // Получаем и подписываемся на рассылку изменения для _MKSig
                AssignToSig("LoseCtrlSig", out LoseCtrlSig);

                // Получаем и подписываемся на рассылку изменения для _PKSig
                if (PKMKSig == null)
                    AssignToSigByObj("Main", "PKMKSig", out PKMKSig);

                // Получаем и подписываемся на рассылку изменения для _MKSig
                if (LoseCtrlSig == null)
                    AssignToSigByObj("Main", "LoseCtrlSig", out LoseCtrlSig);

                if (PKMKSig != null && LoseCtrlSig != null)
                {
                    _PKSig = new PKDiscreteLogic(PKMKSig, LoseCtrlSig);
                    _MKSig = new MKDiscreteLogic(PKMKSig, LoseCtrlSig);
                }
            }
        }

		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				// если стрелка - единственная в стрелочной РЦ, либо по маршруту
                if (_route.IsOnlySwitch(this) || _route.IsTrue(this))
				{
					//обновляем состяние стрелочной секции
					_SwSection.UpdateObjectState();
					// формируем состояние стрелки с учетом РЦ
					state = MakeSwitchState(_SwSection.ObjState.Current, _PKSig != null && _PKSig.On, _MKSig != null && _MKSig.On);
				}
				else
				{
					// формируем состояние стрелки без учета РЦ
					state = MakeSwitchState((int)Uni2States.PathSectionStates.Free, _PKSig.On, _MKSig.On);
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("SwitchState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("SwitchState error update: " + ex.ToString());
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSwitchState(state);

			SetState(state);

			ResetModify();
		}
	}

    public class SwitchStateNewestAlg : SwitchStateNewAlg
    {
        protected BaseControlObj _tObj;

        public SwitchStateNewestAlg(BaseControlObj tObj)
        {
            _tObj = tObj;
        }
        
        protected override void AssignTo(UniObject obj) { }
        public override void UpdateObjectState() { }

        public bool ToUseRCState()
        {
            return _route.IsOnlySwitch(this) || _route.IsTrue(this);
        }
    }

	#endregion

	#region Маневровый светофор
	/// <summary>
	/// Состояние маневрового светофора без сигнала сигнального реле
	/// </summary>
	public class ObjStateMSignal_SNone : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_IntegritySig.On)
					state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
				else if (_IntegritySig.Off)
					state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_SNone " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние маневрового светофора с контролем сигнального реле
	/// </summary>
	public class ObjStateMSignal_SExist : AbsenceObjState
	{
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _MSig))
				if (!AssignToSig("MSig", out _MSig))
					return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_MSig.Off)
                    state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
				else if (_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
				else if (_MSig.Blink)
                    state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние маневрового светофора с контролем сигнального реле и двумя белыми
	/// </summary>
    public class ObjStateMSignal_S_2W : ObjStateMSignal_SExist
	{
        /// <summary>
        /// Контроль сигнального реле (открытие на маневровое показание, ускоренные маневры)
        /// </summary>
        protected ISigDiscreteLogic _2WSig;
        /// <summary>
        /// Контроль сигнала наличия информации МПЦ
        /// </summary>
        protected ISigDiscreteLogic _InfoSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("2WSig", out _2WSig))
				return false;
            
            AssignToSig("InfoSig", out _InfoSig);
            
            return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_InfoSig != null && _InfoSig.Off)
                    state = (int)Uni2States.MSignalStates.Fail;
                else if (_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;      // Маневровое показание (белый огонь)
                else if (_2WSig.On)
                    state = (int)Uni2States.MSignalStates.QuickOpen; // Ускоренные маневры
                else if (_MSig.Off && _2WSig.Off)
                    state = (int)Uni2States.MSignalStates.Close;     // Запрет маневров (синий огонь)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние маневрового светофора с контролем закрытия
    /// </summary>
    public class ObjStateMSignal_Cl : AbsenceObjState
    {
        /// <summary>
        /// Контроль запрещающего огня светофора
        /// </summary>
        protected ISigDiscreteLogic _ClSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для ClSig
            if (!AssignToSig("ClSig", out _ClSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_ClSig.On)
                    state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
                else if (_ClSig.Off)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние маневрового светофора с контролем огневого и сигнального реле
	/// </summary>
	public class ObjStateMSignal_O_S : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _MSig))
				if (!AssignToSig("MSig", out _MSig)) 
					return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_MSig.Off && _IntegritySig.On)
					state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
				else if (_MSig.On )
					state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
				else if (_IntegritySig.Off)
					state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}
    
    public class ObjStateMSignal_Off_S : AbsenceObjState
    {
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityOffSig;
        /// <summary>
        /// Контроль сигнального реле (открытие на маневровое показание)
        /// </summary>
        protected ISigDiscreteLogic _MSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("IntegrityOffSig", out _IntegrityOffSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _MSig))
                if (!AssignToSig("MSig", out _MSig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegrityOffSig.Off)
                {
                    if (_MSig.On)
                        state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                    else if (_MSig.Off)
                        state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
                }
                else
                    state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    public class ObjStateMSignal_Off_S_Cl : ObjStateMSignal_Off_S
    {
        /// <summary>
        /// Контроль запрещающего огня светофора
        /// </summary>
        protected ISigDiscreteLogic _ClSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для ClSig
            if (!AssignToSig("ClSig", out _ClSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if(_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                else if(_IntegrityOffSig.On)
                    state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
                else if (_ClSig.On)
                    state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
                else if (_ClSig.Off)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние маневрового светофора с контролем сигнального реле и закрытия
    /// </summary>
    public class ObjStateMSignal_S_ClRed : AbsenceObjState
    {
        /// <summary>
        /// Контроль сигнального реле (открытие на маневровое показание)
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль запрещающего (красного) огня светофора
        /// </summary>
        protected ISigDiscreteLogic _ClRedSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("ClRedSig", out _ClRedSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _MSig))
                if (!AssignToSig("MSig", out _MSig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                else if (_MSig.Off && _ClRedSig.On)
                    state = (int)Uni2States.MSignalStates.CloseRed;// Запрет маневров (красный огонь)
                else if (_MSig.Off && _ClRedSig.Off)
                    state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние маневрового светофора с контролем огневого и сигнального реле
    /// </summary>
    public class ObjStateMSignal_O_S_Z : ObjStateMSignal_O_S
    {
        /// <summary>
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _CloseSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("CloseSig", out _CloseSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_MSig.Off && _IntegritySig.On || _MSig.Off && _IntegritySig.Off && _CloseSig.On)
                    state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
                else if (_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                else if (_IntegritySig.Off && _CloseSig.Off)
                    state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	public class ObjStateMSignal_O_S_IntegrityR : ObjStateMSignal_O_S
	{
		/// <summary>
		/// Контроль целостности нити лампы красного огня
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня
		/// </summary>
		protected ISigDiscreteLogic _IntegrityARSig;

		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("IntegrityRSig", out _IntegritySig))
				return false;

			if (!AssignToSig("IntegrityARSig", out _IntegritySig))
				return false;

			return true;
		}

		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				bool Integrity = _IntegritySig.On && _IntegrityRSig.On && _IntegrityARSig.On;
				if (Integrity)
				{
					if (_MSig.On)
						state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
					else if (_MSig.Off)
						state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
				}
				else
					state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	public class ObjStateMSignal_O_S_FailOnIntegrity : ObjStateMSignal_O_S
	{
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_MSig.Off && _IntegritySig.Off)
					state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
				else if (_MSig.On && _IntegritySig.Off)
					state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
				else if (_IntegritySig.On)
					state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние маневрового светофора с контролем огневого и сигнального реле
	/// </summary>
	public class ObjStateMSignal_O_S_2W : ObjStateMSignal_O_S
	{
		/// <summary>
		/// Контроль огневого реле
		/// </summary>
		protected ISigDiscreteLogic _Integrity2WSig;
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _2WSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("Integrity2WSig", out _Integrity2WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("2WSig", out _2WSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_2WSig.On)
				{
					state = (int)Uni2States.MSignalStates.QuickOpen;
				}
				else if (_2WSig.Off && _MSig.Off && _IntegritySig.On && _Integrity2WSig.On)
					state = (int)Uni2States.MSignalStates.Close;// Запрет маневров (синий огонь)
				else if (_MSig.On && _Integrity2WSig.On)
					state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
				else if (_MSig.Off && _IntegritySig.Off || _2WSig.Off && _Integrity2WSig.Off)
					state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние маневрового светофора с контролем огневого и сигнального реле
	/// </summary>
	public class ObjStateMSignal_MPC_S_I : AbsenceObjState
	{
		/// <summary>
		/// Контроль нет 0 есть 1 информация
		/// </summary>
		protected ISigDiscreteLogic _InfoSig;
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("InfoSig", out _InfoSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _MSig))
                if (!AssignToSig("MSig", out _MSig))
                    _MSig = null;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_InfoSig.Off)
                    state = (int)Uni2States.MSignalStates.Fail;
				else if (_MSig.On)
					state = (int)Uni2States.MSignalStates.Open;
				else
					state = (int) Uni2States.MSignalStates.Close;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние маневрового светофора с сигналами сигнального и красного огневого реле
    /// </summary>
    public class ObjStateMSignal_S_M_R : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия на маневровое показание
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для MSig
            if (!AssignToSig("MSig", out _MSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegritySig
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                if (!AssignToSig("IntegrityOffSig", out _IntegritySig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_MSig.Off && _IntegritySig.On)
                    state = (int)Uni2States.MSignalStates.CloseRed;// Запрет маневров (красный огонь)
                else if (_MSig.On)
                    state = (int)Uni2States.MSignalStates.Open;// Маневровое показание (белый огонь)
                else if (_IntegritySig.Off)
                    state = (int)Uni2States.MSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    #endregion

	#region Светофор станционный поездной (входной, выходной, маршрутный)
	/// <summary>
	/// Состояние светофора-повторителя с О
	/// </summary>
	public class ObjStateInSignal_O : AbsenceObjState
	{
		/// <summary>
		/// Контроль огневого реле
		/// </summary>
		protected ISigDiscreteLogic _OSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("IntegritySig", out _OSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_OSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;
                else
                    state = (int)Uni2States.StateSignalStates.NoLight;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal с сигналами  SSig
	/// </summary>
	public class ObjStateInSignal_S : AbsenceObjState
	{
		/// <summary>
		/// Контроль сигнального реле
		/// </summary>
		protected ISigDiscreteLogic _SSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
                if (!AssignToSig("GSig", out _SSig))
                    return false;
		
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)
					state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else 
					state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора CInSignal с сигналами  SSig
    /// </summary>
    public class ObjStateInSignal_S_W : ObjStateInSignal_S
    {
        /// <summary>
        /// Контроль пригласительного огня
        /// </summary>
        protected ISigDiscreteLogic _WSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            // Получаем и подписываемся на рассылку изменения для WSig
            if (!AssignToSig("WSig", out _WSig))
                return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;          //Один зеленый огонь (открыт)
                else if (_SSig.Off && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red; //Белый миг и красный огонь (пригласительный)
                else if (_SSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;            //Один красный огонь (закрыт)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние светофора с контролем огневого и сигнального реле
	/// </summary>
	public class ObjStateSignal_O_S : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле (открытие на маневровое показание)
		/// </summary>
		protected ISigDiscreteLogic _SSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			if (!AssignToSig("SSig", out _SSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.Off && _IntegritySig.On)
					state = (int)Uni2States.StateSignalStates.Red;
				else if (_SSig.On)
					state = (int)Uni2States.StateSignalStates.Green;
				else if (_SSig.Off && _IntegritySig.Off)
					state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора с контролем огневого и сигнального реле
    /// </summary>
    public class ObjStateSignal_O_S_Y : ObjStateSignal_O_S
    {
        /// <summary>
        /// Контроль открытия на жёлтый
        /// </summary>
        protected ISigDiscreteLogic _YSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("YSig", out _YSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.Off && _YSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;
                else if (_SSig.On && _YSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;
                else if (_SSig.Off && _YSig.On)
                    state = (int)Uni2States.StateSignalStates.Yellow;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора с контролем сигнального реле и выключения огневого реле
    /// </summary>
    public class ObjStateSignal_S_C : AbsenceObjState
    {
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _CancelSig;
        /// <summary>
        /// Контроль сигнального реле (открытие на маневровое показание)
        /// </summary>
        protected ISigDiscreteLogic _SSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("CancelSig", out _CancelSig))
                return false;
            if (!AssignToSig("SSig", out _SSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && _CancelSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;
                else if (_SSig.Off && _CancelSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;
                else if (_CancelSig.On)
                    state = (int)Uni2States.StateSignalStates.NoLight;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }



	/// <summary>
	/// Состояние светофора с контролем огневого и сигнального реле
	/// </summary>
	public class ObjStateSignal_MPC : AbsenceObjState
	{
        /// <summary>
        /// Контроль открытия светофора на белый и желтый огни
        /// </summary>
        protected ISigDiscreteLogic _WYSig;
        /// <summary>
        /// Контроль открытия светофора на белый и желтый мигающий огни
        /// </summary>
        protected ISigDiscreteLogic _WYBSig;
        /// <summary>
        /// Контроль открытия светофора на белый и зеленый огни
        /// </summary>
        protected ISigDiscreteLogic _WGSig;
        /// <summary>
        /// Контроль открытия светофора на белый огонь (пригласительный)
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль открытия светофора на два желтых огня
        /// </summary>
        protected ISigDiscreteLogic _2YSig;
        /// <summary>
        /// Контроль открытия светофора на желтый огонь
        /// </summary>
        protected ISigDiscreteLogic _YSig;
        /// <summary>
        /// Контроль открытия светофора на желтый и желтый мигающий огни
        /// </summary>
        protected ISigDiscreteLogic _YYBSig;
        /// <summary>
        /// Контроль открытия светофора на желтый мигающий огонь
        /// </summary>
        protected ISigDiscreteLogic _YBSig;
        /// <summary>
        /// Контроль открытия светофора на желтый и зеленый огни
        /// </summary>
        protected ISigDiscreteLogic _YGSig;
        /// <summary>
        /// Контроль открытия светофора на зеленый огонь
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль закрытия светофора
        /// </summary>
        protected ISigDiscreteLogic _GrSig;
        /// <summary>
        /// Контроль закрытия светофора
        /// </summary>
        protected ISigDiscreteLogic _RSig;
        /// <summary>
        /// Контроль наличия информации
        /// </summary>
        protected ISigDiscreteLogic _InfoSig;
        /// <summary>
        /// Контроль открытия светофора на белый мигающий и красный огни (пригласительный)
        /// </summary>
        protected ISigDiscreteLogic _WBRSig;
		
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			AssignToSig("WYSig", out _WYSig);
			AssignToSig("WYBSig", out _WYBSig);
			AssignToSig("WGSig", out _WGSig);
			AssignToSig("WSig", out _WSig);
			AssignToSig("2YSig", out _2YSig);
			AssignToSig("YSig", out _YSig);
			AssignToSig("YYBSig", out _YYBSig);
			AssignToSig("YBSig", out _YBSig);
			AssignToSig("YGSig", out _YGSig);
			AssignToSig("GSig", out _GSig);
            AssignToSig("GrSig", out _GrSig);
            AssignToSig("RSig", out _RSig);
			AssignToSig("InfoSig", out _InfoSig);
			AssignToSig("WBRSig", out _WBRSig);

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_InfoSig != null && _InfoSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;
                else if (_RSig != null && _RSig.On)
                    state = (int)Uni2States.StateSignalStates.Red;
                else if (_WBRSig != null && _WBRSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;
                else if (_WSig != null && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.White;
                else if (_WYBSig != null && _WYBSig.On)
                    state = (int)Uni2States.StateSignalStates.YellowBlink_White;
                else if (_WYSig != null && _WYSig.On)
                    state = (int)Uni2States.StateSignalStates.Yellow_White;
                else if (_WGSig != null && _WGSig.On)
                    state = (int)Uni2States.StateSignalStates.Green_White;
                else if (_YYBSig != null && _YYBSig.On)
                    state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;
                else if (_YBSig != null && _YBSig.On)
                    state = (int)Uni2States.StateSignalStates.YellowBlink;
                else if (_2YSig != null && _2YSig.On)
                    state = (int)Uni2States.StateSignalStates._2Yellow;
                else if (_YSig != null && _YSig.On)
                    state = (int)Uni2States.StateSignalStates.Yellow;
                else if (_YGSig != null && _YGSig.On)
                    state = (int)Uni2States.StateSignalStates.Yellow;
                else if (_GSig != null && _GSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;
                else if (_GrSig != null && _GrSig.On)
                    state = (int)Uni2States.StateSignalStates.Red;
                else if (_InfoSig == null)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;  // Случай, когда все биты в 0, а InfoSig отсутствует
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateMSignal_O_S " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_S_O : AbsenceObjState
    {
        /// <summary>
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Контроль сигнального реле
        /// </summary>
        protected ISigDiscreteLogic _SSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityRSig", out _IntegritySig))
            {
                if (!AssignToSig("IntegritySig", out _IntegritySig))
                    return false;
            }
            if (!AssignToSig("SSig", out _SSig))
                if (!AssignToSig("GSig", out _SSig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else
                {
                    if (_IntegritySig.On)
                        state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_MZF : ObjStateInSignal_S_O
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else
                {
                    if (_IntegritySig.Off)
                        state = (int)Uni2States.StateSignalStates.NoLight;//Погашен
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
	/// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
	/// </summary>
    public class ObjStateInSignal_S_O_W : ObjStateInSignal_S_O
	{
		/// <summary>
		/// Контроль пригласительного сигнала
		/// </summary>
		protected ISigDiscreteLogic _WSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("WSig", out _WSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On && _WSig.Off)
					state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else if (_SSig.Off && _WSig.Off && _IntegritySig.On)
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
				else if (_SSig.Off && _WSig.On )
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _IntegritySig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_S_Y_W_R : AbsenceObjState
    {
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _YSig;
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _RSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("SSig", out _SSig))
                return false;
            if (!AssignToSig("YSig", out _YSig))
                return false;
            if (!AssignToSig("WSig", out _WSig))
                return false;
            if (!AssignToSig("RSig", out _RSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {                
                if (_SSig.Off && _YSig.Off && _WSig.Off || _RSig.On)
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь
                else if (_SSig.On)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь
                else if (_YSig.On)
                    state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                else if (_WSig.On)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый огонь
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_S_O_W_IntG : ObjStateInSignal_S_O_W
    {
        /// <summary>
        /// Контроль огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityGSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                   if(_IntegrityGSig.On)
                       state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                   else
                       state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else
                {
                    if (_WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                    }
                    else
                    {
                        if(_IntegritySig.On)
                            state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                        else
                            state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_S_O_W_IntGYW : ObjStateInSignal_S_O_W
    {
        /// <summary>
        /// Контроль целостности Ж, З, Б
        /// </summary>
        protected ISigDiscreteLogic _IntegrityGYWSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityGYWSig", out _IntegrityGYWSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    if (_IntegrityGYWSig.On)
                        state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь (открыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else if (_WSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                }
                else if (_IntegritySig.On && _IntegrityGYWSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                }
                else if (_IntegritySig.Off && _IntegrityGYWSig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig, IntegrityOffSig
    /// </summary>
    public class ObjStateInSignal_S_Off_W_IntR : ObjStateInSignal_S_O_W
    {
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityOffSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityOffSig", out _IntegrityOffSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_IntegrityOffSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.NoLight;// Погашен
                }
                else if (_SSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else if (_WSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig, IntegrityOffSig
    /// </summary>
    public class ObjStateInSignal_S_Off_IntR : ObjStateInSignal_S_O
    {
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityOffSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityOffSig", out _IntegrityOffSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_IntegrityOffSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.NoLight;// Погашен
                }
                else if (_SSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig, IntegrityOffSig
    /// </summary>
    public class ObjStateInSignal_S_Off_IntR_IntYG : ObjStateInSignal_S_Off_IntR
    {
        /// <summary>
        /// Контроль целостности нитей желтого и зеленого
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_IntegrityOffSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.NoLight;// Погашен
                }
                else if (_SSig.On)
                {
                    if (_IntegrityYGSig.Off)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateInSignal_S_O_W_IntG_IntYG_IntSR : ObjStateInSignal_S_O_W_IntG
    {
        /// <summary>
        /// Контроль целостности нитей желтого и зеленого
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;
        /// <summary>
        /// Контроль вспомогательного сигнального реле
        /// </summary>
        protected ISigDiscreteLogic _IntegritySRSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;
            if (!AssignToSig("IntegritySRSig", out _IntegritySRSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    if (_IntegrityGSig.On && _IntegrityYGSig.On)
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else
                {
                    if (_WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                    }
                    else
                    {
                        if (_IntegritySig.On && _IntegritySRSig.On)
                            state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
                        else
                            state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }


    /// <summary>
	/// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
	/// </summary>
	public class ObjStateInSignal_S_W_Cln_R_GY_A : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
        /// Контроль целостности нитей ламп желтого и зеленого огней светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
        /// Контроль целостности нити лампы красного огня светофора
		/// </summary>
		//protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль сигнального реле
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль пригласительного сигнала
		/// </summary>
		protected ISigDiscreteLogic _WSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

            if (!AssignToSig("IntegritySig", out _IntegritySig))
                if (!AssignToSig("IntegrityRSig", out _IntegritySig))
                    return false;
            
            if (!AssignToSig("SSig", out _SSig))
				return false;
			if (!AssignToSig("WSig", out _WSig))
				return false;

			AssignToSig("IntegrityYGSig", out _IntegrityYGSig);


			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On && _WSig.Off )
					state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else if (_SSig.Off && _WSig.Off && _IntegritySig.On)
					state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
				else if (_SSig.Off && _WSig.On && _IntegritySig.On)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && (_IntegritySig.Off))
					state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;

				TraceSignalState(state);
				SetState(state);

				ResetModify();
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal с сигналами OSig, SSig и без сигнала WhiteSig
	/// </summary>
	public class ObjStateInSignal_WhiteNone_OExist_SExist : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле
		/// </summary>
		protected ISigDiscreteLogic _SSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			if (!AssignToSig("SSig", out _SSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.Off && _IntegritySig.Off)
					state = (int)Uni2States.StateSignalStates.Fail;
				else if (_SSig.Off)
					state = (int)Uni2States.StateSignalStates.Red;
				else
					state = (int)Uni2States.StateSignalStates.Green;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	public class ObjStateInSignal_WhiteNone_OExist_SExist_FailOnIntegrity : ObjStateInSignal_WhiteNone_OExist_SExist
	{
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.Off && (_IntegritySig.On || _IntegritySig.Blink))
					state = (int)Uni2States.StateSignalStates.Fail;
				else if (_SSig.Off)
					state = (int)Uni2States.StateSignalStates.Red;
				else
					state = (int)Uni2States.StateSignalStates.Green;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора CInSignal с сигналами открытия и открытия на маневровое показание
    /// </summary>
    public class ObjStateInSignal_S_M : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия на маневровое показание
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль сигнального реле
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль установки маршрута
        /// </summary>
        protected ISigDiscreteLogic _RouteSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("MSig", out _MSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            AssignToSig("RouteSig", out _RouteSig);

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && (_RouteSig == null || _RouteSig.On))
                    state = (int)Uni2States.StateSignalStates.Green;
                else if (_MSig.On && (_RouteSig == null || _RouteSig.On))
                    state = (int)Uni2States.StateSignalStates.White;
                else if (_SSig.On && _MSig.On) 
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                else
                    state = (int)Uni2States.StateSignalStates.Red;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние светофора CInSignal без сигнала OSig
	/// </summary>
	public class ObjStateInSignal_S_G_W_M_CnlR2Y_G_GYW : AbsenceObjState
	{
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала светофора
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль маневрового огня
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль открытия на зеленый огонь светофора
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль целостности нитей ламп зеленого, желтого и белого огней светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityGYWSig;
        /// <summary>
        /// Контроль целостности нити лампы зеленого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityGSig;
        /// <summary>
        /// Контроль целостности нити лампы красного огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			if (!AssignToSig("IntegrityGYWSig", out _IntegrityGYWSig))
				return false;
			if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
				return false;
			if (!AssignToSig("IntegrityR2YSig", out _IntegrityRSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if ( _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;// Два желтых огня
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityGYWSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)

			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_ONone " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние светофора CInSignal без сигнала OSig
	/// </summary>
	public class ObjStateInSignal_S_G_W_M_CnlR_M_YG : AbsenceObjState
	{
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала светофора
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль маневрового огня
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль открытия на зеленый огонь светофора
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;
        /// <summary>
        /// Контроль целостности нити лампы маневрового огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityMSig;
        /// <summary>
        /// Контроль целостности нити лампы красного огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для целостности красного
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для целостности разрешающих
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Yellow2Sig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;// Два желтых огня
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)

			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_ONone " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние светофора CInSignal без сигнала OSig
    /// </summary>
    public class ObjStateInSignal_S_W_M_Cln_YG : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала светофора
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль маневрового огня
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;
        /// <summary>
        /// Контроль целостности нити лампы огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для WhiteSig
            if (!AssignToSig("WSig", out _WSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для WhiteSig
            if (!AssignToSig("MSig", out _MSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для целостности Yellow2Sig
            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для целостности
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    if (_IntegrityYGSig.Off)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else if (_SSig.Off && _WSig.Off && _MSig.Off)
                {
                    if (_IntegritySig.Off)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
                else if (_SSig.Off && _WSig.On && _MSig.Off)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                else if (_SSig.Off && _WSig.Off && _MSig.On)
                    state = (int)Uni2States.StateSignalStates.White;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)

            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_ONone " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Входной основной: откр зеленый, желтый мигающий, пригласительный, + целостность жз, кр, б
    /// </summary>
    public class ObjStateInSignal_S_G_YB_W_ClnR_W : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль открытия на зеленый огонь светофора
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль открытия светофора на желтый мигающий
        /// </summary>
        protected ISigDiscreteLogic _YBlSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала светофора
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль целостности нити лампы красного огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Контроль целостности нити лампы белого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityWSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("SSig", out _SSig))
                return false;
            if (!AssignToSig("GSig", out _GSig))
                return false;
            if (!AssignToSig("YBlSig", out _YBlSig))
                return false;
            if (!AssignToSig("WSig", out _WSig))
                return false;
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                if (!AssignToSig("IntegrityRSig", out _IntegritySig))
                    return false;
            if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.On)
                {
                    if (_SSig.On)// открыт
                    {
                        if (_GSig.On && _YBlSig.Off && _IntegrityWSig.Off)
                            state = (int)Uni2States.StateSignalStates.Green;
                        else if (_GSig.Off && _YBlSig.Off && _IntegrityWSig.Off)
                            state = (int)Uni2States.StateSignalStates.Yellow;
                        else if (_GSig.Off && _YBlSig.Off && _IntegrityWSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;
                        else if (_GSig.Off && _YBlSig.On && _IntegrityWSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;
                        else
                            state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                    }
                    else if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;
                    else
                        state = (int)Uni2States.StateSignalStates.Red;
                }
                else
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_G_YB_Inv_ClnR_GY_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
	/// Входной основной: откр зеленый, желтый мигающий, пригласительный, + целостность жз, кр, б
	/// </summary>
    public class ObjStateInSignal_S_G_YB_W_M_ClnR_W : ObjStateInSignal_S_G_YB_W_ClnR_W
    {
        /// <summary>
        /// Контроль включения маневрвого сигнала светофора
        /// </summary>
        protected ISigDiscreteLogic _MSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("MSig", out _MSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.On)
                {
                    if (_SSig.On)// открыт
                    {
                        if (_GSig.On && _YBlSig.Off && _IntegrityWSig.Off)
                            state = (int)Uni2States.StateSignalStates.Green;
                        else if (_GSig.Off && _YBlSig.Off && _IntegrityWSig.Off)
                            state = (int)Uni2States.StateSignalStates.Yellow;
                        else if (_GSig.Off && _YBlSig.Off && _IntegrityWSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;
                        else if (_GSig.Off && _YBlSig.On && _IntegrityWSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;
                        else
                            state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                    }
                    else if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;
                    else if (_MSig.On)
                        state = (int)Uni2States.StateSignalStates.White;
                    else
                        state = (int)Uni2States.StateSignalStates.Red;
                }
                else
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_G_YB_Inv_ClnR_GY_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
	/// Входной основной: откр зеленый, желтый мигающий, пригласительный, + целостность жз, кр, б
	/// </summary>
    public class ObjStateInSignal_S_G_YB_W_ClnR_GY_W : ObjStateInSignal_S_G_YB_W_ClnR_W
	{
        /// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if( _SSig.On )// открыт
				{
					if (_GSig.On)
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_YBlSig.On)
                        state = (int)Uni2States.StateSignalStates.YellowBlink;// Один желтый мигающий огонь
					else
                        state = (int)Uni2States.StateSignalStates.Green;//Один зеленый огонь (открыт)
				}
				else if(_SSig.Off && _WSig.Off)// закрыт
				{
					if (_IntegritySig.Off)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
					else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if( _WSig.On && _SSig.Off )// пригласительный
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red; //Один белый мигающий и красный огни (пригла-сительный)
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_YB_Inv_ClnR_GY_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр желтый миг, пригласительный + целостность кр, б, ж1, ж2
	/// </summary>
	public class ObjStateInSignal_S_YB_W_ClnR_Y1_Y2_W : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия светофора на желтый мигающий
		/// </summary>
		protected ISigDiscreteLogic _YBlSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы первого желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY1Sig;
		/// <summary>
		/// Контроль целостности нити лампы второго желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY2Sig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для YBlSig
			if (!AssignToSig("YBlSig", out _YBlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityY1Sig
			if (!AssignToSig("IntegrityY1Sig", out _IntegrityY1Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityY2Sig
			if (!AssignToSig("IntegrityY2Sig", out _IntegrityY2Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_YBlSig.On)
						state = (int)Uni2States.StateSignalStates.YellowBlink;// Один желтый мигающий огонь
					else
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				}
				else if (_SSig.Off && _WSig.Off)// закрыт
				{
					if (_IntegrityRSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь(закрыт);
				}
				else if (_SSig.Off && _WSig.On)// пригласительный
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_YB_Wh_ClnR_Y1_Y2_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityWSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off)
				{
					if (_IntegrityRSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
				else
					state = 1;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
    /// </summary>
    public class ObjStateInSignal_S_Wh_YB_Y2_ClnR_Y : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала на светофоре
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль мигания желтого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _YBSig;
        /// <summary>
        /// Контроль горения второго желтого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _2YSig;
        /// <summary>
        /// Контроль целостности нити лампы красного огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;
        /// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для WSig
            if (!AssignToSig("WSig", out _WSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для YSig
            if (!AssignToSig("YBSig", out _YBSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для Y2Sig
            if (!AssignToSig("2YSig", out _2YSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegrityRSig
            if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegrityYGSig
            if (!AssignToSig("IntegrityYSig", out _IntegrityYSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegrityRSig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                }
                else
                {
                    if (_SSig.On)
                    {
                        if(_IntegrityYSig.Off)
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                        else if (_YBSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
                        else if (_2YSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
                        else
                            state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                    }
                    else if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }


	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_YG_M_O : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int) Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int) Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int) Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else if (_2YWSig.Off && _WSig.Off)
						state = (int) Uni2States.StateSignalStates.Yellow;
					else
						state = (int) Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off || _IntegritySig.Off || _IntegrityMSig.Off)
						state = (int) Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int) Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int) Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.Off)
					state = (int) Uni2States.StateSignalStates.White;
				else
					state = (int) Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
    /// </summary>
    public class ObjStateInSignal_S_Wh_Invite_IntR_IntYG : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения белого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала на светофоре
        /// </summary>
        protected ISigDiscreteLogic _InviteSig;
        /// <summary>
        /// Контроль целостности нити лампы красного огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;
        /// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для WSig
            if (!AssignToSig("WSig", out _WSig))
                _WSig = null;
            // Получаем и подписываемся на рассылку изменения для WSig
            if (!AssignToSig("InviteSig", out _InviteSig))
                _InviteSig = null;
            // Получаем и подписываемся на рассылку изменения для IntegrityRSig
            if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegrityYGSig
            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)// открыт
                {
                    if (_IntegrityYGSig.On)
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                }
                else if (_SSig.Off)
                {
                    if (_InviteSig != null && _InviteSig.On && _IntegrityRSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Белый миг и красный огонь (пригласителный)
                    }
                    else if (_WSig != null && _WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.White;// Белый огонь
                    }
                    else
                    {
                        if (_IntegrityRSig.On)
                            state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                        else
                            state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                    }
                }
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_Wh_Y2W_Bl_M_ClnR_AR_YG_M_O : ObjStateInSignal_S_Wh_Y2W_Bl_M_ClnR_YG_M_O
	{
		
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityARSig;
		

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
            AssignToSig("IntegrityARSig", out _IntegrityARSig);
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_2YWSig.Off)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else if (_2YWSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
                    if (_IntegrityRSig != null && _IntegrityRSig.Off || _IntegritySig.Off || (_IntegrityARSig != null && _IntegrityARSig.Off))
                        state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.Off)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_AR_YG_M : ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_YG_M
	{

		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityARSig;


		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
            AssignToSig("IntegrityARSig", out _IntegrityARSig);

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else if (_2YWSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
                    if (_IntegrityRSig.Off || _IntegrityARSig != null && _IntegrityARSig.Off)
                        state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.Off)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_AR_YG_M_O : ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_YG_M_O
	{

		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityARSig;


		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityARSig", out _IntegrityARSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else if (_2YWSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off || _IntegritySig.Off || _IntegrityARSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.Off)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_Wh_Y2W_Bl_M_ClnR_YG_M : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
                    if (_2YWSig.Off)
                    {
                        if (_IntegrityYGSig.Off)
                            state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                        else
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                    }
                    else if (_2YWSig.On)
                    {
                        if (_BlSig.Off && _WSig.Off)
                            state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
                        else if (_BlSig.On && _WSig.Off)
                            state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
                        else
                            state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
                    }
				}
                else if (_SSig.Off)
                {
                    if (_WSig.Off && _MSig.Off)
                    {
                        if (_IntegrityRSig.Off)
                            state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                        else
                            state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                    }
                    else if (_WSig.On || _MSig.On)
                    {
                        if (_IntegrityMSig.Off)
                            state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
                        else if (_WSig.On && _MSig.Off)
                            state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                        else if (_WSig.Off && _MSig.On)
                            state = (int)Uni2States.StateSignalStates.White;
                    }
                }
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_Wh_Y2W_Bl_M_ClnR_YG_M_O : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;


		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;


			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_2YWSig.Off)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off || _IntegritySig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// 4	107	Светофор (С, КО, ЖО, 2ЖБО)	ObjStateInSignal_S_IntR_Y2W
    /// </summary>
    public class ObjStateInSignal_S_IntR_Y2W : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала на светофоре
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;
        /// <summary>
        /// Контроль горения второго желтого и белого огня на светофоре
        /// </summary>
        protected ISigDiscreteLogic _2YWSig;
        
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegrityRSig
            if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для Y2WSig
            if (!AssignToSig("2YWSig", out _2YWSig))
                return false;
            
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegrityRSig.Off)
                    state = 19;//Отказ на светофоре
                else
                {
                    if (_SSig.On)// открыт
                    {
                        if (_2YWSig.On)
                            state = 12;//Один жёлтый огонь, один белый огонь
                        else
                            state = 5;// Один желтый огонь
                    }
                    else if (_IntegrityRSig.On)
                    {
                        state = 15;//Один красный огонь (закрыт)
                    }
                   else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_Wh_Y2W_M_ClnR_G_M : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала светофора
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
        /// Контроль горения второго желтого и белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
        /// Контроль целостности нити лампы красного огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
        /// Контроль целостности нитей лампы желтого и зеленого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_2YWSig.Off)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off )
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Y2W_Bl_M_ClnR_YG_M : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей лампы желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;
		
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для Y2WSig
			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_2YWSig.On && _BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else if (_2YWSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off )
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_Wh_Bl_M_ClnR_YG_M : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль включения пригласительного сигнала на светофоре
		/// </summary>
		protected ISigDiscreteLogic _WSig;
		/// <summary>
		/// Контроль мигающего режима на светофоре
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нитей ламп желтого и зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityYGSig;
		/// <summary>
		/// Контроль целостности нити лампы белого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityMSig;


		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WSig
			if (!AssignToSig("WSig", out _WSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityWSig
			if (!AssignToSig("IntegrityMSig", out _IntegrityMSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else if (_BlSig.Off && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.Yellow;// Два желтых огня
					else if (_BlSig.On && _WSig.Off)
						state = (int)Uni2States.StateSignalStates.YellowBlink;// Два желтых огня, верхний мигающий
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
				}
				else if (_SSig.Off && _WSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off )
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else if (_SSig.Off && _WSig.Off && _MSig.On && _IntegrityMSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной основной: откр, зел, пригласительный,горение 2го желтого и белого + целостность кр, б, жз
	/// </summary>
	public class ObjStateInSignal_S_G_M_Cln_R_G : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на зеленый огонь светофора
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль маневрового
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль целостности нити лампы зеленого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityGSig;

		
		

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для BlSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
		
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			AssignToSig("IntegrityGSig", out _IntegrityGSig);

			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegritySig", out _IntegrityRSig))
				return false;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_GSig.On)
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
					else
						state = (int)Uni2States.StateSignalStates.Yellow;
				}
				else if (_SSig.Off && _MSig.Off)
				{
					if (_IntegrityRSig.Off )
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _MSig.On)
					state = (int)Uni2States.StateSignalStates.White;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_G_Wh_Y2_Bl_ClnR_YG_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    public class ObjStateInSignal_S_W_Bl_ClnGYW : AbsenceObjState
    {
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала на светофоре
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль мигающего режима на светофоре
        /// </summary>
        protected ISigDiscreteLogic _BlSig;
        /// <summary>
        /// Контроль целостности нити лампы белого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityWSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("SSig", out _SSig))
                return false;
            if (!AssignToSig("WSig", out _WSig))
                return false;
            if (!AssignToSig("BlSig", out _BlSig))
                return false;
            if (!AssignToSig("IntegrityGYWSig", out _IntegrityWSig))
                return false;

            return true;
        }
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)// открыт
                {
                    if (_IntegrityWSig.Off && _BlSig.Off)
                        state = (int)Uni2States.StateSignalStates.Yellow;// Желтый огонь
                    else if (_IntegrityWSig.Off && _BlSig.On)
                        state = (int)Uni2States.StateSignalStates.YellowBlink;// Желтый мигающий огонь
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else if (_IntegrityWSig.On)
                {
                    if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    public class ObjStateInSignal_S_W_Bl_ClnY2_GYW : ObjStateInSignal_S_W_Bl_ClnGYW
    {
        /// <summary>
        /// Контроль целостности нити лампы первого желтого огня светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("Integrity2YSig", out _IntegrityYSig))
                return false;

            return true;
        }
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)// открыт
                {
                    if (_IntegrityWSig.Off && _BlSig.Off)
                        state = (int)Uni2States.StateSignalStates.Yellow;// Желтый огонь
                    else if (_IntegrityWSig.Off && _BlSig.On)
                        state = (int)Uni2States.StateSignalStates.YellowBlink;// Желтый мигающий огонь
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else if (_IntegrityWSig.On)
                {
                    if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }

    }	

	/// <summary>
	/// Входной дополнительный: откр,  пригласительный,горение 2го желтого и белого + целостность кр, б, ж1
	/// </summary>
    public class ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W : ObjStateInSignal_S_W_Bl_ClnY2_GYW
	{
		/// <summary>
		/// Контроль горения второго желтого и белого огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _2YWSig;
		/// <summary>
		/// Контроль целостности нити лампы красного огня на светофоре
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("2YWSig", out _2YWSig))
				return false;
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		///		закрыт		с=0 и з=0 и пр=0 && ц.кр=1
		///		открыт		с=1 и ц.разр)
		///		маневр		-
		///		пригласит   (с=0 и пр=1 && ц.б=1
		///		отказ		(миг и ц.1ж=0) или (пр=1 и ц.б=0)
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)// открыт
				{
					if (_2YWSig.On && _BlSig.Off)
						state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
					else if (_2YWSig.On && _BlSig.On)
						state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;// Два желтых огня, верхний мигающий
					else
						state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				}
				else if (_SSig.Off && _WSig.Off)
				{
					if (_IntegrityRSig.Off)
						state = (int)Uni2States.StateSignalStates.Fail;//Отказ на светофоре
					else
						state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				}
				else if (_SSig.Off && _WSig.On)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_Wh_Y2_Bl_ClnR_Y1_W " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Входной дополнительный: откр + целостность 2ж, з
	/// </summary>
	public class ObjStateInSignal_S_Cln_2Y_G : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль целостности нити лампы второго желтого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityY2Sig;
		/// <summary>
		/// Контроль целостности нити лампы зеленого огня светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegrityGSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityRSig
			if (!AssignToSig("IntegrityY2Sig", out _IntegrityY2Sig))
				return false;
			// Получаем и подписываемся на рассылку изменения для IntegrityYGSig
			if (!AssignToSig("IntegrityGSig", out _IntegrityGSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On)
					state = (int)Uni2States.StateSignalStates.Green; //Один зеленый огонь (открыт) 
				else
					state = (int)Uni2States.StateSignalStates.Red;//Один красный огонь (закрыт)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_S_Cln_2Y_G " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние светофора OutSignalPS с сигналом WhiteSig
	/// </summary>
	public class ObjStateOutSignalPS_WhiteExist : AbsenceObjState
	{
        /// <summary>
        /// Контроль открытия светофора
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль маневрового
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль включения пригласительного сигнала на светофоре
        /// </summary>
        protected ISigDiscreteLogic _WhiteSig;
        /// <summary>
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("WSig", out _WhiteSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для WhiteSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[(NOT {m_SSig}) AND (NOT {m_KMSSig}) AND (NOT {m_WhiteSig})];2:[{m_SSig} AND (NOT {m_KMSSig}) AND NOT {m_WhiteSig}];3:[{m_KMSSig} AND (NOT {m_SSig}) AND NOT {m_WhiteSig}];4:[{m_WhiteSig} AND NOT IsBlink({m_KMSSig})];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.Off && _MSig.Off && _WhiteSig.Off && _IntegritySig.On)
					state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				else if (_SSig.Off && !_MSig.On && _WhiteSig.Off && _IntegritySig.Off)
					state = (int)Uni2States.StateSignalStates.Fail;
				else if (_SSig.On && _MSig.Off && _WhiteSig.Off)
					state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else if (_MSig.On && _SSig.Off && _WhiteSig.Off)
					state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание
				else if (_WhiteSig.On && !_SSig.On && _MSig.Off)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else if (_IntegritySig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateOutSignalPS_WhiteExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние светофора OutSignalPS с сигналом WhiteSig
    /// </summary>
    public class ObjStateOutSignalPS_WhiteExist_FailOnIntegrity : ObjStateOutSignalPS_WhiteExist
	{
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_SSig.Off && _MSig.Off && _WhiteSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.On && _MSig.Off && _WhiteSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_MSig.On && _SSig.Off && _WhiteSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание
                else if (_WhiteSig.On && !_MSig.Blink)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                else if (_MSig.Blink)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateOutSignalPS_WhiteExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние светофора OutSignalPS без сигнала WhiteSig
	/// </summary>
	public class ObjStateOutSignalPS_WhiteNone : AbsenceObjState
	{
		/// <summary>
		/// Контроль открытия светофора (сигнальное реле)
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на маневровое показание
		/// </summary>
		protected ISigDiscreteLogic _MSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("MSig", out _MSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			// 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.Off && _MSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
				else if (_SSig.On && _MSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else if (_MSig.On && _SSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание
				else if (_MSig.Blink)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateOutSignalPS_WhiteNone " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние станционного светофора с огневым и маневровым
    /// </summary>
    public class ObjStateSignal_O_M : DefObjState
    {
        /// <summary>
        /// Контроль открытия на маневровое показание
        /// </summary>
        protected ISigDiscreteLogic _MSig;
        /// <summary>
        /// Контроль огневого реле
        /// </summary>
        protected ISigDiscreteLogic _OSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для KMSSig
            if (!AssignToSig("MSig", out _MSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для KMSSig
            if (!AssignToSig("IntegritySig", out _OSig))
                if (!AssignToSig("IntegrityRSig", out _OSig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_MSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                }
                else
                {
                    if (_OSig.On)
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_M : ObjStateSignal_O_M
    {
        /// <summary>
        /// Контроль открытия светофора (сигнальное реле)
        /// </summary>
        protected ISigDiscreteLogic _SSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else
                {
                    if (_MSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                    }
                    else
                    {
                        if (_OSig.On)
                            state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                        else
                            state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, выключением огневого и маневровым
    /// </summary>
    public class ObjStateSignal_S_Off : DefObjState
    {
        /// <summary>
        /// Контроль открытия светофора (сигнальное реле)
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityOffSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("SSig", out _SSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для IntegrityOffSig
            if (!AssignToSig("IntegrityOffSig", out _IntegrityOffSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else
                {
                    if (_IntegrityOffSig.On)
                        state = (int)Uni2States.StateSignalStates.NoLight;
                    else
                        state = (int)Uni2States.StateSignalStates.Red;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, выключением огневого и маневровым
    /// </summary>
    public class ObjStateSignal_S_Off_M : ObjStateSignal_S_Off
    {
        /// <summary>
        /// Контроль открытия на маневровое показание
        /// </summary>
        protected ISigDiscreteLogic _MSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для MSig
            if (!AssignToSig("MSig", out _MSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                }
                else
                {
                    if (_MSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                    }
                    else
                    {
                        if (_IntegrityOffSig.On)
                            state = (int)Uni2States.StateSignalStates.NoLight;
                        else
                            state = (int)Uni2States.StateSignalStates.Red;
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, зеленым и пригласительным
    /// </summary>
    public class ObjStateSignal_S_O_G_W : DefObjState
    {
        /// <summary>
        /// Контроль открытия светофора (сигнальное реле)
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Контроль открытия на зеленый огонь
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль открытия на пригласительное показание
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("SSig", out _SSig))
                return false;
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                if (!AssignToSig("IntegrityRSig", out _IntegritySig))
                    return false;
            if (!AssignToSig("GSig", out _GSig))
                return false;
            if (!AssignToSig("WSig", out _WSig))
                return false;

            return true;
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, зеленым, пригласительным и целостностями ЖЗО и 2ЖБО
    /// </summary>
    public class ObjStateSignal_S_O_G_W_IntYG_IntY2W : ObjStateSignal_S_O_G_W
    {
        /// <summary>
        /// Контроль сигнала целостности ЖЗ
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;
        /// <summary>
        /// Контроль сигнала целостности Ж2Б
        /// </summary>
        protected ISigDiscreteLogic _IntegrityY2WSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;
            if (!AssignToSig("IntegrityY2WSig", out _IntegrityY2WSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.Off)
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_SSig.On)
                {
                    if (_IntegrityYGSig.On)
                    {
                        if (_GSig.On)
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                        else if (_IntegrityY2WSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
                        else
                            state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                    }
                    else if (_WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый (маневровое показание)
                    }
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, зеленым, пригласительным и целостностями ЖЗО и 2ЖБО
    /// </summary>
    public class ObjStateSignal_S_O_G_W_Bl_IntYGW : ObjStateSignal_S_O_G_W
    {
        /// <summary>
        /// Контроль сигнала целостности ЖЗ
        /// </summary>
        protected ISigDiscreteLogic _BlSig;
        /// <summary>
        /// Контроль сигнала целостности Ж2Б
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGWSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("BlSig", out _BlSig))
                return false;
            if (!AssignToSig("IntegrityYGWSig", out _IntegrityYGWSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.On)
                {
                    if (_WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;
                    }
                    else if (_SSig.On && _BlSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;
                    }
                    else
                    {
                        state = (int)Uni2States.StateSignalStates.Red;
                    }
                }
                else if (_IntegrityYGWSig.On)
                {
                    if (_SSig.On)
                    {
                        if (_GSig.On)
                            state = (int)Uni2States.StateSignalStates.Green;
                        else
                            state = (int)Uni2States.StateSignalStates.Yellow;
                    }
                    else
                        state = (int)Uni2States.StateSignalStates.Red;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, зеленым, пригласительным и целостностями ЖЗО и 2ЖБО
    /// </summary>
    public class ObjStateSignal_S_G_R_W_IntYGW : DefObjState
    {
        /// <summary>
        /// Контроль открытия светофора (сигнальное реле)
        /// </summary>
        protected ISigDiscreteLogic _SSig;
        /// <summary>
        /// Контроль открытия на зеленый огонь
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль фактического горения красного огня
        /// </summary>
        protected ISigDiscreteLogic _RSig;
        /// <summary>
        /// Контроль открытия на пригласительное показание
        /// </summary>
        protected ISigDiscreteLogic _WSig;
        /// <summary>
        /// Контроль сигнала целостности ЖЗ
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("SSig", out _SSig))
                return false;
            if (!AssignToSig("GSig", out _GSig))
                return false;
            if (!AssignToSig("RSig", out _RSig))
                return false;
            if (!AssignToSig("WSig", out _WSig))
                return false;

            if (!AssignToSig("IntegrityYGWSig", out _IntegrityYGSig))
                if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    if (_IntegrityYGSig.On)
                    {
                        if (_GSig.On)
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                        else if (_RSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
                        else
                            state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                    }
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_RSig.On)
                {
                    if (_WSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Пригласительный
                    else
                        state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, зеленым, пригласительным и целостностями ЖЗО и 2ЖБО
    /// </summary>
    public class ObjStateSignal_S_G_R_W_IntYG_IntY2W : ObjStateSignal_S_G_R_W_IntYGW
    {
        /// <summary>
        /// Контроль сигнала целостности Ж2Б
        /// </summary>
        protected ISigDiscreteLogic _IntegrityY2WSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegrityY2WSig", out _IntegrityY2WSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On)
                {
                    if (_IntegrityYGSig.On)
                    {
                        if (_GSig.On)
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                        else if (_IntegrityY2WSig.On)
                            state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых огня
                        else
                            state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                    }
                    else if (_WSig.On)
                    {
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый (маневровое показание)
                    }
                }
                else if (_RSig.On)
                {
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                }
                else
                {
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }



	public class ObjStateSignal_S_O_M_FailOnIntegrity : ObjStateSignal_S_O_M
	{
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_SSig.On && _MSig.Off && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_SSig.Off && _MSig.On && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_OSig.Off && _MSig.Off && _SSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_OSig.On && _SSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;

			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние станционного светофора с сигнальным, огневым и маневровым
	/// </summary>
	public class ObjStateSignal_S_M_MO_YR : DefObjState
	{
		/// <summary>
		/// Контроль открытия светофора (сигнальное реле)
		/// </summary>
		protected ISigDiscreteLogic _SSig;
		/// <summary>
		/// Контроль открытия на маневровое показание
		/// </summary>
		protected ISigDiscreteLogic _MSig;
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль огневого реле маневрового
		/// </summary>
		protected ISigDiscreteLogic _YRSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("SSig", out _SSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("MSig", out _MSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для KMSSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;

			// Получаем и подписываемся на рассылку изменения
			if (!AssignToSig("YRSig", out _YRSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On && _YRSig.Off)
					state = (int) Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				else if (_SSig.On && _YRSig.Off)
					state = (int) Uni2States.StateSignalStates.Yellow;// Один желтый огонь (открыт)
				else if (_SSig.Off && _MSig.On )
					state = (int) Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
				else if (_SSig.Off && _IntegritySig.Off)
					state = (int) Uni2States.StateSignalStates.Fail;// отказ
				else if (_SSig.Off && _MSig.Off && _IntegritySig.On)
					state = (int) Uni2States.StateSignalStates.Red;// закрыт
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_M_W : ObjStateSignal_S_O_M
    {
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _WSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("WSig", out _WSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && _MSig.Off && _WSig.Off)
					state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_SSig.Off && _MSig.On && _WSig.Off)
					state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && _MSig.Off && _OSig.On && _WSig.Off)
					state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && _MSig.Off && _OSig.On && _WSig.On)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else if (_SSig.Off && _MSig.Off && _OSig.Off)
					state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    public class ObjStateSignal_S_O_M_W_ClnY2W : ObjStateSignal_S_O_M_W
    {
        /// <summary>
        /// Контроль целостности нитей ламп второго желтого и белого огня
        /// </summary>
        protected ISigDiscreteLogic _IntegrityY2WSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("IntegrityY2WSig", out _IntegrityY2WSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_WSig.Off && _MSig.Off && _SSig.Off && _IntegrityY2WSig.On && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_WSig.Off && _MSig.Off && _SSig.Off && _OSig.On)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else if (_WSig.Off && _MSig.Off && _SSig.On && _IntegrityY2WSig.On && _OSig.On)
                    state = (int)Uni2States.StateSignalStates._2Yellow;// Два желтых (открыт)
                else if (_WSig.Off && _MSig.On && _SSig.Off && _OSig.On)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_WSig.On && _MSig.Off && _SSig.Off && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_M_W_ClnYG : ObjStateSignal_S_O_M_W
    {
        /// <summary>
        /// Контроль сигнала целостности ЖЗ
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYGSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для IntegrityYGSig
            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && _MSig.Off && _WSig.Off)
                {
                    if (_IntegrityYGSig.On)
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                    else
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                }
                else if (_SSig.Off && _MSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && _MSig.Off && _OSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && _MSig.Off && _OSig.On && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else if (_SSig.Off && _MSig.Off && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым, маневровым, пригласительным и ЖБ
    /// </summary>
    public class ObjStateSignal_S_O_M_W_YB_ClnYG_W : ObjStateSignal_S_O_M_W_ClnYG
    {
        /// <summary>
        /// Контроль сигнального реле ЖБ
        /// </summary>
        protected ISigDiscreteLogic _YBSig;
        /// <summary>
        /// Контроль сигнала целостности Б
        /// </summary>
        protected ISigDiscreteLogic _IntegrityWSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("YBSig", out _YBSig))
                return false;
            if (!AssignToSig("IntegrityWSig", out _IntegrityWSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_WSig.Off && _MSig.Off && _SSig.Off && _YBSig.Off && _OSig.On && _IntegrityWSig.On)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_WSig.Off && _MSig.On && _SSig.Off && _YBSig.On && _OSig.On && _IntegrityWSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_WSig.Off && _MSig.On && _SSig.Off && _YBSig.On && _OSig.Off && _IntegrityWSig.Off)
                    state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь (открыт)
                else if (_WSig.Off && _MSig.Off && _SSig.Off && _YBSig.Off && _OSig.Off && _IntegrityWSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else if (_SSig.On && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние станционного светофора с сигнальным, огневым и маневровым
	/// </summary>
	public class ObjStateSignal_S_O_M_W_YR : ObjStateSignal_S_O_M_W
	{
		/// <summary>
		/// Контроль пригласительного сигнала
		/// </summary>
		protected ISigDiscreteLogic _YRSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SSig
			if (!AssignToSig("YRSig", out _YRSig))
				return false;

			return true;
		}
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_SSig.On && !_MSig.On && !_WSig.On)
				{
                    if (_YRSig.On)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
				}
                else if (!_SSig.On && _MSig.On && !_WSig.On)
					state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (!_SSig.On && !_MSig.On && _OSig.On && _WSig.Off)
					state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && !_MSig.On && _OSig.On && _WSig.On)
					state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
				else if (_SSig.Off && !_MSig.On && _OSig.Off)
					state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_M_W_GR : ObjStateSignal_S_O_M_W
    {
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _GRSig;
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("GRSig", out _GRSig))
                return false;

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && !_MSig.On && !_WSig.On)
                {
                    if (_GRSig.On)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                        state = (int)Uni2States.StateSignalStates.Green;// Зеленый
                }
                else if (_SSig.Off && _MSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && !_MSig.On && _OSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && !_MSig.On && _OSig.On && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else if (_SSig.Off && !_MSig.On && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_M_W_YR_GR : ObjStateSignal_S_O_M_W
    {
        /// <summary>
        /// Контроль сигнала перехода на желтый огонь с зеленого
        /// </summary>
        protected ISigDiscreteLogic _YGSig;
        /// <summary>
        /// Контроль сигнала перехода на резервную нить
        /// </summary>
        protected ISigDiscreteLogic _YRSig;
        /// <summary>
        /// Контроль сигнала перехода на резервную нить
        /// </summary>
        protected ISigDiscreteLogic _GRSig;
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("YGSig", out _YGSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("YRSig", out _YRSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("GRSig", out _GRSig))
                return false;

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && !_MSig.On && !_WSig.On)
                {
                    if (_YRSig.On || _GRSig.On)
                        state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                    else
                    {
                        if (_YGSig.On)
                            state = (int)Uni2States.StateSignalStates.Yellow;// Один желтый огонь
                        else
                            state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                    }
                }
                else if (_SSig.Off && _MSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && !_MSig.On && _OSig.On && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && !_MSig.On && _OSig.On && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else if (_SSig.Off && !_MSig.On && _OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;// Отказ на светофоре
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_Off_M_W : ObjStateSignal_S_O_M_W
    {
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _IntegrityOffSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для SSig
            if (!AssignToSig("IntegrityOffSig", out _IntegrityOffSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Вычисление состояния в зависимости от сигналов по формуле 1:[NOT {m_SSig} AND NOT {m_KMSSig}];2:[{m_SSig} AND NOT {m_KMSSig}];3:[{m_KMSSig} AND NOT {m_SSig}];7:[IsBlink({m_KMSSig})];
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SSig.On && _MSig.Off && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_SSig.Off && _MSig.On)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && _MSig.Off && _IntegrityOffSig.Off && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && _MSig.Off && _IntegrityOffSig.Off && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригласительный)
                else if (_IntegrityOffSig.On)
                    state = (int)Uni2States.StateSignalStates.NoLight;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние станционного светофора с сигнальным, огневым и маневровым
    /// </summary>
    public class ObjStateSignal_S_O_Off_M_W : ObjStateSignal_S_Off_M_W
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_OSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;
                else if (_IntegrityOffSig.On)
                {
                    if (_SSig.On && _MSig.Off && _WSig.On)
                        state = (int)Uni2States.StateSignalStates.Green_White;// Зеленый и белый огни
                    else
                        state = (int)Uni2States.StateSignalStates.NoLight;
                }
                else if (_SSig.On && _MSig.Off && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Green;// Один зеленый огонь (открыт)
                else if (_SSig.On && _MSig.Off && _WSig.On && _IntegrityOffSig.On)
                    state = (int)Uni2States.StateSignalStates.Green_White;// Один зеленый и один белый огни
                else if (_SSig.Off && _MSig.On)
                    state = (int)Uni2States.StateSignalStates.White;// Один белый (маневровое показание)
                else if (_SSig.Off && _MSig.Off && _IntegrityOffSig.Off && _WSig.Off)
                    state = (int)Uni2States.StateSignalStates.Red;// Один красный огонь (закрыт)
                else if (_SSig.Off && _MSig.Off && _IntegrityOffSig.Off && _WSig.On)
                    state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;// Один белый мигающий и красный огни (пригла-сительный)
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateSignal_S_O_M " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }

            SetState(state);

            ResetModify();
        }
    }


    /// <summary>
    /// Состояние светофора CInSignal с сигналами OSig, SSig, WhiteSig
    /// </summary>
    public class ObjStateSignal_MZF : AbsenceObjState
    {
        /// <summary>
        /// Контроль кодового сигнала
        /// </summary>
        protected ISigDiscreteLogic _CodeSig;
        /// <summary>
        /// Контроль пригласительного сигнала
        /// </summary>
        protected ISigDiscreteLogic _WBRSig;
        /// <summary>
        /// Контроль целостности
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Контроль целостности
        /// </summary>
        protected ISigDiscreteLogic _IntegrityRSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("GSig", out _CodeSig))
                return false;
            if (!AssignToSig("WBRSig", out _WBRSig))
                return false;
            AssignToSig("IntegritySig", out _IntegritySig);
            AssignToSig("IntegrityRSig", out _IntegrityRSig);

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig != null && _IntegritySig.Off 
                    || _IntegrityRSig != null && _IntegrityRSig.Off)
                    state = (int)Uni2States.StateSignalStates.Fail;
                else
                {
                    uint code = _CodeSig.IOSignals[0].GetCurrent;
                    if (code == 0 && _WBRSig.Off)
                        state = (int)Uni2States.StateSignalStates.Red;
                    else if (code == 0 && _WBRSig.On)
                        state = (int)Uni2States.StateSignalStates.WhiteBlink_Red;
                    else if (code == 1)
                        state = (int)Uni2States.StateSignalStates.Green;
                    else if (code == 2)
                        state = (int)Uni2States.StateSignalStates.GreenBlink;
                    else if (code == 3)
                        state = (int)Uni2States.StateSignalStates.Yellow;
                    else if (code == 4)
                        state = (int)Uni2States.StateSignalStates.YellowBlink;
                    else if (code == 5)
                        state = (int)Uni2States.StateSignalStates.White;
                    else if (code == 6)
                        state = (int)Uni2States.StateSignalStates._2Yellow;
                    else if (code == 7)
                        state = (int)Uni2States.StateSignalStates._2Yellow_UpperBlink;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    #endregion

	#region Заградительный светофор
    /// <summary>
    /// Состояние заградительного светофора
    /// </summary>
    public class ObjStateBarrageSignal : AbsenceObjState
    {
        /// <summary>
        /// Сигнал заграждения
        /// </summary>
        protected ISigDiscreteLogic _BarSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("BarSig", out _BarSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_BarSig.On)
                    state = (int)Uni2States.BarrageSignalStates.Red; // Один красный огонь (закрыт)
                else if (_BarSig.Off)
                    state = (int)Uni2States.BarrageSignalStates.Canceled; // Погашен
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние заградительного светофора АБТЦМ. Сигнал модуля БПСС
    /// </summary>
    public class ObjStateBarrageSignal_ABTCM : ObjStateBarrageSignal
    {
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_BarSig.Off)
                    state = (int)Uni2States.BarrageSignalStates.Red; // Один красный огонь (закрыт)
                else if (_BarSig.On)
                    state = (int)Uni2States.BarrageSignalStates.Canceled; // Погашен
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние заградительного светофора АБТЦМ. Сигнал модуля БПСС
    /// </summary>
    public class ObjStateBarrageSignal_Z_O_Or : ObjStateBarrageSignal
    {
        /// <summary>
        /// Сигнал контроля целостности нити красного огня
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Сигнал контроля целостности резервных нитей ламп
        /// </summary>
        protected ISigDiscreteLogic _IntegrityResSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegritySig", out _IntegritySig))
                if (!AssignToSig("IntegrityRSig", out _IntegritySig))
                    return false;
            if (!AssignToSig("IntegrityResSig", out _IntegrityResSig))
                return false;

            return true;
        }
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.On && _IntegrityResSig.On)
                {
                    if (_BarSig.On)
                        state = (int) Uni2States.BarrageSignalStates.Red; // Один красный огонь (закрыт)
                    else
                        state = (int) Uni2States.BarrageSignalStates.Canceled; // Погашен
                }
                else
                    state = (int) Uni2States.BarrageSignalStates.Fail;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние заградительного светофора с реле ЗГ, с контролем основной нити
    /// </summary>
    public class ObjStateBarrageSignal_RelZG_CtrlO : ObjStateBarrageSignal
    {
        /// <summary>
        /// Сигнал Контроля целостности основной нити лампы красного огня СУ
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("IntegritySig", out _IntegritySig))
                    return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegritySig.On)
                {
                    if (_BarSig.On)
                        state = (int)Uni2States.BarrageSignalStates.Canceled; // Нормально погашен
                    else
                        state = (int)Uni2States.BarrageSignalStates.Red; // Включение заграждения (красный огонь)
                }
                else
                    state = (int)Uni2States.BarrageSignalStates.Fail; //отказ на светофоре
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    #endregion

	#region Проходной светофор
	/// <summary>
	/// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
	/// </summary>
	public class ObjStateStageSignal : AbsenceObjState
	{
		/// <summary>
		/// Контроль огневого реле сигнальной установки
		/// </summary>
		private ISigDiscreteLogic _IntegritySig;
		/// <summary>
		/// Контроль сигнального реле З сигнальной установки
		/// </summary>
		private ISigDiscreteLogic _SzSig;
		/// <summary>
		/// Контроль сигнального реле Ж сигнальной установки
		/// </summary>
		private ISigDiscreteLogic _SgSig;
		/// <summary>
		/// Контроль мигания сигнальной установки
		/// </summary>
		private ISigDiscreteLogic _BlSig;
		/// <summary>
		/// Индикатор соответствующего направления
		/// </summary>
		private UniObject _DirObj;
		/// <summary>
		/// Сигнал индикатора соответствующего направлния
		/// </summary>
		private ISigDiscreteLogic _DirObjSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SzSig
			if (!AssignToSig("SzSig", out _SzSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SzSig
			if (!AssignToSig("SgSig", out _SgSig))
				return false;

			AssignToSig("BlSig", out _BlSig);

			// Получаем индикатор соотв направления и подписываемся на изменения его состояния
			ObjData objData = (_controlObj.GetObject("DirObj") as ObjData);
			if (objData == null)
			{
				Console.WriteLine("Can't get object DirObj as ObjData in " + _controlObj.Name);
			}
			else
			{
				if (objData.Object is UniObject)
					_DirObj = objData.Object as UniObject;
				else
					Console.WriteLine(_controlObj.Name + ":DirObj is not UniObject!");
				AssignToObj("DirObj");
				AssignToSigByObj("DirObj", "Sig1", out _DirObjSig);
				AssignToSigByObj("DirObj", "Sig2", out _DirObjSig);
			}

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				//проверяем индикатор нашего направления
				if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
				{
					state = (int)Uni2States.StageSignalStates.Canceled;// погашен
				}
				else if (_DirObj != null &&
					(_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
					|| _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
				{
					state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
				}
				else
				{
					//установлено необходимое направление
					if (_SgSig.On && _IntegritySig.Off)
						state = (int)Uni2States.StageSignalStates.Close_Red; // Один красный огонь (закрыт)
					else if (_SgSig.Off && _SzSig.Off && _IntegritySig.Off)
						state = (int)Uni2States.StageSignalStates.Open_Green; // Один зеленый огонь (открыт)
					else if (_SgSig.Off && _SzSig.On && _IntegritySig.Off)
					{
						if (_BlSig != null && _BlSig.On)
							state = (int)Uni2States.StageSignalStates.YellowBlink; // Один желтый мигающий огонь
						else
							state = (int)Uni2States.StageSignalStates.Yellow; // Один желтый огонь
					}
					else if (_IntegritySig.On)
						state = (int)Uni2States.StageSignalStates.Fail; // Отказ на светофоре
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
    /// </summary>
    public class ObjStateStageSignal_G_O : AbsenceObjState
    {
        /// <summary>
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;
        /// <summary>
        /// Контроль сигнального реле З сигнальной установки
        /// </summary>
        protected ISigDiscreteLogic _GSig;

        /// <summary>
        /// Индикатор соответствующего направления
        /// </summary>
        protected UniObject _DirObj;
        /// <summary>
        /// Сигнал индикатора соответствующего направлния
        /// </summary>
        protected ISigDiscreteLogic _DirObjSig;
        /// <summary>
        /// Сигнал индикатора соответствующего направлния
        /// </summary>
        protected ISigDiscreteLogic _DirSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                return false;
            // Получаем и подписываемся на рассылку изменения для SzSig
            if (!AssignToSig("GSig", out _GSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для SzSig
            AssignToSig("DirSig", out _DirSig);

            // Получаем индикатор соотв направления и подписываемся на изменения его состояния
            ObjData objData = (_controlObj.GetObject("DirObj") as ObjData);
            if (objData == null)
            {
                Console.WriteLine("Can't get object DirObj as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
            }
            else
            {
                if (objData.Object is UniObject)
                    _DirObj = objData.Object as UniObject;
                else
                    Console.WriteLine(_controlObj.Name + ":DirObj is not UniObject!");
                AssignToObj("DirObj");
                AssignToSigByObj("DirObj", "Sig1", out _DirObjSig);
                AssignToSigByObj("DirObj", "Sig2", out _DirObjSig);
            }

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_IntegritySig.Off)
                        state = (int)Uni2States.StageSignalStates.Fail;
                    else
                    {
                        if (_GSig.On)
                        {
                            state = (int)Uni2States.StageSignalStates.Open_Green;
                        }
                        else
                            state = (int)Uni2States.StageSignalStates.Close_Red;
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
	/// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
	/// </summary>
	public class ObjStateStageSignal_G_Y : AbsenceObjState
	{
		/// <summary>
		/// Контроль сигнального реле З сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль сигнального реле Ж сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _YSig;
		
		/// <summary>
		/// Индикатор соответствующего направления
		/// </summary>
		protected UniObject _DirObj;
		/// <summary>
		/// Сигнал индикатора соответствующего направлния
		/// </summary>
		protected ISigDiscreteLogic _DirObjSig;
        /// <summary>
        /// Сигнал индикатора соответствующего направлния
        /// </summary>
        protected ISigDiscreteLogic _DirSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для SzSig
			if (!AssignToSig("GSig", out _GSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для SzSig
			if (!AssignToSig("YSig", out _YSig))
				return false;

            // Получаем и подписываемся на рассылку изменения для SzSig
            if (!AssignToSig("DirSig", out _DirSig))
                if (!AssignToSig("DirInSig", out _DirSig))
                    AssignToSig("DirOutSig", out _DirSig);

			// Получаем индикатор соотв направления и подписываемся на изменения его состояния
			ObjData objData = (_controlObj.GetObject("DirObj") as ObjData);
			if (objData == null)
			{
				Console.WriteLine("Can't get object DirObj as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
			}
			else
			{
				if (objData.Object is UniObject)
					_DirObj = objData.Object as UniObject;
				else
					Console.WriteLine(_controlObj.Name + ":DirObj is not UniObject!");
				AssignToObj("DirObj");
				AssignToSigByObj("DirObj", "Sig1", out _DirObjSig);
				AssignToSigByObj("DirObj", "Sig2", out _DirObjSig);
			}

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_GSig.On)
                    {
                        state = (int)Uni2States.StageSignalStates.Open_Green;
                    }
                    else if (_YSig.On)
                    {
                        state = (int)Uni2States.StageSignalStates.Yellow;
                    }
                    else
                        state = (int)Uni2States.StageSignalStates.Close_Red;

                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
    /// </summary>
    public class ObjStateStageSignal_G_Y_O : ObjStateStageSignal_G_Y
    {
        /// <summary>
        /// Контроль целостности нитей ламп светофора
        /// </summary>
        protected ISigDiscreteLogic _IntegritySig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if ((_GSig.Off && _YSig.Off && _IntegritySig.Off)
                        || ((_GSig.On || _YSig.On) && _IntegritySig.Off))
                        state = (int)Uni2States.StageSignalStates.Fail;
                    else
                    {
                        if (_GSig.On)
                        {
                            state = (int)Uni2States.StageSignalStates.Open_Green;
                        }
                        else if (_YSig.On)
                        {
                            state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                            state = (int)Uni2States.StageSignalStates.Close_Red;

                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора.
    /// </summary>
    public class ObjStateStageSignal_G_Y_O_On : ObjStateStageSignal_G_Y_O
    {
        /// <summary>
        /// Есть питание ламп (светофор не погашен)
        /// </summary>
        protected ISigDiscreteLogic _OnSig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("OnSig", out _OnSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_OnSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;
                }
                else
                {
                    if (_YSig.On && _GSig.On)
                        state = (int)Uni2States.StageSignalStates.Open_Green;
                    else if (_YSig.On && _GSig.Off)
                        state = (int)Uni2States.StageSignalStates.Yellow;
                    else if (_IntegritySig.On)
                        state = (int)Uni2States.StageSignalStates.Close_Red;
                    else
                        state = (int)Uni2States.StageSignalStates.Fail;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }

    }

    /// <summary>
	/// Состояние проходного светофора.
	/// </summary>
    public class ObjStateStageSignal_G_Y_Off : ObjStateStageSignal_G_Y
	{
        /// <summary>
        /// Контроль выключения огневого реле
        /// </summary>
        protected ISigDiscreteLogic _OffSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

            if (!AssignToSig("IntegrityOffSig", out _OffSig))
                return false;
           
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
			    //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int) Uni2States.IndObjStates.NotAlight )
			    {
			        state = (int) Uni2States.StageSignalStates.Canceled;// погашен
			    }
				else if (_DirObj != null &&
					(_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
					|| _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
				{
					state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
				}
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_OffSig.On)
                        state = (int)Uni2States.StageSignalStates.Canceled;
                    else
                    {
                        if (_YSig.On)
                        {
                            if (_GSig.On)
                                state = (int)Uni2States.StageSignalStates.Open_Green;
                            else
                                state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                        {
                            state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                    }
                }
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние проходного светофора.
    /// </summary>
    public class ObjStateStageSignal_G_Y_GY_Off : ObjStateStageSignal_G_Y_Off
    {
        /// <summary>
        /// Контроль открытия на зеленый и желтый
        /// </summary>
        protected ISigDiscreteLogic _GYSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("GYSig", out _GYSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_OffSig.On)
                        state = (int)Uni2States.StageSignalStates.Canceled;
                    else
                    {
                        if (_YSig.On)
                        {
                            if (_GSig.On)
                                state = (int)Uni2States.StageSignalStates.Open_Green;
                            else if (_GYSig.On)
                                state = (int)Uni2States.StageSignalStates.Green_Yellow;
                            else
                                state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                        {
                            state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора.
    /// </summary>
    public class ObjStateStageSignal_G_Y_YBl_GY_Off : ObjStateStageSignal_G_Y_GY_Off
    {
        /// <summary>
        /// Контроль мигания желтого
        /// </summary>
        protected ISigDiscreteLogic _YBlSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("YBlSig", out _YBlSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_OffSig.On)
                        state = (int)Uni2States.StageSignalStates.Canceled;
                    else
                    {
                        if (_YSig.On)
                        {
                            if (_GSig.On)
                                state = (int)Uni2States.StageSignalStates.Open_Green;
                            else if (_GYSig.On)
                                state = (int)Uni2States.StageSignalStates.Green_Yellow;
                            else if (_YBlSig.On)
                                state = (int)Uni2States.StageSignalStates.Green_Yellow;
                            else
                                state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                        {
                            state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора.
    /// </summary>
    public class ObjStateStageSignal_G_Y_Off_Fail : ObjStateStageSignal_G_Y_Off
    {
        /// <summary>
        /// Контроль неисправности
        /// </summary>
        protected ISigDiscreteLogic _FailSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("FailSig", out _FailSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_OffSig.On)
                        state = (int)Uni2States.StageSignalStates.Canceled;
                    else
                    {
                        if (_YSig.On)
                        {
                            if (_GSig.On)
                                state = (int)Uni2States.StageSignalStates.Open_Green;
                            else
                                state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                        {
                            if (_FailSig.On)
                                state = (int)Uni2States.StageSignalStates.Fail;
                            else
                                state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора.
    /// </summary>
    public class ObjStateStageSignal_G_Y_YBl_Off_Fail : ObjStateStageSignal_G_Y_Off_Fail
    {
        /// <summary>
        /// Контроль мигания желтого
        /// </summary>
        protected ISigDiscreteLogic _YBlSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("YBlSig", out _YBlSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if (_OffSig.On)
                        state = (int)Uni2States.StageSignalStates.Canceled;
                    else
                    {
                        if (_YSig.On)
                        {
                            if (_GSig.On)
                                state = (int)Uni2States.StageSignalStates.Open_Green;
                            else if (_YBlSig.On)
                                state = (int)Uni2States.StageSignalStates.YellowBlink;
                            else
                                state = (int)Uni2States.StageSignalStates.Yellow;
                        }
                        else
                        {
                            if (_FailSig.On)
                                state = (int)Uni2States.StageSignalStates.Fail;
                            else
                                state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
    /// </summary>
    public class ObjStateStageSignal_G_Y_Bl_O : ObjStateStageSignal_G_Y_O
    {
        /// <summary>
        /// Контроль мигания сигнальной установки
        /// </summary>
        protected ISigDiscreteLogic _BlSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("BlSig", out _BlSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                //проверяем индикатор нашего направления
                if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else if (_DirObj != null &&
                    (_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
                    || _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                }
                else if (_DirSig != null && _DirSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;// погашен
                }
                else
                {
                    if ((_GSig.Off && _YSig.Off && _IntegritySig.Off)
                        || ((_GSig.On || _YSig.On) && _IntegritySig.Off))
                        state = (int)Uni2States.StageSignalStates.Fail;
                    else
                    {
                        if (_YSig.Off)
                        {
                            state = (int)Uni2States.StageSignalStates.Close_Red;
                        }
                        else
                        {
                            if (_GSig.On)
                            {
                                if (_BlSig.On)
                                    state = (int)Uni2States.StageSignalStates.GreenBlink;
                                else
                                    state = (int)Uni2States.StageSignalStates.Open_Green;
                            }
                            else
                            {
                                if (_BlSig.On)
                                    state = (int)Uni2States.StageSignalStates.YellowBlink;
                                else
                                    state = (int)Uni2States.StageSignalStates.Yellow;
                            }
                        }

                        //if (_GSig.On)
                        //{
                        //    if (_BlSig.On)
                        //        state = (int)Uni2States.StageSignalStates.GreenBlink;
                        //    else
                        //        state = (int)Uni2States.StageSignalStates.Open_Green;
                        //}
                        //else if (_YSig.On)
                        //{
                        //    if (_BlSig.On)
                        //        state = (int)Uni2States.StageSignalStates.YellowBlink;
                        //    else
                        //        state = (int)Uni2States.StageSignalStates.Yellow;
                        //}
                        //else
                        //    state = (int)Uni2States.StageSignalStates.Close_Red;

                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
	/// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
	/// </summary>
	public class ObjStateStageSignal_G_Y_ClnR_O_KZ : AbsenceObjState
	{
		/// <summary>
        /// Контроль целостности нитей ламп светофора
		/// </summary>
		protected ISigDiscreteLogic _IntegritySig;
		/// <summary>
        /// Контроль целостности нити красного огня сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _IntegrityRSig;
		/// <summary>
		/// Контроль короткого замыкания
		/// </summary>
		protected ISigDiscreteLogic _KZSig;
		/// <summary>
		/// Контроль сигнального реле З сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _GSig;
		/// <summary>
		/// Контроль сигнального реле Ж сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _YSig;
		/// <summary>
		/// Контроль мигания сигнальной установки
		/// </summary>
		protected UniObject _DirObj;
		/// <summary>
		/// Сигнал индикатора соответствующего направлния
		/// </summary>
		protected ISigDiscreteLogic _DirObjSig;

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegritySig", out _IntegritySig))
				return false;
			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
				_IntegrityRSig = null;

			if (!AssignToSig("KZSig", out _KZSig))
                _KZSig = null;
			// Получаем и подписываемся на рассылку изменения для GSig
			if (!AssignToSig("GSig", out _GSig))
                _GSig = null;
			// Получаем и подписываемся на рассылку изменения для SzSig
			if (!AssignToSig("YSig", out _YSig))
				_YSig = null;

            AssignToSig("DirInSig", out _DirObjSig);
            if (_DirObjSig == null)
                AssignToSig("DirOutSig", out _DirObjSig);

			// Получаем индикатор соотв направления и подписываемся на изменения его состояния
			ObjData objData = (_controlObj.GetObject("DirObj") as ObjData);
			if (objData == null)
			{
				Console.WriteLine("Can't get object DirObj as ObjData in " + _controlObj.Site.Name + "/" + _controlObj.Name);
			}
			else
			{
				if (objData.Object is UniObject)
					_DirObj = objData.Object as UniObject;
				else
					Console.WriteLine(_controlObj.Name + ":DirObj is not UniObject!");
				AssignToObj("DirObj");
				AssignToSigByObj("DirObj", "Sig1", out _DirObjSig);
				AssignToSigByObj("DirObj", "Sig2", out _DirObjSig);
			}

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				//проверяем индикатор нашего направления
				if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
				{
					state = (int)Uni2States.StageSignalStates.Canceled;// погашен
				}
				else if (_DirObj != null &&
					(_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
					|| _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
				{
					state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
				}
                else if (_DirObjSig != null && _DirObjSig.Off)
                {
                    state = (int)Uni2States.StageSignalStates.Canceled;
                }
                else
                {
                    //установлено необходимое направление
                    if (_KZSig != null && _KZSig.Off
                        || ((_GSig != null) && _GSig.Off && (_YSig != null) && _YSig.Off && ((_IntegrityRSig != null && _IntegrityRSig.Off) || _IntegritySig.Off))
                        || ((((_GSig != null) && _GSig.On) || ((_YSig != null) && _YSig.On)) && _IntegritySig.Off))
                        state = (int)Uni2States.StageSignalStates.Fail;
                    else
                    {

                        if (_YSig != null)
                        {
                            if (_YSig.Off)
                            {
                                state = (int)Uni2States.StageSignalStates.Close_Red;
                            }
                            else
                            {
                                if (_GSig != null)
                                {
                                    if (_GSig.On)
                                        state = (int)Uni2States.StageSignalStates.Open_Green;
                                    else
                                        state = (int)Uni2States.StageSignalStates.Yellow;
                                }
                                else
                                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                            }
                        }
                        else
                            state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;

                        // Старая версия
                        //if ((_GSig != null) && _GSig.On)
                        //    state = (int)Uni2States.StageSignalStates.Open_Green;
                        //else if ((_YSig != null) && _YSig.On)
                        //    state = (int)Uni2States.StageSignalStates.Yellow;
                        //else
                        //    state = (int)Uni2States.StageSignalStates.Close_Red;
                    }
                }
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние проходного светофора. Сигнальное З и сигнальное Ж - обязательны
	/// </summary>
	public class ObjStateStageSignal_G_Y_Bl_ClnR_O_KZ : ObjStateStageSignal_G_Y_ClnR_O_KZ
	{
		
		/// <summary>
		/// Контроль мигания
		/// </summary>
		protected ISigDiscreteLogic _BlSig;
        /// <summary>
        /// Контроль целостности нити желтого огня сигнальной установки
        /// </summary>
        protected ISigDiscreteLogic _IntegrityYSig;
		

		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения 
			if (!AssignToSig("BlSig", out _BlSig))
				return false;
            // Получаем и подписываемся на рассылку изменения 
            if (!AssignToSig("IntegrityYSig", out _IntegrityYSig))
                _IntegrityYSig = null;
			
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				//проверяем индикатор нашего направления
				if (_DirObj != null && _DirObj.ObjState.Current == (int)Uni2States.IndObjStates.NotAlight)
				{
					state = (int)Uni2States.StageSignalStates.Canceled;// погашен
				}
				else if (_DirObj != null &&
					(_DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS
					|| _DirObj.ObjState.Current == (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS))
				{
					state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
				}
				else
				{
					//установлено необходимое направление
					if (_KZSig.Off
                        || ((_GSig != null) && _GSig.Off && (_YSig != null) && _YSig.Off && ((_IntegrityRSig != null && _IntegrityRSig.Off) || _IntegritySig.Off))
                        || ((((_GSig != null) && _GSig.On) || ((_YSig != null) && _YSig.On)) && _IntegritySig.Off)
                        || ((_IntegrityYSig != null) && (_GSig != null) && _GSig.Off && (_YSig != null) && _YSig.On && _IntegrityYSig.Off))
						state = (int)Uni2States.StageSignalStates.Fail;
					else
					{
                        if ((_GSig != null) && _GSig.On && _YSig.On)
						{
							if (_BlSig.On)
								state = (int)Uni2States.StageSignalStates.GreenBlink;
							else
								state = (int)Uni2States.StageSignalStates.Open_Green;
						}
                        else if ((_YSig != null) && _YSig.On && _GSig.Off)
						{
							if (_BlSig.On)
								state = (int)Uni2States.StageSignalStates.YellowBlink;
							else
								state = (int)Uni2States.StageSignalStates.Yellow;						
						}
						else
							state = (int)Uni2States.StageSignalStates.Close_Red;
					}
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние проходного светофора. Сигнальное Ж - обязательны
    /// </summary>
    public class ObjStateStageSignal_MPC_R_Y_Yb_G_Cnl_Inf : AbsenceObjState
    {
        /// <summary>
        /// Контроль красного
        /// </summary>
        protected ISigDiscreteLogic _RSig;
        /// <summary>
        /// Контроль зеленого
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Контроль желтого
        /// </summary>
        protected ISigDiscreteLogic _YSig;
        /// <summary>
        /// Контроль желтого мигающего
        /// </summary>
        protected ISigDiscreteLogic _YBlSig;
        /// <summary>
        /// Контроль темного
        /// </summary>
        protected ISigDiscreteLogic _CancelSig;
        /// <summary>
        /// Контроль наличия информации
        /// </summary>
        protected ISigDiscreteLogic _NoInfoSig;
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("YSig", out _YSig))
                return false;
            if (!AssignToSig("CancelSig", out _CancelSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для RSig
            AssignToSig("RSig", out _RSig);
            // Получаем и подписываемся на рассылку изменения для YBlSig
            AssignToSig("YBlSig", out _YBlSig);
            // Получаем и подписываемся на рассылку изменения для GSig
            AssignToSig("GSig", out _GSig);
            // Получаем и подписываемся на рассылку изменения для InfoSig
            AssignToSig("NoInfoSig", out _NoInfoSig);

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if ((_NoInfoSig != null) && _NoInfoSig.On)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                else if ((_RSig != null) && _RSig.On)
                    state = (int)Uni2States.StageSignalStates.Close_Red;
                else if ((_GSig != null) && _GSig.On)
                    state = (int)Uni2States.StageSignalStates.Open_Green;
                else if ((_YBlSig != null) && _YBlSig.On)
                    state = (int)Uni2States.StageSignalStates.YellowBlink;
                else if (_YSig.On)
                    state = (int)Uni2States.StageSignalStates.Yellow;
                else if (_CancelSig.On)
                    state = (int)Uni2States.StageSignalStates.Canceled;
                else
                {
                    if (_RSig == null)  // если сигнала закрытия у объекта нет - считаем дефолтным состоянием "Закрыт"
                        state = (int)Uni2States.StageSignalStates.Close_Red;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора МПЦ. Только перегорание нитей ламп
    /// </summary>
    public class ObjStateStageSignal_MPC_onlyFusion : AbsenceObjState
    {
        /// <summary>
        /// Контроль перегорания
        /// </summary>
        protected ISigDiscreteLogic _FSig;
        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            if (!AssignToSig("FSig", out _FSig))
                return false;
            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_FSig.On)
                    state = (int)Uni2States.StageSignalStates.Fail;
                else
                    state = (int)Uni2States.StageSignalStates.Canceled;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние проходного светофора (контроль закрытия)
	/// </summary>
	public class ObjStateStageSignal_Cl : AbsenceObjState
	{
		/// <summary>
		/// Контроль запрещающего показания сигнальной установки
		/// </summary>
		protected ISigDiscreteLogic _ClSig;
		
		/// <summary>
		/// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OSig
			if (!AssignToSig("ClSig", out _ClSig))
				return false;
		
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_ClSig.On)
                    state = (int)Uni2States.StageSignalStates.Close_Red;// Один красный огонь (закрыт)
                else if (_ClSig.Off)
                    state = (int)Uni2States.StageSignalStates.Canceled;// Погашен
                else
                    state = (int) Uni2States.StageSignalStates.Fail;// отказ
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceSignalState(state);
			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние проходного светофора (контроль закрытия)
    /// </summary>
    public class ObjStateStageSignal_ChDK : AbsenceObjState
    {
        /// <summary>
        /// Контроль запрещающего показания сигнальной установки
        /// </summary>
        protected ISigDiscreteLogic _ClSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OSig
            if (!AssignToSig("ClSig", out _ClSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                uint code = _ClSig.IOSignals[0].GetCurrent;
                if (code == 7)
                    state = (int)Uni2States.StageSignalStates.Close_Red;
                else if (code == 1)
                    state = (int)Uni2States.StageSignalStates.Open_Green;
                else if (code >= 2 && code <= 6)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора АБТЦМ по кодовому сигналу модуля БУСС
    /// </summary>
    public class ObjStateStageSignal_ABTCM_Code : AbsenceObjState
    {
        /// <summary>
        /// Кодовый сигнал модуля БУСС
        /// </summary>
        protected ISigDiscreteLogic _BussSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для BussSig
            if (!AssignToSig("BussSig", out _BussSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_BussSig.IOSignals.Length > 0)
                {
                    uint code = _BussSig.IOSignals[0].GetCurrent;

                    if (code == 0)
                        state = (int)Uni2States.StageSignalStates.Canceled; // Погашен
                    else if (code == 1)
                        state = (int)Uni2States.StageSignalStates.Close_Red; // Один красный огонь (закрыт)
                    else if (code == 2)
                        state = (int)Uni2States.StageSignalStates.Yellow;   // Один желтый огонь
                    else if (code == 3)
                        state = (int)Uni2States.StageSignalStates.Open_Green; // Один зеленый огонь (открыт)
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора АБТЦМ по 4-м сигналам огней
    /// </summary>
    public class ObjStateStageSignal_ABTCM : AbsenceObjState
    {
        /// <summary>
        /// Сигнал "зеленый огонь"
        /// </summary>
        private ISigDiscreteLogic _GSig;
        /// <summary>
        /// Сигнал "желтый огонь"
        /// </summary>
        private ISigDiscreteLogic _YSig;
        /// <summary>
        /// Сигнал "красный огонь"
        /// </summary>
        private ISigDiscreteLogic _RSig;
        /// <summary>
        /// Сигнал "погашен"
        /// </summary>
        private ISigDiscreteLogic _CSig;
        /// <summary>
        /// Кодовый сигнал модуля БУСС
        /// </summary>
        protected ISigDiscreteLogic _BussSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("GSig", out _BussSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("YSig", out _BussSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RSig", out _BussSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("CSig", out _BussSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_BussSig.IOSignals.Length > 0)
                {
                    uint code = _BussSig.IOSignals[0].GetCurrent;

                    if (code == 0)
                        state = (int)Uni2States.StageSignalStates.Canceled; // Погашен
                    else if (code == 1)
                        state = (int)Uni2States.StageSignalStates.Close_Red; // Один красный огонь (закрыт)
                    else if (code == 2)
                        state = (int)Uni2States.StageSignalStates.Yellow;   // Один желтый огонь
                    else if (code == 3)
                        state = (int)Uni2States.StageSignalStates.Open_Green; // Один зеленый огонь (открыт)
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние проходного светофора АСДК по 3-м сигналам целостности
    /// </summary>
    public class ObjStateStageSignal_ASDK : AbsenceObjState
    {
        /// <summary>
        /// Сигнал целостности нити красного огня
        /// </summary>
        private ISigDiscreteLogic _IntegrityRSig;
        /// <summary>
        /// Сигнал целостности нити желтого и зеленого огней
        /// </summary>
        private ISigDiscreteLogic _IntegrityYGSig;
        /// <summary>
        /// Сигнал целостности резервных нитей
        /// </summary>
        private ISigDiscreteLogic _IntegritySig;

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("IntegrityRSig", out _IntegrityRSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("IntegrityYGSig", out _IntegrityYGSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("IntegritySig", out _IntegritySig))
                return false;

            return true;
        }
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_IntegrityRSig.On && _IntegrityYGSig.On && _IntegritySig.On)
                    state = (int)Uni2States.StageSignalStates.Canceled; // Погашен
                else
                    state = (int)Uni2States.StageSignalStates.Fail; // Отказ

            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
	#endregion

	#region Переезд
	/// <summary>
	/// Состояние переезда. 
	/// </summary>
	public class CrossingObjState : AbsenceObjState
	{
		/// <summary>
		/// Контроль закрытия переезда
		/// </summary>
		protected ISigDiscreteLogic _KzSig;
        /// <summary>
        /// Контроль закрытия переезда ДСП
        /// </summary>
        protected ISigDiscreteLogic _KzDspSig;
        /// <summary>
		/// Контроль включения заграждения переезда
		/// </summary>
		protected ISigDiscreteLogic _KzagrSig;
		/// <summary>
		/// Контроль извещения на переезд
		/// </summary>
		protected List<ISigDiscreteLogic> _KPlSigs = new List<ISigDiscreteLogic>();
		/// <summary>
		/// Контроль нормальной работы переезда
		/// </summary>
		protected ISigDiscreteLogic _KNormSig;
		/// <summary>
		/// Контроль неисправности переезда
		/// </summary>
		protected ISigDiscreteLogic _KNSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

            AssignToSig("KzSig", out _KzSig);
            AssignToSig("KzDspSig", out _KzDspSig);
            AssignToSig("KzagrSig", out _KzagrSig);
            for (int i = 0; i <= 8; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                string AddN = string.Empty;
                if (i > 0)
                    AddN = i.ToString();
                if (AssignToSig("KPlSig" + AddN, out tmpSig))
                    _KPlSigs.Add(tmpSig);
            }
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_KzSig != null && _KzSig.On || _KzDspSig != null && _KzDspSig.On)// переезд закрыт
                {
                    if (_KzagrSig != null && _KzagrSig.On)// есть сигнал заграждения и он включен
                        state = (int)Uni2States.CrossingStates.Close_Barrier; //Переезд закрыт и шлагбаум закрыт
                    else
                        state = (int)Uni2States.CrossingStates.Close; //Переезд закрыт
                }
                else if (_KPlSigs.Count > 0)
                {   // переезд открыт
                    bool found = false;
                    foreach (ISigDiscreteLogic KPlSig in _KPlSigs)
                        if (KPlSig.On)
                        {
                            state = (int)Uni2States.CrossingStates.Open_Notification; //Переезд открыт и включено извещение
                            found = true;
                            break;
                        }
                    if (!found)
                        state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
                }
                else
                {
                    if (_KzagrSig != null || _KzDspSig != null || _KPlSigs.Count >= 0)
                        state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
                    else
                        state = (int)Uni2States.GeneralStates.ObjectConserve;
                }
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceCrossingState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние переезда. Подразумевает наличие хотя бы одного из сиганлов.
	/// </summary>
	public class CrossingObjStateDC : AbsenceObjState
	{
		/// <summary>
		/// Контроль закрытия переезда
		/// </summary>
		protected ISigDiscreteLogic _KzSig;
		/// <summary>
		/// Контроль включения заграждения переезда
		/// </summary>
		protected ISigDiscreteLogic _KzagrSig;
		/// <summary>
		/// Контроль извещения на переезд
		/// </summary>
        protected List<ISigDiscreteLogic> _KPlSigs = new List<ISigDiscreteLogic>();
        /// <summary>
        /// Контроль включения сигнализации переезда
        /// </summary>
        protected ISigDiscreteLogic _KSSig;
		
		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;
			bool bRes = false;
			// Получаем и подписываемся на рассылку изменения для KzSig
			if (AssignToSig("KzSig", out _KzSig))
				bRes = true;
			// Получаем и подписываемся на рассылку изменения для _KzagrSig
			if (AssignToSig("KzagrSig", out _KzagrSig))
				bRes = true;
			// Получаем и подписываемся на рассылку изменения для _KPlSig
            for (int i = 0; i <= 8; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                string AddN = string.Empty;
                if (i > 0)
                    AddN = i.ToString();
                if (AssignToSig("KPlSig" + AddN, out tmpSig))
                {
                    _KPlSigs.Add(tmpSig);
                    bRes = true;
                }
            }
			
			/*// Получаем и подписываемся на рассылку изменения для _KNormSig
			if (AssignToSig("KNormSig", out _KNormSig))
				bRes = true;
			// Получаем и подписываемся на рассылку изменения для _KNSig
			if (!AssignToSig("KNSig", out _KNSig))
				bRes = true;*/

            if (AssignToSig("KSSig", out _KSSig))
                bRes = true;

			return bRes;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_KzSig != null)
				{
					if (_KzSig.On) // переезд закрыт
					{
						if (_KzagrSig != null && _KzagrSig.On) // есть сигнал заграждения и он включен
							state = (int) Uni2States.CrossingStates.Close_Barrier; //Переезд закрыт и шлагбаум закрыт
						else
							state = (int) Uni2States.CrossingStates.Close; //Переезд закрыт
					}
                    else if (_KSSig != null && _KSSig.On)
                    {
                        // на случай пешеходного перехода
                        state = (int)Uni2States.CrossingStates.Close;
                    }
                    else
                    {   // переезд открыт
                        bool found = false;
                        foreach (ISigDiscreteLogic KPlSig in _KPlSigs)
                            if (KPlSig.On)
                            {
                                state = (int)Uni2States.CrossingStates.Open_Notification; //Переезд открыт и включено извещение
                                found = true;
                                break;
                            }
                        if (!found)
                            state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
                    }
				}
				else
				{
					if (_KzagrSig != null && _KzagrSig.On) // есть сигнал заграждения и он включен
						state = (int)Uni2States.CrossingStates.Close_Barrier; //Переезд закрыт и шлагбаум закрыт
					else if (_KzagrSig != null && _KzagrSig.Off)
						state = (int)Uni2States.CrossingStates.Close; //Переезд закрыт
                    else if (_KPlSigs.Count > 0)
                    {
                        bool found = false;
                        foreach (ISigDiscreteLogic KPlSig in _KPlSigs)
                            if (KPlSig.On)
                            {
                                state = (int)Uni2States.CrossingStates.Close;
                                found = true;
                                break;
                            }
                        if (!found)
                            state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
                    }
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
				}
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceCrossingState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние переезда. 
	/// </summary>
	public class CrossingObjState_Z_NChI : AbsenceObjState
	{
		/// <summary>
		/// Контроль закрытия переезда
		/// </summary>
		protected ISigDiscreteLogic _KzSig;
		
		/// <summary>
		/// Контроль извещения на переезд
		/// </summary>
		protected ISigDiscreteLogic _KPlNSig;
		/// <summary>
		/// Контроль извещения на переезд
		/// </summary>
		protected ISigDiscreteLogic _KPlChSig;
		/// <summary>
		/// Контроль включения заграждения переезда
		/// </summary>
		protected ISigDiscreteLogic _KzagrSig;
		

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для KzSig
			if (!AssignToSig("KzSig", out _KzSig))
				return false;

			
			// Получаем и подписываемся на рассылку изменения для _KPlSig
			if (!AssignToSig("KPlNSig", out _KPlNSig))
				_KPlNSig = null;

			// Получаем и подписываемся на рассылку изменения для _KPlSig
			if (!AssignToSig("KPlChSig", out _KPlChSig))
				_KPlChSig = null;

			// Получаем и подписываемся на рассылку изменения для _KzagrSig
			AssignToSig("KzagrSig", out _KzagrSig);
			

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_KzSig.On)// переезд закрыт
				{
					if (_KzagrSig != null && _KzagrSig.On)// есть сигнал заграждения и он включен
						state = (int)Uni2States.CrossingStates.Close_Barrier; //Переезд закрыт и шлагбаум закрыт
					else
						state = (int)Uni2States.CrossingStates.Close; //Переезд закрыт
				}
				else
				{
					// переезд открыт
					if (_KPlNSig.On || _KPlChSig.On)
						state = (int)Uni2States.CrossingStates.Open_Notification; //Переезд открыт и включено извещение
					else
						state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceCrossingState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние переезда. Подразумевает наличие хотя бы одного из сиганлов.
    /// </summary>
    public class CrossingObjStateMPC : AbsenceObjState
    {
        /// <summary>
        /// Контроль закрытия или неисправности переезда
        /// </summary>
        protected ISigDiscreteLogic _KznSig;
        /// <summary>
        /// Контроль наличия неисправности переезда
        /// </summary>
        protected ISigDiscreteLogic _KnSig;

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            // Получаем и подписываемся на рассылку изменения для KznSig
            if (!AssignToSig("KznSig", out _KznSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для KfSig
            if (!AssignToSig("KnSig", out _KnSig))
                return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_KznSig.On && _KnSig.Off)
                    state = (int)Uni2States.CrossingStates.Close; //Переезд закрыт
                else if (_KznSig.Off)
                    state = (int)Uni2States.CrossingStates.Open; //Переезд открыт
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceCrossingState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние переезда АБТЦ-М
    /// </summary>
    public class CrossingObjState_ABTCM : AbsenceObjState
    {
        /// <summary>
        /// Кодовый сигнал 'Состояние переезда' модуля БПСС
        /// </summary>
        protected ISigDiscreteLogic _CrossStateSig;
        /// <summary>
        /// Кодовый сигнал 'Извещение переезда' модуля БПСС
        /// </summary>
        protected ISigDiscreteLogic _KPlSig;

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("CrossStateSig", out _CrossStateSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("KPlSig", out _KPlSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_CrossStateSig.On)
                {
                    if (_KPlSig.Off)
                        state = (int)Uni2States.CrossingStates.Open;
                    else if (_KPlSig.On)
                        state = (int)Uni2States.CrossingStates.Open_Notification;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else if (_CrossStateSig.Off)
                {
                    if (_KPlSig.On || _KPlSig.Off)
                        state = (int)Uni2States.CrossingStates.Close;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("CrossingObjState " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceCrossingState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние переезда МЗФ
    /// </summary>
    public class CrossingObjStateMZF : AbsenceObjState
    {
        /// <summary>
        /// Контроль кодового сигнала извещения
        /// </summary>
        protected ISigDiscreteLogic _KPlSig;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("KPlSig", out _KPlSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                uint KPl = _KPlSig.IOSignals[0].GetCurrent;
                if (KPl == 2)
                    state = (int)Uni2States.CrossingStates.Close;
                else if (KPl == 1)
                    state = (int)Uni2States.CrossingStates.Open;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }        
    }

	#endregion

	#region Устройства электропитания
	/// <summary>
	/// Состояние устройства электропитания с одной задачей (питание)
	/// </summary>
	public class PowerSupplyState1Sig_Power : AbsenceObjState
	{
		protected ISigDiscreteLogic _Sig1;

		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
            if (!AssignToSig1orSig2(out _Sig1))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
				else
					state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("PowerSupplyState1Sig " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TracePowerSupplyState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние устройства электропитания с одной задачей (только активность)
	/// </summary>
	public class PowerSupplyState1Sig_Active : PowerSupplyState1Sig_Power
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание и устройство активно
				else
					state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("PowerSupplyState1Sig " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TracePowerSupplyState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние устройства электропитания с одной задачей (0 - есть питание)
    /// </summary>
    public class PowerSupplyState1Sig_0Power : PowerSupplyState1Sig_Power
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание 
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyState1Sig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с одной задачей (0 - есть питание и устройство активно)
    /// </summary>
    public class PowerSupplyState1Sig_0Active : PowerSupplyState1Sig_Power
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание 
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с одной задачей (0 - есть питание, 1 - активность)
    /// </summary>
    public class PowerSupplyState1Sig_0Power_1Active : AbsenceObjState
    {
        protected ISigDiscreteLogic _Sig1;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для RCSig
            if (!AssignToSig1orSig2(out _Sig1))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание и устройство активно
                else
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyState1Sig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние устройства электропитания с двумя задачами (питание, активность)
	/// </summary>
	public class PowerSupplyState2Sig_Power_Active : PowerSupplyState1Sig_Power
	{
		protected ISigDiscreteLogic _Sig2;

		/// <summary>
		/// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			// действия для Sig1
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig2
			if (!AssignToSig("Sig2", out _Sig2))
				return false;

			return true;
		}
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
				else if (_Sig1.On && _Sig2.On)
					state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
				else
					state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TracePowerSupplyState(state);

			SetState(state);

			ResetModify();
		}
	}

    public class PowerSupplyState18Sig_UBP : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// Список аварийных сигналов
        /// </summary>
        protected List<ISigDiscreteLogic> _Sigs_Fail = new List<ISigDiscreteLogic>();

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для _KPlSig
            for (int i = 1; i <= 20; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                if (AssignToSig(string.Format("F{0}", i), out tmpSig))
                    _Sigs_Fail.Add(tmpSig);
            }

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sigs_Fail.Count(s => s.On) > 0)
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                else
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    public class PowerSupplyState_UBP_PN : AbsenceObjState
    {

        protected ISigDiscreteLogic _ASig ;
        protected ISigDiscreteLogic _VBSig;
        protected ISigDiscreteLogic _UInSig;
        protected ISigDiscreteLogic _FaultSig;
        protected ISigDiscreteLogic _EBSig;
        protected ISigDiscreteLogic _BatSig;
        protected ISigDiscreteLogic _BreakSig;
        protected ISigDiscreteLogic _ReadySig;
        protected ISigDiscreteLogic _BufSig;

        public override bool Create(UniObject obj)
        {
            // действия для Sig1
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("ASig", out _ASig))
                return false;
            if (!AssignToSig("VBSig", out _VBSig))
                return false;
            if (!AssignToSig("UInSig", out _UInSig))
                return false;
            if (!AssignToSig("FaultSig", out _FaultSig))
                return false;
            if (!AssignToSig("EBSig", out _EBSig))
                return false;
            if (!AssignToSig("BatSig", out _BatSig))
                return false;
            if (!AssignToSig("BreakSig", out _BreakSig))
                return false;
            if (!AssignToSig("ReadySig", out _ReadySig))
                return false;
            if (!AssignToSig("BufSig", out _BufSig))
                return false;

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_ASig.Off && _VBSig.Off && _UInSig.On && _FaultSig.On && _ReadySig.On && _EBSig.Off && _BatSig.On && _BreakSig.Off && _BufSig.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание и устройство активно
                else if (_ASig.Off && _VBSig.Off && _UInSig.On && _FaultSig.On && _ReadySig.On && _EBSig.Off && _BatSig.On && _BreakSig.Off && _BufSig.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }




    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (питание, активность)
    /// </summary>
    public class PowerSupplyState2Sig_DGA : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                else if (_Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (питание, активность)
    /// </summary>
    public class PowerSupplyState2Sig_UbpMpc : PowerSupplyState2Sig_DGA
    {
        protected ISigDiscreteLogic _Sig3;
        protected ISigDiscreteLogic _Sig4;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            // действия для Sig1
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("Sig3", out _Sig3))
                return false;
            if (!AssignToSig("Sig4", out _Sig4))
                return false;

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig3.On || _Sig4.On)
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                else if (_Sig1.On || _Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание и устройство активно
                else
                    state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (10 - активность, 00 - питание, 01 - нет питания)
    /// </summary>
    public class PowerSupplyState2Sig_00Power_10Active : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
                else if (_Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                else
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (11 - питание, 01 - нет питания, иначе - сбой)
    /// </summary>
    public class PowerSupplyState2Sig_11Power_01NoPower_Fault : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                {
                    if (_Sig1.On)
                        state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                    else
                        state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                }
                else
                    state = (int)Uni2States.PowerSupplyStates.Fault;// Неисправность
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания УЭП с двумя задачами (11 - питание, 01 - сбой, иначе - нет питания)
    /// </summary>
    public class PowerSupplyState2Sig_UEP_11Power_01Fault_NoPower : AbsenceObjState
    {
        protected ISigDiscreteLogic _Sig1;
        protected ISigDiscreteLogic _Sig2;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            AssignToSig("Sig1", out _Sig1);

            if (!AssignToSig("Sig2", out _Sig2))
                return false;

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                {
                    if (_Sig1 != null)
                    {
                        if (_Sig1.On)
                            state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                        else
                            state = (int)Uni2States.PowerSupplyStates.Fault;// Неисправность
                    }
                    else
                        state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                }
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (10 - есть питание, 01 - активность, 00 - нет питания)
    /// </summary>
    public class PowerSupplyState2Sig_10Power_01Active : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (10 - нет питания, 00 - питание, 01 - нет питания)
    /// </summary>
    public class PowerSupplyState2Sig_00Power : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с двумя задачами (10 - нет питания, 00 - питание, 01 - нет питания)
    /// </summary>
    public class PowerSupplyState2Sig_1x_x1_Active : PowerSupplyState2Sig_Power_Active
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с тремя задачами (000 - есть питание, иначе - нет питания)
    /// </summary>
    public class PowerSupplyState3Sig_000Power : AbsenceObjState
    {
        /// <summary>
        /// Сигнал "Контроль отключения батареи"
        /// </summary>
        protected ISigDiscreteLogic _Sig1;
        /// <summary>
        /// Сигнал "Контроль снижения или обрыв батареи"
        /// </summary>
        protected ISigDiscreteLogic _Sig2;
        /// <summary>
        /// Сигнал "Снижение напряжения батареи"
        /// </summary>
        protected ISigDiscreteLogic _Sig3;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2, Sig3 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig1", out _Sig1))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig2
            if (!AssignToSig("Sig2", out _Sig2))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig3
            if (!AssignToSig("Sig3", out _Sig3))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.Off && _Sig2.Off && _Sig3.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с тремя задачами (000 - есть питание, иначе - нет питания)
    /// </summary>
    public class PowerSupplyState_UbpMpc_4Sig : PowerSupplyState3Sig_000Power
    {
        /// <summary>
        /// Сигнал УБП
        /// </summary>
        protected ISigDiscreteLogic _Sig4;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2, Sig3 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig4", out _Sig4))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off && _Sig3.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;
                else if (_Sig1.Off && _Sig2.Off && _Sig3.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower;
                else
                    state = (int)Uni2States.PowerSupplyStates.NoPower;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }


    /// <summary>
    /// Состояние устройства электропитания с одним измерением (U=0 - нет питания, U>0 - есть питание)
    /// </summary>
    public class PowerSupplyStateUSig : AbsenceObjState
	{
		protected ISigAnalog _USig;

		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для USig
            AssignToFirstAnalogSig(out _USig);

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                //if (_USig.Double > 0)
                //    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                //else
                //    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TracePowerSupplyState(state);

			SetState(state);

			ResetModify();
		}

        /// <summary>
        /// Метод переопределен с целью установки законсервированного состояния после восстановления связи (т.к. состояние объекта не зависит от изменения сигналов)
        /// </summary>
        /// <returns>Признак необходимости обновления состояния</returns>
        public override bool IsModify()
        {
            return _controlObj.ObjState.Current == 0 || base.IsModify();
        }
    }
    
    /// <summary>
    /// Состояние устройства электропитания с одним измерением (U=0 - нет питания, U>0 - есть питание) и одной задачей (0 - есть питание)
    /// </summary>
    public class PowerSupplyStateUSigOR1Sig_0Power : AbsenceObjState
    {
        /// <summary>
        /// Аналоговый сигнал
        /// </summary>
        protected ISigAnalog _USig;
        /// <summary>
        /// Дискретный сигнал
        /// </summary>
        protected ISigDiscreteLogic _Sig1;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            /// Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig1", out _Sig1))
            {
                // Получаем и подписываемся на рассылку изменения для USig
                if (!AssignToFirstAnalogOrDiscreteSig(out _USig, out _Sig1))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null)
                {
                    if (_Sig1.On)
                        state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                    else
                        state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                }
                else
                {
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                    //if (_USig.Double > 0)
                    //    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                    //else
                    //    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }

        /// <summary>
        /// Метод переопределен с целью установки законсервированного состояния после восстановления связи (т.к. состояние объекта не зависит от изменения сигналов)
        /// </summary>
        /// <returns>Признак необходимости обновления состояния</returns>
        public override bool IsModify()
        {
            return _controlObj.ObjState.Current == 0 || base.IsModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с одним измерением (U=0 - нет питания, U>0 - есть питание) и одной задачей (1 - есть питание)
    /// </summary>
    public class PowerSupplyStateUSigOR1Sig_Power : AbsenceObjState
	{
        /// <summary>
        /// Аналоговый сигнал
        /// </summary>
        protected ISigAnalog _USig;
        /// <summary>
        /// Дискретный сигнал
        /// </summary>
        protected ISigDiscreteLogic _Sig1;

		/// <summary>
		/// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

            /// Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig1", out _Sig1))
            {
                // Получаем и подписываемся на рассылку изменения для USig
                if (!AssignToFirstAnalogOrDiscreteSig(out _USig, out _Sig1))
                    return false;
            }
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1 != null)
				{
                    if (_Sig1.On)
						state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
					else
						state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания

				}
				else
				{
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                    //if (_USig.Double > 0)
                    //    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                    //else
                    //    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
				}
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TracePowerSupplyState(state);

			SetState(state);

			ResetModify();
		}

        /// <summary>
        /// Метод переопределен с целью установки законсервированного состояния после восстановления связи (т.к. состояние объекта не зависит от изменения сигналов)
        /// </summary>
        /// <returns>Признак необходимости обновления состояния</returns>
        public override bool IsModify()
        {
            return _controlObj.ObjState.Current == 0 || base.IsModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания с одним измерением (U=0 - нет питания, U>0 - есть питание) и одной задачей (1 - есть питание)
    /// </summary>
    public class PowerSupplyState_IBP : AbsenceObjState
    {
        /// <summary>
        /// Унифиццированный параметр
        /// </summary>
        protected UniObjData _uniObjData;
        /// <summary>
        /// Дискретный сигнал
        /// </summary>
        protected ISigDiscreteLogic _DSig;

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            /// Получаем и подписываемся на рассылку изменения для Sig1
            AssignToSig("Sig1", out _DSig);
            
            // Получаем и подписываемся на рассылку изменения для USig
            AssignToUniObjData("Value", out _uniObjData);

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.PowerSupplyStates.InPower;  // Есть питание
            try
            {
                if (_DSig != null && _DSig is ControlSubTask)
                {
                    uint code = (_DSig as ControlSubTask).State;

                    if (_uniObjData != null)
                        _uniObjData.SetParamState((float)code);
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }

    }

    /// <summary>
    /// Состояние устройства электропитания СЗИЦ-Д
    /// </summary>
    public class PowerSupplyStateSZICD : AbsenceObjState
    {
        /// <summary>
        /// Дискретный сигнал - "Ошибка измерения"
        /// </summary>
        protected ISigDiscreteLogic _ErrMeasureSig;
        /// <summary>
        /// Дискретный сигнал - "Мигание символа 'минус'"
        /// </summary>
        protected ISigDiscreteLogic _MinusBlinkSig;
        /// <summary>
        /// Дискретный сигнал - "Состояние ускоряющей перемычки"
        /// </summary>
        protected ISigDiscreteLogic _JumperSig;
        /// <summary>
        /// Дискретный сигнал - "Состояние нажатия кнопки"
        /// </summary>
        protected ISigDiscreteLogic _ButtonSig;
        /// <summary>
        /// Дискретный сигнал - "Снижение напряжения"
        /// </summary>
        protected ISigDiscreteLogic _UnderVoltageSig;
        /// <summary>
        /// Дискретный сигнал - "Завышение напряжения"
        /// </summary>
        protected ISigDiscreteLogic _OverVoltageSig;
        /// <summary>
        /// Дискретный сигнал - "Нестабильность напряжения"
        /// </summary>
        protected ISigDiscreteLogic _UnstableSig;
        /// <summary>
        /// Дискретный сигнал - "Срабатывание сигнализатора СЗИЦ-Д"
        /// </summary>
        protected ISigDiscreteLogic _GroundSig;
        /// <summary>
        /// Дискретный сигнал - "Режим работы"
        /// </summary>
        protected ISigDiscreteLogic _SzicdModeSig;
        /// <summary>
        /// Дискретный сигнал - "Диапазон измеренного Rиз СЗИЦ-Д"
        /// </summary>
        protected ISigDiscreteLogic _SzicdSymbSig;

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("ErrMeasure", out _ErrMeasureSig))
                return false;
            if (!AssignToSig("MinusBlink", out _MinusBlinkSig))
                return false;
            if (!AssignToSig("Jumper", out _JumperSig))
                return false;
            if (!AssignToSig("Button", out _ButtonSig))
                return false;
            if (!AssignToSig("OverVoltage", out _OverVoltageSig))
                return false;
            if (!AssignToSig("UnderVoltage", out _UnderVoltageSig))
                return false;
            if (!AssignToSig("Unstable", out _UnstableSig))
                return false;
            if (!AssignToSig("Ground", out _GroundSig))
                return false;
            if (!AssignToSig("SzicdMode", out _SzicdModeSig))
                return false;
            if (!AssignToSig("SzicdSymb", out _SzicdSymbSig))
                return false;

            return true;
        }

        /// <summary>
        /// Обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SzicdModeSig.IOSignals.Length > 0 && _SzicdSymbSig.IOSignals.Length > 0)
                {
                    uint mode = _SzicdModeSig.IOSignals[0].GetCurrent;
                    uint symb = _SzicdSymbSig.IOSignals[0].GetCurrent;

                    if (symb == 15 && mode == 15
                        && _GroundSig.On && _UnstableSig.On && _UnderVoltageSig.On && _OverVoltageSig.On
                        && _ButtonSig.On && _JumperSig.On && _MinusBlinkSig.On && _ErrMeasureSig.On)
                    {
                        state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                    }
                    else if (mode >= 0 && mode <= 6)
                    {
                        if (_GroundSig.On)
                            state = (int)Uni2States.PowerSupplyStates.NoPower;
                        else
                            state = (int)Uni2States.PowerSupplyStates.InPower;
                    }
                    #region Первая версия требований к алгоритму
                    //{
                    //    if (_GroundSig.Off && _UnstableSig.Off && _UnderVoltageSig.Off && _OverVoltageSig.Off && _ButtonSig.Off && _JumperSig.Off && _MinusBlinkSig.Off && _ErrMeasureSig.Off)
                    //        state = (int)Uni2States.PowerSupplyStates.InPower;
                    //    else if (_GroundSig.On && _UnstableSig.Off && _UnderVoltageSig.Off && _OverVoltageSig.Off)
                    //        state = (int)Uni2States.PowerSupplyStates.NoPower;
                    //    else if (mode >= 2)
                    //    {
                    //        if (_GroundSig.Off &&
                    //            (_UnstableSig.On
                    //            || _UnstableSig.Off &&
                    //                (_OverVoltageSig.On && _UnderVoltageSig.Off || _OverVoltageSig.Off && _UnderVoltageSig.On)
                    //            || _UnstableSig.Off &&
                    //                _OverVoltageSig.Off && _UnderVoltageSig.Off && _ButtonSig.Off && _JumperSig.Off && _MinusBlinkSig.Off && _ErrMeasureSig.Off))
                    //            state = (int)Uni2States.PowerSupplyStates.InPower;
                    //        else
                    //            state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                    //    }
                    //}
                    #endregion
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания СЗИЦ-Д
    /// </summary>
    public class PowerSupplyStateSZICD_AnySig_NoPower : AbsenceObjState
    {
        /// <summary>
        /// Контроль извещения на переезд
        /// </summary>
        protected List<ISigDiscreteLogic> _Sigs = new List<ISigDiscreteLogic>();

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            for (int i = 0; i <= 100; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                string AddN = string.Empty;
                if (i > 0)
                    AddN = i.ToString();
                if (AssignToSig("Sig" + AddN, out tmpSig))
                    _Sigs.Add(tmpSig);
            }

            return true;
        }

        /// <summary>
        /// Обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sigs.Count > 0)
                {
                    bool b = false;
                    foreach (ISigDiscreteLogic Sig in _Sigs)
                        if (Sig.On)
                        {
                            state = (int)Uni2States.PowerSupplyStates.NoPower;
                            b = true;
                            break;
                        }
                    if (!b)
                        state = (int)Uni2States.PowerSupplyStates.InPower;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
                state = (int) Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние устройства электропитания ШУ ДГА
    /// </summary>
    public class PowerSupplyStateSHUDGA : AbsenceObjState
    {
        /// <summary>
        /// Дискретный сигнал - "Состояние ДГА"
        /// </summary>
        protected ISigDiscreteLogic _StateCode;
        /// <summary>
        /// Дискретные аварийные сигналы
        /// </summary>
        protected ISigDiscreteLogic _F0Sig;
        protected ISigDiscreteLogic _F1Sig;
        protected ISigDiscreteLogic _F2Sig;
        protected ISigDiscreteLogic _F3Sig;
        protected ISigDiscreteLogic _F4Sig;
        protected ISigDiscreteLogic _F5Sig;
        protected ISigDiscreteLogic _F6Sig;
        protected ISigDiscreteLogic _F7Sig;
        protected ISigDiscreteLogic _F8Sig;
        protected ISigDiscreteLogic _F10Sig;
        protected ISigDiscreteLogic _F11Sig;
        protected ISigDiscreteLogic _F12Sig;
        protected ISigDiscreteLogic _F13Sig;
        protected ISigDiscreteLogic _F17Sig;
        protected ISigDiscreteLogic _F18Sig;
        protected ISigDiscreteLogic _F19Sig;
        protected ISigDiscreteLogic _F20Sig;

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToSig("StateCode", out _StateCode))
                return false;
            if (!AssignToSig("F0", out _F0Sig))
                return false;
            if (!AssignToSig("F1", out _F1Sig))
                return false;
            if (!AssignToSig("F2", out _F2Sig))
                return false;
            if (!AssignToSig("F3", out _F3Sig))
                return false;
            if (!AssignToSig("F4", out _F4Sig))
                return false;
            if (!AssignToSig("F5", out _F5Sig))
                return false;
            if (!AssignToSig("F6", out _F6Sig))
                return false;
            if (!AssignToSig("F7", out _F7Sig))
                return false;
            if (!AssignToSig("F8", out _F8Sig))
                return false;
            if (!AssignToSig("F10", out _F10Sig))
                return false;
            if (!AssignToSig("F11", out _F11Sig))
                return false;
            if (!AssignToSig("F12", out _F12Sig))
                return false;
            if (!AssignToSig("F13", out _F13Sig))
                return false;
            if (!AssignToSig("F17", out _F17Sig))
                return false;
            if (!AssignToSig("F18", out _F18Sig))
                return false;
            if (!AssignToSig("F19", out _F19Sig))
                return false;
            if (!AssignToSig("F20", out _F20Sig))
                return false;

            return true;
        }

        /// <summary>
        /// Обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_StateCode.IOSignals.Length > 0)
                {
                    uint code = _StateCode.IOSignals[0].GetCurrent;

                    if (_F0Sig.On || _F1Sig.On || _F2Sig.On || _F3Sig.On || _F4Sig.On || _F5Sig.On || _F6Sig.On || _F7Sig.On || _F8Sig.On
                          || _F10Sig.On || _F11Sig.On || _F12Sig.On || _F13Sig.On || _F17Sig.On || _F18Sig.On || _F19Sig.On || _F20Sig.On)
                        state = (int)Uni2States.PowerSupplyStates.NoPower;
                    else if (code == 0)
                        state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;
                    else
                        state = (int)Uni2States.PowerSupplyStates.InPower;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("PowerSupplyStateUSig " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }

    }

    /// <summary>
    /// Состояние устройства электропитания с четырьмя сигналами
    /// </summary>
    public class PowerSupplyState_Ubp_4sig : AbsenceObjState
    {
        /// <summary>
        /// Сигнал "Контроль отсутствия перебоя электросети"
        /// </summary>
        protected ISigDiscreteLogic _Sig1;
        /// <summary>
        /// Сигнал "Контроль отсутствия останова работы"
        /// </summary>
        protected ISigDiscreteLogic _Sig2;
        /// <summary>
        /// Сигнал "Контроль отсутствия нагрузки на байпасе"
        /// </summary>
        protected ISigDiscreteLogic _Sig4;
        /// <summary>
        /// Сигнал "Контроль включения ручного байпаса ШОУ"
        /// </summary>
        protected ISigDiscreteLogic _Sig5;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2, Sig4, Sig5 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig1", out _Sig1))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig2
            if (!AssignToSig("Sig2", out _Sig2))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig4
            if (!AssignToSig("Sig4", out _Sig4))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig5
            if (!AssignToSig("Sig5", out _Sig5))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.Off)
                    state = (int)Uni2States.PowerSupplyStates.Fault; // Неисправность
                else if (_Sig1.On && _Sig2.Off || _Sig1.On && _Sig2.On && _Sig4.Off)
                    state = (int)Uni2States.PowerSupplyStates.NoPower;// Нет питания
                else if (_Sig1.On && _Sig2.On && _Sig4.On && _Sig5.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower;// Есть питание
                else if (_Sig1.On && _Sig2.On && _Sig4.On && _Sig5.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate;// Есть питание и устройство активно
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }


    /// <summary>
    /// Состояние устройства электропитания с четырьмя задачами (есть питание, есть питание и уст-во активно, нет питания, неисправность)
    /// </summary>
    public class PowerSupplyState_10Power_11Active_0NoPower_Fault : PowerSupplyState2Sig_11Power_01NoPower_Fault
    {
        protected ISigDiscreteLogic _PNFSig;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для PNFSig
            if (!AssignToSig("PNFSig", out _PNFSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_PNFSig.On)
                    state = (int)Uni2States.PowerSupplyStates.Fault;// Неисправность
                else if (_Sig1.Off)
                    state = (int)Uni2States.PowerSupplyStates.NoPower;//Нет питания
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.PowerSupplyStates.InPower; // Есть питание
                else if (_Sig1.On && _Sig2.On)
                    state = (int)Uni2States.PowerSupplyStates.InPowerAndActivate; // Есть питание и устройство активно
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TracePowerSupplyState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор горит белым цветом / Индикатор мигает белым цветом / Индикатор мигает белым цветом
    /// </summary>
    public class IndObjState2Sig_10GreenRed_10WhiteNot : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
                else if (_Sig1.Off)
                    state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
                else if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
                else
                {
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит 
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    #endregion

    #region Индикатор состояния устройства

    /// <summary>
    /// Состояние индикатора без сигналов - Не горит
    /// </summary>
    public class IndObjState0Sig : AbsenceObjState
	{
		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.IndObjStates.NotAlight;
			
			TraceIndicatorState(state);

			SetState(state);
		}
	}

	#region Индикаторы с одним сигналом

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1Red : AbsenceObjState
	{
        /// <summary>
        /// Сигнал (1 - горит, 0 - не горит)
        /// </summary>
		protected ISigDiscreteLogic _Sig1;

		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для RCSig
			if (!AssignToSig1orSig2(out _Sig1))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.RedAlight;	// Индикатор горит красным цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;	// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState4Sig_or1Yellow : AbsenceObjState
	{
        /// <summary>
        /// Сигнал (1 - горит, 0 - не горит)
        /// </summary>
		protected ISigDiscreteLogic _Sig1;
        /// <summary>
        /// Сигнал (1 - горит, 0 - не горит)
        /// </summary>
        protected ISigDiscreteLogic _Sig2;
        /// <summary>
        /// Сигнал (1 - горит, 0 - не горит)
        /// </summary>
        protected ISigDiscreteLogic _Sig3;
        /// <summary>
        /// Сигнал (1 - горит, 0 - не горит)
        /// </summary>
        protected ISigDiscreteLogic _Sig4;

		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("Sig1", out _Sig1))
				_Sig1 = null;
			if (!AssignToSig("Sig2", out _Sig2))
				_Sig2 = null;
			if (!AssignToSig("Sig3", out _Sig3))
				_Sig3 = null;
			if (!AssignToSig("Sig4", out _Sig4))
				_Sig4 = null;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// Если в Sig1 импульс, то устанавливаем значение 'Индикатор горит желтым цветом'
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_Sig1 != null && _Sig1.On
                    || _Sig2 != null && _Sig2.On
                    || _Sig3 != null && _Sig3.On
                    || _Sig4 != null && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.YellowAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит зеленым цветом (1) \ Не горит (0)
    /// </summary>
    public class IndObjState4Sig_or1Green : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Если в Sig1 импульс, то устанавливаем значение 'Индикатор горит желтым цветом'
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.On
                    || _Sig2 != null && _Sig2.On
                    || _Sig3 != null && _Sig3.On
                    || _Sig4 != null && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (1) \ Не горит (0)
    /// </summary>
    public class IndObjState4Sig_or1Red : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Если в Sig1 импульс, то устанавливаем значение 'Индикатор горит желтым цветом'
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.On
                    || _Sig2 != null && _Sig2.On
                    || _Sig3 != null && _Sig3.On
                    || _Sig4 != null && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (1) \ Не горит (0)
    /// </summary>
    public class IndObjState4Sig_or0RedB : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Если в Sig1 импульс, то устанавливаем значение 'Индикатор горит желтым цветом'
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.Off
                    || _Sig2 != null && _Sig2.Off
                    || _Sig3 != null && _Sig3.Off
                    || _Sig4 != null && _Sig4.Off)
                    state = (int)Uni2States.IndObjStates.RedBlink;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (1) \ Не горит (0)
    /// </summary>
    public class IndObjState4Sig_StageBusy : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            
            if (_Sig1 == null || _Sig2 == null)
                return false;

            if (_Sig3 == null)
                if (!AssignToSig("MainModeTrSig", out _Sig3))
                    return false;
            if (_Sig4 == null)
                if (!AssignToSig("MainModeRecSig", out _Sig4))
                    return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// Если в Sig1 импульс, то устанавливаем значение 'Индикатор горит желтым цветом'
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig3.On || _Sig2.On && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else
                    state = (int)Uni2States.IndObjStates.WhiteAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с 4 сигналами. Индикатор горит/моргает красным, горит/моргает белым \ Не горит (0 всех сигналов)
    /// </summary>
    public class IndObjState4Sig_RedLB_WhiteLB : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.On)
                    state = (int)Uni2States.IndObjStates.RedAlight; // Индикатор горит зеленым
                else if (_Sig2 != null && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedBlink; // Индикатор мигает зеленым
                else if (_Sig3 != null && _Sig3.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight; // Индикатор горит белым
                else if (_Sig4 != null && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.WhiteBlink; // Индикатор мигает белым
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с 4 сигналами. Индикатор горит/моргает зеленым, горит/моргает белым \ Не горит (0 всех сигналов)
    /// </summary>
    public class IndObjState4Sig_GreenLB_WhiteLB : IndObjState4Sig_or1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight; // Индикатор горит зеленым
                else if (_Sig2 != null && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.GreenBlink; // Индикатор мигает зеленым
                else if (_Sig3 != null && _Sig3.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight; // Индикатор горит белым
                else if (_Sig4 != null && _Sig4.On)
                    state = (int)Uni2States.IndObjStates.WhiteBlink; // Индикатор мигает белым
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0Red : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_RedB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает красным цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1RedB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.RedBlink;// Индикатор мигает красным цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_RedB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает красным цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0RedB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.RedBlink;// Индикатор мигает красным цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_RedB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит белым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1White : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
                else if (_Sig1.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else
                    state = (int)Uni2States.IndObjStates.WhiteBlink;// Мигает белым
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_White " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит белым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0White : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_White " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает белым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1WhiteB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.WhiteBlink;// Индикатор мигает белым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_WhiteB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает белым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0WhiteB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.WhiteBlink;// Индикатор мигает белым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_WhiteB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит зеленым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1Green : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Green " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит зеленым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0Green : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Green " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает зеленым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1GreenB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.GreenBlink;// Индикатор мигает зеленым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_GreenB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает зеленым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0GreenB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.GreenBlink;// Индикатор мигает зеленым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_GreenB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1Yellow : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Yellow " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (1) \ Не горит (0)
    /// </summary>
    public class IndObjState1Sig_TSh : IndObjState1Sig_1Yellow
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                string fName = "D:\\TSh_Trace.txt";
                System.IO.FileInfo fi = new System.IO.FileInfo(fName);
                if (!fi.Exists)
                {
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(fName);
                    sw.WriteLine("TSh trace");
                    try
                    {
                        sw.WriteLine("Sigs = " + _Sig1.IOSignals.Length);
                        sw.WriteLine("Sig[0] is null = " + _Sig1.IOSignals[0] == null ? "yes" : "no");
                        sw.WriteLine("Sig[0] SignalName = " + _Sig1.IOSignals[0].SignalName);
                        sw.WriteLine("Sig[0] FullName = " + _Sig1.IOSignals[0].FullName);
                        sw.WriteLine("Sig[0] Comment = " + _Sig1.IOSignals[0].Comment);
                        sw.WriteLine("Sig[0] SignalState = " + _Sig1.IOSignals[0].SignalState);
                        sw.WriteLine("Sig[0] StringState = " + _Sig1.IOSignals[0].StringState);
                        sw.WriteLine("Sig[0] Current State = " + _Sig1.IOSignals[0].GetCurrent);
                        sw.WriteLine("Sig[0] (int) Current State = " + _Sig1.IOSignals[0].GetCurrent);
                        sw.WriteLine("Sig[0] StateBitCount = " + _Sig1.IOSignals[0].StateBitCount.ToString());
                        sw.WriteLine("Sig[0] = " + _Sig1.IOSignals[0].State.ToString());
                        sw.WriteLine("SigOn = " + _Sig1.On.ToString());
                        sw.WriteLine("SigOff = " + _Sig1.Off.ToString());
                    }
                    catch (Exception ex)
                    {
                        sw.WriteLine(ex.Message);
                        sw.WriteLine(ex.StackTrace);
                    }
                    sw.Flush();
                    sw.Close();
                }

                if (_Sig1.On || (_Sig1.IOSignals.Length > 0 && _Sig1.IOSignals[0].State > 0))
                    state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым цветом
                else if (_Sig1.Off || (_Sig1.IOSignals.Length > 0 && _Sig1.IOSignals[0].State <= 0))
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Yellow " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();

            /*
            string fName = "D:\\Code_Trace.txt";
            System.IO.FileInfo fi = new System.IO.FileInfo(fName);
            if (!fi.Exists)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(fName);
                sw.WriteLine("Code_Trace");
                try
                {
                    foreach (IOSignal sig in _CodeSig.IOSignals)
                    {
                        sw.WriteLine("Signal  ->  " + sig.FullName);
                    }
                    sw.WriteLine("Sigs = " + _Sig1.IOSignals.Length);
                    sw.WriteLine("Sig[0] is null = " + _Sig1.IOSignals[0] == null ? "yes" : "no");
                    sw.WriteLine("Sig[0] SignalName = " + _Sig1.IOSignals[0].SignalName);
                    sw.WriteLine("Sig[0] FullName = " + _Sig1.IOSignals[0].FullName);
                    sw.WriteLine("Sig[0] Comment = " + _Sig1.IOSignals[0].Comment);
                    sw.WriteLine("Sig[0] SignalState = " + _Sig1.IOSignals[0].SignalState);
                    sw.WriteLine("Sig[0] StringState = " + _Sig1.IOSignals[0].StringState);
                    sw.WriteLine("Sig[0] Current State = " + _Sig1.IOSignals[0].GetCurrent);
                    sw.WriteLine("Sig[0] (int) Current State = " + _Sig1.IOSignals[0].GetCurrent);
                    sw.WriteLine("Sig[0] StateBitCount = " + _Sig1.IOSignals[0].StateBitCount.ToString());
                    sw.WriteLine("Sig[0] = " + _Sig1.IOSignals[0].State.ToString());
                    sw.WriteLine("SigOn = " + _Sig1.On.ToString());
                    sw.WriteLine("SigOff = " + _Sig1.Off.ToString());
                }
                catch (Exception ex)
                {
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(ex.StackTrace);
                }
                sw.Flush();
                sw.Close();
            }
            */
        }
    }
    /// <summary>
    /// Состояние индикатора кодирования
    /// </summary>
    public class IndObjState1Sig_Code : AbsenceObjState
    {
        /// <summary>
        /// Сигнал кодирования (дискретный многоуровневый)
        /// </summary>
        protected ISigDiscreteLogic _CodeSig;

        /// <summary>
        /// Создание состояния. Получаем сигнал CodeSig и подписываемся на рассылку изменения его данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для RCSig
            if (!AssignToSig("CodeSig", out _CodeSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_CodeSig.IOSignals.Length > 0)
                {
					uint code = _CodeSig.IOSignals[0].GetCurrent;
					if (code == 0)
						state = (int)Uni2States.GeneralStates.ObjectConserve;
					else if (code == 2 || code == 17)
						state = (int)Uni2States.IndObjStates.GreenAlight;
					else if ((code >= 3 && code <= 10) || (code >= 18 && code <= 24))
						state = (int)Uni2States.IndObjStates.GreenBlink;
					else if (code == 12 || code == 27)
						state = (int)Uni2States.IndObjStates.YellowAlight;
					else if (code == 13 || code == 14 || code == 28)
						state = (int)Uni2States.IndObjStates.YellowBlink;
					else if (code == 15 || code == 29)
						state = (int)Uni2States.IndObjStates.RedYellowAlight;
					else if (code == 30 || code == 31)
						state = (int)Uni2States.IndObjStates.WhiteAlight;
					else
						state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;

					#region Расшифровки кодов
					/*
					0 - В КИ нет данных"
					1 - Ошибка длительности цикла"
					2 - Код 'З' КПТШ-5"
					3 - Код 'З' КПТШ-5 c ошибкой длительности паузы 1"
					4 - Код 'З' КПТШ-5 с ошибкой длительности паузы 2"
					5 - Код 'З' КПТШ-5 с ошибкой длительности паузы 1 и паузы 2"
					6 - Код 'З' КПТШ-5 с ошибкой разности длительностей паузы 1  и паузы 2"
					7 - Код 'З' КПТШ-5 с ошибкой длительности паузы 1 и с ошибкой разности длительностей паузы 1  и паузы 2"
					8 - Код 'З' КПТШ-5 с ошибкой длительности паузы 2 и с ошибкой разности длительностей паузы 1  и паузы 2"
					9 - Код 'З' КПТШ-5 с ошибкой длительности паузы 1, паузы 2 и с ошибкой разности длительностей паузы 1  и паузы 2"
					10 - Код 'З' с ошибкой длительности цикла (длительность пауз 1 и 2 в норме)"
					11 - Резерв"
					12 - Код 'Ж' КПТШ-5"
					13 - Код 'Ж' КПТШ-5 с ошибкой длительности паузы 1"
					14 - Код 'Ж' КПТШ-5 с ошибкой длительности цикла (длительность паузы 1 в норме)"
					15 - Код 'КЖ' КПТШ-5"
					16 - Резерв"
					17 - Код 'З' КПТШ-7"
					18 - Код 'З' КПТШ-7 c ошибкой длительности паузы 1"
					19 - Код 'З' КПТШ-7 с ошибкой длительности паузы 2"
					20 - Код 'З' КПТШ-7 с ошибкой длительности паузы 1 и паузы 2"
					21 - Код 'З' КПТШ-7 с ошибкой разности длительностей паузы 1  и паузы 2"
					22 - Код 'З' КПТШ-7 с ошибкой длительности паузы 1 и с ошибкой разности длительностей паузы 1  и паузы 2"
					23 - Код 'З' КПТШ-7 с ошибкой длительности паузы 2 и с ошибкой разности длительностей паузы 1  и паузы 2"
					24 - Код 'З' КПТШ-7 с ошибкой длительности паузы 1, паузы 2 и с ошибкой разности длительностей паузы 1  и паузы 2"
					25 - Включение питания, код не определен"
					26 - Ошибка количества длительностей"
					27 - Код 'Ж' КПТШ-7"
					28 - Код 'Ж' КПТШ-7 с ошибкой длительности паузы 1"
					29 - Код 'КЖ' КПТШ-7"
					30 - Постоянный '0' (более 2х секунд)"
					31 - Постоянная '1' (более 2х секунд)"
					*/
					#endregion
                }
                else
					state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    
    /// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (1) \ зеленым (0)
	/// </summary>
	public class IndObjState1Sig_1Yellow0Green : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым цветом
				else if (_Sig1.Off)
                    state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Yellow " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (1) \ зеленым (0)
    /// </summary>
    public class IndObjState1Sig_1Green0Yellow : IndObjState1Sig_1Red
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
                else if (_Sig1.Off)
                    state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым цветом
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Yellow " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит желтым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0Yellow : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Yellow " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает желтым цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1YellowB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.YellowBlink;// Индикатор мигает желтым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_YellowB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает желтым цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0YellowB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.YellowBlink;// Индикатор мигает желтым цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_YellowB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит синий цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1Blue : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.BlueAlight;// Индикатор горит синий цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Blue " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит синий цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0Blue : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.BlueAlight;// Индикатор горит синий цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_Blue " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает синий цветом (1) \ Не горит (0)
	/// </summary>
	public class IndObjState1Sig_1BlueB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.BlueBlink;// Индикатор мигает синий цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_BlueB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает синий цветом (0) \ Не горит (1)
	/// </summary>
	public class IndObjState1Sig_0BlueB : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.BlueBlink;// Индикатор мигает синий цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_BlueB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает красным цветом (0) \ Горит белым (1)
	/// </summary>
	public class IndObjState1Sig_0RedB_1WhiteL : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.RedBlink;// Индикатор мигает красным цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_0RedB_1WhiteL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (0) \ Горит белым (1)
	/// </summary>
	public class IndObjState1Sig_0RedL_1WhiteL : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
				else
					state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор мигает красным цветом
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_0Red_1WhiteL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор горит красным цветом (1) \ Горит белым (0)
	/// </summary>
	public class IndObjState1Sig_1RedL_0WhiteL : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
				else
                    state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_0Red_1WhiteL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с одним сигналом. Индикатор мигает красным цветом (0)\ Индикатор горит зеленым цветом (1)
	/// </summary>
	public class IndObjState1Sig_0RedB_1GreenL : IndObjState1Sig_1Red
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On)
					state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
				else if (_Sig1.Off)
					state = (int)Uni2States.IndObjStates.RedBlink;// Индикатор мигает красным цветом
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState1Sig_0RedB_1GreenL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние индикатора с одним сигналом. Индикатор мигает красным цветом (0)\ Индикатор горит зеленым цветом (1)
    /// </summary>
    public class IndObjState1Sig_0RedL_1GreenL : IndObjState1Sig_1Red
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым цветом
                else if (_Sig1.Off)
                    state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// Неопределенное состояние (несоответствие ТС)
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_0RedB_1GreenL " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    #endregion

	#region Индикаторы с двумя сигналами
	/// <summary>
	/// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит красным цветом / Индикатор мигает красным цветом
	/// </summary>
	public class IndObjState2Sig_Red_LB : AbsenceObjState
	{
        /// <summary>
        /// Сигнал (1 - горит)
        /// </summary>
        protected ISigDiscreteLogic _Sig1;
        /// <summary>
        /// Сигнал (1 - мигает)
        /// </summary>
        protected ISigDiscreteLogic _Sig2;

		/// <summary>
		/// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig1
			if (!AssignToSig("Sig1", out _Sig1))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig2
			if (!AssignToSig("Sig2", out _Sig2))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.RedBlink;// Индикатор мигает красным цветом
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_RedLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит красным цветом
    /// </summary>
    public class IndObjState2SigOR_Red_L : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On || _Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedAlight; // Индикатор горит красным цветом
                else
                    state = (int)Uni2States.IndObjStates.NotAlight; // Индикатор не горит
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит красным цветом
    /// </summary>
    public class IndObjState2SigOR_Red_B : IndObjState2SigOR_Red_L
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On || _Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedBlink; // Индикатор мигает красным цветом
                else
                    state = (int)Uni2States.IndObjStates.NotAlight; // Индикатор не горит
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }


	/// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор горит белым цветом / Индикатор мигает белым цветом / Индикатор мигает белым цветом
	/// </summary>
	public class IndObjState2Sig_White_LB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.WhiteBlink;// Индикатор мигает белым цветом
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор горит белым цветом / Индикатор мигает белым цветом / Индикатор горит красным цветом
    /// </summary>

    public class IndObjState2Sig_White_LB_Red:IndObjState2Sig_White_LB
    {
        /// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// </summary>
		public override void UpdateObjectState()
		{
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.WhiteAlight;// Индикатор горит белым цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.WhiteBlink;// Индикатор мигает белым цветом
				else
					state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
    }

	/// <summary>
	/// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит / Отсутствие состояния
	/// </summary>
	public class IndObjState2Sig_11Red_x0Unknown : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.On)
					state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
				else if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;// Отсутствие состояния
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит / Отсутствие состояния
    /// </summary>
    public class IndObjState2Sig_10Red_x1Unknown : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;// Отсутствие состояния
                else
                {
                    if (_Sig1.On)
                        state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
                    else
                        state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит / Отсутствие состояния
    /// </summary>
    public class IndObjState2Sig_00Red_x1Unknown : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;// Отсутствие состояния
                else
                {
                    if (_Sig1.On)
                        state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                    else
                        state = (int)Uni2States.IndObjStates.RedAlight;// Индикатор горит красным цветом
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит / Отсутствие состояния
    /// </summary>
    public class IndObjState2Sig_10RedB : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.RedBlink;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит зеленым  цветом / Индикатор мигает зеленым  цветом
	/// </summary>
	public class IndObjState2Sig_Green_LB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.GreenAlight;// Индикатор горит зеленым  цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.GreenBlink;// Индикатор мигает зеленым  цветом
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_GreenLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит желтым  цветом / Индикатор мигает желтым  цветом
	/// </summary>
	public class IndObjState2Sig_Yellow_LB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым  цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.YellowBlink;// Индикатор мигает желтым  цветом
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    public class IndObjState2Sig_SAUT : IndObjState2Sig_Yellow_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.GreenAlight;
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.NotAlight;
                else
                    state = (int)Uni2States.IndObjStates.RedAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с тремя сигналоми. 01* - не горит; 00* - мигает красным; 1*1 - горит желтым; 1*0 - горит красным
	/// </summary>
	public class IndObjState3Sig_00_RBl_1_1Y_1_0R : AbsenceObjState
	{
		protected ISigDiscreteLogic _Sig1;
		protected ISigDiscreteLogic _Sig2;
		protected ISigDiscreteLogic _Sig3;

		/// <summary>
		/// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig1
			if (!AssignToSig("Sig1", out _Sig1))
				return false;

			// Получаем и подписываемся на рассылку изменения для Sig2
			if (!AssignToSig("Sig2", out _Sig2))
				return false;
			// Получаем и подписываемся на рассылку изменения для Sig3
			if (!AssignToSig("Sig3", out _Sig3))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig3.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;     // включен и неисправен
				else if (_Sig1.On && _Sig3.On)
                    state = (int)Uni2States.IndObjStates.YellowAlight;  // включен и исправен
				else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.RedBlink;      // выключен и неисправен
				else
					state = (int)Uni2States.IndObjStates.NotAlight;     // Индикатор не горит
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с тримя сигналоми. xx1 - миг красным; x10 - желтый; 100 - зеленый; 000 - не горит
	/// </summary>
	public class IndObjState3Sig_xx1RBl_x10Y_100G : AbsenceObjState
	{
		protected ISigDiscreteLogic _Sig1;
		protected ISigDiscreteLogic _Sig2;
		protected ISigDiscreteLogic _Sig3;

		/// <summary>
		/// Создание состояния. Получаем сигналы Sig1, Sig2 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

            // Получаем и подписываемся на рассылку изменения для Sig2
            AssignToSig("Sig1", out _Sig1);
			// Получаем и подписываемся на рассылку изменения для Sig2
            AssignToSig("Sig2", out _Sig2);
			// Получаем и подписываемся на рассылку изменения для Sig3
            AssignToSig("Sig3", out _Sig3);

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_Sig3 != null && _Sig3.On)
					state = (int)Uni2States.IndObjStates.RedBlink;
                else if (_Sig2 != null && _Sig2.On)
					state = (int)Uni2States.IndObjStates.YellowAlight;
                else if (_Sig1 != null && _Sig1.On)
					state = (int)Uni2States.IndObjStates.GreenAlight;
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с тримя сигналоми. xx1 - миг красным; x10 - желтый; 100 - зеленый; 000 - не горит
    /// </summary>
    public class IndObjState3Sig_0x1RBl_01xRBl_1xxR : IndObjState3Sig_xx1RBl_x10Y_100G
    {
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1 != null && _Sig1.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else
                {
                    if (_Sig2 != null && _Sig2.On || _Sig3 != null && _Sig3.On)
                        state = (int)Uni2States.IndObjStates.RedBlink;
                    else
                        state = (int)Uni2States.IndObjStates.NotAlight;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с тримя сигналоми. xx1 - миг красным; x10 - желтый; 100 - зеленый; 000 - не горит
    /// </summary>
    public class IndObjState3Sig_xx1RBl_1x0R_010Y : IndObjState3Sig_xx1RBl_x10Y_100G
    {
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig3 != null && _Sig3.On)
                    state = (int)Uni2States.IndObjStates.RedBlink;
                else if (_Sig1 != null && _Sig2 != null && _Sig3 != null)
                {
                    if (_Sig1.On && _Sig3.Off)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else if (_Sig1.Off && _Sig2.On && _Sig3.Off)
                        state = (int)Uni2States.IndObjStates.YellowAlight;
                    else if (_Sig1.Off && _Sig2.Off && _Sig3.Off)
                        state = (int)Uni2States.IndObjStates.NotAlight;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_YellowLB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
	/// Состояние индикатора с двумя сигналоми. Индикатор не горит / Индикатор горит синим цветом / Индикатор мигает синим  цветом
	/// </summary>
	public class IndObjState2Sig_Blue_LB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.BlueAlight;// Индикатор горит синим  цветом
				else if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.BlueBlink;// Индикатор мигает синим  цветом
				else
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_BlueLB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с двумя сигналоми. 00 - горит красный / 10 - не горит / 01,11 - мигает красным
	/// </summary>
	public class IndObjState2Sig_00RedL_x1RedB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.RedBlink;// мигает красным
				else if (_Sig1.Off && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;// горит красным
				else if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 10 - горит красным / 01 - не горит / 00 - мигает красным
    /// </summary>
    public class IndObjState2Sig_10RedL_01WhiteL : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.RedAlight;     // горит красным
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight;   // горит белым
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 10 - горит красным / 01 - не горит / 00 - мигает красным
    /// </summary>
    public class IndObjState2Sig_10RedL_01GreenL : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.RedAlight;     // горит красным
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.GreenAlight;   // горит зеленым
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние индикатора с пятью сигналами.
    /// </summary>
    public class IndObjState5Sig_PONAB_MPC : AbsenceObjState
    {
        protected ISigDiscreteLogic _Sig1;
        protected ISigDiscreteLogic _Sig2;
        protected ISigDiscreteLogic _Sig3;
        protected ISigDiscreteLogic _Sig4;
        protected ISigDiscreteLogic _Sig5;

        /// <summary>
        /// Создание состояния. Получаем сигналы Sig1, Sig2..Sig5 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для Sig1
            if (!AssignToSig("Sig1", out _Sig1))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig2
            if (!AssignToSig("Sig2", out _Sig2))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig3
            if (!AssignToSig("Sig3", out _Sig3))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig4
            if (!AssignToSig("Sig4", out _Sig4))
                return false;
            // Получаем и подписываемся на рассылку изменения для Sig5
            if (!AssignToSig("Sig5", out _Sig5))
                return false;

            return true;
        }
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On || _Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedBlink;// мигает красным
                else if (_Sig3.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;// горит красным
                else if (_Sig4.On)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else if (_Sig5.On)
                    state = (int)Uni2States.IndObjStates.YellowAlight;// Индикатор горит желтым
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_White_LB " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	

	/// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10 - горит белым / 01,11 - мигает красным
	/// </summary>
	public class IndObjState2Sig_10WhiteL_x1RedB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedBlink;// мигает красным
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.WhiteAlight;// горит белым
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS; 
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10 - горит желтым / 01,11 - мигает красным
    /// </summary>
    public class IndObjState2Sig_10YellowL_x1RedB : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedBlink;// мигает красным
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.YellowAlight;// горит желтым
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10 - горит желтым / 01,11 - мигает красным
    /// </summary>
    public class IndObjState2Sig_10YellowL_x1RedL : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.YellowAlight;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10 - горит зеленым / 01,11 - горит желтым
	/// </summary>
    public class IndObjState2Sig_10GreenL_x1YellowL : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
                if (_Sig2.On)
                    state = (int)Uni2States.IndObjStates.YellowAlight;// горит желтым
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
                else if (_Sig1.On && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.GreenAlight;// горит зеленым
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора с двумя сигналоми. 00 - не горит / 10 - горит зеленым / 01,11 - горит желтым
	/// </summary>
	public class IndObjState2Sig_10GreenL_x1YellowB : IndObjState2Sig_Red_LB
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_Sig2.On)
					state = (int)Uni2States.IndObjStates.YellowBlink;// горит желтым
				else if (_Sig1.Off && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;// Индикатор не горит
				else if (_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.GreenAlight;// горит зеленым
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10, 11 - горит зеленым / 01 - горит белым
    /// </summary>
    public class IndObjState2Sig_10GreenL_01WhiteL : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On )
                    state = (int)Uni2States.IndObjStates.GreenAlight;// горит зеленым
				else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.WhiteAlight;// горит белым
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int) Uni2States.IndObjStates.NotAlight;//не горит
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// несоответствие тс
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора с двумя сигналоми. 00 - не горит / 10, 11 - мигает зеленым / 01 - мигает белым
    /// </summary>
    public class IndObjState2Sig_10_GreenB_01_WhiteB : IndObjState2Sig_Red_LB
    {
        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_Sig1.On)
                    state = (int)Uni2States.IndObjStates.GreenBlink;// мигает зеленым
                else if (_Sig1.Off && _Sig2.On)
                    state = (int)Uni2States.IndObjStates.WhiteBlink;// мигает белым
                else if (_Sig1.Off && _Sig2.Off)
                    state = (int)Uni2States.IndObjStates.NotAlight;//не горит
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// несоответствие тс
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора с двумя сигналами, третий загружается, но не учитывается . 00 или 11- мигает красным / 01- не горит / 10 - горит красным
	/// </summary>
	public class IndObjState3Sig_10_Red_11_00_RedB : IndObjState3Sig_00_RBl_1_1Y_1_0R
	{
		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if((_Sig1.Off && _Sig2.Off) || (_Sig1.On && _Sig2.On))
					state = (int)Uni2States.IndObjStates.RedBlink;// мигает красным
				else if(_Sig1.On && _Sig2.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;// горит красным
				else if(_Sig1.Off && _Sig2.On)
					state = (int)Uni2States.IndObjStates.NotAlight;//не горит
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;// несоответствие тс
			}
			catch(ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("IndObjState2Sig_10GreenL_x1YellowL " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	#endregion

	#region Индикация перегона
	/// <summary>
	/// Состояние индикатора перегона. Без отправления
	/// </summary>
	public class IndObjStateWayApproach2 : AbsenceObjState
	{
		/// <summary>
		/// Установление направления прием
		/// </summary>
		protected ISigDiscreteLogic _InDirSig;
		/// <summary>
		/// Свободность перегона
		/// </summary>
		protected ISigDiscreteLogic _FreeSig;
		/// <summary>
		/// Заятость перегона
		/// </summary>
		protected ISigDiscreteLogic _ApproachSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для FreeSig
			if (!AssignToSig("FreeSig", out _FreeSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для ApproachSig
			if (!AssignToSig("ApproachSig", out _ApproachSig))
				return false;

            // Получаем и подписываемся на рассылку изменения для InDirSig
            if (!AssignToSigByObj("InDirObj", "Sig1", out _InDirSig))
                return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_InDirSig.On && _FreeSig.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;
				else
					state = (int)Uni2States.IndObjStates.NotAlight;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS; ;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора перегона. Без отправления
	/// </summary>
	public class IndObjStateWayApproach_ApFr_N_FF : AbsenceObjState
	{
		/// <summary>
		/// Заятость перегона
		/// </summary>
		protected ISigDiscreteLogic _ApproachSig;
        /// <summary>
        /// Свободность перегона
        /// </summary>
        protected ISigDiscreteLogic _FreeSig;

		/// <summary>
		/// Неисправность перегона
		/// </summary>
		protected ISigDiscreteLogic _NSig;
		/// <summary>
		/// фактическая свободность
		/// </summary>
		protected ISigDiscreteLogic _FFSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для ApproachSig
			if (!AssignToSig("Sig1", out _ApproachSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для FreeSig
			if (!AssignToSig("Sig2", out _FreeSig))
				return false;

			// Получаем и подписываемся на рассылку изменения для ApproachSig
			if (!AssignToSig("Sig3", out _NSig))
				return false;
			// Получаем и подписываемся на рассылку изменения для FreeSig
			if (!AssignToSig("Sig4", out _FFSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_ApproachSig.Off && _FreeSig.Off && _NSig.Off && _FFSig.Off)
					state = (int)Uni2States.IndObjStates.NotAlight;
				else if (_ApproachSig.On && _FreeSig.Off && _NSig.Off && _FFSig.Off)
					state = (int)Uni2States.IndObjStates.RedAlight;
				else if (_ApproachSig.Off && _FreeSig.On && _NSig.Off && _FFSig.Off)
					state = (int)Uni2States.IndObjStates.WhiteAlight;
				else if (_ApproachSig.Off && _FreeSig.Off && _NSig.On && _FFSig.Off)
					state = (int)Uni2States.IndObjStates.RedBlink;
				else if (_ApproachSig.Off && _FreeSig.Off && _NSig.Off && _FFSig.On)
					state = (int)Uni2States.IndObjStates.WhiteBlink;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;

			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS; ;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора перегона. Без отправления
	/// </summary>
	public class IndObjStateMarshrNabor_PM_MM_OP_OM : AbsenceObjState
	{
		/// <summary>
		/// окончание поездной маршрут
		/// </summary>
		protected ISigDiscreteLogic _OPMSig;
		/// <summary>
		/// окончание маневровый маршрут
		/// </summary>
		protected ISigDiscreteLogic _OMMSig;

		/// <summary>
		/// поездной маршрут
		/// </summary>
		protected ISigDiscreteLogic _PMSig;
		/// <summary>
		/// маневровый маршрут
		/// </summary>
		protected ISigDiscreteLogic _MMSig;


		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			if (!AssignToSig("Sig1", out _PMSig))
				return false;
			if (!AssignToSig("Sig2", out _MMSig))
				return false;
			if (!AssignToSig("Sig3", out _OPMSig))
				return false;
			if (!AssignToSig("Sig4", out _OMMSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (!_PMSig.On && !_MMSig.On && !_OPMSig.On && !_OMMSig.On)
					state = (int)Uni2States.IndObjStates.NotAlight;
				else if (_PMSig.On && !_MMSig.On && !_OPMSig.On && !_OMMSig.On)
					state = (int)Uni2States.IndObjStates.GreenBlink;
				else if (!_PMSig.On && _MMSig.On && !_OPMSig.On && !_OMMSig.On)
					state = (int)Uni2States.IndObjStates.WhiteBlink;
				else if (!_PMSig.On && !_MMSig.On && _OPMSig.On && !_OMMSig.On)
					state = (int)Uni2States.IndObjStates.GreenAlight;
				else if (!_PMSig.On && !_MMSig.On && !_OPMSig.On && _OMMSig.On)
					state = (int)Uni2States.IndObjStates.WhiteAlight;
				else
					state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
			}
			catch (ArgumentOutOfRangeException /*ex*/)
			{
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS; ;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора перегона. Занятость + отправление
    /// </summary>
    public class IndObjStateWayApproach_ApproachOut : AbsenceObjState
    {
        /// <summary>
        /// Заятость перегона
        /// </summary>
        protected ISigDiscreteLogic _ApproachSig;
        /// <summary>
        /// Установление направления отправление
        /// </summary>
        protected ISigDiscreteLogic _OutDirSig;

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для OutDirSig
            AssignToSigByObj("OutDirObj", "Sig1", out _OutDirSig);

            // Получаем и подписываемся на рассылку изменения для ApproachSig
            if (!AssignToSig("ApproachSig", out _ApproachSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_OutDirSig != null)
                {
                    if ((_OutDirSig.On && _ApproachSig.On))
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else
                        state = (int)Uni2States.IndObjStates.NotAlight;
                }
                else
                {
                    if (_ApproachSig.On)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else
                        state = (int)Uni2States.IndObjStates.NotAlight;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS; ;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

	/// <summary>
	/// Состояние индикатора перегона. 
	/// </summary>
	public class IndObjStateWayApproach1 : IndObjStateWayApproach2
	{
		/// <summary>
		/// Установление направления отправление
		/// </summary>
		protected ISigDiscreteLogic _OutDirSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для OutDirSig
			if (!AssignToSigByObj("OutDirObj", "Sig1", out _OutDirSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if ((_InDirSig.On && _FreeSig.Off) || (_OutDirSig.On && _ApproachSig.Off))
					state = (int)Uni2States.IndObjStates.RedAlight;
				else
					state = (int)Uni2States.IndObjStates.NotAlight;
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceIndicatorState(state);

			SetState(state);

			ResetModify();
		}
	}

	/// <summary>
	/// Состояние индикатора перегона. свободность + прием
	/// </summary>
	public class IndObjStateWayApproach_Free : AbsenceObjState
	{
		/// <summary>
		/// Свободность перегона
		/// </summary>
		protected ISigDiscreteLogic _FreeSig;
        /// <summary>
        /// Установление направления прием
        /// </summary>
        protected ISigDiscreteLogic _InDirSig;

		/// <summary>
		/// Создание состояния. 
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для FreeSig
			if (!AssignToSig("FreeSig", out _FreeSig))
				return false;

            // Получаем и подписываемся на рассылку изменения для InDirSig
		    AssignToSigByObj("InDirObj", "Sig1", out _InDirSig);
            
			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
		/// </summary>
		public override void UpdateObjectState()
		{
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_InDirSig != null)
                {
                    if (_InDirSig.On && _FreeSig.Off)
                        state = (int) Uni2States.IndObjStates.RedAlight;
                    else
                        state = (int) Uni2States.IndObjStates.NotAlight;
                }
                else
                {
                    if(_FreeSig.Off)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else
                        state = (int)Uni2States.IndObjStates.NotAlight;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS; ;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
		}
	}

    /// <summary>
    /// Состояние индикатора перегона. Занятость + отправление + свободность + приём
    /// </summary>
    public class IndObjStateWayApproach : AbsenceObjState
    {
        /// <summary>
        /// Заятость перегона
        /// </summary>
        protected ISigDiscreteLogic _ApproachSig;
        /// <summary>
        /// Установление направления отправление
        /// </summary>
        protected ISigDiscreteLogic _OutDirSig;
        /// <summary>
        /// Свободность перегона
        /// </summary>
        protected ISigDiscreteLogic _FreeSig;
        /// <summary>
        /// Установление направления прием
        /// </summary>
        protected ISigDiscreteLogic _InDirSig;

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для ApproachSig
            if (!AssignToSig("Sig1", out _ApproachSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для OutDirSig
            if (!AssignToSig("Sig2", out _OutDirSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для InDirSig
            if (!AssignToSig("Sig3", out _InDirSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для FreeSig
            if (!AssignToSig("Sig4", out _FreeSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_OutDirSig.On && _ApproachSig.On
                    || _InDirSig.On && _FreeSig.Off)
                {
                    state = (int)Uni2States.IndObjStates.RedAlight;
                }
                else if (_OutDirSig.On && _ApproachSig.Off
                    || _InDirSig.On && _FreeSig.On)
                {
                    state = (int)Uni2States.IndObjStates.NotAlight;
                }
                else
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора перегона. Занятость + отправление + свободность + приём
    /// </summary>
    public class IndObjStateWayApproachFree : AbsenceObjState
    {
        /// <summary>
        /// Заятость перегона
        /// </summary>
        protected ISigDiscreteLogic _ApproachSig;
        /// <summary>
        /// Свободность перегона
        /// </summary>
        protected ISigDiscreteLogic _FreeSig;

        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для ApproachSig
            if (!AssignToSig("Sig1", out _ApproachSig))
                return false;

            // Получаем и подписываемся на рассылку изменения для FreeSig
            if (!AssignToSig("Sig2", out _FreeSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_ApproachSig.On || _FreeSig.Off)
                {
                    state = (int)Uni2States.IndObjStates.RedAlight;
                }
                else if (_ApproachSig.Off || _FreeSig.On)
                {
                    state = (int)Uni2States.IndObjStates.NotAlight;
                }
                else
                {
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    #endregion

    #region Индикаторы АБТЦ-М

    /// <summary>
    /// Состояние индикатора АБТЦ-М "Кодирование РЦ" с одним сигналом модуля ПМИРЦ.
    /// </summary>
    public class IndObjStateSig_ABTCM_RC_Code : AbsenceObjState
    {
        /// <summary>
        /// Кодовый сигнал модуля ПМИРЦ
        /// </summary>
        protected ISigDiscreteLogic _PmiRCSig;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для PmiRCSig
            if (!AssignToSig("PmiRCSig", out _PmiRCSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_PmiRCSig.IOSignals.Length > 0)
                {
                    uint code = _PmiRCSig.IOSignals[0].GetCurrent;

                    if (code == 0)
                        state = (int)Uni2States.IndObjStates.NotAlight; // Не горит
                    else if (code == 1)
                        state = (int)Uni2States.IndObjStates.GreenAlight; // Горит зеленым
                    else if (code == 2)
                        state = (int)Uni2States.IndObjStates.YellowAlight; // Горит желтым
                    else if (code == 3)
                        state = (int)Uni2States.IndObjStates.RedYellowAlight; // Горит красно-желтым
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Состояние индикатора АБТЦ-М "Кодирование РЦ" с 4-мя огневыми сигналами
    /// </summary>
    public class IndObjStateSig_ABTCM_RC : AbsenceObjState
    {
        /// <summary>
        /// Сигнал "Код З"
        /// </summary>
        protected ISigDiscreteLogic _GSig;
        /// <summary>
        /// Сигнал "Код Ж"
        /// </summary>
        protected ISigDiscreteLogic _YSig;
        /// <summary>
        /// Сигнал "Код КЖ"
        /// </summary>
        protected ISigDiscreteLogic _RYSig;
        /// <summary>
        /// Сигнал "Код отсутствует"
        /// </summary>
        protected ISigDiscreteLogic _NoSig;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("GSig", out _GSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("YSig", out _YSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("RYSig", out _RYSig))
                return false;
            // Получаем и подписываемся на рассылку изменения сигнала
            if (!AssignToSig("NoSig", out _NoSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_GSig.IOSignals.Length > 0)
                {
                    uint code = _GSig.IOSignals[0].GetCurrent;

                    if (_NoSig.On && _GSig.Off && _YSig.Off && _RYSig.Off)
                        state = (int)Uni2States.IndObjStates.NotAlight; // Не горит
                    else if (_NoSig.Off && _GSig.On && _YSig.Off && _RYSig.Off)
                        state = (int)Uni2States.IndObjStates.GreenAlight; // Горит зеленым
                    else if (_NoSig.Off && _GSig.Off && _YSig.On && _RYSig.Off)
                        state = (int)Uni2States.IndObjStates.YellowAlight; // Горит желтым
                    else if (_NoSig.Off && _GSig.Off && _YSig.Off && _RYSig.On)
                        state = (int)Uni2States.IndObjStates.RedYellowAlight; // Горит красно-желтым
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    #endregion

    #region Индикаторы ИМСИ

    /// <summary>
    /// Состояние индикатора АБТЦ-М "Кодирование РЦ" с 4-мя огневыми сигналами
    /// </summary>
    public class IndObjStateSig_IMSI : AbsenceObjState
    {
        /// <summary>
        /// Аналоговый сигнал
        /// </summary>
        protected ISigAnalog _USig;
        /// <summary>
        /// Дискретный сигнал
        /// </summary>
        protected ISigDiscreteLogic _Sig1;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для USig
            if (!AssignToFirstAnalogOrDiscreteSig(out _USig, out _Sig1))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                bool o = _controlObj.DiagStates.Count(ds => ds.Id == 2437) > 0;
                bool p = _controlObj.DiagStates.Count(ds => ds.Id == 2438) > 0;

                if (o)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else if (p)
                    state = (int)Uni2States.IndObjStates.YellowAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }


    }
    /// <summary>
    /// Задача выявления ТО по диагностическому состояниям
    /// </summary>
    public class IndObjStateSig_IMSI_Diag : AbsenceObjState_Diag
    {
        /// <summary>
        /// Аналоговый сигнал
        /// </summary>
        protected ISigAnalog _USig;
        /// <summary>
        /// Дискретный сигнал
        /// </summary>
        protected ISigDiscreteLogic _Sig1;

        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение данного объекта
            AddWatchToObject(_controlObj);

            _Sig1 = AssignToDiscretSignal("Sig1");
            _USig = AssignToAnalogSignal("USig");
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                bool o = _controlObj.DiagStates.Count(ds => ds.Id == 2437) > 0;
                bool p = _controlObj.DiagStates.Count(ds => ds.Id == 2438) > 0;

                if (o)
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else if (p)
                    state = (int)Uni2States.IndObjStates.YellowAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }

            SetState(state);

            ResetModify();
        }
    }
    #endregion

    #region Индикатор БарсУК
    /// <summary>
    /// Задача выявления ТО по диагностическому состояниям
    /// </summary>
    public class IndObjStateSig_Barsuk_Diag : AbsenceObjState_Diag
    {
        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение данного объекта
            AddWatchToObject(_controlObj);
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                bool o = _controlObj.DiagStates.Count(ds => ds.Id == 2415 || ds.Id == 2416 || ds.Id == 2417) > 0;

                if (o)
                    state = (int)Uni2States.IndObjStates.RedBlink;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }

            SetState(state);

            ResetModify();
        }
    }

    #endregion

    #region Индикаторы САУТ-ЦМ НСП

    /// <summary>
    /// Состояние индикатора перегона. Занятость + отправление + свободность + приём
    /// </summary>
    public class IndObjState_SAUT_CM_NSP : AbsenceObjState
    {
        /// <summary>
        /// Признак блокировки БКП-М
        /// </summary>
        protected ISigDiscreteLogic _BBKPSig;
        /// <summary>
        /// Признак "нет связи" БКП-М
        /// </summary>
        protected ISigDiscreteLogic _NSBKPSig;
        
        /// <summary>
        /// Признак ошибки строба 1
        /// </summary>
        protected ISigDiscreteLogic _OS1Sig;
        /// <summary>
        /// Признак ошибки строба 0
        /// </summary>
        protected ISigDiscreteLogic _OS0Sig;
        /// <summary>
        /// Признак ошибки шины
        /// </summary>
        protected ISigDiscreteLogic _OSHSig;
        /// <summary>
        /// Признак ошибки адреса УВС-М
        /// </summary>
        protected ISigDiscreteLogic _OAUVSSig;
        /// <summary>
        /// Признак ошибки "нет связи" УВС-М
        /// </summary>
        protected ISigDiscreteLogic _NSUVSSig;

        /// <summary>
        /// Признак ошибки адреса БПМ
        /// </summary>
        protected ISigDiscreteLogic _OABPMSig;
        /// <summary>
        /// Признак ошибки блока ГПУ
        /// </summary>
        protected ISigDiscreteLogic _OGPUSig;
        /// <summary>
        /// Признак блокировки БПМ
        /// </summary>
        protected ISigDiscreteLogic _BBPMSig;
        /// <summary>
        /// Признак "нет связи" БПМ
        /// </summary>
        protected ISigDiscreteLogic _NSBPMSig;

        /// <summary>
        /// Признак ошибки кода в шлейфе
        /// </summary>
        protected ISigDiscreteLogic _OKSHSig;
        /// <summary>
        /// Признак кодового режима
        /// </summary>
        protected ISigDiscreteLogic _KRSig;
        /// <summary>
        /// Признак обрыва шлейфа
        /// </summary>
        protected ISigDiscreteLogic _OSHLSig;
        /// <summary>
        /// Признак ток не в норме
        /// </summary>
        protected ISigDiscreteLogic _TNvNSig;
        /// <summary>
        /// Признак отсутствия связи по линии 2
        /// </summary>
        protected ISigDiscreteLogic _NSL2Sig;
        /// <summary>
        /// Признак отсутствия связи по линии 1
        /// </summary>
        protected ISigDiscreteLogic _NSL1Sig;
        /// <summary>
        /// Признак ошибки адреса ГПУ
        /// </summary>
        protected ISigDiscreteLogic _OAGPUSig;
        /// <summary>
        /// Признак блокировки ГПУ
        /// </summary>
        protected ISigDiscreteLogic _BGPUSig;


        /// <summary>
        /// Создание состояния. 
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // САУТБКП-М
            AssignToSig("BBKPSig", out _BBKPSig);
            AssignToSig("NSBKPSig", out _NSBKPSig);
            // САУТУВС-М
            AssignToSig("OS1Sig", out _OS1Sig);
            AssignToSig("OS0Sig", out _OS0Sig);
            AssignToSig("OSHSig", out _OSHSig);
            AssignToSig("OAUVSSig", out _OAUVSSig);
            AssignToSig("NSUVSSig", out _NSUVSSig);
            // САУТБПМ
            AssignToSig("OABPMSig", out _OABPMSig);
            AssignToSig("OGPUSig", out _OGPUSig);
            AssignToSig("BBPMSig", out _BBPMSig);
            AssignToSig("NSBPMSig", out _NSBPMSig);
            //САУТГПУ
            AssignToSig("OKSHSig", out _OKSHSig);
            AssignToSig("KRSig", out _KRSig);
            AssignToSig("OSHLSig", out _OSHLSig);
            AssignToSig("TNvNSig", out _TNvNSig);
            AssignToSig("NSL2Sig", out _NSL2Sig);
            AssignToSig("NSL1Sig", out _NSL1Sig);
            AssignToSig("OAGPUSig", out _OAGPUSig);
            AssignToSig("BGPUSig", out _BGPUSig);

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_BBKPSig != null && _NSBKPSig != null)
                {
                    if (_BBKPSig.On)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else
                    {
                        if (_NSBKPSig.Off)
                            state = (int)Uni2States.IndObjStates.NotAlight;
                        else
                            state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                    }
                }
                else if (_OS1Sig != null && _OS0Sig != null && _OSHSig != null && _OAUVSSig != null && _NSUVSSig != null)
                {
                    if (_OAUVSSig.On || _OSHSig.On || _OS0Sig.On || _OS1Sig.On)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else if (_OAUVSSig.Off && _NSUVSSig.On)
                        state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                    else if (_OAUVSSig.Off && _NSUVSSig.Off && _OSHSig.Off && (_OS0Sig.On || _OS1Sig.On))
                        state = (int)Uni2States.IndObjStates.YellowAlight;
                    else if (_OAUVSSig.Off && _NSUVSSig.Off && _OSHSig.Off && _OS0Sig.Off && _OS1Sig.On)
                        state = (int)Uni2States.IndObjStates.NotAlight;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else if (_OABPMSig != null && _OGPUSig != null && _BBPMSig != null && _NSBPMSig != null)
                {
                    if(_BBPMSig.On)
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else if (_BBPMSig.Off && _OABPMSig.On)
                        state = (int)Uni2States.IndObjStates.YellowAlight;
                    else if (_BBPMSig.Off && _OABPMSig.Off && _NSBPMSig.On)
                        state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                    else if (_BBPMSig.Off && _OABPMSig.Off && _NSBPMSig.Off)
                        state = (int)Uni2States.IndObjStates.NotAlight;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else if (_OKSHSig != null && _KRSig != null && _OSHLSig != null && _TNvNSig != null && _NSL1Sig != null && _NSL2Sig != null && _OAGPUSig != null && _BGPUSig != null)
                {
                    if(_KRSig.On && (_OKSHSig.On || _OSHLSig.On || _TNvNSig.On || _NSL1Sig.On || _NSL2Sig.On || _OAGPUSig.On || _BGPUSig.On))
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else if(_KRSig.Off && (_OSHLSig.On || _TNvNSig.On || _NSL1Sig.On || _NSL2Sig.On || _OAGPUSig.On || _BGPUSig.On))
                        state = (int)Uni2States.IndObjStates.RedAlight;
                    else if(_KRSig.On && (_OKSHSig.Off && _OSHLSig.Off && _TNvNSig.Off && _NSL1Sig.Off && _NSL2Sig.Off && _OAGPUSig.Off && _BGPUSig.Off))
                        state = (int)Uni2States.IndObjStates.NotAlight;
                    else if (_KRSig.Off && (_OSHLSig.Off && _TNvNSig.Off && _NSL1Sig.Off && _NSL2Sig.Off && _OAGPUSig.Off && _BGPUSig.Off))
                        state = (int)Uni2States.IndObjStates.NotAlight;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else
                {
                    state = (int)Uni2States.GeneralStates.ObjectConserve;
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    
    #endregion

    #region Групповой индикатор СКОС
    
    /// <summary>
    /// Класс формирования унифицированного состояния по ДС ссылочных объектов
    /// </summary>
    public class IndObjState_SKOS : AbsenceObjState
    {
        /// <summary>
        /// Список ссылочных унифицированных объектов
        /// </summary>
        protected List<UniObject> _refObjs = new List<UniObject>();
        /// <summary>
        /// Список целевых ДС-отказов
        /// </summary>
        public static List<int> _trgFaultList = new List<int>() { 2200, 2513, 35050 };
        /// <summary>
        /// Список целевых ДС-предотказов
        /// </summary>
        public static List<int> _trgPreFaultList = new List<int>() { 2201, 2397, 2398, 2514, 2517, 2520 };

        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            _refObjs = GetUniObjectsByRefs(_controlObj);
            if (_refObjs.Count == 0)
                return false;

            foreach (UniObject uni in _refObjs)
            {   // подписываемся на изменение всех ссылочных объектов
                AssignToObj(_controlObj);
            }

            return true;
        }

        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                List<BaseControlObj.DiagState> diags = UnionObjDiagStates(_refObjs);

                if (ObjHasDiagStates(diags, _trgFaultList))
                    state = (int)Uni2States.IndObjStates.RedAlight;
                else if (ObjHasDiagStates(diags, _trgPreFaultList))
                    state = (int)Uni2States.IndObjStates.YellowAlight;
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
        /// <summary>
        /// Объединение ДС унифицированных объектов в список
        /// </summary>
        /// <param name="objs">Унифицированные объекты</param>
        /// <returns>Список ДС</returns>
        protected static List<BaseControlObj.DiagState> UnionObjDiagStates(List<UniObject> objs)
        {
            List<BaseControlObj.DiagState> res = new List<BaseControlObj.DiagState>();
            foreach (UniObject obj in objs)
                res.AddRange(obj.DiagStates);
            return res;
        }

        /// <summary>
        /// Сравнение списка ДС объекта и целевых
        /// </summary>
        /// <param name="objDiags">ДС объекта</param>
        /// <param name="trgDiags">ДС целевые</param>
        /// <returns>Признак надичия целевых ДС у объекта</returns>
        protected static bool ObjHasDiagStates(List<BaseControlObj.DiagState> objDiags, List<int> trgDiags)
        {
            foreach (BaseControlObj.DiagState ds in objDiags)
                if (trgDiags.Contains(ds.Id))
                    return true;
            return false;
        }

    }

    
    #endregion

    #endregion

    #region Тормозной упор (УТС - упор тормозной стационарный)
    /// <summary>
	/// Состояние тормозного упора
	/// </summary>
    public class BrakeState : BrakeState_noWheels
	{
		/// <summary>
		/// Сигнал присутствия колесной пары в зоне тормозных упоров
		/// </summary>
		protected ISigDiscreteLogic _KPSig;

		/// <summary>
		/// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
		/// </summary>
		/// <param name="obj">Объект контроля</param>
		/// <returns>true в случае успеха</returns>
		public override bool Create(UniObject obj)
		{
			if (!base.Create(obj))
				return false;

			// Получаем и подписываемся на рассылку изменения для _WheelPairInZone
			if (!AssignToSig("KPSig", out _KPSig))
				return false;

			return true;
		}

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			try
			{
				if (_KPSig.On)
				{
					if (_SetSig.On)
						state = (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairInZone;//УТС установлен и колесная пара в зоне УТС
					else if (_UnSetSig.On)
						state = (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairInZone;//УТС снят и колесная пара в зоне УТС
					else
						state = (int)Uni2States.BrakeStates.UTS_Unknown_WheelPairInZone;//Нет контроля положения УТС и колесная пара в зоне УТС
				}
				else
				{
					if (_SetSig.On)
						state = (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairNone;//УТС установлен и нет колесной пары в зоне УТС
					else if (_UnSetSig.On)
						state = (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairNone;//УТС снят и нет колесной пары в зоне УТС
					else
						state = (int)Uni2States.BrakeStates.UTS_Unknown_WheelPairNone;//Нет контроля положения УТС и нет колесной па-ры в зоне УТС
				}
					
			}
            catch (ArgumentOutOfRangeException /*ex*/)
			{
				//Debug.WriteLine("BrakeState " + _controlObj.Name + ":" + ex.Message);
				state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
			}
			TraceBrakeState(state);

			SetState(state);

			ResetModify();
		}
	}
    /// <summary>
    /// Состояние тормозного упора
    /// </summary>
    public class BrakeState_noWheels : AbsenceObjState
    {
        /// <summary>
        /// Сигнал установленного УТС
        /// </summary>
        protected ISigDiscreteLogic _SetSig;
        /// <summary>
        /// Сигнал убранного УТС
        /// </summary>
        protected ISigDiscreteLogic _UnSetSig;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;
            // Получаем и подписываемся на рассылку изменения для _UTS_Mounted
            if (!AssignToSig("SetSig", out _SetSig))
                return false;
            // Получаем и подписываемся на рассылку изменения для _UTS_TakeAway
            if (!AssignToSig("UnsetSig", out _UnSetSig))
                if (!AssignToSig("UnSetSig", out _UnSetSig))
                    return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_SetSig.On)
                    state = (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairNone;//УТС установлен и нет колесной пары в зоне УТС
                else if (_UnSetSig.On)
                    state = (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairNone;//УТС снят и нет колесной пары в зоне УТС
                else
                    state = (int)Uni2States.BrakeStates.UTS_Unknown_WheelPairNone;//Нет контроля положения УТС и нет колесной пары в зоне УТС
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("BrakeState " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceBrakeState(state);

            SetState(state);

            ResetModify();
        }
    }

    public class BrakeState_NoInfo : BrakeState_noWheels
    {
        /// <summary>
        /// Сигнал присутствия колесной пары в зоне тормозных упоров
        /// </summary>
        protected ISigDiscreteLogic _NoInfoSig;

        /// <summary>
        /// Создание состояния. Получаем сигнал Sig1 и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для _WheelPairInZone
            if (!AssignToSig("NoInfo", out _NoInfoSig))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_NoInfoSig.On)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                else if (_SetSig.On)
                    state = (int)Uni2States.BrakeStates.UTS_Mounted_WheelPairNone;//УТС установлен и колесная пара в зоне УТС
                else if (_UnSetSig.On)
                    state = (int)Uni2States.BrakeStates.UTS_TakeAway_WheelPairNone;//УТС снят и колесная пара в зоне УТС
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("BrakeState " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceBrakeState(state);

            SetState(state);

            ResetModify();
        }
    }
	#endregion

	#region Стрелочный коммутатор
	#endregion

    #region Контактная сеть
    /// <summary>
    /// Переключатель контактной сети(без контроля электротягового маршрута)
    /// </summary>
    public class Catenary_NoControl : AbsenceObjState
    {
        /// <summary>
        /// Наличие постоянного напряжения 
        /// </summary>
        protected ISigDiscreteLogic _DCVoltage;
        /// <summary>
        ///  Наличие переменного напряжения
        /// </summary>
        protected ISigDiscreteLogic _ACVoltage;
        /// <summary>
        /// Включение постоянного напряжения
        /// </summary>
        protected ISigDiscreteLogic _DCVoltageSwitchingOn;
        /// <summary>
        /// Включение переменного напряжения
        /// </summary>
        protected ISigDiscreteLogic _ACVoltageSwitchingOn;

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для сигналов
            if (!AssignToSig("DCVoltage",out _DCVoltage))
                return false;
            if (!AssignToSig("ACVoltage", out _ACVoltage))
                return false;
            if (!AssignToSig("DCVoltageSwitchingOn", out _DCVoltageSwitchingOn))
                return false;
            if (!AssignToSig("ACVoltageSwitchingOn", out _ACVoltageSwitchingOn))
                return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_DCVoltage.On && _ACVoltageSwitchingOn.Off)
                    state = (int) Uni2States.CatenaryStates.DCVoltageOn;
                else if (_DCVoltage.On&&_ACVoltageSwitchingOn.On)
                    state = (int) Uni2States.CatenaryStates.DCVoltageOn_ACVoltageSwitchingOn;
                else if (_ACVoltage.On && _DCVoltageSwitchingOn.Off)
                    state = (int) Uni2States.CatenaryStates.ACVoltageOn;
                else if (_ACVoltage.On && _DCVoltageSwitchingOn.On)
                    state = (int) Uni2States.CatenaryStates.ACVoltageOn_DCVoltageSwitchingOn;
                else if (_DCVoltageSwitchingOn.On&&_ACVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.DCVoltageSwitchingOn;
                else if (_ACVoltageSwitchingOn.On&&_DCVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.ACVoltageSwitchingOn;
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }

    /// <summary>
    /// Переключатель контактной сети(с контролем электротягового маршрута)
    /// </summary>
    public class Catenary_Control : Catenary_NoControl
    {
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения второго автономного локомотива (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _DieselTrain2;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения второго автономного локомотива (М) на пути 
        /// </summary>
        protected ISigDiscreteLogic _DieselTrain2_ManualBreak;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения первого электровоза (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _Train1;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения первого электровоза (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _Train1_ManualBreak;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения первого автономного локомотива (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _DieselTrain1;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения первого автономного локомотива (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _DieselTrain1_ManualBreak;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения второго электровоза (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _Train2;
        /// <summary>
        /// Контроль наличия (1), искусственной разделки нахождения второго электровоза (М) на пути
        /// </summary>
        protected ISigDiscreteLogic _Train2_ManualBreak;

        /// <summary>
        /// Создание состояния. Получаем сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для сигналов
            if (!AssignToSig("DieselTrain1", out _DieselTrain1))
                return false;
            if (!AssignToSig("DieselTrain2", out _DieselTrain2))
                return false;
            if (!AssignToSig("DieselTrain1_ManualBreak", out _DieselTrain1_ManualBreak))
                return false;
            if (!AssignToSig("DieselTrain2_ManualBreak", out _DieselTrain2_ManualBreak))
                return false;
            if (!AssignToSig("Train1", out _Train1))
                return false;
            if (!AssignToSig("Train2", out _Train2))
                return false;
            if (!AssignToSig("Train1_ManualBreak", out _Train1_ManualBreak))
                return false;
            if (!AssignToSig("Train2_ManualBreak", out _Train2_ManualBreak))
                return false;
            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state=0;
            bool train = (_Train1.On||_Train2.On||_Train1_ManualBreak.On||_Train2_ManualBreak.On);
            bool diesel = (_DieselTrain1.On||_DieselTrain2.On||_DieselTrain1_ManualBreak.On||_DieselTrain2_ManualBreak.On);
            bool manbreak = (_Train1_ManualBreak.On||_Train2_ManualBreak.On||_DieselTrain1_ManualBreak.On||_DieselTrain2_ManualBreak.On);
            try
            {
                if (_DCVoltage.On && _ACVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.DCVoltageOn;//6
                else if (_DCVoltage.On && _ACVoltageSwitchingOn.On)
                    state = (int)Uni2States.CatenaryStates.DCVoltageOn_ACVoltageSwitchingOn;//8
                else if (_ACVoltage.On && _DCVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn;//7
                else if (_ACVoltage.On && _DCVoltageSwitchingOn.On)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn_DCVoltageSwitchingOn;//9
                else if (_DCVoltageSwitchingOn.On && _ACVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.DCVoltageSwitchingOn;//4
                else if (_ACVoltageSwitchingOn.On && _DCVoltageSwitchingOn.Off)
                    state = (int)Uni2States.CatenaryStates.ACVoltageSwitchingOn;//5
                else if (_DCVoltage.On && train && !manbreak)
                    state = (int) Uni2States.CatenaryStates.DCVoltageOn_NetCircuit_Train;//12
                else if (_ACVoltage.On && train && !manbreak)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn_NetCircuit_Train;//13
                else if (_DCVoltage.On && train && manbreak)
                    state = (int)Uni2States.CatenaryStates.DCVoltageOn_NetCircuit_Train_ManualBreak;//14
                else if (_ACVoltage.On && train && manbreak)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn_NetCircuit_Train_ManualBreak;//15
                else if (_DCVoltage.On && diesel && !manbreak)
                    state = (int)Uni2States.CatenaryStates.DCVoltageOn_NetCircuit_DieselTrain;//16
                else if (_ACVoltage.On && diesel && !manbreak)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn_NetCircuit_DieselTrain;//17
                else if (_DCVoltage.On && diesel && manbreak)
                    state = (int)Uni2States.CatenaryStates.DCVoltageOn_NetCircuit_DieselTrain_ManualBreak;//18
                else if (_ACVoltage.On && diesel && manbreak)
                    state = (int)Uni2States.CatenaryStates.ACVoltageOn_NetCircuit_DieselTrain_ManualBreak;//19
                else
                    state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                    
                
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    #endregion

    #region Кодирование
    /// <summary>
    /// Состояние индикатора кодирования
    /// </summary>
    public class CodeObjState1Sig : AbsenceObjState
    {
        /// <summary>
        /// Сигнал кодирования (дискретный многоуровневый)
        /// </summary>
        protected ISigDiscreteLogic _CodeSig;
        /// <summary>
        /// Унифицированный объект-владелец экземпляра алгоритма
        /// </summary>
        protected UniObject _obj = null;

        /// <summary>
        /// Создание состояния. Получаем сигнал CodeSig и подписываемся на рассылку изменения его данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            // Получаем и подписываемся на рассылку изменения для RCSig
            if (!AssignToSig("CodeSig", out _CodeSig))
                if (!AssignToSig("NoSig", out _CodeSig))
                    return false;
            
            _obj = obj;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            #region Отладочный лог сигналов на входе подзадач контроля
            //if (_obj != null
            //    //&& (_obj.Name == "1П" || _obj.Name == "IIП" || _obj.Name == "IIIП")
            //    //&& _obj.Site.ID == 11167
            //    )
            //{
            //    System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_codeSignalTrace.txt", true);
            //    sw.WriteLine(string.Format("Site={0}, Object={1}, CodeSig={2} (в АДК: Current - [{3}])"
            //        , _obj.Site.Name
            //        , _obj.Name
            //        , _CodeSig.On
            //        , RCState_RC_Z.ConcatSigCurrent(_CodeSig.IOSignals)));
            //    sw.Flush();
            //    sw.Close();
            //}
            #endregion

            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_CodeSig.IOSignals.Length > 0)
                {
                    uint code = _CodeSig.IOSignals[0].GetCurrent;
                    if(code >= 7 && code <= 10)
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                    else if (code == 0)
                        state = (int)Uni2States.CodeStates.NoCode;
                    else if (code == 3)
                        state = (int)Uni2States.CodeStates.RedYellow5;
                    else if (code == 2)
                        state = (int)Uni2States.CodeStates.Yellow5;
                    else if (code == 1)
                        state = (int)Uni2States.CodeStates.Green5;
                    else if (code == 6)
                        state = (int)Uni2States.CodeStates.RedYellow7;
                    else if (code == 5)
                        state = (int)Uni2States.CodeStates.Yellow7;
                    else if (code == 4)
                        state = (int)Uni2States.CodeStates.Green7;
                    else
                        state = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
                }
                else
                    state = (int)Uni2States.IndObjStates.NotAlight;
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("IndObjState1Sig_Red " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceIndicatorState(state);

            SetState(state);

            ResetModify();
        }
    }
    

    #endregion

    #region Самодиагностика
    
    /// <summary>
    /// Состояние объекта самодиагностики
    /// </summary>
    public class SelfDiagObjState : AbsenceObjState
    {
        /// <summary>
        /// Контроль кодового сигнала исправности устройства
        /// </summary>
        protected List<ISigDiscreteLogic> _OkSigs = new List<ISigDiscreteLogic>();

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            bool bRes = false;
            // Получаем и подписываемся на рассылку изменения для _KPlSig
            for (int i = 0; i <= 8; i++)
            {
                ISigDiscreteLogic tmpSig = null;
                if (AssignToSig("OkSig" + (i > 0 ? i.ToString() : string.Empty), out tmpSig))
                {
                    _OkSigs.Add(tmpSig);
                    bRes = true;
                }
            }
            return bRes;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                state = (int)Uni2States.SelfDiagStates.HasControl;
                foreach (ISigDiscreteLogic l in _OkSigs)
                {
                    if (l.On)
                    {
                        state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                        break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
    }
    /// <summary>
    /// Состояние элемента подсистемы ввода сигналов
    /// </summary>
    public class AdkIoSysSelfDiagObjState : AbsenceObjState
    {
        /// <summary>
        /// Сигнал элемента подсистемы
        /// </summary>
        protected ISigDiscreteLogic _AnySig;
        /// <summary>
        /// Элемент подсистемы
        /// </summary>
        protected IOBaseElement _baseElem;

        /// <summary>
        /// Создание состояния. Получаем необходимые сигналы и подписываемся на рассылку изменения их данных
        /// </summary>
        /// <param name="obj">Объект контроля</param>
        /// <returns>true в случае успеха</returns>
        public override bool Create(UniObject obj)
        {
            if (!base.Create(obj))
                return false;

            if (!AssignToModule(_controlObj, out _baseElem))
                return false;

            return true;
        }

        /// <summary>
        /// обработка исходных данных - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectState()
        {
            int state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            try
            {
                if (_baseElem.GeneralState.Current == IOGeneralState.Unknown
                    || _baseElem.GeneralState.Current == IOGeneralState.None)
                    state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
                else
                    state = (int)Uni2States.SelfDiagStates.HasControl;

                SetDiagStates(_baseElem.GeneralState.Current, _controlObj.Type.SubGroupID);
            }
            catch (ArgumentOutOfRangeException /*ex*/)
            {
                //Debug.WriteLine("ObjStateInSignal_WhiteNone_OExist_SExist " + _controlObj.Name + ":" + ex.Message);
                state = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
            }
            finally
            {
                if (_controlObj.FullName.Contains("уапсе") && _controlObj.FullName.Contains("101МАВ"))
                {
                    #region Отладочный лог ДС МПУ
                    StreamWriter swLog = new StreamWriter("Туапсе_101МАВ_МПУ.log", true);
                    swLog.WriteLine("{0}\t{1}\t{2}", DateTime.Now, state, _baseElem.GeneralState.Current);
                    swLog.Flush();
                    swLog.Close();
                    #endregion
                }
            }
            TraceSignalState(state);
            SetState(state);

            ResetModify();
        }
        /// <summary>
        /// Установка/снятие ДС по состоянию элемента подсистемы
        /// </summary>
        /// <param name="state"></param>
        /// <param name="uniType"></param>
        protected void SetDiagStates(IOGeneralState state, int uniType)
        {
            if (uniType == 101)
            {
                CheckState(state, IOGeneralState.Unstable, (int)Uni2DiagStates.SelfDiagStates.Controller_Unstable);
                CheckState(state, IOGeneralState.Refusal, (int)Uni2DiagStates.SelfDiagStates.Controller_Fail);
            }
            else if (uniType == 102)
            {
                CheckState(state, IOGeneralState.Unstable, (int)Uni2DiagStates.SelfDiagStates.Concentrator_Unstable);
                CheckState(state, IOGeneralState.Refusal, (int)Uni2DiagStates.SelfDiagStates.Concentrator_Fail);
            }
            else if (uniType == 104)
            {
                CheckState(state, IOGeneralState.Unstable, (int)Uni2DiagStates.SelfDiagStates.Link_Unstable);
                CheckState(state, IOGeneralState.Refusal, (int)Uni2DiagStates.SelfDiagStates.Link_Fail);
            }
        }
        /// <summary>
        /// Проверка состояния элемента, установка/снятие ДС
        /// </summary>
        /// <param name="curState">Текущее состояние</param>
        /// <param name="checkState">Проверяемое состояние</param>
        /// <param name="dsID">ID ДС</param>
        protected void CheckState(IOGeneralState curState, IOGeneralState checkState, int dsID)
        {
            if (curState == checkState)
            {
                if (_controlObj.DiagStates.Count(ds => ds.Id == dsID) == 0)
                    _controlObj.ActivateObjDiagState(dsID);
            }
            else
            {
                if (_controlObj.DiagStates.Count(ds => ds.Id == dsID) > 0)
                    _controlObj.DeactivateObjDiagState(dsID);
            }
        }

    }


    #endregion
}
