﻿// Collector.cs  : базовый класс для сбора данных 
// Создан: 14/3/2007
// Copyright (c) 2007 НПП ЮгПромАвтоматизация Прищепа М.В.
/////////////////////////////////////////////////////////////////////////////
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Threading;
using AfxEx;
using Ivk.IOSys;
using Dps.UniRequest;
using Dps.AppDiag;
using Tdm.Unification;

namespace Tdm
{
	/// <summary>
	/// Интерфейс коллектора данных
	/// </summary>
	public interface ICollector
	{
		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		void SetData(byte[] data, ClientBase client, DateTime time, IOBaseElement targetElement);
		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		void SetData(object data, ClientBase client, DateTime time, IOBaseElement targetElement);
	}
	/// <summary>
	/// Интерфейс запроса сосотяния подсистем ввода
	/// </summary>
	public interface IRequesterIOSys
	{
		/// <summary>
		/// Начать запрашивать состояние подсистемы sys (вдобавок к предыдущим)
		/// </summary>
		/// <param name="sys">подсистема</param>
		/// <returns>true, когда это возможно</returns>
		bool StartRequestFor(BaseIOSystem sys);
		/// <summary>
		/// Закончить запрашивать состояние подсистемы sys (для остальных запросы продолжаются)
		/// </summary>
		/// <param name="sys">подсистема</param>
		void StopRequsetFor(BaseIOSystem sys);
	}

	/// <summary>
	/// Базовый класс клиента, реализующего протокол получения данных
	/// </summary>
	public class ClientBase : DlxObject
	{
		/// <summary>
		/// получить коллектор-владельца
		/// </summary>
		/// <returns>коллектор-владелец</returns>
		public virtual ICollector GetOwnerCollector() { return Owner as ICollector; }
		/// <summary>
		/// Метод установки данных - вызывают установку у коллектора
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		protected virtual void SetData(byte[] data, DateTime time, IOBaseElement targetElement)
		{
			GetOwnerCollector().SetData(data, this, time, targetElement);
		}
		/// <summary>
		/// Метод установки данных - вызывают установку у коллектора
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		protected virtual void SetData(object data, DateTime time, IOBaseElement targetElement)
		{
			GetOwnerCollector().SetData(data, this, time, targetElement);
		}
	}

	/// <summary>
	/// Базовый класс сборщика данных, имеющего коллекцию клиентов, реализующих протоколы получения данных
	/// </summary>
	public abstract class Collector : DlxCollection, ICollector
	{
		/// <summary>
		/// Базовый класс клиента, реализующего протокол обмена на отдельном потоке (нужно переопределить метод Run)
		/// </summary>
		public abstract class ClientBaseOnThread : ClientBase
		{
			#region Методы для работы на потоке
			/// <summary>
			/// Виртуальный метод, вызываемый при старте потока
			/// </summary>
			/// <returns>true если инициализация прошла успешно</returns>
			public virtual bool OnInitThread()
			{
				return true;
			}
			/// <summary>
			/// Абстрактный метод работы потока
			/// </summary>
			protected abstract void Run();

			/// <summary>
			/// Виртуальный метод, вызываемый при корректном завершении потока
			/// </summary>
			protected virtual void OnDestroyThread()
			{
			}

			#endregion

			/// <summary>
			/// Рабоча процедура с защитой от неотловленного исключения - осуществляет повторный запуск
			/// </summary>
			protected void WorkProc()
			{
				while(true)
				{
					try
					{
						Thread.Sleep(3000);
						Trace.WriteLine("Run thread: " + GetType().ToString() + " named " + Name);
						Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)100, "Запуск потока клиента " + Name);
						// основная работа
						Run();
						Trace.WriteLine("End thread: " + GetType().ToString() + " named " + Name);
						Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)101, "Завершение потока клиента " + Name);
					}
					catch(ThreadInterruptedException)
					{
						// завершение работы
						Trace.WriteLine("Interrupted thread: " + GetType().ToString() + " named " + Name);
						Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)101, "Прерывание потока клиента " + Name);
						break;
					}
					catch(ThreadAbortException)
					{
						// завершение работы
						Trace.WriteLine("Abort thread: " + GetType().ToString() + " named " + Name);
						Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Warning, (EventLogID)101, "Принудительная остановка потока клиента " + Name);
						break;
					}
					catch(Exception e)
					{
						Trace.WriteLine("ClientBaseOnThread catch exception but try to Run again: " + e.ToString());
						Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)101, "Исключение на потоке клиента " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					}
				}
			}

			/// <summary>
			/// Основная процедура потока
			/// </summary>
			public void ThreadProc()
			{
				try
				{
					// вызываем инициализацию
					if(OnInitThread())
						WorkProc();// основная работа
				}
				catch(ThreadInterruptedException)
				{
					// завершение работы
					Trace.WriteLine("Interrupted thread before start: " + GetType().ToString() + " named " + Name);
				}
				finally
				{
					Trace.WriteLine("End thread: " + GetType().ToString() + " named " + Name);
					// завершение работы
					OnDestroyThread();
				}
			}

			#region Переопределенные методы из DlxObject

			/// <summary>
			/// Переопределена загрузка для загрузки приоритета потока
			/// </summary>
			/// <param name="Loader"></param>
			public override void Load(DlxLoader Loader)
			{
				base.Load(Loader);
				_priority = Loader.GetAttributeString("Priority", "Normal");
			}
			/// <summary>
			/// Осуществляется запуск потока
			/// </summary>
			/// <returns></returns>
			public override void Create()
			{
				base.Create();

				// создаем потока
				_thread = new Thread(new ThreadStart(ThreadProc));
				// задаем приоритет
				switch(_priority)
				{
					case "Normal":
						_thread.Priority = ThreadPriority.Normal;
						break;
					case "AboveNormal":
						_thread.Priority = ThreadPriority.AboveNormal;
						break;
					case "BelowNormal":
						_thread.Priority = ThreadPriority.BelowNormal;
						break;
					case "Lowest":
						_thread.Priority = ThreadPriority.Lowest;
						break;
					case "Highest":
						_thread.Priority = ThreadPriority.Highest;
						break;
				}
				// запускаем поток
				_thread.Start();
			}
			/// <summary>
			/// Осуществляется остановка потока
			/// </summary>
			/*public override void Destroy()
            {
				base.Destroy();
				if (_thread != null)
				{
					Trace.WriteLine("Interrupt thread " + Name + "...");
					// завершаем поток
					_thread.Interrupt();
					// ожидаем завершения потока 1 секунду
					if (!_thread.Join(1000))
					{
						Trace.WriteLine("Abort thread " + Name + "...");
						// поток не завершился - принудительно завершаем поток
						_thread.Abort();
					}
				}
            }*/

			#endregion

			#region Данные класса
			/// <summary>
			/// Значение приоритета потока - загружается из xml
			/// </summary>
			protected string _priority = "Normal";
			/// <summary>
			/// Поток, на котором работает обмен
			/// </summary>
			protected Thread _thread = null;

			#endregion
		}

		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		public abstract void SetData(byte[] data, ClientBase client, DateTime time, IOBaseElement targetElement);

		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		public abstract void SetData(object data, ClientBase client, DateTime time, IOBaseElement targetElement);

		/// <summary>
		/// получить клиента по имени
		/// </summary>
		/// <param name="name">имя клиента</param>
		/// <returns>клиент</returns>
		public ClientBase GetClient(string name) { return GetObject(name) as ClientBase; }
		/// <summary>
		/// загрузка - осуществялется загрузка по имени тега Client, и дочерние элементы должны быть типа ClientBase
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			if(!Loader.GetAttribute(XmlElement.s_TextName, out m_Name) || (m_Name.Length == 0))
				throw new ArgumentException(string.Format("Имя объекта должно быть определено: {0}", Loader.CurName));

			// получаем элменты с именем тега Client
			List<XmlElement> Src;
			Loader.CurXmlElement.EnumInhElements(out Src, "Client", null, null);
			// загружаем в коллекцию по типу ClientBase
			LoadChildren(Loader, Src, typeof(ClientBase));
		}
	}

	/// <summary>
	/// Сборщик данных с очередью (Реализован метод SetData потокобезопасно)
	/// </summary>
	public class DataCollector : Collector
	{
		/// <summary>
		/// Класс данных для обработки
		/// </summary>
		protected class Data
		{
			/// <summary>
			/// массив данных
			/// </summary>
			public byte[] _data;
			/// <summary>
			/// данные в виде объекта
			/// </summary>
			public object _dataObj;
			/// <summary>
			/// Ссылка на клиента, получившего эти данные
			/// </summary>
			public ClientBase _client;
			/// <summary>
			/// Время, соответсвующее данным
			/// </summary>
			public DateTime _time;
			/// <summary>
			/// Ссылка на элемент, которому эти данные предназначены
			/// </summary>
			public IOBaseElement _targetElement;
		}

		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		public override void SetData(byte[] data, ClientBase client, DateTime time, IOBaseElement targetElement)
		{
			// создаем элемент данных
			Data dataObj = new Data();
			if(data != null)
			{
				dataObj._data = new byte[data.Length];
				Array.Copy(data, dataObj._data, data.Length);
			}
			dataObj._dataObj = null;
			dataObj._client = client;
			dataObj._time = time;
			dataObj._targetElement = targetElement;
			// блокируем очередь
			Monitor.Enter(_dataQueue);
			try
			{
                // добавляем в очередь
                _dataQueue.Enqueue(dataObj);
            }
			finally
			{
				// разблокируем очередь
				Monitor.Exit(_dataQueue);
			}
			// сигнализируем о добавлении
			_dataEvent.Set();
		}

		/// <summary>
		/// Установить данные (в reader) от клиента client с временем time для элемента targetElement
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="client">клиент, передающий данные</param>
		/// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
		/// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
		public override void SetData(object data, ClientBase client, DateTime time, IOBaseElement targetElement)
		{
			// создаем элемент данных
			Data dataObj = new Data();
			dataObj._data = null;
			dataObj._dataObj = data;
			dataObj._client = client;
			dataObj._time = time;
			dataObj._targetElement = targetElement;
			// блокируем очередь
			Monitor.Enter(_dataQueue);
			try
			{
				// добавляем в очередь
				_dataQueue.Enqueue(dataObj);
			}
			finally
			{
				// разблокируем очередь
				Monitor.Exit(_dataQueue);
			}
			// сигнализируем о добавлении
			_dataEvent.Set();
		}

		/// <summary>
		/// Событие для сигнализации о добавлении данных в очередь
		/// </summary>
		protected AutoResetEvent _dataEvent = new AutoResetEvent(false);
		/// <summary>
		/// Очередь данных
		/// </summary>
		protected Queue _dataQueue = new Queue();
		/// <summary>
		/// Пуста ли очередь данных
		/// </summary>
		public bool IsDataQueueEmpty
		{
			get { return _dataQueue.Count == 0; }
		}
	}

	/// <summary>
	/// Сборщик данных на выделенном потоке с ожиданием обновления данных (Требует переопределнения метода OnData)
	/// </summary>
	public abstract class DataCollectorOnThread : DataCollector
	{
		#region Методы для работы на потоке
		/// <summary>
		/// Виртуальный метод, вызываемый при старте потока
		/// </summary>
		/// <returns>true если инициализация прошла успешно</returns>
		protected virtual bool OnInitThread()
		{
			return true;
		}
		/// <summary>
		/// Метод работы потока
		/// </summary>
		protected virtual void Run()
		{
			while(true)
			{
				// ждем события одну секунду
				if(_dataQueue.Count > 0 || _dataEvent.WaitOne(1000, false))
				{
					/////////////////////////////////////
					///Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)42, "Collector processes queue");
					/////////////////////////////////////

					// пока есть хотябы один элемент в очереди
					while(_dataQueue.Count > 0)
					{
						Data data = null;
						// блокируем очередь данных
						Monitor.Enter(_dataQueue);
						try
						{
							// получаем элемент
							data = _dataQueue.Dequeue() as Data;
						}
						finally
						{
							// разблокируем очередь
							Monitor.Exit(_dataQueue);
						}

                        // вызываем обработку
                        if (data != null)
                            OnData(data);
					}
				}
			}
		}

		/// <summary>
		/// Виртуальный метод, вызываемый при корректном завершении потока
		/// </summary>
		protected virtual void OnDestroyThread()
		{
		}

		#endregion

		/// <summary>
		/// Рабоча процедура с защитой от неотловленного исключения - осуществляет повторный запуск
		/// </summary>
		protected void WorkProc()
		{
			while(true)
			{
				try
				{
					Thread.Sleep(3000);
					Trace.WriteLine("Run thread: " + GetType().ToString() + " named " + Name);
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)200, "Запуск потока концентратора " + Name);
					// основная работа
					Run();
				}
				catch(ThreadInterruptedException)
				{
					// завершение работы
					Trace.WriteLine("Interrupted thread: " + GetType().ToString() + " named " + Name);
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)201, "Прерывание потока концентратора " + Name);
					break;
				}
				catch(ThreadAbortException)
				{
					// завершение работы
					Trace.WriteLine("Abort thread: " + GetType().ToString() + " named " + Name);
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Warning, (EventLogID)201, "Принудительная остановка потока концентратора " + Name);
					break;
				}
				catch(Exception e)
				{
					Trace.WriteLine("DataCollectorOnThread catch exception but try to Run again: " + e.ToString());
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)201, "Исключение на потоке концентратора " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
				}
			}
		}

		/// <summary>
		/// Основная процедура потока
		/// </summary>
		public void ThreadProc()
		{
			// вызываем инициализацию
			if(OnInitThread())
			{
				try
				{
					// основная работа
					WorkProc();
				}
				finally
				{
					Trace.WriteLine("End thread: " + GetType().ToString() + " named " + Name);
					// завершение работы
					OnDestroyThread();
				}
			}
		}

		/// <summary>
		/// Абстрактный метод обработки данных
		/// </summary>
		/// <param name="data">новые данные</param>
		protected abstract void OnData(Data data);

		#region Переопределенные методы из DlxObject

		/// <summary>
		/// Переопределена загрузка для загрузки приоритета потока
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_priority = Loader.GetAttributeString("Priority", "Normal");
		}
		/// <summary>
		/// Осуществляется запуск потока
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			base.Create();

			// создаем потока
			_thread = new Thread(new ThreadStart(ThreadProc));
			// задаем приоритет
			switch(_priority)
			{
				case "Normal":
					_thread.Priority = ThreadPriority.Normal;
					break;
				case "AboveNormal":
					_thread.Priority = ThreadPriority.AboveNormal;
					break;
				case "BelowNormal":
					_thread.Priority = ThreadPriority.BelowNormal;
					break;
				case "Lowest":
					_thread.Priority = ThreadPriority.Lowest;
					break;
				case "Highest":
					_thread.Priority = ThreadPriority.Highest;
					break;
			}
			// запускаем поток
			_thread.Start();
		}
		/// <summary>
		/// Осуществляется остановка потока
		/// </summary>
		/*public override void Destroy()
		{
			base.Destroy();
			if (_thread != null )
			{
				// завершаем поток
				_thread.Interrupt();
				// ожидаем завершения потока 10 секунд
				if (!_thread.Join(10000))
				{
					// поток не завершился - принудительно завершаем поток
					_thread.Abort();
				}
			}
		}*/

		#endregion

		#region Данные класса
		/// <summary>
		/// Значение приоритета потока - загружается из xml
		/// </summary>
		protected string _priority = "Normal";
		/// <summary>
		/// Поток, на котором работает обмен
		/// </summary>
		protected Thread _thread;

		#endregion
	}

	/// <summary>
	/// Концентратор - сборщик данных на потоке с возможностью подключения потребителей
	/// </summary>
	public abstract class Concentrator : DataCollectorOnThread
	{
		/// <summary>
		/// Информация о потребителе
		/// </summary>
		public class ConsumerInfo
		{
			/// <summary>
			/// Событие для извещение потребителя о добавлении данных в его очередь
			/// </summary>
			public AutoResetEvent _consumerEvent;
			/// <summary>
			/// Очередь данных потребителя
			/// </summary>
			public Queue _consumerQueue;
			/// <summary>
			/// Требуемый потребителю элемент (может быть равен нулю, тогда нужны все данные)
			/// </summary>
			public IOBaseElement _requiredElement;
		}
		/// <summary>
		/// Добавить потребителя
		/// </summary>
		/// <param name="dataQueue">очередь данных потребителя</param>
		/// <param name="requiredElement">требуемый потребителю элемент (может быть равен нулю, тогда нужны все данные)</param>
		/// <param name="eventOnData">Событие для извещения о добавлении в очередь данных (если равно нулю, то создается новое событие и возвращается в return)</param>
		/// <returns>Возвращается событие для извещения потребителя о помещении в очередь новых данных</returns>
		public AutoResetEvent AddConsumerQueue(Queue dataQueue, IOBaseElement requiredElement, AutoResetEvent eventOnData)
		{
			// заполняем информацию
			ConsumerInfo info = new ConsumerInfo();
			// создаем событие
			if(eventOnData != null)
				info._consumerEvent = eventOnData;
			else
				info._consumerEvent = new AutoResetEvent(false);
			info._consumerQueue = dataQueue;
			info._requiredElement = requiredElement;
			// блокируем массив потребителей
			Monitor.Enter(_consumers);
			try
			{
				// добавляем потребителя
				_consumers.Add(info);
			}
			finally
			{
				Monitor.Exit(_consumers);
			}
			// возвращаем событие
			return info._consumerEvent;
		}

		/// <summary>
		/// Удалить потребителя
		/// </summary>
		/// <param name="dataQueue">очередь данных потребителя</param>
		public void RemoveConsumerQueue(Queue dataQueue)
		{
			// блокируем массив потребителей
			Monitor.Enter(_consumers);
			try
			{
				// добавляем потребителя
				for(int i = _consumers.Count - 1; i >= 0; i--)
				{
					if(_consumers[i]._consumerQueue == dataQueue)
						_consumers.RemoveAt(i);
				}
			}
			finally
			{
				Monitor.Exit(_consumers);
			}
		}

		/// <summary>
		/// Абстрактный метод получения данных по элементу (используется потребителем)
		/// Порожденные классы должны обеспечить потоко безопасность
		/// </summary>
		/// <param name="requiredElement">требуемый элемент (может быть не задан)</param>
		/// <returns>данные</returns>
		public abstract object GetOverallData(IOBaseElement requiredElement);

		/// <summary>
		/// Массив информации о потребителях
		/// </summary>
		protected List<ConsumerInfo> _consumers = new List<ConsumerInfo>();

		/// <summary>
		/// Добавить данные потребителям
		/// </summary>
		/// <param name="data">данные</param>
		/// <param name="ownerElement">элемент, соответствующий данным</param>
		public void AddDataToConsumers(object data, IOBaseElement ownerElement)
		{
			// блокируем массив потребителей
			Monitor.Enter(_consumers);
			try
			{
				foreach(ConsumerInfo info in _consumers)
				{
					if(info._requiredElement == null || info._requiredElement == ownerElement)
					{
						// блокируем очередь данных потребителя
						Monitor.Enter(info._consumerQueue);
						try
						{
							// добавляем данные
							info._consumerQueue.Enqueue(data);
						}
						finally
						{
							Monitor.Exit(info._consumerQueue);
						}
						// извещаем о дабавлении данных
						info._consumerEvent.Set();
					}
				}
			}
			finally
			{
				Monitor.Exit(_consumers);
			}
		}
	}

	/// <summary>
	/// Концентратор данных, выполняющий трансляцию данных подключенным потребителям
	/// </summary>
	public class TransferConcentrator : Concentrator
	{
		/// <summary>
		/// Метод обработки данных - выполняет трансляцию потребителям
		/// </summary>
		/// <param name="data">новые данные</param>
		protected override void OnData(Data data)
		{
			// блокируем массив потребителей
			Monitor.Enter(_consumers);
			try
			{
				foreach(Concentrator.ConsumerInfo info in _consumers)
				{
					// если потребителю требуются все данные или новые данные как раз требуются потребителю
					if(info._requiredElement == null || info._requiredElement == data._targetElement)
					{
						// блокиркем очередб данных потребителя
						Monitor.Enter(info._consumerQueue);
						try
						{
							// добавляем данные в его очередь
							info._consumerQueue.Enqueue(data._data);
						}
						finally
						{
							Monitor.Exit(info._consumerQueue);
						}
						// извещаем о дабавлении данных
						info._consumerEvent.Set();
					}
				}
			}
			finally
			{
				Monitor.Exit(_consumers);
			}
		}

		/// <summary>
		/// Метод получения данных по элементу (используется потребителем)
		/// </summary>
		/// <param name="requiredElement">требуемый элемент (может быть не задан)</param>
		/// <returns>данные</returns>
		public override object GetOverallData(IOBaseElement requiredElement)
		{
			return null;
		}
	}
	/// <summary>
	/// Концентратор с обработкой данных
	/// </summary>
	public class ProcessConcentrator : Concentrator, ISuivComponent
	{
		public event Action<DlxObject, string, int, DateTime?, string> SendState;
		public event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;

		private void SendSiteState(Data data, string message = "")
		{
			string subname = "";
			int siteId;

			if((data._targetElement is DpsOld.IOSysOld) || (data._targetElement is IOSystem))
				subname = "Подсистема ввода";
			else if((data._targetElement is DpsOldControlObjSystem) || (data._targetElement is ControlObjSystem))
				subname = "Подсистема объектов";

			if((data._targetElement as UniObjSystem)?.SiteID == null)
				siteId = (data._targetElement.Owner as Tdm.Site).ID;
			else
				siteId = (int)(data._targetElement as UniObjSystem).SiteID;

			SendState?.Invoke(this, subname, siteId, data._time, message);
		}

		/// <summary>
		/// Метод обработки данных - выполняет обработку элементами
		/// </summary>
		/// <param name="data">новые данные</param>
		protected override void OnData(Data data)
		{
			//Console.WriteLine(string.Format("ProcessConcentrator.OnData d{0} do{1} {2}", data._data, data._dataObj, data._targetElement?.Name));

			if(data._targetElement == null)
			{
				Trace.WriteLine("ProcessConcentrator " + Name + ": Невозможно обработать данные для неизвестной подсистемы");
				return;
			}
			if(!typeof(BaseIOSystem).IsInstanceOfType(data._targetElement))
			{
				Trace.WriteLine("ProcessConcentrator " + Name + ": Обработка выполняется только для подсистем в целом, а получено для " + data._targetElement.Name);
				SendSiteState(data, "Обработка выполняется только для подсистем в целом, а получено для " + data._targetElement.Name);
				return;
			}
			BaseIOSystem iosys = data._targetElement as BaseIOSystem;

			if(data._data != null)
			{
				BinaryReader reader = new BinaryReader(new MemoryStream(data._data));
				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(iosys, data._time);
					// разбор данных
					iosys.ProcessData(reader);
					data._time = iosys.DataTime;
					// известное состояние
					iosys.SetValidData();
					SendSiteState(data);
				}
				catch(Exception e)
				{
					Console.WriteLine("ProcessConcentrator " + Name + ": Ошибка обработки данных для элемента " + iosys.Owner.Name + "/" + iosys.Name + ": " + e.ToString());
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)202, "Ошибка обработки данных для " + iosys.Owner.Name + "/" + iosys.Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					// все элементы в неизвестное сосотяние
					iosys.SetInvalidData();
					SendSiteState(data, "Ошибка обработки данных для " + iosys.Owner.Name + " / " + iosys.Name + ": " + e.Message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			else
			{
				// блокируем элемент
				Monitor.Enter(data._targetElement);
				try
				{
					// обновляем время
					UpdateTime(iosys, data._time);
					// неизвестное сосотяние
					iosys.SetInvalidData();
					string message;
					if(data._dataObj != null && data._dataObj is string)
					{
						message = data._dataObj.ToString();
					}
					else
					{
						message = "Неизвестное состояние";
					}
					SendSiteState(data, message);
				}
				finally
				{
					Monitor.Exit(data._targetElement);
				}
			}
			try
			{
                // обновляем другие данные на основе изменений полученных
                if (iosys.IsValidData)
                {
                    UpdateDataBy(iosys);
                    Console.WriteLine("UpdateDataBy " + iosys.Owner.Name + "/" + iosys.Name);
                }
				//SendSiteState(data);
			}
			catch(Exception e)
			{
				Console.WriteLine("ProcessConcentrator " + Name + ": Ошибка обработки данных для элемента " + iosys.Owner.Name + "/" + iosys.Name + ": " + e.ToString());
				Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)203, "Ошибка на цикле обновления данных по изменению " + iosys.Owner.Name + "/" + iosys.Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
				// все элементы в неизвестное сосотяние
				iosys.SetInvalidData();
				SendSiteState(data, "Ошибка на цикле обновления данных по изменению " + iosys.Owner.Name + "/" + iosys.Name + ": " + e.Message);
			}
		}

		/// <summary>
		/// Обновление времени согласно пришедшим для элемента данным
		/// </summary>
		/// <param name="iosys">подсистема, для которого пришли данные</param>
		/// <param name="time">новое время</param>
		protected virtual void UpdateTime(BaseIOSystem iosys, DateTime time)
		{
			// если время задано
			if(time != DateTime.MinValue && iosys.DataTime != time)
				iosys.DataTime = time;
		}

		/// <summary>
		/// Обновление зависимых данных
		/// </summary>
		/// <param name="iosys">подсистема, для которого пришли данные</param>
		protected virtual void UpdateDataBy(BaseIOSystem iosys)
		{

		}

		/// <summary>
		/// Метод получения данных по элементу (используется потребителем)
		/// </summary>
		/// <param name="requiredElement">требуемый элемент (может быть не задан)</param>
		/// <returns>данные</returns>
		public override object GetOverallData(IOBaseElement requiredElement)
		{
			return null;
		}
	}

	/// <summary>
	/// Клиент получения данных по запросам
	/// </summary>
	public class RequestClientBase : Collector.ClientBaseOnThread
	{
		#region Методы работы на потоке
		/// <summary>
		/// Инициализация потока - подготавливаем запрос
		/// </summary>
		/// <returns></returns>
		public override bool OnInitThread()
		{
			PrepareRequest();
			return true;
		}
		/// <summary>
		/// Определение основного метода работы потока
		/// </summary>
		protected override void Run()
		{
			// открываем сессию
			if(IsNeedRequest())
			{
				Trace.WriteLine("RequestClientBase " + Name + ": Try to open connection first...");
				OpenResource();
			}
			// счетчик полученных сообщений в рамках одного соединения
			_recieveMsgCounter = 0;
			// Всегда
			while(true)
			{
				//string s = string.Format("Start at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);
				try
				{
					// если есть сессия
					if(_session != null)
					{
						// отправляем запрос
						if(!_answerMsg.PartMessage)
						{
							// обновляем таймаут на отправку
							UpdateSessionTimeout(false);
							// получаем сообщение
							_session.SendMessage(_requestMsg);

							/////////////////////////////////////
							//Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)42, "ClientBase sent message");
							/////////////////////////////////////
							//s += string.Format("Sent at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);

							// сообщение отправлень
							OnMessageSended();
						}

						// обновляем таймаут на получение
						UpdateSessionTimeout(true);
						// получаем ответ
						_answerMsg = _session.ReceiveMessage();
						if(_answerMsg.Code != _requestMsg.Code || _answerMsg.SubCode != _requestMsg.SubCode)
							OnWrongAnswerCode();
						// инкрементируем счетчик
						_recieveMsgCounter++;
						//s += string.Format("Rec at \t{0}.{1} ({2})\n", DateTime.Now, DateTime.Now.Millisecond, _recieveMsgCounter);

						// если нужно запрашивать, то вызываем обработку ответа
						if(IsNeedRequest())
							ProcessAnswer();
						else// иначе закрываем сессию
							_session = null;

						//s += string.Format("Proc at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);

						// выдержка времени для посылки следующего запроса
						if(!_answerMsg.PartMessage)
							Thread.Sleep(_rateQuery);
						//s += string.Format("Wake at \t{0}.{1}\n", DateTime.Now, DateTime.Now.Millisecond);
					}
				}
				catch(ThreadInterruptedException)
				{
					return;
				}
				catch(ThreadAbortException)
				{
					return;
				}
				/*catch (TimeoutException e)
				{
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)102, "Исключение на потоке клиента " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					// произошла ошибка - пустые данные коллектору
					OnExchengeError();
				}*/
				catch(Exception e)
				{
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Error, (EventLogID)102, "Исключение на потоке клиента " + Name + ": " + e.Message + "\nSTACK:\n" + e.StackTrace);
					// произошла ошибка - пустые данные коллектору
					OnExchengeError();
					// закрываем сессию
					_session.Close();
					_session = null;

					Trace.WriteLine("RequestClientBase " + Name + " catch exception : " + e.ToString());
				}
				// если нужно перезапустить обмен
				if(_bRestart && _session != null)
				{
					_session.Close();
					_session = null;
				}
				// в любом случае проверяем сесию
				if(_session == null)
				{
					// если нет полученных сообщений и не требуется перезапуск, то выдерживаем время
					if(_recieveMsgCounter < 1 && !_bRestart)
						_restartEvent.WaitOne(_rateOpen, false);

					_recieveMsgCounter = 0;
					_bRestart = false;
					// открываем сессию
					if(IsNeedRequest())
					{
						Trace.WriteLine("RequestClientBase " + Name + ": Try to reopen connection...");
						OpenResource();
					}
				}
				//LogRequestRun(Name, s);
			}
		}
		/// <summary>
		/// Запись в лог-файл полученных сбоев АДК
		/// </summary>
		/// <param name="faults"></param>
		protected void LogRequestRun(string siteName, string s)
		{
			string fName = string.Format("_Run_{0}.txt", siteName);
			System.IO.FileInfo fi = new System.IO.FileInfo(fName);
			System.IO.StreamWriter sw = new System.IO.StreamWriter(fName, true);
			sw.WriteLine("--------------");
			sw.WriteLine(s);
			sw.Flush();
			sw.Close();
		}
		/// <summary>
		/// При завершении потока завершаем сессию
		/// </summary>
		protected override void OnDestroyThread()
		{
			if(_session != null)
				_session.Close();

			_session = null;
		}

		/// <summary>
		/// Подготовка запроса
		/// </summary>
		protected virtual void PrepareRequest()
		{
			_requestMsg.Code = RequestCode.CurrentState;
			_requestMsg.Dest = _dest;
		}
		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected virtual void ProcessAnswer()
		{
		}
		/// <summary>
		/// Сообщение послано
		/// </summary>
		protected virtual void OnMessageSended()
		{
		}
		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected virtual void OnExchengeError()
		{
		}
		/// <summary>
		/// Обработчик неверного заголовка
		/// </summary>
		protected virtual void OnWrongAnswerCode()
		{
			RequestException.ThrowRequestException(_answerMsg);
		}
		/// <summary>
		/// Открытие сессии
		/// </summary>
		protected void OpenResource()
		{
			try
			{
				_answerMsg.PartMessage = false;
				_session = new ClientSession(_connFactory.CreateConnection());
				UpdateSessionTimeout(false);
				Trace.WriteLine("RequestClientBase " + Name + ": OK to open connection!");

				if((DateTime.Now - _sessionStartTime).TotalMinutes > 1)
				{   // если с момента предыдущего подключения прошло более минуты - пишем в лог
					Dps.AppDiag.EventLog.AppEventLog.WriteText(EventLogSeverity.Info, (EventLogID)100, "Выполнена установка соединения клиента " + Name);
				}
				_sessionStartTime = DateTime.Now;
			}
			catch(Exception e)
			{
				_session = null;
				Trace.WriteLine("RequestClientBase " + Name + " open client session exception : " + e.ToString());
				// произошла ошибка - пустые данные коллектору
				OnExchengeError();
			}
		}

		/// <summary>
		/// Требуется ли запрашивать данные
		/// </summary>
		public virtual bool IsNeedRequest()
		{
			return GetOwnerCollector() != null;
		}

		#endregion

		/// <summary>
		/// Переопределена загрузка для получения фабрики соединений
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_findRoot = Loader.GetObjectFromAttribute("FindRoot") as DlxObject;

			if(_findRoot != null)
			{
				// загружаем ссылку на фабрику соединений
				_connectionPath = Loader.GetAttributeString("Connection", string.Empty);
				_connFactory = Loader.GetObject(_connectionPath, typeof(ConnectionFactory), null, false) as ConnectionFactory;
			}
			else
			{
				// загружаем ссылку на фабрику соединений
				_connFactory = Loader.GetObjectFromAttribute("Connection") as ConnectionFactory;
			}

			// считываем частоту запросов
			_rateQuery = Loader.GetAttributeInt("QueryRate", 3000);
			// считываем частоту открытия соединения
			_rateOpen = Loader.GetAttributeInt("OpenRate", 30000);
			// считываем имя источника
			_dest = Loader.GetAttributeString("Dest", string.Empty);
			// считываем таймауты
			_ReceiveTimeout = Loader.GetAttributeInt("ReceiveTimeout", _ReceiveTimeout);
			_SendTimeout = Loader.GetAttributeInt("SendTimeout", _SendTimeout);
		}
		/// <summary>
		/// Переобределно для поиска фабрики соединений
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if(_connFactory == null && _findRoot != null)
				_connFactory = _findRoot.FindObject(_connectionPath) as ConnectionFactory;

			if(_connFactory == null)
			{
				throw new ArgumentException("RequestClientBase " + Name + ": Не найдена фабрика соединений по значению атрибута Connection = " + _connectionPath);
			}

			_connectionPath = string.Empty;

			base.Create();
		}
		/// <summary>
		/// Установить таймауты
		/// </summary>
		/// <param name="receiveTimeout">таймаут на получение</param>
		/// <param name="sendTimeout">таймаут на отправку</param>
		public void SetTimeouts(int receiveTimeout, int sendTimeout)
		{
			_ReceiveTimeout = receiveTimeout;
			_SendTimeout = sendTimeout;
			UpdateSessionTimeout(false);
		}
		/// <summary>
		/// Обновить таймаут
		/// <param name="isForReceive">isForReceive==true - нужен таймаут на получение</param>
		/// </summary>
		public void UpdateSessionTimeout(bool isForReceive)
		{
			if(_session != null)
			{
				if(_session.Connection.CanTimeout)
					_session.Connection.Timeout = isForReceive ? _ReceiveTimeout : _SendTimeout;
			}
		}

		#region Данные
		/// <summary>
		/// Фабрика соединений
		/// </summary>
		protected ConnectionFactory _connFactory = null;
		/// <summary>
		/// Сессия открытого соединения
		/// </summary>
		protected ClientSession _session = null;
		/// <summary>
		/// Сообщение для запроса
		/// </summary>
		protected Message _requestMsg = new Message();
		/// <summary>
		/// Сообщение для ответа
		/// </summary>
		protected Message _answerMsg = new Message();
		/// <summary>
		/// Частота запросов в миллисекундах
		/// </summary>
		protected int _rateQuery;
		/// <summary>
		/// Частота открытия соединения в миллисекундах
		/// </summary>
		protected int _rateOpen;
		/// <summary>
		/// имя источника информации
		/// </summary>
		protected string _dest = string.Empty;
		/// <summary>
		/// Объект - корень для поиска фабрики соединений, подсистемы ввода и подсистемы объектов
		/// </summary>
		protected DlxObject _findRoot = null;
		/// <summary>
		/// Путь от корня для поиска фабрики соединений
		/// </summary>
		protected string _connectionPath = string.Empty;
		/// <summary>
		/// Признак перезапуска обмена (может быть установлен на другом потоке)
		/// </summary>
		protected bool _bRestart = false;
		/// <summary>
		/// Счетчик сообщений в рамках открытой сессии
		/// </summary>
		protected int _recieveMsgCounter = 0;
		/// <summary>
		/// Время открытия сессии
		/// </summary>
		protected DateTime _sessionStartTime = DateTime.MinValue;
		/// <summary>
		/// признак первого ответа
		/// </summary>
		public bool IsFirstAnswer
		{
			get { return _recieveMsgCounter == 1; }
		}

		/// <summary>
		/// Событие для сигнализации о начале запроса
		/// </summary>
		protected AutoResetEvent _restartEvent = new AutoResetEvent(false);
		/// <summary>
		/// значение тайм аута на получение
		/// </summary>
		protected int _ReceiveTimeout = 1000 * 60;
		/// <summary>
		/// значение таймаута на отправку
		/// </summary>
		protected int _SendTimeout = 1000 * 5;

		#endregion

	}

	/// <summary>
	/// Клиент получения данных от новой системы по одной подсистеме
	/// </summary>
	class RequestClient : RequestClientBase
	{
		#region Методы работы на потоке

		public override bool OnInitThread()
		{
			Thread.Sleep(20000);
			return base.OnInitThread();
		}
		/// <summary>
		/// Подготовка запроса
		/// </summary>
		protected override void PrepareRequest()
		{
			_requestMsg.Code = RequestCode.CurrentState;
			_requestMsg.SubCode = 0x2;
			_requestMsg.Dest = _dest;
		}
		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected override void ProcessAnswer()
		{

			if(_answerMsg.Data.Length == 0 || _answerMsg.Code != _requestMsg.Code || _answerMsg.SubCode != _requestMsg.SubCode)
			{
				Trace.WriteLine("RequestClient " + Name + ": Получен неверный ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetIOsys.Owner.Name + ": размер данных = " + _answerMsg.Data.Length);
				SetData(null, DateTime.MinValue, _targetIOsys);
				return;
			}
			Trace.WriteLine("RequestClient " + Name + ": Получен правильный ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetIOsys.Owner.Name + ": размер данных = " + _answerMsg.Data.Length);

			// передаем данные коллектору
			SetData(_answerMsg.Data, DateTime.MinValue, _targetIOsys);
		}
		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected override void OnExchengeError()
		{
			// произошла ошибка - пустые данные коллектору
			SetData(null, DateTime.MinValue, _targetIOsys);
		}

		protected override void OnWrongAnswerCode()
		{
		}
		#endregion

		/// <summary>
		/// Переопределена загрузка для получения фабрики соединений
		/// </summary>
		/// <param name="Loader">Загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			if(_findRoot != null)
			{
				// загружаем ссылку на подсистему ввода
				_iosysPath = Loader.GetAttributeString("IOSys", string.Empty);
			}
			else
			{
				// загружаем ссылку на подсистему ввода
				_targetIOsys = Loader.GetObjectFromAttribute("IOSys") as IOBaseElement;
			}

		}
		/// <summary>
		/// Переопределено для поиска фабрики соединений, подсистемы ввода и подсистемы объектов
		/// </summary>
		/// <returns>удачно ли прошло создание</returns>
		public override void Create()
		{
			if(_targetIOsys == null && _findRoot != null)
				_targetIOsys = _findRoot.FindObject(_iosysPath) as IOBaseElement;


			if(_targetIOsys == null)
			{
				throw new ArgumentException("DpsOldRequestClient " + Name + ": Не найдена подсистема ввода по значению атрибута IOSys = " + _iosysPath);
			}

			_iosysPath = string.Empty;
			base.Create();
		}

		#region Данные

		/// <summary>
		/// Элемент подсистемы ввода
		/// </summary>
		protected IOBaseElement _targetIOsys;
		/// <summary>
		/// Путь от корня для поиска подсистемы ввода
		/// </summary>
		protected string _iosysPath = string.Empty;

		#endregion

	}



}
