﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
using System.Linq;
using Ivk.IOSys;
using AfxEx;

namespace Tdm.Unification
{
	/// <summary>
	/// Класс, облегчающий загрузку параметров соединения с базой и создания соединения
	/// </summary>
	public class DBConnector : DlxObject
	{
		/// <summary>
		/// Конструктор
		/// </summary>
        public DBConnector()
        { }
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="name"></param>
		public DBConnector(string name)
		{
			this.m_Name = name;
		}
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="name"></param>
		/// <param name="connectionString"></param>
		public DBConnector(string name, string connectionString)
		{
			this.m_Name = name;
			_connectionString = connectionString;
		}
		/// <summary>
		/// Проверка наличия конфигурации
		/// </summary>
		public bool IsConfigurated
		{
			get { return _connectionString.Length > 0; }
		}
		/// <summary>
		/// Строка подключения
		/// </summary>
		protected string _connectionString = string.Empty;
		/// <summary>
		/// Строка подключения
		/// </summary>
		public string ConnectionString
		{
			get { return _connectionString; }
		}
		/// <summary>
		/// Соединение с БД
		/// </summary>
		protected SqlConnection _sqlConn = new SqlConnection();
		/// <summary>
		/// Получить соединения
		/// </summary>
		public SqlConnection SqlConn
		{
			get { return _sqlConn; }
		}
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			if (!Loader.GetAttributeValue("connectionString", ref _connectionString))
			{
				string strServer = string.Empty, strSQLserver = string.Empty;
				// загрузка данных о БД
				if (Loader.GetAttributeValue("Server", ref strServer) && Loader.GetAttributeValue("SQLServer", ref strSQLserver))
				{
					string strDBname = Loader.GetAttributeString("DBName", "TestDB");
					string strUser = Loader.GetAttributeString("User", "sa");
					string strPswd = Loader.GetAttributeString("Pswd", "1234567890");
					_connectionString = "server=" + strServer + "\\" + strSQLserver + "; user id=" + strUser +
												   "; password=" + strPswd + "; initial catalog = " + strDBname;
				}
			}
			else
			{
				try
				{
					ConnectionStringSettings css = ConfigurationManager.ConnectionStrings[_connectionString];
					_connectionString = css.ConnectionString.Trim();
				}
				catch
				{
					Trace.WriteLine(Name + ": Use connectionString as connection string.");
				}
			}
		}
		/// <summary>
		/// Соединиться с БД
		/// </summary>
		public bool Connect()
		{
			if (IsConnected)
				return true;
			Monitor.Enter(_sqlConn);
			try
			{
				if (IsConnected)
					return true;
				_sqlConn.ConnectionString = _connectionString;
				_sqlConn.Open();
			}
			catch (Exception ex)
			{
                Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)500, "DBConnector " + Name + ": ERROR open BD by connection string:" + _connectionString + ": " + ex.Message);
                Console.WriteLine("DBConnector " + Name + ": ERROR open BD by connection string:" + _connectionString + ": " + ex.Message);
                Disconnect();
			}
			finally
			{
				Monitor.Exit(_sqlConn);
			}
			return IsConnected;
		}
		/// <summary>
		/// Закрыть соединение
		/// </summary>
		public void Disconnect()
		{
			_sqlConn.Close();
		}
		/// <summary>
		/// Открыто ли соединение
		/// </summary>
        public bool IsConnected
        { get { return _sqlConn.State == ConnectionState.Open; } }

        /// <summary>
        /// Типы сообщений
        /// </summary>
        public enum TypeExchangeInfo
        {
            /// <summary>
            /// Текущее состояние
            /// </summary>
            CS,
            /// <summary>
            /// ДС (протокол)
            /// </summary>
            DS,
            /// <summary>
            /// Нормаль
            /// </summary>
            Norm
        }
	}

    /// <summary>
    /// Соединение с базой. Использует строку подключения их общих объектов
    /// </summary>
    public class DBConnectorCmn: DBConnector
    {
        public DBConnectorCmn()
        {
            _AttrNameConnString = "ConnString";
        }

        public DBConnectorCmn(string attrName)
        {
            _AttrNameConnString = attrName;
        }

        /// <summary>
        /// Имя атрибута, которым задается ссылка на объект строки подключения
        /// </summary>
        protected string _AttrNameConnString;

        /// <summary>
        /// Загрузка _connectionString из объекта по атрибуту "ConnString"
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        public override void Load(DlxLoader Loader)
        {
            _connectionString = string.Empty;

            if (!Loader.GetAttributeValue("connectionString", ref _connectionString))
            {
                StringValue ConnString = Loader.GetObjectFromAttribute("ConnString") as StringValue;
                if (ConnString != null)
                    _connectionString = ConnString.Value;
            }
            if(_connectionString.Length == 0)
            {
                string strServer = string.Empty, strSQLserver = string.Empty;
                // загрузка данных о БД
                if (Loader.GetAttributeValue("Server", ref strServer) && Loader.GetAttributeValue("SQLServer", ref strSQLserver))
                {
                    string strDBname = Loader.GetAttributeString("DBName", "TestDB");
                    string strUser = Loader.GetAttributeString("User", "sa");
                    string strPswd = Loader.GetAttributeString("Pswd", "1234567890");
                    _connectionString = "server=" + strServer + "\\" + strSQLserver + "; user id=" + strUser +
                                                   "; password=" + strPswd + "; initial catalog = " + strDBname;
                }
            }
            else
            {
                try
                {
                    ConnectionStringSettings css = ConfigurationManager.ConnectionStrings[_connectionString];
                    _connectionString = css.ConnectionString.Trim();
                }
                catch
                {
                    Trace.WriteLine(Name + ": Use connectionString as connection string.");
                }
            }
        }
    }
	/// <summary>
	/// Поток записи динамического протокола унифицированного обмена
	/// </summary>
	public class UniProtRecorder : DataCollectorOnThread
	{
        private UdpDiagnostic _UdpDiag;
        /// <summary>
        //// Количество ошибок вставки/обновления в БД
        /// </summary>
        private int _SqlErrCount = 0;
        /// <summary>
        //// Количество ошибок вставки/обновления в БД
        /// </summary>
        private int SqlErrCount
        {
            get { return _SqlErrCount; }
            set { _SqlErrCount = value; _UdpDiag?.FixState(this, DiagState); } 
        }
        /// <summary>
        /// Количество успешных попыток вставки записей в БД
        /// </summary>
        private int _SqlOkExecCount = 0;
        /// <summary>
        /// Количество успешных попыток вставки записей в БД
        /// </summary>
        private int SqlOkExecCount
        {
            get { return _SqlOkExecCount; }
            set { _SqlOkExecCount = value; _UdpDiag?.FixState(this, DiagState); }
        }
        /// <summary>
        /// Время задержки выполенния запроса
        /// </summary>
        private TimeSpan _MaxDelayExecute = new TimeSpan();

	    private TimeSpan MaxDelayExecute
	    {
	        get { return _MaxDelayExecute; }
	        set { _MaxDelayExecute = value;_UdpDiag?.FixState(this,DiagState); }
	    }

	    /// <summary>
        /// Вывод статистики
        /// </summary>
        private string DiagState
        {
            get
            {
                return "Err=" + _SqlErrCount + "\n OKExec=" + _SqlOkExecCount +  "\n QueueCount=" + _dataQueue.Count +
                      "\n MaxDelayExecute="+_MaxDelayExecute;
            }
        }

	    #region Данные
		DBConnector _dbconnector = new DBConnector();

		/// <summary>
		/// Таймер для открытия БД
		/// </summary>
		protected Timer _connectTimer = null;
        /// <summary>
        /// Ссылка на концентратор
        /// </summary>
        protected Concentrator _concentrator = null;
		/// <summary>
		/// Время разрыва соединения
		/// </summary>
		protected DateTime _disconnectTime = DateTime.MinValue;
		/// <summary>
		/// Время подключения
		/// </summary>
		protected DateTime _connectTime = DateTime.MinValue;
		/// <summary>
		/// признак необходимости записи времения подключения
		/// </summary>
		protected bool _writeConnectTime = true;
		/// <summary>
		/// период записи полного состояния объектов в минутах
		/// </summary>
		protected int _periodOverallRecord_min = 10;
		/// <summary>
		/// последние данные на которых произошла ошибка
		/// </summary>
		protected object _lastExeptData = null;
		/// <summary>
		/// Кол-во записей в локальных таблицах, при превышении которого выполняется bulk-запись в БД
		/// </summary>
		protected int _maxRowsInLocalTable = 10000;
		/// <summary>
		/// Таймаут записи локальной таблицы в БД (по умолчанию - 1 минута)
		/// </summary>
		protected int _bulkCopyTimeout = 60;
		/// <summary>
		/// Максимально допустимая длина очереди
		/// </summary>
		protected int _maxTableQueueLength = 100;
        /// <summary>
        /// Признак использования альтернативной таблицы унифицированных параметров
        /// </summary>
        protected bool _useAltParamsTable = false;
        /// <summary>
        /// Признак логирования работы с альтернативной таблицей унифицированных параметров
        /// </summary>
        protected bool _logAltParams = false;

		/// <summary>
		/// ссылка на коллекцию контролируемых станций
		/// </summary>
		protected DlxCollection _siteList = null;
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<uint, UniObjSystem> _idsysTOrefMAP = new Dictionary<uint, UniObjSystem>();
        /// <summary>
        /// Использовать ли обновление старых состояний
        /// </summary>
        private bool _useUpdateOld = true;

	    /// <summary>
	    /// Записывать ли измерения (числовые) в ParamsNumericUni
	    /// </summary>
	    private bool _ParamsNumericWriter = false;

	    /// <summary>
	    /// Записывать ли нечисловые параметры в  ARH_ParamsDataUni
	    /// </summary>
	    private bool _ParamsDataWriter = false;

	    /// <summary>
	    /// Записывать ли состояния объектов в ARH_StatesDYN
	    /// </summary>
	    private bool _StateWriter = false;

	    /// <summary>
	    /// Записывать ли диагностические ситуации в ARH_States
	    /// </summary>
	    private bool _DSWriter = false;

	    /// <summary>
	    /// Записывать ли факты выполнения и план ТОиР в ARH_TO_Detected и ARH_TOPlanWorks
	    /// </summary>
	    private bool _TOWriter = false;

		/// <summary>
		/// Очистить перечень контролируемых станций
		/// </summary>
		public void RemoveAllSites()
		{
			_idsysTOrefMAP.Clear();
		}
		/// <summary>
		/// Добавить станцию на контроль
		/// </summary>
		/// <param name="SiteID">идентификатор станции</param>
		/// <param name="ObjSys">ссылка на подсистему объектов</param>
		public void AddSite(uint SiteID, UniObjSystem ObjSys)
		{
            if (!_idsysTOrefMAP.ContainsKey(SiteID))
                _idsysTOrefMAP.Add(SiteID, ObjSys);
   
		}
        

		private const string _tName_StatesDYN = "ARH_StatesDYN";
		private const string _tName_UniParams = "ARH_ParamsNumericUni";
        private string _tName_UniParams_alt = "ARH_Trafic";

		/// <summary>
		/// Текущая локальная таблица динамических состояний
		/// </summary>
		private DataTable _local_ARH_StatesDYN = new DataTable(_tName_StatesDYN);
		/// <summary>
		/// Текущая локальная таблица измерений
		/// </summary>
		private DataTable _local_ARH_ParamsNumericUni = new DataTable(_tName_UniParams);
		/// <summary>
		/// Очередь локальных таблиц динамических состояний
		/// </summary>
		private Queue<DataTable> _q_StatesDYN = new Queue<DataTable>();
		/// <summary>
		/// Очередь локальных таблиц измерений
		/// </summary>
		private Queue<DataTable> _q_UniParams = new Queue<DataTable>();

		private DataTable CreateLocalTable(string TableName)
		{
			if (TableName == _tName_StatesDYN)
				return CreateLocalTable_StatesDYN();
            else if (TableName == _tName_UniParams)
				return CreateLocalTable_UniParams();
			else return null;
		}
		/// <summary>
		/// Создать локальную таблицу динамических состояний
		/// </summary>
		private DataTable CreateLocalTable_StatesDYN()
		{
			DataTable tDYN = new DataTable(_tName_StatesDYN); ;

			DataColumn columnID = new DataColumn();
			columnID.DataType =	System.Type.GetType("System.Int64");
			columnID.ColumnName = "ID";
			columnID.AutoIncrement = true;
			tDYN.Columns.Add(columnID);
			tDYN.PrimaryKey = new DataColumn[] { columnID };

			DataColumn col = new DataColumn("Site_ID", Type.GetType("System.Int32"));
			col.Unique = false;
			tDYN.Columns.Add(col);
			col = new DataColumn("Object_ID", Type.GetType("System.Int32"));
			col.Unique = false;
			tDYN.Columns.Add(col);
			col = new DataColumn("State_ID", Type.GetType("System.Int32"));
			col.Unique = false;
			tDYN.Columns.Add(col);
			col = new DataColumn("PrevState_ID", Type.GetType("System.Int32"));
			col.Unique = false; 
			tDYN.Columns.Add(col);
			col = new DataColumn("Time", Type.GetType("System.DateTime"));
			col.Unique = false;
			tDYN.Columns.Add(col);

			return tDYN;
		}
		/// <summary>
		/// Создать локальную таблицу измерений
		/// </summary>
		private DataTable CreateLocalTable_UniParams()
		{
			DataTable tPrm = new DataTable(_tName_UniParams); ;

			DataColumn columnID = new DataColumn();
			columnID.DataType = System.Type.GetType("System.Int64");
			columnID.ColumnName = "ID";
			columnID.AutoIncrement = true;
			tPrm.Columns.Add(columnID);
			tPrm.PrimaryKey = new DataColumn[] { columnID };

			DataColumn col = new DataColumn("Site_ID", Type.GetType("System.Int32"));
			col.Unique = false;
			tPrm.Columns.Add(col);
			col = new DataColumn("Param_ID", Type.GetType("System.Int32"));
			col.Unique = false;
			tPrm.Columns.Add(col);
			col = new DataColumn("Time", Type.GetType("System.DateTime"));
			col.Unique = false;
			tPrm.Columns.Add(col);
			col = new DataColumn("Value", Type.GetType("System.Single"));
			col.Unique = false;
			tPrm.Columns.Add(col);

			return tPrm;
		}

		protected DateTime _lastDataTime = DateTime.MinValue;
		protected DateTime _timeOverallStateRecord = DateTime.MinValue;

        /// <summary>
        /// Сброс времени записи полного состояния
        /// </summary>
        public void ClearTimes()
		{ 
			_lastDataTime = DateTime.MinValue;
			_timeOverallStateRecord = DateTime.MinValue;
		}

		#endregion

		#region Переопределенные методы из DlxObject

		/// <summary>
		/// Переопределена загрузка для получения информации о БД
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			_dbconnector.Load(Loader);

            _concentrator = Loader.GetObjectFromAttribute("Concentrator") as Concentrator;
			_periodOverallRecord_min = Loader.GetAttributeInt("OverallRecordPeriod", _periodOverallRecord_min);

			_maxRowsInLocalTable = Loader.GetAttributeInt("MaxRowsInLocalTable", _maxRowsInLocalTable);
			_bulkCopyTimeout = Loader.GetAttributeInt("BulkCopyTimeout", _bulkCopyTimeout);
			_maxTableQueueLength = Loader.GetAttributeInt("MaxTableQueueLength", _maxTableQueueLength);
            _useAltParamsTable = Loader.GetAttribute("AltParamsTable", out _tName_UniParams_alt);
            _logAltParams = Loader.GetAttributeBool("LogAltParams", false);
            _ParamsNumericWriter = Loader.GetAttributeBool("WriteParamsNumeric", false);
            _ParamsDataWriter = Loader.GetAttributeBool("WriteParamsData", false);
            _StateWriter = Loader.GetAttributeBool("WriteState", false);
            _DSWriter = Loader.GetAttributeBool("WriteDS", false);
            _TOWriter = Loader.GetAttributeBool("WriteTO", false);
			// получаем коллекцию станций
			_siteList = Loader.GetObjectFromAttribute("SiteList", typeof(DlxCollection)) as DlxCollection;
            _useUpdateOld = Loader.GetAttributeBool("UseUpdateOld", true);
            _UdpDiag = Loader.GetObjectFromAttribute("Diag", typeof(UdpDiagnostic)) as UdpDiagnostic;
        }
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <returns>true в случае успеха</returns>
		public override void Create()
		{
			if (!_dbconnector.IsConfigurated)
			{
				throw new ArgumentException("UniProtRecorder: не заданы параметры подключения к БД");
			}

			// создаем локальные таблицы
			_local_ARH_StatesDYN = CreateLocalTable_StatesDYN();
			_local_ARH_ParamsNumericUni = CreateLocalTable_UniParams();
			// заводим таймер
			TimerCallback timerDelegate = new TimerCallback(this.OnConnectTimer);
			_connectTimer = new Timer(timerDelegate, null, 1000, 3*60000);// первый раз через секунду и затем с интервалов в 3 минуты

			_concentrator.AddConsumerQueue(_dataQueue, null, _dataEvent);

			////////////////////////////////////////////////////////////////////////
			// поиск в первом уровне вложенности
			foreach (DlxObject obj in _siteList.Objects)
			{
				UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
                if (objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
					_idsysTOrefMAP.Add(objSys.SiteID, objSys);
			}
			// если ничего не нашли
			if (_idsysTOrefMAP.Count == 0)
			{
				// поиск во втором уровне вложенности
				foreach (DlxCollection subcoll in _siteList.Objects)
				{
					if (subcoll != null)
					{
						foreach (DlxObject obj in subcoll.Objects)
						{
							UniObjSystem objSys = obj.GetObject("Objects") as UniObjSystem;
                            if (objSys != null && !_idsysTOrefMAP.ContainsKey(objSys.SiteID))
								_idsysTOrefMAP.Add(objSys.SiteID, objSys);
						}
					}
				}
			}
			////////////////////////////////////////////////////////////////////////

			base.Create();
		}
		/// <summary>
		/// Завершение работы
		/// </summary>
		public override void NotifyPostDestroy()
		{
			// закрываем соединение
			Disconnect();
            base.NotifyPostDestroy();
		}

		#endregion

		#region Методы, вызываемые на потоке

		/// <summary>
		/// Метод работы потока
		/// </summary>
		protected override void Run()
		{
			// задерживаем запись данных в БД на 20 секунд после старта приложения 
			// (этого должно хватить на установление связи с доступными источниками)
			Thread.Sleep(20000);
			bool bNeedUpdateOldCurrentDiagsInDB = true;
			while (true)
			{
				// ждем события одну секунду
				if (_dataQueue.Count > 0 || _dataEvent.WaitOne(1000, false))
				{
                    if (_UdpDiag != null)
                        _UdpDiag.WriteLog("Try to process queue " + _dataQueue.Count);
					while (_dataQueue.Count > 0)
					{	// пока есть хотя бы один элемент в очереди
						if (((TimeSpan)(_lastDataTime - _timeOverallStateRecord)).TotalMinutes >= _periodOverallRecord_min)
						{	// один раз в _periodAverallRecord_min минут добавляем в очередь состояния всех объектов и параметров
							MakeOverallStateRecord();
							_timeOverallStateRecord = _lastDataTime;
							bNeedUpdateOldCurrentDiagsInDB = true;
						}

						if (!IsConnected && _dataQueue.Count < int.MaxValue / 2)
						{	// если не подключены к базе, то буфферизируем не более int.MaxValue изменений и спим 5 секунд
							Thread.Sleep(5000);
							continue;
						}

						object data = null;
						// блокируем очередь данных
						Monitor.Enter(_dataQueue);
						try
						{	// получаем элемент
							data = _dataQueue.Peek();
						}
						finally
						{	// разблокируем очередь
							Monitor.Exit(_dataQueue);
						}

                        // Пытаемся обработать очередные данные
						if (OnDataObject(data))
						{	// если успешно обработали, то удаляем элемент из очереди
							// блокируем очередь данных
							Monitor.Enter(_dataQueue);
							try
							{	// удаляем обработанный элемент
								_dataQueue.Dequeue();
							}
							finally
							{	// разблокируем очередь
								Monitor.Exit(_dataQueue);
							}
						}
						BulkCopyData(_maxRowsInLocalTable);
                        _UdpDiag?.FixState(this, DiagState);
					}
					// обработана вся очередь - копируем все записи из текущих локальных таблиц
					BulkCopyData(0);
				}
				// здесь уже все полученные данные обработаны и занесены в БД
				// нужно подчистить текущие отказы, которые остались в результате предыдущей остановки процесса
                if(_useUpdateOld)
				    if (bNeedUpdateOldCurrentDiagsInDB && UpdateOldCurrentDiagsInDB("ARH_States") && UpdateOldCurrentDiagsInDB("ARH_StatesFalse"))
					    bNeedUpdateOldCurrentDiagsInDB = false;
			}
		}
		/// <summary>
		/// Обновление старых сбоев
		/// </summary>
		protected bool UpdateOldCurrentDiagsInDB(string tableName)
		{
			if (IsConnected)
			{
				bool bRes = true;
				SqlDataReader reader = null;
				Monitor.Enter(_dbconnector.SqlConn);
				try
				{
					// нужно посмотреть на активные сбои всех объектов и те в БД, которых нет в этом списке установить время окончания
					SqlCommand sql = new SqlCommand();
					sql.Connection = _dbconnector.SqlConn;
					sql.CommandText = string.Format("SELECT ID, Site_ID, Object_ID, State_ID, Time FROM {0} WHERE (EndTime IS NULL) AND IsDiag=1", tableName);
					
					int SiteID, ObjID, StateID;
					long ID;
					DateTime BegTime, EndTime = DateTime.Now;

					SqlDataAdapter da = new SqlDataAdapter(sql);
					DataSet ds = new DataSet();
					da.Fill(ds);
					DataTable tbsites = ds.Tables[0];

					foreach (DataRow row in tbsites.Rows)
					{
						if (row[1] == null)
							continue;
						SiteID = Convert.ToInt32(row[1]);
						ObjID = Convert.ToInt32(row[2]);
						StateID = Convert.ToInt32(row[3]);
						BegTime = Convert.ToDateTime(row[4]);
						ID = Convert.ToInt64(row[0]);

						UniObjSystem objSys = null;
						try
						{
							objSys = _idsysTOrefMAP[(uint)SiteID];
							UniObject obj = objSys.UniObjsDictionary[ObjID] as UniObject;
							// если есть объект и такая ситуация активна вне зависимости от времени начала, то ничего не делаем
							if (obj != null && (obj._diagStateTOtimeMAPuni.ContainsKey(StateID)))
							{
								continue;// такая ситуация еще активна
							}
							else // такой ситуации нет
							{
								// чтобы отказ не повис устанавливаем текущее время окончания
								EndTime = objSys.DataTime;
							}
						}
						catch (KeyNotFoundException)
						{
							// либо нет такой станции, либо нет такого объекта
							// в любом случае ситуацию нужно завершить
							if (objSys != null)
								EndTime = objSys.DataTime;
							else
								continue;
						}
						
                        if (EndTime == DateTime.MinValue)
                            EndTime = DateTime.Now;
                        // обновляем запись в таблице
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _dbconnector.SqlConn;
                        cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
                        cmd.Parameters["@EndTime"].Value = EndTime;

                        cmd.CommandText = string.Format("UPDATE {0} SET EndTime=@EndTime WHERE ID={1};", tableName, ID);
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqlException ex)
                        {
                            bRes = false;
                            Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)503, "Ошибка завершения старого сбоя в " + tableName + ":" + string.Format("Site={0}, Obj={1}, State={2} - ", SiteID, ObjID, StateID) + ex.Message);
                            if (ex.Class >= 20)// ошибка связи с БД - нужно закрыть соединение
                            {
                                Disconnect();
                                return false;
                            }
                        }
                    }

					return bRes;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("UniProtRecorder " + Name + ": ERROR store to DB: " + ex.Message);
					Disconnect();
					return false;
				}
				finally
				{
					if (reader != null)
						reader.Close();
					Monitor.Exit(_dbconnector.SqlConn);
				}
			}
			else
				return false;
		}
		/// <summary>
		/// Обработка одного изменения 
		/// </summary>
		/// <param name="data"></param>
		protected virtual bool OnDataObject(object data)
		{
			if (IsConnected)
			{
				Monitor.Enter(_dbconnector.SqlConn);
				try
				{
					UnificationConcentrator.UniDataItem item = data as UnificationConcentrator.UniDataItem;
					// если данные с неустановленным временем, то игнорируем

					if (item._dataTime == DateTime.MinValue)
						return true;
					// запоминаем последнее время
					_lastDataTime = item._dataTime;
					if (_timeOverallStateRecord == DateTime.MinValue)
						_timeOverallStateRecord = item._dataTime;
					// если следует сделать запись о восстановлении				
					if (_writeConnectTime)
					{
						// запоминаем время разрыва
						if (item != null)
							_connectTime = item._dataTime;
						else
							_connectTime = DateTime.Now;
						// записываем в БД
						WriteConnectionTimeToDB();
						// записали => больше не пишем - сбрасываем флаг
						_writeConnectTime = false;
					}
                    DateTime before = DateTime.Now;
                    // формируем команду
                    ExecuteSqlCommands(data);
                    DateTime after = DateTime.Now;
                    TimeSpan befaf = after - before;
                    if (MaxDelayExecute < befaf)
                    {
                        MaxDelayExecute = befaf;

                    }
					// удачно обработали
				    SqlOkExecCount++;
					return true;
				}
				catch (SqlException ex)
				{
					UnificationConcentrator.UniDataItem item = data as UnificationConcentrator.UniDataItem;
					string addstr = string.Empty;
					if (item != null)
					{
						addstr += " Site=" + item._siteID + ", obj=" + item._objectID + item._dataTime.ToString("; yyyy.MM.dd HH:mm:ss");
						if (item is UnificationConcentrator.UniDiagState)
							addstr += ", State_ID=" + (item as UnificationConcentrator.UniDiagState)._uniDiagState;
					}

                    SqlErrCount++;
                    //коммент Баглаева    _UdpDiag.WriteLog("Ошибка записи в БД: " + ex.Message + addstr + ". Всего записей в очереди: " + _dataQueue.Count);

					Trace.WriteLine("UniProtRecorder " + Name + ": ERROR store to DB: sqlex - " + ex.Message);
					if (ex.Class >= 20)
					{	// ошибка связи с БД - нужно закрыть соединение
				//коммент Баглаева		
                        Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)501, "Ошибка записи в БД: " + ex.Message + addstr + ". Всего записей в очереди: " + _dataQueue.Count);
						Disconnect();
					}
					else
					{	// ошибка не требующая закрытия - ошибка данных - пропускаем данные
                        //коммент Баглаева 
                        Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)502, "Ошибка записи в БД (ex.Class = " + ex.Class +"): " + ex.Message + addstr + ". Всего записей в очереди: " + _dataQueue.Count);
						// если на этой записи уже были ошибки, то считаем, что она обработана
						if (_lastExeptData == data)
						{
							_lastExeptData = null;
							return true;
						}
						else
							_lastExeptData = data;
					}
				    SqlErrCount++;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("UniProtRecorder " + Name + ": ERROR store to DB: " + ex.Message);
                    //коммент Баглаева 
                    Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)503, "Ошибка записи в БД (не sql): " + ex.Message + ". Всего записей в очереди: " + _dataQueue.Count);
					// иначе, если на этой записи уже были ошибки, то считаем, что она обработана
					if (_lastExeptData == data)
					{
						_lastExeptData = null;
						return true;
					}
					else
						_lastExeptData = data;
				}
				finally
				{
					Monitor.Exit(_dbconnector.SqlConn);
				}
			}
			else
			{	// переполнилась очередь
				// если еще не запомнили о разрыве, то запоминаем время первой потеряной записи
				if (!_writeConnectTime)
				{
					UnificationConcentrator.UniDataItem item = data as UnificationConcentrator.UniDataItem;
					// запоминаем время разрыва
					if (item != null)
						_disconnectTime = item._dataTime;
					else
						_disconnectTime = DateTime.Now;
					// при восстановлении будем записывать время
					_writeConnectTime = true;
				}
				// т.к. переполнилась очередь, то считаем, что удачно обработали
				return true;
			}
			// по умолчанию - ошибка обработки
			return false;
		}
		/// <summary>
		/// Реализация базового виртуального метода
		/// </summary>
		/// <param name="data"></param>
		protected override void OnData(DataCollector.Data data)
		{
			throw new Exception("The method or operation is not implemented.");
		}
		/// <summary>
		/// записать время соединения в Базу данных
		/// </summary>
		protected void WriteConnectionTimeToDB()
		{
			SqlCommand cmd =  _dbconnector.SqlConn.CreateCommand();
			// формируем запрос
			cmd.Parameters.Add("@Time", SqlDbType.DateTime);
			cmd.Parameters["@Time"].Value = _connectTime;

			if (_disconnectTime == DateTime.MinValue)
			{
				cmd.Parameters["@Time"].Value = _connectTime;
				//cmd.CommandText = "INSERT INTO ARH_States (State_ID, Time ) VALUES (0, @Time);";
				//cmd.ExecuteNonQuery();
				cmd.CommandText = "INSERT INTO ARH_StatesDYN (State_ID, Time ) VALUES (0, @Time);";
				cmd.ExecuteNonQuery();
			}
			else
			{
				cmd.Parameters["@Time"].Value = _disconnectTime;
				//cmd.CommandText = "INSERT INTO ARH_States (State_ID, Time) VALUES (1, @Time);";
				//cmd.ExecuteNonQuery();
				cmd.CommandText = "INSERT INTO ARH_StatesDYN (State_ID, Time) VALUES (1, @Time);";
				cmd.ExecuteNonQuery();
			}
		}
		/// <summary>
		/// Сделать запись полного состояния объектов и параметров
		/// </summary>
		protected void MakeOverallStateRecord()
		{
			Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)500, "Запись полного состояния.");
			foreach (UniObjSystem ObjSys in _idsysTOrefMAP.Values)
			{
                if (ObjSys.DataTime == DateTime.MinValue)
                    continue;
				Monitor.Enter(ObjSys);
				Monitor.Enter(_dataQueue);
				try
				{
					foreach (IOBaseElement elem in ObjSys.UniObjsDictionary.Values)
					{
						UniObject obj;
						if (elem is UniObject)
							obj = elem as UniObject;
						else
							continue;

						UnificationConcentrator.UniObjState state = new UnificationConcentrator.UniObjState();
						state._siteID = ObjSys.SiteID;
						state._objectID = (UInt32)obj.Number;
						state._uniState = (byte)obj.ObjState.Current;
						state._uniPrevState = (byte)obj.ObjState.Current;
						state._dataTime = ObjSys.DataTime;
						_dataQueue.Enqueue(state);


						// Теперь не добавляем всю диагностику в таблицу при записи полного состояния,
						// а проверяем отсутсвующие ситуации в БД
						foreach (int id in obj._diagStateTOtimeMAPuni.Keys)
						{
							if (_dbconnector.IsConnected)
							{
								try
								{
									// ищем есть ли такая текущая ситуация в БД
									SqlCommand sql = new SqlCommand();
									sql.Connection = _dbconnector.SqlConn;
									sql.CommandText = string.Format("SELECT COUNT(ARH_States.ID) FROM ARH_States WHERE Site_ID={0} AND Object_ID={1} AND State_ID={2} AND (EndTime IS NULL) AND IsDiag=1"
										, ObjSys.SiteID, obj.Number, id);
									int count = (int)sql.ExecuteScalar();

									if (count == 0)
									{	// такой ситуации нет - ищем в таблице ложных ситуаций
										sql.CommandText = string.Format("SELECT COUNT(ARH_StatesFalse.ID) FROM ARH_StatesFalse WHERE Site_ID={0} AND Object_ID={1} AND State_ID={2} AND (EndTime IS NULL) AND IsDiag=1"
											, ObjSys.SiteID, obj.Number, id);
										// добавляем к общему количеству
										count += (int)sql.ExecuteScalar();

										if (count == 0)
										{	// если такой ситуации нет и среди ложных - добавляем
											UnificationConcentrator.UniDiagState diag = new UnificationConcentrator.UniDiagState();
											diag._siteID = ObjSys.SiteID;
											diag._objectID = (UInt32)obj.Number;
											diag._uniDiagState = (UInt16)id;
											diag._dataTime = ObjSys.DataTime;
											diag._detectTime = obj._diagStateTOtimeMAPuni[id];
											diag._endTime = DateTime.MinValue;
											_dataQueue.Enqueue(diag);
										}
									}
								}
								catch
                                {
                                    continue;
                                }
							}
						}
						
						foreach (DlxObject dlxobj in obj.ObjDataCollection.Objects)
						{
							UniObjData objdata = dlxobj as UniObjData;
							if (objdata != null && objdata.IsNumeric)
							{
                                if (objdata._lastSetParamTime != DateTime.MinValue)
                                {
                                    UnificationConcentrator.UniNumericParamState numpar = new UnificationConcentrator.UniNumericParamState();
                                    numpar._siteID = ObjSys.SiteID;
                                    numpar._objectID = (UInt32)obj.Number;
                                    numpar._dataTime = ObjSys.DataTime;
                                    numpar._paramID = objdata.ParamID;
                                    numpar._paramTime = objdata._lastSetParamTime;
                                    numpar._paramValue = objdata._lastSetParamState;
                                    _dataQueue.Enqueue(numpar);
                                }
							}
						}

						foreach (DlxObject dlxobj in obj.ObjDataCollection.Objects)
						{
							UniObjData objdata = dlxobj as UniObjData;
							if (objdata != null && !objdata.IsNumeric && objdata.IsString)
							{
								UnificationConcentrator.UniDataParamState datapar = new UnificationConcentrator.UniDataParamState();
								datapar._siteID = ObjSys.SiteID;
								datapar._objectID = (UInt32)obj.Number;
								datapar._dataTime = ObjSys.DataTime;
								datapar._paramID = objdata.ParamID;
								datapar._paramValue = System.Text.Encoding.ASCII.GetBytes(objdata.String);
								_dataQueue.Enqueue(datapar);
							}
						}
					}
				}
				finally
				{
					Monitor.Exit(ObjSys);
					Monitor.Exit(_dataQueue);
					_dataEvent.Set();
				}
			}
		}
		/// <summary>
		/// Выполнить обработку данных - запись в БД
		/// </summary>
		/// <param name="data"></param>
		protected void ExecuteSqlCommands(object data)
		{
			// вызываем обработку
			if (data != null)
			{
				UnificationConcentrator.UniObjState state;
				UnificationConcentrator.UniDiagState diag;
				UnificationConcentrator.UniNumericParamState numpar;
				UnificationConcentrator.UniDataParamState datapar;
				UnificationConcentrator.UniObjWork work;

				if ((state = data as UnificationConcentrator.UniObjState) != null && (_StateWriter == true))
				{
					#region Тип данных - состояние объекта
					// Создаём строку для локальной таблицы
					DataRow row = _local_ARH_StatesDYN.NewRow(); 
					row["Site_ID"] = state._siteID;
					row["Object_ID"] = state._objectID;
					row["State_ID"] = state._uniState;
					row["PrevState_ID"] = state._uniPrevState;
					row["Time"] = state._dataTime;

                    
                    // Добавляем строку в локальную таблицу
                    _local_ARH_StatesDYN.Rows.Add(row);
					#endregion
				}
				else if ((diag = data as UnificationConcentrator.UniDiagState) != null && (_DSWriter == true))
				{
					#region Тип данных - диагностическая ситуация
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					//cmd.Parameters.Add("@Time", SqlDbType.DateTime);
					//cmd.Parameters["@Time"].Value = diag._detectTime;
					if (diag._endTime == DateTime.MinValue)
					{	// если обрабатывается новая незавершенная ситуация,
						// смотрим есть ли такие ситуации с любым временем начала, но незавершенные
						cmd.CommandText = string.Format("SELECT COUNT(ARH_States.ID) FROM ARH_States WHERE Site_ID={0} AND Object_ID={1} AND State_ID={2} AND (EndTime IS NULL) AND IsDiag=1;", diag._siteID, diag._objectID, diag._uniDiagState);
						int SuchCount = -1;
						try
						{
							SuchCount = (int)cmd.ExecuteScalar();
						}
						catch { }
						
						if (SuchCount == 0)
						{	// если таких нет, то смотрим среди ложных
							cmd.CommandText = string.Format("SELECT COUNT(ARH_StatesFalse.ID) FROM ARH_StatesFalse WHERE Site_ID={0} AND Object_ID={1} AND State_ID={2} AND (EndTime IS NULL) AND IsDiag=1;", diag._siteID, diag._objectID, diag._uniDiagState);
							try
							{
								SuchCount = (int)cmd.ExecuteScalar();
							}
							catch { }
						}

						if (SuchCount == 0)
						{
						    
                            // если таких незавершенных ситуаций нет - добавляем запись
							cmd.CommandText = string.Format("INSERT INTO ARH_States (Site_ID, Object_ID, State_ID, Time, IsDiag ) VALUES ({0}, {1}, {2}, '{3}.{4}', 1);", diag._siteID, diag._objectID, diag._uniDiagState, diag._detectTime, diag._detectTime.Millisecond);
							try
							{
								cmd.ExecuteNonQuery();
                                LogDbAction(diag._siteID, cmd.CommandText, string.Format(" (@time={0})", diag._detectTime));                             
							}
							catch
							{
								StreamWriter sw = new StreamWriter("err_sql_cmd", true, Encoding.Default);
								sw.WriteLine(cmd.CommandText);
								sw.Flush();
								sw.Close();
								throw;
							}
						}
                        
					}
					else
					{	// завершение ситуации
						cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
						cmd.Parameters["@EndTime"].Value = diag._endTime;
						// завершаем такие незвершенные ситуации, с любым временем начала
						cmd.CommandText = string.Format("UPDATE ARH_States SET EndTime = @EndTime WHERE Site_ID = {0} AND Object_ID = {1} AND State_ID = {2} AND (EndTime IS NULL) AND IsDiag = 1", diag._siteID, diag._objectID, diag._uniDiagState);
                        cmd.ExecuteNonQuery();
                        
                        // завершаем и в таблице ложных
						cmd.CommandText = string.Format("UPDATE ARH_StatesFalse SET EndTime = @EndTime WHERE Site_ID = {0} AND Object_ID = {1} AND State_ID = {2} AND (EndTime IS NULL) AND IsDiag = 1", diag._siteID, diag._objectID, diag._uniDiagState);
                        cmd.ExecuteNonQuery();
                        LogDbAction(diag._siteID, cmd.CommandText, string.Format(" (@endtime={0})", diag._endTime));
					}
					#endregion
				}
                else if ((numpar = data as UnificationConcentrator.UniNumericParamState) != null && (_ParamsNumericWriter == true))
                {
                    #region Тип данных - числовой параметр (измерение)
                    if (_useAltParamsTable)
                    {
                        SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
                        cmd.Parameters.Add("@Time", SqlDbType.DateTime);
                        cmd.Parameters["@Time"].Value = numpar._paramTime;
                        cmd.CommandText = string.Format("INSERT INTO {3} (Site_ID, Param_ID, Time, Value ) VALUES ({0}, {1}, @Time, {2});", numpar._siteID, numpar._paramID, numpar._paramValue, _tName_UniParams_alt);
                        try
                        {
                            if (_logAltParams)
                            {
                                StreamWriter sw = new StreamWriter("_logAltParams.log", true, Encoding.Default);
                                sw.WriteLine("{0}\t{1}:{2}\t{3}", numpar._paramTime, numpar._siteID, numpar._paramID, numpar._paramValue);
                                sw.Flush();
                                sw.Close();
                            }
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            StreamWriter sw = new StreamWriter("err_sql_cmd", true, Encoding.Default);
                            sw.WriteLine(cmd.CommandText);
                            sw.WriteLine(e.Message);
                            sw.Flush();
                            sw.Close();
                            throw;
                        }
                        
                    }
                    else
                    {
                        // Создаём строку для локальной таблицы
                        DataRow row = _local_ARH_ParamsNumericUni.NewRow();
                        row["Site_ID"] = numpar._siteID;
                        row["Param_ID"] = numpar._paramID;
                        row["Time"] = numpar._paramTime;

                        if (numpar._paramValue == float.MaxValue || float.IsNaN(numpar._paramValue) || float.IsInfinity(numpar._paramValue))
                            row["Value"] = float.MaxValue;
                        else
                            row["Value"] = numpar._paramValue;

                        // Добавляем строку в локальную таблицу
                        _local_ARH_ParamsNumericUni.Rows.Add(row);
                    }
                    #endregion
                }
                // если тип данных нечисловые параметры
                else if ((datapar = data as UnificationConcentrator.UniDataParamState) != null && (_ParamsDataWriter == true))
                {
                    #region Тип данных - нечисловой параметр (данные)
                    SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
                    cmd.Parameters.Add("@Time", SqlDbType.DateTime);
                    cmd.Parameters["@Time"].Value = datapar._dataTime;
                    cmd.Parameters.Add("@Value", SqlDbType.Binary);
                    cmd.Parameters["@Value"].Value = datapar._paramValue;

                    cmd.CommandText = string.Format("INSERT INTO ARH_ParamsDataUni (Site_ID, Param_ID, Time, Value ) VALUES ({0}, {1}, @Time, @Value);", numpar._siteID, numpar._paramID);
                    cmd.ExecuteNonQuery();
                    #endregion
                }
                else if ((work = data as UnificationConcentrator.UniObjWork) != null && work._flag == UnificationConcentrator.UniObjWork.Kind.Complete && (_TOWriter == true))
                {
                    #region Тип данных - факты выполнения ТОиР
                    SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
                    cmd.Parameters.Add("@BeginTime", SqlDbType.DateTime);
                    cmd.Parameters["@BeginTime"].Value = work._beginTime;
                    cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
                    cmd.Parameters["@EndTime"].Value = work._endTime;

                    //!TODO: новая таблица для фактов работ + для привязаных диагностических ситуаций добавить Comment и обновить IsTO=true
                    //+ на эту таблицу триггер на добавление - с выполнением отправки в АСУ-Ш-2

                    string qIns = "INSERT INTO ARH_TO_Detected (Site_ID, Object_ID, Punkt_ID, Work_ID, fact_beg_time, fact_end_time ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime);";
                    string qInsDiag = "INSERT INTO ARH_TO_Detected (Site_ID, Object_ID, Punkt_ID, Work_ID, fact_beg_time, fact_end_time, Arh_State_ID) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime, {4});";
                    string qSel = @"select st.ID, Object_ID, st.[Time], ID_ArhState as Comment_DS_ID, IsTO 
                                    from ARH_States st left join ARH_DiagComments com on st.ID=com.ID_ArhState
                                    where Site_ID = {0} AND IsDiag = 1 AND State_ID = {1} and st.Time > dbo.GetCustomDate(0)";
                    string qUpd = "UPDATE [ARH_States] SET [IsTO]=1 WHERE ID={0}";
                    string qInsComment = "INSERT INTO ARH_DiagComments (ID_ArhState, Comment, UserName, Time) VALUES ({0}, '{1}', 'Система', @Time)";

                    //if (work._diags.Count == 0)
                    //{   // добавление записи в ARH_TO_Detected
                    cmd.CommandText = string.Format(qIns, work._siteID, work._objectID, work._punktID, work._workID);
                    cmd.ExecuteNonQuery();
                   // }
                  /*  else
                    {
                        cmd.Parameters.Add("@Time", SqlDbType.DateTime);
                        foreach (UInt16 stateID in work._diags.Keys)
                        {
                            List<Int64> res = new List<long>();
                            cmd.Parameters["@Time"].Value = work._diags[stateID];
                            try
                            {   // поиск сответствующей ДС
                                //cmd.CommandText = string.Format(qSel, work._siteID, work._objectID, diagID);
                                //res.Add((Int64)cmd.ExecuteScalar());

                                // запрос всех однотипных ДС по станции
                                List<UnificationConcentrator.UniObjWork.RootDiag> diags = UnificationConcentrator.UniObjWork.RootDiag.GetDiags(string.Format(qSel, work._siteID, stateID), _dbconnector.SqlConn);
                                // определение ID обрабатываемой ДС
                                Int64 curDiagID = UnificationConcentrator.UniObjWork.RootDiag.GetDiagID(diags, work, stateID);
                                if (curDiagID > -1)
                                {   // если соотв. ДС найдена - добавление записи в ARH_TO_Detected
                                    cmd.CommandText = string.Format(qInsDiag, work._siteID, work._objectID, work._punktID, work._workID, curDiagID);
                                    cmd.ExecuteNonQuery();
                                }

                                if (work._workID == 0)
                                {   // если работы не было в план-графике ТО - проверяем кол-во возможных ТО за смену на двнной станции, найденные добавляем в список
                                    // иначе - классифицируем, как выполнение ТО, только саму ДС
                                    if (diags.Count == 1 || UnificationConcentrator.UniObjWork.RootDiag.IsOnlyObject(diags, (int)work._objectID))
                                    {   // если данное проявление единственное, или все проявления по одному объекту - не факт, что это ТО
                                        diags.Clear();
                                    }
                                }
                                else
                                {   // если работа была в графике - обрабатываем только одновременные с ней ДС
                                    // т.к есть разница в хранении времени в БД и в системе, передаем время БД, чтобы получить такой же формат
                                    SqlCommand getSqlTimeCmd = _dbconnector.SqlConn.CreateCommand();
                                    getSqlTimeCmd.CommandText = string.Format("select convert(datetime,'{0}.{1}',104)", work._diags[stateID], work._diags[stateID].Millisecond);
                                    DateTime workTIme = (DateTime)getSqlTimeCmd.ExecuteScalar();
                                    diags = diags.Where(d => d.Time == workTIme).ToList();
                                }

                                foreach (UnificationConcentrator.UniObjWork.RootDiag d in diags)
                                {
                                    // убрал отметку у ДС, что он относится к ТО
                                    /*if (!d.IsTO)
                                    {   // отметка ДС, как ТО
                                        cmd.CommandText = string.Format(qUpd, d.ID);
                                        cmd.ExecuteNonQuery();
                                    }*/
                     /*               if (!d.IsCommented)
                                    {   // добавление комментария к ДС
                                        cmd.CommandText = string.Format(qInsComment, d.ID, string.Format(UnificationConcentrator.UniObjWork.Comment, work._punktID));
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                cmd.CommandText = string.Format(qIns, work._siteID, work._objectID, work._punktID, work._workID);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }*/
                    #endregion
                }
                else if ((_TOWriter = true) && ((work = data as UnificationConcentrator.UniObjWork) != null
                    && (work._flag == UnificationConcentrator.UniObjWork.Kind.Add || work._flag == UnificationConcentrator.UniObjWork.Kind.Remove)))
                {
                    #region Тип данных - план выполнения ТОиР
                    SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
                    // удаление работы из плана
                    if (work._flag == UnificationConcentrator.UniObjWork.Kind.Remove)
                    {
                        cmd.CommandText = string.Format("DELETE FROM ARH_TOPlanWorks WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
                        cmd.ExecuteNonQuery();
                    }
                    else// добавление или перенос
                    {
                        // проверяем была ли раньше эта работа
                        cmd.CommandText = string.Format("SELECT Count(*) FROM ARH_TOPlanWorks WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
                        int res = (int)cmd.ExecuteScalar();

                        cmd.Parameters.Add("@BeginTime", SqlDbType.DateTime);
                        cmd.Parameters["@BeginTime"].Value = work._beginTime;
                        cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
                        cmd.Parameters["@EndTime"].Value = work._endTime;
                        // если не было, то добавляем
                        if (res == 0)
                        {
                            cmd.CommandText = string.Format("INSERT INTO ARH_TOPlanWorks (Site_ID, Object_ID, Punkt_ID, Work_ID, BeginTime, EndTime ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime);", work._siteID, work._objectID, work._punktID, work._workID);
                            cmd.ExecuteNonQuery();
                        }
                        else// была - обновляем
                        {
                            cmd.CommandText = string.Format("UPDATE ARH_TOPlanWorks SET BeginTime = @BeginTime, EndTime = @EndTime WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    #endregion
                }
                else
                {
                }
			}
		}

        void LogDbAction(uint Site_ID, string cmdText, string cmdParams)
        {
            return;
            if(Site_ID != 11609)
                return;

            StreamWriter sw = new StreamWriter("log_db_actions.txt", true, Encoding.Default);
            sw.WriteLine(string.Format("{0} '{1}' {2}", DateTime.Now, cmdText, cmdParams));
            sw.Flush();
            sw.Close();
        }

		/// <summary>
		/// Метод обработки таймера (вызывается на другом потоке)
		/// Выполняет открытие БД 
		/// </summary>
		public void OnConnectTimer(Object stateInfo)
		{
			if (!IsConnected)
			{
				Connect();
                if (IsConnected)
                {
                    Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Info, (Dps.AppDiag.EventLogID)500, "Выполнено подключение к БД: " + Name);
                    Console.WriteLine("Выполнено подключение к БД " + Name);
                }
                else
                {
                    Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)500, "Ошибка: Подключиться к БД " + Name + " не удалось");
                    Console.WriteLine("Ошибка: Подключиться к БД " + Name + " не удалось");
                }
            }
		}
		/// <summary>
		/// соединение с БД
		/// </summary>
		protected bool Connect()
		{
			return _dbconnector.Connect();
		}
		/// <summary>
		/// Закрыть соединение
		/// </summary>
		protected void Disconnect()
		{
			_dbconnector.Disconnect();
		}
		/// <summary>
		/// Открыто ли соединение
		/// </summary>
		protected bool IsConnected
		{
			get
			{
				return _dbconnector.IsConnected;
			}
		}
		/// <summary>
		/// Копирование данных из локальных таблиц
		/// </summary>
		protected void BulkCopyData_OLD()
		{
			/*SqlBulkCopy bulk = new SqlBulkCopy(_dbconnector.SqlConn);
			bulk.DestinationTableName = _tName_StatesDYN;
			bulk.BulkCopyTimeout = _bulkCopyTimeout;

			try
			{
				bulk.WriteToServer(_local_ARH_StatesDYN);
				_local_ARH_StatesDYN.Rows.Clear();
			}
			catch (Exception ex)
			{
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)504, "Ошибка копирования записей в БД (ARH_StatesDYN): " + ex.Message + ". Всего записей в очереди: " + _dataQueue.Count + ". Записей для копирования: " + _local_ARH_StatesDYN.Rows.Count);
			}
			bulk.Close();


			bulk = new SqlBulkCopy(_dbconnector.SqlConn);
			bulk.DestinationTableName = _tName_UniParams;
			bulk.BulkCopyTimeout = _bulkCopyTimeout;

			try
			{
				bulk.WriteToServer(_local_ARH_ParamsNumericUni);
				_local_ARH_ParamsNumericUni.Rows.Clear();
			}
			catch (Exception ex)
			{
				Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)505, "Ошибка копирования записей в БД (ARH_ParamsNumericUni): " + ex.Message + ". Всего записей в очереди: " + _dataQueue.Count + ". Записей для копирования: " + _local_ARH_ParamsNumericUni.Rows.Count);
			}

			bulk.Close();*/
		}
		/// <summary>
		/// Выполнить обработку данных - запись в БД
		/// </summary>
		/// <param name="data"></param>
		protected void ExecuteSqlCommands_OLD(object data)
		{
			// вызываем обработку
			/*if (data != null)
			{
				UnificationConcentrator.UniObjState state;
				UnificationConcentrator.UniDiagState diag;
				UnificationConcentrator.UniNumericParamState numpar;
				UnificationConcentrator.UniDataParamState datapar;
				UnificationConcentrator.UniObjWork work;

				// если тип данных состояние
				if ((state = data as UnificationConcentrator.UniObjState) != null)
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					cmd.Parameters.Add("@Time", SqlDbType.DateTime);
					cmd.Parameters["@Time"].Value = state._dataTime;
					cmd.CommandText = string.Format("INSERT INTO ARH_StatesDYN (Site_ID, Object_ID, State_ID, Time ) VALUES ({0}, {1}, {2}, @Time);", state._siteID, state._objectID, state._uniState);
					cmd.ExecuteNonQuery();
				}
				// если тип данных ситуации
				else if ((diag = data as UnificationConcentrator.UniDiagState) != null)
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					cmd.Parameters.Add("@Time", SqlDbType.DateTime);
					cmd.Parameters["@Time"].Value = diag._detectTime;
					if (diag._endTime == DateTime.MinValue)
					{
						cmd.CommandText = string.Format("SELECT COUNT(ARH_States.ID) FROM ARH_States WHERE Site_ID={0} AND Object_ID={1} AND State_ID={2} AND Time=@Time AND IsDiag=1;", diag._siteID, diag._objectID, diag._uniDiagState);
						int SuchCount = 0;
						try
						{
							SuchCount = (int)cmd.ExecuteScalar();
						}
						catch { }

						if (SuchCount == 0)
						{
							cmd.CommandText = string.Format("INSERT INTO ARH_States (Site_ID, Object_ID, State_ID, Time, IsDiag ) VALUES ({0}, {1}, {2}, @Time, 1);", diag._siteID, diag._objectID, diag._uniDiagState);
							try
							{
								cmd.ExecuteNonQuery();
							}
							catch
							{
								StreamWriter sw = new StreamWriter("err_sql_cmd", true, Encoding.Default);
								sw.WriteLine(cmd.CommandText);
								sw.Flush();
								sw.Close();
								throw;
							}
						}
					}
					else
					{
						cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
						cmd.Parameters["@EndTime"].Value = diag._endTime;

						cmd.CommandText = string.Format("UPDATE ARH_States SET EndTime = @EndTime WHERE Site_ID = {0} AND Object_ID = {1} AND State_ID = {2} AND Time= @Time AND IsDiag = 1", diag._siteID, diag._objectID, diag._uniDiagState);
						cmd.ExecuteNonQuery();
					}
				}
				// если тип данных числовые параметры
				else if ((numpar = data as UnificationConcentrator.UniNumericParamState) != null)
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					cmd.Parameters.Add("@Time", SqlDbType.DateTime);
					cmd.Parameters["@Time"].Value = numpar._paramTime;
					cmd.Parameters.Add("@Value", SqlDbType.Real);
					cmd.Parameters["@Value"].Value = numpar._paramValue;

					cmd.CommandText = string.Format("INSERT INTO ARH_ParamsNumericUni (Site_ID, Param_ID, Time, Value ) VALUES ({0}, {1}, @Time, @Value);", numpar._siteID, numpar._paramID);
					cmd.ExecuteNonQuery();
				}
				// если тип данных нечисловые параметры
				else if ((datapar = data as UnificationConcentrator.UniDataParamState) != null)
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					cmd.Parameters.Add("@Time", SqlDbType.DateTime);
					cmd.Parameters["@Time"].Value = datapar._dataTime;
					cmd.Parameters.Add("@Value", SqlDbType.Binary);
					cmd.Parameters["@Value"].Value = datapar._paramValue;

					cmd.CommandText = string.Format("INSERT INTO ARH_ParamsDataUni (Site_ID, Param_ID, Time, Value ) VALUES ({0}, {1}, @Time, @Value);", numpar._siteID, numpar._paramID);
					cmd.ExecuteNonQuery();
				}
				// если тип данных факты работ
				else if ((work = data as UnificationConcentrator.UniObjWork) != null && work._flag == 2)
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					cmd.Parameters.Add("@BeginTime", SqlDbType.DateTime);
					cmd.Parameters["@BeginTime"].Value = work._beginTime;
					cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
					cmd.Parameters["@EndTime"].Value = work._endTime;

					if (work._diags.Count == 0)
					{
						cmd.CommandText = string.Format("INSERT INTO CUR_ProbableTOWorks (Site_ID, Object_ID, Punkt_ID, Work_ID, BeginTime, EndTime ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime);", work._siteID, work._objectID, work._punktID, work._workID);
						cmd.ExecuteNonQuery();
					}
					else
					{
						cmd.Parameters.Add("@Time", SqlDbType.DateTime);
						Int64 res;
						foreach (UInt16 diagID in work._diags.Keys)
						{
							cmd.Parameters["@Time"].Value = work._diags[diagID];

							cmd.CommandText = string.Format("SELECT ID FROM ARH_States WHERE Site_ID = {0} AND Object_ID = {1} AND IsDiag = 1 AND State_ID = {3} AND Time = {4};", work._siteID, work._objectID, diagID);
							try
							{
								res = (Int64)cmd.ExecuteScalar();
								cmd.CommandText = string.Format("INSERT INTO CUR_ProbableTOWorks (Site_ID, Object_ID, Punkt_ID, Work_ID, BeginTime, EndTime, History_ID ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime, {4});", work._siteID, work._objectID, work._punktID, work._workID, res);
								cmd.ExecuteNonQuery();
							}
							catch (Exception)
							{
								cmd.CommandText = string.Format("INSERT INTO CUR_ProbableTOWorks (Site_ID, Object_ID, Punkt_ID, Work_ID, BeginTime, EndTime ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime);", work._siteID, work._objectID, work._punktID, work._workID);
								cmd.ExecuteNonQuery();
							}
						}
					}
				}
				// если тип данных план работ
				else if ((work = data as UnificationConcentrator.UniObjWork) != null && (work._flag == 0 || work._flag == 1))
				{
					SqlCommand cmd = _dbconnector.SqlConn.CreateCommand();
					// удаление работы из плана
					if (work._flag == 1)
					{
						cmd.CommandText = string.Format("DELETE FROM ARH_TOPlanWorks WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
						cmd.ExecuteNonQuery();
					}
					else// добавление или перенос
					{
						// проверяем была ли раньше эта работа
						cmd.CommandText = string.Format("SELECT Count(*) FROM ARH_TOPlanWorks WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
						int res = (int)cmd.ExecuteScalar();

						cmd.Parameters.Add("@BeginTime", SqlDbType.DateTime);
						cmd.Parameters["@BeginTime"].Value = work._beginTime;
						cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
						cmd.Parameters["@EndTime"].Value = work._endTime;
						// если не было, то добавляем
						if (res == 0)
						{
							cmd.CommandText = string.Format("INSERT INTO ARH_TOPlanWorks (Site_ID, Object_ID, Punkt_ID, Work_ID, BeginTime, EndTime ) VALUES ({0}, {1}, {2}, {3}, @BeginTime, @EndTime);", work._siteID, work._objectID, work._punktID, work._workID);
							cmd.ExecuteNonQuery();
						}
						else// была - обновляем
						{
							cmd.CommandText = string.Format("UPDATE ARH_TOPlanWorks SET BeginTime = @BeginTime, EndTime = @EndTime WHERE Site_ID = {0} AND Object_ID = {1} AND Punkt_ID = {2} AND Work_ID = {3};", work._siteID, work._objectID, work._punktID, work._workID);
							cmd.ExecuteNonQuery();
						}
					}

				}
			}*/
		}

		/// <summary>
		/// Копировать записи из локальных таблиц в БД при превышении заданного порогового количества записей
		/// </summary>
		/// <param name="n">Пороговое количество записей, при превышении которого выполняется запись в БД</param>
		protected void BulkCopyData(int n)
		{
			// проверка таблицы динамических состояний
			if (CheckLocalTable(_q_StatesDYN, _local_ARH_StatesDYN, n))
			{	// обработка очереди таблиц динамическиз состояний
				ProcessQueue(_q_StatesDYN);
			}
			// проверка таблицы измерений
			if (CheckLocalTable(_q_UniParams, _local_ARH_ParamsNumericUni, n))
			{	// обработка очереди таблиц измерений
				ProcessQueue(_q_UniParams);
			}
		}
		/// <summary>
		/// Проверка локальной таблицы (при превышении заданного числа записей - помещение в очередь на запись)
		/// </summary>
		/// <param name="q">Очередь на запись</param>
		/// <param name="t">Текущая локальная таблица</param>
		/// <param name="rowsN">Заданное число записей</param>
		/// <returns>Признак наличия таблиц в очереди на запись</returns>
		protected bool CheckLocalTable(Queue<DataTable> q, DataTable t, int rowsN)
		{
			if (t.Rows.Count > rowsN)
			{	// если кол-во записей в таблице больше заданного - помещаем таблицу в очередь
				q.Enqueue(t);
				t = CreateLocalTable(t.TableName);

				if (q.Count > _maxTableQueueLength)
				{	// если длина очереди превысила допустимое значение - удаляем самый старый элемент
					q.Dequeue();
				}
			}
			return q.Count > 0;
		}
		/// <summary>
		/// Обработка очереди на запись (копирование данных из локальных таблиц в БД)
		/// </summary>
		/// <param name="q">Очередь на запись</param>
		protected void ProcessQueue(Queue<DataTable> q)
		{
			// время, затраченное на копирование данных, мс
			double spentTime = 0;
			SqlBulkCopy bulk = new SqlBulkCopy(_dbconnector.SqlConn);

			while (q.Count > 0 && spentTime < _bulkCopyTimeout * 1000)
			{	// пока в очереди есть таблицы, и затрачено меньше времени, чем таймаут - копируем в БД
				DataTable t = q.Peek();
				bulk.DestinationTableName = t.TableName;
				bulk.BulkCopyTimeout = _bulkCopyTimeout;

				try
				{
					// засекаем время начала копирования
					DateTime start = DateTime.Now;
					// копируем данные в БД
					bulk.WriteToServer(t);
					// чистим локальную таблицу
					t.Rows.Clear();
					// удаляем ее из очереди
					q.Dequeue();
					// прибавляем время копирования к затраченному
					spentTime += (DateTime.Now - start).TotalMilliseconds;
                    _UdpDiag?.WriteLog(string.Format("Время, затраченное на копирование таблицы {0} составило: {1} мс", t.TableName, spentTime));
				}
				catch (Exception ex)
				{
					string msg = string.Format("Ошибка копирования записей в БД ({0}): {1}. Записей в очереди данных: {2}. Таблиц в очереди: {3}. Записей для копирования: {4}"
						, t.TableName, ex.Message, _dataQueue.Count, q.Count, t.Rows.Count);
					Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error, (Dps.AppDiag.EventLogID)504, msg);
                    _UdpDiag?.WriteLog(string.Format("Ошибка копирования записей в БД ({0}): {1}. Записей в очереди данных: {2}. Таблиц в очереди: {3}. Записей для копирования: {4}"
                        , t.TableName, ex.Message, _dataQueue.Count, q.Count, t.Rows.Count));
					break;
				}
			}
			bulk.Close();
		}


		#endregion
	}
}
