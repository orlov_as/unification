﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Text;
using AfxEx;
using Ivk.IOSys;
using Tdm.Unification.v2;

namespace Tdm.Unification
{
    public abstract class DlxTimer : DlxObject
    {
        /// <summary>
        /// Таймер для выполнения синхронизации
        /// </summary>
        protected Timer _Timer = null;
        /// <summary>
        /// Задержка перед первым выполнением, мс
        /// </summary>
        protected int _initDelay = 60 * 1000;// 60 секунд
        /// <summary>
        /// Частота запросов, мс
        /// </summary>
        protected int _rateQuery = 60 * 1000;// 60 секунд
        /// <summary>
        /// Загрузка
        /// </summary>
        /// <param name="Loader">загрузчик</param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            _initDelay = Loader.GetAttributeInt("InitDelay", _initDelay);
            _rateQuery = Loader.GetAttributeInt("QueryRate", _rateQuery);
        }
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            // заводим таймер
            TimerCallback timerDelegate = new TimerCallback(OnTimer);
            _Timer = new Timer(timerDelegate, null, _initDelay, _rateQuery);
            base.Create();
        }
        /*/// <summary>
        /// Деинициализация
        /// </summary>
        public override void Destroy()
        {
            if (_Timer != null)
                _Timer.Dispose();
            base.Destroy();
        }*/
        /// <summary>
        /// Метод обработки таймера (вызывается на другом потоке)
        /// </summary>
        protected void OnTimer(Object stateInfo)
        {
            OnTimerProc();
        }
        /// <summary>
        /// Метод, выполняющийся при срабатывании таймера
        /// </summary>
        protected abstract void OnTimerProc();
    }
    /// <summary>
    /// Поставщик плана работ по АТО
    /// </summary>
    public class TOPlanProvider : DlxTimer
    {
        #region Dlx
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            connString = Loader.GetAttributeString("ConnString", connString);
            qSelectWorks = Loader.GetAttributeString("SelectWorks", qSelectWorks);
            lockTimeout = Loader.GetAttributeInt("LockTimeout", lockTimeout);
        }
        public override void Create()
        {
            base.Create();
            conn = new SqlConnection(connString);

            //cmd = new SqlCommand(qSelectWorks + PrepareSiteIDsCondition(siteList), conn);
            cmd = new SqlCommand(qSelectWorks, conn);
            cmd.Parameters.Add("@curdate", SqlDbType.DateTime);
        }
        #endregion

        /// <summary>
        /// Метод, вызываемый при срабатывании таймера
        /// </summary>
        protected override void OnTimerProc()
        {
            if (Monitor.TryEnter(locker, lockTimeout))
            {
                SqlDataReader r = null;
                List<PlanWorkTO> tmp = new List<PlanWorkTO>();
                try
                {
                    conn.Open();
                    cmd.Parameters["@curdate"].Value = DateTime.Now.Date;
                    r = cmd.ExecuteReader();
                    if (r.HasRows)
                        while (r.Read())
                        {
                            PlanWorkTO w = PlanWorkTO.Parse(r);
                            if (w != null)
                                tmp.Add(w);
                        }
                    works.Clear();
                    works = tmp;
                }
                catch (Exception e)
                {
                    Dps.AppDiag.EventLog.AppEventLog.WriteText(Dps.AppDiag.EventLogSeverity.Error
                        , Dps.AppDiag.EventLogID.Common
                        , string.Format("Ошибка на потоке обновления плана работ по ТО  ({0}):", e.Message));
                }
                finally
                {
                    if (r != null)
                        r.Close();
                    conn.Close();
                    Monitor.Exit(locker);
                }
            }
        }

        /// <summary>
        /// Найти плановые работы по объекту станции/перегона
        /// </summary>
        /// <param name="sid">ID станции/перегона</param>
        /// <param name="oid">ID объекта</param>
        /// <returns>Список работ</returns>
        public List<PlanWorkTO> GetPlanWorks(int sid, int oid)
        {
            List<PlanWorkTO> res = new List<PlanWorkTO>();
            try
            {
                if (Monitor.TryEnter(locker, lockTimeout))
                {
                    res.AddRange((from w in works
                                  where w.site_id == sid && w.object_id == oid
                                  select w));
                }
            }
            catch { }
            finally { Monitor.Exit(locker); }
            return res;
        }

        /// <summary>
        /// Подготовка IN-условия выборки плановых работ (списка ID станций/перегонов)
        /// </summary>
        /// <param name="coll">Dlx-коллекция станций/перегонов</param>
        /// <returns>Фрагмент условия выборки плановых работ</returns>
        string PrepareSiteIDsCondition(DlxCollection coll)
        {
            StringBuilder whCond = new StringBuilder(" AND p.[Site_ID] IN (", 200);
            foreach (DlxObject dlx in coll.Objects)
            {
                Site st = dlx as Site;
                if (st != null)
                    whCond = whCond.AppendFormat("{0},", st.ID);
            }
            whCond = whCond.Append(")");
            whCond = whCond.Replace(",)", ")");

            return coll.Objects.Count > 0 ? whCond.ToString() : string.Empty;
        }

        #region Data
        /// <summary>
        /// Таймаут получения блокировки
        /// </summary>
        int lockTimeout = 5000;
        /// <summary>
        /// Строка подключения
        /// </summary>
        string connString = "Data Source=DDCSK-DBSRV;Initial Catalog=MainDBv2;Persist Security Info=True;User ID=shdm;Password=shdmshdm";
        /// <summary>
        /// Соединение с БД
        /// </summary>
        SqlConnection conn;
        /// <summary>
        /// Выборка списка плановых работ
        /// </summary>
        string qSelectWorks = @"SELECT w.[shch_id]
      , w.[obj_osn_id]
      , w.[obj_id]
      , w.[punkt_id]
      , w.[rab_id]
      , w.[wk_id]
      , w.[plan_beg_time]
      , w.[plan_end_time]
      , w.[cor_tip]
	  , p.[Site_ID]
	  , p.[Object_ID]
FROM  [ASH_PlanWorks] w
--INNER JOIN [ASH_KTOObjs_ForTransfer] o ON w.obj_osn_id=o.obj_osn_id and w.obj_id=o.obj_id and w.obj_kod=o.obj_kod
INNER JOIN [PRJ_Objects] p ON w.obj_osn_id = ISNULL(p.Site_ID_peregon, p.Site_ID) and w.obj_kod=p.Object_ID
WHERE w.[plan_beg_time] BETWEEN @curdate and DATEADD(d, 1, @curdate)";
        /// <summary>
        /// Команда выборки списка плановых работ
        /// </summary>
        SqlCommand cmd;
        /// <summary>
        /// Перечень работ по ТО на текущую дату
        /// </summary>
        List<PlanWorkTO> works = new List<PlanWorkTO>();
        /// <summary>
        /// Dlx-коллекция обрабатываемых станций/перегонов
        /// </summary>
        DlxCollection siteList;
        /// <summary>
        /// Объект синхронизации для потокобезопасного обновления плана работ по ТО
        /// </summary>
        object locker = new object();
        #endregion
    }

    /// <summary>
    /// Плановая работа по ТО
    /// </summary>
    public class PlanWorkTO
    {
        /// <summary>
        /// Идентификатор ШЧ
        /// </summary>
        public readonly int shch_id;
        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public readonly int obj_osn_id;
        /// <summary>
        /// Идентификатор объекта ЖАТ
        /// </summary>
        public readonly int obj_id;
        /// <summary>
        /// Идентификатор пункта работы по инструкции
        /// </summary>
        public readonly int punkt_id;
        /// <summary>
        /// Идентификатор работы
        /// </summary>
        public readonly int rab_id;
        /// <summary>
        /// Идентификатор работы в графике
        /// </summary>
        public readonly int wk_id;
        /// <summary>
        /// Плановая дата/время начала работы
        /// </summary>
        public readonly DateTime plan_beg_time;
        /// <summary>
        /// Плановая дата/время окончания работы
        /// </summary>
        public readonly DateTime plan_end_time;
        /// <summary>
        /// Тип операции над работой
        /// </summary>
        public readonly string cor_tip;
        /// <summary>
        /// ID станции/перегона в СТДМ
        /// </summary>
        public readonly int site_id;
        /// <summary>
        /// ID контролируемого объекта в СТДМ
        /// </summary>
        public readonly int object_id;

        public PlanWorkTO(int shch_id, int obj_osn_id, int obj_id, int punkt_id, int rab_id, int wk_id, DateTime beg_time, DateTime end_time, string cor_tip, int site_id, int object_id)
        {
            this.shch_id = shch_id;
            this.obj_osn_id = obj_osn_id;
            this.obj_id = obj_id;
            this.punkt_id = punkt_id;
            this.rab_id = rab_id;
            this.wk_id = wk_id;
            this.plan_beg_time = beg_time;
            this.plan_end_time = end_time;
            this.cor_tip = cor_tip;
            this.site_id = site_id;
            this.object_id = object_id;
        }
        /// <summary>
        /// Создание плановой работы по записи выборки из БД 
        /// </summary>
        /// <param name="r">Запись выборки</param>
        /// <returns>Плановая работа по ТО</returns>
        public static PlanWorkTO Parse(IDataReader r)
        {
            try
            {
                PlanWorkTO w = new PlanWorkTO((int)r["shch_id"]
                    , (int)r["obj_osn_id"]
                    , (int)r["obj_id"]
                    , (int)r["punkt_id"]
                    , (int)r["rab_id"]
                    , (int)r["wk_id"]
                    , (DateTime)r["plan_beg_time"]
                    , (DateTime)r["plan_end_time"]
                    , (string)r["cor_tip"]
                    , (int)r["Site_ID"]
                    , (int)r["Object_ID"]);
                return w;
            }
            catch { return null; }
        }
    }

    /// <summary>
    /// Задача выявления ТО по диагностическому состояниям
    /// </summary>
    public abstract class DiagStateDetectTask : UniObjectUpTO.TOTask
    {
        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение данного объекта
            AddWatchToObject(_controlObj);
        }

        /// <summary>
        /// Обработка обновления перечня ДС
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            UpdateSubtasks();
        }
        /// <summary>
        /// Обработка обновления значений сигналов
        /// </summary>
        public override void UpdateObjectSignals()
        { }

    }

    #region Выявление ТО по диагностическим ситуациям
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.1.1)
    /// </summary>
    public class DSDetectTask_LampChange_1411 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1411()
        {
            TOSubTask st = new TOSubTaskDiag(26781490);
            st.AddCondition(new DSCondition(0, 1, 903, true));
            st.AddCondition(new DSCondition(1, 0, 903, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.1.2)
    /// </summary>
    public class DSDetectTask_LampChange_1412 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1412()
        {
            TOSubTask st = new TOSubTaskDiag(26781491);
            st.AddCondition(new DSCondition(0, 1, 903, true));
            st.AddCondition(new DSCondition(1, 0, 903, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.2.1)
    /// </summary>
    public class DSDetectTask_LampChange_1421 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1421()
        {
            TOSubTask st = new TOSubTaskDiag(26781492);
            st.AddCondition(new DSCondition(0, 1, 1503, true));
            st.AddCondition(new DSCondition(1, 0, 1503, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.2.2)
    /// </summary>
    public class DSDetectTask_LampChange_1422 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1422()
        {
            TOSubTask st = new TOSubTaskDiag(26781493);
            st.AddCondition(new DSCondition(0, 1, 1503, true));
            st.AddCondition(new DSCondition(1, 0, 1503, false));
            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.3.1)
    /// </summary>
    public class DSDetectTask_LampChange_1431 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1431()
        {
            // выходные, маршрутные
            /*TOSubTask st1 = new TOSubTaskDiag(26781495);
            st1.AddCondition(new DSCondition(0, 1, 905, true));
            st1.AddCondition(new DSCondition(1, 0, 905, false));
            subTasks.Add(st1);

            // маневровые
            TOSubTask st2 = new TOSubTaskDiag(26781495);
            st2.AddCondition(new DSCondition(0, 1, 600, true));
            st2.AddCondition(new DSCondition(1, 0, 600, false));
            subTasks.Add(st2);*/

            TOSubTask st3 = new TOSubTaskDiag(26781495);
            st3.AddCondition(new DSCondition(0, 1, 903, true));
            st3.AddCondition(new DSCondition(1, 0, 903, false));
            subTasks.Add(st3);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.3.2)
    /// </summary>
    public class DSDetectTask_LampChange_1432 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_1432()
        {
            // выходные, маршрутные
            TOSubTask st1 = new TOSubTaskDiag(26781496);
            st1.AddCondition(new DSCondition(0, 1, 903, true));
            st1.AddCondition(new DSCondition(1, 0, 903, false));
            subTasks.Add(st1);

            // маневровые
            /*TOSubTask st2 = new TOSubTaskDiag(26781496);
            st2.AddCondition(new DSCondition(0, 1, 600, true));
            st2.AddCondition(new DSCondition(1, 0, 600, false));
            subTasks.Add(st2);*/
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 2.1.2.1)
    /// </summary>
    public class DSDetectTask_SwitchCheck_2121 : DiagStateDetectTask
    {
        public DSDetectTask_SwitchCheck_2121()
        {
            TOSubTask st = new TOSubTaskDiag(26781524);
            st.AddCondition(new DSCondition(0, 1, 301, true));
            st.AddCondition(new DSCondition(1, 0, 301, false));
            /*st.AddCondition(new DSCondition(2, 3, 0, 30 * 60000, 0, 301, true));
            st.AddCondition(new DSCondition(3, 0, 301, false));
            */
            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 2.1.2.2)
    /// </summary>
    public class DSDetectTask_SwitchCheck_2122 : DiagStateDetectTask
    {
        public DSDetectTask_SwitchCheck_2122()
        {
            TOSubTask st = new TOSubTaskDiag(26781709);
            st.AddCondition(new DSCondition(0, 1, 301, true));
            st.AddCondition(new DSCondition(1, 0, 301, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 2.1.11)
    /// </summary>
    public class DSDetectTask_SwitchCheck_2111 : DiagStateDetectTask
    {
        public DSDetectTask_SwitchCheck_2111()
        {
            TOSubTask st = new TOSubTaskDiag(26781536);
            st.AddCondition(new DSCondition(0, 1, 301, true));
            st.AddCondition(new DSCondition(1, 0, 301, false));

            subTasks.Add(st);
        }
        public override void Load(DlxLoader loader)
        {
            base.Load(loader);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 2.1.5)
    /// </summary>
    public class DSDetectTask_SwitchCheck_215 : DiagStateDetectTask
    {
        public DSDetectTask_SwitchCheck_215()
        {
            TOSubTask st = new TOSubTaskDiag(26781528);
            st.AddCondition(new DSCondition(0, 1, 301, true));
            st.AddCondition(new DSCondition(1, 0, 301, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 2.1.6)
    /// </summary>
    public class DSDetectTask_SwitchCheck_216 : DiagStateDetectTask
    {
        public DSDetectTask_SwitchCheck_216()
        {
            TOSubTask st = new TOSubTaskDiag(26781529);
            st.AddCondition(new DSCondition(0, 1, 301, true));
            st.AddCondition(new DSCondition(1, 0, 301, false));

            subTasks.Add(st);
        }
    }

    /// <summary>
    /// Задача Задача выявления ТО (пункты 3.3.1 и 3.3.2) 
    /// </summary>
    public class CSDetectTask_RCCheck_33 : DiagStateDetectTask
    {
        public CSDetectTask_RCCheck_33()
        {
            TOSubTask st = new TOSubTaskDiag(26781544);
            st.AddCondition(new DSCondition(0, 1, 77, true));
            st.AddCondition(new DSCondition(1, 0, 77, false));

            subTasks.Add(st);
        }
    }

    /// <summary>
    /// Задача выявления ТО (пункт 5.12)
    /// </summary>
    public class DSDetectTask_NetSwitchCheck_512 : DiagStateDetectTask
    {
        public DSDetectTask_NetSwitchCheck_512()
        {
            TOSubTask st = new TOSubTaskDiag(26781668);
            st.AddCondition(new DSCondition(0, 1, 2407, true));
            st.AddCondition(new DSCondition(1, 0, 2407, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 9.3.1)
    /// </summary>
    public class DSDetectTask_LampChange_931 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_931()
        {
            TOSubTask st = new TOSubTaskDiag(26781516);
            st.AddCondition(new DSCondition(0, 1, 1200, true));
            st.AddCondition(new DSCondition(1, 0, 1200, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 9.3.2)
    /// </summary>
    public class DSDetectTask_LampChange_932 : DiagStateDetectTask
    {
        public DSDetectTask_LampChange_932()
        {
            TOSubTask st = new TOSubTaskDiag(26781517);
            st.AddCondition(new DSCondition(0, 1, 1200, true));
            st.AddCondition(new DSCondition(1, 0, 1200, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 10.1.8)
    /// </summary>
    public class DSDetectTask_GroundCheck_1018 : DiagStateDetectTask
    {
        public DSDetectTask_GroundCheck_1018()
        {
            TOSubTask st = new TOSubTaskDiag(26781607);
            st.AddCondition(new DSCondition(0, 1, 2105, true));
            st.AddCondition(new DSCondition(1, 0, 2105, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 14.4)
    /// </summary>
    public class DSDetectTask_UkspsCheck_144 : DiagStateDetectTask
    {
        public DSDetectTask_UkspsCheck_144()
        {
            TOSubTask st = new TOSubTaskDiag(26782306);     /*26781688*/ // работа стала относиться к 14.1
            st.AddCondition(new DSCondition(0, 1, 2402, true));
            st.AddCondition(new DSCondition(1, 0, 2402, false));

            subTasks.Add(st);
        }
    }

    /// <summary>
    /// Задача выявления ТО (пункт 14.1.1) 240
    /// </summary>
    public class DSDetectTask_UkspsCheck_1411 : DiagStateDetectTask
    {
        public DSDetectTask_UkspsCheck_1411()
        {
            TOSubTask st = new TOSubTaskDiag(26781685);
            st.AddCondition(new DSCondition(0, 1, 2402, true));
            st.AddCondition(new DSCondition(1, 0, 2402, false));

            subTasks.Add(st);
        }
    }

    /// <summary>
    /// Задача выявления ТО (пункт 14.1.2) 245
    /// </summary>
    public class DSDetectTask_UkspsCheck_1412 : DiagStateDetectTask
    {
        public DSDetectTask_UkspsCheck_1412()
        {
            TOSubTask st = new TOSubTaskDiag(26782306);
            st.AddCondition(new DSCondition(0, 1, 2402, true));
            st.AddCondition(new DSCondition(1, 0, 2402, false));

            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 15.2)
    /// </summary>
    public class DSDetectTask_BrakeCheck_152 : DiagStateDetectTask
    {
        public DSDetectTask_BrakeCheck_152()
        {
            TOSubTask st = new TOSubTaskDiag(26781691);
            st.AddCondition(new DSCondition(0, 1, 2700, true));
            st.AddCondition(new DSCondition(1, 0, 2700, false));

            subTasks.Add(st);
        }
    }

    /// <summary>
    /// Задача выявления ТО (пункт 1.2)
    /// </summary>
    public class CSDetectTask_LightCheck_12 : DiagStateDetectTask
    {
        public CSDetectTask_LightCheck_12()
        {
            TOSubTask st = new TOSubTaskDiag(26781488);
            st.AddCondition(new DSCondition(0, 1, 925, true));
            st.AddCondition(new DSCondition(1, 0, 925, false));
            subTasks.Add(st);
        }
    }

    #endregion
    /// <summary>
    /// Задача выявления ТО по состояниям
    /// </summary>
    public abstract class StateDetectTask : UniObjectUpTO.TOTask
    {
        /// <summary>
        /// Инициализация алгоритма
        /// </summary>
        /// <returns></returns>
        public override void Create()
        {
            base.Create();
            // подписываемся на изменение данного объекта
            AddWatchToObject(_controlObj);
        }

        /// <summary>
        /// Обработка обновления перечня ДС
        /// </summary>
        public override void UpdateObjectDiagState()
        {
            UpdateSubtasks();
        }
        /// <summary>
        /// Обработка обновления значений сигналов
        /// </summary>
        public override void UpdateObjectSignals()
        { }

        public void UpdateObjectState()
        {
            UpdateObjectDiagState();
        }
    }

    public class STDetectTask_DSN : StateDetectTask
    {
        public STDetectTask_DSN()
        {
            TOSubTask st = new TOSubTaskState(111111111);  // изменить на необходимое
            st.AddCondition(new SCondition(0, 1, 4, true));
            st.AddCondition(new SCondition(1, 0, 3, true));
            subTasks.Add(st);
        }
    }
    /// <summary>
    /// Задача выявления ТО по задачам контроля/измерения
    /// </summary>
    public abstract class ControlSigDetectTask : UniObjectUpTO.TOTask
    {
        #region Загрузка сигналов, подписка на их изменение
        /// <summary>
        /// Получаем сигнал у объекта контроля и подписываемся на рассылку его изменения
        /// </summary>
        /// <param name="sigName">Имя сигнала</param>
        /// <param name="sig">Логика сигналов для инициализации</param>
        /// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
        public bool AssignToSig(string sigName, out ISigDiscreteLogic sig)
        {
            sig = null;
            if (sigName.Length == 0)
            {
                Console.WriteLine("BaseObjState: Can't get reference for signal without name for object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            // Получаем и подписываемся на рассылку изменения для сигнала sigName
            ObjData objData = _controlObj.GetObject(sigName) as ObjData;
            if (objData == null)
            {
                Console.WriteLine("BaseObjState: Can't find " + sigName + " in object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            if (!objData.IsSigDiscretLogic)
            {
                Console.WriteLine("BaseObjState: " + sigName + " is not 'SigDiscretLogic' in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            foreach (IOSignal s in objData.SigDiscreteLogic.IOSignals)
            {
                AddWatchToSignal(s);
            }
            sig = objData.SigDiscreteLogic;

            return true;
        }
        /// <summary>
        /// Получаем сигнал у объекта контроля и подписываемся на рассылку его изменения
        /// </summary>
        /// <param name="sigName">Имя сигнала</param>
        /// <param name="sig">Логика сигналов для инициализации</param>
        /// <returns>true, если все успешно. false, если у объекта контроля нет такого сигнала</returns>
        public bool AssignToSig(string sigName, out ISigAnalog sig)
        {
            sig = null;
            if (sigName.Length == 0)
            {
                Console.WriteLine("BaseObjState: Can't get reference for signal without name for object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            // Получаем и подписываемся на рассылку изменения для сигнала sigName
            UniObjData objData = _controlObj.GetObject(sigName) as UniObjData;
            if (objData == null)
            {
                Console.WriteLine("BaseObjState: Can't find " + sigName + " in object " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            if (!objData.IsSigAnalog)
            {
                Console.WriteLine("BaseObjState: " + sigName + " is not 'SigAnalog' in " + _controlObj.Site.Name + "/" + _controlObj.Name);
                return false;
            }
            foreach (IOSignal s in objData.SigAnalog.IOSignals)
            {
                AddWatchToSignal(s);
            }
            sig = objData.SigAnalog;

            return true;
        }
        /// <summary>
        /// Подписка на изменения сигналов задачи контроля
        /// </summary>
        /// <param name="sigName">Название задачи контроля</param>
        /// <returns>Признак успешности операции</returns>
        protected bool AssignToControlTask(string sigName)
        {
            // подписываемся на рассылку изменения сигнала контроля
            if (AssignToSig(sigName, out _dSig))
            {
                controlTasks.Add(sigName, _dSig);
                controlTaskStates.Add(new KeyValuePair<string, bool>(sigName, false));
                return true;
            }
            else
            {
                LogAbsentTask(ControlObj.Site.Name, ControlObj.Name, sigName);
                return false;
            }
        }
        /// <summary>
        /// Подписка на изменения сигналов задачи измерения
        /// </summary>
        /// <param name="sigName">Название задачи измерения</param>
        /// <returns>Признак успешности операции</returns>
        protected bool AssignToMeasureTask(string sigName)
        {
            // подписываемся на рассылку изменения сигнала контроля
            if (AssignToSig(sigName, out _aSig))
            {
                measureTasks.Add(sigName, _aSig);
                return true;
            }
            else
            {
                LogAbsentTask(ControlObj.Site.Name, ControlObj.Name, sigName);
                return false;
            }
        }
        /// <summary>
        /// Внести в лог отсутствующие входные данные
        /// </summary>
        /// <param name="site">Станция</param>
        /// <param name="obj">Объект</param>
        /// <param name="task">Задача</param>
        /// <param name="sig">Сигнал</param>
        protected void LogAbsentTask(string site, string obj, string sig)
        {
            string task = Type.GetTypeArray(new object[] { this }).First().Name;
            System.IO.StreamWriter sw = new System.IO.StreamWriter(@"_absentTOTaskSignals.txt", true);
            sw.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}"
                                    , site, obj, task, GetSigCode(sig)));
            sw.Flush();
            sw.Close();
        }
        /// <summary>
        /// Получение шифра сигнала
        /// </summary>
        /// <param name="s">Имя сигнала</param>
        /// <returns>Шифр сигнала</returns>
        protected string GetSigCode(string s)
        {
            switch (s)
            {
                case "RCSig":
                    return "П";
                case "ZSig":
                    return "З";
                case "BusySig":
                    return "Ппер";
                case "MainModeRecSig":
                    return "Смена-Напр(прием)";
                case "MainModeTrSig":
                    return "Смена-Напр(Отпр)";
                case "SubModeRecSig":
                    return "ВспомПрием";
                case "SubModeTrSig":
                    return "ВспомОтпр";
                case "Sig1":
                    return "ДГА";
                case "Sig2":
                    return "ДГАакт";
                default:
                    return s;
            }
        }

        #endregion

        /// <summary>
        /// Обработка изменения сигналов объекта - вызывается, если IsModify() == TRUE
        /// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
        /// </summary>
        public override void UpdateObjectSignals()
        {
            if (HasTaskStateChanged())
                UpdateSubtasks();
        }
        /// <summary>
        /// Обработка обновления перечня ДС
        /// </summary>
        public override void UpdateObjectDiagState()
        { }

        /// <summary>
        /// Поиск подзадачи контроля
        /// </summary>
        /// <param name="taskName">Название задачи контроля</param>
        /// <returns>Задача контроля</returns>
        public ISigDiscreteLogic GetControlTask(string taskName)
        {
            if (controlTasks.ContainsKey(taskName))
                return controlTasks[taskName];
            else
                return null;
        }
        /// <summary>
        /// Поиск подзадачи измерения
        /// </summary>
        /// <param name="taskName">Название задачи контроля</param>
        /// <returns>Задача контроля</returns>
        public ISigAnalog GetMeasureTask(string taskName)
        {
            if (measureTasks.ContainsKey(taskName))
                return measureTasks[taskName];
            else
                return null;
        }
        /// <summary>
        /// Проверка изменений состояния подзадач контроля
        /// </summary>
        /// <returns></returns>
        protected bool HasTaskStateChanged()
        {
            bool res = false;
            try
            {
                for (int i = 0; i < controlTaskStates.Count; i++)
                {
                    bool tres = GetControlTask(controlTaskStates[i].Key).On;
                    if (controlTaskStates[i].Value != tres)
                    {
                        controlTaskStates[i] = new KeyValuePair<string, bool>(controlTaskStates[i].Key, tres);
                        res |= true;
                    }
                }
            }
            catch { }
            return res;
        }

        #region Data
        /// <summary>
        /// Задача контроля
        /// </summary>
        protected ISigDiscreteLogic _dSig;
        /// <summary>
        /// Задача измерения
        /// </summary>
        protected ISigAnalog _aSig;
        /// <summary>
        /// Ссылки на подзадачи контроля
        /// </summary>
        protected Dictionary<string, ISigDiscreteLogic> controlTasks = new Dictionary<string, ISigDiscreteLogic>();
        /// <summary>
        /// Задачи измерения
        /// </summary>
        protected Dictionary<string, ISigAnalog> measureTasks = new Dictionary<string, ISigAnalog>();
        /// <summary>
        /// Состояние подзадач контроля
        /// </summary>
        protected List<KeyValuePair<string, bool>> controlTaskStates = new List<KeyValuePair<string, bool>>();
        #endregion

    }

    #region Выявление ТО по сигналам
    /* //  переделана на дс
    /// <summary>
    /// Задача выявления ТО (пункт 1.2) 
    /// </summary>
    public class CSDetectTask_LightCheck_12 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаётся номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_LightCheck_12()
        {
            TOSubTask st = new TOSubTaskControl(26781488);
            // 2. открытие пригласительного сигнала
            st.AddCondition(new CTCondition(0, 1, "WSig", true));
            // 3. перекрытие пригласительного сигнала
            st.AddCondition(new CTCondition(1, 0, "WSig", false));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();
            if (!AssignToControlTask("WSig"))
                throw new ArgumentException("Ошибка подключения к WSig объекта " + Name);
        }
    }*/
    public class CSDetectTask_LightCheck_12v2 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаётся номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_LightCheck_12v2()
        {
            TOSubTask st = new TOSubTaskControl(26781488);
            // 2. открытие пригласительного сигнала
            st.AddCondition(new CTCondition(0, 1, "ПС", true));
            // 3. перекрытие пригласительного сигнала
            st.AddCondition(new CTCondition(1, 0, "ПС", false));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();
            if (!AssignToControlTask("ПС"))
                throw new ArgumentException("Ошибка подключения к WSig объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.12)
    /// </summary>
    public class CSDetectTask_LightCheck_112 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаётся номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_LightCheck_112()
        {
            TOSubTask st = new TOSubTaskControl(26781508);
            st.AddCondition(new CTCondition(0, 1, "Sig1", true));
            //st.AddCondition(new MTCondition(1, 2, 0, 5000, 0, "USig", 125, MTCondition.Operation.Less));
            st.AddCondition(new CTCondition(1, 0, 0, /*30 * 60000*/0, 0, "Sig1", false));
            //st.AddCondition(new MTCondition(3, 0, 0, 5000, 0, "USig", 125, MTCondition.Operation.More));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("Sig1"))
                throw new ArgumentException("Ошибка подключения к Sig1 объекта " + Name);

            // Исключен из требований к задачам
            //if (!AssignToMeasureTask("USig"))
            //    return false;
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 5.2.1)
    /// </summary>
    public class CSDetectTask_StageCheck_521 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаёт номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_StageCheck_521()
        {
            #region Основной вариант
            TOSubTask st1 = new TOSubTaskControl(26781600);
            // 2. получение информации об основном режиме смены направления
            st1.AddCondition(new MultiCondition(0, 1
                , new List<Condition> { new CTCondition(0, 1, "MainModeRecSig", true)
                                      , new CTCondition(0, 1, "MainModeTrSig", true) }
                , MultiCondition.Logic.Or));
            // 3. получение информации о занятости перегона
            //st1.AddCondition(new CTCondition(1, 2, 0, 0, 3000, "Sig1", true));
            st1.AddCondition(new MultiCondition(1, 2
                , new List<Condition> { new CTCondition(1, 2, 0, 0, 3000, "Sig1", true)
                                      , new CTCondition(1, 2, 0, 0, 3000, "Sig2", true) }
                , MultiCondition.Logic.Or));

            // 4. получение информации о вспомогательной смене направления
            st1.AddCondition(new MultiCondition(2, 3
                , new List<Condition> { new CTCondition(2, 3, "SubModeRecSig", true)
                                      , new CTCondition(2, 3, "SubModeTrSig", true) }
                , MultiCondition.Logic.Or));
            // 5. получение информации о свободности перегона
            //st1.AddCondition(new CTCondition(3, 0, 0, 0, 3000, "Sig1", false));
            st1.AddCondition(new MultiCondition(3, 0
                , new List<Condition> { new CTCondition(3, 0, 0, 0, 3000, "Sig1", false)
                                      , new CTCondition(3, 0, 0, 0, 3000, "Sig2", false) }
                , MultiCondition.Logic.And));

            subTasks.Add(st1);
            #endregion

            #region Вариант по примечанию
            TOSubTask st2 = new TOSubTaskControl(26781600);
            // 3. получение информации о занятости перегона
            //st2.AddCondition(new CTCondition(0, 1, 0, 0, 3000, "Sig1", true));
            st2.AddCondition(new MultiCondition(0, 1
                , new List<Condition> { new CTCondition(0, 1, 0, 0, 3000, "Sig1", true)
                                      , new CTCondition(0, 1, 0, 0, 3000, "Sig2", true) }
                , MultiCondition.Logic.Or));

            // 4. получение информации о вспомогательной смене направления
            st2.AddCondition(new MultiCondition(1, 2, 0, 0, 0
                , new List<Condition> { new CTCondition(1, 2, 0, 0, 0, "SubModeRecSig", true)
                                      , new CTCondition(1, 2, 0, 0, 0, "SubModeTrSig", true) }
                , MultiCondition.Logic.Or));
            // 5. получение информации о свободности перегона
            //st2.AddCondition(new CTCondition(2, 3, 0, 0, 3000, "Sig1", false));
            st2.AddCondition(new MultiCondition(2, 3
                , new List<Condition> { new CTCondition(2, 3, 0, 0, 3000, "Sig1", false)
                                      , new CTCondition(2, 3, 0, 0, 3000, "Sig2", false) }
                , MultiCondition.Logic.And));

            // 2. получение информации об основном режиме смены направления
            st2.AddCondition(new MultiCondition(3, 0, 0, 0, 0
                , new List<Condition> { new CTCondition(3, 0, 0, 0, 0, "MainModeRecSig", true)
                                      , new CTCondition(3, 0, 0, 0, 0, "MainModeTrSig", true) }
                , MultiCondition.Logic.Or));
            subTasks.Add(st2);
            #endregion
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("Sig1"))
                throw new ArgumentException("Ошибка подключения к Sig1 объекта " + Name);
            AssignToControlTask("Sig2");

            if (!AssignToControlTask("MainModeRecSig"))
                throw new ArgumentException("Ошибка подключения к MainModeRecSig объекта " + Name);

            if (!AssignToControlTask("MainModeTrSig"))
                throw new ArgumentException("Ошибка подключения к MainModeTrSig объекта " + Name);

            if (!AssignToControlTask("SubModeRecSig"))
                throw new ArgumentException("Ошибка подключения к SubModeRecSig объекта " + Name);

            if (!AssignToControlTask("SubModeTrSig"))
                throw new ArgumentException("Ошибка подключения к SubModeTrSig объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 11.4.1)
    /// </summary>
    public class CSDetectTask_DGACheck_1141 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаётся номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_DGACheck_1141()
        {
            TOSubTask st = new TOSubTaskControl(26781639);
            st.AddCondition(new MultiCondition(0, 1
                    , new List<Condition> { new CTCondition(0, 1, "ДГА", true)
                                          , new CTCondition(0, 1, "ДГАакт", false) }
                    , MultiCondition.Logic.And));
            st.AddCondition(new CTCondition(1, 0, "ДГА", false));
            
            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("ДГА"))
                throw new ArgumentException("Ошибка подключения к ДГА объекта " + Name);

            if (!AssignToControlTask("ДГАакт"))
                throw new ArgumentException("Ошибка подключения к ДГАакт объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 11.4.4)
    /// </summary>
    public class CSDetectTask_DGACheck_1144 : ControlSigDetectTask
    {
        /// <summary>
        /// Конструктор (задаётся номер пункта ТО и набор условий выполнения задачи)
        /// </summary>
        public CSDetectTask_DGACheck_1144()
        {
            TOSubTask st = new TOSubTaskControl(26781642);
            // Отсутствие (отключение) 2-го и 1-го фидера одновременно («Откл2Хф»=1 или «ФН» = 0 фидера 1 и «ФН» = 0 фидера 2)
            st.AddCondition(/*new MultiCondition(0, 1,*/
                   /* , new List<Condition> { new CTCondition(0, 1, "NoFeedSig", true, false)
                                          , */new MultiCondition(0, 1
                                                        , new List<Condition> { new CTCondition(0, 1, "ФН1", false)
                                                                              , new CTCondition(0, 1, "ФН2", false)}
                                                        , MultiCondition.Logic.And)//}
                    /*, MultiCondition.Logic.Or)*/);
            // Подключение ДГА «ДГА»=1 и «ДГАакт»=1»
            //st.AddCondition(new MultiCondition(1, 2
            //        , new List<Condition> { new CTCondition(1, 2, "Sig1", true)
            //                              , new CTCondition(1, 2, "Sig2", true) }
            //        , MultiCondition.Logic.And));

            // Переход ДГА во включенное состояние «ДГА»=1 (подключение)
            st.AddCondition(new CTCondition(1, 2, "ДГА", true));
            // Переход ДГА во включенное состояние «ДГАакт»=1 (активность)
            st.AddCondition(new CTCondition(2, 3, "ДГАакт", true));
            
            // Наличие фидеров питания («Откл2хФ=0» или, «ФН» = 1 фидера 1 и «ФН» = 0 фидера 2 или, ФН = 0 фидера 1 и «ФН» = 1 фидера 2
            st.AddCondition(new MultiCondition(3, 4
                    , new List<Condition> { /*new CTCondition(3, 4, "NoFeedSig", false, false),*/
                                           new MultiCondition(3, 4
                                                        , new List<Condition> { new CTCondition(3, 4, "ФН1", true)
                                                                              , new CTCondition(3, 4, "ФН2", false)}
                                                        , MultiCondition.Logic.And)
                                          , new MultiCondition(3, 4
                                                        , new List<Condition> { new CTCondition(3, 4, "ФН1", false)
                                                                              , new CTCondition(3, 4, "ФН2", true)}
                                                        , MultiCondition.Logic.And) }
                    , MultiCondition.Logic.Or));
            // Переход ДГА в отключенное состояние «ДГА»=0
            st.AddCondition(new CTCondition(4, 0, "ДГА", false));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("ДГА"))
                throw new ArgumentException("Ошибка подключения к ДГА объекта " + Name);

            if (!AssignToControlTask("ДГАакт"))
                throw new ArgumentException("Ошибка подключения к ДГАакт объекта " + Name);

            bool b1 = true, b2 = true;
            b1 &= AssignToControlTask("NoFeedSig");
            b2 &= AssignToControlTask("ФН1");
            b2 &= AssignToControlTask("ФН2");

            if((b1 || b2) == false)
                throw new ArgumentException("Ошибка подключения к сигналам объекта " + Name); ;
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункты 3.3.1, 3.3.2)
    /// </summary>
    /*public class CSDetectTask_RCCheck_33 : ControlSigDetectTask
    {
        public CSDetectTask_RCCheck_33()
        {
            TOSubTask st = new TOSubTaskControl(26781544, new List<int> { 0, 1 });
            // 2. получение информации о занятости РЦ без замыкания на период не менее 3с
            st.AddCondition(new MultiCondition(0, 1, 0, 0, 0
                , new List<Condition> { new CTCondition(0, 1, "RCSig", true)
                                      , new CTCondition(0, 1, "ZSig", false) }
                , MultiCondition.Logic.And));
            // 3. получение информации о свободности РЦ на период не менее 1,3с
            st.AddCondition(new CTCondition(1, 2, 3000, 10 * 60000, 0, "RCSig", false));
            // 4. получение информации о занятости РЦ без замыкания на период не менее 3с
            st.AddCondition(new MultiCondition(2, 3, 1300, 8 * 60 * 60000, 0
                , new List<Condition> { new CTCondition(2, 3, "RCSig", true)
                                      , new CTCondition(2, 3, "ZSig", false) }
                , MultiCondition.Logic.And));
            // 5. получение информации о свободности РЦ на период не менее 1,3с
            st.AddCondition(new CTCondition(3, 0, 3000, 10 * 60000, 1300, "RCSig", false));
            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("RCSig"))
                throw new ArgumentException("Ошибка подключения к RCSig объекта " + Name);

            if (!AssignToControlTask("ZSig"))
                throw new ArgumentException("Ошибка подключения к ZSig объекта " + Name);
        }
    }*/

    #region Дублирование функциональности задач по ДС (требования скорректированы, т.к. ПО ЛПД не формируют нужных сбоев (входных данных для задачи))
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.1.1 и 1.4.1.2)
    /// </summary>
    public class CSDetectTask_LampChange_141 : ControlSigDetectTask
    {
        public CSDetectTask_LampChange_141()
        {
            TOSubTask st = new TOSubTaskControl(26781490);

            // 2. получение информации о занятости РЦ без замыкания на период не менее 1.3с
            st.AddCondition(new CTCondition(0, 1, 0, 0, 1300, "IntegrityRSig", false));
            // 3. получение информации о свободности РЦ на период не менее 1,3с
            st.AddCondition(new CTCondition(1, 0, 0, 0, 1300, "IntegrityRSig", true));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("IntegrityRSig"))
                throw new ArgumentException("Ошибка подключения к IntegrityRSig объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.2.1 и 1.4.2.2)
    /// </summary>
    public class CSDetectTask_LampChange_142 : ControlSigDetectTask
    {
        public CSDetectTask_LampChange_142()
        {
            TOSubTask st = new TOSubTaskControl(26781492);

            // 2. получение информации о занятости РЦ без замыкания на период не менее 3с
            st.AddCondition(new CTCondition(0, 1, 0, 0, 1300, "IntegritySig", false));
            // 3. получение информации о свободности РЦ на период не менее 1,3с
            st.AddCondition(new CTCondition(1, 0, 0, 0, 1300, "IntegritySig", true));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("IntegritySig"))
                throw new ArgumentException("Ошибка подключения к IntegritySig объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 1.4.3.1 и 1.4.3.2)
    /// </summary>
    public class CSDetectTask_LampChange_143 : ControlSigDetectTask
    {
        public CSDetectTask_LampChange_143()
        {
            // выходные, маршрутные
            TOSubTask st = new TOSubTaskControl(26781495);

            // 2. получение информации о занятости РЦ без замыкания на период не менее 3с
            st.AddCondition(new MultiCondition(0, 1, 0, 0, 1300
                , new List<Condition> { new CTCondition(0, 1, "IntegritySig", false)
                                      , new CTCondition(0, 1, "IntegrityRSig", false) }
                , MultiCondition.Logic.Or));
            // 3. получение информации о занятости РЦ без замыкания на период не менее 3с
            st.AddCondition(new MultiCondition(1, 0, 0, 0, 1300
                , new List<Condition> { new CTCondition(1, 0, "IntegritySig", true)
                                      , new CTCondition(1, 0, "IntegrityRSig", true) }
                , MultiCondition.Logic.And));
            
            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            bool res = false;
            res |= AssignToControlTask("IntegritySig");
            res |= AssignToControlTask("IntegrityRSig");

            if( res == false )
                throw new ArgumentException("Ошибка подключения к сигналам объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 5.12)
    /// </summary>
    public class CSDetectTask_NetSwitchCheck_512 : ControlSigDetectTask
    {
        public CSDetectTask_NetSwitchCheck_512()
        {
            TOSubTask st = new TOSubTaskControl(26781668);

            // 2. Потеря контроля ПКС...
            st.AddCondition(new MultiCondition(0, 1, 0, 0, 0
                , new List<Condition> { new CTCondition(0, 1, "ACVoltage", false)
                                      , new CTCondition(0, 1, "DCVoltage", false) }
                , MultiCondition.Logic.And));
            // 3. ...на время более 3 секунд и не более 10 секунд
            st.AddCondition(new MultiCondition(1, 0, 3000, 5 * 60000, 0
                , new List<Condition> { new CTCondition(0, 1, "ACVoltage", true)
                                      , new CTCondition(0, 1, "DCVoltage", true) }
                , MultiCondition.Logic.Or));

            subTasks.Add(st);
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("ACVoltage"))
                throw new ArgumentException("Ошибка подключения к ACVoltage объекта " + Name);
            if (!AssignToControlTask("DCVoltage"))
                throw new ArgumentException("Ошибка подключения к DCVoltage объекта " + Name);
        }
    }
    /// <summary>
    /// Задача выявления ТО (пункт 15.2)
    /// </summary>
    public class CSDetectTask_BrakeCheck_152 : ControlSigDetectTask
    {
        public CSDetectTask_BrakeCheck_152()
        {
            TOSubTask st1 = new TOSubTaskControl(26781691);

            #region Основной вариант выполнения задачи
            // 2. Фрикционный перевод УТС из п положения снят  в положение установлен («УУ =1»  –  «СУПК=1» – «УУ =0» – «УУПК=1» – «УУ =1»)
            st1.AddCondition(new CTCondition(0, 1, 0, 0, 0, "SetSig", true));
            st1.AddCondition(new CTCondition(1, 2, 0, 0, 0, "UnsetControlSig", true));
            st1.AddCondition(new CTCondition(2, 3, 0, 0, 0, "SetSig", false));
            st1.AddCondition(new CTCondition(3, 4, 0, 0, 0, "SetControlSig", true));
            st1.AddCondition(new CTCondition(4, 5, 0, 0, 0, "SetSig", true));
            // 3. Фрикционный перевод УТС из  положения установлен в положение снят («СУ=1» – «УУПК=1» – «СУ =0» – «СУПК=1» – «СУ =1»)
            st1.AddCondition(new CTCondition(5, 6, 0, 0, 0, "UnsetSig", true));
            st1.AddCondition(new CTCondition(6, 7, 0, 0, 0, "UnsetSig", false));
            st1.AddCondition(new CTCondition(7, 0, 0, 0, 0, "UnsetSig", true));
            #endregion

            subTasks.Add(st1);

            TOSubTask st2 = new TOSubTaskControl(26781691);

            #region Дополнительный вариант выполнения задачи (по примечанию в требованиях)
            // 3. Фрикционный перевод УТС из  положения установлен в положение снят («СУ=1» – «УУПК=1» – «СУ =0» – «СУПК=1» – «СУ =1»)
            st2.AddCondition(new CTCondition(0, 1, 0, 0, 0, "UnsetSig", true));
            st2.AddCondition(new CTCondition(1, 2, 0, 0, 0, "SetControlSig", true));
            st2.AddCondition(new CTCondition(2, 3, 0, 0, 0, "UnsetSig", false));
            st2.AddCondition(new CTCondition(3, 4, 0, 0, 0, "UnsetControlSig", true));
            st2.AddCondition(new CTCondition(4, 5, 0, 0, 0, "UnsetSig", true));
            // 2. Фрикционный перевод УТС из п положения снят  в положение установлен («УУ =1»  –  «СУПК=1» – «УУ =0» – «УУПК=1» – «УУ =1»)
            st2.AddCondition(new CTCondition(5, 6, 0, 0, 0, "SetSig", true));
            st2.AddCondition(new CTCondition(6, 7, 0, 0, 0, "SetSig", false));
            st2.AddCondition(new CTCondition(7, 0, 0, 0, 0, "SetSig", true));
            #endregion

            subTasks.Add(st2);
        
        }
        /// <summary>
        /// Подписка на рассылку изменений сигнала контроля
        /// </summary>
        /// <returns>Признак успешности операции</returns>
        public override void Create()
        {
            base.Create();

            if (!AssignToControlTask("UnsetSig"))
                throw new ArgumentException("Ошибка подключения к UnsetSig объекта " + Name);
            if (!AssignToControlTask("SetSig"))
                throw new ArgumentException("Ошибка подключения к SetSig объекта " + Name);
            if (!AssignToControlTask("UnsetControlSig"))
                throw new ArgumentException("Ошибка подключения к UnsetControlSig объекта " + Name);
            if (!AssignToControlTask("SetControlSig"))
                throw new ArgumentException("Ошибка подключения к SetControlSig объекта " + Name);
        }
    }
    #endregion

    #endregion
}