﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using AfxEx;
using Ivk.IOSys;

namespace Tdm.Unification
{
    /// <summary>
    /// Интерфейс владельца асинхронной операции
    /// </summary>
    public interface IAsyncOperationProvider
    {
        /// <summary>
        /// асинхронная операция
        /// </summary>
        AsyncOperation AsyncOperation
        {
            get;
        }
    }
    
    /// <summary>
	/// Коллектор для передачи обработки на GUI поток
	/// </summary>
    public class GuiCollector  : DlxCollection , ICollector
    {
		/// <summary>
		/// данные для передачи в GUI поток
		/// </summary>
        class DataGuiInfo
        {
			/// <summary>
			/// данные
			/// </summary>
            public object data;
			/// <summary>
			/// подсистема
			/// </summary>
            public Ivk.IOSys.IOBaseElement iosys;
			/// <summary>
			/// время
			/// </summary>
            public DateTime InfoTime;
        }
		/// <summary>
		/// установить данные
		/// </summary>
		/// <param name="DataGuiInfo"></param>
        private void SetDataGui(object DataGuiInfo)
        {
            DataGuiInfo dgi = DataGuiInfo as DataGuiInfo;
            Debug.Assert(dgi != null);

			if (dgi.iosys == null && dgi.data != null)
			{
				List<UnificationConcentrator.UniDataWithObjSys> dataList = dgi.data as List<UnificationConcentrator.UniDataWithObjSys>;
				if (dataList != null)
				{
					foreach (UnificationConcentrator.UniDataWithObjSys dataWithSys in dataList)
					{
						try
						{
							// обновляем время
							dataWithSys._objSys.DataTime = dataWithSys._data._dataTime;
							// разбор данных
							dataWithSys._objSys.ProcessDataObj(dataWithSys._data);
						}
						catch (Exception e)
						{
							Trace.WriteLine("GuiCollector " + Name + ": Ошибка обработки данных для элемента " + dataWithSys._objSys.Owner.Name + "/" + dataWithSys._objSys.Name + ": " + e.ToString());
							// неизвестное состояние
							dataWithSys._objSys.SetInvalidData();
						}
					}

					foreach (UnificationConcentrator.UniDataWithObjSys dataWithSys in dataList)
					{
						dataWithSys._objSys.SetValidData();
						//dataWithSys._objsOwner.RaiseDataChangedEvent(false);
					}

					return;
				}
			}

            UnificationConcentrator.UniDataItem data = dgi.data as UnificationConcentrator.UniDataItem;

            Ivk.IOSys.IOBaseElement targetElement = dgi.iosys;

            UniObjSystem uos = targetElement as UniObjSystem;
            if (uos == null)
                throw new Exception("Тип параметра targetElement не наследует от типа UniObjSystem.");

            if (data == null)
                uos.SetInvalidData();
            else
            {
                try
                {

					// известное состояние
					uos.DataTime = data._dataTime;
                    // разбор данных
                    uos.ProcessDataObj(data);

                    uos.SetValidData();
                    //uos.RaiseDataChangedEvent(false);
                }
                catch (Exception e)
                {
					Trace.WriteLine("GuiCollector " + Name + ": Ошибка обработки данных для элемента " + uos.Name + ": " + e);
                    // все элементы в неизвестное сосотяние
                    uos.SetInvalidData();
                }
            }
        }
        /// <summary>
        /// установить данные
        /// </summary>
        /// <param name="DataGuiInfo"></param>
        private void SetDataGui2(object DataGuiInfo)
        {
            DataGuiInfo dgi = DataGuiInfo as DataGuiInfo;
            Debug.Assert(dgi != null);

            IOBaseElement targetElement = dgi.iosys;
            BaseIOSystem uos = targetElement as BaseIOSystem;
            byte[] buffer = dgi.data as byte[];

            if (uos != null)
                uos.DataTime = dgi.InfoTime;

            if (buffer == null)
                targetElement.SetInvalidData();
            else
            {
                try
                {
                    BinaryReader reader = new BinaryReader(new MemoryStream(buffer));
                    targetElement.ProcessData(reader);
                    if (targetElement is BaseIOSystem)
                        (targetElement as BaseIOSystem).SetValidData();
                    else
                    {
                        targetElement.IOSystem.RaiseDataChangedEvent(targetElement.IsDataChanged);
                        //targetElement.RaiseDataChangedEvent(false);
                    }
                }
                catch (Exception e)
                {
                    Trace.WriteLine("GuiCollector " + Name + ": Ошибка обработки данных для элемента " + targetElement.Name + ": " + e);

                    // все элементы в неизвестное сосотяние
                    targetElement.SetInvalidData();
                }
            }
        }
        #region ICollector Members

        /// <summary>
        /// Установить данные (в reader) от клиента client с временем time для элемента targetElement
        /// </summary>
        /// <param name="data">данные</param>
        /// <param name="client">клиент, передающий данные</param>
        /// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
        /// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
        public void SetData(byte[] data, ClientBase client, DateTime time, Ivk.IOSys.IOBaseElement targetElement)
        {
            if (_aop.AsyncOperation != null)
            {
                DataGuiInfo dgi = new DataGuiInfo();
                dgi.data = data;
                dgi.iosys = targetElement;
                dgi.InfoTime = time;
                _aop.AsyncOperation.Post(SetDataGui2, dgi);
            }
        }

        /// <summary>
        /// Установить данные (в reader) от клиента client с временем time для элемента targetElement
        /// </summary>
        /// <param name="data">данные</param>
        /// <param name="client">клиент, передающий данные</param>
        /// <param name="time">время соответствующее данным (если 0, то используется локальное время)</param>
        /// <param name="targetElement">элемент, которому предназначены данные (может быть не задан)</param>
        public void SetData(object data, ClientBase client, DateTime time, Ivk.IOSys.IOBaseElement targetElement)
        {
           if (_aop.AsyncOperation != null)
            {
                DataGuiInfo dgi = new DataGuiInfo();
                dgi.data = data;
                dgi.iosys = targetElement;
                _aop.AsyncOperation.Post(SetDataGui, dgi);
            }
        }

        #endregion
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            _aop = Loader.GetObjectFromAttribute("Provider") as IAsyncOperationProvider;
            if (_aop == null)
                throw new ArgumentNullException("Параметр Provider не задан");

            base.Load(Loader);
        }
		/// <summary>
		/// Хранитель асинхронного обработчика
		/// </summary>
        private IAsyncOperationProvider _aop;
    }
}
