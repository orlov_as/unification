﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using AfxEx;
using Ivk.IOSys;
using Tdm;
using Dps.UniRequest;

namespace Tdm.Unification
{
	/// <summary>
	/// Класс соединения с ДЦ-Юг с РКП через файл
	/// </summary>
	public class FileConnection : AConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public FileConnection(FileStream file)
		{
			_file = file;
		}

		/// <summary>
		/// Переопределен метод для иммитации отправки заголовка
		/// </summary>
		/// <param name="Hd">заголовк в старом формате (должен приводится к типу OldHeader)</param>
		protected override void OnSendHeader(Header Hd)
		{
			if (Hd == null)
				throw new ArgumentNullException("Header");

			_lastSendedID = Hd.ID;
		}
		/// <summary>
		/// Переопределен метод для иммитации получения заголовка 
		/// </summary>
		/// <returns>Заголовок сообщения</returns>
		protected override Header OnReceiveHeader()
		{
			if (_file == null)
				throw new ObjectDisposedException(this.GetType().FullName);
			Header Hd = new Header();
			
			Hd.Code = RequestCode.CurrentState;
			Hd.ID = _lastSendedID;
			Hd.Size = (uint)(_file.Length - _skipBytes);

			// сдвигаемся к началу
			_file.Seek(_skipBytes, SeekOrigin.Begin);
			
			return Hd;
		}
		/// <summary>
		/// Имиитация отправки данных
		/// </summary>
		/// <param name="Buffer">буффер</param>
		/// <param name="Index">позиция</param>
		/// <param name="Count">количество</param>
		protected override void OnSendData(byte[] Buffer, int Index, int Count)
		{
			// ничего не делаем
		}
		/// <summary>
		/// Получение данных
		/// </summary>
		/// <param name="Buffer">буффер</param>
		/// <param name="Index">позиция</param>
		/// <param name="Count">количество</param>
		/// <returns></returns>
		protected override int OnReceiveData(byte[] Buffer, int Index, int Count)
		{
			if (_file == null)
				throw new ObjectDisposedException(this.GetType().FullName);

			// читаем из файла
			int Res = _file.Read(Buffer, Index, Count);
			if (Res == 0)
				throw new EndOfStreamException();
			return Res;
		}

		/// <summary>
		/// файл для чтения
		/// </summary>
		protected FileStream _file;
		/// <summary>
		/// идентификатор поледнего отправленного сообщения
		/// </summary>
		protected uint _lastSendedID = 0;
		/// <summary>
		/// Количество пропускаемых байт
		/// </summary>
		public int _skipBytes = 0;
		/// <summary>
		/// Разрушение
		/// </summary>
		/// <param name="disposing"></param>
		protected override void  Dispose(bool disposing)
		{
			if (disposing && _file != null)
				_file.Close();
			_file = null;

 			base.Dispose(disposing);
		}
	}
	/// <summary>
	/// Соединение читающее последовательность файлов 
	/// </summary>
	public class FileListConnection : FileConnection
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="filename">имя файла</param>
		public FileListConnection(string filename):base(null)
		{
			_filename = filename;
		}
		/// <summary>
		/// получение заголовка
		/// </summary>
		/// <returns>заголовок</returns>
		protected override Header OnReceiveHeader()
		{
			if (_file != null)
			{
				_file.Close();
				_file = null;
			}

			for (int i = 0; i < 2; i++)
			{
				try
				{
					_file = new FileStream(_filename + "_" + _fileIndex, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
					_fileIndex++;
					break;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("FileListConnection: " + ex.Message);
					_fileIndex = 0;
				}
			}

			return base.OnReceiveHeader();
		}
		/// <summary>
		/// текущий индекс файла
		/// </summary>
		protected int _fileIndex = 1;
		/// <summary>
		/// путь к файлу
		/// </summary>
		protected string _filename = string.Empty;
	}

	/// <summary>
	/// Класс фабрики клиентских соединений с ДЦ-Юг с РКП через файл
	/// </summary>
	public class DCrkpConnectionFactory : ConnectionFactory
	{
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="Loader">загрузчик</param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			_listOfFiles = Loader.GetAttributeBool("list", _listOfFiles);
			_skipBytes = Loader.GetAttributeInt("skipbytes", _skipBytes);
			_filePath = Loader.GetAttributeString("ShareFilePath", string.Empty);
			if (!_listOfFiles && !File.Exists(_filePath))
				throw new FileNotFoundException("Не найден файл для обмена с ДЦ-Юг с РКП (ShareFilePath=" + _filePath + ")");
		}
		/// <summary>
		/// Переопределен метод создания соединения 
		/// </summary>
		/// <returns>Соединение</returns>
		public override AConnection CreateConnection()
		{
			if (!_listOfFiles)
			{
				FileStream file = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
				FileConnection Cln = new FileConnection(file);
				Cln._skipBytes = _skipBytes;
				return Cln;
			}
			else 
			{
				FileListConnection Cln = new FileListConnection(_filePath);
				Cln._skipBytes = _skipBytes;
				return Cln;
			}
		}
		/// <summary>
		/// Путь к файлу для обмена с ДЦ-Юг с РКП
		/// </summary>
		protected string _filePath = string.Empty;
		/// <summary>
		/// Количество пропускаемых байт
		/// </summary>
		protected int _skipBytes = 0;
		/// <summary>
		/// Список файлов
		/// </summary>
		protected bool _listOfFiles = false;
	}
	/// <summary>
	/// Клиент обмена с ДЦ-Юг с РКП
	/// </summary>
	public class DCrkpRequestClient : RequestClientBase
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public DCrkpRequestClient()
		{
		}
		/// <summary>
		/// Загрузка (производится загрузка соответствия КП и станций АДК)
		/// </summary>
		/// <param name="Loader"></param>
		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);
			
			// получаем коллекцию станций
			_siteList = Loader.GetObjectFromAttribute("SiteList", typeof(DlxCollection)) as DlxCollection;
			// получаем соостветствие КП и станций
			List<XmlElement> kpAccordance;
			Loader.CurXmlElement.EnumInhElements(out kpAccordance, "kp", null, null);
			int kpid = 0;
			string sitepath;
			foreach (XmlElement elem in kpAccordance)
			{
				sitepath = string.Empty;
				if (elem.GetAttributeValue("kpid", ref kpid) && elem.GetAttribute("sitepath", out sitepath))
					_idsysTOpathMAP.Add(kpid, sitepath);
			}
		}
		/// <summary>
		/// Инициализация (производится поиск подсистем)
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			DlxObject obj;
			foreach (int kpid in _idsysTOpathMAP.Keys)
			{
				obj = _siteList.FindObject(_idsysTOpathMAP[kpid] + "/IOSys");
				if (obj != null && obj is BaseIOSystem)
					_idsysTOrefMAP.Add(kpid, obj as BaseIOSystem);
				else
				{
					throw new ArgumentException("DCrkpRequestClient " + Name + ": Can't find site by name " + _idsysTOpathMAP[kpid]);
				}
			}

			base.Create();
		}
		/// <summary>
		/// Обработка ответа
		/// </summary>
		protected override void ProcessAnswer()
		{
			BinaryReader reader = new BinaryReader(new MemoryStream( _answerMsg.Data));
			GidUralHeader dataHeader = new GidUralHeader();
			// читаем заголовок
			dataHeader.Load(reader);
			// проверяем размеры
			if (_answerMsg.Data.Length < 32 + dataHeader._nRecLen * dataHeader._nCountKP)
				throw new RequestException(ErrorCode.WrongFormat);

			int kpID;
			DateTime kpTime;
			byte[] data;
			BaseIOSystem iosys;
			// читаем данные по каждой станции
			for (int i = 0; i < dataHeader._nCountKP; i++)
			{
				// идентификатор станции
				kpID = reader.ReadInt32();
				// время данных
				kpTime = Afx.CTime.ToDateTime(reader.ReadInt32());
				// резерв
				reader.ReadBytes(8);
				// данные ТС
				data = reader.ReadBytes(dataHeader._nRecLen - 16);
				// если есть такая подсистема, то передаем коллектору
				if (_idsysTOrefMAP.TryGetValue(kpID, out iosys))
				{
					SetData(data, kpTime, iosys);
					// добавляем в перечень станций
					if (_addKpIDtoList && !_kpRecievedList.Contains(kpID))
						_kpRecievedList.Add(kpID);
				}
			}
			_addKpIDtoList = false;
		}
		/// <summary>
		/// Обработка ошибки
		/// </summary>
		protected override void OnExchengeError()
		{
			foreach (int kp in _kpRecievedList)
				SetData(null, DateTime.Now, _idsysTOrefMAP[kp]);
		}

		#region Данные
		/// <summary>
		/// MAP станций
		/// </summary>
		private Dictionary<int, BaseIOSystem> _idsysTOrefMAP = new Dictionary<int, BaseIOSystem>();
		/// <summary>
		/// MAP путей к станциям
		/// </summary>
		private Dictionary<int, string> _idsysTOpathMAP = new Dictionary<int, string>();

		/// <summary>
		/// Список станций, по которым приходили данные
		/// </summary>
		protected List<int> _kpRecievedList = new List<int>();
		/// <summary>
		/// Признак необходимости добавить в список КП, по которым есть данные
		/// </summary>
		protected bool _addKpIDtoList = true;
		/// <summary>
		/// Коллекция станций
		/// </summary>
		protected DlxCollection _siteList;

		#endregion
	}
}
