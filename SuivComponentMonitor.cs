﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using AfxEx;
using static Tdm.Unification.DBConnector;

namespace Tdm.Unification
{
	/// <summary>
	/// Позволяет передавать в БД состояние компонента СУИВ
	/// </summary>
	public interface ISuivComponent
	{
		event Action<DlxObject, string, int, DateTime?, string> SendState;
		event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;
	}

	/// <summary>
	/// Отслеживает состояние компонентов СУИВ и пишет его в БД
	/// </summary>
	public class SuivComponentMonitor : DlxObject
	{
		private class SuivComponent
		{
			string componentName;
			int order, timeout;
			public string ComponentName { get { return componentName; } }
			public int Order { get { return order; } }
			public int Timeout { get { return timeout; } }
			public SuivComponent(string componentName, int order, int timeout)
			{
				this.componentName = componentName;
				this.order = order;
				this.timeout = timeout;
			}
		}

		string connectionString, serviceName;
		DlxCollection siteList = new DlxCollection();
		Dictionary<DlxObject, SuivComponent> components = new Dictionary<DlxObject, SuivComponent>();
		BackgroundWorker worker = new BackgroundWorker();

		public override void Create()
		{
			base.Create();

			worker.DoWork += new DoWorkEventHandler(worker_DoWork);
			worker.RunWorkerAsync();
		}

		public override void Load(DlxLoader Loader)
		{
			base.Load(Loader);

			connectionString = Loader.GetAttributeString("ConnectionString", "здесь могла быть ваша реклама");
			serviceName = Loader.GetAttributeString("ServiceName", "SUIV");
			siteList = Loader.GetObjectFromAttribute("SiteList", typeof(DlxCollection)) as DlxCollection;

			var componentName = Loader.GetAttributeString("ComponentName", "Служба");
			var order = Loader.GetAttributeInt("Order", 0);
			var timeout = Loader.GetAttributeInt("Timeout", 10);

			components.Add(this as DlxObject, new SuivComponent(componentName, order, timeout));

			foreach(var obj in Loader.CurXmlElement.Elements)
			{
				try
				{
					var dlxComponent = Loader.GetObject(obj.Attributes["Value"]) as DlxObject;

					if(dlxComponent is Dlx.Extensions.ExtDlxCollection)
					{
						foreach(var site in (dlxComponent as Dlx.Extensions.ExtDlxCollection).Objects)
						{
							var protocolDownloader = site.GetObject("PD_" + (site as UniSite).ID) ?? site.GetObject("PD");
							if(protocolDownloader == null)
								continue;

							(protocolDownloader as ISuivComponent).SendState += SendData;
							(protocolDownloader as ISuivComponent).SendAllSitesState += SendData;
							var suivComponent = new SuivComponent(obj.Attributes["ComponentName"], int.Parse(obj.Attributes["Order"]), int.Parse(obj.Attributes["Timeout"]));
							components.Add(protocolDownloader, suivComponent);
						}
					}
					else
					{
						(dlxComponent as ISuivComponent).SendState += SendData;
						(dlxComponent as ISuivComponent).SendAllSitesState += SendData;
						var suivComponent = new SuivComponent(obj.Attributes["ComponentName"], int.Parse(obj.Attributes["Order"]), int.Parse(obj.Attributes["Timeout"]));
						components.Add(dlxComponent, suivComponent);
					}
				}
				catch { }
			}
		}

		/// <summary>
		/// Отправляет в БД данные о состоянии компонента СУИВ для выбранной станции
		/// </summary>
		/// <param name="sender">отправляющий компонент СУИВ</param>
		/// <param name="subname">подназвание компонента</param>
		/// <param name="siteID">ID станции</param>
		/// <param name="requestTime">время отправки состояния</param>
		/// <param name="lastTime">время последних данных</param>
		/// <param name="description">описание состояния</param>
		public void SendData(DlxObject sender, string subname, int siteID, DateTime? lastTime, string description)
		{
			SqlConnection sqlConn = null;

			foreach(var obj in components)
			{
				if(obj.Key == sender && siteID != 0)
				{
					try
					{
						sqlConn = new SqlConnection(connectionString);
						SqlCommand cmd = sqlConn.CreateCommand();
						cmd.CommandText = @"exec UpdateSuivComponentState @SiteID, @OrderID, @ComponentName, @PName, @Timeout, @LastTime, @Description";

						cmd.Parameters.Add("@SiteID", SqlDbType.Int);
						cmd.Parameters.Add("@OrderID", SqlDbType.Int);
						cmd.Parameters.Add("@ComponentName", SqlDbType.VarChar);
						cmd.Parameters.Add("@PName", SqlDbType.VarChar);
						cmd.Parameters.Add("@Timeout", SqlDbType.Int);
						cmd.Parameters.Add("@LastTime", SqlDbType.DateTime);
						cmd.Parameters.Add("@Description", SqlDbType.VarChar);

						if(subname != "")
							subname = string.Format(" ({0})", subname);

						cmd.Parameters["@SiteID"].Value = siteID;
						cmd.Parameters["@OrderID"].Value = obj.Value.Order;
						cmd.Parameters["@ComponentName"].Value = obj.Value.ComponentName + subname;
						cmd.Parameters["@PName"].Value = serviceName;
						cmd.Parameters["@Timeout"].Value = obj.Value.Timeout;
						cmd.Parameters["@LastTime"].Value = ((lastTime == null) || (lastTime == DateTime.MinValue)) ? Convert.DBNull : lastTime;
						cmd.Parameters["@Description"].Value = description;

						sqlConn.Open();
						cmd.ExecuteNonQuery();
					}
					catch { }
					finally
					{
						sqlConn?.Close();
					}
				}
			}
		}
		/// <summary>
		/// Отправляет в БД данные о состоянии компонента СУИВ для выбранных станций
		/// </summary>
		/// <param name="sender">отправляющий компонент СУИВ</param>
		/// <param name="subname">подназвание компонента</param>
		/// <param name="siteList">список станций</param>
		/// <param name="requestTime">время отправки состояния</param>
		/// <param name="lastTime">время последних данных</param>
		/// <param name="description">описание состояния</param>
		public void SendData(DlxObject sender, string subname, List<DlxObject> siteList, DateTime? lastTime, string description)
		{
			foreach(Tdm.Site site in siteList)
			{
				SendData(sender, subname, site.ID, lastTime, description);
			}
		}

		private void worker_DoWork(object sender, DoWorkEventArgs e)
		{
			while(true)
			{
				SendData(this, "", siteList.Objects, DateTime.Now, "");
				Thread.Sleep(5000);
			}
		}
	}
}
