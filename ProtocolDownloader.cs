﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using AfxEx;
using Tdm.Arm.Prot.Readers;


namespace Tdm.Unification
{
    /// <summary>
    /// 
    /// </summary>
    /*public class ProtocolDownloaderSiteList: DlxObject
    {

    }*/
    /// <summary>
    /// Перекачка протоколов разраба в БД
    /// </summary>
    public class ProtocolDownloader : DlxObject, ISuivComponent
    {         
        /// <summary>
        /// Подключение к БД
        /// </summary>
        DBConnector _DBConnector;

        /// <summary>
        /// Читатель данных
        /// </summary>
        ProtocolDataSetReader reader;

        /// <summary>
        /// Таймер получения данных
        /// </summary>
        private Timer Timer;

        /// <summary>
        /// ИД станции
        /// </summary>
        protected int _SiteID;

        /// <summary>
        /// Подсистема объектов унификации
        /// </summary>
        protected UniObjSystem ObjSystem;

        /// <summary>
        /// объект блокировки повторного вызова таймера
        /// </summary>
        private Object SyncObject = new Object();

        /// <summary>
        /// Временной интервал для очередного запроса
        /// </summary>
        LibEx.TimeInterval CurrentReadTimeInterval = new LibEx.TimeInterval();

		/// <summary>
		/// Конвертор диагностического состояния ЛПД3 -> УНИ
		/// </summary>
		v2.DBLpdToUniStatesHelper DSConverter;

		public event Action<DlxObject, string, int, DateTime?, string> SendState;
		public event Action<DlxObject, string, List<DlxObject>, DateTime?, string> SendAllSitesState;

		/// <summary>
		/// Функция таймера. Определение интервала и чтение данных
		/// </summary>
		/// <param name="state"></param>
		private void ReadData(object state)
        {
            // Если не получилось захватить синхр.объект - выполняется чтение. выходим.
            if (Monitor.TryEnter(SyncObject))
                try
				{
					if(_DBConnector.Connect())
					{
						// Определяется временной период очередного запроса
						CheckLastProtocols();
						// Чтение данных
						ReadProtocolData();

						_DBConnector.Disconnect();
					}
					else
					{
						Console.WriteLine(Name + ": Can't connect to DB (" + _DBConnector.ConnectionString + ")");
						SendState?.Invoke(this, "", _SiteID, null, "Не удалось установить соединение с сервером MS SQL");
					}
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Tdm.Unification.ProtocolDownloader.ReadData: " + ex.Message);
					SendState?.Invoke(this, "", _SiteID, null, ex.Message);

				}
                finally
                {
                    Monitor.Exit(SyncObject);
                }
        }

        private void CheckLastProtocols()
        {
           /* CurrentReadTimeInterval.StartTime = DateTime.Parse("13.03.2017");
            CurrentReadTimeInterval.Span = TimeSpan.FromDays(1);
            return;*/

            try
			{
                // получение последних записей в БД
                SqlCommand cmd = new SqlCommand("SELECT MAX(Time) FROM ARH_States WHERE Site_ID=" + _SiteID, _DBConnector.SqlConn);
                object res = cmd.ExecuteScalar();
                if (res != DBNull.Value)
                {
                    // Смещаем на 10 минут ранее, чтобы ничего не пропустить на стыке операций чтения
                    DateTime maxTime = (DateTime)res;// - TimeSpan.FromMinutes(10);

                    if (maxTime < DateTime.Now - TimeSpan.FromDays(10))
                        maxTime = DateTime.Now - TimeSpan.FromDays(10);
                    // корректировка интервала запроса
                    if (maxTime > CurrentReadTimeInterval.StartTime)
                    {
                        CurrentReadTimeInterval.StartTime = maxTime;
                        // запрашиваем по 1 дню, чтобы небыло больших объемов
                        CurrentReadTimeInterval.Span = DateTime.Now - CurrentReadTimeInterval.StartTime + TimeSpan.FromDays(1);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("CheckLastProtocols: " + ex.Message);
            }
        }
        
        /// <summary>
        /// Загрузка параметров
        /// </summary>
        /// <param name="Loader"></param>
        public override void Load(DlxLoader Loader)
        {
            base.Load(Loader);
            Console.WriteLine(Name + ": Load...");
            
            // Заугрзщик протколов
            reader = Loader.GetObjectFromAttribute("Reader") as ProtocolDataSetReader;
            // Подключение к БД
            _DBConnector = Loader.GetObjectFromAttribute("DBConnector") as DBConnector;
            if (_DBConnector == null)
                throw new ArgumentException("Не задан параметр DBConnector для " + m_Name);
            // период времени для запросов
            string tm = "";
            /*if (Loader.GetAttributeValue("StartTime", ref tm))
            {
                CurrentReadTimeInterval.StartTime = DateTime.Parse(tm);
            }
            else*/
                CurrentReadTimeInterval.StartTime = DateTime.Now - TimeSpan.FromDays(10); // качаем данные за 10 последних дней
            //CurrentReadTimeInterval.Span = TimeSpan.FromDays(1);
            CurrentReadTimeInterval.Span = TimeSpan.FromDays(11);

            // ИД станции
            Tdm.Site site = Loader.GetObjectFromAttribute("Site") as Tdm.Site;
            if(site == null)
                throw new ArgumentException("Не задан параметр Site для " + m_Name);
            _SiteID = site.ID;
            // получаем подсистему объектов унификации
            ObjSystem = (UniObjSystem)site.FindObject("Objects");

			DSConverter = Loader.GetObjectFromAttribute("DiagStateConverter") as v2.DBLpdToUniStatesHelper;
			if(DSConverter == null)
				throw new ArgumentException("Не задан параметр DiagStateConvertHelper типа DBLpdToUniStatesHelper  для " + m_Name);

            if (reader== null)
                throw new ArgumentException("Не задан параметр Reader для " + m_Name);
        }

        /// <summary>
        /// Создание объекта. Запуск таймера перекачки протоколов
        /// </summary>
        public override void Create()
        {
            base.Create();

            // стартуем с частотой 100 мсек. потом уменьшим
            Timer = new Timer(ReadData, null, 5000, 10000);
            Console.WriteLine(Name + ": Start protocol download... " + _SiteID);
        }

        /// <summary>
        ////Получение данных протокола по текущему периоду
        /// </summary>
        public void ReadProtocolData()
        {
            Console.WriteLine(Name + ": запрос протоколов " + CurrentReadTimeInterval.StartTime + " длит." + CurrentReadTimeInterval.Span);

            // подготовка параметров запроса
            var @params = new DataReaderParams { QueryName = QueryNames.Data, TimeInterval = CurrentReadTimeInterval };

			try
			{
				var readed = reader.Read(@params);
				OnData(readed, CurrentReadTimeInterval);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message + ": " + ex.InnerException?.Message);
				var message = "Не удалось получить протоколы";
				if(ex.InnerException is SocketException)
				{
					message = "Не удалось установить соединение с сервером протоколов";
				}
				else if(ex.InnerException is ApplicationException)
				{
					message = "У сервера протоколов нет данных по станции";
				}
				SendState?.Invoke(this, "", _SiteID, null, message);
			}
        }

        /// <summary>
        /// Обработка полученных данных
        /// 
        /// </summary>
        /// <param name="dataset"></param>
        /// <param name="interval"></param>
        private void OnData(IEnumerable<DataSet> dataset, LibEx.TimeInterval interval)
        {
            // Разбор полученных данных по протоколам
            List<ProtDiags> protDiagList = new List<ProtDiags>();
            try
            {
				DateTime lastProtocol = new DateTime();
                foreach (var data in dataset)
                {
                    DataTable dt = data.Tables["Main"];
                    Console.WriteLine("RCV Rows count=" + dt.Rows.Count);
                    // замедляем таймер, если дошли до текущих протоколов
                    //if (dt.Rows.Count == 0 && CurrentReadTimeInterval.StartTime + CurrentReadTimeInterval.Span > DateTime.Now)
                    //    Timer.Change(10000, 10000);

                    foreach (DataRow row in dt.Rows)
                    {
                        // обрабатываем запись протокола
                        int LPD3_ObjID = (int)(row["ObjID"]);
                        int LPD3_GroupID = (int)(row["GroupID"]);
                        int LPD3_SubGroupID = (int)(row["SubGroupID"]);
                        uint LPD3_StateID = (uint)(row["StateID"]);
                        DateTime Time = (DateTime)(row["Время"]);
                        byte DiagSign = (byte)(row["Type"]);

                        if (LPD3_ObjID == 348 && ObjSystem.SiteID == 51000393)
                            Console.WriteLine(string.Format("GR:{0} SGR:{1} ST:{2} DSGN:{3} TM:{4}", LPD3_GroupID, LPD3_SubGroupID, LPD3_StateID, DiagSign, Time.Date.ToString() + " " + Time));

                        if (DiagSign == (byte)Tdm.BaseControlObj.DiagSign.Regular ||       // Записи о штатных ситуация не обрабатываем (типа перевод стрелки)
                            DiagSign == (byte)Tdm.BaseControlObj.DiagSign.NotProjected ||   // Записи сбоев, вызванных нерабочими модулями
                            DiagSign == (byte)Tdm.BaseControlObj.DiagSign.NoControl ||      // 
                            DiagSign == (byte)Tdm.BaseControlObj.DiagSign.Guard             // логическое ограждение
                            )       // Записи сбоев с нарушением логики
                            continue;
                        // восстановление нормальной работы
                        bool End = DiagSign == (byte)Tdm.BaseControlObj.DiagSign.Normal;

                        // Проверяем есть ли такой объект унификации
                        if (ObjSystem.UniObjsDictionary.ContainsKey(LPD3_ObjID) == false)
                            continue;

                        // получаем номер унифицированного сбоя из типа объекта и сбоя ЛПД3
                        v2.PKObjType objType = new v2.PKObjType(LPD3_GroupID, LPD3_SubGroupID, v2.PKObjType.AllSubtypes);
                        BaseControlObj.DiagState ds = new BaseControlObj.DiagState();
                        ds.Id = DSConverter.ConvertToUniDiagState(objType, (int)LPD3_StateID);

						//var s = ObjSystem.UniObjsDictionary.Where(x => x.Key == LPD3_ObjID).FirstOrDefault().Value as UniObjectUpTO;

                        // если ДС есть в фильтре то все запишется (передаем LPD3_ObjID, т.к он такой же как и в унификации)
                        if (ObjSystem.FindDiagStateInFilter(ds.Id, LPD3_ObjID))
						{
							// Добавляем в список записей
							protDiagList.Add(new ProtDiags(LPD3_ObjID, ds, Time, End, (ushort)LPD3_ObjID, LPD3_StateID, DiagSign, (uint)LPD3_GroupID, (uint)LPD3_SubGroupID));
							lastProtocol = Time;
						}
                    }
                }

                if (protDiagList.Count > 0)
                {
					try
					{
						Console.WriteLine("Запись данных тех. протокола в базу. " + protDiagList.Count);
						string cmndString = "if object_id('TempDB..#TechProt') is not null begin truncate table #TechProt; end else begin " +
											"create table #TechProt (ID int, adkGroup int not null, adkID int not null, adkState bigint not null, Site_ID int not null, ObjectID int not null, UniDStateID int not null, ProtocolTime datetime not null, IsEnd bit not null, GroupID bigint, SubGroupID bigint) end;";
						string sprocString = "TechProtStore_LPDBV3";
						//string sprocString = "TechProtStore";

						BulkAdd BulkAddObj = new BulkAdd(ref protDiagList, cmndString, sprocString, _SiteID);
						BulkAddObj.AddRecord(_DBConnector.SqlConn);

						SendState?.Invoke(this, "", _SiteID, lastProtocol, "");
					}
					catch
					{
						SendState?.Invoke(this, "", _SiteID, null, "Не удалось записать протоколы в БД");
					}
				}
            }
            catch(Exception ex)
            {
                Console.WriteLine(Name + ": " + ex.Message);
				throw new Exception(ex.Message);
			}
        }
    }
}