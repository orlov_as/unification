﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using AfxEx;
using Dlx.Extensions;
using Ivk.IOSys;

namespace Tdm.Unification.v2
{
	/// <summary>
	/// Тип объекта ПК
	/// </summary>
	public class PKObjType
	{
		/// <summary>
		/// ID группы
		/// </summary>
		public int GroupID { get; protected set; }
		/// <summary>
		/// ID объекта
		/// </summary>
		public int TypeID { get; protected set; }
		/// <summary>
		/// ID подобъекта
		/// </summary>
		public int SubtypeID { get; protected set; }

		/// <summary>
		/// Аналог "*" - все подтипы
		/// </summary>
		public const int AllSubtypes = 0;

		/// <summary>
		/// Конструктор типа объекта ПК
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		public PKObjType(int groupID, int typeID, int subtypeID)
		{
			GroupID = groupID;
			TypeID = typeID;
			SubtypeID = subtypeID;
		}
		public PKObjType() : this(-1, -1, -1) { }

		public override int GetHashCode()
		{
			return GroupID ^ TypeID ^ SubtypeID;
		}

		public override string ToString()
		{
			return string.Format("{0}/{1}/{2}", GroupID, TypeID, SubtypeID != AllSubtypes ? SubtypeID.ToString() : "*");
		}

		public override bool Equals(object obj)
		{
			if(obj == null)
				return false;
			var ot = obj as PKObjType;
			if(ot == null)
				return false;

			return (ot.GroupID == GroupID && ot.TypeID == TypeID && ot.SubtypeID == SubtypeID);
			//return ((ot.GroupID == GroupID || GroupID == 0 || ot.GroupID == 0)
			//    && (ot.TypeID == TypeID || TypeID == 0 || ot.TypeID == 0)
			//    && (ot.SubtypeID == SubtypeID || SubtypeID == 0 || ot.SubtypeID == 0));
		}

		///<summary>
		///</summary>
		///<param name="a"></param>
		///<param name="b"></param>
		///<returns></returns>
		public static bool operator ==(PKObjType a, PKObjType b)
		{
			if(ReferenceEquals(a, b))
				return true;
			if(((object)a == null) || ((object)b == null))
				return false;
			return a.GroupID == b.GroupID && a.TypeID == b.TypeID && a.SubtypeID == b.SubtypeID;
			//return ((a.GroupID == b.GroupID || a.GroupID == 0 || b.GroupID == 0)
			//    && (a.TypeID == b.TypeID || a.TypeID == 0 || b.TypeID == 0)
			//    && (a.SubtypeID == b.SubtypeID || a.SubtypeID == 0 || b.SubtypeID == 0));
		}

		///<summary>
		///</summary>
		///<param name="a"></param>
		///<param name="b"></param>
		///<returns></returns>
		public static bool operator !=(PKObjType a, PKObjType b)
		{
			return !(a == b);
		}
	}

	/// <summary>
	/// Преобразование состояний ПК
	/// </summary>
	public class TdmToUniStateConversionItem
	{
		/// <summary>
		/// Тип объекта ПК
		/// </summary>
		public PKObjType PkObjType { get; protected set; }
		/// <summary>
		/// Унифицированное состояние
		/// </summary>
		public int UniStateID { get; protected set; }
		/// <summary>
		/// Состояние объекта ПК
		/// </summary>
		public int AdkStateID { get; protected set; }
		/// <summary>
		/// Состояние объекта на который ссылается объект ПК
		/// </summary>
		public int RefStateID { get; protected set; }
		/// <summary>
		/// Является ли состояние объекта состоянием по ДС
		/// </summary>
		public bool ByDiag { get; protected set; }

		/// <summary>
		/// Конструктор элемента преобразования
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		/// <param name="adkStateID">Состояние объекта ПК</param>
		/// <param name="uniStateID">Унифицированное состояние</param>
		/// <param name="refStateID">Состояние объекта на который ссылается объект ПК</param>
		public TdmToUniStateConversionItem(int groupID, int typeID, int subtypeID, int adkStateID, int uniStateID, int refStateID = -1, bool byDiag = false)
		{
			PkObjType = new PKObjType(groupID, typeID, subtypeID);
			AdkStateID = adkStateID;
			UniStateID = uniStateID;
			RefStateID = refStateID;
			ByDiag = byDiag;
		}

		public TdmToUniStateConversionItem() : this(-1, -1, -1, -1, -1) { }


		/// <summary>
		/// Получить все сочетания преобразований
		/// </summary>
		/// <param name="conn">Открытое SQL-соединение</param>
		/// <returns></returns>
		public static IEnumerable<TdmToUniStateConversionItem> GetAllItems(SqlConnection conn, string Query)
		{
			var lst = new List<TdmToUniStateConversionItem>();
			SqlDataReader reader = null;
			try
			{
				var cmd = new SqlCommand(Query, conn);
				reader = cmd.ExecuteReader();
				// ReSharper disable PossibleNullReferenceException
				while(reader.Read())
					// ReSharper restore PossibleNullReferenceException
					lst.Add(new TdmToUniStateConversionItem(Convert.ToInt32(reader[0]), Convert.ToInt32(reader[1]),
						Convert.ToInt32(reader[2]),
						Convert.ToInt32(reader[3]), Convert.ToInt32(reader[4])));
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка получения списка элементов! " + ex.Message);
				return null;
			}
			finally
			{
				if(reader != null)
					reader.Close();
			}
			return lst;
		}
	}

	///<summary>
	/// Преобразование диагностики ПК
	///</summary>
	public class TdmToUniDiagStateConversionItem
	{
		/// <summary>
		/// Тип объекта ПК
		/// </summary>
		public PKObjType PkObjType { get; protected set; }
		/// <summary>
		/// Унифицированное диагностическое состояние
		/// </summary>
		public int UniDiagStateID { get; protected set; }
		/// <summary>
		/// Состояние объекта ПК
		/// </summary>
		public int TdmDiagStateID { get; protected set; }

		/// <summary>
		/// Конструктор элемента преобразования
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		/// <param name="tdmStateID">Состояние объекта ПК</param>
		/// <param name="uniStateID">Унифицированное состояние</param>
		public TdmToUniDiagStateConversionItem(int groupID, int typeID, int subtypeID, int tdmStateID, int uniStateID)
		{
			PkObjType = new PKObjType(groupID, typeID, subtypeID);
			TdmDiagStateID = tdmStateID;
			UniDiagStateID = uniStateID;
		}
		public TdmToUniDiagStateConversionItem() : this(-1, -1, -1, -1, -1) { }

		/// <summary>
		/// Получить все сочетания преобразований
		/// </summary>
		/// <param name="conn">Открытое SQL-соединение</param>
		/// <returns></returns>
		public static IEnumerable<TdmToUniDiagStateConversionItem> GetAllItems(SqlConnection conn, string query)
		{
			var lst = new List<TdmToUniDiagStateConversionItem>();
			SqlDataReader reader = null;
			try
			{
				var cmd = new SqlCommand(query, conn);
				reader = cmd.ExecuteReader();
				// ReSharper disable PossibleNullReferenceException
				while(reader.Read())
					// ReSharper restore PossibleNullReferenceException
					lst.Add(new TdmToUniDiagStateConversionItem(Convert.ToInt32(reader[0]), Convert.ToInt32(reader[1]),
						Convert.ToInt32(reader[2]),
						Convert.ToInt32(reader[3]), Convert.ToInt32(reader[4])));
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка получения списка элементов! " + ex.Message);
				return null;
			}
			finally
			{
				if(reader != null)
					reader.Close();
			}
			return lst;
		}
	}

	/// <summary>
	/// Вспомогательный класс, с загрузкой данных из БД
	/// </summary>
	public class DBHelper : DlxObject
	{
		protected string _ConnectionString = string.Empty;

		/// <summary>
		/// Загрузка конфигурации
		/// </summary>
		/// <param name="loader">Загрузчик</param>
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);
			_ConnectionString = loader.GetAttributeString("Conn", string.Empty);
		}

		/// <summary>
		/// Создание объекта
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			if(!LoadData())
				throw DlxExceptionCreator.ArgumentException(this, null, "Не удалось загрузить данные из БД");
			base.Create();
		}

		/// <summary>
		/// Загрузка данных из списка ситуаций
		/// </summary>
		protected virtual bool LoadData()
		{
			return false;
		}
	}

	/// <summary>
	/// Вспомогательный класс преобразования состояний ТДМ
	/// </summary>
	public class DBTdmToUniStatesHelper : DBHelper
	{
		protected static List<TdmToUniStateConversionItem> _states = new List<TdmToUniStateConversionItem>();
		protected static List<TdmToUniDiagStateConversionItem> _diagStates = new List<TdmToUniDiagStateConversionItem>();

		/// <summary>
		/// Запрос правил конвертации состояний из БД
		/// </summary>
		private string statesQuery = "SELECT [ADK_GroupID],[ADK_TypeID],[ADK_SubtypeID],[ADK_State],[Uni_State] FROM [TPD_StatesPK]";
		/// <summary>
		/// Запрос правил конвертации диагностики из БД
		/// </summary>
		private string diagsQuery = "SELECT [ADK_GroupID],[ADK_TypeID],[ADK_SubtypeID],[ADK_DiagState],[Uni_DiagState] FROM [TPD_DiagStatesPK]";
		/// <summary>
		/// Запись в БД полученных состояний ТДМ
		/// </summary>
		private string logQueryStates = @"INSERT INTO [dbo].[ARH_LpdConversionLog] ([Site_ID],[Object_ID],[RefObject_ID],[Group_ID],[Type_ID],[Subtype_ID],[TdmState],[RefTdmState],[UniState],[Time])
                                    VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},getdate())";
		/// <summary>
		/// Запись в БД полученных ДС ТДМ
		/// </summary>
		private string logQueryDiags = @"INSERT INTO [dbo].[ARH_LpdConversionDiagLog] ([Site_ID],[Object_ID],[Group_ID],[Type_ID],[Subtype_ID],[TdmState],[UniState],[Added],[Time])
                                    VALUES ({0},{1},{2},{3},{4},{5},{6},'{7}',getdate())";


		/// <summary>
		/// Неопределенное состояние
		/// </summary>
		public const int UndefinedState = (int)Uni2States.GeneralStates.IndefiniteState_AbsenceCS;
		/// <summary>
		/// Неизвестное состояние
		/// </summary>
		public const int UnknownState = (int)Uni2States.GeneralStates.IndefiniteState_DiscrepancyCS;
		/// <summary>
		/// Законсервированное состояние
		/// </summary>
		public const int ConservedState = (int)Uni2States.GeneralStates.ObjectConserve;

		private static Dictionary<PKObjType, List<int>> _KnownDiscrepancy = new Dictionary<PKObjType, List<int>>();

		/// <summary>
		/// Получить преобразованное значение
		/// </summary>
		/// <param name="type">Тип объекта ПК</param>
		/// <param name="tdmState">Состояние ПК</param>
		/// <returns></returns>
		public virtual int ConvertToUniState(PKObjType type, int tdmState)
		{
			// 0,1, 100 состояния ТДМ эквивалентны 0 унифицированному
			if(tdmState == 0)
				return UndefinedState;
			if(tdmState == 1)
				return UnknownState;

			if(_states.Count == 0)
				return UnknownState;

			var res = _states.Where(i => i.PkObjType == type && i.AdkStateID == tdmState).Select(it => it.UniStateID);

			int state = 0;
			if(res.Count() == 0)
				state = UnknownState;
			else
				state = res.First();

			#region Тестовая запись состояний, сконвертированных как несоответствие
			if(state == 1)
			{
				bool write = false;
				if(!_KnownDiscrepancy.ContainsKey(type))
				{
					write = true;
					_KnownDiscrepancy.Add(type, new List<int>());
				}
				else if(!_KnownDiscrepancy[type].Contains(tdmState))
					write = true;
				if(write)
				{
					_KnownDiscrepancy[type].Add(tdmState);
					StreamWriter sw = new StreamWriter("Discrepancy.txt", true);
					sw.WriteLine("{0}\t{1}", type, tdmState);
					sw.Close();
				}
			}
			#endregion
			return state;
		}

		/// <summary>
		/// Получить преобразованное значение диагностики
		/// </summary>
		/// <param name="type">Тип объекта ПК</param>
		/// <param name="tdmState">Диагностическое состояние ПК</param>
		/// <returns></returns>
		public virtual int ConvertToUniDiagState(PKObjType type, int tdmState)
		{
			if(_diagStates.Count == 0)
				return -1;

			var res = _diagStates.Where(i => i.PkObjType.GroupID == type.GroupID && i.PkObjType.TypeID == type.TypeID && i.TdmDiagStateID == tdmState).Select(it => it.UniDiagStateID);
			if(res.Count() == 0)
			{
				return -1;
			}
			return res.First();
		}

		/// <summary>
		/// Преобразование сбоя ЛПД б.в.3 к унифицированному ДС
		/// </summary>
		/// <param name="LPDBV3_GroupID">ИД группы объекта ЛПд б.в.3</param>
		/// <param name="tdmState">ИД сбоя ЛПД б.в.3</param>
		/// <returns>ИД унифицированного ДС</returns>
		public virtual int ConvertToUniDiagStateByGroup(int LPDBV3_GroupID, int tdmState)
		{
			if(_diagStates.Count == 0)
				return -1;
			var res = _diagStates.Where(i => i.PkObjType.GroupID == LPDBV3_GroupID && i.TdmDiagStateID == tdmState).Select(it => it.UniDiagStateID);
			if(res.Count() == 0)
			{   // запись не преобразованных ДС
				return -1;
			}
			return res.First();
		}

		/// <summary>
		/// Загрузка конфигурации
		/// </summary>
		/// <param name="loader">Загрузчик</param>
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);
			_logEnabled = loader.GetAttributeBool("LogEnabled", false);
		}
		/// <summary>
		/// Загрузка данных из списка ситуаций
		/// </summary>
		protected override bool LoadData()
		{
			// Избегаем повторной загрузки
			if(_states.Count > 0)
				return true;

			bool res = true;
			SqlConnection conn = new SqlConnection(_ConnectionString);
			try
			{
				conn.Open();
				_states = TdmToUniStateConversionItem.GetAllItems(conn, statesQuery).ToList();
				_diagStates = TdmToUniDiagStateConversionItem.GetAllItems(conn, diagsQuery).ToList();
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка соединения с БД!\n" + ex.Message);
				res = false;
			}
			finally
			{
				conn.Close();
			}
			return res;
		}

		protected bool _logEnabled = false;
		protected SqlConnection _logConn;
		protected SqlCommand _logCmd;
		public void LogStatesToDB(int SiteID, int ObjectID, int RefObjectID, PKObjType pt, int tdmState, int refTdmState, int uniState)
		{
			if(!_logEnabled)
				return;

			if(_logConn == null || _logConn.State != System.Data.ConnectionState.Open)
			{
				_logConn = new SqlConnection(_ConnectionString);
				_logConn.Open();
				_logCmd = new SqlCommand(logQueryStates, _logConn);
			}
			try
			{
				_logCmd.CommandText = string.Format(logQueryStates, SiteID, ObjectID, RefObjectID, pt.GroupID, pt.TypeID, pt.SubtypeID, tdmState, refTdmState, uniState);
				_logCmd.ExecuteNonQuery();
			}
			catch
			{
				Trace.WriteLine(string.Format("Write DB error: {0}, {1}, {2}, {3}", SiteID, ObjectID, tdmState, uniState));
			}
		}
		public void LogDiagsToDB(int SiteID, int ObjectID, PKObjType pt, int tdmDiagState, int uniDiagState, bool added)
		{
			if(!_logEnabled)
				return;

			if(_logConn == null || _logConn.State != System.Data.ConnectionState.Open)
			{
				_logConn = new SqlConnection(_ConnectionString);
				_logConn.Open();
				_logCmd = new SqlCommand(logQueryDiags, _logConn);
			}
			try
			{
				_logCmd.CommandText = string.Format(logQueryDiags, SiteID, ObjectID, pt.GroupID, pt.TypeID, pt.SubtypeID, tdmDiagState, uniDiagState, added);
				_logCmd.ExecuteNonQuery();
			}
			catch { }
		}
	}

	///<summary>
	/// Преобразователь в унифицированный формат состояний ПК
	///</summary>
	public class TdmToUniConverter : UniObjectUpSt.ObjectState
	{
		/// <summary>
		/// Хранилище соответствий состояний
		/// </summary>
		protected DBTdmToUniStatesHelper Helper = new DBTdmToUniStatesHelper();
		/// <summary>
		/// Текущее унифицированное состояние объекта
		/// </summary>
		protected int CurrentUniState;
		/// <summary>
		/// Тип родительского объекта ПК
		/// </summary>
		protected PKObjType Type;
		/// <summary>
		/// Объект ТДМ
		/// </summary>
		protected BaseControlObj TdmObject;
		/// <summary>
		/// Запрос только диагностических состояний
		/// </summary>
		protected bool _onlyDiag = false;

		#region Dlx-members
		/// <summary>
		/// Загрузка
		/// </summary>
		/// <param name="loader">загрузчик</param>
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);
			Helper = loader.GetObjectFromAttribute("Helper") as DBTdmToUniStatesHelper;
			_onlyDiag = loader.GetAttributeBool("OnlyDiag", false);
		}
		/// <summary>
		///  Инициализация
		/// </summary>
		/// <param name="obj">родительский объект контроля</param>
		/// <returns>истина, если удачно</returns>
		public override bool Create(UniObject obj)
		{
			if(obj == null)
				return false;
			_controlObj = obj;

			if(!AssignToBaseObject())
				return false;

			Helper.Create();
			return true;
		}
		/// <summary>
		/// Деинициализация
		/// </summary>
		public override void Destroy()
		{ }
		#endregion

		#region Привязка к подсистеме ТДМ
		/// <summary>
		/// Привязка к объекту из подсистем
		/// </summary>
		/// <returns></returns>
		protected virtual bool AssignToBaseObject()
		{
			var stage = (Site)_controlObj.IOSystem.Owner.GetObject("Stage1");
			IOBaseElement system1 = null;
			IOBaseElement system2 = null;

			if(stage == null)
			{   // новая версия ТДМ, с технологическим БАс
				stage = (Site)_controlObj.IOSystem.Owner.GetObject("Stage");
			}
			system1 = stage.GetObject("Objects") as IOBaseElement;

			stage = (Site)_controlObj.IOSystem.Owner.GetObject("Stage2");
			if(stage == null)
				system2 = system1;
			else
				system2 = stage.GetObject("Objects") as IOBaseElement;

			if(system1 == null)
			{
				Trace.WriteLine("Не удалось подключить подсистему Objects1!");
				return false;
			}
			if(system2 == null)
			{
				Trace.WriteLine("Не удалось подключить подсистему Objects2!");
				return false;
			}

			// Обрезаем (прав)/(неправ)
			string trimmedName = GetTrimmedName(_controlObj.Name);

			// поиск в 1-й...
			BaseControlObj el = system1.FindObject(trimmedName) as BaseControlObj;
			if(el == null)
			{   // ... и во 2-й подсистемах
				el = system2.FindObject(trimmedName) as BaseControlObj;
				if(el == null)
				{   // и по ID
					//el = FindByID(system1, system2, _controlObj.Number);
					if(el == null)
					{
						// и среди объектов, вложенных в те, чьё имя содержится в имени искомого
						el = FindByNameInOwnSubObjects(system1, system2, trimmedName);

						// и среди всех вложенных объектов

						//system1.SubElements.Objects

						if(el == null)
						{
							Trace.WriteLine(string.Format("Не удалось найти объект {0} в подсистемах!\nПерегон {1}", _controlObj.Name, _controlObj.Owner.Owner.Name));
							#region Логирование отсутствующих объектов ЛПД
							string fName = string.Format("_AbsentTdmObjs_{0}.txt", _controlObj.Site.Name);
							System.IO.FileInfo fi = new System.IO.FileInfo(fName);
							System.IO.StreamWriter fsw = new System.IO.StreamWriter(fName, true);
							fsw.WriteLine(_controlObj.Name);
							fsw.Flush();
							fsw.Close();
							#endregion
							return true;
							//return false;
						}
					}
					StreamWriter sw = new StreamWriter("convert_error.txt", true);
					sw.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}", _controlObj.Owner.Owner.Name, _controlObj.Number, _controlObj.Name, el.Name));
					sw.Close();
				}
				TdmObject = el;
				Type = new PKObjType(el.Type.GroupID, el.Type.SubGroupID, PKObjType.AllSubtypes);
			}
			else
			{
				var el2 = system2.FindObject(trimmedName) as BaseControlObj;
				if(el2 == null)
				{
					TdmObject = el;
					Type = new PKObjType(el.Type.GroupID, el.Type.SubGroupID, PKObjType.AllSubtypes);
				}
				else
				{
					// 2 объекта в двух подсистемах
					if(el is UniStubControlObj)
					{
						TdmObject = el2;
						Type = new PKObjType(el2.Type.GroupID, el2.Type.SubGroupID, PKObjType.AllSubtypes);
					}
					else
					{
						TdmObject = el;
						Type = new PKObjType(el.Type.GroupID, el.Type.SubGroupID, PKObjType.AllSubtypes);
					}
				}
			}
			// Подписка на изменения состояний объекта
			if(!_onlyDiag)
				TdmObject.ObjState.ChangedEvent += OnStateChanged;
			TdmObject.DiagStatesChanged += OnDiagStateChanged;
			return true;
		}
		/// <summary>
		/// Поиск объекта в подсистеме по ID
		/// </summary>
		/// <param name="system1">Первая подсистема объектов</param>
		/// <param name="system2">Вторая подсистема объектов</param>
		/// <param name="id">ID объекта</param>
		/// <returns></returns>
		protected BaseControlObj FindByID(IOBaseElement system1, IOBaseElement system2, int id)
		{
			var el = system1.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Number == id) as BaseControlObj;
			if(el == null)
			{
				el = system2.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Number == id) as BaseControlObj;
				return el;
			}

			if(el is UniStubControlObj)
				return system2.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Number == id) as BaseControlObj;
			return el;
		}
		/// <summary>
		/// Поиск объекта в подсистеме по имени
		/// </summary>
		/// <param name="system1">Первая подсистема объектов</param>
		/// <param name="system2">Вторая подсистема объектов</param>
		/// <param name="name">Имя объекта</param>
		/// <returns></returns>
		protected BaseControlObj FindByName(IOBaseElement system1, IOBaseElement system2, string name)
		{
			name = name.Replace("'", "|q");
			var el = system1.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
			if(el == null)
			{
				el = system2.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
				return el;
			}

			if(el is UniStubControlObj)
				return system2.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
			return el;
		}
		/// <summary>
		/// Поиск объекта в подсистеме по имени
		/// </summary>
		/// <param name="system1">Первая подсистема объектов</param>
		/// <param name="system2">Вторая подсистема объектов</param>
		/// <param name="name">Имя объекта</param>
		/// <returns></returns>
		protected BaseControlObj FindByNameInOwnSubObjects(IOBaseElement system1, IOBaseElement system2, string name)
		{
			name = name.Replace("'", "|q");
			var el = system1.SubElements.Objects.FirstOrDefault(e => name.Contains(((BaseControlObj)e).Name)) as BaseControlObj;
			DlxCollection innerObjs = null;
			if(el != null)
			{
				innerObjs = el.SubElements;
			}
			else
			{
				el = system2.SubElements.Objects.FirstOrDefault(e => name.Contains(((BaseControlObj)e).Name)) as BaseControlObj;
				if(el != null)
					innerObjs = el.SubElements;
			}
			if(innerObjs != null)
			{   // поиск во вложенных
				el = innerObjs.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
			}
			else
			{
				foreach(DlxObject o in system1.SubElements.Objects)
				{
					innerObjs = (o as BaseControlObj).SubElements;
					if(innerObjs != null)
					{
						el = innerObjs.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
						if(el != null) break;
					}
				}
				if(el == null)
				{
					foreach(DlxObject o in system2.SubElements.Objects)
					{
						innerObjs = (o as BaseControlObj).SubElements;
						if(innerObjs != null)
						{
							el = innerObjs.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
							if(el != null) break;
						}
					}
				}
			}

			if(el is UniStubControlObj)
				return system2.SubElements.Objects.FirstOrDefault(e => ((BaseControlObj)e).Name == name) as BaseControlObj;
			return el;
		}


		/// <summary>
		/// Вырезать (прав)/(неправ), осн/рез
		/// </summary>
		/// <param name="name">Имя объекта</param>
		/// <returns></returns>
		protected string GetTrimmedName(string name)
		{
			if(name.EndsWith(" прав"))
				return name.Remove(name.Length - 6).Trim();
			if(name.EndsWith(" неправ"))
				return name.Remove(name.Length - 8).Trim();
			if(name.EndsWith("(прав)"))
				return name.Remove(name.Length - 7).Trim();
			if(name.EndsWith("(неправ)"))
				return name.Remove(name.Length - 9).Trim();
			return name;
		}
		#endregion

		#region Обработчики изменений
		/// <summary>
		/// Обработчик изменения данных
		/// </summary>
		/// <param name="module">Модуль, данные которго изменились</param>
		/// <param name="time">Время изменения</param>
		/// <param name="force">Обработка внештатной ситуации (обыв связи)</param>
		protected override void OnChanged(IOBaseElement module, DateTime time, bool force)
		{
			BaseControlObj obj = module as BaseControlObj;
			if(obj != null)
				CurrentUniState = Helper.ConvertToUniState(Type, obj.ObjState.Current);
			SetState(CurrentUniState);
			SetModify();
		}
		/// <summary>
		/// Обработчик изменения состояния объекта
		/// </summary>
		protected virtual void OnStateChanged()
		{
			CurrentUniState = Helper.ConvertToUniState(Type, TdmObject.ObjState.Current);
			SetState(CurrentUniState);
			SetModify();

			Helper.LogStatesToDB(this._controlObj.Site.ID, this._controlObj.Number, -1, Type, TdmObject.ObjState.Current, -1, CurrentUniState);
		}
		/// <summary>
		/// Обработчик изменения диагностического состояния объекта
		/// </summary>
		/// <param name="state">Состояние</param>
		/// <param name="added">Добавить или удалить</param>
		protected virtual void OnDiagStateChanged(BaseControlObj.DiagState state, bool added)
		{
			var st = Helper.ConvertToUniDiagState(Type, state.Id);
			if(added)
				_controlObj.ActivateObjDiagState(st);
			else
				_controlObj.DeactivateObjDiagState(st);

			Helper.LogDiagsToDB(this._controlObj.Site.ID, this._controlObj.Number, Type, state.Id, st, added);
		}
		#endregion

		/// <summary>
		/// обработка исходных данных - вызывается, если IsModify() == TRUE
		/// если требуется всегда выполнять обработку, то нужно вызывать SetModify и не вызывать ResetModify 
		/// </summary>
		public override void UpdateObjectState()
		{
			SetState(CurrentUniState);
			ResetModify();
		}
		/// <summary>
		/// Вызывается если нужно установить неизвестное состояние
		/// </summary>
		public override void SetInvalidObjState(int state)
		{
			SetState(state);
		}
		///<summary>
		/// Получить ошибочное состояние
		///</summary>
		///<param name="defaultInvalidState"></param>
		///<returns></returns>
		public override int GetInvalidObjState(int defaultInvalidState)
		{
			return defaultInvalidState;
		}
	}

	/// <summary>
	/// Преобразует диагностику по питанию РШ
	/// </summary>
	public class TdmToUniRSHSupplyConverter : TdmToUniConverter
	{
		private readonly List<int> MainSupplyStates = new List<int>() { 0, 2, 11, 12, 15, 16 };
		private readonly List<int> ReserveSupplyStates = new List<int>() { 1, 13, 14 };
		/// <summary>
		/// Принадлежность состояния текущему объекту
		/// </summary>
		/// <param name="state">Состояние</param>
		/// <returns></returns>
		protected bool IsThisState(int state)
		{
			if(_controlObj.Name.EndsWith(" осн"))
				return MainSupplyStates.Contains(state);
			else if(_controlObj.Name.EndsWith(" рез") || _controlObj.Name.EndsWith(" резер"))
				return ReserveSupplyStates.Contains(state);
			else
				throw new Ivk.IOSys.ConfigurationException(_controlObj,
					"Недопустимый объект для данного класса преобразования! Имя объекта должно содержать \"осн\" или \"рез\"");
		}

		/// <summary>
		/// Обработчик изменения диагностического состояния объекта
		/// </summary>
		/// <param name="state">Состояние</param>
		/// <param name="added">Добавить или удалить</param>
		protected override void OnDiagStateChanged(BaseControlObj.DiagState state, bool added)
		{
			// Выполняем преобразование, если для этого объекта оно
			if(IsThisState(state.Id))
				base.OnDiagStateChanged(state, added);
		}
	}

	/// <summary>
	/// Мультизапросный клиент
	/// </summary>
	class MultiRequestClient : RequestClient
	{
		protected Dictionary<string, string> DestToSiteMap = new Dictionary<string, string>();
		protected List<IOBaseElement> ListIoBase = new List<IOBaseElement>();
		protected List<string> ListDest = new List<string>();
		protected int CurSiteIndex = 0;
		protected int RateQueryInitial = 2000;
		protected bool _WriteLog = false;

		/// <summary>
		/// Загрузка конфигурации
		/// </summary>
		/// <param name="loader">Загрузчик</param>
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);
			RateQueryInitial = _rateQuery;
			_WriteLog = loader.GetAttributeBool("WriteLog", false);
			List<XmlElement> elements;
			loader.CurXmlElement.EnumInhElements(out elements, "dest", null, null);
			string dest = string.Empty;
			string site = string.Empty;
			foreach(XmlElement elem in elements)
			{
				loader.CurXmlElement = elem;
				if(elem.GetAttributeValue("name", ref dest)
					&& (elem.GetAttributeValue("Site", ref site)))
				{
					DestToSiteMap[dest] = site + "%" + elem.GetAttribute("IOSys");
				}
				else
					throw new ArgumentException("MultiRequestClient: " + Owner.Name + ": Error to load item of dest association");
			}
		}

		/// <summary>
		/// Создание объектов
		/// </summary>
		/// <returns></returns>
		public override void Create()
		{
			foreach(KeyValuePair<string, string> keyv in DestToSiteMap)
			{
				string[] path = keyv.Value.Split('%');
				if(path[1].Length == 0)
					path[1] = "IOSys";

				IOBaseElement ioSys = _findRoot.FindObject(path[0] + "/" + path[1]) as IOBaseElement;
				if(ioSys == null)
					throw new ArgumentException("MultiRequestClient: " + Owner.Name + ": Error to create item of dest association:" + (path[0] + "/" + path[1]).ToString());
				ListIoBase.Add(ioSys);

				ListDest.Add(keyv.Key);
			}

			if(ListDest.Count > 0)
			{
				_targetIOsys = ListIoBase[0];
				_dest = ListDest[0];
			}

			base.Create();
		}

		protected override void ProcessAnswer()
		{
			//base.ProcessAnswer();
			if(_answerMsg.Data.Length == 0 || _answerMsg.Code != _requestMsg.Code || _answerMsg.SubCode != _requestMsg.SubCode)
			{
				Console.WriteLine("RequestClient " + Name + ": Получен неверный ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetIOsys.Owner.Name + ": размер данных = " + _answerMsg.Data.Length + "; SubCode = " + ((Dps.UniRequest.ErrorCode)_answerMsg.SubCode).ToString());

				string message = null;
				if(_answerMsg.Code == Dps.UniRequest.RequestCode.Error)
				{
					message = "Не удалось получить данные по станции";
				}

				SetData(message, DateTime.MinValue, _targetIOsys);
			}
			else
			{
				Trace.WriteLine("RequestClient " + Name + ": Получен правильный ответ " + _answerMsg.Code.ToString() + " на запрос " + _requestMsg.Code.ToString() + " к " + _targetIOsys.Owner.Name + ": размер данных = " + _answerMsg.Data.Length);

				// передаем данные коллектору
				SetData(_answerMsg.Data, DateTime.MinValue, _targetIOsys);
			}

			// Переход к следующему запросу
			ToNextSite();
		}

		protected override void OnExchengeError()
		{
			Console.WriteLine(Name + ": OnExchengeError");
			base.OnExchengeError();
			ToNextSite();
		}

		protected override void OnWrongAnswerCode()
		{
			// при несовпадении кодов сообщений - выставляем флаг перезапуска сессии
			// => отсутствие связи со станциями (и закрытие сессии сервером) не приводит к ошибкам сессии (чтение после конца потока)
			_bRestart = true;
		}

		private DateTime _StartPortion = DateTime.Now;

		protected void ToNextSite()
		{
			// переходим к следующей станции
			if(++CurSiteIndex >= ListDest.Count)
			{   // опросили все подсистемы, начинаем заново, выдержав рассчитанный интервал
				_rateQuery = RateQueryInitial;
				CurSiteIndex = 0;

				// вычисляем, сколько времени нужно ждать, чтобы запросы данных повторялись не чаще RateQueryInitial
				/*var msecs = Convert.ToInt32((DateTime.Now - _StartPortion).TotalMilliseconds);
                if (msecs > 0)
                    _rateQuery -= msecs;
                if (_rateQuery < 0)
                    _rateQuery = 0;
                //если задано в конфигурации: выводим в лог время запроса по всем подсистемам данного клиента
                if (_WriteLog)
                {
                    StreamWriter sw = new StreamWriter("Times\\time_" + this.Name + ".txt", true);
                    sw.WriteLine(DateTime.Now + "\t" + msecs);
                    sw.Close();
                }
                _StartPortion = DateTime.Now.AddMilliseconds(_rateQuery);*/
			}
			else
			{   // продолжаем без задержки
				_rateQuery = 0;
			}

			if(ListDest.Count > 0)
			{   // задаем подсистему и путь к очередному клиенту
				_targetIOsys = ListIoBase[CurSiteIndex];
				_dest = ListDest[CurSiteIndex];
			}
			Console.WriteLine("Запрос к " + _dest + " - " + _targetIOsys.Name);
			// изменяем запрос
			PrepareRequest();
		}
	}

	///<summary>
	/// Подсистема объектов ТДМ
	///</summary>
	public class TdmUniObjSystem : UniObjSystem
	{
		public override void Create()
		{
			base.Create();

			if(_concentrator != null)
			{
				BaseIOSystem iosys1 = FindObject("../Stage1/IOSys") as BaseIOSystem;
				if(iosys1 != null)
					_concentrator.AddUpdateDependence(iosys1, this);
				BaseIOSystem iosys2 = FindObject("../Stage2/IOSys") as BaseIOSystem;
				if(iosys2 != null)
					_concentrator.AddUpdateDependence(iosys2, this);
				BaseIOSystem obj1 = FindObject("../Stage1/Objects") as BaseIOSystem;
				if(obj1 != null)
					_concentrator.AddUpdateDependence(obj1, this);
				BaseIOSystem obj2 = FindObject("../Stage2/Objects") as BaseIOSystem;
				if(obj2 != null)
					_concentrator.AddUpdateDependence(obj2, this);

				if(iosys1 == null)
				{
					iosys1 = FindObject("../Stage/IOSys") as BaseIOSystem;
					if(iosys1 != null)
						_concentrator.AddUpdateDependence(iosys1, this);
				}
				if(obj1 == null)
				{
					obj1 = FindObject("../Stage/Objects") as BaseIOSystem;
					if(obj1 != null)
						_concentrator.AddUpdateDependence(obj1, this);
				}
			}
		}
	}

	///<summary>
	/// Подсистема объектов ЛПД
	///</summary>
	public class LpdUniObjSystem : UniObjSystem
	{
		public override void Create()
		{
			base.Create();

			if(_concentrator != null)
			{
				BaseIOSystem iosys = FindObject("../Site/IOSys") as BaseIOSystem;
				if(iosys != null)
					_concentrator.AddUpdateDependence(iosys, this);
				BaseIOSystem obj = FindObject("../Site/Objects") as BaseIOSystem;
				if(obj != null)
					_concentrator.AddUpdateDependence(obj, this);
			}
		}
	}

	#region Вспомогательные классы для загрузки конфигурации ТДМ
	/// <summary>
	/// Класс для отсева дублированных объектов на перегонах
	/// </summary>
	public class UniStubControlObj : BaseControlObj
	{

	}

	///<summary>
	/// Унифицированный объект контроля для загрузки конфигурации перегонов
	///</summary>
	public class UniControlObj : BaseControlObj
	{
		public override void Load(DlxLoader Loader)
		{
			while(Loader.CurXmlElement.ElementsCount > 0)
				Loader.CurXmlElement.RemoveElement(Loader.CurXmlElement.Elements[0]);

			base.Load(Loader);
		}

		protected override Tdm.TechTaskCollection CreateTechTaskCollection()
		{
			return new TechTaskCollection();
		}
	}

	///<summary>
	///</summary>
	public class TechTaskCollection : Tdm.TechTaskCollection
	{
		public override void Load(DlxLoader loader)
		{
			List<XmlElement> sources;
			m_Name = "TASKS";
			loader.CurXmlElement.EnumInhElements(out sources, "ObjTask", null, null);
			XmlElement oldXmlElem = loader.CurXmlElement;
			foreach(XmlElement src in sources)
			{
				loader.CurXmlElement = src;
				TechTask task = new TechTask();
				task.Load(loader);
				Objects.Add(task);
			}
			loader.CurXmlElement = oldXmlElem;
		}
	}
	#endregion

	/// <summary>
	/// Конфигурация станции без проверки типов объектов
	/// </summary>
	public class SiteTypeless : Tdm.Site
	{
		///<summary>
		/// Отмена проверки типа объекта контроля
		///</summary>
		///<param name="type">тип</param>
		public override void CheckType(ObjType type) { }
	}

	/// <summary>
	/// Тип объекта ЛПД (отличается от ПК учетом подстановочных символов (0) при сравнении объектов)
	/// </summary>
	public class LPDObjType : PKObjType
	{
		/// <summary>
		/// Конструктор типа объекта ПК
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		public LPDObjType(int groupID, int typeID, int subtypeID)
		{
			GroupID = groupID;
			TypeID = typeID;
			SubtypeID = subtypeID;
		}

		public override bool Equals(object obj)
		{
			if(obj == null)
				return false;
			var ot = obj as LPDObjType;
			if(ot == null)
				return false;

			return ((ot.GroupID == GroupID || GroupID == 0 || ot.GroupID == 0)
				&& (ot.TypeID == TypeID || TypeID == 0 || ot.TypeID == 0)
				&& (ot.SubtypeID == SubtypeID || SubtypeID == 0 || ot.SubtypeID == 0));
		}

		///<summary>
		///</summary>
		///<param name="a"></param>
		///<param name="b"></param>
		///<returns></returns>
		public static bool operator ==(LPDObjType a, LPDObjType b)
		{
			if(ReferenceEquals(a, b))
				return true;
			if(((object)a == null) || ((object)b == null))
				return false;
			return ((a.GroupID == b.GroupID || a.GroupID == 0 || b.GroupID == 0)
				&& (a.TypeID == b.TypeID || a.TypeID == 0 || b.TypeID == 0)
				&& (a.SubtypeID == b.SubtypeID || a.SubtypeID == 0 || b.SubtypeID == 0));
		}

		///<summary>
		///</summary>
		///<param name="a"></param>
		///<param name="b"></param>
		///<returns></returns>
		public static bool operator !=(LPDObjType a, LPDObjType b)
		{
			return !(a == b);
		}
	}

	/// <summary>
	/// Преобразование состояний ЛПД
	/// </summary>
	public class LpdToUniStateConversionItem : TdmToUniStateConversionItem
	{
		/// <summary>
		/// Состояние ссылочного объекта ПК
		/// </summary>
		public int AdkRefStateID { get; protected set; }

		/// <summary>
		/// Конструктор элемента преобразования
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		/// <param name="adkStateID">Состояние объекта ПК</param>
		/// <param name="uniStateID">Унифицированное состояние</param>
		/// <param name="priority">Код приоритета условия требований</param>
		public LpdToUniStateConversionItem(int groupID, int typeID, int subtypeID, int adkStateID, int uniStateID, int adkRefStateID)
		{
			PkObjType = new LPDObjType(groupID, typeID, subtypeID);
			AdkStateID = adkStateID;
			AdkRefStateID = adkRefStateID;
			UniStateID = uniStateID;
		}
		public LpdToUniStateConversionItem(int groupID, int typeID, int subtypeID, int adkStateID, int uniStateID)
		{
			PkObjType = new LPDObjType(groupID, typeID, subtypeID);
			AdkStateID = adkStateID;
			UniStateID = uniStateID;
		}
		public LpdToUniStateConversionItem() : this(-1, -1, -1, -1, -1) { }
		/// <summary>
		/// Получить все сочетания преобразований
		/// </summary>
		/// <param name="conn">Открытое SQL-соединение</param>
		/// <returns></returns>
		public static IEnumerable<TdmToUniStateConversionItem> GetAllItems(SqlConnection conn, string Query)
		{
			var lst = new List<TdmToUniStateConversionItem>();
			SqlDataReader reader = null;
			try
			{
				var cmd = new SqlCommand(Query, conn);
				reader = cmd.ExecuteReader();
				while(reader.Read())
					lst.Add(new LpdToUniStateConversionItem(Convert.ToInt32(reader[0]), Convert.ToInt32(reader[1]),
						Convert.ToInt32(reader[2]), Convert.ToInt32(reader[3]),
						Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5])));
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка получения списка элементов! " + ex.Message);
				return null;
			}
			finally
			{
				if(reader != null)
					reader.Close();
			}
			return lst;
		}
	}

	public class LpdToUniStateFromDiagConversionItem : LpdToUniStateConversionItem
	{
		/// <summary>
		/// Код приоритета условия требований
		/// </summary>
		public int Priority { get; protected set; }

		/// <summary>
		/// Код приоритета, устанавливаемый состояниям "по умолчанию"
		/// </summary>
		public const int PriorityDefault = -1;


		/// <summary>
		/// Конструктор элемента преобразования
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		/// <param name="adkStateID">Состояние объекта ПК</param>
		/// <param name="uniStateID">Унифицированное состояние</param>
		/// <param name="priority">Код приоритета условия требований</param>
		public LpdToUniStateFromDiagConversionItem(int groupID, int typeID, int subtypeID, int adkStateID, int uniStateID, int adkRefStateID, int priority)
		{
			PkObjType = new LPDObjType(groupID, typeID, subtypeID);
			AdkStateID = adkStateID;
			AdkRefStateID = adkRefStateID;
			UniStateID = uniStateID;
			Priority = priority;
		}
		/// <summary>
		/// Получить все сочетания преобразований
		/// </summary>
		/// <param name="conn">Открытое SQL-соединение</param>
		/// <returns></returns>
		public static IEnumerable<LpdToUniStateFromDiagConversionItem> GetAllItems(SqlConnection conn, string Query)
		{
			var lst = new List<LpdToUniStateFromDiagConversionItem>();
			SqlDataReader reader = null;
			try
			{
				var cmd = new SqlCommand(Query, conn);
				reader = cmd.ExecuteReader();
				while(reader.Read())
					lst.Add(new LpdToUniStateFromDiagConversionItem(Convert.ToInt32(reader[0]), Convert.ToInt32(reader[1]), Convert.ToInt32(reader[2]),
						Convert.ToInt32(reader[3]), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), Convert.ToInt32(reader[6])));
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка получения списка элементов! " + ex.Message);
				return null;
			}
			finally
			{
				if(reader != null)
					reader.Close();
			}
			return lst;
		}
	}

	///<summary>
	/// Преобразование диагностики ПК
	///</summary>
	public class LpdToUniDiagStateConversionItem : TdmToUniDiagStateConversionItem
	{
		/// <summary>
		/// Конструктор элемента преобразования
		/// </summary>
		/// <param name="groupID">ID группы</param>
		/// <param name="typeID">ID типа объекта</param>
		/// <param name="subtypeID">ID подобъекта</param>
		/// <param name="tdmStateID">Состояние объекта ПК</param>
		/// <param name="uniStateID">Унифицированное состояние</param>
		public LpdToUniDiagStateConversionItem(int groupID, int typeID, int subtypeID, int tdmStateID, int uniStateID)
		{
			PkObjType = new PKObjType(groupID, typeID, subtypeID);
			TdmDiagStateID = tdmStateID;
			UniDiagStateID = uniStateID;
		}
		public LpdToUniDiagStateConversionItem() : this(-1, -1, -1, -1, -1) { }
	}

	/// <summary>
	/// Вспомогательный класс преобразования состояний ЛПД (DpsSt)
	/// </summary>
	public class DBLpdToUniStatesHelper : DBTdmToUniStatesHelper
	{
		/// <summary>
		/// Запрос из БД правил конвертации состояний
		/// </summary>
		protected string statesQuery = "SELECT [ADK_GroupID],[ADK_TypeID],[ADK_SubtypeID],[ADK_State],[Uni_State],[ADK_RefState] FROM [TPD_StatesLPD] where ByDiag=0";
		/// <summary>
		/// Запрос из БД правил конвертации диагностики в состояния
		/// </summary>
		protected string statesFromDiagQuery = "SELECT [ADK_GroupID],[ADK_TypeID],[ADK_SubtypeID],[ADK_State],[Uni_State],[ADK_RefState],[ADK_Priority] FROM [TPD_StatesLPD] where ByDiag=1";
		/// <summary>
		/// Запрос из БД правил конвертации диагностики
		/// </summary>
		protected string diagsQuery = @"SELECT [ADK_GroupID],[ADK_TypeID],[ADK_SubtypeID],[ADK_DiagState],[Uni_DiagState] FROM [TPD_DiagStatesLPD]
                                    --where Uni_DiagState not in (select Uni_DiagState_ID from tpd_diagstatesuni where DiagSignMeasure > 1)";

		protected static List<LpdToUniStateFromDiagConversionItem> _statesFromDiag = new List<LpdToUniStateFromDiagConversionItem>();

		/// <summary>
		/// Ссылочный объект
		/// </summary>
		private BaseControlObj refObj = null;
		/// <summary>
		/// Признак необходимости использования состояния ссылочного объекта
		/// </summary>
		private bool toUseRefObjState = false;

		private List<TdmToUniStateConversionItem> _localStates = new List<TdmToUniStateConversionItem>();
		private List<LpdToUniStateFromDiagConversionItem> _localStatesFromDiag = new List<LpdToUniStateFromDiagConversionItem>();
		private List<TdmToUniDiagStateConversionItem> _localDiagStates = new List<TdmToUniDiagStateConversionItem>();

		/// <summary>
		/// ID преобразования по умолчанию
		/// </summary>
		public const int DefaultRuleID = -1;

		/// <summary>
		/// Загрузка данных из списка ситуаций
		/// </summary>
		protected override bool LoadData()
		{
			// Избегаем повторной загрузки
			if(_states.Count > 0)
				return true;

			bool res = true;
			SqlConnection conn = new SqlConnection(_ConnectionString);
			try
			{
				conn.Open();
				_states = LpdToUniStateConversionItem.GetAllItems(conn, statesQuery).ToList();
				_statesFromDiag = LpdToUniStateFromDiagConversionItem.GetAllItems(conn, statesFromDiagQuery).ToList();
				_diagStates = LpdToUniDiagStateConversionItem.GetAllItems(conn, diagsQuery).ToList();
			}
			catch(Exception ex)
			{
				Trace.WriteLine("Ошибка соединения с БД!\n" + ex.Message);
				res = false;
			}
			finally
			{
				conn.Close();
			}
			return res;
		}


		/// <summary>
		/// Получить преобразованное значение
		/// </summary>
		/// <param name="type">Тип объекта ПК</param>
		/// <param name="tdmState">Состояние ПК</param>
		/// <returns></returns>
		public override int ConvertToUniState(PKObjType type, int tdmState)
		{
			// 0,1, 100 состояния ТДМ эквивалентны 0 унифицированному
			if(tdmState == 0)
				return UndefinedState;
			if(tdmState == 1 || tdmState == 100)
				return UnknownState;
			if(tdmState == 101)
				return ConservedState;

			if(_states.Count == 0)
				return UnknownState;

			//var res = _states.Where(i => (LPDObjType)i.PkObjType == (LPDObjType)type && i.AdkStateID == tdmState).Select(it => it.UniStateID);
			// поиск по сокращенному списку правил (только для данного подобъекта)
			var res = _localStates.Where(i => i.AdkStateID == tdmState).Select(it => it.UniStateID);

			int state = UndefinedState;
			if(res.Count() == 0)
				state = UnknownState;
			else if(res.Count() > 1)
			{   // состояния стрелок, зависящие от РЦ
				state = ConvertToUniStateByRef(_localStates, type, tdmState, refObj == null ? 0 : refObj.ObjState.Current, toUseRefObjState);
				refObj = null;
			}
			else
				state = res.First();

			return state;
		}
		public int ConvertToUniStateByRef(List<TdmToUniStateConversionItem> states, PKObjType type, int tdmState, int tdmRefState, bool useRefState)
		{
			List<LpdToUniStateConversionItem> sts = states
				.Where(i => (LPDObjType)i.PkObjType == (LPDObjType)type && i.AdkStateID == tdmState)
				.Select(s => s as LpdToUniStateConversionItem).ToList();
			if(useRefState && tdmRefState != 0)
			{
				foreach(LpdToUniStateConversionItem s in sts)
					if(s != null && s.AdkRefStateID == tdmRefState)
						return s.UniStateID;
			}
			else
			{
				return sts.Min(s => s.UniStateID);
			}
			return UnknownState;
		}

		/// <summary>
		/// Получение кода униф.состояния по кодам ДС контролируемого объекта
		/// </summary>
		/// <param name="states">Правила преобразования (для подобъекта)</param>
		/// <param name="obj">Контролируемый объект</param>
		/// <returns>Код униф.состояния</returns>
		public int ConvertToUniStateByPriority(List<LpdToUniStateFromDiagConversionItem> states, BaseControlObj obj)
		{
			var priorities = states.Where(s => s.Priority > DefaultRuleID).Select(s => s.Priority).Distinct().OrderBy(s => s).ToList();

			// поиск первого (в порядке приоритета) правила, все ДС которого есть у контролируемого объекта
			foreach(int p in priorities)
			{
				var rules = states.Where(s => s.Priority == p).ToList();
				if(rules.Count(r => obj.DiagStates.Count(d => d.Id == r.AdkStateID) > 0) == rules.Count)
					return rules[0].UniStateID;
			}
			// если соответствие не найдено - возвращаем default, либо неизвестное состояние
			var defRule = states.Where(s => s.Priority == DefaultRuleID).FirstOrDefault();
			return defRule == null
				? UnknownState
				: defRule.UniStateID;
		}


		/// <summary>
		/// Установка параметров helper-объекта перед конвертацией состояния
		/// </summary>
		/// <param name="o">Ссылочный объект ЛПД</param>
		/// <param name="toUseObjState">Признак использования состояния ссылочного объекта (используется для стрелок)</param>
		/// <param name="ls">Список правил преобразования состояний</param>
		/// <param name="lsd">Список правил преобразования диагностики в состояния</param>
		/// <param name="lds">Список правил преобразования диагностики</param>
		internal void SetRefObj(BaseControlObj o, bool toUseObjState
			, List<TdmToUniStateConversionItem> ls, List<LpdToUniStateFromDiagConversionItem> lsd, List<TdmToUniDiagStateConversionItem> lds)
		{
			refObj = o;
			toUseRefObjState = toUseObjState;
			_localStates = ls;
			_localStatesFromDiag = lsd;
			_localDiagStates = lds;
		}
		public List<TdmToUniStateConversionItem> GetStateConversionItems(PKObjType type)
		{
			return _states.Where(i => (LPDObjType)i.PkObjType == (LPDObjType)type).ToList();
		}
		public List<LpdToUniStateFromDiagConversionItem> GetStateFromDiagConversionItems(PKObjType type)
		{
			return _statesFromDiag.Where(i => (LPDObjType)i.PkObjType == (LPDObjType)type).ToList();
		}
		public List<TdmToUniDiagStateConversionItem> GetDiagStateConversionItems(PKObjType type)
		{
			return _diagStates.Where(i => i.PkObjType == type).ToList();
		}
	}

	///<summary>
	/// Преобразователь в унифицированный формат состояний ЛПД
	///</summary>
	public class LpdToUniConverter : TdmToUniConverter
	{
		/// <summary>
		/// Объект ТДМ по ссылке (влияет на формирование унифицированного состояния)
		/// </summary>
		public BaseControlObj TdmRefObject;
		/// <summary>
		/// Алгоритм исключающей логики для формирования состояний стрелок
		/// </summary>
		protected SwitchStateNewestAlg _swAlg = null;

		protected List<TdmToUniStateConversionItem> _localStates = new List<TdmToUniStateConversionItem>();
		protected List<LpdToUniStateFromDiagConversionItem> _localStatesFromDiag = new List<LpdToUniStateFromDiagConversionItem>();
		protected List<TdmToUniDiagStateConversionItem> _localDiagStates = new List<TdmToUniDiagStateConversionItem>();

		/// <summary>
		/// Признак объекта, состояние которого формируется по диагностике
		/// </summary>
		public bool ByDiag { get; protected set; }
		/// <summary>
		/// Состояние, преборазованное по диагностике
		/// </summary>
		protected int StateByDiag = 0;

		/// <summary>
		/// Правила преобразования состояний для подобъекта
		/// </summary>
		protected static List<TdmToUniStateConversionItem> _states = new List<TdmToUniStateConversionItem>();
		/// <summary>
		/// Правила преобразования диагностики для подобъекта
		/// </summary>
		protected static List<TdmToUniDiagStateConversionItem> _diagStates = new List<TdmToUniDiagStateConversionItem>();


		public const string refObjDataName = "SwSection";
		protected List<string> refSwitches = new List<string> { "NextPl", "NextMi", "PrevSec" };

		protected bool _bTraceConsole = false;
		protected bool IsTraceConsole
		{
			get
			{
				return _bTraceConsole;
			}
		}

		#region Dlx-members
		public override void Load(DlxLoader loader)
		{
			base.Load(loader);

			_bTraceConsole = loader.GetAttributeBool("Trace", false);
		}
		/// <summary>
		///  Инициализация
		/// </summary>
		/// <param name="obj">родительский объект контроля</param>
		/// <returns>истина, если удачно</returns>
		public override bool Create(UniObject obj)
		{
			if(obj == null)
				return false;
			_controlObj = obj;

			if(!AssignToBaseObject())
				return false;

			if(_controlObj.Type.SubGroupID == 2)
			{   // инициализация алгоритма исключающей логики для стрелок
				_swAlg = new SwitchStateNewestAlg(TdmRefObject);
				if(!_swAlg.Create(obj))
					_swAlg = null;
			}

			DBLpdToUniStatesHelper hlp = Helper as DBLpdToUniStatesHelper;
			if(hlp != null)
			{
				_localStates = hlp.GetStateConversionItems(Type);
				_localStatesFromDiag = hlp.GetStateFromDiagConversionItems(Type);
				ByDiag = _localStatesFromDiag.Count > 0;
				if(ByDiag)
				{
					StateByDiag = hlp.ConvertToUniStateByPriority(_localStatesFromDiag, TdmObject);
				}
				_localDiagStates = hlp.GetDiagStateConversionItems(Type);
			}
			Helper.Create();
			return true;
		}
		#endregion

		#region Привязка к подсистеме ТДМ
		/// <summary>
		/// Привязка к объекту из подсистемы
		/// </summary>
		/// <returns></returns>
		protected override bool AssignToBaseObject()
		{
			var stage = (Site)_controlObj.IOSystem.Owner.GetObject("Site");
			IOBaseElement objsys = stage.GetObject("Objects") as IOBaseElement;

			if(objsys == null)
			{
				Trace.WriteLine("Не удалось подключить подсистему объектов!");
				return false;
			}

			BaseControlObj el = FindByID(objsys, objsys, _controlObj.Number);
			if(el == null)
			{
				el = FindByName(objsys, objsys, _controlObj.Name);
				if(el == null)
				{
					Trace.WriteLine(string.Format("Не удалось найти объект {0} в подсистеме объектов!", _controlObj.Name));
					#region Логирование отсутствующих объектов ЛПД
					string fName = string.Format("_AbsentLpdObjs_{0}.txt", _controlObj.Site.Name);
					System.IO.FileInfo fi = new System.IO.FileInfo(fName);
					System.IO.StreamWriter sw = new System.IO.StreamWriter(fName, true);
					sw.WriteLine(_controlObj.Name);
					sw.Flush();
					sw.Close();
					#endregion
					return true;
				}
			}
			TdmObject = el;
			Type = new LPDObjType(el.Type.GroupID, el.Type.SubGroupID, el.Type.SubObjectID);

			if(_controlObj.Type.SubGroupID == 2)
			{
				AssignToRefObj(objsys);

			}

			// Подписка на изменения состояний объекта
			if(!_onlyDiag)
				TdmObject.ObjState.ChangedEvent += OnStateChanged;
			TdmObject.DiagStatesChanged += OnDiagStateChanged;
			return true;
		}
		/// <summary>
		/// Привязка к ссылочному объекту из подсистемы
		/// </summary>
		/// <param name="sys">Подсистема объектов</param>
		protected void AssignToRefObj(IOBaseElement sys)
		{
			DlxObject rc = _controlObj.ObjData.Where(d => d.Name == refObjDataName).FirstOrDefault();
			UniObject uniRC = null;
			if(rc != null)
			{
				uniRC = (rc as ObjData).Object as UniObject;
				TdmRefObject = FindByID(sys, sys, uniRC.Number);
				if(TdmRefObject == null)
				{
					TdmRefObject = FindByName(sys, sys, uniRC.Name);
				}

				if(TdmRefObject != null)
				{
					if(!_onlyDiag)
						TdmRefObject.ObjState.ChangedEvent += OnStateChanged;
					//TdmRefObject.DiagStatesChanged += OnDiagStateChanged;
				}
			}

			var sws = _controlObj.ObjData.Where(d => refSwitches.Contains(d.Name)).ToList();
			foreach(DlxObject sw in sws)
			{
				UniObject nextSw = null;
				if(sw != null)
				{
					UniObject uniRefObj = (sw as ObjData).Object as UniObject;
					if(uniRefObj.Type.SubGroupID == 1)
					{
						nextSw = SwitchStateNewAlg.FindNextSwitch(uniRefObj, uniRC);
						if(nextSw != null)
						{
							if(!_onlyDiag)
								nextSw.ObjState.ChangedEvent += OnStateChanged;
						}
					}
				}
			}
		}
		#endregion

		#region Обработчики изменений
		/// <summary>
		/// Обработчик обновления состояния объекта
		/// </summary>
		public override void UpdateObjectState()
		{
			ChangeState(_controlObj, TdmObject);
			ResetModify();
		}
		/// <summary>
		/// Обработчик изменения данных
		/// </summary>
		/// <param name="module">Модуль, данные которго изменились</param>
		/// <param name="time">Время изменения</param>
		/// <param name="force">Обработка внештатной ситуации (обыв связи)</param>
		protected override void OnChanged(IOBaseElement module, DateTime time, bool force)
		{
			BaseControlObj obj = module as BaseControlObj;
			if(obj != null)
			{
				ChangeState(_controlObj, obj);
			}
			SetState(CurrentUniState);
			SetModify();
		}
		/// <summary>
		/// Обработчик изменения состояния объекта
		/// </summary>
		protected override void OnStateChanged()
		{
			//ChangeState(_controlObj, TdmObject);
			//SetState(CurrentUniState);
			SetModify();
			_controlObj.UpdateObjectState();
		}
		protected virtual void ChangeState(UniObject uObj, BaseControlObj tObj)
		{
			try
			{
				if(ByDiag)
				{
					SetState(StateByDiag);
				}
				else
				{
					if(Type != null && tObj != null)
					{
						if(uObj.Type.SubGroupID == 2 && uObj.ObjState.Current == 0 && tObj.ObjState.Current != 0)
						{   // для стрелок с незаданным состоянием
							(Helper as DBLpdToUniStatesHelper).SetRefObj(TdmRefObject, false, _localStates, _localStatesFromDiag, _localDiagStates);
							CurrentUniState = Helper.ConvertToUniState(Type, tObj.ObjState.Current);
							SetState(CurrentUniState);
						}

						bool useRC = _swAlg == null ? true : _swAlg.ToUseRCState();
						(Helper as DBLpdToUniStatesHelper).SetRefObj(TdmRefObject, useRC, _localStates, _localStatesFromDiag, _localDiagStates);
						CurrentUniState = Helper.ConvertToUniState(Type, tObj.ObjState.Current);
						// трассировка преобразованиия
						if(IsTraceConsole)
						{
							Console.WriteLine(String.Format("LpdToUniConverter lpd({0}:{1})-{2}.{3}.{4}.{5} uni({6}:{7})-{8}.{9}.{10}.{11}",
								tObj.Name, tObj.Number, Type.GroupID, Type.TypeID, Type.SubtypeID, tObj.ObjState.Current,
								uObj.Name, uObj.Number, uObj.Type.GroupID, uObj.Type.SubGroupID, uObj.Type.SubObjectID, CurrentUniState));
						}

						#region Logging
						DBLpdToUniStatesHelper hlp = Helper as DBLpdToUniStatesHelper;
						if(hlp != null)
						{
							if(TdmRefObject == null)
								hlp.LogStatesToDB(uObj.Site.ID, uObj.Number, -1, Type, tObj.ObjState.Current, -1, CurrentUniState);
							else
								hlp.LogStatesToDB(uObj.Site.ID, uObj.Number, TdmRefObject.Number, Type, tObj.ObjState.Current, TdmRefObject.ObjState.Current, CurrentUniState);
						}
						#endregion

						SetState(CurrentUniState);
					}
				}
			}
			catch { }
		}

		/// <summary>
		/// Обработчик изменения диагностического состояния объекта
		/// </summary>
		/// <param name="state">Состояние</param>
		/// <param name="added">Добавить или удалить</param>
		protected override void OnDiagStateChanged(BaseControlObj.DiagState state, bool added)
		{
			var st = Helper.ConvertToUniDiagState(Type, state.Id);
			// трассировка преобразованиия
			if(IsTraceConsole)
			{
				Console.WriteLine(String.Format("LpdToUniConverter DS lpd({0}:{1})-{2}.{3}.{4}.{5} uni({6}:{7})-{8}.{9}.{10}.{11}",
					TdmObject.Name, TdmObject.Number, Type.GroupID, Type.TypeID, Type.SubtypeID, state.Id,
					_controlObj.Name, _controlObj.Number, _controlObj.Type.GroupID, _controlObj.Type.SubGroupID, _controlObj.Type.SubObjectID, st));
			}

			bool normalState = state.Sign < 4;
			if(st > -1)
			{
				//if (added && !normalState)
				//{
				//    hlp.LogDiagsToDB(_controlObj.Site.ID, _controlObj.Number, Type, state.Id, st, added);
				//}
				if(added && !normalState)
					_controlObj.ActivateObjDiagState(st);
				else if(!added || normalState)
					_controlObj.DeactivateObjDiagState(st);

				_controlObj.UpdateObjectDiagState();
			}

			if(ByDiag)
			{
				DBLpdToUniStatesHelper hlp = Helper as DBLpdToUniStatesHelper;
				StateByDiag = hlp.ConvertToUniStateByPriority(_localStatesFromDiag, TdmObject);
				SetModify();
			}
		}
		#endregion
	}
	
	/// <summary>
	/// Преобразователь в унифицированный формат состояний ЛПД
	/// </summary>
	public class CondState : LpdToUniConverter
	{
		private new DiagStateLpdToUniHelper Helper;

		public override bool Create(UniObject obj)
		{
			if(obj == null)
				return false;
			_controlObj = obj;

			if(!AssignToBaseObject())
				return false;

			if(Type != null)
			{
				_localStates = _localStates.Select(x =>
									new TdmToUniStateConversionItem(obj.Type.GroupID,
																	obj.Type.SubGroupID,
																	obj.Type.SubObjectID,
																	x.AdkStateID,
																	x.UniStateID,
																	x.RefStateID)).ToList();
			}

			return true;
		}

		public override void Load(DlxLoader Loader)
		{
			Helper = Loader.GetObjectFromAttribute("Helper") as DiagStateLpdToUniHelper;

			foreach(var state in Loader.CurXmlElement.Elements)
			{
				int adkState = -1, refState = -1, uniState = -1;
				bool byDiag = false;

				state.GetAttributeValue("AdkState", ref adkState);
				state.GetAttributeValue("UniState", ref uniState);
				state.GetAttributeValue("RefState", ref refState);
				state.GetAttributeValue("ByDiag", ref byDiag);
				ByDiag = byDiag;

				_localStates.Add(new TdmToUniStateConversionItem(0, 0, 0, adkState, uniState, refState, byDiag));
			}
		}

		protected override bool AssignToBaseObject()
		{
			var stage = (Site)_controlObj.IOSystem.Owner.GetObject("Site");
			var objsys = stage.GetObject("Objects") as IOBaseElement;

			if(objsys == null)
			{
				return false;
			}

			var el = FindByID(objsys, objsys, _controlObj.Number);
			if(el == null)
			{
				el = FindByName(objsys, objsys, _controlObj.Name);
				if(el == null)
				{
					return true;
				}
			}
			TdmObject = el;
			Type = new LPDObjType(el.Type.GroupID, el.Type.SubGroupID, el.Type.SubObjectID);

			if(_controlObj.Type.SubGroupID == 2)
			{
				AssignToRefObj(objsys);
			}

			// Подписка на изменения состояний объекта
			if(!_onlyDiag)
				TdmObject.ObjState.ChangedEvent += OnStateChanged;
			TdmObject.DiagStatesChanged += OnDiagStateChanged;
			return true;
		}

		protected override void ChangeState(UniObject uObj, BaseControlObj tObj)
		{
			try
			{
				if(Type != null && tObj != null)
				{
					if(TdmRefObject == null)
						CurrentUniState = ConvertToUniState(tObj.ObjState.Current);
					else
						CurrentUniState = ConvertToUniState(tObj.ObjState.Current, TdmRefObject.ObjState.Current);

					SetState(CurrentUniState);
				}
			}
			catch { }

			#region вариант под вопросом
			//if(ByDiag)
			//{
			//	SetState(StateByDiag);
			//}
			//else
			//{
			//	if(Type != null && tObj != null)
			//	{
			//		if(TdmRefObject == null)
			//			CurrentUniState = ConvertToUniState(tObj.ObjState.Current);
			//		else
			//			CurrentUniState = ConvertToUniState(tObj.ObjState.Current, TdmRefObject.ObjState.Current);

			//		SetState(CurrentUniState);
			//	}
			//}
			#endregion
		}

		protected override void OnDiagStateChanged(BaseControlObj.DiagState state, bool added)
		{
			var st = Helper.ConvertToUniDiagState(Type,state.Id);

			var normalState = state.Sign < 4;
			if(st > -1)
			{
				if(added && !normalState)
					_controlObj.ActivateObjDiagState(st);
				else if(!added || normalState)
					_controlObj.DeactivateObjDiagState(st);

				_controlObj.UpdateObjectDiagState();
			}

			SetModify();

			#region вариант под вопросом
			//if(ByDiag)
			//{
			//	if(TdmRefObject == null)
			//		StateByDiag = ConvertToUniState(_controlObj.ObjState.Current);
			//	else
			//		StateByDiag = ConvertToUniState(_controlObj.ObjState.Current, TdmRefObject.ObjState.Current);

			//	SetModify();
			//}
			#endregion
		}

		protected int ConvertToUniState(int tdmState)
		{
			if(tdmState == 0)
				return 0; //неопределённое состояние
			if(tdmState == 1 || tdmState == 100 || _localStates.Count == 0)
				return 1; //неизвестное состоние
			if(tdmState == 101)
				return 2; //законсервированное состояние

			return _localStates.Where(x => x.AdkStateID == tdmState).FirstOrDefault()?.UniStateID ?? 1;
		}

		protected int ConvertToUniState(int tdmState, int tdmRefState)
		{
			if(tdmState == 0)
				return 0; //неопределённое состояние
			if(tdmState == 1 || tdmState == 100 || _localStates.Count == 0)
				return 1; //неизвестное состоние
			if(tdmState == 101)
				return 2; //законсервированное состояние

			return _localStates.Where(x => (x.AdkStateID == tdmState) && (x.RefStateID == tdmRefState)).FirstOrDefault()?.UniStateID ?? 1;
		}
	}

	/// <summary>
	/// Вспомогательный класс получающий объекты преобразовния ДС в унифицированный формат
	/// </summary>
	public class DiagStateLpdToUniHelper : DBLpdToUniStatesHelper
	{
		public override void Create()
		{
			if(!LoadData())
				throw DlxExceptionCreator.ArgumentException(this, null, "Не удалось загрузить данные из БД");
		}

		public override void Load(DlxLoader loader)
		{
			base.Load(loader);
		}

		protected override bool LoadData()
		{
			if(_diagStates.Count > 0)
				return true;

			bool res = true;
			SqlConnection conn = new SqlConnection(_ConnectionString);
			try
			{
				conn.Open();
				_diagStates = TdmToUniDiagStateConversionItem.GetAllItems(conn, diagsQuery).ToList();
			}
			catch(Exception ex)
			{
				res = false;
			}
			finally
			{
				conn?.Close();
			}
			return res;
		}

		/// <summary>
		/// Получить преобразованное значение диагностики
		/// </summary>
		/// <param name="type">Тип объекта ПК</param>
		/// <param name="tdmState">Диагностическое состояние ПК</param>
		/// <returns></returns>
		public override int ConvertToUniDiagState(PKObjType type, int tdmState)
		{
			if(_diagStates.Count == 0)
				return -1;

			var res = _diagStates.Where(i => (i.PkObjType.GroupID == type.GroupID) && 
											(i.PkObjType.TypeID == type.TypeID) &&	
											(i.TdmDiagStateID == tdmState)).Select(it => it.UniDiagStateID);
			if(res.Count() == 0)
			{
				return -1;
			}
			return res.First();
		}
	}
}
